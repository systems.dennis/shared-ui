describe("Html Class", function () {
  const html = new h();

  it("html instanceof h == true", () => {
    expect(html).toBeInstanceOf(h);
  });

  it("html.isH == true", () => {
    expect(html.isH).toBe(true);
  });
});

describe("static span", function () {
  const span = h.span("className", null, false);

  it("null in text", () => {
    expect(span.get().textContent).toBe("");
  });

  it("no className", () => {
    expect(span.get().classList[0]).toBe("className");
  });
});

describe('static textElement',()=>{
  let textEl = h.textElement('test_class', 'only_text', false)
  it('textElement instanseOf h', ()=>{
    expect(textEl).toBeInstanceOf(h)
  })

  it('textElement tagName', ()=>{
    expect(textEl.get().tagName).toBe("INPUT")
  })

  it("textElement className",()=>{
     expect(textEl.get().classList[0]).toBe('test_class')
  })

  it("textElement placeholder",()=>{
    expect(textEl.get().placeholder).toBe('only_text')
 })
})

describe('static numberElement',()=>{
  let numberEl = h.numberElement('test_class', 'only_number', false)
  it('numberElement instanseOf h', ()=>{
    expect(numberEl).toBeInstanceOf(h)
  })

  it('numberElement tagName', ()=>{
    expect(numberEl.get().tagName).toBe("INPUT")
  })

  it("numberElement className",()=>{
     expect(numberEl.get().classList[0]).toBe('test_class')
  })

  it("numberElement placeholder",()=>{
    expect(numberEl.get().placeholder).toBe('only_number')
  })

  it("text value in numberElement",()=>{
    numberEl.get().value = 'text'
   expect(numberEl.get().value).toBe('')
  })
})

// describe('handleEvent',()=>{
//   let input = h.input('text')
 
//   it('handleEvent onclick function', ()=>{
//     let toggle = true
//     input.handleEvent('onclick', ()=>{toggle = false})
//     input.get().click()
//     expect(toggle).toBeFalse()
//   })
// })

describe('cloneNode method',()=>{
  let input = h.input('text').cl("my_node")
 
  it('cloneNode returned', ()=>{
    let clone = input.cloneNodeEl()
    expect(clone).toBeInstanceOf(h);
    expect(clone.get().tagName).toBe("INPUT");
    expect(clone.ccl("my_node")).toBeTrue()
  })
})

describe('remove method',()=>{
  let div = h.div('class');
  let input = h.input('text').appendTo(div)

  it('remove returned', ()=>{
    expect(div.get().childNodes.length).toBe(1);
    input.remove();
    expect(div.get().childNodes.length).toBe(0);
    
  })
})
// тест что-то если что-то тогда вот это
describe('getChildsLength method',()=>{
  let div = h.div('class');
  h.input('text').appendTo(div)
  h.input('button').appendTo(div)

  it('getChildsLength returned', ()=>{
    expect(div.getChildsLength()).toBe(2);
    h.div('title').appendTo(div)
    expect(div.getChildsLength()).toBe(3);
    
  })
})

describe('h.fromId method',()=>{
  h.input("button").id(5).appendToBody()
  it('h.fromId exeption null', ()=>{
    expect(() => h.fromId(null)).toThrow(new Error('id should not be null'));
  })

  it('h.fromId exeption unexisted Id', ()=>{
    expect(() => h.fromId("unexisted Id")).toThrow(new Error("unexpected null element"));
  })

  it('h.fromId exeption number', ()=>{
    expect(h.fromId(5)).toBeInstanceOf(h);
  })

  it('h.fromId exeption string', ()=>{
    expect(h.fromId("5")).toBeInstanceOf(h);
  })

})

describe('h.fromFirst method',()=>{
  h.input("button").cl('test_class').id('test_id').appendToBody()

  it('h.fromFirst exeption null', ()=>{
    expect(() => h.fromFirst(null)).toThrow(new Error('id should not be null'));
  })
  it('h.fromFirst exeption unexisted Id', ()=>{
    expect(() => h.fromFirst(".unexisted Id")).toThrow(new Error("unexpected null element"));
  })
  it('h.fromFirst search by class', ()=>{
    expect(h.fromFirst(".test_class")).toBeInstanceOf(h);
  })
  it('h.fromFirst search by id', ()=>{
    expect(h.fromFirst("#test_id")).toBeInstanceOf(h);
  })
  it('h.fromFirst search by tag', ()=>{
    expect(h.fromFirst("input")).toBeInstanceOf(h);
  })
})

describe('h.from method',()=>{
  
  it('h.from exeption null', ()=>{
    expect(() => h.from(null)).toThrow(new Error("unexpected null element"));
  })

  it('h.from exeption NOT HTML element', ()=>{
    expect(() => h.from("string")).toThrow(new Error("should be HTML element"));
    expect(() => h.from(5)).toThrow(new Error("should be HTML element"));
    expect(() => h.from({})).toThrow(new Error("should be HTML element"));
    expect(() => h.from([])).toThrow(new Error("should be HTML element"));

  })

  it('h.from exeption h element', ()=>{
    expect(h.from(h.div('test_element'))).toBeInstanceOf(h);
  })

  it('h.from exeption h element', ()=>{
    let element = document.createElement('div')
    expect(h.from(element)).toBeInstanceOf(h);
  })

})

describe('input method', ()=>{

  it('input exeption null', ()=>{
    expect(h.input(null)).toBeInstanceOf(h);
    expect(h.input(null).get().type).toBe("text");
  })
  it('input exeption epmty', ()=>{
    expect(h.input()).toBeInstanceOf(h);
    expect(h.input().get().type).toBe("text");
  })
  it('input exeption boolean', ()=>{
    expect(h.input(true)).toBeInstanceOf(h);
    expect(h.input(true).get().type).toBe("text");
  })
  it('input exeption button', ()=>{
    expect(h.input("button")).toBeInstanceOf(h);
    expect(h.input("button").get().type).toBe("button");
  })
  it('input exeption hidden', ()=>{
    expect(h.input("hidden")).toBeInstanceOf(h);
    expect(h.input("hidden").get().type).toBe("hidden");
  })
})

describe('inputFunc method', ()=>{
  it('inputFunc get function', ()=>{
    expect(h.input('text').inputFunc(()=>{}).get().oninput).toBeInstanceOf(Function); 
  })

  it('inputFunc exeption not function', ()=>{
    expect(()=>h.input('text').inputFunc(5)).toThrow(new Error("The parameter should be a function"));
    expect(()=>h.input('text').inputFunc('string')).toThrow(new Error("The parameter should be a function"));
    expect(()=>h.input('text').inputFunc(null)).toThrow(new Error("The parameter should be a function"));
  })
})


describe('hideIf method', ()=>{
  it('hideIf expect true', ()=>{
    expect(h.input('text').hideIf(true).get().style.display).toBe('none'); 
  })
  it('hideIf expect false', ()=>{
    expect(h.input('text').hideIf(false).get().style.display).toBe(''); 
  })
  it('hideIf expect nothing', ()=>{
    expect(h.input('text').hideIf().get().style.display).toBe(''); 
  })
 
})

describe('clIf method', ()=>{
  it('clIf expect true', ()=>{
    expect(h.input('text').clIf(true, 'my_class').ccl('my_class')).toBeTrue(); 
  })
  it('clIf expect false', ()=>{
    expect(h.input('text').clIf(false, 'my_class').ccl('my_class')).toBeFalse(); 
  })

  it('clIf expect 3 words', ()=>{
    expect(h.input('text').clIf(true, 'first_class second_class third_class').ccl('first_class')).toBeTrue(); 
    expect(h.input('text').clIf(true, 'first_class second_class third_class').ccl('second_class')).toBeTrue(); 
    expect(h.input('text').clIf(true, 'first_class second_class third_class').ccl('third_class')).toBeTrue(); 
  })
})


describe('addIf method', ()=>{
  let func = () => h.div('my_second_class')

  it('addIf expect true', ()=>{
    expect(h.div('my_first_class').addIf(true, h.div('my_second_class')).child(0)).toBeInstanceOf(h); 
  })

  it('addIf expect false', ()=>{
    expect(h.div('my_first_class').addIf(false, h.div('my_second_class')).childNodes().length).toBe(0); 
  })

  it('addIf expect function', ()=>{
    expect(h.div('my_first_class').addIf(true, func).childNodes().length).toBe(1); 
  })
})


describe('has (attribute) method', ()=>{

  it('has search attruibute', ()=>{
    expect(h.div('my_first_class').attr('id', 5).has('id')).toBeTrue(); 
  })

  it('has not search attruibute', ()=>{
    expect(h.div('my_first_class').has('id')).toBeFalse(); 
  })
})

describe('get method', ()=>{

  it('get from h element', ()=>{
    expect(h.div('my_first_class').get()).toBeInstanceOf(HTMLElement); 
  })
})

describe('parent method', ()=>{
  let childElement = h.div('child_element')
  h.div('parent_element').add(childElement)

  it('parent expect nothing', ()=>{
    expect(childElement.parent()).toBeInstanceOf(h); 
  })

  it('parent expect false', ()=>{
    expect(childElement.parent(false)).toBeInstanceOf(HTMLElement); 
  })

  it('has parent', ()=>{
    expect(childElement.parent().ccl('parent_element')).toBeTrue(); 
  })

  it('has no parent, expect nothing', ()=>{
    let element = h.div('child_element')
    expect(()=> element.parent()).toThrow(new Error("unexpected null element"));
  })

  it('has no parent, expect false', ()=>{
    let element = h.div('child_element')
    expect(element.parent(false)).toBeNull(); 
  }) 
})

describe('next method', ()=>{
  let firstElement = h.div('first_element')
  h.div('parent_element').add(firstElement).add(h.div('second_element'))

  it('next expect nothing', ()=>{
    expect(firstElement.next()).toBeInstanceOf(h); 
  })

  it('next expect false', ()=>{
    expect(firstElement.next(false)).toBeInstanceOf(HTMLElement); 
  })

  it('has next element', ()=>{
    expect(firstElement.next().ccl('second_element')).toBeTrue(); 
  })

  it('has no next element, expect nothing', ()=>{
    let firstElement = h.div('first_element')
    expect(()=> firstElement.next()).toThrow(new Error("unexpected null element"));
  })

  it('has no parent, expect false', ()=>{
    let firstElement = h.div('first_element')
    expect(firstElement.next(false)).toBeNull(); 
  }) 
})

describe('val method', ()=>{
 
  it('input val() expect string', ()=>{
    let input = h.input().text('my_text', false)
    expect(input.val()).toBe('my_text'); 
  })

  it('input val() expect empty string', ()=>{
    let input = h.input().text("", false)
    expect(input.val()).toBe(""); 
  })

  it('div val() expect empty string', ()=>{
    let div = h.div('').text("", false)
    expect(div.val()).toBe(""); 
  })

  it('div val() expect empty string', ()=>{
    let div = h.div('').text("my_text", false)
    expect(div.val()).toBe("my_text"); 
  })
})


describe('onkey method', ()=>{
 
  it('onkey expect function', ()=>{
    expect(h.input().onKey(()=>{}).get().onkeyup).toBeInstanceOf(Function); 
  })

  it('onkey expect not function', ()=>{
    expect(()=>h.input().onKey('').get().onkeyup).toThrow(new Error("The parameter should be a function"));
    expect(()=>h.input().onKey(true).get().onkeyup).toThrow(new Error("The parameter should be a function"));
    expect(()=>h.input().onKey(5).get().onkeyup).toThrow(new Error("The parameter should be a function"));
  })
})

describe('tag method', ()=>{
 
  it('tag expect input', ()=>{
    expect(h.tag('input').get().tagName).toBe("INPUT"); 
  })

  it('tag expect div', ()=>{
    expect(h.tag('div').get().tagName).toBe("DIV"); 
  })

  it('tag expect P', ()=>{
    expect(h.tag('P').get().tagName).toBe("P"); 
  })
})

describe('href method', ()=>{
 
  it('href expect string', ()=>{
    expect(h.tag('a').href('hyperlink').get().attributes.href.value).toBe('hyperlink'); 
  })
  it('href expect number', ()=>{
    expect(h.tag('a').href(5).get().attributes.href.value).toBe('5'); 
  })
  it('href expect booleam', ()=>{
    expect(h.tag('a').href(true).get().attributes.href.value).toBe('true'); 
  })
  it('href expect', ()=>{
    expect(h.tag('a').href().get().attributes.href.value).toBe('#'); 
  })
})

describe('add method', ()=>{
  it('add h element', ()=>{
    expect(h.div('parent_element').add(h.div("childElement")).childNodes().length).toBe(1); 
  })
  it('add h element', ()=>{
    let parent = h.div('parent_element').add(h.div("first_childElement"));
    expect(parent.add(h.div("second_childElement")).child(1).ccl("second_childElement")).toBeTrue(); 
  })
  it('add h element', ()=>{
    expect(h.div('parent_element').add().childNodes().length).toBe(0); 
  })
})

describe('prependTo method', ()=>{

  it('prependTo h element', ()=>{
    let div = h.div('parent_element')
    h.div("childElement").prependTo(div)
    expect(div.childNodes().length).toBe(1); 
  })

  it('prependTo html element', ()=>{
    let html = document.createElement('input')
    h.div("childElement").prependTo(html)
    expect(html.childNodes.length).toBe(1); 
  })

  it('prependTo at the beginning of an element ', ()=>{
    let html = h.div('parent').add(h.div('first_child')).add(h.div('second_child'))
    h.div("third_child").prependTo(html)
    expect(html.child(0).ccl("third_child")).toBeTrue(); 
  })
})

describe('toHtml method', ()=>{
  it('toHtml returned', ()=>{
    expect(h.div('parent_element').toHtml()).toBe('<div class="parent_element"></div>'); 
  })
})

describe('getContent method', ()=>{
  it('getContent returned', ()=>{
    expect(h.div('parent_element').add(h.input()).getContent()).toBe('<input type="text">'); 
  })
})

describe('style method', ()=>{
  it('style returned', ()=>{
    expect(h.div('parent_element').style()).toBeInstanceOf(Style); 
  })
})

describe('zIndex method', ()=>{
  it('zIndex expected number', ()=>{
    expect(h.div('parent_element').zIndex(5).get().style.zIndex).toBe("5"); 
  })
  it('zIndex expected string', ()=>{
    expect(h.div('parent_element').zIndex('string').get().style.zIndex).toBe(""); 
  })
})

describe('img method', ()=>{
  it('img expected empty srting', ()=>{
    expect(h.img('').get().src).toBe(Environment.self + Environment.defaultTemplate + "images/" + "?v=" + Environment.version); 
  })

  it('img expected null', ()=>{
    expect(h.img(null).get().src).toBe(Environment.self + Environment.defaultTemplate + "images/" + 'null.png' + "?v=" + Environment.version); 
  })

  it('img expected null with size number', ()=>{
    expect(h.img(null, 3).get().src).toBe(Environment.self + Environment.defaultTemplate + "images/" + 'null.png' + "?v=" + Environment.version); 
  })
  
  it('img expected nothing', ()=>{
    expect(h.img().get().src).toBe(Environment.self + Environment.defaultTemplate + "images/" + 'null.png' + "?v=" + Environment.version); 
  })

  it('img expected filestorage img', ()=>{
    expect(h.img(Environment.fileStorageApi + 'my_image.png').get().src).toBe(Environment.fileStorageApi+ 'my_image.png' + "?v=" + Environment.version); 
  })

  it('img expected filestorage img with size number', ()=>{
    expect(h.img(Environment.fileStorageApi + 'my_image.png', 3).get().src).toBe(Environment.fileStorageApi+ 'my_image.png' +'/'+ 3); 
  })

  it('img expected not filestorage img with size number', ()=>{
    expect(h.img('http://localhost/ROOT/', 3).get().src).toBe("http://localhost/ROOT/" + "?v=" + Environment.version); 
  })

})

describe('first method', ()=>{
  let parent = h.div('parent').add(h.div('first_child')).add(h.div('second_child'))
  it('first expected nothing', ()=>{
    expect(parent.first().ccl('first_child')).toBeTrue(); 
  })
  it('first expected false', ()=>{
    expect(parent.first(false)).toBeInstanceOf(HTMLElement); 
  })
})


describe('second method', ()=>{
  let parent = h.div('parent').add(h.div('first_child')).add(h.div('second_child'))
  it('second expected nothing', ()=>{
    expect(parent.second().ccl('second_child')).toBeTrue(); 
  })
  it('second expected false', ()=>{
    expect(parent.second(false)).toBeInstanceOf(HTMLElement); 
  })
})

describe('last method', ()=>{
  let parent = h.div('parent').add(h.div('first_child')).add(h.div('second_child')).add(h.div('third_child'))
  it('second expected nothing', ()=>{
    expect(parent.last().ccl('third_child')).toBeTrue(); 
  })
  it('second expected false', ()=>{
    expect(parent.last(false)).toBeInstanceOf(HTMLElement); 
  })
})

describe('child method', ()=>{
  let parent = h.div('parent').add(h.div('first_child')).add(h.div('second_child')).add(h.div('third_child'))
  it('child expected nothing', ()=>{
    expect(()=> parent.child()).toThrow(new Error('first parametr should be a number'))
  })
  it('child expected null', ()=>{
    expect(()=> parent.child(null)).toThrow( new Error('first parametr should be a number'))
  })
  it('child expected string', ()=>{
    expect(()=> parent.child('')).toThrow( new Error('first parametr should be a number'))
  })
  it('child expected number, without secind parametr', ()=>{
    expect(parent.child(2).ccl('third_child')).toBeTrue();
    expect(parent.child(2)).toBeInstanceOf(h)
  })
  it('child expected number, with secind parametr', ()=>{
    expect(parent.child(2).ccl('third_child')).toBeTrue();
    expect(parent.child(2, false)).toBeInstanceOf(HTMLElement)
  })
})

describe('performClick method', ()=>{
  let parent = h.div('parent').click(()=>{parent.cl('clicked_element')})
  it('performClick add class', ()=>{
    expect(parent.ccl('clicked_element')).toBeFalse()
    expect(parent.performClick().ccl('clicked_element')).toBeTrue()
  }) 
})


describe('performClickIf method', ()=>{
  it('performClickIf expect true', ()=>{
    let parent = h.div('parent').click(()=>{parent.cl('clicked_element')})
    expect(parent.ccl('clicked_element')).toBeFalse()
    expect(parent.performClickIf(true).ccl('clicked_element')).toBeTrue()
  }) 
  it('performClickIf expect false', ()=>{
    let parent = h.div('parent').click(()=>{parent.cl('clicked_element')})
    expect(parent.ccl('clicked_element')).toBeFalse()
    expect(parent.performClickIf(false).ccl('clicked_element')).toBeFalse()
  }) 
})

describe('draggable method', ()=>{
  it('draggable add attribute', ()=>{
    expect(h.div('class_draggable').draggable().has('draggable')).toBeTrue()
  }) 
})


describe('draggableIf method', ()=>{
  it('draggableIf expected nothing', ()=>{
    expect(h.div('class_draggable').draggableIf().has('draggable')).toBeFalse()
  }) 
  it('draggableIf expected true', ()=>{
    expect(h.div('class_draggable').draggableIf(true).has('draggable')).toBeTrue()
  }) 
  it('draggableIf expected false', ()=>{
    expect(h.div('class_draggable').draggableIf(false).has('draggable')).toBeFalse()
  }) 
})


describe('a method', ()=>{
  it('a expected nothing', ()=>{
    expect(h.a().get().tagName).toBe('A')
  }) 
  it('a expected href, text', ()=>{
    expect(h.a('#', 'my_text').val()).toBe('my_text')
    expect(h.a('#', 'my_text').get().attributes.href.value).toBe('#')
    expect(h.a('#', 'my_text', true).has('target')).toBeFalse()
  }) 
  it('a expected href, text, translate', ()=>{
    expect(h.a('#', 'my_text', true).val()).toBe(MagicPage.translate('my_text'))
    expect(h.a('#', 'my_text', true).has('target')).toBeFalse()
  }) 

  it('a expected href, text, translate target', ()=>{
    expect(h.a('#', 'my_text', true, true).has('target')).toBeTrue()
  })
})

describe('firstByClass method', ()=>{
  let root = h.div('root');
  let one = h.div('one');
  let two = h.div('two').cl('target');
  let three = h.div('three');
  let four = h.div('four');
  let five = h.div('five');
  let six = h.div('six').cl('target');
 
  root.add(one.add(two.add(three.add(four).add(five).add(six))))
 
  it('firstByClass expected nothing', ()=>{
    expect(three.firstByClass()).toBeNull()
  }) 

  it('firstByClass expected class', ()=>{
    expect(three.firstByClass("target").classList.contains('six')).toBeTrue()
    expect(three.firstByClass("target")).toBeInstanceOf(HTMLElement)
  })

  it('firstByClass expected class, and into - false', ()=>{
    expect(three.firstByClass("target", false).classList.contains('two')).toBeTrue()
    expect(three.firstByClass("target", false)).toBeInstanceOf(HTMLElement)
  })

  it('firstByClass expected class, and into - false', ()=>{
    expect(three.firstByClass("target", false, true).ccl("two")).toBeTrue()
    expect(three.firstByClass("target", false, true)).toBeInstanceOf(h)
  })
 
})

describe('textArea method', ()=>{
  it('textArea expected nothing', ()=>{
    expect(h.textArea().get().tagName).toBe('TEXTAREA')
    expect(h.textArea().val()).toBe('')
  }) 
  it('textArea string', ()=>{
    expect(h.textArea('my_text').val()).toBe('my_text')
  }) 
  it('textArea string toTranslate', ()=>{
    expect(h.textArea('my_text', true).val()).toBe(MagicPage.translate('my_text'))
  }) 
})

describe('click method', ()=>{
  it('click get function', ()=>{
    expect(h.input('button').click(()=>{}).get().onclick).toBeInstanceOf(Function); 
  })

  it('click exeption not function', ()=>{
    expect(()=>h.input('button').click(5)).toThrow(new Error("The parameter should be a function"));
    expect(()=>h.input('button').click('string')).toThrow(new Error("The parameter should be a function"));
    expect(()=>h.input('button').click(null)).toThrow(new Error("The parameter should be a function"));
  })

  // it('click exeption stopPrapog', ()=>{   
    //STOP PRAPOGANATION IS NOT TESTED YET
  // })
})


describe('clickIf method', ()=>{
  it('clickIf get true and function', ()=>{
    expect(h.input('button').clickIf(true, ()=>{}).get().onclick).toBeInstanceOf(Function); 
  })

  it('clickIf get false and function', ()=>{
    expect(h.input('button').clickIf(false, ()=>{}).get().onclick).toBeNull(); 
  })
})

describe('change method', ()=>{
  it('change get function', ()=>{
    expect(h.input('button').change(()=>{}).get().onchange).toBeInstanceOf(Function); 
  })

  it('change exeption not function', ()=>{
    expect(()=>h.input('button').change(5)).toThrow(new Error("The parameter should be a function"));
    expect(()=>h.input('button').change('string')).toThrow(new Error("The parameter should be a function"));
    expect(()=>h.input('button').change(null)).toThrow(new Error("The parameter should be a function"));
  })

  // it('change exeption stopPrapog', ()=>{   
    //STOP PRAPOGANATION IS NOT TESTED YET
  // })
})


describe('changeIf method', ()=>{
  it('changeIf get true and function', ()=>{
    expect(h.input('button').changeIf(true, ()=>{}).get().onchange).toBeInstanceOf(Function); 
  })

  it('changeIf get false and function', ()=>{
    expect(h.input('button').changeIf(false, ()=>{}).get().onchange).toBeNull(); 
  })
})

describe('setA method', ()=>{
  it('setA set attribute', ()=>{
    expect(h.div('my_div').setA('title', 'my_attribute').get().getAttribute('title')).toBe('my_attribute'); 
    expect(h.div('my_div').setA('title', 5).get().getAttribute('title')).toBe('5'); 
    expect(h.div('my_div').setA('title', true).get().getAttribute('title')).toBe('true');
    expect(h.div('my_div').setA('title', false).get().getAttribute('title')).toBe('false');  
    expect(h.div('my_div').setA('title', '').get().getAttribute('title')).toBe(''); 
  })

  it('setA title wuth null attribute', ()=>{
    let elem = h.div('my_div').setA('title', 'my_attribute')
    expect(elem.setA('title').get().getAttribute('title')).toBeNull();
    expect(elem.setA('title', null).get().getAttribute('title')).toBeNull(); 
  })


})



