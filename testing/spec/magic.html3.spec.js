describe("fixed method", () => {
  let div = h.div().fixed().appendToBody();
  let position = div.get().style.position;

  it("position is fixed", () => {
    expect(position).toBe("fixed");
  });
});
