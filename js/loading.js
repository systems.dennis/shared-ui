var globalLoadingCurtain = h.div("global_loading_curtain").cl("hidden").add(h.img("loading.gif", 48));

document.body == null ? globalLoadingCurtain.appendTo(document.head) : globalLoadingCurtain.appendToBody();


class Loading {
    #where;
    #closeElement;

    #disabled;
    
    constructor(where = globalLoadingCurtain) {
        this.#where = h.from(where);
    }

    show(blockButton){
        if (blockButton != undefined) {
            blockButton.classList.add("disabled");
            blockButton.setAttribute("disabled", true);
        }

        if (this.#where.get() == globalLoadingCurtain.get()){
            globalLoadingCurtain.show();
            return this ;
        }

        this.#closeElement = h.div("local_curtain");
        this.#closeElement.add(globalLoadingCurtain);
        this.#where.parent().add(this.#closeElement);

        if (utils.isNull(this.#where.parent())) {
            this.#where.parent().add(this.#closeElement);
        }

        this.#closeElement.show();

        if(blockButton) this.#disabled = blockButton;
        return this;
    }

    hide (){
        if (this.#disabled != undefined) {
            this.#disabled.removeAttribute("disabled");
            this.#disabled.classList.remove("disabled");
        }
        if (this.#where == globalLoadingCurtain){
            globalLoadingCurtain.classList.add("hidden");
            return ;
        }


        h.from(this.#where).show();
        let close = this.#closeElement;
         setTimeout(function () {close.remove()}, 100);
    }
}
  