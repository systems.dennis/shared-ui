
class EnvironmentUploader {
  static onLoad;
  static loadingInterval;

  static load(callback) {
    EnvironmentUploader.addIcon();
    EnvironmentUploader.startLoading();
    EnvironmentUploader.onLoad = callback;  
    // ThemeUploader.setCurrentTheme();
  }

  static addIcon() {
    let icon = document.createElement("link");
    icon.href= Environment.self + "/favicon.svg";
    icon.type = "image/svg+xml";
    icon.rel = "icon";
    document.head.append(icon);
  }

  static loadJS(){
      // EnvironmentUploader.loadRoot(() => {
      // EnvironmentUploader.loadScripts(EnvironmentUploader.onLoad);
      // });
  }

  static startLoading(){
    EnvironmentUploader.addCss(Environment.self + 'loading/loading.css');

    let i = 0;
    EnvironmentUploader.loadingInterval = setInterval(() => {
      if(i > 10) {
        EnvironmentUploader.endLoading();
      }
      i++;
    }, 1000);
  }

  static  endLoading(){
    if(document.getElementById("page-loading")) {
      document.getElementById("page-loading").remove();
    }

    if(EnvironmentUploader.loadingInterval) {
      clearInterval(EnvironmentUploader.loadingInterval);
    }
  }

  static loadStyles() {
    Environment.styles.forEach((path) => {
      try {
        EnvironmentUploader.includeCSS(path);
      } catch (Exc) {
        console.log({ Exc });
      }
    });
  }

  static loadScripts(callback) {
    Environment.scripts.forEach((path, i) => {
      let toEnd = i + 1 == Environment.scripts.length ? callback : false;

      try {
        EnvironmentUploader.includeDirect(path, toEnd);
      } catch (Exc) {
        console.log({ Exc });
      }
    });
  }

  static loadRoot(callback) {
    try {
      EnvironmentUploader.loadScript("root7.js", () => {
        const toPush = Root.getRoots();

        toPush.forEach((scriptPath, i) => {
          let toEnd = i + 1 == toPush.length ? callback : false;

          EnvironmentUploader.loadScript(scriptPath, toEnd);
        });
      });
    } catch (Exc) {
      console.log({ Exc });
    }
  }

  static loadScript(path, callback) {
    const head = document.getElementsByTagName("html")[0];
    const script = document.createElement("script");
    script.async = true;
    script.src = Environment.scriptServer + path + '?v=' + Environment.version;
    script.type = "text/javascript";

    if (callback) {
      script.onload = callback;
    }

    head.prepend(script);
  }

  static includeDirect(what, onLoad) {
    const head = document.getElementsByTagName("html")[0];
    const script = document.createElement("script");
    script.defer = true;
    script.async = what.indexOf("js-local") == -1;
    script.src = Environment.self + what + '?v=' + Environment.version;
    script.type = "text/javascript";

    if (onLoad) {
      script.onload = onLoad;
    }

    head.prepend(script);
  }

  static includeCSS(filename, preprocess = true, last = false) {
    var fileFullPath = Environment.self + Environment.defaultTemplate + filename;
    if (!preprocess) {
      EnvironmentUploader.addCss(fileFullPath);

      return;
    }

    try {
      var changedName =
        Environment.serverID + "_" + filename.split("/").join("_");
      var processorFullPath = Environment.PROCESSOR_URL + changedName;
      var processorExistsPath = Environment.PROCESSOR_EXISTS_URL + changedName;

      EnvironmentUploader.runGet(processorExistsPath, (data) => {
        if (data.exists && !Environment.FORCE_PROCESSOR) {
          EnvironmentUploader.addCss(processorFullPath, last);
        } else {
          EnvironmentUploader.runGet(fileFullPath, (data) => {
            EnvironmentUploader.runPost(
              Environment.PROCESSOR_UPLOAD_URL + changedName,
              JSON.stringify({
                text: data,
                root: Environment.self + Environment.defaultTemplate,
              }),
              () => {
                EnvironmentUploader.addCss(processorFullPath, last);
              },
              "application/json"
            );
          });
        }
      });
    } catch (ex) {
      console.log(ex);
    }
  }

  static runGet(path, success) {
    let xhr = new XMLHttpRequest();
    xhr.open("GET", path, true);
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4 && xhr.status === 200) {
        let content = xhr.responseText;
        success(content);
      }
    };

    xhr.send();
  }

  static runPost(url, payloadData, success, contentType = "text/plain") {
    // Create a new XMLHttpRequest object
    var xhr = new XMLHttpRequest();

    // Configure it as a POST request to the specified URL
    xhr.open("POST", url, true);

    // Set the Content-Type header for the payload (assuming it's JSON)
    xhr.setRequestHeader("Content-Type", contentType);

    // Convert the payloadData object to JSON and send it in the request
    xhr.send(payloadData);

    // Set up a callback function to handle the response
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        // Check if the request was successful (status code 2xx)
        if (xhr.status >= 200 && xhr.status < 300) {
          success(xhr.responseText);
          console.log("Response:", xhr.responseText);
        } else {
          // Handle errors here
          console.error("Error:", xhr.status, xhr.statusText);
        }
      }
    };
  }

  static addCss(path, last) {
  
    var script = document.createElement("link");
    script.href = path;
    script.type = "text/css";
    script.rel = "stylesheet";
    document.head.appendChild(script);

    if(last) EnvironmentUploader.loadJS();
    }
}