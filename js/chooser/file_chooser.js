class FileUploader {
    #fileSelector;
    #onUpload;
    #where;
    #loadingListener = null;

    constructor(id, onUpload, parent) {
        if (parent.getParent) {
            this.#where = parent?.getParent()?.getWrapper();
        }

        this.#onUpload = onUpload;
        this.#fileSelector = document.createElement("input");
        this.#fileSelector.type = "file";
        this.#fileSelector.name = "file";

        let form = h.tag("form").id(id).hide();

        form.add(this.#fileSelector);

        if (this.#where) {
            form.appendTo(this.#where)
        } else {
            form.appendToBody();
        }

        let select = this;
        this.#fileSelector.onchange = function () {
            select.constructResult();
        }

    }

    setLoadingListener(func) {
      this.#loadingListener = func;

      return this;
    }

    getFileSelector(){
        return this.#fileSelector;
    }


    fireEvent(type) {
      if(this.#loadingListener) {
        this.#loadingListener(new LoadingEvent(type))
      }
    }


    getFileSelector() {
        return this.#fileSelector;
    }

    clean() {
        return this.#fileSelector.value = '';
    }

    constructResult() {  
      this.fireEvent(LoadingEvent.LOADING_START);
        if (FileReader) {
            let reader = new FileReader();
            let me = this;

            try {
                reader.readAsDataURL(this.#fileSelector.files[0]);
                reader.onload = function (e) {
                    me.upload();
                };

            } catch (Ex) {
                dom.toast("cannot display file")
                me.upload();

            }
        } else {
            dom.toast("Browser doesn't support reading file.", false)   // Not supported
        }
    }

    upload(file){
      var path = Environment.fileStorageApi + '/public/upload/' + Environment.authScope;

      if(_.isNull(file)) {
        file = this.#fileSelector.files[0]
      }

      Executor.postFile(path, (data) => {
        this.fireEvent(LoadingEvent.LOADING_END);
        this.#onUpload(Environment.fileStorageApi + "/public" + data.downloadUrl,
        data.fileName, data.fileType, data);
      }, file, (e) => {})
  }

    openSelector() {
        this.#fileSelector.click();
    }
}