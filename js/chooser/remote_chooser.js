const searchPath = Environment.dataApi + "/api/v1/search/type/";

class RemoteChooser extends Refreshable {
    #where;
    #field;
    #dataField;
    #modal;
    #title
    #id;
    #textValue = _.$("pages.global.search.select");
    // #pagination

    #content = h.div("search_entity_content").get();

    #remoteType;

    #searchComponent;

    #innerForm;
    #initialData;

    constructor(id, where, field, dataField, remoteType = null, title, data = null) {
        super()
        this.#dataField = dataField;
        this.#field = utils.isElement(field) ? field : get(field);
        this.#where = utils.isElement(where) ? where : get(where);
        this.#id = id;
        this.#remoteType = remoteType;
        this.#title = title;
        this.#initialData = data;
        if (this.#field.value != "" && this.#field.value > 0) {
            this.loadDataFromExisting(this.#field.value);
        }
        this.#content.id = this.#id + "_search_entity_content";
        this.drawValue();
        this.drawModal(this.#title);
    }

    getField() {
        return this.#field;
    }

    getModal() {
        return this.#modal;
    }

    refresh() {
        this.showSelection();
    }


    drawValue(text) {
        log.trace(text);
        let link = h.a("#").get();

        let _this = this;
        link.onclick = function (e) {
            e.preventDefault();
            log.trace('first action');
            showSelection(_this);
        }
        text ? link.innerHTML = text : link.innerHTML = this.#textValue;
        this.#where.childNodes[0] == null ? this.#where.appendChild(link) : this.#where.childNodes[0].innerHTML = link.innerHTML;

    }

    drawModal(titleText) {

        let modal = h.div("div").get();
        let searchButton = h.img("img").get();
        searchButton.classList.add("search_button");


        searchButton.src = Environment.self + '/images/search.png';
        let _this = this;

        searchButton.onclick = function (e) {
            _this.showSelection()
        }

        this.#modal = modal;
        searchButton.onclick = function (e) {
            e.preventDefault()
            showSelection(_this)
        }
        modal.classList.add("modal-selector")
        modal.classList.add("hidden_modal")

        let searchForm = h.div("div").get();
        searchForm.classList.add("search_entity_form")
        let searchHeader = h.div("div").get();

        if (titleText) {
            let title = h.div('search_entity_title').get();
            searchForm.appendChild(title);
            title.innerText = _.$(titleText);
        }

        // searchForm.appendChild(this.#pagination)

        searchHeader.classList.add("search", "search_header");

        if (this.#remoteType != null && this.#remoteType.trim() != "") {
            //Now add button can be added! Bravo!

            let div = h.div("searchbox").get();

            this.#where.appendChild(div);

            let addBtn = h.img("add.png.png").wh(16).get();
            let _this = this;
            let formId = _this.#where.id + "_s_box_add" + new Date().getTime();
            addBtn.onclick = (a) => {

                if (_this.#innerForm == undefined) {
                    _this.#innerForm = h.div("modal").get();
                    _this.#innerForm.id = formId;
                    _this.#innerForm.classList.add("hidden", "form");
                    document.body.appendChild(_this.#innerForm);
                }
                try {
                    new fetcher(formId, "/api/v2/" + _this.#remoteType, "form", null, this).new();
                } catch (Exc) {
                    closeButton.click();
                }
            }
            addBtn.classList.add("_s_box_add_btn")

            searchHeader.appendChild(addBtn);

            //Bill Clinton is the 42nd president of the usa.

        }


        searchForm.appendChild(searchHeader);

        let searchText = h.input(dom.TEXT).get("");
        searchText.id = this.#id + "_chooser_req"
        searchText.classList.add("search_text")
        searchText.onkeyup = function (e) {
            _this.showSelection();
        }
        this.#searchComponent = searchText;
        searchHeader.appendChild(searchText);


        searchForm.appendChild(this.#content);

        let footer = h.div("div").get();

        searchForm.appendChild(footer);

        let closeButton = document.createElement("input");

        closeButton.src = Environment.self + "/images/close.png.png";

        closeButton.value = _.$("pages.global.remote_search.close")
        closeButton.type = "button";
        closeButton.classList.add("close_modal")
        closeButton.onclick = function (e) {
            _this.removeModal()
            _this.#innerForm.remove();
        }
        footer.appendChild(closeButton)
        footer.classList.add("search_footer");


        modal.appendChild(searchForm);

        document.body.appendChild(modal);
        dom.linkTo(modal, this.#where);

    }


    removeModal() {
        this.#modal.classList.add("hidden_modal");

    }


    showSelection(page = 0) {
        let type = this.getField().getAttribute('data-type');
        let field = this.getField().getAttribute('data-sfield');

        let _this = this;

        Executor.runGet(searchPath + type + "?s=" + encodeURI(_this.#searchComponent.value) + '&page=' + page, function (res) {
            _this.showVariants(res, field);
        }, true, Executor.HEADERS);

        this.getModal().classList.remove("hidden_modal")
    }

    showVariants(res, field) {
        let content = this.#content;
        content.innerHTML = ''
        for (let item = 0; item < res.content.length; item++) {
            content.appendChild(this.compile(res.content[item], field))
        }

        this.showChooserPagination(res, field);
    }

    compile(res) {

        let li = document.createElement("a");
        li.classList.add("selection");
        li.id = res.id;
        li.href = "";
        let _this = this;
        li.onclick = function (e) {
            e.preventDefault();
            _this.selectData(res)
        }
        li.innerHTML = res[this.#dataField];
        return li
    }

    showChooserPagination(res) {
        let _this = this
        let totalPages = res.totalPages;
        let currentPage = res.number + 1;
        if (totalPages > 1) {
            let pagination = h.div('chooser_pagination');
            let prevBtn = h.img('prev.png').cl('prev_btn_chooser').appendTo(pagination);
            h.div('chooser_bord').text(`${currentPage} / ${totalPages}`, false).appendTo(pagination);
            let nextBtn = h.img('next.png').cl('next_btn_chooser').appendTo(pagination);

            if (currentPage == totalPages) {
                nextBtn.cl('opacity');
                prevBtn.click(function () {
                    _this.showSelection(res.number - 1);
                })
            }
            if (currentPage == 1) {
                prevBtn.cl('opacity');
                nextBtn.click(function () {
                    _this.showSelection(res.number + 1);
                })
            }
            if (currentPage !== totalPages && currentPage !== 1) {
                prevBtn.click(function () {
                    _this.showSelection(res.number - 1);
                })
                nextBtn.click(function () {
                    _this.showSelection(res.number + 1);
                })
            }
            this.#content.prepend(pagination.get())
        }

    }


    selectData(data) {
        this.#field.value = data.id;

        if (data.id == 0) {
            this.#textValue = _.$("pages.global.search.select");
        } else {
            this.#textValue = data[this.#dataField];
            let _this = this;
            let cleanValueImg = h.img('rm.png').click(()=> {
                _this.drawValue(_.$("pages.global.search.select"));
                _this.#field.value = null;
                cleanValueImg.remove();
            });

            let where = h.from(this.#where);

            if (!where.get().querySelector('img')) {
                where.add(cleanValueImg);
            }
        }

        this.drawValue();
        this.removeModal();

    }

    loadDataFromExisting(id) {
        let data = {
            "id": id,

        }
        this.getDataById(id, data);

    }


    getDataById(id, data) {
        let type = this.#field.getAttribute('data-type');
        let _this = this;

        Executor.runGet(searchPath + "toString/" + type + "/" + id, function (res) {
            data[_this.#dataField] = res;
            _this.selectData(data)

        }, true, Executor.HEADERS_PLAIN);

        return undefined;
    }
}

function showSelection(owner) {
    owner.showSelection();
}

function get(id) {
    return document.getElementById(id);
}
