var _structured_view = [];
class MagicView{
    #objectId;
    #id;
    #path;
    #modal;
    #rootData;
    #searchName;
    #parent
    #listener;

    #form = h.div("modal").cl("hidden").id("__view_" + new Date().getDate());

    constructor(objectId, id,  path, rootData = false, searchName, listener) {
        if (objectId < 1){
            return;
        }
        this.#rootData = rootData;
        this.#objectId = objectId;
        this.#id = id;
        this.#path = path;
        this.#searchName = searchName;
        this.#listener = listener;
        this.draw();
    }


    getModal(){
      return this.#modal;
    }

    setParent(parent){
      this.#parent = parent;
    }

    getObjectId(){
      return this.#objectId;
    }

    draw(){
        this.#modal = dom.createModal(dom.REMOVE_TYPE_REMOVE).cl("modal_view")
        document.body.append(this.#form)
        this.#form.appendTo(document.body);
        this.buildContent();
        this.#modal.appendTo(document.body);
        
    }

    buildContent(){
        let path = Environment.dataApi + "/api/v2/" +  this.#path + "/root/fetch/details/" + this.#objectId;
        let _this = this;
        if (_structured_view[path]){
            this.buildForm (_structured_view[path]);
        } else {

            Executor.runGet(path, (ores) => {
              
              Executor.runGet(Environment.dataApi + "/api/v2/" +  this.#path + "/root/fetch/list/", (res) => {
                ores.fields = res.fields;
                _structured_view[path]  = ores;

                _this.buildForm(ores);
              }, true, Executor.HEADERS, (e) => {
                verifyResponse(e)
                dom.toast("Error during loading data", ToastType.ERROR)
            })
            
            }, true, Executor.HEADERS, (e) => {
                verifyResponse(e)
                dom.toast("Error during loading data", ToastType.ERROR)
          })
        }

    }

    buildForm (data){
        let _this = this;

        let dataContent = h.div("view_content").appendTo(this.#modal);
        let header = h.div("__mv_header");
        let img = dom.createCloseImage();
        h.img("edit.png").wh(16).pointer().appendTo(dataContent).click(() => {
  
            let magicFormOptions = new MagicFormOptions( _this.#path); 
            magicFormOptions.setParent(this.#parent.getParent())
            MagicForm.Instance(_this.#path, magicFormOptions).load(_this.#objectId);

        });
        img.get().onclick =  function (e){
            e.preventDefault();
            _this.close();
        }
        header.appendChild(img);
        this.#modal.appendChild(header);

        for (let i = 0; i < data['fields'].length; i ++ ){

            let dataEl =  data.data == undefined ? data  : data.data;

            if (data.fields[i].showContent && data.fields[i].visible ) {
              let id = (data['fields'][i].field + '_m_view');
                h.div("field_content")
                    .add(h.span("label", _.$(data['fields'][i]['translation'], false)))
                    .add(h.span("value").id(id).add(this.transform(dataEl.id, dataEl[data.fields[i].field], data.fields[i])))
                    .appendTo(dataContent);
            }
        }

        let favoriteEnabled = Favorite.enabled();
        if(favoriteEnabled){
            this.searchObjectType(data)
        }
        if(this.#listener) {
          this.#listener(this);
        }
    }


    searchObjectType(objData){

       let _this = this
       let path = Environment.dataApi + '/api/v2/shared/favorite/byName?name=' + this.#searchName;
       if(this.#searchName !='IssueForm'&& _.notNull(this.#searchName)){
          Executor.runGet(path, function (data) {
        _this.buildFavarite(objData, data.result)
       }) 
       }else{
        _this.buildFavarite(objData, 'IssueForm')
       }
      
    }

    buildFavarite(objData, type){

        let heart = h.span(Favorite.FAVORITE_CLASS + "_holder", "", false).click(function (e) {
            Favorite.markElementAsFavorite(e, objData.data, type)
        }).appendTo(this.#modal);

        Favorite.markIfElementFavorite(heart, objData.data, type);
    }


    transform(id, what, header, path = null){ 
      var type = header.type;

      if(_e(type, "file")) {
        return h.img(what, Environment.defaultIconSize)
      }

      if(_e(type, "text") || _e(type, "number")) {
        return h.span("value").text(what, false)
      }

      if (_e(type, "checkbox")) {

          if (what|| _e(what, "true") || _e(what, "True")) { 
              return h.img("true.png", Environment.defaultIconSize)
          } else if (!what || _e(what, "false") || _e(what, "False")) {
              return h.img("false.png", Environment.defaultIconSize)
          }
      } 

      if (_e(type, "object_chooser")) {
          let res = "";
          if (what && what.value) {
              res = h.span("value").pointer()
              .click(() => h.magicView( what.key, header == null ? path:   header.remoteType, false, header.searchName )).text(what.value, false)
          
            } else {
              res = h.span("value");
          }

          if (what && !_.isEmpty(what.additional)){
            let path = what.additional.icon;
            if(!path && what.additional.length) path = what.additional;

            if(res && path) {
              h.img(path, Environment.defaultIconSize).cl("image_in_detail").prependTo(res);
            } else {
              h.img(null, Environment.defaultIconSize).cl("image_in_detail").prependTo(res);
            }

          }
          if (what == null){
              res = "";
          }
          return res;
      }

      if (!what) {
          return what;
      }
      return  what.value;
    }

    close(){
        this.#modal.hideModal()
    }
}