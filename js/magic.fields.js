class FieldResolver {
  #field;
  #fieldData;

  constructor(field) {
    this.#field = field;

    this.#fieldData = field.getData();
  }

  clearErrors() {
    try {
      let err = get("error_" + this.#field.getField());
      if (err == null) {
        return;
      }
      err.innerHTML = "";
      err.classList.add("hidden");

      document
        .getElementById(this.#field.getField())
        .classList.remove("error_state");
    } catch (e) {
      log.error(e);
    }
  }

  read(field) {}

  create(field) {}

  setValue(value) {}

  isRequired() {
    return this.#fieldData.required;
  }

  toShowLabel() {
    return this.#fieldData.showLabel;
  }

  getLabelText() {
    return MagicPage.translate(this.#fieldData.translation);
  }

  toShowPlaceHolder() {
    return this.#fieldData.showPlaceHolder;
  }

  getPlaceHolder() {
    return this.#fieldData.placeHolder;
  }

  getSetting(s) {
    return this.getFieldData()[s];
  }

  getIsId() {
    return this.getFieldData()["id"] || this.#fieldData.field == "id";
  }

  getFieldData() {
    return this.#fieldData;
  }

  getField() {
    return this.#field;
  }

  createContainer() {
    return h.div("row").cl(this.getFieldData().type);
  }

  markElementsRequired(items) {
    if (!this.isRequired()) {
      return;
    }
    for (let i = 0; i < items.length; i++) {
      items.classList.add("required");
    }
  }

  showLabel(div) {
    if (this.toShowLabel()) {
      let labelDiv = h
        .div("row_label_container")
        .cl(this.getSetting("type") + "_label_container")
        .prependTo(div);

      let lb = this.createLabelComponent().cl("label").cl("text_label");

      labelDiv.add(lb);
      if (this.isRequired()) {
        lb.cl("required");
        labelDiv.add(h.span("required_cell", "*"));
      }
    }
  }

  afterAdd() {}

  createLabelComponent() {
    return h.label(this.getField().getField(), this.getLabelText(), false);
  }
}

class MultiFilesSelector extends FieldResolver {
  constructor(field) {
    super(field);
  }

  create(value) {
    if (value == null) {
    }
    let selection = new FileSelector(this.getField()).create();

    h.div("selection");
  }

  #fileSelector = new FileSelector();
}

class FileSelector extends FieldResolver {
  #fileChooser;
  #valueHolder;
  #preview;

  clearErrors() {}

  create(value) {
    let row = this.createContainer();
    let _this = this;
    let info = h
      .img("upload.png.png")
      .wh(16)
      .click((x) => {
        _this.#fileChooser.openSelector();
      });

    let preview = h
      .img("no_image.png.png")
      .wh(48)
      .click((x) => {
        utils.showFullImage(x.src);
      });

    this.#valueHolder = h.input(dom.HIDDEN);
    // this.#valueHolder.value = value;

    this.#fileChooser = new FileUploader(
      this.getField().getField() + "_file_chooser",
      preview,
      function (x) {
        _this.#valueHolder.value = x;
        _this.#preview.src = x;
      }
    );

    let btnScreenshot = h
      .img("sreenshot.png.png")
      .wh(16)
      .click((x) => {
        utils.capture(function (img) {
          //

          img.toBlob((blob) => {
            let file = new File(
              [blob],
              "screenshot_from_" + new Date().getTime() + ".jpg",
              { type: "image/jpeg" }
            );
            const dataTransfer = new DataTransfer();
            dataTransfer.items.add(file);
            _this.#fileChooser.getFileSelector().files = dataTransfer.files;
            _this.#fileChooser.upload();
            //preview.src = img.toDataURL("image/png");
          }, "image/jpeg");
        });
      });

    let div = h.div("dd_data");
    let val = value == null ? null : value[this.getField().getField()];
    let rm = h
      .img("/images/remove.png.png")
      .wh(16)
      .click((x) => {
        _this.#valueHolder.text(null);
        preview.src("no_image.png.png");
      });

    this.#preview = preview;
    let fromList = h
      .img("select.png.png")
      .wh(16)
      .click(() => {
        _this.createFileSelectionForm();
      });

    if (val != null) {
      this.#valueHolder.text(val, false);

      preview.src(val);
    }

    this.markElementsRequired({ info, div });

    this.showLabel(div);

    div.appendChild(this.#valueHolder);
    let divInner = h.div("file_selector_preview");
    divInner.add(info).add(preview).add(btnScreenshot).add(rm).add(fromList);
    div
      .add(divInner)
      .add(dom.errorPlate(this.getField().getField()))
      .appendTo(row);

    return row;
  }

  createFileSelectionForm() {
    let headerConf = h.div("row");
    let form = h.div("findFile").appendTo(headerConf);

    let _this = this;

    form.appendChild(headerConf);
    let fcp = h.span("fcp_class", null).appendTo(headerConf);

    let chkBox = dom.chk(
      this.getField().getField() + "_fcp",
      MagicPage.translate("pages.filechooser.only_public"),
      true,
      fcp
    );

    headerConf.appendChild(fcp);

    let fcOnlyMy = h.span("fcom_class", null).appendTo(headerConf);

    let chkBoxFcOM = dom.chk(
      this.getField().getField() + "_fcom",
      MagicPage.translate("pages.filechooser.only_my"),
      false,
      fcOnlyMy
    );

    let headerSearchField = h.div("row", "search".appendTo(form));

    let searchField = h
      .input("text")
      .id(this.getField().getField() + "_fcsf")
      .onkeyup((x) => _this.fillVariants())
      .appendTo(headerSearchField);

    let content = h.div("fs_content").appendTo(form);

    h.img("close.png.png")
      .wh(16)
      .click(() => form.remove())
      .appendTo(form);

    this.#valueHolder.parent().add(form);

    this.fillVariants(chkBox, chkBoxFcOM, searchField, content);
  }

  setValue(value) {
    this.#valueHolder.value = value;
    this.#preview.src = value;
  }

  fillVariants(chkBoxOnPublic, chkBoxFcOM, searchField, content) {
    let path =
      Environment.fileStorageApi +
      "/cabinet/search?page=0&s=" +
      searchField.value +
      "&onPublic=" +
      chkBoxOnPublic.checked +
      "&onlyMy=" +
      chkBoxFcOM.value;
    let _this = this;
    Executor.runGet(
      path,
      function (data) {
        content.text(null);

        if (data["empty"] == true) {
          content.text(MagicPage.translate("pages.filechooser.no_data"));
        } else {
          for (let i = 0; i < data.content.length; i++) {
            let row = h.div("fc_row").appendTo(content);

            h.span("fc_row_name")
              .text(data.content[i].originalName, false)
              .appendTo(row);
            h.span("fc_row_type").text(data.content[i].type, false);
            let img = h
              .img(
                Environment.fileStorageApi +
                  (data.content[i].pub ? "/public" : "/private") +
                  data.content[i].downloadUrl
              )
              .wh(24)
              .appendTo(row);

            let link = h
              .a("#")
              .text("pages.filechooser.select")
              .click(() => {
                _this.setValue(img.get().src);
                content.parentElement.remove();
              })
              .appendTo(row);
          }
        }
      },
      true,
      Executor.HEADERS,
      function (e) {
        $.toast(MagicPage.translate("global.exception") + e);
      }
    );
  }

  read() {
    return this.#valueHolder.value;
  }
}

class DropDown extends FieldResolver {
  #dropDown;

  constructor(field) {
    super(field);
  }

  create(value) {
    let row = this.createContainer();

    let dropDown = dom.select(this.getField().getField());
    this.#dropDown = dropDown;
    let label = h.label(this.getField(), "").attr("for", this.getField());

    let optionsData = this.getSetting("dataProviders");
    if (optionsData == null || optionsData.length == 0) {
      log.error(
        "Data provider not set for this field " + this.getField().getField()
      );
    }

    if (value != null) {
      dom.setCboValue(dropDown, value);
    }

    let div = h.div("dd_data");

    // $(dropDown).prettyDropdown();

    this.markElementsRequired({ dropDown, div });

    this.showLabel(div);

    div.appendChild(label);
    div.appendChild(dropDown);
    div.appendChild(dom.errorPlate(this.getField().getField()));

    row.appendChild(div);

    return row;
  }

  read(field) {
    return this.#dropDown.value;
  }
}

class DateElement extends FieldResolver {
  #datePicker;

  constructor(field) {
    super(field);
  }

  create(value) {
    let row = h.div("row").cl("date-row");

    let div = h.div("date").appendTo(row);

    // this.markElementsRequired({el, div})
    // this.showLabel(div);

    this.showLabel(div);
     new DateChooser({
      block: row.get(),
      format: "dd/mm/yy",
    }).draw().change((date)=>console.log(date));

    // d.change((data) => console.log(data));
    // let el = h.input("text", this.getField().getField());

    // this.#datePicker = el.jq().datepicker({
    //     format: 'dd.mm.yy',
    //     autoclose: true,
    //     assumeNearbyYear: true,
    //     clearBtn: true,
    //     todayHighlight: true,

    // });
    // this.#datePicker.datepicker('option', 'dateFormat', "dd.mm.yy");
    // if (value != null && value[this.getField().getField()] != null) {
    //     let dateParams = value[this.getField().getField()].replace('/', '.').split(".");
    //     let val = value == null ? null : new Date(dateParams[2], dateParams[1] - 1, dateParams[0].substring(0, 2));
    //     if (val != null) {

    //         this.#datePicker.datepicker("setDate", val);
    //     }
    // }

    // let div = h.div("date").add(el, dom.errorPlate(this.getField().getField())).appendTo(row);

    // this.markElementsRequired({el, div})
    // this.showLabel(div);

    return row;
  }

  read(field) {
    let date = this.#datePicker.datepicker("getDate");
    if (date == null) {
      return null;
    }
    let month = date.getMonth() + 1;
    if (month < 10) {
      month = "0" + month;
    }
    let day = date.getDate();
    if (day < 10) {
      day = "0" + day;
    }
    return day + "." + month + "." + date.getFullYear();
    // return this.#datepicker.getValue().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"))
  }

  setValue(value) {
    this.#datePicker.val(value);
  }
}

class ObjectChooser extends FieldResolver {
  valueHolder;
  #dataPicker;

  constructor(field) {
    super(field);
  }

  create(value) {
    let row = h.div("row");

    let div = h.div("chooser").appendTo(row);

    this.valueHolder = h
      .input(dom.HIDDEN)
      .id(this.getField().getField())
      .attr("data-type", this.getSetting("searchName"))
      .appendTo(div);

    if (!utils.isEmpty(value)) {
      this.valueHolder.text(value[this.getField().getField()].key, false);
    }

    this.markElementsRequired({ div });

    this.showLabel(row);

    //whereToDraw, data ,  hiddenElement, dataType, header

    //on add form we have a different key
    this.#dataPicker = new MagicRemoteChooser(
      div,
      value == null ? [] : value[this.getField().getField()],
      this.valueHolder,
      this.getSetting("remoteType"),
      this.getField()
    );

    return row;
  }

  read(field) {
    return this.valueHolder.val();
  }

  //
  setValue(value) {}
}

class InputField extends FieldResolver {
  #holder;

  constructor(field) {
    super(field);
  }

  read() {
    if (this.getIsId()) {
      return parseInt(this.#holder.val());
    }
    return this.#holder.val();
  }

  setValue(el, value) {
    if (el == null) {
      el = this.html();
    }
    if (value != null) {
      el.value = value;
    }
  }

  html() {
    return this.#holder;
  }

  createWebObject() {
    return h.input(this.getField().getType()).id(this.getField().getField());
  }

  create(value) {
    let div = this.createContainer();

    let el = this.createWebObject();
    this.#holder = el;

    if (this.toShowPlaceHolder()) {
      el.placeHolder(this.getPlaceHolder(), false);
    }

    this.markElementsRequired(el, div);

    this.setValue(el, value);

    if (value != null) {
      el.text(value[this.getField().getField()], false);
    }

    this.showLabel(div);
    this.addElementToInput(div, el);

    dom.errorPlate(this.getField().getField()).appendTo(div);

    div.clIf(utils.e(this.getField().getType(), dom.HIDDEN), dom.HIDDEN);

    return div;
  }

  addElementToInput(div, el) {
    h.from(div).add(el);
  }
}

class PasswordField extends InputField {
  constructor(field) {
    super(field);
  }

  createWebObject() {
    return h.input("password").noCache().id(this.getField().getField());
  }
}

class NumberField extends InputField {
  #hidden;

  constructor(field, hidden) {
    super(field);
    this.#hidden = hidden;
  }

  read() {
    return parseInt(super.read());
  }

  createWebObject() {
    return super.createWebObject().hideIf(this.#hidden);
  }
}

class TextArea extends InputField {
  #el;

  constructor(field) {
    super(field);
  }

  createWebObject() {
    return (this.#el = h.tag("textarea").noCache().id(new Date().getTime()));
  }

  afterAdd() {
    this.editor = utils.initMCE(this.#el);
  }

  read() {
    try {
      return this.editor.getContents();
    } catch (e) {
      return super.read();
    }
  }

  setValue(el, value) {
    super.setValue(el, value);
  }
}

class CheckBox extends InputField {
  constructor(field) {
    super(field);
  }

  create(value) {
    let chk = super.create(value);
    chk.get().childNodes[1].onchange = function () {
      if (chk.childNodes[1].getAttribute("checked") != undefined) {
        chk.childNodes[1].removeAttribute(dom.CHECKED);
      } else {
        chk.childNodes[1].setAttribute(dom.CHECKED, dom.CHECKED);
      }
    };
    return chk;
  }

  createContainer() {
    return super.createContainer().cl("checkbox").cl("field_container");
  }

  addElementToInput(div, el) {
    el.prependTo(div);
  }

  showLabel(div) {
    if (this.toShowLabel()) {
      this.createLabelComponent()
        .cl("label")
        .cl("text_label")
        .appendTo(div)
        .clIf(this.isRequired(), "required");
      div.addIf(this.isRequired(), h.span("required_cell", "*", false));
    }
  }

  setValue(el, val) {
    if (val == null) {
      return;
    }
    let value = val[this.getField().getField()];
    if (el == null) {
      el = this.html();
    }
    if (value == null) {
      value = this.getSetting("checked");
    }

    el.attrIf(value, dom.CHECKED, dom.CHECKED);
  }

  createLabelComponent() {
    return super.createLabelComponent().cl("checkbox");
  }

  read() {
    return this.html().has(dom.CHECKED);
  }
}
