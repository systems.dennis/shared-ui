/**
 * Flaw sdk is a small framework to work with flaw
 * You might need:
 * http://dennis.systems/js/magic.*.js
 * http://dennis.systems/js/html.js
 * http://dennis.systems/js/userdata.js
 *
 *    * means all files like this in directory.
 *    directory is safe to explore
 *
 */







class Issue {
  static ISSUE_PARAM = 'id';
  static getFromPage(callBack) {
      let issueNumber = MagicPage.getPageParam(Issue.ISSUE_PARAM);
      let path = Environment.dataApi + "/api/v2/flaw/issue/byId/" + issueNumber;
      Executor.runGet(path, callBack);
  }

  static getLinkedIssues(callBack) {
      let issueNumber = MagicPage.getPageParam(Issue.ISSUE_PARAM);
      let path = Environment.dataApi + "/api/v2/flaw/issue_to_issue_link/by_parent/" + issueNumber;
      Executor.runGet(path, callBack);
  }

  static getLinkedIssuesWithId(callBack, id) {
      let path = Environment.dataApi + "/api/v2/flaw/issue_to_issue_link/by_parent/" + id;
      Executor.runGet(path, callBack);
  }

  static getStatus(callBack, id){
      let path = Environment.dataApi + '/api/v1/search/type/toString/issue_status/' + id ;
      Executor.runGet(path, callBack, true, Executor.HEADERS_PLAIN);
  }

  static deletLinkedIssues(callBack, id) {
      let issueNumber = MagicPage.getPageParam(Issue.ISSUE_PARAM);
      let path = Environment.dataApi + "/api/v2/flaw/issue_to_issue_link" + '/delete/' + id;
      log.trace(path);
      let method = 'DELETE'
      Executor.run(path, method, callBack);
  }

  static showDescription(data) {
      log.debug(exc);
  
      if (data["description"] == null || data["description"] == "") {
        return h.span("empty_desc_span", data["name"]);
      }
      let span = h.span("show_description", MagicPage.translate("pages.global.list.flaw.show_description")
      );
      span.style.cursor = "pointer";
      span.onclick = function () {
        let item = h.div("modal");
        let descriptonValue = ['name', 'creator', 'group', 'description', 'performer','project','sprint', 'status','tester',  'icon'];
  
        for(key in data){
          if (descriptonValue.indexOf(key) != -1){
            let nameBag = h.div('show_descr_name')
            let spanNameTitle = h.tag('span').cl('show_descr_title').text(key);
            let spanName = h.tag('span').cl('show_descr_name')
        
            if(data[key] instanceof Object){
              spanName.text(data[key]['value'], false);
            }else if(key == 'icon' && data[key]){
              h.img(data[key]).appendTo(spanName);
            }else{
              spanName.text(data[key], false);
            }
            nameBag.appendChild(spanNameTitle);
            nameBag.appendChild(spanName);
            spanName.get().innerHTML && nameBag.appendTo(item);
          }
        }
  
        let linkedIssue = html.div("linked_issue")
        item.get().appendChild(linkedIssue);
        Issue.getLinkedIssuesWithId(function (data) {
            let res = '';
            for (let i = 0; i < data.length; i++) {
                let linked = MagicView.transform(null, data[i]['linked'], null, "flaw/issue", "object_chooser", true);
                let type = MagicView.transform(null, data[i]['linkType'], null, "flaw/issue_link_type", "object_chooser", true);
                // let close = `<img src = "/ROOT//images/rm.png" onclick = remove(${data[i].id})>`;
                // let edit = `<img src = "/ROOT//images/edit.png" onclick = editItem(${data[i].id})>`;
                res += `<div class='link_row' id ='${data[i].id}'> ${type}  ${linked} </div>`;
            }
    
            linkedIssue.innerHTML = res;
            
        },data.id )
  
        
  
        h.img("close.png.png")
          .wh(24)
          .appendTo(item)
          .click(function () {
            item.get().remove();
          });
  
        document.body.appendChild(item.get());
      };
      return h.from(span);
    }

  static promote(id = null, callBack){
      let issueNumber  =  id==null ? MagicPage.getPageParam(Issue.ISSUE_PARAM) : id;
      let path = Environment.dataApi + "/api/v2/flaw/issue/promote/" + issueNumber;
      Executor.runPost(path, callBack);
  }

  static demote(id = null, callBack){
      let issueNumber  =  id==null ? MagicPage.getPageParam(Issue.ISSUE_PARAM) : id;
      let path = Environment.dataApi + "/api/v2/flaw/issue/demote/" + issueNumber;
      log.debug(Environment.dataApi);
      Executor.runPost(path, callBack);
  }

  static showNotification(callBack){
      let path = Environment.dataApi + "/api/v2/flaw/user_notification/my";
      Executor.runGet(path, callBack);
  }

  static deleteNotification(callBack, id){
      let path = Environment.dataApi + `/api/v2/flaw/user_notification/${id}/read?isRead=true`;
      log.debug(path);
      Executor.runPost(path, callBack);
  }

  static changeStatus(callBack, elementId, payload){
      let path = Environment.dataApi + `/api/v2/flaw/issue/edit_field/id/${elementId}`;
      Executor.runPutWithPayload(path, callBack, payload)
    }
}