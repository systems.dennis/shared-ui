class Root {
    static getRoots(min = false) {
        let toLoad = [];
        toLoad.push("js/jquery.js");
        toLoad.push("js/jquery-ui.js");
        toLoad.push("js/userdata.js");
        toLoad.push("js/magic/magic.dom.v1.js");
        toLoad.push("js/magic/magic.utils.v1.js");
        toLoad.push("js/magic/magic.sort.v1.js");
        toLoad.push("js/magic/magic.drag.v1.js");
        toLoad.push("js/magic/magic.ordering.v1.js");
        toLoad.push("js/magic/magic.pagination.v1.js");
        toLoad.push("js/magic/magic.request.v1.js");
        toLoad.push("js/magic/magic.search.v1.js");
        toLoad.push("js/magic/magic.html.v1.js");
        toLoad.push("js/magic/magic.date_chooser.v1.js")
        toLoad.push("js/magic/magic.table.v2.js");
        toLoad.push("js/magic/magic.prompt.v1.js");
        toLoad.push("js/magic/magic.tabs.v1.js")
        toLoad.push("js/magic/magic.bar.v1.js");
        toLoad.push("js/magic/magic.remote_chooser.v2.js");
        toLoad.push("js/magic/magic.executor.v1.js");
        toLoad.push("js/magic/magic.page.v1.js");
        toLoad.push("js/magic/magic.favorite.v1.js");
        toLoad.push("js/suneditor.min.js");
        toLoad.push("js/const.js");
        toLoad.push("js/toast.js");
        toLoad.push("js/loading.js");
        toLoad.push("js/chooser/file_chooser.js");
        toLoad.push("js/magic.js");
        toLoad.push("js/magic.fields.js");
        toLoad.push("js/magic/magic.list.v2.js");
        toLoad.push("js/magic_view.js");
        toLoad.push("js/chooser/remote_chooser.js");
        toLoad.push("js/qrcode.js");
        return toLoad;
    }
}