class Root {
    static getRoots(min = false) {
        let toLoad = [];
        toLoad.push("magic/magic.dom.v2.js");
        toLoad.push("magic/magic.html.v2.js");
        toLoad.push("userdata.js");
        toLoad.push("magic/magic.event.v1.js");
        toLoad.push("magic/magic.files.v1.js")

        toLoad.push("magic/magic.utils.v1.js");
        toLoad.push("magic/magic.sort.v1.js");
        toLoad.push("magic/magic.drag.v1.js");
        toLoad.push("magic/magic.ordering.v1.js");
        toLoad.push("magic/magic.pagination.v1.js");
        toLoad.push("magic/magic.request.v1.js");
        toLoad.push("magic/magic.search.v2.js");
        toLoad.push("magic/magic.context.menu.v1.js");
        toLoad.push("magic/magic.date_constants.v1.js");
        toLoad.push("magic/magic.date_chooser.v1.js");
        toLoad.push("magic/magic.personal.settings.js");
     
        toLoad.push("magic/magic.table.v3.js");
        toLoad.push("magic/magic.prompt.v2.js");
        toLoad.push("magic/magic.tabs.v2.js")
        toLoad.push("magic/magic.cache.v1.js")
        toLoad.push("magic/magic.bar.v1.js");
        toLoad.push("magic/magic.remote_chooser.v3.js");
        toLoad.push("magic/magic.executor.v2.js");
        toLoad.push("magic/magic.page.v1.js");
        toLoad.push("magic/magic.favorite.v1.js");
        toLoad.push("suneditor.min.js");
        toLoad.push("const.js");
        toLoad.push("loading.js");
    
        toLoad.push("chooser/file_chooser.js");
        toLoad.push("magic/magic.form.v1.js");
        toLoad.push("magic/magic.fields.v1.js");
        toLoad.push("magic/magic.switchbox.js");
        toLoad.push("magic/magic.list.v3.js");

        toLoad.push("magic/magic.view.v1.js");
        toLoad.push("magic/magic.true_false_selector.v1.js");
        toLoad.push("magic/magic.authorization.v1.js");
        toLoad.push("magic/magic.jtree.v2.js")

       
  
      
       
        return toLoad;
    }
}
