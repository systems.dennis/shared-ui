class Environment {
  static self = "http://localhost/ROOT/";
  static authApi = "https://auth.dennis.systems";
  static dataApi = "https://demo-api.flaw.space";
  static authScope = "test_flaw";
  static scriptServer = "http://localhost/ROOT/js/";
  static defaultLang = "ru";
  static version = "1.97";
  static FORCE_PROCESSOR = false;
  static serverID = "serj.localhost";
  static defaultIconSize = 32;
  static fileStorageServer = "https://files.dennis.systems/";
  static fileStorageApi = Environment.fileStorageServer + "api/v2/files/";
  static defaultTemplate = "themes/white/";
  static PROCESSOR_URL = Environment.fileStorageServer + "css/get/";
  static PROCESSOR_UPLOAD_URL = Environment.fileStorageServer + "css/compile/";
  static PROCESSOR_EXISTS_URL = Environment.fileStorageServer + "css/exists/";
  static LOG_LEVEL = -4;
  static scripts = [
    "js-local/flaw-sdk-sprint.js",
    "js-local/flaw-sdk-group.js",
    "js-local/flaw-sdk-rhythm.js",
    "js-local/flaw-sdk-issue.js",
    "js-local/flaw-notice.js",
    "js-local/flaw-edit-field.js",
    "js-local/plugins/git_support.js",
    "js-local/flaw-issue-flow-stages.js",
    "js-local/plugins/qr.js"
  ];
  static styles = ["css/toast.css", "css/issue.css"];
  static themes = ["dark", "white"];
  static formButtons = {'close': true, 'close_and_save':true, 'save':false};

  static isNotRegisterLoginOrReset() {
    let path = location.protocol + "//" + location.host + location.pathname;
    return (
      path.indexOf("/client_login") == -1 &&
      path.indexOf("/restore") == -1 &&
      path.indexOf("/restore") == -1 &&
      path.indexOf("/register") == -1 &&
      path.indexOf("/verify/verification/verify_scope/") == -1 &&
      path.indexOf("/invitation/info") == -1
    );
  }

  static addFiles() {
    if (Environment.isNotRegisterLoginOrReset()) {
      Environment.styles.push(
        "css/suneditor.min.css",
        "css/menu/menu_28102022.css"
      )

      Environment.scripts.push(
        "js-local/flaw-connect-chooser.js",
        "js-local/common.js",
        "js-local/plugins/share.js",
        "js-local/menu.js"
      );
    } else {
      Environment.styles.push("css/menu/login_out_reg_pw_form.css");
    }
  }

  static initLoader(path, onLoad) {
    const head = document.getElementsByTagName("html")[0];

    const script = document.createElement("script");
    script.async = true;
    script.src = Environment.scriptServer + path;
    script.type = "text/javascript";

    script.onload = onLoad;

    head.prepend(script);
  }
}

Environment.addFiles();

Environment.initLoader("env-uploader.js", () =>
  EnvironmentUploader.load(() => {
    try {
      onPageLoad();
    } catch (e) {}
    if (_.isNull(cache.get(Group.GROUP_SELECTED))) {
      Group.drawGroupSelection();
    }
    ____USER.initPage();
  })
);
