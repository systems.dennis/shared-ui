class Environment {
    static self = "/ROOT/";
    static authApi = "http://dennis.systems:7777";

    static dataApi = "http://dennis.systems:888";

    static scriptServer = Environment.self;
    static defaultLang = 'en';


    static fileStorageApi = "http://dennis.systems:4444/api/v2/files";


    static loadBasics() {
        let toLoad = [];


        if (this.isNotRegisterLoginOrReset()) {
            this.#includeCSS("css/multi.css")
            this.#includeCSS("css/jquery-ui.css")
            this.#includeCSS("css/menu/menu_28102022.css?v9")
        }
        this.#includeCSS("css/toast.css")

        toLoad.push("js/jquery.js")
        toLoad.push("js/tinymce.min.js")
        toLoad.push("js/jquery-ui.js")
        toLoad.push("js/userdata.js")
        toLoad.push("js/html.js");
        toLoad.push("js/magic/magic.sort.v1.js");
        toLoad.push("js/magic/magic.drag.v1.js");
        toLoad.push("js/magic/magic.ordering.v1.js");
        toLoad.push("js/magic/magic.pagination.v1.js");
        toLoad.push("js/magic/magic.request.v1.js");
        toLoad.push("js/magic/magic.search.v1.js");
        toLoad.push("js/magic/magic.html.v1.js");
        toLoad.push("js/magic/magic.table.v1.js");
        toLoad.push("js/magic/magic.prompt.v1.js");
        toLoad.push("js/magic/magic.bar.v1.js");
        toLoad.push("js/const.js");
        toLoad.push("js/toast.js");
        toLoad.push("js/loading.js");
        toLoad.push("js/chooser/file_chooser.js");
        toLoad.push("js/magic.js");
        toLoad.push("js/magic.fields.js");
        toLoad.push("js/magic_list.js");
        toLoad.push("js/magic/magic.list.v1.js");
        toLoad.push("js/magic_view.js");
        toLoad.push("js/chooser/remote_chooser.js");
        toLoad.push("js/qrcode.js");


        this.#includeJS(toLoad, 0, function (){ ____USER.verifyPage(Environment.isNotRegisterLoginOrReset());});



        window.onload = function (){
            ____USER.initPage();

            if (onPageLoad != undefined){
                onPageLoad();
            }

        }


    }

    static isNotRegisterLoginOrReset() {
        return document.location.href.indexOf("/client_login") == -1 && document.location.href.indexOf("/restore") == -1 && document.location.href.indexOf("/register") == -1
    }

    static #includeJS(toLoad, i, onEnd) {

        if (i >= toLoad.length - 1) {
            onEnd();
            return;
        }

        try {
            const head = document.getElementsByTagName('html')[0];

            const script = document.createElement('script');
            script.async = true;
            script.defer = true;
            script.setAttribute("defer", true)
            script.src = Environment.scriptServer + toLoad[i];
            script.type = 'text/javascript';

            script.onload = function () {

                Environment.#includeJS(toLoad, ++i, onEnd);
            }

            head.prepend(script)
        } catch (e){
           log.error({e});
        }
    }

    static includeDirect(what, onLoad){
        const head = document.getElementsByTagName('html')[0];

        const script = document.createElement('script');
        script.async = true;
        script.defer = true;
        script.setAttribute("defer", true)
        script.src = Environment.self + what;
        script.type = 'text/javascript';
        script.onload  = onLoad;
        head.prepend(script)
    }

    static #includeCSS(filename) {

        var head = document.getElementsByTagName('title');
        if (head.length == 0){
            head = document.getElementsByTagName('html')
        }
        head = head[0];

        var script = document.createElement('link');
        script.href = Environment.self + filename;
        script.rel = 'stylesheet';


        head.appendChild(script)
    }

}

Environment.loadBasics();