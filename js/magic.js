class Refreshable {
    refresh() {
    }
}

class fetcher {
    /**
     * Where to put the form. data-id should be filled up with the object id;
     */
    #id;

    /**
     * a type of the method ['GET, POST, DELETE, PATCH']
     */
    #type;

    /**
     * Execution base path
     */
    #path;
    /**
     * function to deal with custom types
     */
    #drawType;

    #edit;

    #refreshable;

    #frm;

    #holder;

    constructor(id, path, type, drawType = null, refreshable) {
        this.#id = id;
        this.#type = type;
        if (path.indexOf("http") == 0) {
            this.#path = path;
        } else {
            this.#path = Environment.dataApi + path;
        }
        this.#drawType = drawType;
        this.#refreshable = refreshable;
        this.#holder = h.from(get(this.#id));
    }

    fetch() {
        let _this = this;

        let dId = document.getElementById(this.#id).getAttribute("data-id");
        if (dId != null && dId.trim() != "") {
            dId = "?id=" + dId;
        } else {
            dId = "";
        }

        Executor.runGet(
            this.#path + "/root/fetch/" + this.#type + dId,
            function (data) {
                _this.build(data);
            }
        );
    }

    build(res) {
        this.#frm = new form(
            this,
            this.#id,
            res,
            this.#edit,
            null,
            this.#refreshable
        );

        for (let i = 0; i < res.fieldList.length; i++) {
            this.#frm.addField(res.fieldList[i]);
        }

        this.#frm.draw();
    }

    loadId(id) {
        this.#holder.setData("id", id);
        this.#edit = true;
        this.fetch();
        this.#holder.rcl("hidden");
    }

    new() {
        this.#holder.setData("id", "");
        this.#edit = false;
        this.fetch();
        this.open();
        return this;
    }

    open() {
        this.#holder.rcl("hidden");
        this.#holder.cl("open-form");

        document.body.classList.add("form-exists");
    }

    preselect(field, value) {
        let _this = this;
        dom.waitForItem(
            function () {
                return _this.#frm;
            },
            function () {
                _this.#frm.select(field, value);
            }
        );

        return this;
    }
}

class form {
    #where;
    #fields = [];
    #action;
    #method;
    #showTitle;
    #title;
    #commitMessage;
    #value;
    #drawType;
    #parent;
    #edit;
    #refreshable;
    #executing = false;

    #holder;

    constructor(parent, where, res, edit, drawType, refreshable) {
        this.#parent = parent;
        this.#where = where;
        this.#action = res.action;
        this.#method = res.method;
        this.#commitMessage = res.commitText;
        this.#showTitle = res.showTitle;
        this.#title = res.title;
        this.#value = res.value;
        this.#drawType = drawType;
        this.#edit = edit;
        this.#refreshable = refreshable;
        this.#holder = h.from(get(this.#where));
    }

    addField(_field) {
        if (!_field.visible) {
            return;
        }
        this.#fields.push(new field(_field, this.#drawType).init());
    }

    select(field, value) {
        for (let i = 0; i < this.#fields.length; i++) {
            if (this.#fields[i].getField() == field) {
                this.#fields[i].setValue(value);
            }
        }
    }

    draw() {
        let _this = this;
        let box = this.#holder.text(null);
        box.cl('form-window')
        h.img("close.png.png")
            .wh(24)
            .cl("close_inner_form")
            .appendTo(box)
            .click(function (e) {
                _this.hideForm();
            })
            .appendTo(box);

        let formHolder = h.div("form_content").cl("edit-person").appendTo(box);

        if (this.#showTitle) {
            h.div("formTitle")
                .text(this.#edit ? this.#title + "_edit" : this.#title + "_add")
                .appendTo(formHolder);
        }

         this.separator(formHolder);
        // for (let i = 0; i < this.#fields.length; i++) {
        //     formHolder.add(this.#fields[i].draw(this.#value));
        //   }

        h.div("footer").appendTo(box)
            .add(
                h.input("button").text(this.#commitMessage).cl("save_auto_form")
                    .click((x) => {
                        _this.execute(box);
                    }))

            .add(h.input("button").text("global.pages.form.close_and_save")
                .cl("close_and_save_auto_form")
                .click(() => {
                    _this.execute(box, true, this.hideForm);
                    _this.hideForm();
                }))
            .add(h.input("button").text("global.pages.form.close").cl("close_auto_form")
                .click(() => _this.hideForm()))
            .appendTo(box);



        for (let i = 0; i < this.#fields.length; i++) {
            this.#fields[i].afterAdd(this.#value);
        }
    }

    separator(formHolder) {
        let groups = [];
        //collect groups in array
        for (let i = 0; i < this.#fields.length; i++) {
            if (groups.indexOf(this.#fields[i].getData().group) === -1) {
                groups.push(this.#fields[i].getData().group);
            }
        }
        //made divs equael groups
        let components = groups.map(e => h.div('tabs_copmponent_div').setData( 'group', e))

        for (let i = 0; i < this.#fields.length; i++) {
            let compGroup = this.#fields[i].getData().group;
           
              components.forEach((e) => {
                if(e.getData('group') === compGroup ){
                    this.#fields[i].draw(this.#value).appendTo(e)
                }})
              
        }

        let fieldTab = new MagicTab()
        
        for (let k = 0; k < groups.length; k++) {
            fieldTab.addTab(groups[k], components[k])  
        }
        fieldTab.deployTo(formHolder)
    }


    hideForm() {
        this.#holder.text(null);
        this.#holder.rcl("open-form");
        this.#holder.cl("hidden");

        Array.from(document.querySelectorAll(".actions_cell")).forEach((el) => {
            if (!el.classList.contains("hidden")) el.classList.add("hidden");
        });

        Array.from(document.querySelectorAll(".actions_cell_dots")).forEach(
            (el) => {
                if (el.classList.contains("hidden")) el.classList.remove("hidden");
            }
        );

        if (!h.anyByClassExists("open-form")) {
            document.body.classList.remove("form-exists");
        }
    }

    execute(box, toHide = false, hideForm = false) {
        if (this.#executing) {
            $.toast(MagicPage.translate("global.app.already.in_run"));
            return;
        }

        this.#executing = true;
        for (let i = 0; i < this.#fields.length; i++) {
            this.#fields[i].clearErrors();
        }
        let payload = {};
        for (let i = 0; i < this.#fields.length; i++) {
            payload[this.#fields[i].getField()] = this.#fields[i].read();
        }
        let _this = this;

        $.ajax({
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader(
                    "Authorization",
                    "Bearer " + sessionStorage.getItem("___token___")
                );
            },
            _this: _this,
            type: _this.#method,
            url:
                Environment.dataApi +
                (_this.#action.startsWith("/") ? _this.#action : "/" + _this.#action),

            data: JSON.stringify(payload),
            dataType: "json",
            success: function (res) {
                _this.#executing = false;

                if (hideForm) {
                    _this.hideForm();
                }

                try {
                    _this.#refreshable.refresh();
                } catch (e) {
                    log.error(e);
                }
                $.toast(
                    MagicPage.translate("global.object.saved") +
                    res.id +
                    "" +
                    MagicPage.translate("global.thanks")
                );
                if (!toHide) {
                    _this.#parent.loadId(res.id);
                }
            },

            error: function (e, status) {
                _this.#executing = false;
                verifyResponse(e.status);
                let data = e.responseJSON;

                if (data != null) {
                    var iterator = data.keys();

                    // printing index array using the iterator
                    for (let key of iterator) {
                        let errorPlate = document.getElementById("error_" + data[key].key);
                        errorPlate.classList.remove("hidden");
                        errorPlate.innerHTML +=
                            "<span>" + MagicPage.translate(data[key].value) + "</span>";
                        document.getElementById(data[key].key).classList.add("error_state");
                    }
                } else {
                    $.toast(MagicPage.translate("global.fail" + e.responseText));
                }
            },
        });
    }
}

class field {
    #drawType;
    #drawClass;
    #data;

    #field;
    #type;
    #visible;

    constructor(_field, drawType) {
        this.#type = _field.type;
        this.#field = _field.field;
        this.#drawType = drawType;

        this.#data = _field;
    }

    init() {
        if (this.#type == "checkbox") {
            this.#drawClass = new CheckBox(this);
        }
        if (this.#type == "text_area") {
            this.#drawClass = new TextArea(this);
        }
        if (this.#type == "number") {
            this.#drawClass = new NumberField(this, false);
        }

        if (this.#type == "text") {
            this.#drawClass = new InputField(this);
        }

        if (this.#type == "hidden") {
            this.#drawClass = new InputField(this);
        }

        if (this.#type == "password") {
            this.#drawClass = new PasswordField(this);
        }
        if (this.#type == "date") {
            this.#drawClass = new DateElement(this);
        }

        if (this.#type == "drop-down") {
            this.#drawClass = new DropDown(this);
        }

        if (this.#type == "file") {
            this.#drawClass = new FileSelector(this);
        }

        if (this.#type == "object_chooser") {
            this.#drawClass = new ObjectChooser(this);
        }

        return this;
    }

    getData() {
        return this.#data;
    }

    getType() {
        return this.#type;
    }

    getField() {
        return this.#field;
    }

    read() {
        return this.#drawClass.read();
    }

    clearErrors() {
        this.#drawClass.clearErrors();
    }

    setValue(value) {
        this.#drawClass.setValue(value);
    }

    draw(value) {
        if (this.#drawClass == null) {
            $.toast(
                "Field cannot be created: " + this.getField() + "#" + this.getType()
            );
        }

        if (this.#drawType != null) {
            let res = this.#drawType(this, value);

            if (res != null) {
                return res;
            }
        }

        try {
            let res = this.#drawClass.create(value);

            if (res == null) {
                res = this.#drawType(this);
            }

            if (res != null) {
                return res;
            }
        } catch (e) {
            console.error(e)
            return h.span("span").text("ERROR " + this.#field);
        }

        return h.span("span").text("ERROR " + this.#field);
    }

    afterAdd() {
        this.#drawClass.afterAdd();
    }
}

function get(id) {
    return document.getElementById(id);
}
