class MagicPage {
  
      static ____UNTRANSLATED = [];
  
      static list;
  
      static modalWindows = [];
      static #openedModalWindow = [];
      static modalKeyListeners  = [];
  
  
      static hideWindow(elem){
          _.each(MagicPage.#openedModalWindow,(e, i)=>{
                  if (_.e(e.modalName, elem.getData(dom.MODAL) )){
                      if(_.e(e.removeType, dom.REMOVE_TYPE_REMOVE )){
                          e.div.remove();
                          MagicPage.#openedModalWindow.splice(i, 1);
                      }else if(_.e(e.removeType, dom.REMOVE_TYPE_HIDE)){
                          e.div.hide();
                          MagicPage.#openedModalWindow.splice(i, 1);
                      }
                      
                  }
              })
      }
  
      static showWindow(elem){
          _.each(MagicPage.modalWindows, (e) =>{
                  if (_.e(e.modalName, elem.getData(dom.MODAL))){
                      MagicPage.#openedModalWindow.unshift(e)
                      e.div.show()
                  }
              })
      }
  
  
      static setModal(modalName, modalDiv, removeType){
          if(_.e(removeType, dom.REMOVE_TYPE_HIDE)){
              MagicPage.modalWindows.push({modalName: modalName, div: modalDiv, removeType:removeType})
              MagicPage.#openedModalWindow.unshift({modalName: modalName, div: modalDiv, removeType:removeType})
          }
          
          if(_.e(removeType, dom.REMOVE_TYPE_REMOVE)){
              MagicPage.#openedModalWindow.unshift({modalName: modalName, div: modalDiv, removeType:removeType})
          }
      }
  
      static focusModal(focusedModalName){
          _.each(MagicPage.#openedModalWindow, (e,i)=>{
              if(_.e(e.modalName, focusedModalName)){
                  MagicPage.#openedModalWindow.splice(0, 0, MagicPage.#openedModalWindow.splice(i, 1)[0])
              } 
          })
      }
  
      static keyDownListner() {
          h.from(document).handleEvent('keydown', (e)=>{
              if(_.e(e.key, 'Escape')){
                  MagicPage.closeCurrantModal()
              }
              if(!_.isEmpty(MagicPage.modalKeyListeners)) {
                _.each(this.modalKeyListeners, func => func(e))
              }         
          })
      }
  
    
  
  
      static closeCurrantModal(){
          if(_.e(MagicPage.#openedModalWindow[0]?.removeType, dom.REMOVE_TYPE_HIDE)){
              MagicPage.#openedModalWindow[0].div.hide();
              MagicPage.#openedModalWindow.splice(0,1);
          }else if(_.e(MagicPage.#openedModalWindow[0]?.removeType, dom.REMOVE_TYPE_REMOVE)){
              MagicPage.#openedModalWindow[0].div.remove();
              MagicPage.#openedModalWindow.splice(0,1);
          }
      }
  
      static translatePage() {
  
          dom.eachDomElement( (x) => {
              if (x.hasAttribute("data-text")) {
  
                  x.innerHTML = MagicPage.translate(x.getAttribute('data-text'));
  
              }
              if (x.hasAttribute("data-working")){
                  x.onclick = function (){
                      try {
                          dom.toast("global.app.in_work", ToastType.SUCCESS,  true)
                      } catch (ex){
                          console.log("not able to display on work")
                      }
                  }
              }
  
              if (x.hasAttribute('data-value')) {
  
                  x.value = MagicPage.translate(x.getAttribute('data-value'));
  
              }
  
              if (x.hasAttribute("data-placeholder")) {
                  x.placeholder = MagicPage.translate(x.getAttribute('data-placeholder'));
              }
  
              if (x.hasAttribute("data-src")) {
  
                  h.from(x).src(utils.transFormAttributeValue(x.getAttribute('data-src')));
              }
              if (x.hasAttribute("data-href")) {
                  x.href = Environment.self + x.getAttribute('data-href');
              }
  
              if (x.classList.contains("menu-item")){
                  if (window.location.pathname == x.href ||  window.location.pathname.endsWith(x.getAttribute("data-href"))){
                      x.classList.add("selected-menu-item");
                  }
              }
  
              if (x.getAttribute("data-use") != null) {
  
                  let box = x.getAttribute("data-use");
                  let path = x.getAttribute("data-path");
                  let attribute = x.getAttribute("data-attribute");
                  let dataFetcher = x.getAttribute("data-fetcher");
  
                  if (dataFetcher == null) {
                      let magicListOptions = new MagicListOptions(path, dataFetcher)
                      new MagicList2(path, magicListOptions).load(box == null ? x.id : box);
                  } else {
  
                      if (attribute != null) {
                          dataFetcher += "?" + attribute + "=" + MagicPage.getPageParam(attribute);
                      }
                      let magicListOptions = new MagicListOptions(path, dataFetcher)
                      new MagicList2(path, magicListOptions).load(box == null ? x.id : box);
                  }
  
              }
  
          })
      }
  
      static doSingleInclude(x) {
          var xhttp = new XMLHttpRequest();
          var file = x.getAttribute("data-include");
          xhttp.onreadystatechange = function () {
              if (this.readyState == 4) {
                  if (this.status == 200) {
                      x.innerHTML = this.responseText;
                  }
                  if (this.status == 404) {
                      x.innerHTML = "Page not found.";
                  }
                  /* Remove the attribute, and call this function once more: */
                  x.removeAttribute("data-include")
              }
          }
          xhttp.open("GET", file, true);
          xhttp.send();
      }
  
      static changeSelect(x, parent = null, tagProcessor = null){
  
          if (tagProcessor != null){
              tagProcessor.executeTag(x);
              return;
          }
          x.style.opacity = 0;
          if (parent == null) parent = x.parentElement;
          let selectBlock = h.div("select-custom").appendTo(parent);
          let innerSelect = h.div("select").appendTo(selectBlock);
  
          let selectedDiv = h.div("select__icon");
          let current = h.div("select__current");
          h.div("select__header").appendTo(innerSelect)
              .add(current.text(dom.findSelectedValue(x), false))
              .add(selectedDiv).appendTo(innerSelect);
  
          let body = h.div("select__body").appendTo(innerSelect);
  
          _.each(x.options, (opt)=>{
              body.add(h.div("select__item")
                  .attr("data-value",  opt.value)
                  .text(opt.label, false)
                  .click(function (el, event){
                      event.preventDefault();
                      _.each(x.options, (option, i)=> {
                          if (_e(option.value, el.getAttribute('data-value'))) {
                              x.selectedIndex = i;
                          };
                      });
                      x.dispatchEvent(new Event("change"));
                      current.text(x.options[x.selectedIndex].label, false);
                      x.classList.toggle("is-active");
                      selectedDiv.get().classList.toggle("rotated");
                      innerSelect.cl('___bug');
                      innerSelect.get().classList.toggle("is-active");
  
              }))
          })
  
          innerSelect.click( function (x, e){
              innerSelect.get().classList.toggle("is-active");
              if (x.classList.contains('___bug')) {
                  x.classList.remove('___bug');
                  return;
              }
  
              selectedDiv.get().classList.toggle("rotated");
          });
  
  
  
      }
  
      static include(tagProcessor=null) {
          dom.eachDomElement( (x) => {
  
              if (tagProcessor != null){
                  if (tagProcessor.executeTag(x) != undefined) {
                      return;
                  }
              }
              //Here is now common behavior
  
  
              let toCache = false;
  
              if (x.hasAttribute("data-include")) {
                  let value ;
                  let cacheItem = x.getAttribute("data-cache")
                  if (utils.notNull(cacheItem) ){
                      value = localStorage.getItem("cache_" + cacheItem);
  
                      if (value != null) {
                          MagicPage.insert(x, value);
                          return;
                      } else {
                          toCache = true;
                      }
  
                  }
                  var xhttp = new XMLHttpRequest();
                  var file = Environment.self + x.getAttribute("data-include");
                  xhttp.onreadystatechange = function () {
                      if (this.readyState == 4) {
                          if (this.status == 200) {
  
                            MagicPage.insert(x,  this.responseText);
                          }
                          if (this.status == 404) {
                              x.innerHTML = "Page not found.";
                              log.trace('error replacing: ' + {x});
                          }
                          log.trace('finished: ' + {x});
                          /* Remove the attribute, and call this function once more: */
                          x.removeAttribute("data-include")
  
                          if (toCache){
                              localStorage.setItem("cache_" + cacheItem, this.responseText);
                          }
                      }
                  }
                  xhttp.open("GET", file, false);
                  xhttp.send();
              }
          });
      }
  
      static insert(x, value){
          x.innerHTML = value;
           log.trace('replaced: ' + {x});
  
      }
  
      static transFormAttributeValue(text, def = null){
          if (text.startsWith("!{")){
              text = text.replaceAll("!{", "").replaceAll("}", "");
  
              if (text.indexOf(":") > 0){
                  text = text.split(":");
  
                  def = text [1];
                  text = text[0];
              }
  
              if (def == null) return   sessionStorage.getItem("___" + text);
  
              return utils.isEmpty(sessionStorage.getItem("___" + text)) ?  def : sessionStorage.getItem("___" + text);
          }
          return  text;
      }
  
      static translateFromGlobal(what){
  
          let lang = cache.get('___user_lang___')
  
          if( typeof GLOBAL_TRANSLATIONS !== 'undefined' && _.isTrue(GLOBAL_TRANSLATIONS[lang])){
              let translation = GLOBAL_TRANSLATIONS[lang].find(elem => elem[what])
              if(_.isTrue(translation)){
                  return translation[what]
              }   
          }
  
          return false
      }
  
      static translate(what, placeholders = []) {
        if (what.startsWith("!{")) {
            return MagicPage.transFormAttributeValue(what);
        }
    
        let fromGlobal = MagicPage.translateFromGlobal(what);
    
        if (_.isTrue(fromGlobal)) {
            return MagicPage.replacePlaceholders(fromGlobal, placeholders);
        } else {
            what = utils.escapeDollar(what);
            let rs = localStorage.getItem("translate_" + what.toLowerCase());
            if (rs != undefined) {
                return MagicPage.replacePlaceholders(rs, placeholders)
            } else {
                if (!MagicPage.____UNTRANSLATED.includes(what)) {
                    MagicPage.____UNTRANSLATED.push(what);
                }
            }
            let whatItems = what.split(".");
            return "[" + whatItems[whatItems.length - 1] + "]"; 
        }
    }

    static replacePlaceholders(string, placeholders) {
        _.each(placeholders, (value, index) => {
            const placeholder = `{${index + 1}}`;
            string = string.replace(placeholder, value);
        })
        return string;
    }
  
  
    static getPageParam(param) {
      let urlParams = new URLSearchParams(window.location.search);
      return urlParams.get(param);
    }

    static addPageParam(param, value) {
        window.history.pushState({}, '', document.location.href.addUrlParam(param, value));
    }

    static deletePageParam(param) {
        window.history.pushState({}, '', document.location.href.deleteUrlParam(param));
    }
      
  
      static changeUserTemplate(path){
        if(path && typeof path === 'string'){
          localStorage.setItem('userTemplate',(path) );
  
        } else if(path && typeof path !== 'string'){
          dom.toast('Path must be String', ToastType.ERROR)
        }  else {
          dom.toast('Path is null!', ToastType.ERROR)
        }
      }
  
      static removeUserTemplate(){
        localStorage.removeItem('userTemplate');
      }
  
     
  }
  
  try {
      MagicPage.keyDownListner()
  } catch (e){
      console.log(e);
  }

  String.prototype.addUrlParam = function (param, paramValue){
      let href = new URL(this);
      href.searchParams.set(param, paramValue);
      return href.toString();
  }

String.prototype.deleteUrlParam = function (param){
    let href = new URL(this);
    href.searchParams.delete(param);
    return href.toString();
}
  
  
