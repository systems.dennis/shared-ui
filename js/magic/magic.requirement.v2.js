class $$ {
    static #___loaded = []

    static requireLocal(magicPath) {
        return $$.require(new Requirement().setIsLocal().setPath((magicPath)))
    }

    static requireMagic(magicPath, version = null) {
        if (!version) {
            version = __versions[magicPath]
        }
        return $$.require(new Requirement().magic(magicPath, version))
    }

    static require(requirement) {
        if ($$.#___loaded.indexOf(requirement.getPath()) > -1) {
            return
        }
        try {
            requirement.setSync().createScript(() => {
                $$.#___loaded.push(requirement.getPath())
                console.log(requirement.getPath() + "... is loaded")
            })
        } catch (ex) {
            console.log("script \n" + requirement.getPath() + "\n cannot be loaded:  \n" + ex)
        }

    }

}

class Requirement {
    static #count = 0;
    static MAGIC_FOLDER = "magic";
    static SCRIPT_TAG = "script";
    static HEAD_TAG = "html";
    #local = false;
    #path;
    #version = null;
    #async = true;
    #folder = null;
    #isMagic = false;
    static #finished = undefined;

    static #firstToLoad = [];
    static #asyncToLoad = []

    static isLoadingFinished() {
        return Requirement.#finished && Requirement.#count === 0;
    }

    finish() {


        this.createScriptFunction()


        Requirement.#finished = true;
        Requirement.showLoading()
    }

    static processed = 0;


    createScriptFunction() {

        if (Environment.developmentMode) {
            _.each(this.#firstToLoad, (script) => {
                this.createScript(script.getFullPath())
            })
        } else {
            let script = document.createElement("script");

            let content = this.getUnifiedContent(this.#firstToLoad)
            script.innerHTML = content;
            script.append(document.body);
        }

    }

    static showLoading() {

        if (Requirement.isLoadingFinished() == true) {
            setTimeout(Requirement.showLoading, 500)
        } else {

            try {
                onPageLoad();
            } catch (e) {
                console.log(e);
            }
            ____USER.initPage();

            setTimeout(() => {
                EnvironmentUploader.endLoading()
            }, 200);

        }
    }

    magic(path, version = "1") {
        this.#folder = Requirement.MAGIC_FOLDER;
        this.#path = Requirement.MAGIC_FOLDER + "." + path;
        this.#version = version;
        this.#isMagic = true;
        return this;
    }

    toQueue() {


        Requirement.#firstToLoad.push(this);

        return this;
    }

    createScript(onloadEvent) {
        let scriptLoaded = false;

        let script = document.createElement("script");
        script.async = false;
        script.defer = false

        let name = this.getPath().split("/").pop();

        let value = this.getContent(this.getPath());

        let strings = value.split("\n");

        let res = "";

        strings.forEach((line)=> {
            line = line.trim();
            if (line.trim().length == 0){
                return;
            }
            if (line.startsWith("$$")){
                //do include
                eval(line) //avoid double run
                return;
            }

            if (line.startsWith("//") || line.startsWith("/*") || line.startsWith("*")|| line.endsWith("**/")){
                //avoiding comments like // /** **/
                return;
            }
            res += line + "\n";


        })

        script.innerHTML =
            res;
        script.id = name

        try {
            document.getElementsByTagName(Requirement.HEAD_TAG)[0].append(script)
        } catch (ex){
            console.log("script \n" + this.getPath() + "\n cannot be loaded:  \n" + ex)
        }
        onloadEvent()


    }

    getContent(path) {
        let xhr = new XMLHttpRequest();
        xhr.open("GET", path, false);
        let text = "// ---> loaded from " + path + "." + "\n";
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                let content = xhr.responseText;
                text += content;
            }
        };

        xhr.send();
        return text

    }

    getUnifiedContent(paths, name='local') {
        let xhr = new XMLHttpRequest();
        xhr.open("POST", path, false);

        let text = "// ---> loaded from " + path + "." + "\n";
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                text = xhr.responseText;
            }
        };

        xhr.send( JSON.stringify({"name" : name, "unifyJS" : paths}));
        return text

    }

    setFolder(folder) {
        this.#folder = folder;
    }

    getPath() {
        if (this.#local) {

            return this.getLocalFolderOrDefault() + this.getFileName();
        } else {

            return this.getRemoteOrDefault() + this.getFileName();
        }
    }

    getFileName() {

        return (this.#folder ? this.#folder + "/" : "")
            + this.#path
            + (this.#version ? (this.#isMagic ? (".v" + this.#version) : "." + this.#version) : "")
            + ".js";

    }


    getLocalFolderOrDefault() {
        return Environment?.localScriptFolder ? Environment?.localScriptFolder : Environment?.self + "js-local/";
    }

    getRemoteOrDefault() {
        return Environment?.scriptServer ? Environment?.scriptServer : "https://js.dennis.systems/";
    }

    setIsLocal() {
        this.#local = true;
        return this;
    }

    setSync() {
        this.#async = false;
        return this;
    }

    setPath(path) {
        this.#path = path;
        return this;
    }

    setVersion(version) {
        this.#version = version;
        return this;
    }

    next(sync = false) {
        if (sync) {
            this.setSync();
        }

        this.toQueue();
        return new Requirement();
    }

}