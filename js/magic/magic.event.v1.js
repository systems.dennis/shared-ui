class DropDownEvent {
    static DD_EVENT_RENDERED = "dd_rendered";
    static DD_EVENT_ITEM_RENDERED = "dd_item_rendered";
    static DD_EVENT_PRE_ITEM_CREATED = "dd_pre_item_created";
    static DD_EVENT_ITEM_CREATED = "dd_item_created";
    static DD_EVENT_ITEM_REMOVED = "dd_item_removed"
    static DD_EVENT_ITEM_SELECTED = "dd_item_selected"
    static DD_EVENT_ITEM_DESELECTED = "dd_item_deselected"
    static DD_EVENT_PRESELECTED_VALUE_RENDERED = 'dd_preselected_value_rendered'
    static DD_EVENT_SEARCH_PERFORMED = "dd_search_performed"
    /**
     * @deprecated as never called in v2
     * @type {string}
     */
    static DD_EVENT_FIRST_RENDER_SELECTED = 'dd_first_render_selected'
    static DD_EVENT_NO_VALUE_SELECTED = 'dd_novalue_selected'


    constructor(eventName, data){
        this.eventName = eventName;
        this.data = data;

    }
}
class MagicChooserEvent{

    static CHOOSER_VALUE_CHANGED = "remote_chooser_value_changed"
    static SEARCH_BEFORE = "remote_chooser_before_search";
    static SEARCH_AFTER = "remote_chooser_after_search";
    static CHOOSER_VALUE_DESTROED = "remote_chooser_value_destroed";

    constructor(eventName, element, elementId, tree){
        this.eventName = eventName;
        this.element = element;
        this.elementId = elementId;

    }
}
class MagicTreeEvent {

    static ADD_ITEM = "add_item"
    static COLLAPSE = "collapse"
    static EXPAND = "expand"
    static REMOVE = "remove"
    static PREBUILD_TEXT = "prebuild_text"
    constructor(eventName, element, elementId, tree){
        this.eventName = eventName;
        this.element = element;
        this.elementId = elementId;
        this.tree = tree;
    }
}
class MagicFormEvent {
    type;
    form;

    field;

    additional;

    static FORM_LOADED = 'form_loaded';
    static FORM_SHOW = 'form_shown';
    static FORM_HIDE = 'form_hidden';
    static FIELD_GENERATED = 'form_generated';
    static FORM_TAB_ADD = 'form_tab_add';
    static FORM_TABS_GENERATED = 'form_tabs_generated';
    static FORM_GENERATED_FIELDS = 'form_generated_fields';
    static FORM_FOOTER_GENERATED = 'form_generated_footer';
    static FORM_INITIATED = 'form_generated_footer';
    static FORM_SAVED = 'form_saved';
    static FORM_ERROR_SAVING = 'form_error_on_save';
    static FORM_TOAST_REQUESTED = "form_toast_requested";
    static FORM_FIELDS_DRAGABLE = "form_fields_dragable";
    static FORM_CONTEXT_MENU_FIELDS_GENERATED = "form_context_menu_fields_generated";
    static FORM_CONTEXT_MENU_START_CREATED = "form_context_menu_start_created";
    static FORM_CONTEXT_MENU_FINISH_CREATED = "form_context_menu_finish_created";
    static FORM_PRELOAD = "form_preload"


    constructor(type, form = null, additional = null, field = null, response = null) {
        this.form = form;
        this.field = field;
        this.type = type;
        this.additional = additional;
        this.response = response;
    }

}

class MagicListEvent{
    static PRELOAD_LIST = "preload_list";
    static PAGINATION_FINISHED = "pagination_finished"
    static PREBUILD_PAGINATION = "prebuild_pagination"
    static PREBUILD_VALUES = "prebuild_values"
    static BUILD_DATA_READY = "build_data_ready"
    static VALUES_HAS_BEEN_REBUILDED = "values_has_been_rebuiled"
    static BUILD_VALUES = "build_values"
    static CLEAR_LIST = 'clear_list'
    static PSEUDO_ITEM_VALIDATION_FAILED = 'pseudo_item_validation_failed'
    static PSEUDO_ITEM_VALIDATION_SUCCESS = 'pseudo_item_validation_success'
    static PSEUDO_ITEM_ADDED = 'pseudo_item_added'
    static PSEUDO_ITEM_SAVED = 'pseudo_item_saved'
    static PSEUDO_ITEM_FAILED_TO_SAVE = 'pseudo_item_saved'
    static PSEUDO_ITEM_RENDERED = 'pseudo_item_rendered'
    static PSEUDO_ITEM_ALREADY_EXISTS = 'pseudo_item_already_exists'

    constructor(type, list = null, additional = null ){
        this.type = type;
        this.list = list;
        this.additional = additional;
    }

}


class MagicMenuEvent {

    static CONTEXT_ITEM_CREATED = "CONTEXT_ITEM_CREATED"
    static PRE_CREATED_ITEM = "PRE_CREATED_ITEM"


    eventType;
    id;
    parent;
    element;

    constructor(eventType, item, id, parent, data = null, element) {
        this.eventType = eventType;
        this.item = item;
        this.id = id;
        this.parent = parent;
        this.data = data;
        this.element = element
    }
}

class DateEvent{
    static CALENDAR_OPEN = "calendar_open";
    static CALENDAR_CLOSE = "calendar_close";
    static VALUE_IS_SET = "calendar_value_is_set";
    static DATE_SELECTED = "date_selected";
  
  
    constructor(eventName, dateChooser, data){
      this.eventName = eventName;
      this.dateChooser = dateChooser;
      this.data = data;
    }
  }


  class SearchEvent {
    static SEARCH_CREATED = "search_created";
    static SEARCH_ADVANCED_OPEN = "search_advanced_open";
    static SEARCH_APPLY_SEARCH_PERFORMED = "apply_search_performed";
    static DROPDOWN_NAME_FIELD_CHANGED = "dropdown_name_field_changed";
    static SEARCH_QUICK_INPUT = "search_quick_input";
    static SEARCH_QUICK_ENTER = "search_quick_enter";
    static SEARCH_QUICK_BACKSPACE = "search_quick_backspace";
    
    constructor(eventName, form, additional) {
        this.form = form;
        this.type = eventName;
        this.additional = additional;
    }
}

class MagicSwitchEvent{

    constructor(eventName, element, value = null){
        this.eventName = eventName;
        this.element = element;
        if(_.notNull(value)){
          this.value = value;  
        }
    }
}


class TableEvent {
    static TABLE_HEADER_CELL_CREATED = 'table.header.cell.created';
    static TABLE_HEADER_CREATION_STARTED = 'table.header.creation.started';
    static TABLE_HEADER_CREATION_FINISHED = 'table.header.creation.finished';
    static TABLE_TOAST_REQUESTED = "table_toast_requested";
    static TABLE_ROW_CREATED = "table_row_created";

    eventType;
    target;
    data;
    header;
    list;

    constructor(eventType, target, data, header, list, additional) {
        this.eventType = eventType;
        this.target = target;
        this.data = data;
        this.header = header;
        this.list = list;
        this.additional = additional
    }
}

class MagicTabEvent {

    static EVENT_TAB_TITLE_GENERATED = "EVENT_TAB_TITLE_GENERATED";

    static EVENT_TAB_CONTENT_GENERATED = 'EVENT_TAB_CONTENT_GENERATED';
    static EVENT_TAB_SELECTED = 'EVENT_TAB_SELECTED';
    static EVENT_TAB_ADDED = 'EVENT_TAB_ADDED';


    tab;

    additional;
    parent;
    eventType;
}

class LoadingEvent {
  static LOADING_START = "EVENT_LAODING_IS_START";
  static LOADING_END = "EVENT_LAODING_IS_END";

  type;

  constructor(type) {
    this.type = type;
  }
}