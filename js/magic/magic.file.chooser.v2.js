class FileUploader {
    #fileSelector;
    #onUpload;
    #where;
    #loadingListener = null;
    #selectedFile;

    constructor(id, onUpload, where, request) {
        this.#where = where;
        this.#onUpload = onUpload;
        this.#fileSelector = document.createElement("input");
        this.#fileSelector.type = "file";
        this.#fileSelector.name = "file";

        let form = h.tag("form").id(id).hide();

        form.add(this.#fileSelector);

        if (this.#where) {
            form.appendTo(this.#where);
        } else {
            form.appendToBody();
        }

        let select = this;
        this.#fileSelector.onchange = function () {
            select.constructResult(request);
        }
    }

    setLoadingListener(func) {
        this.#loadingListener = func;
        return this;
    }

    getFileSelector() {
        return this.#fileSelector;
    }

    fireEvent(type) {
        if (this.#loadingListener) {
            this.#loadingListener(new LoadingEvent(type));
        }
    }

    clean() {
        this.#fileSelector.value = '';
        this.#selectedFile = null;
    }

    constructResult(request) {
        this.fireEvent(LoadingEvent.LOADING_START);
        if (FileReader) {
            let reader = new FileReader();
            let me = this;

            try {
                reader.readAsDataURL(this.#fileSelector.files[0]);
                reader.onload = function (e) {
                    me.upload(request);
                };

            } catch (Ex) {
                dom.toast("cannot display file")
                me.upload(request);

            }
        } else {
            dom.toast("Browser doesn't support reading file.", false)   // Not supported
        }
    }


    upload(request, file = null) {

        if(_.isNull(file)) {
            file = this.#fileSelector.files[0]
        }

        request.presetHeaders(false)
            .method(ServerRequest.POST_METHOD)
            .sendFile(file, (data) => {
                this.fireEvent(LoadingEvent.LOADING_END);
                this.#onUpload( data.downloadUrl,
                    data.fileName, data.fileType, data);

            });
    }

    openSelector() {
        this.#fileSelector.click();
    }
}

class FileRequest extends ServerRequest {
    #authorization
    constructor(authorization, path = Environment.fileStorageServer) {
        super()
        this.#authorization = authorization
        this.root(path).scope(this.getAuthorization().getScope()).entry('files').subEntry('upload').request('true')
    }

    upload(file, onSuccess) {
        this.scope(this.getAuthorization().getScope())
        this.presetHeaders(false)
        Executor.postFile(this.entry('files').subEntry('upload').request('true').getFullPath(), onSuccess, file, (e) => {}, true, this.getHeaders())
    }

    getAuthorization() {
        return this.#authorization
    }

}
