/**
 * Concept of the Cache options is to use Listable (methods (api) that return list of data to save
 * Ids instead of whole objects
 *
 * When item is deleted, IDS a reduced by this id, When item is called By id  - key value map fills
 * with the value
 *
 * Same when list is loaded, ids are collected to the Key/Value map
 *
 * When cached data is loaded then, it has IDS instead of oll objects, so to fetch data the
 * id->value map is used
 *
 *
 * Cache options are used to generate cache keys, or to fetch id, or to fetch data from the listable api
 *
 * example:
 *
 * someApi/getItems/ will return an object of type {
 *     time :
 *     items: [
 *         {...}
 *     ]
 * }
 * then the fetchData  method of the CacheOptions should be:
 *
 *  {
 *      return data[items]
 *  }
 *
 * someApi/getItems/ returns a data as it is:
 *
 *  method of the CacheOptions should be:
 *
 *  {
 *      return data
 *  }
 */
class CacheOptions {

    static ID_FIELD = "id"

    fetchId(data) {
        return data[CacheOptions.ID_FIELD]
    };

    fetchData(data) {
        return data.content;
    }

    generateCacheKey(payload, request) {
        return payload.asString() + request.getUniqueId();
    }

    generateIdCacheKey(request, id) {
        return request.getUniqueId() + id
    }

    toPageData() {

    }
}

class PageResponse {
    length;
    content;
    totalElements

}

class CacheRequest extends ServerRequest {
    #isIdRequest = false;
    #isCollectionRequest = false;
    #isDeleteRequest = false;
    static #ids = new Map();
    static #requestToData = new Map()

    #executionParams = null;

    #cacheOptions;

    constructor(cacheOptions = new CacheOptions()) {
        super();
        this.#cacheOptions = cacheOptions;
    }

    edit(payload, success, fail) {
        super.edit(payload, (data) => {
            CacheRequest.#ids.set(this.#cacheOptions.generateIdCacheKey(this, id), data)
        }, fail)
    }

    add(payload, success, fail) {
      super.add(payload,
            (data)=> {
                success(data)
                let key = this.#cacheOptions.generateIdCacheKey(this, id);
                CacheRequest.#ids.set(key, data)

            }, fail);
    }



    byId(id, success, fail) {
        if (_.isNull(id)) {
            super.byId(id, success, fail)
            return;
        }
        this.#executionParams = id;
        this.#isIdRequest = true;
        let key = this.#cacheOptions.generateIdCacheKey(this, id);
        let el = CacheRequest.#ids.get(key);
        if (_.isNull(el)) {
            super.byId(id, (data) => {
                CacheRequest.#ids.set(key, data)
                success(data);
            }, fail)
        } else {
            success(el);
        }

    }

    isGetByIdRequest() {
        return this.#isIdRequest;
    }

    isCollectionRequest() {
        return this.#isCollectionRequest;
    }

    delete(id, success, fail) {
        this.#executionParams = id;
        this.#isDeleteRequest = true;
        super.delete(id, success, fail);
    }

    deleteItems(ids, success, fail) {
        this.#executionParams = ids
        this.#isDeleteRequest = true;
        super.deleteItems(ids, success, fail);
    }


    successFunction = (success) => {


        return (data) => {
            if (typeof success === 'function') {
                success(data);
                //here set data to cache


            }

            this.#executionParams = null;
            if (this.getForceNoCache()) {
                return
            }
            if (this.isGetByIdRequest()) {
                CacheRequest.#ids.push(
                    this.#cacheOptions.generateIdCacheKey(this, this.#cacheOptions.fetchId(data)), data)
                this.#executionParams = null;
            }

            if (this.isCollectionRequest()) {
                let key = this.#cacheOptions.generateCacheKey(this.getPayload(), this);
                CacheRequest.#requestToData.set(key, this.#cacheOptions.fetchData(data).map(x => {
                    return this.#cacheOptions.fetchId(x)
                }))
                _.each(this.#cacheOptions.fetchData(data), (element) => {
                    CacheRequest.#ids.set(this.#cacheOptions.generateIdCacheKey(this, element.id), element)
                })
            }

            if (this.#isDeleteRequest) {
                let ids = [...this.#executionParams];
                this.#executionParams = null;
                _.each(ids, (id) => {
                    CacheRequest.#ids.remove(this.#cacheOptions.generateIdCacheKey(this, id))
                })

            }
        };
    }

    search(magicRequest, success, fail) {
        this.#isCollectionRequest = true;
        let key = this.#cacheOptions.generateCacheKey(magicRequest, this);
        if (!this.getForceNoCache(), CacheRequest.#requestToData.has(key)) {
            let data = [];
            let ids = CacheRequest.#requestToData.get(key)
            _.each(ids, (id) => {
                //assume that id should here be anyway
                let idKey = this.#cacheOptions.generateIdCacheKey(this, id);
                data.push(CacheRequest.#ids.get(idKey));
            })

            this.successFunction(success)(data);

            return;

        }

        //fetch from server if no data
        super.search(magicRequest, success, fail);
    }


}