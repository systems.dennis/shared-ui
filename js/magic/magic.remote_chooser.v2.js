class MagicRemoteFetcher {

    init() {
    }

    drawSelection(magicRemoteChooser) {
    }

    getName() {
    }
}

class FavoriteFetcher extends MagicRemoteFetcher {

    #content = h.div('content-chooser');
    #parent;

    constructor(parent = null) {
        super();
        this.#parent = parent;
    }

    getParent(){
        return this.#parent;
    }

    
    drawItemSelector(magicRemoteChooser) {
        magicRemoteChooser.getData(magicRemoteChooser.#parent.getHeader())
        return magicRemoteChooser.getContent();
    }

    getData(header) {
        let type = encodeURI(header.getData().searchName);

        let _this = this;
        let searchPath = Environment.dataApi + "/api/v2/shared/favorite/byName?name=" + type;

        let searchPathFav = Environment.dataApi + "/api/v2/shared/favorite/list/";

        Executor.runGet(searchPath, function (res) {
            let favType = res.result;
            let field = _this.getParent().getHeader().getField();

            Executor.runGet(searchPathFav + favType + "/" + type,  (data)=> {
                _this.showVariants(data, field );
            })

        }, true, Executor.HEADERS);
    }

    showVariants(res, field) {
        this.#content.text(null);
        utils.each(res.content, (item) => {
            this.#content.add(this.#parent.compile(item, field))
        });
    }

    getContent() {
        return this.#content;
    }

    getName() {
        return "global.pages.chooser.remote.favorite";
    }
}

class SearchMagicFetcher extends MagicRemoteFetcher {

    #parent;
    #searchText;
    #content = h.div('content-chooser');
    #selection = h.div("selector");

    constructor(parent = null) {
        super();
        this.#parent = parent;
    }

    
    drawSelectionBox(parent) {
        if (!this.#selection.ccl("loaded")) {
            this.buildSearchForm(parent);
            this.showSelection();
        };
        return parent.#content
    }

    drawItemSelector(parent) {
        return parent.drawSelectionBox(parent);
    }

    getContent() {
        return this.#content;
    }

    getStatus(){
        return h.div("search_entity_title").text("global.pages.search." + this.#parent.getHeader().getField());
    }

    buildSearchForm(parent) {
        let _this = this;

        let searchForm = h.div("div").cl("search_entity_form").appendTo(parent.#content);
        let searchHeader = h.div("div").cl("search").cl("search_header");
 
        //add button

        h.img("add.png.png").cl("_s_box_add_btn").wh(16).click(function () {
            _this.#parent.showForm();
        }).appendTo(searchHeader).hide();

        searchHeader.prependTo(searchForm);
        this.#searchText = h.input("text").cl("search_text").appendTo(searchHeader);
        h.div('search_backgraund_image').appendTo(searchHeader);

        this.#searchText.get().onkeyup = function () {

            _this.showSelection(0);
        };
        parent.getContent().add(searchForm);

        this.#selection.cl("loaded").appendTo(searchForm);

    }

    refresh() {
        this.showSelection(0);
    }

    showSelection(page = 0) {

        let searchPath = Environment.dataApi + "/api/v1/search/type/";

        //todo change it to the properties


        let type = this.#parent.getHeader().getData().searchName;

        let field = this.#parent.getHeader().getField();

        let _this = this;
        searchPath = searchPath + type + "?a=a";

        try {
            if (changeSearchPath != null) {
                searchPath = changeSearchPath(searchPath, this.#parent.getHeader().getData().searchName);

            }
        } catch ( e){
            log.debug("Error "  + e)
        }


        Executor.runGet( searchPath+ "&s=" + encodeURI(_this.#searchText.get().value) + '&page=' + page, function (res) {
            _this.showVariants(res, field);
        }, true, Executor.HEADERS);

        // this.addModal();
    }

    showVariants(res, field) {
        this.#selection.text(null);
        utils.each(res.content, (item) => {
            this.#selection.add(this.#parent.compile(item, field))
        });

        this.#parent.showChooserPagination(res, this);
    }

    getName() {
      
        return "global.pages.chooser.remote.search" ;
    }


}

class MagicRemoteChooser {

    static SELECT_TEXT = "pages.global.search.select";

    #fetchers = [];
    #tabbed = false;
    #whereToDraw;
    #data;
    #hiddenValue;
    #header;
    #dataType;
    #favType;
    #tab;
    #wrapper = h.div('chooser-wrapper').hide().appendToBody();
    #content = h.div("remote-chooser").appendTo(this.#wrapper);
    #status
    #innerForm;

    constructor(whereToDraw, data, hiddenElement, dataType, header, favType) {

        this.#tab = new MagicTab();
        try {

                this.#fetchers = getCustomSearchFetchers(this, dataType, data);

                if (_.isEmpty(this.#fetchers)){throw Error('No fetchers found')}
        } catch (e) {

            this.addFetcher(new SearchMagicFetcher(this))
            this.addFetcher(new FavoriteFetcher(this))
        }

        this.#data = data;
        this.#hiddenValue = h.from(hiddenElement);
        this.#header = header;
        this.#whereToDraw = h.from(whereToDraw);
        this.#dataType = dataType;
        this.#favType = favType;
        this.#status = new SearchMagicFetcher(this).getStatus()
        this.draw();
        this.addCloseBtn();
    }

    getWhere() {
        return this.#whereToDraw;
    }

    getData() {
        return this.#data;
    }

    getHeader() {
        return this.#header;
    }

    init(){
       this.drawSelection();
    }

    buildAddForm() {
        log.debug('adding form')
        if (utils.isNull(this.#innerForm)) {

            let formId = this.#whereToDraw.getId() + "_s_box_add" + new Date().getTime();

            this.#innerForm = h.div("modal").id(formId).appendTo(this.#content);
        }
        try {
            
            new fetcher(this.#innerForm.getId(), "/api/v2/" + this.#dataType, "form", null, this).new();
        } catch (exc) {
            console.error(exc)
            this.#wrapper.hide();
        }

    }


    addCloseBtn(){
      this.#content.add(h.div('close-chooser-btn').text('close', false).click(()=>{
        this.hideForm();
      }))
    }

    closeImg(){
     return h.divImg('rm.png').wh(16).click(()=>{
          this.hideForm();
        })
      }

    draw() {

        utils.each(this.#fetchers, (fetcher) => {
            this.#tab.addLazyTab(fetcher.getName(), fetcher.drawItemSelector, fetcher,)
        });

        let windowHeaders = h.div('chooser_headers').appendTo(this.#content);
        this.#status.appendTo(windowHeaders);
        this.closeImg().appendTo(windowHeaders);

        this.#tab.deployTo(this.#content);
        this.drawSelection();
        // this.showForm();
    }

    hideForm() {
        this.#wrapper.hide();
    }

    showForm() {
        this.#wrapper.show();
    }

    getContent() {
        return this.#content;
    }


    drawSelection() {
        // log.debug(this.getWhere().get(), false)
        this.getWhere().text(null);
        // log.debug(this.getWhere().get(), false)
        let _this = this;
        if (utils.isNull(this.getData()) || utils.isEmpty(this.getData().value)) {

            let value = h.a("#", MagicRemoteChooser.SELECT_TEXT, true).add(h.div("select_item")).click(function () {
                _this.showForm(this)
            });

            h.div("magic_object_chooser_content").add(value).appendTo(this.getWhere());
        } else {

            let drawContent;

            try {
                drawContent = drawCustomSelectChooserElementValue(this.getHeader(), this.getData()).click(function () {
                    _this.showForm(this)
                });

            } catch (e) {

            }

            if (drawContent == null) {
                drawContent = MagicTable.defaultConverter(this.#header, this.#data, this.#data, null, null, function () {
                }).click(function () {
                    _this.showForm();
                });
            }

            let value = h.a("#", "").add(drawContent);

            let cleanValueImg = h.img('rm.png').click(function () {
                _this.#data = {};
                _this.init();
                if(_this.#whereToDraw){
                    _this.#whereToDraw.setData('id', 0 );
                };
                _this.#hiddenValue.get().value = 0;
                
            });

            //reset where and add correspondent result;
            this.getWhere().text(null);

            h.div("magic_object_chooser_content").add(value).add(cleanValueImg).appendTo(this.getWhere());

        }

    }

    compile(res) {

        let _this = this;
        let element;
        try {
            element = drawCustomSelectChooserElement(this.#header, res, this.#data);
        } catch (e) {
            log.trace(e, true);
            element = h.div("selection").text(res[this.#header.getData().searchField], false);
        }

        if (element == null) {
            element = h.div("selection").text(res[this.#header.getData().searchField], false);
        }

        return h.a("#").cl("selection").id(res['id']).clickPreventive(function () {
            _this.selectData(res)
        }).add(element);

    }


    selectData(data) {
       
        this.#hiddenValue.get().value = data.id;
        this.#whereToDraw.setData('id', data.id )
        if (utils.isEmpty(data.value)) {
            this.#data.value = data[this.#header.getData().searchField];
            this.#data.id = data.id;
            this.#data.key = data.id;
            this.#data.additional = data.icon;

            try {
                transformCustomData(this.#header.getField(), this.#data, data);
            } catch (e) {
                log.trace(e, true)
            }

        } else {
            this.#data = data;
        }
        this.drawValueInWhere(this.#data);
        this.hideForm();
        this.#hiddenValue.get().dispatchEvent(new Event('change'));

    }

    drawValueInWhere(data) {
        this.#data = data;
        this.drawSelection();
    }

    addFetcher(fetcher) {
        this.#fetchers.push(fetcher);
        return this;
    }

    showChooserPagination(res, fetcher) {
        let _this = this
        let totalPages = res.totalPages;
        let currentPage = res.number + 1;
        fetcher.getContent().get().querySelector('.chooser_pagination')?.remove()
        if (totalPages > 1) {
            let pagination = h.div('chooser_pagination');
            let prevBtn = h.img('prev.svg').cl('prev_btn_chooser').appendTo(pagination);
            h.div('chooser_bord').text(`${currentPage} / ${totalPages}`, false).appendTo(pagination);
            let nextBtn = h.img('next.svg').cl('next_btn_chooser').appendTo(pagination);

            if (currentPage == totalPages) {
                nextBtn.cl('opacity');
                prevBtn.click(function () {
                    fetcher.showSelection(res.number - 1);
                    pagination.remove();
                })
            }
            if (currentPage == 1) {
                prevBtn.cl('opacity');
                nextBtn.click(function () {
                    fetcher.showSelection(res.number + 1);
                    pagination.remove();
                })
            }
            if (currentPage !== totalPages && currentPage !== 1) {
                prevBtn.click(function () {
                    fetcher.showSelection(res.number - 1);
                    pagination.remove();
                })
                nextBtn.click(function () {
                    fetcher.showSelection(res.number + 1);
                    pagination.remove();
                })
            }
           
           
           
            fetcher.getContent().add(pagination.get())
        }

    }

    getDataById(id, type) {
        if (_e(id, 0)){return  undefined}
        let _this = this;
        Executor.runGet(Environment.dataApi + "/api/v2/flaw/" + type + "/root/fetch/details/"+ id, function (res) {
          _this.selectData(res.data)
        }, true);

        return undefined;
    }

}