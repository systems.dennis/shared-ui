
class MagicDoubleRangeOptions{

    #maxValue = 100
    #minValue = 0
    #listener = null;

    setMaxValue(val){
        this.#maxValue = val;
        return this;
    }

    getMaxValue(){
        return this.#maxValue ;
    }

    setMinValue(val){
        this.#minValue = val;
        return this;
    }

    getMinValue(){  
        return this.#minValue;
    }
        
    setListener(list){
        this.#listener = list;
        return this;
    }

    getListener(){
        return this.#listener;
    }

}

class MagicDoubleRange{
    #where
    #options

    static CREATED = 'RANGE_CREATED'
    static FIRST_VALUE_CHANGED_FINISH = "first_value_changed_finish"
    static SECOND_VALUE_CHANGED_FINISH = "second_value_changed_finish"
    static VALUE_CHANGED = "value_changed"

    constructor(where, options){
        this.#where = where
        this.#options = options
    }

    fire(eventName, additional){
        let event = {
            eventName:eventName,
            additional:additional,
            parent: this,
        }
        
        if(this.getListener()){
            this.getListener()(event)
        }
    }

    getOptions(){
        return this.#options;
    }

    getListener(){
        return this.#options.getListener()
    }

    getMaxValue(){
        return this.#options.getMaxValue()
    }
    getMinValue(){
        return this.#options.getMinValue()
    }

    build(){
        this.sliderOne = h.input("range").id("slider-1")
        .attr("max", this.getMaxValue())
        .attr("min", this.getMinValue())
        .text(this.getMinValue(), false)
        .inputFunc(()=>{
            this.slideOne()
        })
        .handleEvent("mouseup", ()=>{
            this.fire(MagicDoubleRange.FIRST_VALUE_CHANGED_FINISH, {firstValue: this.sliderOne.val(), secondValue: this.sliderTwo.val()})
        })

        this.sliderTwo = h.input("range")
        .id("slider-2")
        .attr("max", this.getMaxValue())
        .attr("min",  this.getMinValue())
        .text(this.getMaxValue(), false)
        .inputFunc(()=>{
            this.slideTwo();
        })
        .handleEvent("mouseup", ()=>{
            this.fire(MagicDoubleRange.SECOND_VALUE_CHANGED_FINISH, {firstValue: this.sliderOne.val(), secondValue: this.sliderTwo.val()})
        })

        this.displayValOne = h.tag("span").id("range1");
        this.displayValTwo = h.tag("span").id("range2");
        this.minGap = 0;
        this.sliderTrack = h.div("slider-track");
        this.sliderMaxValue = this.sliderOne.getA("max")
        this.sliderMinValue = this.sliderOne.getA("min")

        this.distanceMin = h.input("text")
        .id("distanse-1")
        .blur((e)=>{
            this.blurOne(e)
            this.fire(MagicDoubleRange.FIRST_VALUE_CHANGED_FINISH, {firstValue: this.sliderOne.val(), secondValue: this.sliderTwo.val()})
        })

        this.distanceMax = h.input("text")
        .id("distanse-2")
        .blur((e)=>{
            this.blurTwo(e)
            this.fire(MagicDoubleRange.SECOND_VALUE_CHANGED_FINISH, {firstValue: this.sliderOne.val(), secondValue: this.sliderTwo.val()})
        })

        let slider = h.div('double_slider').appendTo(this.#where)
        h.div("range-wrapper")
        .add(h.div("values")
            .add(this.displayValOne)
            .add(h.tag("span").text("-", false))
            .add(this.displayValTwo))
        .add(h.div('range-container')
            .add(this.sliderTrack)
            .add(this.sliderOne)
            .add(this.sliderTwo)
        ).appendTo(slider)

        this.distanceMin.appendTo(slider)
        this.distanceMax.appendTo(slider)

        this.slideOne();
        this.slideTwo();

        this.fire(MagicDoubleRange.CREATED, slider)
    }


    blurOne(e){
        if(parseInt(e.value) < this.getMinValue()){
            e.value = this.getMinValue()
        }
        this.sliderOne.text(parseInt(e.value), false)
        this.slideOne()
    }  
    
    blurTwo(e){
        if(parseInt(e.value) > this.getMaxValue()){
            e.value = this.getMaxValue()
        }
        this.sliderTwo.text(parseInt(e.value), false)
        this.slideTwo()
    }

    slideOne(){
        if(parseInt(this.sliderTwo.val()) - parseInt(this.sliderOne.val()) <= this.minGap){
            this.sliderOne.text(parseInt(this.sliderTwo.val()) - this.minGap, false);
        }
        this.displayValOne.text(this.sliderOne.val(), false) 
        this.distanceMin.text(this.sliderOne.val(), false) 
        this.fire(MagicDoubleRange.VALUE_CHANGED, {firstValue: this.sliderOne.val(), secondValue: this.sliderTwo.val()})
        this.fillColor();
    }

    slideTwo(){
        if(parseInt(this.sliderTwo.val()) - parseInt(this.sliderOne.val()) <= this.minGap){
            this.sliderTwo.text(parseInt(this.sliderOne.val()) + this.minGap, false)
        }
        this.displayValTwo.text(this.sliderTwo.val(), false);
        this.distanceMax.text(this.sliderTwo.val(), false);
        this.fire(MagicDoubleRange.VALUE_CHANGED, {firstValue: this.sliderOne.val(), secondValue: this.sliderTwo.val()})
        this.fillColor();   
    }

    fillColor(){

        let percent1 = (
            (this.sliderOne.val() -  this.sliderMinValue) /
            (this.sliderMaxValue -  this.sliderMinValue)) * 100;

        let percent2 = (
            (this.sliderTwo.val()-this.sliderMinValue )/
            (this.sliderMaxValue-this.sliderMinValue)) * 100;

        this.sliderTrack.get().style.left = `${percent1}%`;
        this.sliderTrack.get().style.width = `${percent2 - percent1}%`;
    }

    resetRange(){
        this.sliderMaxValue = this.getMaxValue()
        this.sliderMinValue = this.getMinValue()
        this.sliderOne
            .attr("max", this.getMaxValue())
            .attr("min", this.getMinValue())
            .text(this.getMinValue(),false)
        this.sliderTwo
            .attr("max", this.getMaxValue())
            .attr("min", this.getMinValue())
            .text(this.getMaxValue(),false)
        this.slideOne()
        this.slideTwo()
    }

}
