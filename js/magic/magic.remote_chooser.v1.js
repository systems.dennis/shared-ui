class MagicRemoteChooser {
    #data;
    #hiddenValue;

    #whereToDraw;
    #dataType;

    //div to create an add form
    #innerForm;

    #searchText;
    #content = h.div("search_entity_content");

    static SELECT_TEXT = "pages.global.search.select";
    static HIDDEN = "hidden_modal";
    #header;

    #favType;
    #selection = h.div("modal-selector").cl(MagicRemoteChooser.HIDDEN).appendTo(document.body);

    #table;
    constructor(whereToDraw, data, hiddenElement, dataType, header, refreshable = null, favType) {
        this.#data = data;
        this._refreshable = refreshable;
        this.#hiddenValue = h.from( hiddenElement);
        this.#header = header;
        this.#whereToDraw = h.from(whereToDraw);
        this.#dataType = dataType;
        this.#favType = favType;


        this.init();
        
        log.info(this.#whereToDraw.get())
    }

    init() {

        this.drawValueInWhere();
    }

    drawValueInWhere() {
        this.#whereToDraw.text(null);
        let _this = this;
        if (utils.isNull(this.#data) || utils.isEmpty(this.#data.value)) {

            let value = h.a("#", MagicRemoteChooser.SELECT_TEXT, true).add(h.div("select_item") ).click(function (){
                _this.drawSelectionBox()
            });

            h.div("magic_object_chooser_content").add(value).appendTo(this.#whereToDraw);
        } else {

            let drawContent ;

            try {
                drawContent = drawCustomSelectChooserElementValue(this.#header, this.#data).click(function (){
                    _this.drawSelectionBox();
                });

            } catch (e){

            }

            if (drawContent == null){
                drawContent = MagicTable.defaultConverter(this.#header, this.#data, this.#data, null, null, function () {
                }).click(function () {
                    _this.drawSelectionBox()
                });
            }

            let value = h.a("#", "").add(drawContent);

            let cleanValueImg = h.img('rm.png').click(function () {
                _this.#data = {};
                _this.init();
            });

            //reset where and add correspondent result;
            this.#whereToDraw.text(null);

            h.div("magic_object_chooser_content").add(value).add(cleanValueImg).appendTo(this.#whereToDraw);

        }
    }

    /**
     *
     * Draws selection window for user to select data
     */
    drawSelectionBox() {
        if (!this.#selection.get().classList.contains("loaded")) {
            this.buildSearchForm();
            this.showSelection();
        }

        this.addModal();
    }

    removeModal() {
        this.#selection.cl(MagicRemoteChooser.HIDDEN);

    }

    buildSearchForm() {
        let _this = this;


        let searchForm = h.div("div").cl("search_entity_form").appendTo(this.#selection);


        let searchHeader = h.div("div").cl("search").cl("search_header");

        h.div("search_entity_title").text("global.pages.search." + this.#header.getField()).appendTo(searchForm);

        //add button

            h.img("add.png.png").cl("_s_box_add_btn").wh(16).click(function () {
                if (_this.#innerForm == undefined) {
                    let formId = _this.#whereToDraw.get().id + "_s_box_add" + new Date().getTime();
                    _this.#innerForm = h.div("modal").id(formId).cl("hidden").cl("form").appendTo(searchForm);
                }
                try {
                    new fetcher(_this.#innerForm.get().id, "/api/v2/" + _this.#dataType, "form", null, this).new();
                } catch (exc) {
                    console.error(exc)
                    _this.removeModal();
                }
            }).appendTo(searchHeader);

        searchForm.add(searchHeader)

        this.#searchText = h.input("text").cl("search_text").appendTo(searchHeader);
        this.#searchText.get().onkeyup = function (){
            _this.showSelection(0)
        }
        _this.#content.appendTo(searchForm);



        let footer = h.div("search_footer").appendTo(searchForm);


        h.input("button").cl("close_modal").click(function (){
            _this.removeModal();
        }).src("/images/close.png.png") //button src? hm ? why todo
            .text("pages.global.remote_search.close").appendTo(footer)

        this.#selection.cl("loaded");

    }

    refresh(){
        this.showSelection(0);
    }

    showSelection(page = 0) {

        if (page == 0 && utils.isEmpty(this.#searchText.get().value) && this.#favType != null){
            //draw favorite here


        }

        let searchPath = Environment.dataApi + "/api/v1/search/type/";

        let type = this.#header.getData().searchName;

        let field = this.#header.getField();

        let _this = this;

        Executor.runGet(searchPath + type + "?s=" + encodeURI(_this.#searchText?.get()?.value) + '&page=' + page, function (res) {
log.debug(res)
            _this.showVariants(res, field);
        }, true, Executor.HEADERS);

        this.addModal();
    }

    showVariants(res, field) {
        let content = this.#content;
        content.text(null);
        for (let item = 0; item < res.content.length; item++) {
            content.add(this.compile(res.content[item], field))
        }

        this.showChooserPagination(res, field);
    }

    compile(res) {

        let _this = this;
        let element ;
        try {
            element = drawCustomSelectChooserElement(this.#header,res, this.#data );
        } catch (e){
            console.error(e);
            element = h.div("selection").text(res[this.#header.getData().searchField], false);
        }

        if (element == null){
            element = h.div("selection").text(res[this.#header.getData().searchField], false);
        }

        let li = h.a("#").cl("selection").id(res['id']).click(function (x,e){
            e.preventDefault();

            _this.selectData(res)
log.debug(res)
        }).add(element);

        return li
    }

    showChooserPagination(res){
        let _this = this
        let totalPages = res.totalPages;
        let currentPage = res.number +1;
        if(totalPages>1){
            let pagination = h.div('chooser_pagination');
            let prevBtn = h.img('prev.svg').cl('prev_btn_chooser').appendTo(pagination);
            h.div('chooser_bord').text(`${currentPage} / ${totalPages}`, false).appendTo(pagination);
            let nextBtn = h.img('next.svg').cl('next_btn_chooser').appendTo(pagination);

            if(currentPage == totalPages){
                nextBtn.cl('opacity');
                prevBtn.click(function(){ _this.showSelection(res.number - 1);})
            }
            if(currentPage == 1){
                prevBtn.cl('opacity');
                nextBtn.click(function(){ _this.showSelection(res.number + 1);})
            }
            if(currentPage !== totalPages && currentPage !== 1){
                prevBtn.click(function(){ _this.showSelection(res.number - 1);})
                nextBtn.click(function(){ _this.showSelection(res.number + 1);})
            }
            this.#content.add(pagination.get())
        }

    }


    selectData(data){

        this.#hiddenValue.get().value = data.id;
        if (utils.isEmpty(data.value )){
            this.#data.value = data[this.#header.getData().searchField];
            this.#data.id = data.id;
            this.#data.key = data.id;
            this.#data.additional = data.icon;
if(this.#whereToDraw){
                this.#whereToDraw.setData('id', data.id)
            }
            

            try {
                transformCustomData(this.#header.getField(), this.#data, data);
            } catch (e){

            }

        } else {
            this.#data = data;
        }
        this.drawValueInWhere();
        this.removeModal();


    }
    addModal() {
        this.#selection.rcl(MagicRemoteChooser.HIDDEN);
    }


    getDataById(id, type) {
        if (_e(id, 0)){return  undefined}
        let _this = this;
        Executor.runGet(Environment.dataApi + "/api/v2/flaw/" + type + "/root/fetch/details/"+ id, function (res) {
          _this.selectData(res.data)
        }, true);

        return undefined;
    }


}