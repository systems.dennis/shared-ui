/**
 * Personal settings allow to save and read settings from ui
 */
class PersonalSettings{

    static TYPE_STRING  = "string";
    static TYPE_BOOL = "boolean"
    static PERSONAL_SETTINGS_PATH = Environment.dataApi + "/api/v2/shared/personal_settings";

    static setSetting(name, value, success = ()=> {}, type= this.TYPE_STRING.TYPE_STRING){
        Executor.runPostWithPayload(PersonalSettings.PERSONAL_SETTINGS_PATH + "/add", success, {
            'name' : name,
            'value' : value
        });
    }

    static getSetting(name, def){
        let res;
        Executor.runGet(PersonalSettings.PERSONAL_SETTINGS_PATH + "/by_name?name=" + name, function (data){
            res = data.value
        
        }, false, undefined, ()=> {res =  def});

        if (_e(res?.type, PersonalSettings.TYPE_BOOL)){
            return !utils.isFalse(res)
        }

        return res;
    }

    static deleteSetting(name, success){
        Executor.runDelete(PersonalSettings.PERSONAL_SETTINGS_PATH + "/by_name?name=" + name, success);
    }
}