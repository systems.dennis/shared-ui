class dom {

    static CHECKED = "checked";
    static HIDDEN = "hidden";
    static CHECKBOX = "checkbox";
    static LABEL = "label";
    static INPUT = "input";
    static TEXT = "text";
    static SPAN = "span";
    static DIV = "div";
    static SELECT = "select";
    static OPTION = "option";

    static TYPE_ATTR = "type";

    static REMOVE_TYPE_REMOVE = "remove";
    static REMOVE_TYPE_HIDE = "hide";
    static MODAL = "modal";


    static findSelectedValue(x) {
        try {
            return x.options[x.selectedIndex].innerHTML;
        } catch (e) {
            log.error(e);
            return "";
        }
    }

    static setCboValue(cbo, value) {
        if (cbo.isH) {
            cbo = cbo.get();
        }
        _.each(cbo.options, (option, i) => {
            if (_e(option.value, value)) {
                cbo.selectedIndex = i;
            }
        })
    }

    static addOptionInSelect(select, value) {
        if (select.isH) {
            select = select.get();
        }

        let newOption = new Option(value.label, value.value);
        select.append(newOption)
    }

    static redirectToViewPage(type, id) {
        return h.a(Environment.self + 'pages/' + type + '/view/?id=' + id).attr('target', "_blank")
    }

    static waitForItem(item, whatTodo) {
        (async () => {
            log.trace("waiting for variable");
            if (item() === undefined) { // define the condition as you like
                setTimeout(function () {
                    (async () => {
                        dom.waitForItem(item, whatTodo)
                    })()
                }, 100);
            } else {
                whatTodo();
            }
        })();
    }

    static chk(id = null, label, checked, where) {

        let chk = h.tag(dom.INPUT).attr(dom.TYPE_ATTR, dom.CHECKBOX).id(id);
        chk.appendTo(where);
        h.tag(dom.LABEL).text(label, false).attr('for', id).appendTo(where).get().checked = checked;
        return chk;
    }

    static navigate(to) {
        document.location.href = Environment.self + "/" + to;
    }

    static message(text = "global.success", toTranslate = true) {
        $.toast(toTranslate ? _.$(text) : text);
    }

    static hasChildren(obj) {
        if (obj.isH) {
            return obj.get().childNodes.length > 0;
        } else {
            return obj.childNodes.length > 0;
        }
    }

    static getCboValue(cbo, value) {
        for (let i = 0; i < cbo.options.length; i++)
            if (cbo.options[i].value == value) {
                cbo.selectedIndex = i;
                return;
            }
    }

    static selectedOption(cb, toWrap = false) {

        if (cb.isH) {

            let res = cb.get().options[cb.get().selectedIndex];
            return toWrap ? h.from(res) : res;
        } else {
            let res = cb.options[cb.selectedIndex];
            return toWrap ? h.from(res) : res;
        }
    }

    static getCursorIndex(inputH) {
        return inputH.get().selectionStart;
    }

    static setCursorIndex(inputH, i) {
        inputH.get().selectionStart = i;
    }

    static setSelectionRange(inputH, from, to) {
        inputH.setSelectionRange(from, to)
    }

    static select(id) {
        return h.tag("select").id(id).cl("dropdown");

    }

    static option(item, toTranslate = false) {
        return h.tag("option").text(item.label, toTranslate).attr("value", item.value);
    }


    static tinyTextArea() {
        return h.tag("textarea").cl("text_area_tini_mce").id("id_" + new Date().getTime() + "_txt_area");
    }

    static errorPlate(id) {
        return h.tag('span').cl("hidden").cl("error_plate").id("error_" + id);
    }

    static img(path, size) {
        return h.img(path).wh(size);
    }


    static linkTo(objectWhat, linkToObj) {
        let bounds = h.from(linkToObj).get().getBoundingClientRect();
        let hObjWhat = h.from(objectWhat).get()
        let hlinkToObj = h.from(linkToObj).get()
        hObjWhat.style.top = bounds.top + hlinkToObj.height + 'px';
        hObjWhat.style.left = bounds.left + hlinkToObj.width + 'px';
    }

    static createModal(removeType, className = null) {
        let modalName = "modal_" + new Date().getTime() + Math.ceil(Math.random() * (1000 - 1) + 1);
        let modalDiv = h.div(_.ofNullable(className, dom.MODAL)).setData(dom.MODAL, modalName).click(() => {
            MagicPage.focusModal(modalName)
        })
        MagicPage.setModal(modalName, modalDiv, removeType);
        return modalDiv
    }

    static redirect(url, blank = true) {
        h.a(Environment.self + url)
            .attr("target", blank ? "_blank" : "_self").performClick();
    }

    static sortDate(arrWithDate) {
        let sorted = arrWithDate.sort((a, b) => {
            let timeA = a.date.split(' ');
            let dataA = timeA[1].split('.').reverse().join('-');
            let hourA = timeA[0];
            let timeSpA = new Date(dataA + 'T' + hourA).getTime();

            let timeB = b.date.split(' ');
            let dataB = timeB[1].split('.').reverse().join('-');
            let hourB = timeB[0];
            let timeSpB = new Date(dataB + 'T' + hourB).getTime();

            return timeSpB - timeSpA
        })

        return sorted
    }

    static editableDiv(div, isEditable) {
        if (div.isH) {
            div.attr("contenteditable", isEditable);
        } else {
            div.setAttribute("contenteditable", isEditable)
        }
        return div
    }

    static setCheckboxValue(checkbox, val) {
        if (checkbox.isH) {
            checkbox.get().checked = val;
        } else {
            checkbox.checked = val;
        }
    }


}
