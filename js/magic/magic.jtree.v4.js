class MagicTreeOptions {
    #paddingDef;
    #onSelect;
    #id;
    #subFetcher;
    #treeListener;
    #itemRenderer;
    #translate = false;
    #showCount = true;
    #imgDivIntree = false

    constructor(paddingDef = 10, onSelect, subFetcher, id = null) {
        this.#paddingDef = paddingDef;
        this.#onSelect = onSelect;
        this.#id = id;
        this.#subFetcher = subFetcher;
    }

    setToTranslate() {
        this.#translate = true;
        return this
    }

    addImgDivInTree() {
        this.#imgDivIntree = true;
        return this;
    }

    getIsImgDiv() {
        return this.#imgDivIntree
    }

    getTranslate() {
        return this.#translate
    }

    setIsShowCount(value) {
        this.#showCount = value;
        return this;
    }

    getShowCount() {
        return this.#showCount;
    }

    setItemRenderer(renderer) {
        this.#itemRenderer = renderer;
    }

    setTreeListener(listener) {
        this.#treeListener = listener;
    }

    getItemRenderer() {
        return this.#itemRenderer;
    }

    getTreeListener() {
        return this.#treeListener;
    }

    getPaddingDef() {
        return this.#paddingDef;
    }

    getOnSelect() {
        return this.#onSelect;
    }

    getId() {
        return this.#id;
    }

    getSubFetcher() {
        return this.#subFetcher;
    }
}

class MagicTree {
    static ITEM_CLASS = "new_item";
    static BOX_CLASS = "tree_box";

    #options;

    #storage = new Map();

    #userDataDiv = h.div("tree_data_content");

    constructor(options) {
        this.#options = options;
    }

    getOptions() {
        return this.#options;
    }

    getListener() {
        return this.#options.getTreeListener();
    }

    getContent() {
        return this.#userDataDiv;
    }

    fireEvent(eventName, element, elementId, tree) {
        if (_.notNull(this.getListener())) {
            this.getListener()(new MagicTreeEvent(eventName, element, elementId, tree));
        }
    }

    addItem(idRoot, data) {
        let level = this.getLevel(idRoot);

        let box = idRoot.firstByClass(MagicTree.BOX_CLASS);

        if (box == null) {
            box = h.div(MagicTree.BOX_CLASS).appendTo(idRoot);
        }

        let mainText = data.name
        let cntText = data.cnt > 0 ? " [" + data.cnt + "]" : ""

        if (data.selectable) {
            if (_.isNull(this.#options.getItemRenderer())) {
                mainText = this.defaultRenderItem(mainText, cntText, data);
            } else {
                mainText = this.#options.getItemRenderer()(mainText, cntText, data);
            }
        }

        this.fireEvent(MagicTreeEvent.PREBUILD_TEXT, mainText, data, this);

        const id = idRoot.get().id + "_" + level;

        let res = h.div(MagicTree.ITEM_CLASS)
            .setData("parent", idRoot.get().id)
            .setData("level", level)
            .appendTo(box);


        this.mainBranchContainer = h.div("branch_container").id(data.id).appendTo(res);
        if (_e(data.cnt, 0)) {
            this.mainBranchContainer.click(function (e, ev) {
                // dom.redirect('?id=' + data.id)
                try {
                    ev.preventDefault();

                    let el = document.querySelectorAll(".clicked");
                    for (let i = 0; i < el.length; i++) {
                        el[i].classList.remove("clicked");
                    }
                    e.classList.add("clicked");

                    _this.getOptions().getOnSelect()(data, _this.getOptions().getId());
                } catch (e) {
                    console.error(e);
                }
            })
        } else {
            this.mainBranchContainer.click(function (e) {
                _this.collapseOrFetch(e);
            });
        }

        if (data.selectable) {
            mainText.appendTo(this.mainBranchContainer);
        } else {
            this.mainBranchContainer
                .addIf(this.#options.getIsImgDiv(), h.div("img_div"))
                .add(h.div("first_level_title").text(mainText, this.#options.getTranslate()))
                .andTextIf(this.#options.getShowCount(), cntText, false)
                .cl(data.id);

        }

        let _this = this;
        if (data.cnt > 0) {
            h.img("collapsed.png.png")
                .cl("collapsed")
                .wh(16)
                .prependTo(this.mainBranchContainer)
        }

        res.get().style.paddingLeft = this.getOptions().getPaddingDef() * level;

        this.fireEvent(MagicTreeEvent.ADD_ITEM, res, data.id, this);

        this.#storage.set(data, new TreeItem(idRoot, id, this, res));

        return res;
    }

    collapseOrFetch(e) {
        if (e.classList.contains("expanded_tree")) {
            this.collapse(e);
        } else {
            this.expand(e);
        }
    }

    defaultRenderItem(mainText, cntText, data) {
        let _this = this;
        return h.a(null, mainText, this.#options.getTranslate()).andTextIf(this.#options.getShowCount(), cntText, false)
    }

    collapse(e) {
        let el = h.from(e).rcl("expanded_tree");
        el.firstByClass("collapsed", true, true).src("collapsed.png.png").rcl("expanded_tree_item");
        let divel = el.parent(true).get().querySelectorAll(".tree_box");
        _.each(divel, (el) => el.style.display = "none")
        this.fireEvent(MagicTreeEvent.COLLAPSE, h.from(e), h.from(e).getId(), this);
    }

    getNode(data) {
        return this.#storage.get(data);
    }

    removeNodeByData(data) {
        let item = this.getNode(data);
        item.removeSelf();
        this.#storage.delete(data);
    }

    expand(e) {
        e = h.from(e).cl("expanded_tree")
        e.firstByClass("collapsed", true, true).src("expanded.png.png").cl("expanded_tree_item")

        if (!e.ccl("data_fetched")) {
            e.cl("data_fetched");

            let data = this.getOptions().getSubFetcher()(e);
            _.each(data, item => {
                this.addItem(e.parent(true), item);
            });
        }

        e.parent(true).eachOfClass(MagicTree.BOX_CLASS, divEl => {
            divEl.style.display = "block";
        });

        this.fireEvent(MagicTreeEvent.EXPAND, h.from(e), h.from(e).getId(), this);
    }

    remove(idRoot) {
        idRoot.get().remove();
        this.fireEvent(MagicTreeEvent.EXPAND, idRoot, idRoot, this);
    }

    getLevel(root) {
        let level = root.getData("level");
        if (_e(level, undefined)) {
            level = 0;
        }

        return parseInt(level) + 1;
    }
}

class TreeItem {
    #parentNode;
    #id;
    #self;
    #isHide = false;
    #parent;

    constructor(parentNode, id, magicTree, self) {
        this.#parentNode = parentNode;
        this.#id = id;
        this.#parent = magicTree;
        this.#self = self;
    }

    getIsHide() {
        return this.#isHide;
    }

    toggleChilren() {
        let box = this.getChildBox();
        if (!box) {
            return;
        }

        if (this.#isHide) {
            box.show();
        }

        if (!this.#isHide) {
            box.hide();
        }

        this.#isHide = !this.#isHide;
    }

    hideChildren() {
        let box = this.getChildBox();
        if (box) {
            box.hide();
            this.#isHide = true;
        }
    }

    showChildren() {
        let box = this.getChildBox();
        if (box) {
            box.show();
            this.#isHide = false;
        }
    }

    getParent() {
        return this.#parent;
    }

    getSelf() {
        return this.#self;
    }

    getChildBox() {
        return h.fromId(this.#id).firstChildByClass('.' + MagicTree.BOX_CLASS)
    }

    getId() {
        return this.#id;
    }

    getParentNode() {
        return this.#parentNode;
    }

    removeSelf() {
        this.#self.remove();
    }
}
