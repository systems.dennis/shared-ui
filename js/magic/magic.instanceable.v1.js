class Instanceable {
    static injectTo(object) {
        object.getInstance = function () {
            if (_.isNull(object.___instance)) {
                object.___instance = new object.prototype.constructor();
                Object.freeze(object.___instance)
            }

            return object.___instance;
        }
    }
}

class Lombok {

    $(asBuilder = true, allowToString = true) {

        if (this["#___lomboked"]){
            return;
        }

        let prototype = Object.getPrototypeOf(this)
        let properties = Object.getOwnPropertyNames(this);


        _.each(properties, (name) => {

            this["#" + name] = this[name];
            delete this[name];
            prototype[this.getterFor(name)] = function () {
                return this['#' + name];
            }


            prototype["set" + Lombok.upperCaseFirstLetter(name)] = function (what) {
                this['#' + name] = what;
                if (asBuilder) return this;
            }
        })

        if (allowToString) {
            prototype ["toString"] = function (a) {
                let value = "[";
                _.each(properties, (property) => {
                    value += property + " " + this['#' + property] + " | "
                })

                value += "]"

                return value;
            }
        }

        this['#___lomboked'] = true;


    }

    getterFor(name) {
        return "get" + Lombok.upperCaseFirstLetter(name)
    }

    static upperCaseFirstLetter(what) {
        return what.firstLetter(what).toUpperCase() + what.substring(1);
    }
}
