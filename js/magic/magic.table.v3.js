class MagicTable extends h {
  static SELECTED_ROW_CLASS = 'row_selected'
  static SELECTED_ATTR = 'data-selected';
  static DELETED_ROW = "row_deleted";
  
    #magicTable;

    constructor(magicTable) {
        super();
        this.#magicTable = magicTable;
        if (magicTable.getOptions().getTableListener()) {
            this.#eventListener = magicTable.getOptions().getTableListener();
        }
    }

    getId() {
        return this.#magicTable.getId();
    }

    getSelectedCount() {
        let res = 0;
        this.#root.eachOfClass(MagicTable.SELECTED_ROW_CLASS, () => {
            res++
        })

        return res;
    }

    findBySelectedId(id, row){
        if (this.getSelectedCount() < 2){
            return row;
        } else {
            return this.#root.eachOfClass(MagicTable.SELECTED_ROW_CLASS, (row)=>{
                if (_e(row.getAttribute(MagicTable.SELECTED_ATTR), id)){
                    return row;
                }
            })
        }
    }

    getSelectedIds(idIfNull) {
        let res = [];
        this.#root.eachOfClass(MagicTable.SELECTED_ROW_CLASS, (x) => {
            res.push(x.getData('selected'))
        }, false, true)

        if (_e(res.length, 0)){
            res.push(idIfNull);
        }
        return res;
    }

    #currentRow;

    #dataConverter;
    #root = h.tag("table").cl("table magic");
    #eventListener;


    #sortable = [];
    #searchable = [];
    #editable = [];


    getParent() {
        return this.#magicTable;
    }

    getRoot() {
        return this.#root;
    }


    getDataConverter() {
        return this.#dataConverter;
    }

    createRow(header = false, item = null) {
        this.#currentRow = new MagicTableRow(this, header, item);
        this.callEventListener(new TableEvent(TableEvent.TABLE_ROW_CREATED, this.#currentRow, item, header));
        return this.#currentRow;
    }


    /**
     *
     * @param event Always TableEvent
     */
    callEventListener(event) {
        if (this.#eventListener != undefined) {
          return  this.#eventListener(event);
        }
    }

    get() {
        return this.#root.get();
    }

    rebuildHeaders() {
        this.#root.text(null);
        this.drawHeader(this.#where, this.#cellType, this.#dataConverter);
    }

    getWhere(){
        return this.#where
    }

    #where;
    #cellType;
    #rowType;

    drawHeader(where, cellType, dataConverter = null) {
        this.#where = where;
        this.#cellType = cellType;
        let data = this.#magicTable.getLastData().fields;
        this.#dataConverter = dataConverter;
        this.#root.text(null);
        this.#root.appendTo(where);
        this.#root.get().remove();


        let headers = this.#magicTable.getOrdering()?.getHeaders();

        if (headers != null) {
            data = headers;
        }

        this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CREATION_STARTED, this, data, null, this.#magicTable))
        this.createRow(true);
        let _this = this;
        for (let i = 0; i < data.length; i++) {
            let item = data[i];

            let cell
            if (item.visible) {
                this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CREATION_STARTED, this, data, null, this.#magicTable))
                if(item.customized){
                  cell = this.#currentRow.createCell('cell_header', item.translation, true);
                } else {
                  if(_e(item.field, "additionalValues")) continue;
                  cell = this.#currentRow.createCell('cell_header', _.$(item.translation, false), true);
                }
                this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CREATION_FINISHED, item, cell, null, this.#magicTable))
            }
            // create sortImage in HeaderList, call a function from MagicSort. Function give us the same img as the sortForm 
            if (item.sortable && !item.actions?.length && cell) {
                let hCell = cell.get();
                let sortImage = this.#magicTable.getSort().buildImgForTable(item.field);
                let sortImageAction = sortImage.click(function (e) {
                    
                    _this.setFieldOrder(e)
                }).get();
                cell.getThContent().add(sortImageAction);
            }

        }
        this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CREATION_FINISHED, this, data, null, this.#magicTable))


        this.#root.appendTo(where);


        return this;

    }

    setFieldOrder(e) {
        this.#magicTable.getSort().setFieldOrder(e, true);
    }


    drawValues(data, list, dataConverter = null, eventListener = null) {
        let _this = this
        if (dataConverter == null) {
            dataConverter = MagicTable.defaultConverter;
        }
        let headers = this.#magicTable.getOrdering()?.getHeaders();

        let visibleFields;
        if (headers != null) {
            visibleFields = headers;
        } else {
            visibleFields = this.#magicTable.getLastData().fields;
        }


        if (utils.e(data.content.length, 0)) return;
        
        let favoriteEnabled = Favorite.enabled();

        utils.each(data.content, (item) => {
            this.createRow(false, item).get().click((el, e) => {
                _this.selectRow(e.currentTarget, item.id);
            });


            utils.each(visibleFields, (field) => {
                if (utils.isFalse(field.visible)) {
                    return utils.CONTINUE;
                }
        
                let cell;
                if (!utils.isEmpty(field.actions)) {
                    this.#currentRow.getMenuOptions().addActions(field.actions).setListner(this.startActions);
                    let divActions = h.div('actions');
                    let content = h.tag("span").cl("actions_cell");
                    let imgContent = h.img("dots.png").cl("actions_cell_dots").wh(16).click(function () {
                        content.get().classList.toggle('hidden');
                        dom.linkTo(content.get(), imgContent.get());
                    }).appendTo(divActions);

                    let arr = [];

                    utils.each(field.actions, (action) => {
                        let act = h.from(this.drawAction(action.name, content, list, item['id'], this, item));
                        arr.push(act);
                        content.add(act);
                    });

                    let hasCustomImpl = utils.each(arr, (x) => {
                        if (x.ccl("custom_action")) return true;
                    });

                    if (!hasCustomImpl) {
                        content.cl('hidden').appendTo(divActions);
                    }

                    
                    cell = hasCustomImpl ? this.#currentRow.createCell('cell_header', content) : this.#currentRow.createCell('cell_header', divActions);
                    this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CELL_CREATED, cell, item, field, this.#magicTable))


                } else {
                    let value = dataConverter(field, item[field.field], item, this, null, null, favoriteEnabled);
                    cell = this.#currentRow.createCell('cell_header', value).getParent();
                    this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CELL_CREATED, cell.get().last(), item, field, this.#magicTable, {field: field.field, row: this.#currentRow}))
                }

            });

            utils.each(this.#magicTable.getLastData().fields, (field) => {
                if (utils.isFalse(field.visible)) {
         
                    if (!utils.isEmpty(field.actions)) {
                        this.#currentRow.getMenuOptions().addActions(field.actions).setListner(this.startActions);
                    }

                    return utils.CONTINUE;
                }
                
            })
        });
    }

    selectRow(el, id) {

      if(!this.getParent().getOptions().getNeedMultiselect()){
        this.#root.eachOfClass(MagicTable.SELECTED_ROW_CLASS, (item) => {
          if (item && item.ccl(MagicTable.SELECTED_ROW_CLASS) && !_e(el, item.get())) {
            item.rcl(MagicTable.SELECTED_ROW_CLASS)
            item.attr(MagicTable.SELECTED_ATTR, null)
          }
        }, false, true)
      }

        el = h.from(el);
        if (el.ccl(MagicTable.SELECTED_ROW_CLASS)) {
            el.rcl(MagicTable.SELECTED_ROW_CLASS)
            el.attr(MagicTable.SELECTED_ATTR, null)
        } else {
            el.cl(MagicTable.SELECTED_ROW_CLASS);
            el.attr(MagicTable.SELECTED_ATTR, id);

            this.callEventListener(new TableEvent(MagicTable.SELECTED_ROW_CLASS, el, id, this, this.#magicTable))
        }

    }

    deleteById(event, table, list, id) {
        let opts = new MagicPromptOptions("global.prompt.allow_delete_title", "global.prompt.change_group_text.allow_delete_text");
        opts.applyClick(MagicPromptButton.OK_LABEL, () => {
            opts.closeSelf();
            let row = event.parent.parent().findBySelectedId(id, event.parent.get());
            table.deleteRowByContextMenu(row, list, id)
            opts.markNoRepeat()
        }).setIsAction(MagicPromptButton.OK_LABEL, true)
            .applyClick(MagicPromptButton.CANCEL_LABEL, () => opts.closeSelf()).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
            .allowNoRepeat("global.prompt_DELETE").newPrompt();

    }

    startActions(event) {
      
        let action = event.eventType;
        let id = event.id;
        let list = event.parent.parent().getParent();
        let table = this.getParent().parent();


        if (!_.isNull(list.getOptions()) && !_.isNull(list.getOptions().getContextMenuListener())) {
            if (list.getOptions().getContextMenuListener()(event)) {
                return;
            }
        }

        if (_e(action, 'edit')) {
            let magicFormOptions = new MagicFormOptions(list.getOptions().getDataType(), undefined, list);
            magicFormOptions.setFormListener(list.getOptions().getFormListener());
            magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
            MagicForm.Instance(list.getOptions().getDataType(), magicFormOptions).load(id);
            return;
        }

        if ((_e(action, "copy"))) {
            let copy = true;
            let magicFormOptions = new MagicFormOptions(list.getOptions().getDataType(), undefined, list);
            magicFormOptions.setFormListener(list.getOptions().getFormListener());
            magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
            MagicForm.Instance(list.getOptions().getDataType(), magicFormOptions).load(id, copy);
            return;
        }


        if (_e(action, 'delete')) {

            if (event.parent.parent().getSelectedCount() >= 2) {
                _.each(event.parent.parent().getSelectedIds(), (id) => {
                    event.parent.parent().deleteById(event, table, list, id)
                })
            } else {
                event.parent.parent().deleteById(event, table, list, id)
            }
        }

        if (_e(action, 'details')) {
            showDetails(id);
        }

        if (utils.e(action, 'import')) {
            if (e.getAttribute("disabled") != undefined) {
                h.message("global.messages.import.in_process");
            }

            let progress = new Loading(list.getGrid());
            progress.show(img);

            Executor.runPost(list.getRootPath() + "/import/", function () {
                progress.hide();
                dom.toast(_.$("global.messages.success"));
                e.removeAttribute("disabled");
                list.drawValues()

            }, function (e) {
                Executor.errorFunction(e);
                progress.hide();
                e.removeAttribute("disabled");
            });
            return;

        }


    }

    /**
     * Here only generating actions for the row
     * @param action - name of the action
     * @param cell - a place where the action will be displayed
     * @param rowAction - true if row (//todo to remove it)
     * @param id - id of the line
     * @param data - the whole row of data
     * @returns {HTMLSpanElement|HTMLImageElement|*|h.h}
     */

    drawAction(action, cell, list, id, magicTable, data) {

        let res = null;

        let _this = this;

        try {
            res = drawCustomAction(action, id, list, data)
        } catch (ex) {
            log.error(ex)

        }

        if (res != null) {
            return h.from(res).cl('custom_action');
        }

        if (_e(action.name, 'edit')) {

            return h.img("edit.png").wh(16).cl(action).click(() => {
                let magicFormOptions = new MagicFormOptions(list.getOptions().getDataType(), undefined, _this.getParent());
                magicFormOptions.setFormListener(list.getOptions().getFormListener());
                magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
                MagicForm.Instance(list.getOptions().getDataType(), magicFormOptions).load(id);
            });

        }

        if (_e(action.name, "copy")) {

            return h.img("copy.png").wh(16).cl(action).click(function () {
                let copy = true
                let magicFormOptions = new MagicFormOptions(list.getOptions().getDataType(), undefined, this);
                magicFormOptions.setFormListener(list.getOptions().getFormListener());
                magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
                MagicForm.Instance(list.getOptions().getDataType(), magicFormOptions).load(id, copy);
            });

        }

        if (_e(action.name, "visible")) {

            return h.img("visibility.png").wh(16).cl(action);

        }

        if (_e(action.name, 'delete')) {
            let _this = this;
            return h.img("rm.png").wh(16).click(function (e) {

                let deleteRule = '____user_delete_objects_rule';
                let opts = new MagicPromptOptions("global.prompt.allow_delete_title", "global.prompt.change_group_text.allow_delete_text");
                opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                    opts.closeSelf();
                    _this.deleteRow(e, list, id);

                }).setIsAction(MagicPromptButton.OK_LABEL, true)
                    .applyClick(MagicPromptButton.CANCEL_LABEL, () => opts.closeSelf()).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                    .allowNoRepeat("global.prompt_DELETE").newPrompt();


            });
        }
        if (_e(action.name, 'details')) {
            return h.img("details.png").cl(action).wh(16).click(function () {
                showDetails(id);
            }).get();
        }

        if (utils.e(action, 'import')) {

            return h.img("import.png").cl(action).wh(16).click(function (e) {
                if (e.getAttribute("disabled") != undefined) {
                    h.message("global.messages.import.in_process");
                }

                let progress = new Loading(list.getGrid());
                progress.show(img);

                Executor.runPost(list.getRootPath() + "/import/", function () {
                    progress.hide();
                    dom.toast(_.$("global.messages.success"));
                    e.removeAttribute("disabled");
                    list.drawValues()

                }, function (e) {
                    Executor.errorFunction(e);
                    progress.hide();
                    e.removeAttribute("disabled");
                })
            }).get();

        }

        if (res == null) {
            return h.span("info.error.list.actions", "global.list.actions.not.implemented." + action);
        }


    }

    deleteRow(e, list, id) {
        e = h.from(e);
        let path = list.getOptions().getRoot("/delete/" + id)

        Executor.run(path, "DELETE", function () {

            while (!e.ccl('data_row') && !e.ccl('table')) {
                e = e.parent();
            }

            if (e.ccl('data_row')) {
                e.remove();
            } else if (e.ccl('table')) {
                log.trace('No data_row in table');
            }
            ;

            this.successToast(id);

            this.callEventListener(new TableEvent(MagicTable.DELETED_ROW, this, data, null, this.#magicTable))
        });
    }

    successToast(id = null){

      var allowToast = this.callEventListener(new TableEvent(TableEvent.TABLE_TOAST_REQUESTED, this, id, null, this.#magicTable))
  
      if(!_e(allowToast, false)) {
        dom.toast( _.$("global.success"), ToastType.SUCCESS);
      }
    }

    deleteRowByContextMenu(row, list, id) { 
        let path = list.getOptions().getRoot("/delete/" + id)

        Executor.run(path, "DELETE", (data) => {

            row.remove()

            this.successToast(id);

            this.callEventListener(new TableEvent(MagicTable.DELETED_ROW, this, id, null, this.#magicTable))
        });
    }

    static defaultConverter(header, what, data, table, dataType = null, onclick = null, favoriteEnabled) {

        let res = null;
        try {

            if (utils.notNull(table.getParent().getOptions().getColumnRenderer())) {
                let render = table.getParent().getOptions().getColumnRenderer();
                res = render(header, what, data, table, this, favoriteEnabled)
            }

            if (utils.notNull(drawCustomField) && utils.isNull(res)) {
                res = drawCustomField(header, what, data, table, this, favoriteEnabled)
            }

        } catch (ex) {
            log.error(ex)
        }

        if (!utils.isNull(res)) {
            return res;
        }

        if (utils.e(header.field, "id")) {
            if (favoriteEnabled) {

                let heart = h.span(Favorite.FAVORITE_CLASS + "_holder", "", false).click(function (e) {

                    Favorite.markElementAsFavorite(e, data, table.getParent().getFavoriteType())
                })
                Favorite.markIfElementFavorite(heart, data, table.getParent().getFavoriteType())

                let idWithFavarite = h.div("id_with_favorite")
                    .add(heart)
                    .add(h.span("id_content", what, false));

                return idWithFavarite
            }
        }

        let type = header == null ? dataType : (header.type == undefined ? header.getType() : header.type);

        //here is going something interesting. Something like a hell

        if (utils.e(type, "checkbox")) {

            if (!utils.isFalse(what)) {
                return h.img("true.png").wh(16);
            } else if (what === false || what === "false" || what === "False") {
                return h.img("false.png").wh(16);
            }
        } else if (utils.e(type, "object_chooser")) {
            let val = h.tag("span").cl("value_span");
            let res = "";

            if (what != null && !utils.isEmpty(what.value)) {
                h.a("#").cl("object_link").text(what.value, false).click(onclick == null ? function () {
                    h.magicView(what.key, header.remoteType, false, header.searchName);
                    return false;
                } : onclick, false).appendTo(val);
            } else {
                val.text(what == null ? what : what.value);
            }

            //we add icon here if object contains an Icon (or download file link)
            if (what != null && what.additional != null && what.additional.length > 0) {
                res = h.img(what.additional, 32).cl("image_in_details").click(function (e) {

                  if(!_.isEmpty(h.from(e).getA("have-full-path"))) {
                    utils.showFullImage(_.getFullImgPath(e.src));

                    return;
                  }

                    utils.showFullImage(e.src)
                }).wh(16);
                val.get().prepend(res.get());
            }
            if (what == null) {
                val.text(null);
            }
            return val;
        }

        if (utils.e(type, "file")) {
  
            let img = "";
            if (utils.isEmpty(what)) {
                return "&nbsp;"
            }

            if (utils.isImage(what)) {

                img = h.img(what, Environment.defaultIconSize).wh(24).click(function (e, ex) {

                  if(!_.isEmpty(h.from(e).getA("have-full-path"))) {
                    utils.showFullMedia(_.getFullImgPath(e.src));

                    return;
                  }

                  utils.showFullMedia(e.src);
                })
            } else {
                img = document.createElement("a");
                img.href = what;
                img.target = "__blank";
                img.innerHTML = _.$("global.pages.download");
            }

            return h.from(img);
        }


        if (utils.e(type, "files")) {
            let div = h.div('icons')
            what.forEach(e => {
                let img = "";
                if (utils.isEmpty(e)) {
                    return "&nbsp;"
                }

                if (utils.isImage(e)) {
                    img = h.img(e).wh(24).appendTo(div)
                } else {
                    img = document.createElement("a");
                    img.href = what;
                    img.target = "__blank";
                    img.innerHTML = _.$("global.pages.download");
                }
            })
            div.click(function () {
                _.showFullMedia(what[0], what)

            })

            return h.from(div);
        }

        if (what == null || what.value == undefined) {
            return what;
        }
        return what.value;
    }

}

class MagicTableRow extends h {
    #parent;
    #root;
    #isHeader;
    #menuOptions;
    #menu;
    #cells = []
    #data;

    constructor(table, isHeader, info = null) {
        super();
        this.#parent = table;
        this.#root = MagicTableRow.tag("tr").cl("tr").cl("data_row").appendTo(table);
        this.#isHeader = isHeader;
        if (!isHeader) {
            this.#data = info;
            this.#menuOptions = new MagicMenuOptions(this.#root, info, this, "data_row");
            var custom = this.#parent.getParent().getOptions().getContextMenuToGroupTransformer();
            if(custom){
                this.#menuOptions.setContextMenuToGroupTransformer(custom);
            };
            this.#menu = new MagicMenu(this.#menuOptions, info);
        } else {

        }
    }

    getMenuOptions() {
        return this.#menuOptions
    }

    getData(){
      return this.#data;
    }

    parent() {
        return this.#parent;
    }

    getCells(){
      return this.#cells;
    }

    get() {
        return this.#root;
    }

    getDataConverter() {
        return this.#parent.getDataConverter();
    }

    createCell(type, value) {

        let cell = new MagicTableCell(this, this.#isHeader).draw(type, value);
        this.#cells.push(cell)
        return cell;
    }


}

class MagicTableCell extends h {
    #parent;

    #root;

    #isHeader;

    #resize;

    #thContent;

    constructor(row, isHeader = false) {
        super();
        this.#parent = row;
        this.#isHeader = isHeader;
        let tag = isHeader ? "th" : "td";
        this.#root = h.tag(tag);

        if (isHeader) {
            this.#root.cl("th").cl("data_header");
        } else {
            this.#root.cl("td").cl("data_cell");
        }
    }

    get() {
        return this.#root;
    }

    getParent() {
        return this.#parent;
    }

    getThContent() {
        return this.#thContent
    }

    draw(type, value) {
       

        if (this.#isHeader) {

            let litType = this.#parent.parent().getParent().getDataType(); 
            let resizeField = litType + "_" + value + '_resize'
            this.#resize = h.div('data_header__resize').setData("resize", resizeField).appendTo(this.#root);
            this.#thContent = h.div('data_header__content').appendTo(this.#root).add(h.span('data_header__span').text(value, false))
            cache.isPresent(this.#resize.getData('resize')) ? this.#thContent.get().style.width = cache.get(this.#resize.getData('resize')):null
            this.#root.cl(type).appendTo(this.#parent.get());
            this.addResizeEvent(resizeField);
            return this;
        }

        let dataConverter = this.#parent.getDataConverter();
        if (dataConverter) {
            value = dataConverter(value, value);
        }
        if (value != null && value.isH) {
            this.#root.add(value).appendTo(this.#parent.get());
        } else {
            this.#root.cl(type).text(value, false).appendTo(this.#parent.get());
        }

        return this;
    }

    parent() {
        return this.#parent;
    }

    addResizeEvent(resizeField) {
        this.startX
        this.startWidth;
        let tEvent
        this.#resize.handleEvent('mousedown', (event) => {
            tEvent = event
            this.startX = event.clientX;
            this.startWidth = this.#thContent.get().offsetWidth;
            document.addEventListener('mousemove', this.myFunc = (ev)=>{this.mousemoveEvent(ev, this)})
        })
        document.addEventListener('mouseup', (e) => {
            document.removeEventListener('mousemove', this.myFunc)
            if(_.e(resizeField, h.from(tEvent?.target)?.getData('resize'))){
               cache.add(h.from(tEvent.target).getData("resize"), this.#thContent.get().style.width) 
            }    
        });
    }

    mousemoveEvent(event, this_){
        const width = this_.startWidth + event.clientX - this_.startX;
        this_.#thContent.width(width);

    }
}

