class MagicPrompt{

    #windowPromptWrapper;
    #checkbox;
    #action;
    #onAccept;
    #showPromptAgainKey;
// e, list, id, magicTable,
    constructor( action, onAccept, deleteRule){
      this.#checkbox = false;  

      this.#action = action;
      this.#onAccept = onAccept;
      this.#showPromptAgainKey = deleteRule
    }

    deleteOrPromptDelete(){
        
       if(sessionStorage.getItem(this.#showPromptAgainKey)){
        this.#onAccept()
       }else{
        this.drawPrompt()
       }
    }

    drawPrompt(){
        let _this = this
        let body = document.querySelector('body');
        this.#windowPromptWrapper = h.div('prompt_window_bgd').appendTo(body);
        let windowPrompt = h.div('prompt_window').id('prompt_window').appendTo(this.#windowPromptWrapper);

        h.img('close.png.png').click(function (e) {
            _this.closeWindow();
        }).wh(24).appendTo(windowPrompt);

        h.div('prompt_window_title').text(this.#action).appendTo(windowPrompt);

        let selectorOptions = new MagicTrueFalseSelectorOptions(
            _.$("global.remember"), sessionStorage.getItem(this.#showPromptAgainKey)
        ).setComponentListener((event)=> {
            if (_e(event.type, MagicTrueFalseSelectorEvent.VALUE_CHANGED )){
                _this.#checkbox = event.component.getValue();
            }
        })
        new MagicTrueFalseSelector().draw(windowPrompt);



        _this.#checkbox = sessionStorage.getItem(this.#showPromptAgainKey);

        let footer = h.div("actions_delete_confirm").appendTo(windowPrompt);

        h.input('button').text('global.messages.submit').click(function (e) {
            _this.confirmChoice();
        }).appendTo(footer);

        h.input('button').text('global.messages.cancel').click(function (e) {
            _this.closeWindow();
        }).appendTo(footer);
    }

    closeWindow(){
        this.#windowPromptWrapper.get().remove();
    }

    confirmChoice(){
        if (this.#checkbox){
            sessionStorage.setItem(this.#showPromptAgainKey, true);
        }
        if (this.#checkbox == false && sessionStorage.getItem(this.#showPromptAgainKey)){
            sessionStorage.removeItem(this.#showPromptAgainKey)
        }
        this.#onAccept();
        this.closeWindow();
    }
}