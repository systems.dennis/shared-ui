
var current_drug_event_on_drag = [];
class Drug {
    #at;
    #onDrop;

    allowOnly;
    #id;

    constructor(id, at, onDrop, allowOnly) {
        this.#at = at;
        this.#onDrop = onDrop;

        this.allowOnly = allowOnly;
        this.#id = id;
        this.#enableDrug();
    }

    getId() {
        return this.#id;
    }

    #enableDrug() {
        let elements = this.#at.childNodes;

        current_drug_event_on_drag.push({"id": this.#id, "onDrop": this.#onDrop})
        let _this = this;
        elements.forEach(item => {
            item.setAttribute('draggable', true)
            item.addEventListener('dragstart', _this.dragStart)
            item.addEventListener('drop', _this.dropped)
            item.addEventListener('dragenter', _this.cancelDefault)
            item.addEventListener('dragover', _this.cancelDefault)
            item.setAttribute("data-allowed", this.allowOnly);
            item.setAttribute("data-drag-id", this.getId());
        })
    }


    dragStart(e) {
        current_drug_event_limits = this.getAttribute("data-allowed");
        let item = Drug.findRequiredParent(e.target);
        if (item == null) {
            log.trace("wrong drag")
        }
        let index = dom.getElementIndex(item);

        e.dataTransfer.setData('text/plain', index)
    }

    dropped(e) {
        e.preventDefault()
        e.stopPropagation()

        // get new and old index
        let oldIndex = e.dataTransfer.getData('text/plain')
        let target = h.from(Drug.findRequiredParent(e.target));
        var parent = target.parent();

        if (_.isNull(target)) {
            log.trace("Wrong drop");
            return;
        }

        let newIndex =  dom.getElementIndex(target);
        
        if(oldIndex == newIndex){
            log.trace("drop itself");
            return;
        }

        // remove dropped items at old place
        let dropped = parent.child(oldIndex, true).remove();

        // insert the dropped items at new place
        if (newIndex < oldIndex) {
          parent.addBefore( dropped, target)
        } else {
          if(target.next()) {
            parent.addBefore( dropped , target.next(true))
          } else {
            parent.add(dropped)
          }
        }
        

        for (let i = 0; i < current_drug_event_on_drag.length; i++) {
            if (current_drug_event_on_drag[i].id == target.getA('data-drag-id')) {
                current_drug_event_on_drag[i].onDrop(oldIndex, newIndex);
            }
        }
        //   this.#onDrop(newIndex, oldIndex);


    }

    cancelDefault(e) {
        e.preventDefault()
        e.stopPropagation();
        return false
    }

    static findRequiredParent(target) {

        if (!target.classList.contains(current_drug_event_limits)) {
            if (target.parentElement == undefined) return undefined;
            return this.findRequiredParent(target.parentElement);
        } else {
            return target
        }
    }


}