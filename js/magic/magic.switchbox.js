class MagicSwitchOptions{

    static SWITCH = 'switch'
    static SWITCH_ON = 'switch_on'
    static SWITCH_OFF = 'switch_off'
    static CREATE_SWITCH_DIV = 'create_switch'

    #listener;
    #defaultValue;
    #where;
    #attribute;
    #text;
    #magicSwitch

    constructor(defaultValue = false, where, text, attribute, listener = null){
        this.#defaultValue = defaultValue;
        this.#listener = listener;
        this.#text = text;
        this.#where = where;
        this.#attribute = attribute;
    }

    getWhere(){
        return this.#where;
    }

    getDefValue(){
        return this.#defaultValue;
    }

    getAttribute(){
        return this.#attribute;
    }

    getText(){
        return this.#text;
    }

    setText(text){
        this.#text = text
    }

    setListener(listener){
        this.#listener = listener;
    }

    setValue(value){
        this.#magicSwitch.setValue(value);
    }

    draw(){
       this.#magicSwitch = new MagicSwitch(this);
    }

    createEvent(eventName, element, value){
        if(_.notNull(this.#listener)){
            this.#listener(new MagicSwitchEvent(eventName, element, value));
        }
    }

}



class MagicSwitch{

    #options

    constructor(options){
        this.#options = options
        this.build()
    }

    build(){

        this.switchDiv = h.div(MagicSwitchOptions.SWITCH)
            .setData(MagicSwitchOptions.SWITCH, this.#options.getAttribute())
            .appendTo(this.#options.getWhere())
        this.switch = h.div('background_switch')
            .add(h.div('toggle_switch'))
            .click(()=>{
                this.toggleSwitch(this.switch)
            })
            .appendTo(this.switchDiv)

        h.div('switch_text').
            text(this.#options.getText())
            .appendTo(this.switchDiv)

        this.#options.createEvent(MagicSwitchOptions.CREATE_SWITCH_DIV, this.switchDiv);

        this.setValue(this.#options.getDefValue())

    }

    toggleSwitch(e){
        if(e.ccl(MagicSwitchOptions.SWITCH_ON)){
            e.rcl(MagicSwitchOptions.SWITCH_ON);
            e.cl(MagicSwitchOptions.SWITCH_OFF)
            this.#options.createEvent(MagicSwitchOptions.SWITCH_OFF, e.parent(), false )
        }else{
            e.cl(MagicSwitchOptions.SWITCH_ON);
            e.rcl(MagicSwitchOptions.SWITCH_OFF)
            this.#options.createEvent(MagicSwitchOptions.SWITCH_ON, e.parent(), true )
        }
        
    }

    setValue(value){
        if(value){
            this.switch.rcl(MagicSwitchOptions.SWITCH_OFF)
            this.switch.cl(MagicSwitchOptions.SWITCH_ON)
        }else{
            this.switch.rcl(MagicSwitchOptions.SWITCH_ON)
            this.switch.cl(MagicSwitchOptions.SWITCH_OFF)
        }
    }
    
}