class log{
    static LOG_LEVEL_DEBUG = 2;
    static LOG_LEVEL_INFO = 1;
    static LOG_LEVEL_ERROR = 0;
    static LOG_LEVEL_TRACE = 3;
    static LOG_LEVEL_NONE = -1;
    static error (val, toWrap = true ){
        if (Environment.LOG_LEVEL >= log.LOG_LEVEL_ERROR){
            console.error(toWrap ? {val} : val );
        }

    }
    static debug (val, toWrap = true){
        if (Environment.LOG_LEVEL >= this.LOG_LEVEL_DEBUG){
            console.log(toWrap ? {val} : val );
        }
    }

    static trace(val, toWrap = true){
        if (Environment.LOG_LEVEL >= this.LOG_LEVEL_TRACE){
            console.log(toWrap ? val : val );
        }
    }


    static info(val,  toWrap = true){
        if (Environment.LOG_LEVEL === this.LOG_LEVEL_INFO){
            console.log(toWrap ? val : val );
        }
    }
    static promisedDebug(val,  toWrap = true){
        if (Environment.test){
            console.log( val() );
        }
    }
}
class utils {

    static CONTINUE;

    static escapeDollar(x) {
        if (_.isNull(x)) return '';
        return x.replaceAll("#", '').replaceAll("{", "").replaceAll("}", "");
    }

    static transFormAttributeValue(text, def = null) {
        if (text.startsWith("!{")) {
            text = text.replaceAll("!{", "").replaceAll("}", "");

            if (text.indexOf(":") > 0) {
                text = text.split(":");

                def = text [1];
                text = text[0];
            }

            if (def == null) return sessionStorage.getItem("___" + text);

            return utils.isEmpty(sessionStorage.getItem("___" + text)) ? def : sessionStorage.getItem("___" + text);
        }
        return text;
    }

    static getLocalDatefromMillisecond(milisec, toFormat = true) {
        if (Environment.dateFormat && toFormat) {
            return _.rebuildDataToUserSettings(milisec)
        }
        return new Date(milisec).toLocaleDateString();

    }

    static getLocalTimefromMillisecond(milisec) {
        return new Date(milisec).toLocaleTimeString();
    }

    static rebuildDataToUserSettings(data) {
        var mySimpleDateFormatter = new simpleDateFormat(Environment.dateFormat);
        var myDate = new Date(data);
        return mySimpleDateFormatter.format(myDate);
    }

    static getUTFmiliseconds(date) {
        let milisecond = new Date(date.split(".").reverse().join(".")).getTime();
        return milisecond
    }

    static isFalse(obj) {
        return obj == false || obj == null || obj == "False" || obj == "false";
    }

    static getFullImgPath(path) {

        let newPath = path.split("/");

        newPath.pop();

        return newPath.join("/");
    }

    static isTrue(obj) {
        return !utils.isFalse(obj);
    }

    static isAnyNull(...obj) {
        if (_.isNull(obj)) {
            return true;
        }
        var isNull = _.each(obj, () => {
            if (_.isNull(obj)) return true
        })
        return this.isTrue(isNull);
    }

    static isNull(obj) {
        return obj == undefined || obj == 'null';
    }

    static toInt(obj) {
        return utils.isNull(obj) ? undefined : parseInt(obj);
    }

    static notNull(obj) {

        return !this.isNull(obj)
    }

    static initMCE(textArea) {
        return SUNEDITOR.create(textArea.get(), {
                buttonList: [
                    ['undo', 'redo'],
                    ['font', 'fontSize', 'formatBlock'],
                    ['paragraphStyle', 'blockquote'],
                    ['bold', 'underline', 'italic', 'strike', 'subscript', 'superscript'],
                    ['fontColor', 'hiliteColor', 'textStyle'],
                    ['removeFormat'],
                    ['outdent', 'indent'],
                    ['align', 'horizontalRule', 'list', 'lineHeight'],
                    ['table', 'link', 'image', 'audio'],
                    ['fullScreen', 'showBlocks', 'codeView'],
                    ['preview', 'print'],
                ],
                imageAccept: '.png, .jpg, .svg, .gif, .jpeg',
                imageUploadUrl: Environment.fileStorageApi + '/upload/' + Environment.authScope,
                imageUploadHeader: {
                    'Authorization': 'Bearer ' + sessionStorage.getItem('___TOKEN___'),
                    'AUTH_SCOPE': Environment.authScope
                }
            }
        );
    }


    isNull() {
        return this.get().value == null || this.get().value.trim().length == 0;
    }

    static isEmpty(obj, countEmpty = 0) {
        return (
            obj == null || obj == "" || (obj.length != undefined && obj.length < countEmpty)
        );
    }

    static deepClone(obj) {
        return JSON.parse(JSON.stringify(obj))
    }

    static isObjectEmpty = (object, travers = false) => {


        if (travers) {
            let isEmpty = true;

            for (let key in object) {
                if (!_.isObject(object[key])) {
                    if (_.notNull(object[key])) {
                        return false;
                    }
                }

                if (_.isObject(object[key]) && Object.values(object[key]).length) {
                    isEmpty = false;
                    return;
                }
            }
            return isEmpty;
        } else

            return !_.isEmpty(object) && Object.keys(object).length === 0
    }

    static isObject = (val) => {
        return typeof val === 'object' &&
            !Array.isArray(val) &&
            !!val
    }


    static isElement(obj) {
        try {
            //Using W3 DOM2 (works for FF, Opera and Chrome)
            return obj instanceof HTMLElement;
        } catch (e) {
            //Browsers not supporting W3 DOM2 don't have HTMLElement and
            //an exception is thrown and we end up here. Testing some
            //properties that all elements have (works on IE7)
            return (typeof obj === "object") &&
                (obj.nodeType === 1) && (typeof obj.style === "object") &&
                (typeof obj.ownerDocument === "object");
        }
    }


    static isImage(path) {
        if (utils.isEmpty(path)) {
            return false;
        }
        if (
            utils.endsWith(path, ".jpg") ||
            utils.endsWith(path, ".png") ||
            utils.endsWith(path, "jpeg") ||
            utils.endsWith(path, ".gif") ||
            utils.endsWith(path, ".svg") ||
            utils.endsWith(path, ".svg+xml")
        ) {
            return true;
        }

        let res = false;
        if (utils.startsWith(path, Environment.fileStorageApi)) {
            //here we need to check version from the filesever, cause fileserver doesn't contain information about type (no extension)
            Executor.runGet(
                Environment.fileStorageApi + "/info?path=" + path,
                function (data) {
                    //types comes in mime types, so we need to replace image / to be able to run this method again
                    res = utils.isImage("." + utils.removeFromString(data.type, "image/"));
                },
                false
            );
        }

        return res;
    }

    static last(arr) {
        return arr[arr.length - 1];
    }

    static zeroTimeOut(f) {
        setTimeout(f, 0);
    }

    static capture = async (callback) => {
        const canvas = document.createElement("canvas");
        const context = canvas.getContext("2d");
        const video = document.createElement("video");

        try {
            const captureStream = await navigator.mediaDevices.getDisplayMedia();
            video.srcObject = captureStream;
            await new Promise((resolve) => {
                video.onloadedmetadata = resolve;
            });

            await new Promise((resolve) => setTimeout(resolve, 1000)); // Esperar 1 segundo
            video.play();

            const displayWidth = window.innerWidth;
            const displayHeight = window.innerHeight;
            const screenLeft = window.screenLeft;
            const screenTop = window.screenTop;
            const windowLeft = window.outerWidth - window.innerWidth - screenLeft;
            const windowTop = window.outerHeight - window.innerHeight - screenTop;

            canvas.width = displayWidth;
            canvas.height = displayHeight;
            context.drawImage(video, windowLeft, windowTop, displayWidth, displayHeight, 0, 0, displayWidth, displayHeight);

            callback(canvas);
            captureStream.getTracks().forEach(track => track.stop());

        } catch (err) {
            log.error(err);
        }
    };


    static removeFromString(str, strToReplace) {
        if (strToReplace == null || str == null) {
            return str;
        }
        return str.replaceAll(strToReplace, "");
    }

    static endsWith(path, what) {
        if (utils.isEmpty(what)) {
            return false;
        }
        return path != undefined && path.toLowerCase().endsWith(what.toLowerCase());
    }

    static startsWith(path, what) {
        if (utils.isEmpty(what) || !_e(typeof path, 'string')) {
            return false;
        }
        return (
            path != undefined && path.toLowerCase().startsWith(what.toLowerCase())
        );
    }

    static e(obj, obj1, isObject = false) {
        if (_.isFunction(obj?._equals)){
            return obj._equals(obj1)
        }
        var res = obj == obj1;
        if (res) return true;

        if (isObject) {
            try {
                return this.isDeepEqual(obj, obj1)
            } catch (e) {
                return false;
            }
        }
        return false
    }

    static isDeepEqual(obj1, obj2) {

        if (obj1 === obj2) {

            return true;

        }

        if (obj1 === null || obj2 === null || typeof obj1 !== 'object' || typeof obj2 !== 'object') {

            return false;

        }


        const keys1 = Object.keys(obj1);

        const keys2 = Object.keys(obj2);


        if (keys1.length !== keys2.length) {

            return false;

        }


        for (let key of keys1) {

            if (!keys2.includes(key) || !utils.isDeepEqual(obj1[key], obj2[key])) {

                return false;

            }

        }

        return true;

    };

    static isOneOf(obj, ...obj1) {
        return _.notNull(obj1) && obj1.indexOf(obj) > -1
    }

    static ne(obj, obj1) {
        return obj != obj1;
    }


    static each(a, f, isWrap = false) {
        if (utils.isNull(a)) {
            return null;
        }
        for (let index = 0; index < a.length; index++) {
            let res
            if (isWrap) {
                res = f(h.from(a[index]), index);
            } else {
                res = f(a[index], index);
            }
            if (utils.notNull(res) && !utils.e(res, utils.CONTINUE)) {
                return res;
            }
        }
        return null
    }

    static map(arr, func) {
        return _.isEmpty(arr) ? [] : arr.map(func);
    }

    static filter(arr, func) {
        return _.isEmpty(arr) ? [] : arr.filter(func);
    }

    static arrayMove(arr, oldIndex, newIndex) {
        if (newIndex >= arr.length) {
            let k = newIndex - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);

        return arr;

    };


    static in(what, ...items) {
        return items.includes(what);
    }

    //strict true - all
    //strict false - at least one
    static containsText(where, strict, [...items]) {

        if (_.isNull(where)) {
            return false
        }

        let contains = _.each(items, (item) => {

            if (!(where + "").includes(item) && strict) {
                return false
            }
            if ((where + "").includes(item) && !strict) {
                return true
            }
        })

        return contains

    }

    static find(array, conditionFunction, def = null) {
        let res = this.each(array, (item) => {
            if (conditionFunction(item)) {
                return item;
            }
        });
        if (_.isEmpty(res)) {
            return def
        } else {
            return res
        }
    }

    static debounce(callback, delay) {
        let timer;

        return (...args) => {
            clearTimeout(timer);
            timer = setTimeout(() => callback(...args), delay);
        }
    }

    static whatToTranslate() {

        let div = h.div("test").get();
        div.style.background = "white";
        let str = "";
        for (let i = 0; i < MagicPage.____UNTRANSLATED.length; i++) {

            str += MagicPage.____UNTRANSLATED[i] + "=<br>";
        }
        div.innerHTML = str;
        document.body.appendChild(div);
    }


    static showFullImage(path) {
        utils.showFullMedia(path);
    }

    static isFunction(func) {
        if (func instanceof Function) {
            return true
        }
        return false
    }

    static isEmptyFile(src) {
        return (src.includes(RemoteFile.DLETED_FILE_ICON));
    }

    static createRandomId(base = "", useRandom = true, useDate = true) {
        if (useRandom) {
            base += '_' + _.getRandom(100, 1000)
        }

        if (useDate) {
            let date = new Date()
            base += '_' + date.getFullYear() + "_" + date.getMonth() + "_" + date.getDate()
        }
        return base
    }

    static getRandom(min = 0, max = 100) {
        return Math.floor(Math.random() * (max - min) + min)
    }

    static showFullMedia(src, images, type = 'image') {
        return new FullMediaView(src, images, type).init();
    }

    static ofNullable(obj, then) {

        if (_.isNull(obj)) {
            return _.isFunction(then) ? then() : then;
        }

        return obj;
    }
    static ofEmptyString(obj, then){
        if (_.isEmpty(obj)) {
            return _.isFunction(then) ? then() : then;
        }

        return obj;
    }

    static ofEmpty(obj, then) {
        if (_.isObject(obj) && _.isObjectEmpty(obj)) {
            return _.isFunction(then) ? then() : then;
        }
        return obj;
    }

    static ob(key, value) {
        let res = new ObjectBuilder();
        if (_.isNull(key)) {
            return res;
        }
        return res.put(key, value);
    }

    static  $(text, placeholders = null){
        return MagicPage.translate(text, placeholders)
    }
}

class FullMediaView {
    #src;
    #images;
    #type;
    #shadowWrapper;
    #fullImgHeader;
    #zoomDiv;
    #info;
    #actionsDiv;
    #fullImgMain;
    #isImagesArray;
    #imageElement
    #scale = 1;
    #fullImg;
    #hideShare = true;
    #shareLinks;
    #previewImages = null
    #previewImagesControlsEnabled = false


    constructor(src, images, type = 'image') {
        this.#src = src;
        this.#images = images;
        this.#type = type;
        this.#isImagesArray = !_.isEmpty(images, 1);
    }

    init = () => {
        if (utils.isEmptyFile(this.#src)) {
            return
        }
        this.draw();
        this.drawImages();
        this.drawSingleFile()

        return this
    }

    enablePreviewControls(sliderOptions){
        this.#previewImagesControlsEnabled = true
        this.drawImagesPreviewControls(sliderOptions)
    }

    drawSingleFile() {
        if (!this.#isImagesArray) {
            this.#imageElement = h.img(this.#src).appendTo(this.#fullImg);
            this.#fullImgMain.addIf(_e(this.#type, "image"), this.#fullImg);

            if (_e(this.#type, "video")) {
                this.#fullImgMain.text(null).add(dom.getVideoElement(this.#src));
            }

            if (_e(this.#type, "audio")) {
                this.#fullImgMain.text(null).add(dom.getAudioElement(src));
            }
        }
    }

    drawImages = () => {
        let src = this.#src;
        if (this.#isImagesArray) {
            this.#fullImg.remove()
            h.div("array_left").click(() => this.swipeImage(-1)).appendTo(this.#fullImgMain);
            this.#fullImg = h.div('full_img').appendTo(this.#fullImgMain).handleEvent("mousemove", this.move)
            this.#imageElement = h.img(src).appendTo(this.#fullImg);
            h.div("array_right").click(() => this.swipeImage(1)).appendTo(this.#fullImgMain);
            MagicPage.modalKeyListeners.push(this.keyListener)
        }
    }

    keyListener = (e) => {
        if (_e(e.keyCode, 37)) this.swipeImage(-1);
        if (_e(e.keyCode, 39)) this.swipeImage(1)
    }

    swipeImage = (action) => {
        let imgIndex = this.#images.indexOf(this.#src)
        if(this.#previewImagesControlsEnabled) this.#previewImages[imgIndex].rcl('_active')

        imgIndex += action;
        if (imgIndex < 0) imgIndex = this.#images.length - 1;
        if (imgIndex > this.#images.length - 1) imgIndex = 0

        let newSrc = this.#images[imgIndex];
        this.#imageElement.src(newSrc);
        this.#src = newSrc;

        if(this.#previewImagesControlsEnabled) this.#previewImages[imgIndex].cl('_active')
    }

    draw = () => {
        this.#shadowWrapper = dom.createModal(dom.REMOVE_TYPE_REMOVE).cl('shadow_wrapper').appendToBody();
        this.#fullImgHeader = h.div('fullImg_header').appendTo(this.#shadowWrapper);
        this.#fullImgMain = h.div('fullImg_main').appendTo(this.#shadowWrapper);
        this.#info = h.div('img_info').appendTo(this.#fullImgHeader);
        this.#zoomDiv = h.div('zoom_div').appendTo(this.#fullImgHeader);
        h.div('zoom_in').appendTo(this.#zoomDiv).click(() => this.zoom(1))
        this.#fullImg = h.div('full_img').appendTo(this.#fullImgMain).handleEvent("mousemove", this.move)
        h.div('zoom_out').appendTo(this.#zoomDiv).click(() => this.zoom(-1))
        this.#actionsDiv = h.div('actions_div').appendTo(this.#fullImgHeader);
        h.div('actions_download').click(this.download).appendTo(this.#actionsDiv);
        h.div('actions_link').appendTo(this.#actionsDiv).click(this.link)
        h.div('actions_share').appendTo(this.#actionsDiv).click(this.share);
        this.#shareLinks = this.createLinks().hide().appendTo(this.#actionsDiv);
        h.div('div_image').cl('close_full_img').click(() => {
            this.#shadowWrapper.remove()
            this.removeKeyListener();
        }).appendTo(this.#actionsDiv);
    }

    drawImagesPreviewControls(sliderOptions) {
        const sliderContainer = h.div('slider_container').prependTo(this.#fullImgMain)
        const sliderWrapper = h.div('slider_wrapper').appendTo(sliderContainer)
        const previewContainer = h.div('side-gallery__images-wrapper')
        this.#previewImages = _.map(this.#images, image => {
             const result = h.img(image, 84).cl('side-gallery__image').click((e) => {
                _.each(this.#previewImages, previewImage => {
                    previewImage.rcl('_active')
                })
                 this.#src = image
                 result.cl('_active')
                 this.#imageElement.src(image);
            }).appendTo(previewContainer)
            return result
        })
        this.#previewImages[0].cl('_active')
        if(this.#images.length > 1) {
            previewContainer.appendTo(sliderWrapper)
            new MagicSlider(sliderWrapper, previewContainer, sliderOptions).build()
        }
    }

    zoom = (action) => {
        if (this.#scale >= 1.5 && _e(action, 1)) return;
        if (this.#scale >= 1.5 && _e(action, -1)) return;
        this.#scale += (action / 10);
        this.#imageElement.style().transform(`scale(${this.#scale})`)
    }

    createLinks() {
        let links = [
            {
                img: "tel-icon.svg",
                social: "telegram"
            },
            {
                img: "twitter-icon.svg",
                social: "twitter"
            },
            {
                img: "viber-icon.svg",
                socical: "viber"
            },
            {
                img: "whatsapp-icon.svg",
                social: "whatsapp"
            },
            {
                img: "linkedin-icon.svg",
                social: "linkedin"
            }
        ];
        let wrapper = h.div();
        _.each(links, (link) => {
            wrapper.add(
                h.img(link.img, Environment.defaultIconSize).wh(32).cl("share-links__link").setData("social", link.social).setData("url", this.#src)
            )
        });
        return wrapper;
    }

    move = (e) => {
    }

    download = () => {
        h.a(this.#src, null, false, true).performClick();
    }

    link = () => {
        try {
            navigator.clipboard.writeText(window.location.href);
            dom.successMessage();
        } catch (error) {
        }
    }

    share = () => {
        if (this.#hideShare) {
            this.#shareLinks.show();
            try {
                share()
            } catch (error) {
                log.error('No share js')
            }
        } else this.#shareLinks.hide();
        this.#hideShare = !this.#hideShare;
    }

    removeKeyListener = () => {
        let index = MagicPage.modalKeyListeners.indexOf(this.keyListener);
        if (_e(index, -1)) MagicPage.modalKeyListeners.splice(index, 1);
    }
}

class ImgRedactor {
    static CL = "img-redactor__";

    canvas;
    modal;
    wrapper;
    canvasWrapp;
    panel;
    rangeInput;
    colorWrapp;
    inputWrapper;
    input;
    acceptInput;

    isInputOpen = false;
    cPushArray = new Array();
    cStep = -1;
    context;
    img;
    color = "black";
    size = 25;
    canvasPosition;
    src;
    resultFunc;
    filename;

    init(src, filename) {
        this.filename = filename;
        this.draw();
        this.loadImg(src);
        this.getCanvasPosition();
        this.colorSelectEvent();
        this.drawToolsPanel();
        this.paintSizeEvent();

        return this;
    }

    draw() {
        this.modal = h.div(ImgRedactor.CL + "fixed-wrapper").appendToBody();
        this.wrapper = h
            .div(ImgRedactor.CL + "wrapper")
            .cl("modal")
            .appendTo(this.modal);

        h.div(ImgRedactor.CL + "close-btn")
            .appendTo(this.wrapper)
            .click(() => {
                this.modal.remove();
            });

        this.canvasWrapp = h
            .div("canvas__wrapper")
            .cl("droppable")
            .appendTo(this.wrapper);
        this.canvas = h.tag("canvas").appendTo(this.canvasWrapp).get();

        this.panel = h.div(ImgRedactor.CL + "panel").appendTo(this.wrapper);
        this.rangeInput = h
            .input("range")
            .attr("min", "1")
            .attr("max", "100")
            .attr("value", "25")
            .attr("step", "1")
            .cl(ImgRedactor.CL + "range")
            .appendTo(this.panel);
        this.colorWrapp = h
            .div(ImgRedactor.CL + "color-wrapp")
            .appendTo(this.panel);

        this.colorWrapp.add(
            h
                .input("radio")
                .cl("paint-radio")
                .attr("name", "colorRadio")
                .attr("value", "black")
                .attr("checked", "")
        );
        this.colorWrapp.add(h.label("black", "Black", false));

        this.colorWrapp.add(
            h
                .input("radio")
                .cl("paint-radio")
                .attr("name", "colorRadio")
                .attr("value", "white")
                .attr("checked", "")
        );
        this.colorWrapp.add(h.label("white", "White", false));

        this.colorWrapp.add(
            h
                .input("radio")
                .cl("paint-radio")
                .attr("name", "colorRadio")
                .attr("value", "red")
                .attr("checked", "")
        );
        this.colorWrapp.add(h.label("red", "Red", false));

        this.colorWrapp.add(
            h
                .input("radio")
                .cl("paint-radio")
                .attr("name", "colorRadio")
                .attr("value", "green")
                .attr("checked", "")
        );
        this.colorWrapp.add(h.label("green", "Green", false));

        this.colorWrapp.add(
            h
                .input("radio")
                .cl("paint-radio")
                .attr("name", "colorRadio")
                .attr("value", "blue")
                .attr("checked", "")
        );
        this.colorWrapp.add(h.label("blie", "Blue", false));
    }

    loadImg(src) {
        let img = h.img(src).attr("crossorigin", "anonymous").get();
        this.src = src;

        img.onload = () => {
            if (img.width > 1300) {
                img.height = (img.height * 1000) / img.width;
                img.width = 1300;
            }

            this.drawOnImage(img);
        };
    }

    drawOnImage(image) {
        this.context = this.canvas.getContext("2d");

        const imageWidth = image.width;
        const imageHeight = image.height;

        this.img = image;

        this.canvas.width = imageWidth;
        this.canvas.height = imageHeight;

        this.context.drawImage(image, 0, 0, imageWidth, imageHeight);
    }

    paint() {
        if (this.inputWrapper) this.inputWrapper.remove();
        let isDrawing;

        this.canvas.style.cursor = "crosshair";

        this.canvas.addEventListener("mousedown", (e) => {
            this.getCanvasPosition();
            isDrawing = true;
            this.context.beginPath();
            this.context.lineWidth = this.size;
            this.context.strokeStyle = this.color;
            this.context.lineJoin = "round";
            this.context.lineCap = "round";
            this.context.moveTo(
                e.clientX - this.canvasPosition.left,
                e.clientY - this.canvasPosition.top
            );
        });

        this.canvas.addEventListener("mousemove", (e) => {
            this.context.save();
            this.getCanvasPosition();
            if (isDrawing) {
                this.context.lineTo(
                    e.clientX - this.canvasPosition.left,
                    e.clientY - this.canvasPosition.top
                );
                this.context.stroke();
            }
        });

        this.canvas.addEventListener("mouseup", () => {
            isDrawing = false;
            this.context.closePath();
            this.cPush();
        });
    }

    paintSizeEvent() {
        this.size = this.rangeInput.val();
        this.rangeInput.get().oninput = (e) => {
            this.size = e.target.value;
            this.input.get().style.fontSize = this.size + "px";
            this.context.font = `normal ${this.size}px Inter`;
        };
    }

    getCanvasPosition() {
        this.canvasPosition = this.canvas.getBoundingClientRect();
    }

    colorSelectEvent() {
        this.colorWrapp.eachOfClass("paint-radio", (c) => {
            if (c.checked) this.color = c.value;
        });

        this.colorWrapp.eachOfClass("paint-radio", (c) => {
            c.onclick = () => {
                this.color = c.value;
                this.input.get().style.color = this.color;
                this.context.fillStyle = this.color;
            };
        });
    }

    drawToolsPanel() {
        let toolPanel = h.div(ImgRedactor.CL + "tool").prependTo(this.wrapper);

        let painter = h
            .div(ImgRedactor.CL + "draw")
            .appendTo(toolPanel)
            .click(() => this.paint())
            .get();
        h.div(ImgRedactor.CL + "text")
            .appendTo(toolPanel)
            .click(() => this.text());

        let wrapperSettings = h.div(ImgRedactor.CL + "btns").appendTo(this.panel);

        h.div(ImgRedactor.CL + "clear")
            .text("clear", false)
            .appendTo(wrapperSettings)
            .click(() => this.clearAll());
        h.div(ImgRedactor.CL + "back")
            .text("back", false)
            .appendTo(wrapperSettings)
            .click(() => this.cUndo());
        h.div(ImgRedactor.CL + "save")
            .text("save", false)
            .appendTo(wrapperSettings)
            .click(() => this.save());

        painter.dispatchEvent(new Event("click"));
    }

    cPush() {
        if (
            this.cPushArray[this.cPushArray.length - 1] === this.canvas.toDataURL()
        ) {
            return;
        }
        this.cStep++;
        if (this.cStep < this.cPushArray.length) {
            this.cPushArray.length = this.cStep;
        }
        this.cPushArray.push(this.canvas.toDataURL());
    }

    cUndo() {
        if (this.cStep === 0) {
            this.clearAll();
        }
        if (this.cStep > 0) {
            this.cStep--;
            let canvasPic = new Image();
            canvasPic.src = this.cPushArray[this.cStep];
            canvasPic.onload = () => {
                this.context.drawImage(canvasPic, 0, 0);
            };
        }
    }

    clearAll() {
        if (this.inputWrapper) this.inputWrapper.remove();
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.drawImage(this.img, 0, 0, this.img.width, this.img.height);
        this.cPushArray.splice(0, this.cPushArray.length);
    }

    save() {
        this.resultFunc(this.canvas, this.filename);
        this.modal.remove();
    }

    result(f) {
        this.resultFunc = f;
    }

    drawInput() {
        if (!this.isInputOpen) {
            this.inputWrapper = h.div(ImgRedactor.CL + "input-wrapper");
            this.inputWrapper.get().style.top = 30 + "px";
            this.inputWrapper.get().style.left = 10 + "px";
            let inputDragWrapper = h.div(ImgRedactor.CL + "inpt-drag-wrapper").appendTo(this.inputWrapper);
            this.dragInput = h.div(ImgRedactor.CL + "drag").appendTo(inputDragWrapper).get();
            this.acceptInput = h.div(ImgRedactor.CL + "accept").appendTo(inputDragWrapper).click(() => this.drawText());
            this.input = h.input("text").cl("canvas__text").appendTo(inputDragWrapper);
            this.input.get().style.color = this.color;
            this.input.get().style.fontSize = this.size + "px";
            this.isInputOpen = true;
        }
    }

    text() {
        this.drawInput();
        this.dragInputEvent();
        this.inputEvent();
        this.inputWrapper.appendTo(this.canvasWrapp);
        this.input.get().focus({focusVisible: true});
        this.context.fillStyle = this.color;
        this.context.font = `normal ${this.size}px Inter`;
        this.context.textAlign = "left";
        this.context.textBaseline = "alphabetic";
        this.canvas.style.cursor = "default";
    }

    inputEvent() {
        let input = this.input.get();
        input.addEventListener("input", () => {
            input.style.boxSizing = "content-box";
            input.style.width = input.value.length + "ch";
        });
    }

    drawText() {
        let text = this.input.val();
        let left = parseInt(this.inputWrapper.get().style.left, 10);
        let top = parseInt(this.inputWrapper.get().style.top, 10);
        top = Number(top) + Number(this.size) + 9;
        this.context.fillText(text, left + 16, top);
        this.inputWrapper.remove();
        this.cPush();
        this.isInputOpen = false;
    }

    dragInputEvent() {
        this.dragInput.addEventListener("mousedown", (e) => {
            this.input.get().focus();
            let currentDroppable = null;
            let block = this.inputWrapper.get();

            let shiftX = e.clientX - block.getBoundingClientRect().left;
            let shiftY = e.clientY - block.getBoundingClientRect().top;

            document.body.append(block);
            moveAt(e.pageX, e.pageY);

            function moveAt(pageX, pageY) {
                block.style.left = pageX - shiftX + "px";
                block.style.top = pageY - shiftY + "px";
            }

            let isInRedactor = false;
            let onMouseMove = (event) => {
                this.input.get().focus();
                moveAt(event.pageX, event.pageY);
                block.hidden = true;
                let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
                block.hidden = false;
                if (!elemBelow) return;
                let droppableBelow = elemBelow.closest(".droppable");

                if (
                    currentDroppable != droppableBelow &&
                    currentDroppable !== this.modal.get()
                )
                    isInRedactor = true;
            };
            document.addEventListener("mousemove", onMouseMove);
            block.onmouseup = (event) => {
                this.input.get().focus();
                this.inputWrapper.appendTo(this.canvasWrapp);

                if (isInRedactor) {
                    let wrapperRect = this.canvasWrapp.get().getBoundingClientRect();
                    block.style.left = event.pageX - wrapperRect.left - 10 + "px";
                    block.style.top = event.pageY - wrapperRect.top + 10 + "px";
                } else {
                    block.style.left = 30 + "px";
                    block.style.top = 30 + "px";
                }

                document.removeEventListener("mousemove", onMouseMove);
                this.input.get().focus();
                block.onmouseup = null;
            };

            block.ondragstart = function () {
                return false;
            };
        });
    }



}

function $(text) {

}

function _e(obj1, obj2) {
    return _.e(obj1, obj2)
}

class _ extends utils {


}

class ObjectConverter {

    static map = new Map()

    addMapping(from, to, fun) {
        ObjectConverter.map.set(from, {"field": to, "func": fun})
    }

    //example, how this class works

    // test(){
    //     var object = {"id": 0, "name" : 10, "url" : "httpd"}
    //     //{objectId - > id, name,  additional -> url"}

    //     this.addMapping("id", "objectId", (x)=> x );
    //     this.addMapping("url", "additional", (x)=> {return {"url": x} } )
    //     this.convertToObject(object);

    // }

    convertToObject(incomingObject) {
        let res = {}
        for (let key in incomingObject) {
            let item = ObjectConverter.map.get(key);
            if (item != null) {
                res[item.field] = item.func(incomingObject[key], res)
            } else {
                if (_.isNull(res[key])) {
                    res[key] = incomingObject[key];
                }

            }
        }
        return res;
    }

}

class ObjectBuilder {
    #obj = {};
    #context;

    constructor(context = {}) {
        this.#context = context;
    }

    byKey(key) {
        this.put(key, this.#context[key])
        return this;
    }


    copy() {
        for (let key in Object.keys( this.#context)) {
            this.put(key, this.#context[key]);
        }
        return this;
    }

    byKeyInt(key) {
        this.putAsInt(key, this.#context[key])
        return this;
    }

    byKeyFloat(key) {
        this.putAsInt(key, this.#context[key])
        return this;
    }


    put(key, value) {
        this.#obj[key] = value;
        return this;
    }

    putAsInt(key, value) {
        this.put(key, parseInt(value))
    }

    putAsFloat(key, value) {
        this.put(key, parseFloat(value))
    }

    get() {
        this.#context = null;
        return this.#obj;
    }

}


function _debounce(callee, timeoutMs) {
    return function perform(...args) {
        let previousCall = this.lastCall;

        this.lastCall = Date.now()
        if (previousCall && this.lastCall - previousCall <= timeoutMs) {
            clearTimeout(this.lastCallTimer);
        }

        this.lastCallTimer = setTimeout(() => callee(...args), timeoutMs);
    }
}
