class EditValueComponent {
    getComponent() {
    }

    getValue() {
    }

    setValue() {
    }
}

class DefaultEditValueComponent extends EditValueComponent {

    #controller;
    #component;

    getValue() {
        return this.#controller.val();
    }

    getComponent() {
        return this.#component;
    }

    setValue(val) {
        this.#controller._text(val)
    }

    constructor(editableDiv) {
        super();
        this.#controller = editableDiv.createEditComponent();
        this.#component =
            h.div("edit_field_container")
                .add(this.#controller)
                .add(editableDiv.createSaveButton())
                .add(editableDiv.createCancelButton())

                .hide();

    }


}

class EditableDivOptions {

    static  EDIT_TRIGGER_TYPE_CLICK = "click";
    static  EDIT_TRIGGER_TYPE_DOUBLE_CLICK = "double_click";
    static  EDIT_COMPONENT_TYPE_TEXT = "text";
    static  EDIT_COMPONENT_TYPE_TEXT_AREA = "textarea";

    #editTriggerType = EditableDivOptions.EDIT_TRIGGER_TYPE_DOUBLE_CLICK;
    #changeListener;
    #editComponentType = EditableDivOptions.EDIT_COMPONENT_TYPE_TEXT;

    #editComponentSaveButtonText = null;

    #cancelButtonText = null;

    constructor(changeListener) {
        this.#changeListener = changeListener;

    }

    setCancelButtonText(text){
        this.#cancelButtonText = text;
        return this;
    }


    setEditComponentSaveButtonText(text) {
        this.#editComponentSaveButtonText = text;
        return this;
    }

    setEditComponentType(type) {
        this.#editComponentType = type;
        return this;
    }

    setEditTriggerType(type) {
        this.#editTriggerType = type;
        return this;
    }

    getEditTriggerType() {
        return this.#editTriggerType;
    }

    getCancelButtonText(){
        return this.#cancelButtonText;
    }


    getEditComponentSaveButtonText() {
        return this.#editComponentSaveButtonText;
    }

    getEditComponentType() {
        return this.#editComponentType;
    }

    getChangeListener() {
        return this.#changeListener;
    }
}

class EditableDiv extends h {
    #options;
    #editComponent;

    getValue() {
        return this.#editComponent.getValue();
    }

    constructor(options = new EditableDivOptions()) {
        super()
        if (!(options instanceof EditableDivOptions)) {
            throw new Error('editable div options should be extended from EditableDivOptions')
        }

        this.#options = options;
    }

    static fromField(field, options, parent = null) {
        let el = new EditableDiv(options).setEl(h.from(field).get()).assignTrigger();

        _.ofNullable(parent, () => el.parent())?.add(el.getEditComponent().getComponent());

        return el;
    }

    assignTrigger() {

        this.clickIf(_e(this.#options.getEditTriggerType(), EditableDivOptions.EDIT_TRIGGER_TYPE_CLICK), this.toggleToEdit.bind(this))
        this.doubleClickIf(_e(this.#options.getEditTriggerType(), EditableDivOptions.EDIT_TRIGGER_TYPE_DOUBLE_CLICK), this.toggleToEdit.bind(this))
        return this;
    }

    toggleToEdit() {
        this.getEditComponent().setValue(this.val())
        this.hide();
        this.getEditComponent().getComponent().show();
    }

    toggleToView() {
        this.show();
        this.#editComponent.getComponent().hide();

    }

    getEditComponent() {
        if (this.#editComponent) {
            return this.#editComponent;
        }

        return this.#editComponent = new DefaultEditValueComponent(this);
    }

    /**
     * Method can be overridden by extended class to present a component to edit, if required
     * @returns   an H object to edit value
     */
    createEditComponent() {
        if (_e(this.#options.getEditComponentType(), EditableDivOptions.EDIT_COMPONENT_TYPE_TEXT)) {
            return h.input("text").cl("editable_component_input");
        } else {
            return h.textArea().cl("editable_component_textarea");
        }


    }

    createSaveButton() {
        return h.span("save_button_editable_div")
            ._textIf(this.#options.getEditComponentSaveButtonText(), this.#options.getEditComponentSaveButtonText())
            .click(() => {
                this.setVal(this.getEditComponent().getValue())
                if (this.#options.getChangeListener()) {
                    this.#options.getChangeListener()(this)
                }

                this.toggleToView();
            });
    }

    createCancelButton() {
        return h.span("cancel_button_editable_div")
            ._textIf(this.#options.getCancelButtonText(), this.#options.getCancelButtonText())
            .click(() => {
                this.toggleToView();
            });
    }

}