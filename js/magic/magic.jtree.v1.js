class Tree {
    #paddingDef;
    #onSelect;
    #id;
    #subFetcher;

    #userDataDiv = h.div("tree_data_content");

    constructor(paddingDef = 10, onSelect, subFetcher , id = null) {

        this.#paddingDef = paddingDef;
        this.#onSelect = onSelect;
        this.#id = id;
        this.#subFetcher = subFetcher;

    }

    getContent(){
        return this.#userDataDiv;
    }

    addItem(idRoot, data) {
        let level = this.getLevel(idRoot);

        let box = idRoot.firstByClass("tree_box");

        if (box == null) {
            box = h.div("tree_box").appendTo(idRoot);

        }

        let text = data.cnt > 0 ? (data.name + " [" + data.cnt + "]") : data.name
        if (data.selectable) {
            let _this = this;
            text = h.a(null, text, false).click(function (e,ev){
                try{
                    ev.preventDefault();

                    let el = document.querySelectorAll(".clicked");
                    for (let i = 0; i< el.length; i ++ ){
                        el[i].classList.remove("clicked");
                    }
                    e.classList.add("clicked");

                    _this.#onSelect(data, _this.#id)
                }catch (e){
                    console.error(e)
                }
            });
        }
        let res = h.div("new_item").id(idRoot.get().id + "_" + level)
            .setData("parent", idRoot.get().id)
            .setData("level", level)
            .appendTo(box);

        if (data.selectable) {
            text.appendTo(res);
        } else {
            res.text(text, false);
        }

        let _this = this;
        if (data.cnt > 0) {
            h.img("collapsed.png.png").data_id(data.id).cl("collapsed").wh(16).prependTo(res).click(function (e) {
                _this.collapseOrFetch(e);
            });
        } else {
            h.img("tree_item.png.png").data_id(data.id).prependTo(res).wh(16)
        }

        res.get().style.paddingLeft = this.#paddingDef * level;
        return res;
    }

    collapseOrFetch(e) {
        if (e.classList.contains("expanded_tree_item")) {
            this.collapse(e)
        } else {
            this.expand(e);
        }
    }

    collapse(e) {
        let el = h.from(e).src("collapsed.png.png").rcl("expanded_tree_item").firstByClass("tree_box", false);
        let divel = el.querySelectorAll('.tree_box');
        for (let i = 0; i < divel.length; i++) {
            divel[i].style.display = 'none';
        }

    }

    expand(e) {
       e = h.from(e).src("expanded.png.png").cl("expanded_tree_item");

        if (!e.ccl("data_fetched")) {
            e.cl("data_fetched");

            let data = this.#subFetcher(e);
            utils.each(data, (item) => { this.addItem(e.parent(true),item) })

        }

        e.parent(true).eachOfClass("tree_box", (divEl)=> {
            divEl.style.display = 'block';
        })

    }

    remove(idRoot) {
        idRoot.get().remove();
    }

    getLevel(root) {
        let level = root.getData("level");
        if (utils.e(level, undefined)) {
            level = 0;
        }

        return parseInt(level) + 1;
    }


}