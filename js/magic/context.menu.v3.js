class ContextMenuAction {

    #listener;
    #renderer;

    #subEntries;
    constructor(listener) {
        this.#listener = listener;
    }

    setRenderer(renderer){
        this.#renderer = renderer;
    }

    getListener(){
        return this.#listener;
    }

    getRenderer(){
        return _.ofNullable(this.#renderer, () => {return ContextMenuAction.defaultRenderer.bind(this)})
    }

    addSubEntry(contextMenuAction){
        if (!contextMenuAction instanceof  ContextMenuAction){
            throw new Error ('context subentry should be of type ContextMenuAction')
        }
        this.#subEntries.push(contextMenuAction);
    }

    assignListeners(){

    }

    static defaultRenderer(){

    }



}

class ContextMenu {

    #items = [];
    #container;

    addAction (item){
        if (!item instanceof  ContextMenuAction){
            throw new Error ('context subentry should be of type ContextMenuAction')
        }

        /**
         *replace original renderer on closing renderer
         */

        let originAction = item.getListener();

        item.setListener(()=> {originAction(item); this.hide()})

        this.#items.push()
    }

    show(){
        this.#container.show();
    };
    hide(){
        this.#container.hide();
    };

    build(){

    }
}