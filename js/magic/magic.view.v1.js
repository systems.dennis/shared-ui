var _structured_view = [];
class MagicView{
    #objectId;
    #id;
    #path;
    #modal;
    #rootData;
    #searchName;
    #parent
    #listener;
    #listRequest;
    #dataRequest;

    #form = h.div("modal").cl("hidden").id("__view_" + new Date().getDate());

    constructor(objectId, id,  path, rootData = false, searchName, listener, listRequest = null, dataRequest = null) {
        if (objectId < 1){
            return;
        }
        this.#rootData = rootData;
        this.#objectId = objectId;
        this.#id = id;
        this.#path = path;
        this.#searchName = searchName;
        this.#listener = listener;
        this.#listRequest = listRequest;
        this.#dataRequest = dataRequest;
        this.draw();
    }


    getModal(){
      return this.#modal;
    }

    setParent(parent){
      this.#parent = parent;
    }

    getParent(){
        return this.#parent;
    }

    getObjectId(){
      return this.#objectId;
    }

    draw(){
        this.#modal = dom.createModal(dom.REMOVE_TYPE_REMOVE).cl("modal_view")
        document.body.append(this.#form)
        this.#form.appendTo(document.body);
        this.buildContent();
        this.#modal.appendTo(document.body);

    }

    buildContent(){
        let dataPath = Environment.dataApi + "/api/v2/" +  this.#path + "/root/fetch/details/" + this.#objectId;
        let listPath = Environment.dataApi + "/api/v2/" +  this.#path + "/root/fetch/list/";

        if (this.#listRequest) {
           listPath = this.#listRequest.getFullPath();
        }

        if (this.#dataRequest) {
            dataPath = this.#dataRequest.getFullPath();
        }

        let _this = this;
        if (_structured_view[dataPath]){
            this.buildForm (_structured_view[dataPath]);
        } else {

            Executor.runGet(dataPath, (ores) => {
              
              Executor.runGet(listPath, (res) => {
                ores.fields = res.fields;
                _structured_view[dataPath]  = ores;
                _this.buildForm(ores);
              }, true, Executor.HEADERS, (e) => {
                verifyResponse(e)
                dom.toast("Error during loading data", ToastType.ERROR)
            })
            
            }, true, Executor.HEADERS, (e) => {
                verifyResponse(e)
                dom.toast("Error during loading data", ToastType.ERROR)
          })
        }

    }

    buildForm (data){
        let _this = this;
        this.headerTitle = h.div('title')
        let dataContent = h.div("view_content").appendTo(this.#modal);
        let header = h.div("__mv_header")
        .add(this.headerTitle)
        .add(dom.createCloseImage().click((elem, event)=>{
            event.preventDefault();
            this.close();
        }))
        
        h.img("edit.png").wh(16).pointer().prependToIf(this.#parent, header).click(() => {
            let magicFormOptions = new MagicFormOptions( _this.#path); 
            magicFormOptions.setParent(this.#parent.getParent())
            MagicForm.Instance(_this.#path, magicFormOptions).load(_this.#objectId);
        });
 
       
        this.#modal.appendChild(header);
        let groups = []
        _.each(data.fields, (field)=>{
            if(_e(groups.indexOf(field.group), -1) && _.isTrue(field.group)){
                groups.push(field.group)
            }
        })

        _.isEmpty(groups) && groups.push('defaultGroup')

        let component = groups.map((e)=> h.div('div_component').setData('component', e))
    
        _.each(component, (comp, i)=>{
            _.each(data.fields,(field)=>{
                if(_e(field.group, comp.getData('component')) || (_.isEmpty(field.group) && _e(i,0))){
                    let createdField = this.createField(field, data);
                    createdField &&  createdField.appendTo(comp);
                }
            })
        })

        let tabOption = new MagicTabOptions()
        let tabs = new MagicTab(tabOption);
        _.each(groups, (group, i)=>{
            tabs.addTab(group, component[i]) 
        })
        tabs.deployTo(dataContent)


        let favoriteEnabled = Favorite.enabled();
        if(favoriteEnabled){
            this.searchObjectType(data)
        }

        if(this.#listener) {
          this.#listener(this);
        }
    }

    createField(field, data){
        let fieldData = data.data[field.field]
        if(_.e(field.type, 'text')){
           return this.buildText(field, data, fieldData); 
        }
        if(_.e(field.type, "object_chooser")){
            return this.buildChooser(field, data, fieldData);
        }
        if(_.e(field.type, "file")){
            return this.buildFile(field, data, fieldData);  
        }
        if(_.e(field.type, "files")){
            return this.buildFiles(field, data, fieldData)
        }
        if(_.e(field.type, "checkbox")){
            return this.buildCheckbox(field, data, fieldData)
        }
        if(_.e(field.type, "datetime")){
            return this.buildDatetime(field, data, fieldData)
        }
        if(_.e(field.type, "action")){
            return null
        }
    }

    searchObjectType(objData){
       let _this = this
       let path = Environment.dataApi + '/api/v2/shared/favorite/byName?name=' + this.#searchName;
       if(this.#searchName !='IssueForm'&& _.notNull(this.#searchName)){
          Executor.runGet(path, function (data) {
        _this.buildFavarite(objData, data.result)
       }) 
       }else{
        _this.buildFavarite(objData, 'IssueForm')
       }
      
    }

    buildFavarite(objData, type){
        let _this = this
        let heart = h.span(Favorite.FAVORITE_CLASS + "_holder", "", false).click(function (e) { 
            if(_.isTrue(_this.getParent())){
                _this.getParent().getParent().drawValues()
            } 
            Favorite.markElementAsFavorite(e, objData.data, type);
        }).appendTo(this.#modal);

        Favorite.markIfElementFavorite(heart, objData.data, type);
    }

    close(){
        this.#modal.hideModal()
    }

    buildText(field, data, fieldData){
        let div = h.div('field_div').add(h.div('field_label').text(field.translation))
        if(_e(field.field,'id')){
            this.headerTitle.text("ID " +fieldData, false);
            return null  
        }
        if(_e(field.field,'action')){
            return null  
        }
        if(_e(field.field,'stages')){
            h.div('field_description').text("global.translate.magicview.stages").pointer().appendTo(div);
            new Stages(div, fieldData).init()
            return div  
        }
      
        _.e(field.type, 'description')
        ? h.div('field_description').setContent(fieldData).appendTo(div)
        : h.div('field_description').text(fieldData, false).appendTo(div);
        return div
    }

    buildChooser(field, data, fieldData){
        let div = h.div('field_div').add(h.div('field_label').text(field.translation))
        if(_e(fieldData.key,0)){
            return null
        }else{
            let chooser_div = h.div('chooser_div')
            if(_.isObject(fieldData.additional)){
                chooser_div
                .addIf(fieldData.additional.icon, h.img(fieldData.additional.icon))
                .add(h.div('text').text(fieldData.value, false))
               
                fieldData.additional.color && chooser_div.style().color(fieldData.additional.color)
            }else{
                chooser_div
                .addIf(_.isTrue(fieldData.additional), h.img(fieldData.additional))
                .add(h.div('text').text(fieldData.value, false))
            }   
            chooser_div.appendTo(div);
            div.click(() => h.magicView(fieldData.key, field.remoteType, false, field.searchName ))  
        }
        return div
    }

    buildFile(field, data, fieldData){
        let div = h.div('field_div').add(h.div('field_label').text(field.translation));
        if(!_.isTrue(fieldData)){
            return null
        }else{
            h.img(fieldData).appendTo(div);  
        }
        return div
    }

    buildFiles(field, data, fieldData){
        let div = h.div('field_div').add(h.div('field_label').text(field.translation));
        if(_.isEmpty(fieldData)){
            return null
        }else{
            let icons = h.div('icons_div')
            _.each(fieldData, (icon)=>{
                h.img(icon).appendTo(icons)
            })
            icons.appendTo(div);  
        }
        return div
    }

    buildCheckbox(field, data, fieldData){
        let div = h.div('field_div').add(h.div('field_label').text(field.translation));
        if(_.isNull(fieldData)){
            return null
        }else{
            fieldData
            ?h.img("true.png", Environment.defaultIconSize).appendTo(div) 
            :h.img("false.png", Environment.defaultIconSize).appendTo(div) 
            
        }
        return div
    }

    buildDatetime(field, data, fieldData){
        let div = h.div('field_div').add(h.div('field_label').text(field.translation));
        if(_.isNull(fieldData)){
            return null
        }else{
        h.div('date_time').text(fieldData, false).appendTo(div); 
        }
        return div
    }
    

}