class SearchEvent {
  static SEARCH_CREATED = "search_created";
  static SEARCH_ADVANCED_OPEN = "search_advanced_open";
  static SEARCH_APPLY_SEARCH_PERFORMED = "apply_search_performed";

  constructor(eventName, form, additional) {
    this.form = form;
    this.type = eventName;
    this.additional = additional;
  }
}

class SearchForm {
  #data = {};

  form;
  #parent;

  #content;

  #tag = h.div("tags");

  #showTemplates = [];
  #toSaveTemplate = [];

  fireEvent(eventName, additional) {
    if (
      _.isNull(this.#parent) ||
      _.isNull(this.#parent.getOptions().getFormListener())
    ) {
      return;
    }

    let magicFormEvent = new SearchEvent(eventName, this.#parent, additional);
    this.#parent.getOptions().getFormListener()(magicFormEvent);
  }

  show() {
    this.#content.show();
    this.fireEvent(SearchEvent.SEARCH_ADVANCED_OPEN, this);
  }

  hide() {
    this.#content.hide();
  }

  constructor(data, id, list) {
    this.#data = data;
    this.#parent = list;
  }

  getDefaultField() {
    return this.#data["defaultField"];
  }

  getList() {
    return this.#parent;
  }

  getForm() {
    return this.form;
  }

  build() {
    let searchBox = h.div("search_form").cl(dom.HIDDEN);

    let _this = this;
    this.#content = searchBox;
    let searchFieldContainer = h.div("search_box_container");

    let search = h
      .input("text")
      .placeHolder("global_search_placeholder")
      .cl("quick_search")
      .appendTo(searchFieldContainer);

    let advancedSearch = h.div("table-btns-panel").get();
    h.divImg("advanced_search.png.png")
      .wh(16)
      .cl("advanced_search")
      .appendTo(advancedSearch)
      .appendTo(advancedSearch);

    search.onKey((x) => {
      let defaultField = _this.getDefaultField();

      if (x.value.trim().length < 1) {
        _this.removeByName(defaultField);
        _this.applySearch();
        return;
      }

      let criteria = _this.getSearchCriteriaByFieldName(defaultField);

      if (criteria === undefined) {
        _this.form.appendChild(_this.createCriteria(defaultField).html);
        let created = _this.getLastCriteria();
        console.dir(created.get(), false);
        dom.setCboValue(created.child(0, false), defaultField);
        created.child(0, false).onchange();
        dom.setCboValue(created.child(1, false), "%...%");
        criteria = _this.getSearchCriteriaByFieldName(defaultField);
      }

      criteria.value = x.value;
      _this.applySearch();
    });
    searchFieldContainer.appendChild(search);
    searchFieldContainer.appendChild(advancedSearch);

    let reset = h.div("table-btns-panel").get();
    reset.appendChild(
      h
        .divImg("reset.png.png")
        .wh(16)
        .cl("reset_button_of_search")
        .click(function () {
          _this.getList().drawValues();
          dom.message();
        })
        .get()
    );
    searchFieldContainer.appendChild(reset);

    advancedSearch.onclick = function () {
      searchBox.rcl("hidden");
      dom.linkTo(searchBox, search);
    };

    h.tag("h2").text("global.pages.search.params.title").appendTo(searchBox);
    let actives = h.div("box_templates").appendTo(searchBox);
    this.form = h.div("search_elements").appendTo(searchBox);
    let addCriteria = h
      .a("#", "global.app.list.search.add_filter", true)
      .click(() => {
        _this.form.appendChild(_this.createCriteria().html);
        _this.form.appendChild(_this.createCriteria().addedWrapper);
      });

    let addCp = h.div("buttons").add(addCriteria).appendTo(searchBox);

    h.input("button")
      .text("global.app.list.search.cancel")
      .click(() => {
        _this.closeWindow(searchBox);
      })
      .appendTo(addCp);

    h.input("button")
      .text("global.app.list.search.apply")
      .click(() => _this.applySearch())
      .appendTo(addCp);

    h.divImg("close.png.png")
      .wh(16)
      .cl("search_fields_close")
      .click(() => {
        _this.closeWindow(searchBox);
        _this.visibilityTemplatesImgs();
      })
      .appendTo(searchBox);

    this.#parent.getSearchAndSettingsBar().add(searchFieldContainer); //advanced search and buttons
    this.#parent.getSearchAndSettingsBar().add(searchBox);

    this.toSaveTemplate(searchFieldContainer);
    this.toSaveTemplate(actives);

    this.showTemplate(searchFieldContainer, search);
    this.showTemplate(actives);

    this.visibilityTemplatesImgs();
    searchFieldContainer.add(this.#tag);

    this.fireEvent(SearchEvent.SEARCH_CREATED, [
      this.#content,
      searchFieldContainer,
    ]);
    //Save template window
  }

  applySearch() {
    this.rebuildTags();
    this.fireEvent(SearchEvent.SEARCH_APPLY_SEARCH_PERFORMED, this);
    this.getList().drawValues();
  }

  closeWindow(window) {
    window.cl("hidden");
  }

  rebuildTags() {
    this.#tag.text(null);
    this.form.eachOf((element) => {
      if (!h.from(element).ccl("new-selects__wrapper")) {
        h.from(element.nextElementSibling).eachOf((item) => {
          h.from(element).add(h.from(item.cloneNode(true)).hide());
        });
        let cbField = element.children[0];
        let cbType = element.children[1];

        this.#tag.add(this.buildTagText(h.from(element), cbField, cbType));
      }
    });
  }

  buildTagText(cbFieldHolder, cbField, cbType) {
    let value = [];

    let labelField = cbField.options[cbField.selectedIndex].label;

    let labelType = cbType.options[cbType.selectedIndex].label;

    cbFieldHolder.eachOf((item) => {
      let divH = h.from(item);
      if (
        divH.ccl("last_criteria_field") ||
        divH.ccl("search__chooser-field")
      ) {
        value.push(item.textContent);
      }
    });

    let tagText = labelField + " " + labelType + " " + value.join(", ");

    let tagValueContainer = h
      .div("tag_value")
      .appendTo(this.#tag)
      .text(tagText, false);

    let _this = this;
    return h
      .div("tag_value_container")
      .add(tagValueContainer)
      .add(
        h
          .img("rm.png")
          .cl("search_tag_remove")
          .wh(16)
          .click(function () {
            cbFieldHolder.get().nextElementSibling.remove();
            cbFieldHolder.remove();
            _this.applySearch();
          })
      );
  }

  toSaveTemplate(where) {
    let _this = this;
    const wrapper = h.div("template_save_wrapper").cl("hidden");
    //Save template window
    let templateWindow = h.div("template_save");
    wrapper.appendChild(templateWindow.get());
    let checkBoxdiv = h.div("template_save-flex");

    h.div("table_settings_items")
      .text("global.template.save_filter_as_template")
      .appendTo(templateWindow);
    let textInp = h
      .input("text")
      .placeHolder("global.template.enter_name")
      .cl("template_save-search-input");
    h.label(textInp, "search_title")
      .text("global.template.query.name")
      .appendTo(templateWindow);
    textInp.appendTo(templateWindow);

    let checkInp = h
      .input("checkbox")
      .id("save_search_checkbox")
      .appendTo(checkBoxdiv);
    h.label(checkInp, "search_title")
      .text("global.template.save.menu")
      .appendTo(checkBoxdiv);

    templateWindow.appendChild(checkBoxdiv.get());
    let buttons = h.div("buttons").appendTo(templateWindow);

    h.input("button")
      .text("global.template.cancel")
      .appendTo(buttons)
      .click(function () {
        wrapper.cl("hidden");
        textInp.get().value = "";
      });

    h.input("button")
      .text("global.template.save")
      .cl("template_save_savebtn")
      .appendTo(buttons)
      .click(function () {
        //place for Denis code

        Executor.runPostWithPayload(
          Environment.dataApi + "/api/v2/shared/table_setting/add",
          function () {
            dom.toast("success");
            // _this.findMySettings(templates)
            _this.visibilityTemplatesImgs();
            textInp.get().value = "";
          },
          _this.createTableSettingsPayLoad(textInp, checkInp)
        );
        wrapper.cl("hidden");
      });

    h.divImg("close.svg")
      .wh(16)
      .appendTo(templateWindow)
      .click(function () {
        wrapper.cl("hidden");
        textInp.get().value = "";
      });

    let templateImgDiv = h.div("table-btns-panel").get();
    where.appendChild(templateImgDiv);
    let templateImg = h
      .divImg("save_template.png")
      .cl("save_template")
      .wh(16)
      .click(function (e) {
        wrapper.rcl("hidden");
        dom.linkTo(templateWindow.get(), templateImgDiv);
      })
      .get();
    templateImgDiv.appendChild(templateImg);

    this.#parent.getSearchAndSettingsBar().add(wrapper.get());
    this.#toSaveTemplate.push(templateImg);
  }

  showTemplate(where, search) {
    let _this = this;
    let saveSearchWindow = h.div("template_list_window").cl("hidden");
    let wrapInWindow = h.div("template_list_wrapp").appendTo(saveSearchWindow);

    h.divImg("close.svg")
      .wh(16)
      .cl("template_list_wrapp-close")
      .appendTo(wrapInWindow)
      .click(function () {
        saveSearchWindow.cl("hidden");
      });
    h.div("template_list_title").text("saved_search").appendTo(wrapInWindow);

    let inputArea = h.input("text").appendTo(wrapInWindow).get();

    let divForUl = h.div("template_div_ul").appendTo(wrapInWindow);
    let ul = h.tag("ul").appendTo(divForUl);

    let divForShowBtn = h.div("table-btns-panel").appendTo(where);
    let showBtn = h
      .divImg("template_list.png")
      .cl("template_list")
      .wh(16)
      .appendTo(divForShowBtn)
      .click(function () {
        ul.get().innerHTML = "";
        saveSearchWindow.rcl("hidden");
        dom.linkTo(saveSearchWindow.get(), divForShowBtn.get());
        Executor.runGet(
          Environment.dataApi +
            "/api/v2/shared/table_setting/list" +
            "?topic=" +
            _this.getList().getRootPath(),
          function (data) {
            for (let i = 0; i < data.length; i++) {
              let li = h.tag("li").appendTo(ul);
              let span = h
                .a("#")
                .text(data[i].name, false)
                .click(function (e) {
                  _this.searchFromTemlate(data[i].magicQuery, search);
                  _this.replaceHeaders(data[i].fieldsOrder);
                  saveSearchWindow.cl("hidden");
                })
                .appendTo(li);
              h.divImg("delete.png")
                .data_id(data[i].id)
                .wh(16)
                .appendTo(li)
                .cl("search_remote_container-rm-im")
                .click(function (element, x) {
                  Executor.runDelete(
                    Environment.dataApi +
                      "/api/v2/shared/table_setting/delete/" +
                      element.getAttribute("data-id"),
                    function () {
                      dom.message();
                      h.from(element).get().parentElement.remove();
                      _this.visibilityTemplatesImgs();
                    }
                  );
                });
            }
          }
        );
      })
      .get();

    inputArea.oninput = function (e) {
      for (let i = 0; i < ul.get().childNodes.length; i++) {
        if (
          !ul
            .get()
            .childNodes[i].firstChild.innerText.toLowerCase()
            .includes(e.target.value.toLowerCase())
        ) {
          ul.get().childNodes[i].classList.add("hidden");
        } else {
          ul.get().childNodes[i].classList.remove("hidden");
        }
      }
    };

    this.#parent.getSearchAndSettingsBar().add(saveSearchWindow);
    this.#showTemplates.push(showBtn);
  }

  replaceHeaders(fieldsOrder) {
    let nweOrder = [];
    fieldsOrder.forEach((el) => {
      let a = this.#parent
        .getOrdering()
        .getHeaders()
        .find((u) => u.field === el);
      nweOrder.push(a);
    });
    this.#parent.getOrdering().setHeaders(nweOrder);
  }

  visibilityTemplatesImgs() {
    let _this = this;
    Executor.runGet(
      Environment.dataApi +
        "/api/v2/shared/table_setting/list" +
        "?topic=" +
        _this.getList().getRootPath(),
      function (data) {
        _this.#showTemplates.forEach((i) => {
          if (data.length) {
            i.classList.remove("hidden");
          } else {
            i.classList.add("hidden");
          }
        });
      }
    );

    let res = _this.getSearchCriteria();
    this.#toSaveTemplate.forEach((i) => {
      if (res.length) {
        i.classList.remove(dom.HIDDEN);
      } else {
        i.classList.add(dom.HIDDEN);
      }
    });
  }

  createTableSettingsPayLoad(textInp, checkInp) {
    let res = {
      topic: this.getList().getRootPath(),
      magicQuery: this.getList().createPayload(),
      addToMenu: checkInp.get().checked,
      name: textInp.get().value,
      fieldsOrder: this.getList().getOrdering().getOrder(),
    };

    return res;
  }

  getLastCriteria() {
    return this.form.last();
  }

  removeByName(name) {
    this.form.eachOf((node) => {
      let cbField = node.childNodes[0];
      if (utils.e(cbField.value, name)) {
        node.remove();
        return ""; //not to remove other searches with this field
      }
    });
  }

  getSearchCriteriaByFieldName(name) {
    return this.form.eachOf((x) => {
      let cbField = x.childNodes[0];
      if (utils.e(cbField.value, name)) {
        if (utils.e(cbField.getAttribute("data-type"), "object_chooser")) {
          return x.childNodes[2].childNodes[1];
        }
        return x.childNodes[2];
      }
    });
  }

  getSearchCriteria() {
    let res = [];
    this.form.eachOf((x) => {
      if (h.from(x).ccl("criteria_element")) {
        let cbField = x.childNodes[0];
        if (utils.notNull(cbField)) {
          if (utils.e(cbField.value, "null")) {
            return null;
          }
        }

        let remoteType = cbField.selectedOptions[0].getAttribute("data-type");
        let cbType = x.childNodes[1];

        let value;
        if (
          utils.e(cbType.value, "is_null") ||
          utils.e(cbType.value, "not_null")
        ) {
          value = cbType.value;
        } else if (utils.e(remoteType, "object_chooser")) {
          value = [];
          if (utils.e(cbType.value, "in") || utils.e(cbType.value, "not_in")) {
            let spans = x.querySelectorAll(".last_criteria_field");
            for (let i = 0; i < spans.length; i++) {
              const span = spans[i];
              value.push(parseInt(h.from(span).getDataId()));
            }
          } else {
            try {
              let spans = x.querySelectorAll(".last_criteria_field");
              value = parseInt(parseInt(h.from(spans[0]).getDataId()));
            } catch (exc) {
              log.error(exc);
            }
          }
        } else {
          value = x.childNodes[2];
        }

        res.push({
          field: cbField.value,
          value: value.value ? value.value : value,
          searchName:
            cbField.options[cbField.selectedIndex].getAttribute("data-name"),
          dataType: remoteType,
          type: cbType.value,
        });
      }
    });
    log.debug(res, false);
    return res;
  }

  searchFromTemlate(query, search) {
    let queries = query.query;
    this.#content.hide();
    dom.linkTo(this.#content, search);
    this.#content.eachOfClass("criteria_element", (oldCriteria) => {
      oldCriteria.remove();
    });

    this.#content.eachOfClass("new-selects__wrapper", (wrap) => {
      wrap.remove();
    });

    for (let y = 0; y < queries.length; y++) {
      this.form.add(this.createCriteria(null, queries[y]).html);
    }
  }

  createCriteria(myField = null, query) {
    //add field with 2 selects
    let chdiv = h.div("criteria_element").get();
    //first select
    let cb = dom.select("criteria_element_field");
    dom
      .option({
        label: _.$("global.pages.list.search.select_message"),
        value: "null",
      })
      .appendTo(cb);
    //second select
    let cbType = dom.select("type");
    let fields = this.#data["fields"];
    let _this = this;

    utils.each(fields, (field) => {
      if (field["searchable"]) {
        dom
          .option({
            label: _.$(field["translation"]),
            value: field["field"],
          })
          .attr("data-type", field["searchType"])
          .attr("data-field", field["searchField"])
          .attr("data-name", field["searchName"])
          .appendTo(cb);
      }
    });

    cb.click(() => {
      chdiv.rcl("present_perfect");
    });

    let cbObject = cb.get();
    cbObject.onchange = function () {
      if (cbObject.value == "null") {
        cbObject.parentElement.remove();
        return;
      }
      if (!chdiv.ccl("present_perfect")) {
        query = null;
      }

      let dataField =
        cbObject.options[cbObject.selectedIndex].getAttribute("data-field");
      let dataType =
        cbObject.options[cbObject.selectedIndex].getAttribute("data-type");
      let dataName =
        cbObject.options[cbObject.selectedIndex].getAttribute("data-name");

      if (
        chdiv.child(2).ccl("criteria_element_field") ||
        chdiv.child(2).ccl("search__chooser-field")
      ) {
        chdiv.child(2).remove();
      }

      _this.createCriteriaType(
        dataType,
        cbType,
        chdiv,
        cb,
        dataField,
        dataName,
        query
      );
      _this.createCriteriaField(
        dataType,
        dataField,
        dataName,
        cbType,
        chdiv,
        query
      );

      _this.visibilityTemplatesImgs();
    };

    let imgDel = this.createRemoveCriteria(chdiv);
    chdiv = h.from(chdiv);
    chdiv.appendChild(cb);
    chdiv.appendChild(cbType);
    chdiv.appendChild(imgDel);

    let wrapperAddedSelect = h.div("new-selects__wrapper");

    if (query) {
      cb.get().value = query.field;
      chdiv.cl("present_perfect");
      cb.get().onchange();
    }

    // if (myField){
    //     cbObject.value = myField
    //     cbObject.onchange()
    // }

    return { html: chdiv, tag: null, addedWrapper: wrapperAddedSelect };
  }

  createRemoveCriteria(chDiv) {
    let _this = this;
    let imgDel = h
      .img("/remove.png.png", 24)
      .cl("rm_criteria")
      .click(() => {
        if (chDiv.nextElementSibling) {
          if (
            chDiv.nextElementSibling.classList.contains("new-selects__wrapper")
          ) {
            chDiv.nextElementSibling.remove();
          }
        }
        chDiv.remove();
        _this.applySearch();
        _this.visibilityTemplatesImgs();
      });
    return imgDel;
  }

  createCriteriaType(type, cbType, chdiv, div, field, name, query) {
    cbType.text(null);
    let equals = _.$("global.search.equals");
    let doesNotEqual = _.$("global.search.does.not.equal");
    let inThe = _.$("global.search.in");
    let notInThe = _.$("global.search.not_in");

    if (type == "text") {
      dom.option({ label: equals, value: "=" }).appendTo(cbType);
      dom.option({ label: doesNotEqual, value: "!=" }).appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.begins.with"),
          value: "...%",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.not.begins.with"),
          value: "!...%",
        })
        .appendTo(cbType);

      dom
        .option({
          label: _.$("global.search.contains"),
          value: "%...%",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.not_contains"),
          value: "!%...%",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.ends.with"),
          value: "%...",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.not.ends.with"),
          value: "!%...",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.is_null"),
          value: "is_null",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.not_null"),
          value: "not_null",
        })
        .appendTo(cbType);
    }
    if (type == "integer" || type == "date") {
      dom.option({ label: equals, value: "=" }).appendTo(cbType);
      dom.option({ label: doesNotEqual, value: "!=" }).appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.more"),
          value: ">",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.less"),
          value: "<",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.more.or.equal"),
          value: ">=",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.less.or.equal"),
          value: "<=",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.is_null"),
          value: "is_null",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.not_null"),
          value: "not_null",
        })
        .appendTo(cbType);
    }
    if (type == "object_chooser") {
      dom.option({ label: equals, value: "=" }).appendTo(cbType);
      dom.option({ label: doesNotEqual, value: "!=" }).appendTo(cbType);
      dom.option({ label: inThe, value: "in" }).appendTo(cbType);
      dom.option({ label: notInThe, value: "not_in" }).appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.is_null"),
          value: "is_null",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.not_null"),
          value: "not_null",
        })
        .appendTo(cbType);
    }

    if (type == "collection") {
      dom.option({ label: "has", value: "has" }).appendTo(cbType);
      dom.option({ label: "in", value: "in" }).appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.is_null"),
          value: "is_null",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.not_null"),
          value: "not_null",
        })
        .appendTo(cbType);
    }
    if (type == "checkbox") {
      dom.option({ label: equals, value: "=" }).appendTo(cbType);
      dom.option({ label: doesNotEqual, value: "!=" }).appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.is_null"),
          value: "is_null",
        })
        .appendTo(cbType);
      dom
        .option({
          label: _.$("global.search.not_null"),
          value: "not_null",
        })
        .appendTo(cbType);
    }
    let _this = this;

    //when toggle field, delete old btn, and create new btn with new function
    chdiv.get().querySelector(".addBtn")?.remove();
    let addImg = h.img("add.png.png").wh(24).get();
    let addCooserButton = h
      .a("#")
      .click(function () {
        _this.addChooserField(div, name, chdiv.next(), null);
      })
      .cl("hidden")
      .cl("addBtn");
    addCooserButton.add(addImg);

    chdiv.add(addCooserButton);

    let rememberChild;

    cbType.get().onchange = function (e) {
      //when choos new cr.type, delete all searchType fields

      //hide or show 'add' button, depending our choose
      if (e.target.value == "in" || e.target.value == "not_in") {
        addCooserButton.rcl(dom.HIDDEN);
      } else {
        addCooserButton.cl(dom.HIDDEN);
      }

      if (e.target.value == "is_null" || e.target.value == "not_null") {
        if (chdiv.get().childNodes[2].tagName != "IMG") {
          rememberChild = chdiv.get().childNodes[2];

          chdiv.eachOf((el) => {
            if (
              h.from(el).ccl("last_criteria_field") ||
              h.from(el).ccl("search__chooser-field")
            ) {
              el.remove();
            }
          });
        }
      } else if (
        rememberChild &&
        (e.target.value !== "is_null" || e.target.value !== "not_null")
      ) {
        cbType.get().after(rememberChild);
        rememberChild = null;
      }
    };
    // utils.each(span, (x) => x.remove());

    if (query) {
      cbType.value = query.type;
    }
  }

  createCriteriaField(type, field, searchName, cb, div, query) {
    div.eachOfClass("last_criteria_field", (x) => x.remove());

    let object = h
      .input("text", "")
      .cl("last_criteria_field")
      .attr("data-type", type);

    if (utils.e(type, "text") || utils.e(type, "textarea")) {
      if (query) {
        object.text(query.value, false);
      }
    }

    if (type == "date") {
      object.jq().datepicker();
      if (query) {
        object.value = query.value;
      }
    }

    if (type == "checkbox") {
      object = dom
        .select("")
        .cl("last_criteria_field")

        .add(dom.option({ label: "True", value: "true" }))
        .add(dom.option({ label: "False", value: "false" }))
        .add(dom.option({ label: "Unset", value: "null" }));

      if (query) {
        object.get().value = query.value;
      }
    }

    if (query?.value == "is_null" || query?.value == "not_null") {
      object = "";
    }

    if (type == "object_chooser") {
      object = "";
      if (query && !Array.isArray(query.value)) {
        this.addChooserField(cb, searchName, field, query.value);
      } else if (query && Array.isArray(query.value)) {
        for (let i = 0; i < query.value.length; i++) {
          this.addChooserField(
            cb,
            query.searchName,
            query.field,
            query.value[i]
          );
        }
      } else {
        this.addChooserField(cb, searchName, field, false, false);
      }
    }

    let _this = this;

    let imgDel = div.get().getElementsByTagName("img");

    //todo strange story. for sure can be done better

    if (imgDel[0] != undefined && object) {
      div.addBefore(object, imgDel[0]);
    } else if (object) {
      div.add(object);
      div.add(this.createRemoveCriteria(div));
    }
  }

  addChooserField(div, searchName, field, id = false, needRm = true) {
    let cb = div.parent().first().get();
    let cbValue = cb.value;
    let chooserField = dom.selectedOption(cb).getAttribute("data-field");
    let header = {
      getData() {
        return {
          searchName: searchName,
          searchField: chooserField,
        };
      },
      getField() {
        return cbValue;
      },
      getType() {
        return "object_chooser";
      },
    };

    let object = h.span("last_criteria_field", null).get();
    let chooserDiv = h.div("search__chooser-field").add(object);
    let valueHolder = h
      .input("text")
      .id("status")
      .attr("data-type", searchName);

    if (id) {
      new MagicRemoteChooser(object, {}, valueHolder, null, header).getDataById(
        id,
        searchName
      );
    } else {
      new MagicRemoteChooser(object, {}, valueHolder, "dataType", header);
    }

    //add delete additional choosers
    let cl = h
      .img("rm.png")
      .cl("close_aditional_chooser")
      .wh(16)
      .click(function () {
        if (needRm) {
          chooserDiv.remove();
        } else {
          chooserDiv.eachOfClass("object_link", (el) => {
            console.log(el);
          });
          chooserDiv, object, div;
        }
      })
      .get();

    chooserDiv.add(cl);

    //this is where we add chooser in field
    let imgDel = div.parent(false).querySelector(".rm_criteria");

    if (field && field.isH) {
      field.add(chooserDiv);
    } else {
      div.parent(false).insertBefore(chooserDiv.get(), imgDel);
    }
  }
}