class FieldResolver {
    #field;
    #fieldData;

    #parent;
    rowWrapper

    static EVENT_ELEMENT_RENDERED = "EVENT_ELEMENT_RENDERED";
    static EVENT_ERRORS_CLEANED = "EVENT_ERRORS_CLEANED";
    static EVENT_ERROR_ON_CHANGE = "EVENT_ERROR_ON_CHANGE";
    static EVENT_CONTAINER_CREATED = "EVENT_CONTAINER_CREATED";
    static EVENT_VALUES_CONTAINER_CREATED = "EVENT_VALUES_CONTAINER_CREATED";
    static EVENT_COMPONENT_CREATED = "EVENT_COMPONENT_CREATED";
    static EVENT_OBJECT_SELECTED = "EVENT_OBJECT_SELECTED";
    static EVENT_AFTER_ADD = "EVENT_AFTER_ADD";

    static EVENT_OBJECT_CHANGED;// todo remove if this is not used until 01.10.2023

    static EVENT_OBJECT_CLEARED = "EVENT_OBJECT_CLEARED";
    static EVENT_VALUE_SET = "EVENT_VALUE_SET";

    static EVENT_FILE_UPLOADED = "EVENT_FILE_UPLOADED";

    static EVENT_FILE_EDITED = "EVENT_FILE_EDITED";
    static EVENT_ADD_CHOOSER_TO_LIST = "EVENT_ADD_CHOOSER_TO_LIST";
    static EVENT_REMOVE_CHOOSER_TO_LIST = "EVENT_REMOVE_CHOOSER_TO_LIST";

    static EVENT_FILE_ERROR = "EVENT_FILE_ERROR";
    static EVENT_LABEL_GENERATED = "EVENT_LABEL_GENERATED";
    static EVENT_FIELD_INFO_GENERATED = "EVENT_FIELD_INFO_GENERATED";
    static EVENT_SCREENSHOT_TAKEN = "EVENT_SCREENSHOT_TAKEN";
    static EVENT_VALUE_READ = "EVENT_VALUE_READ";
    static EVENT_ICON_PREVIEW_GENERATED = "EVENT_ICON_PREVIEW_GENERATED"
    static EVENT_DROPDOWN_VALUE_SET = "DROP-DOWN-VALUE-SET";

    getParent() {
        return this.#parent;
    }

    getRowWrapper() {
        return this.rowWrapper;
    }

    setParent(parent) {
        this.#parent = parent;
        return this;
    }

    getParent() {
        return this.#parent;
    }

    cancel() {

    }


    fireEvent(eventName, additional) {

        if (_.isNull(this.#parent) || _.isNull(this.#parent.getFormFieldListener())) {
            return
        }
        let magicFormEvent = new MagicFormEvent(eventName, this.#parent, additional, this.#field.getField());
        this.#parent.getFormFieldListener()(magicFormEvent);
    }

    constructor(field) {
        this.#field = field;

        this.#fieldData = field.getData();
    }


    static getDrawElement(field, parent) {

        let type = field.getType();

        try {
            let renderer = getCustomRenderer(field, parent);
            if (_.notNull(renderer)) return renderer;
        } catch (e) {

        }
        if (_.e(type, "checkbox")) {
            return new CheckBox(field).setParent(parent);
        }
        if (_.e(type, "number")) {
            return new NumberField(field, false).setParent(parent);
        }
        if (_.e(type, "double")) {
            return new DoubleField(field, false).setParent(parent);
        }
        if (_.e(type, "text_area")) {
            return new TextArea(field, false).setParent(parent);
        }
        if (_.e(type, "text")) {
            return new InputField(field).setParent(parent);
        }
        if (_.e(type, "hidden")) {
            return new InputField(field).setParent(parent);
        }
        if (_.e(type, "password")) {
            return new PasswordField(field).setParent(parent);
        }
        if (_.e(type, "date")) {
            return new DateElement(field).setParent(parent);
        }
        if (_.e(type, "drop-down")) {
            return new DropDown(field).setParent(parent);
        }
        if (_.e(type, "file")) {
            return new FileSelector(field).setParent(parent);
        }
        if (_.e(type, "files")) {
            return new MultiFilesSelector(field).setParent(parent);
        }
        if (_.e(type, "object_chooser")) {
            return new ObjectChooser(field).setParent(parent);
        }
        if (_.e(type, "object_chooser_list")) {
            return new ObjectChooserList(field).setParent(parent);
        }
        if (_e(type, "dictionary")) {
            return new DictionaryChooser(field).setParent(parent);
        }
        if (_e(type, "collections")) {
            return new CollectionField(field).setParent(parent);
        }
        if (_e(type, "category_attributes")) {
            return new CategoryAttributesField(field).setParent(parent);
        }
        if (_e(type, "category_attributes_values")) {
            return new CategoryAttributesValuesField(field).setParent(parent);
        }


        return new InputField(field).setParent(parent);
    }

    clearErrors() {
        try {
            let err = get("error_" + this.#field.getField());
            if (err == null) {
                return;
            }
            err.innerHTML = "";
            err.classList.add("hidden");

            document
                .getElementById(this.#field.getField())
                .classList.remove("error_state");
            this.fireEvent(FieldResolver.EVENT_ERRORS_CLEANED, null)
        } catch (e) {
            log.error(e);
        }
    }

    read(field) {
    }

    create(field) {
    }

    setValue(value) {
    }

    isRequired() {
        return this.#fieldData.required;
    }

    toShowLabel() {
        return this.#fieldData.showLabel;
    }

    getLabelText() {
        if (this.#fieldData.customized) return this.#fieldData.translation;


        return _.$(this.#fieldData.translation);
    }

    toShowPlaceHolder() {
        return this.#fieldData.showPlaceHolder;
    }

    getDescription() {
        return this.#fieldData.description;
    }

    getPlaceHolder() {
        return this.#fieldData.placeHolder;
    }

    getSetting(s) {
        return this.getFieldData()[s];
    }

    getIsId() {
        return this.getFieldData()["id"] || this.#fieldData.field == "id";
    }

    getFieldData() {
        return this.#fieldData;
    }

    getField() {
        return this.#field;
    }

    getType() {
        return this.#field.getType();
    }

    createContainer() {
        return h.div("row").clIf(this.getFieldData(), this.getFieldData().type);
    }

    get isCustom() {
        return this.getField().getData().customized
    }

    get fieldName() {
        return this.getField().getField();
    }


    markElementsRequired(items) {
        if (!this.isRequired()) {
            return;
        }
        for (let i = 0; i < items.length; i++) {
            items.classList.add("required");
        }

    }

    showLabel(div) {
        if (this.toShowLabel()) {
            let labelDiv = h
                .div("row_label_container")
                .cl(this.getSetting("type") + "_label_container")
                .prependTo(div);

            let lb = this.createLabelComponent().cl("label").cl("text_label");

            labelDiv.add(lb);
            if (this.isRequired()) {
                lb.cl("required");
                labelDiv.add(h.span("required_cell", "*"));
            }

            this.fireEvent(FieldResolver.EVENT_LABEL_GENERATED, labelDiv)
        }

    }

    showDescription(div) {
        let DESCRIPTION_FIELD_CLASS = "prompt_descr_image";
        if (_.notNull(this.getDescription())) {
            let descr = h.div(DESCRIPTION_FIELD_CLASS).text(this.getDescription(), true).hide();
            h.div('descr_wrap')
                .add(h.divImg("menu-item-10.svg").wh(16).cl(DESCRIPTION_FIELD_CLASS).click(() => {
                    descr.toggle(dom.HIDDEN)
                }))
                .add(descr)
                .appendTo(div)
            this.fireEvent(FieldResolver.EVENT_FIELD_INFO_GENERATED, {"descr": descr, "field": this.getField()});
        }
    }

    afterAdd() {
    }

    createLabelComponent() {
        let res = h.label(this.getField().getField(), this.getLabelText(), false);

        this.fireEvent(FieldResolver.EVENT_LABEL_GENERATED, res);
        return res;
    }

    createFieldDiv() {
        return h.div("row__wrapper").setData("field", this.#field.getField())
    }

    selectorListener(event) {
        if (_e(event.type, MagicTrueFalseSelectorEvent.VALUE_CHANGED)) {
            event.parent.fillVariants()
        }
    }

    fillVariants() {
        let path = Environment.fileStorageApi + "cabinet/search/" + Environment.authScope +
            "?page=0&s=" + this.searchField.get().value + "&onPublic="
            + this.onlyPablicSelector.getValue() + "&onlyMy=true"
            + "&onlyImages=" + this.onlyImagesSelector.getValue();
        let _this = this;
        Executor.runGet(path, function (data) {
                _this.content.text(null);
                if (data["empty"] == true) {
                    _this.content.text(_.$("pages.filechooser.no_data"));
                } else {
                    for (let i = 0; i < data.content.length; i++) {
                        let src = Environment.fileStorageApi + (data.content[i].pub ? "/public" : "/private") + data.content[i].downloadUrl
                        _this.makePreviewInUpdates(_this.content, src, data.content[i])
                    }
                }
            },
            true,
            Executor.HEADERS,
            function (e) {
                dom.toast(_.$("global.exception") + e, ToastType.ERROR);
            }
        );
    }
}

class MultiFileHolder {

    #files = [];

    addFile(file) {
        this.#files.push(file);
    }

    removeFile(name, callback) {
        _.each(this.#files, (file, index) => {

            if (_e(this.findFile(name), file)) {
                this.#files.splice(index, 1);

                RemoteFile.delete(file.getId(), callback)

                return '';
            }
        })
    }

    findFile(name) {
        return _.find(this.#files, (file) => {
            return _.e(file.getFileName(), name)
        })
    }

    getValues() {
        return _.map(this.#files, (file) => {
            return file.getSrc();
        })
    }

    getImages() {
        let images = [];
        _.each(this.#files, (file) => {
            if (file.getFile().isImage()) {
                images.push(file.getSrc())
            }
        })

        return images;
    }

    cleanFields() {
        this.#files = [];
    }

    getLength() {
        return this.#files.length;
    }
}

class MultiFilesSelector extends FieldResolver {
    #fileChooser;
    fileNames = [];
    #uploadedDiv = false;
    prevDiv;
    #loadingDiv;
    #dropDiv;
    #filesHolder = new MultiFileHolder();
    static NO_IMAGE = "no_image.png.png";

    clearErrors() {
    }

    cancel() {

    }

    create() {
        this.rowWrapper = super.createFieldDiv();
        let row = this.createContainer().appendTo(this.rowWrapper);
        this.showDescription(row)
        this.showLabel(row);
        this.wrapp = h.div('wrapp_for_attach').appendTo(row);
        this.init();
        return this.rowWrapper;
    }

    init = () => {
        this.buildDropBlock();
        this.buildScreenshotBlock();
        this.buildUploadBlock();
        this.buildFileUploader();
        this.#loadingDiv = h.div("loading__files").appendTo(this.wrapp);
        this.loadingGifAnim = h.div('loading-files__gif').appendTo(this.#loadingDiv);
        this.prevDiv = h.div('wrap_preview_div').appendTo(this.#loadingDiv);
        this.uploadBox = h.div('uloaded_box').appendTo(this.wrapp);
        this.onDrop();
    }

    buildFileUploader = () => {
        this.#fileChooser = new FileUploader(this.getField().getField() + "_file_chooser",
            (src) => {
                this.insertFile(src)
            }, this).setLoadingListener(this.loadingListener);
    }

    buildDropBlock = () => {
        this.#dropDiv = h.div('drop_div')
            .add(h.div('drop_bgi'))
            .add(h.div('drop_title_text').text('global.attachment.drop_files_here_or'))
            .add(h.a('#', 'global.attachment.click_to_select', true).click(() => {
                if (this.#filesHolder.getLength() < 6) {
                    this.#fileChooser.openSelector();
                } else {
                    dom.toast(_.$("global.attachment.max_length"))
                }
            }))
            .add(h.div('image_info').text('global.attachment.all_types_file'))
            .appendTo(this.wrapp);
    }

    buildScreenshotBlock = () => {
        this.screenshotDiv = h.div('screenshot_div')
            .add(h.a('#', 'global.attachment.take_a_screenshoot', true).click((x) => {
                _this.screenShot();
            }))
            .appendTo(this.wrapp);
    }

    buildUploadBlock = () => {
        this.uploadDiv = h.div('preview_div')
            .add(h.a('#', 'global.attachment.add_uploaded_attachments', true).click(() => {
                if (!this.#uploadedDiv) {
                    this.createFileSelectionForm(this.uploadBox);
                    this.#uploadedDiv = true;
                }
            }))
            .appendTo(this.wrapp);
    }

    onDrop = () => {
        var dropCl = "for_drop_files";
        this.#dropDiv.dragOver((event) => {
            event.preventDefault();
            this.#dropDiv.cl(dropCl)
        })

        this.#dropDiv.dragDrop((event) => {
            event.preventDefault();
            if (this.#filesHolder.getLength() < 6) {
                this.#dropDiv.rcl(dropCl);
                let file = event.dataTransfer.files[0];
                this.loadingListener(new LoadingEvent(LoadingEvent.LOADING_START));
                this.#fileChooser.upload(file);
            } else {
                dom.toast(_.$("global.attachment.max_length"))
            }
        })
    }

    loadingListener = (event) => {
        var showLoadingClass = "loading_to_show";

        if (_e(event.type, LoadingEvent.LOADING_START)) {
            this.#loadingDiv.cl(showLoadingClass);
        }

        if (_e(event.type, LoadingEvent.LOADING_END)) {
            this.#loadingDiv.rcl(showLoadingClass);
        }
    }

    setValue(value) {
        this.prevDiv.text(null);
        this.#filesHolder.cleanFields();

        if (this.isCustom && value) {
            if (value.value) {
                _.each(value.value.split(","), (src) => {
                    this.insertFile(src)
                })
            }
            return;
        }


        if (value && value[this.getField().getField()] && value[this.getField().getField()].length) {
            let values = value[this.getField().getField()];
            values.forEach(e => this.insertFile(e));
        }
    }

    screenShot() {
        let _this = this
        utils.capture(function (img) {
            img.toBlob((blob) => {
                let file = new File(
                    [blob],
                    "screenshot_from_" + new Date().getTime() + ".jpg",
                    {type: "image/jpeg"});
                const dataTransfer = new DataTransfer();
                dataTransfer.items.add(file);
                _this.#fileChooser.getFileSelector().files = dataTransfer.files;
                _this.#fileChooser.upload();
                img.toDataURL("image/png");
            }, "image/jpeg");
        })
    }

    makePreviewInField(src = false, filename, fileType) {
        let preview = h.div('attach_preview_item').clIf(src, 'image_has_uploaded');
        let previewBgi = h.div('drop_bgi').appendTo(preview);
        var file = FileFactory.load(src, new FileDisplayOptions(48, 48, "loaded", 64, false));

        file.draw(previewBgi);
        this.#filesHolder.addFile(file);

        let hoverDiv = h.div('hover_preview_item').click(() => {
            this.showFull(file)
        }).appendTo(preview);

        h.div('header_hover_div')
            .add(h.div('header_div_download').click(() => {
                this.downloadImg(file.getSrc())
            }))
            .addIf(utils.isImage(file.getFileName()), h.div('header_div_edite').click(() => {
                this.startPaint(file, preview);
            }))
            .add(h.div('header_div_delete').id('header_div_delete').click(() => {
                this.deleteFilePrompt(preview, file)
            }))
            .appendTo(hoverDiv);

        this.buildFileFooter(hoverDiv, filename, fileType);
        preview.prependTo(this.prevDiv);

    }

    deleteFilePrompt = (preview, file) => {
        let opts = new MagicPromptOptions("global.prompt.the.image.will.be.removed");
        opts.applyClick(MagicPromptButton.OK_LABEL, () => {
            this.deletePreview(preview, file);
            opts.markNoRepeat();
            opts.closeSelf();
        }).setIsAction(MagicPromptButton.OK_LABEL, true)
            .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                opts.closeSelf();
                opts.markNoRepeat();
            })
            .setIsAction(MagicPromptButton.CANCEL_LABEL, false)
            .allowNoRepeat("global.prompt.delete_item").newPrompt();
    }

    buildFileFooter = (hoverDiv, filename, fileType) => {
        let footerForHoverDiv = h.div('footer_hover_div').appendTo(hoverDiv);
        let firstText = h.div('footer_div_textname').text(filename, false).appendTo(footerForHoverDiv);
        h.div('footer_div_texttype').text(fileType, false).appendTo(footerForHoverDiv);
    }

    showFull(file) {
        let fileClass = file.getFile();
        if (fileClass.isImage()) {
            utils.showFullMedia(file.getSrc(), this.#filesHolder.getImages());
        } else if (fileClass.isVideo()) {
            utils.showFullMedia(file.getSrc(), [], "video");
        } else if (fileClass.isAudio()) {
            utils.showFullMedia(file.getSrc(), [], "audio")
        }
    }

    insertFile(src, filename, fileType) {
        if (!filename) {
            Executor.runGet(Environment.fileStorageApi + "/public/info?path=" + src,
                (data) => {

                    filename = data.originalName;
                    fileType = data.type;
                    this.makePreviewInField(src, filename, fileType)

                }, false);
        } else {
            this.makePreviewInField(src, filename, fileType)
        }
        this.#fileChooser.clean();
    }

    startPaint(file, preview) {
        if (file && file.getFile().isImage()) {
            let src = file.getSrc()
            let filename = file.getFileName();

            new ImgRedactor().init(src, filename).result((img, filename) => {
                this.loadingListener(new LoadingEvent(LoadingEvent.LOADING_START));
                img.toBlob(async (blob) => {
                    let fileImg = new File(
                        [blob],
                        filename,
                        {type: "image/jpeg"}
                    );
                    await this.#fileChooser.upload(fileImg);
                    this.deletePreview(preview, file);
                }, "image/jpeg");
            })
        }
    }

    downloadImg(src) {
        if (!src.includes(MultiFilesSelector.NO_IMAGE)) {
            let a = h.a(src).attr('download');
            a.get().click();
        }
    }

    deletePreview(prew, file) {
        this.#filesHolder.removeFile(file.getFileName(), () => {
            prew.remove();
        });
    }


    createFileSelectionForm(where) {
        let _this = this;
        let headerConf = h.div("row");
        let form = h.div("findFile").appendTo(where);
        let headerSearchField = h.div("row", "search").appendTo(form);
        headerConf.appendTo(form)
        let fcp = h.span("fcp_class", null).appendTo(headerConf);


        this.searchField = h
            .input("text")
            .id(this.getField().getField() + "_fcsf")
            .onKey((x) => _this.fillVariants())
            .placeHolder('global.attachment.search')
            .appendTo(headerSearchField);

        let onlyPablicSelectorOptions = new MagicTrueFalseSelectorOptions("pages.filechooser.only_public", false, true, false, true)
        onlyPablicSelectorOptions.setComponentListener(this.selectorListener)
        onlyPablicSelectorOptions.setParent(this)
        this.onlyPablicSelector = new MagicTrueFalseSelector(onlyPablicSelectorOptions).draw(fcp);


        headerConf.appendChild(fcp);

        let fcOnlyImages = h.span("fcom_class", null).appendTo(headerConf);

        let onlyImagesSelectorOptions = new MagicTrueFalseSelectorOptions("pages.filechooser.only_images", false, true, false, true)
        onlyImagesSelectorOptions.setComponentListener(this.selectorListener)
        onlyImagesSelectorOptions.setParent(this)
        this.onlyImagesSelector = new MagicTrueFalseSelector(onlyImagesSelectorOptions).draw(fcOnlyImages);


        this.content = h.div("fs_content").appendTo(form);

        h.img("close.png.png")
            .wh(16)
            .click(() => {
                _this.closeUploaded();
            })
            .appendTo(form);

        this.fillVariants();
    }


    closeUploaded() {
        this.uploadBox.first()?.remove();
        this.#uploadedDiv = false;
    }

    makePreviewInUpdates(div, src, data) {
        h.div('attach_preview_item')
            .add(h.div('drop_bgi')
                .add(h.img(src ? src : MultiFilesSelector.NO_IMAGE).wh(48)))
            .add(h.div('hover_preview_item')
                .add(h.div('header_hover_div'))
                .add(h.div('footer_hover_div')
                    .add(h.div('footer_div_textname').text(data.originalName, false))
                    .add(h.div('footer_div_texttype').text(data.type, false))))
            .click(() => {
                this.insertFile(src);
                this.closeUploaded();
            }).appendTo(div);
    }

    read() {
        return this.#filesHolder.getValues()
    }
}

class FileSelector extends FieldResolver {
    #fileChooser;
    #valueHolder = h.input(dom.HIDDEN);
    #preview;
    fileNames = [];
    prevDiv;
    #loadingDiv;
    #uploadedDiv = false;
    #canDelete = false;
    #dropDiv;
    #wrap;
    #file = null;
    static NO_IMAGE = "no_image.png.png";

    clearErrors() {
    }

    setWrapp(el) {
        this.#wrap = el;

        return this;
    }

    create() {
        this.rowWrapper = super.createFieldDiv();
        let row = this.createContainer().appendTo(this.rowWrapper);
        this.showDescription(row);
        this.showLabel(row);
        this.#wrap = h.div('wrapp_for_attach').appendTo(row);
        this.init();

        return this.rowWrapper;
    }

    init = () => {
        this.buildDropDiv();
        this.buildScreenshotDiv();
        this.buildUpLoadDiv();
        this.buildPreviewContainer();
        this.buildFileWrapper();
        this.initFileChooser();
        this.onDrop();
    }

    buildPreviewContainer() {
        this.#loadingDiv = h.div("loading__files").appendTo(this.#wrap)
        this.loadingGifAnim = h.div('loading-files__gif').appendTo(this.#loadingDiv);
        this.prevDiv = h.div('wrap_preview_div').appendTo(this.#loadingDiv).hide();
        this.uploadBox = h.div('uloaded_box').appendTo(this.#wrap);
    }

    buildDropDiv = () => {
        this.#dropDiv = h.div('drop_div')
            .add(h.div('drop_bgi'))
            .add(h.div('drop_title_text').text('global.attachment.drop_file_here_or'))
            .add(h.a('#', 'global.attachment.click_to_select', true).click(() => {
                this.replaceImgConfirmation(() => {
                    this.#fileChooser.openSelector();
                })
            }))
            .add(h.div('image_info').text('global.attachment.all_types_file'))
            .appendTo(this.#wrap);
    }

    buildScreenshotDiv = () => {
        this.screenshotDiv = h.div('screenshot_div')
            .add(h.a('#', 'global.attachment.take_a_screenshoot', true).click((x) => {
                this.replaceImgConfirmation(() => {
                    this.makeScreenshot()
                })
            }))
            .appendTo(this.#wrap);
    }

    buildUpLoadDiv = () => {
        this.uploadDiv = h.div('preview_div')
            .add(h.a('#', 'global.attachment.add_uploaded_attachments', true).click(() => {
                if (!this.#uploadedDiv) {
                    this.createFileSelectionForm(this.uploadBox);
                    this.#uploadedDiv = true;
                }
            }))
            .appendTo(this.#wrap);
    }

    initFileChooser() {
        this.#fileChooser = new FileUploader(this.getField().getField() + "_file_chooser",
            (src, fileName, fileType, data) => {
                this.#canDelete = !!data.newlyCreated;
                this.insertFile(src, fileName, fileType);

            }, this).setLoadingListener(this.loadingListener)
    }

    onDrop = () => {
        var dropCl = "for_drop_files";
        this.#dropDiv.dragOver((event) => {
            event.preventDefault();
            this.#dropDiv.cl(dropCl)
        })

        this.#dropDiv.dragDrop((event) => {
            event.preventDefault();
            this.#dropDiv.rcl(dropCl);
            let file = event.dataTransfer.files[0];
            this.replaceImgConfirmation(() => {
                this.loadingListener(new LoadingEvent(LoadingEvent.LOADING_START));
                this.#fileChooser.upload(file);
            })
        })
    }

    loadingListener = (event) => {
        var showLoadingClass = "loading_to_show";

        if (_e(event.type, LoadingEvent.LOADING_START)) {
            this.#loadingDiv.cl(showLoadingClass)
        }

        if (_e(event.type, LoadingEvent.LOADING_END)) {
            this.#loadingDiv.rcl(showLoadingClass)
        }
    }

    setValue(value) {

        if (this.isCustom && value && value.value) {
            this.insertFile(value.value);
            return;
        }

        if (value && value[this.getField().getField()]) {
            this.#valueHolder.value = value[this.getField().getField()];
            this.insertFile(this.#valueHolder.value);
        } else {
            this.deletePreview();
        }
    }

    makeScreenshot() {
        let _this = this
        utils.capture(function (img) {
            img.toBlob((blob) => {
                let file = new File(
                    [blob],
                    "screenshot_from_" + new Date().getTime() + ".jpg",
                    {type: "image/jpeg"}
                );
                const dataTransfer = new DataTransfer();
                dataTransfer.items.add(file);
                _this.#fileChooser.getFileSelector().files = dataTransfer.files;
                _this.#fileChooser.upload();
            }, "image/jpeg");
        })
    }

    cancel() {
        this.removeFile();
    }

    removeFile = () => {
        if (this.#canDelete) {
            RemoteFile.delete(this.#file.getRemoteData().id)
        }
        this.#canDelete = false;
    }

    buildFileWrapper = () => {
        this.fileWrapper = h.div('attach_preview_item').cl("image_has_uploaded").prependTo(this.prevDiv);
        this.#preview = h.div('drop_bgi').appendTo(this.fileWrapper);
        this.hoverDiv = h.div('hover_preview_item').appendTo(this.fileWrapper)
    }

    makePreviewInField() {
        this.hoverDiv.text(null).add(h.div('header_hover_div')
            .add(h.div('header_div_download').click(() => this.download()))
            .addIf(this.addEditBtnToFilePreview(), h.div('header_div_edite').id('header_div_edite').click(() => this.startPaint()))
            .add(h.div('header_div_delete').id('header_div_delete').click(() => {
                let opts = new MagicPromptOptions("global.prompt.the.image.will.be.removed");
                opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                    opts.markNoRepeat();
                    opts.closeSelf();
                    this.removeFile();
                    this.deletePreview();
                }).setIsAction(MagicPromptButton.OK_LABEL, true)
                    .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                        opts.closeSelf();
                        opts.markNoRepeat();
                    }).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                    .allowNoRepeat("global.replace_attachment").newPrompt();
            }))).click(() => {
            this.showFull()
        })
        this.buildItemFooter();
        this.fireEvent(FieldResolver.EVENT_ICON_PREVIEW_GENERATED, this.hoverDiv)
    }

    buildItemFooter() {
        var footer = h.div('footer_hover_div')
        this.fileNameDiv = h.div('footer_div_textname').appendTo(footer).text(this.#file.getFileName(), false);
        this.fileTypeDiv = h.div('footer_div_texttype').appendTo(footer).text(this.#file.getFileType(), false);
    }

    addEditBtnToFilePreview = () => {
        return this.#file && this.#file.getFile().isImage()
    }

    showFull() {
        let fileClass = this.#file.getFile();
        if (fileClass.isImage()) {
            utils.showFullMedia(fileClass.getSrc(), []);
        } else if (fileClass.isVideo()) {
            utils.showFullMedia(fileClass.getSrc(), [], "video");
        } else if (fileClass.isAudio()) {
            utils.showFullMedia(fileClass.getSrc(), [], "audio")
        }
    }

    deletePreview() {
        this.#preview.text(null).add(h.img(FileSelector.NO_IMAGE, 64).wh(64))
        this.#valueHolder.text(null)
        this.#fileChooser.clean();
        this.#file = null;
        this.prevDiv.hide();
    }

    download() {
        if (this.#file) {
            h.a(this.#file.getSrc()).attr('download').performClick();
        }
    }

    replaceImgConfirmation(myFunc) {
        if (_.isNull(this.#file)) {
            myFunc();
        } else {
            let opts = new MagicPromptOptions("global.prompt.confirm_replace");
            opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                opts.markNoRepeat();
                opts.closeSelf();
                if (this.#canDelete) {
                    RemoteFile.delete(this.#file.getRemoteData().id)
                }
                myFunc();
            }).setIsAction(MagicPromptButton.OK_LABEL, true)
                .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                    opts.closeSelf();
                    opts.markNoRepeat();
                }).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                .allowNoRepeat("global.prompt_no_repeat").newPrompt();
        }
    }

    closeUploaded() {
        this.uploadBox.first()?.remove();
        this.#uploadedDiv = false;
    }

    makePreviewInUpdates(prevDiv, src, data) {
        h.div('attach_preview_item')
            .add(h.div('drop_bgi')
                .add(h.img(src ? src : FileSelector.NO_IMAGE).wh(48)))
            .add(h.div('hover_preview_item')
                .add(h.div('header_hover_div'))
                .add(h.div('footer_hover_div')
                    .add(h.div('footer_div_textname').text(data.originalName, false))
                    .add(h.div('footer_div_texttype').text(data.type, false))))
            .click(() => {
                this.replaceImgConfirmation(() => {
                    this.insertFile(src);
                    this.closeUploaded();
                })
            }).appendTo(prevDiv);
    }

    insertFile(src) {
        this.#preview.text(null)
        var file = FileFactory.load(src, new FileDisplayOptions(48, 48, "loaded", 64, false));
        file.draw(this.#preview);
        this.#valueHolder.text(src, false);
        this.#file = file;
        log.info(file);
        this.makePreviewInField();
        this.prevDiv.show();
    }

    startPaint() {
        if (this.#file && this.#file.getFile().isImage()) {
            let src = this.#file.getSrc()
            let filename = this.#file.getServerFileName();

            new ImgRedactor().init(src, filename).result((img, filename) => {
                img.toBlob(async (blob) => {
                    let file = new File(
                        [blob],
                        filename,
                        {type: "image/jpeg"}
                    );
                    await this.#fileChooser.upload(file);
                }, "image/jpeg");
            })
        }
    }

    createFileSelectionForm(where) {
        let _this = this;
        let headerConf = h.div("row");
        let form = h.div("findFile").appendTo(where);
        let headerSearchField = h.div("row", "search").appendTo(form);
        headerConf.appendTo(form)
        let fcp = h.span("fcp_class", null).appendTo(headerConf);

        this.searchField = h
            .input("text")
            .id(this.getField().getField() + "_fcsf")
            .onKey((x) => _this.fillVariants())
            .placeHolder('global.attachment.search')
            .appendTo(headerSearchField);

        let onlyPablicSelectorOptions = new MagicTrueFalseSelectorOptions("pages.filechooser.only_public", false, true, false, true)
        onlyPablicSelectorOptions.setComponentListener(this.selectorListener)
        onlyPablicSelectorOptions.setParent(this)
        this.onlyPablicSelector = new MagicTrueFalseSelector(onlyPablicSelectorOptions).draw(fcp);


        headerConf.appendChild(fcp);

        let fcOnlyImages = h.span("fcom_class", null).appendTo(headerConf);

        let onlyImagesSelectorOptions = new MagicTrueFalseSelectorOptions("pages.filechooser.only_images", false, true, false, true)
        onlyImagesSelectorOptions.setComponentListener(this.selectorListener)
        onlyImagesSelectorOptions.setParent(this)
        this.onlyImagesSelector = new MagicTrueFalseSelector(onlyImagesSelectorOptions).draw(fcOnlyImages);


        this.content = h.div("fs_content").appendTo(form);

        h.img("close.png.png")
            .wh(16)
            .click(() => {
                _this.closeUploaded();
            })
            .appendTo(form);

        this.fillVariants();
    }


    read() {
        return this.#file?.getSrc();
    }

}

class DropDown extends FieldResolver {
    #dropDown;

    constructor(field) {
        super(field);
    }

    getDropDown() {
        return this.#dropDown;
    }

    create(value) {

        if (this.getFieldData().dataProviders) {
            value = this.getFieldData().dataProviders
        }

        this.rowWrapper = super.createFieldDiv();
        let row = this.createContainer().appendTo(this.rowWrapper);
        this.showDescription(row)

        let dropDown = dom.select(this.getField().getField());
        this.#dropDown = dropDown;
        let label = h.label(this.getField(), "").attr("for", this.getField());

        let optionsData = this.getSetting("dataProviders");
        if (optionsData == null || optionsData.length == 0) {
            log.error(
                "Data provider not set for this field " + this.getField().getField()
            );
        }

        if (Array.isArray(value)) {
            _.each(value, (val) => {
                dom.addOptionInSelect(this.#dropDown, val);
            })
        } else if (value != null) {
            dom.setCboValue(dropDown, value);
        }

        let div = h.div("dd_data");

        // $(dropDown).prettyDropdown();

        this.markElementsRequired({dropDown, div});

        this.showLabel(div);

        div.appendChild(label);
        div.appendChild(dropDown);
        div.appendChild(dom.errorPlate(this.getField().getField()));

        row.appendChild(div);

        return this.rowWrapper;
    }

    read(field) {
        return this.#dropDown.val();
    }

    setValue(val) {
        let value = val && val[this.getField().getField()]
        if (value) {
            this.#dropDown.setVal(value)
        }

        this.fireEvent(FieldResolver.EVENT_DROPDOWN_VALUE_SET, {
            dropDown: this,
            value: this.#dropDown.val(),
            inputElement: this.#dropDown
        })
    }
}

class DateElement extends FieldResolver {
    #chooser;

    #options;

    #dateDiv

    #value;

    constructor(field) {
        super(field);
    }

    dateListener = (event) => {
        if (_e(event.eventName, DateEvent.DATE_SELECTED)) {
            this.#value = event.data;
        }
    }

    create(value) {
        let needPreset = !!value;
        this.rowWrapper = super.createFieldDiv().cl("date-row");
        this.showDescription(this.rowWrapper)

        this.#dateDiv = h.div("chooser").text("pages.filechooser.select").appendTo(this.rowWrapper);

        let val = "";

        if (value && value[this.getField().getField()]) {
            val = _.getLocalDatefromMillisecond(value[this.getField().getField()].date, true); // 21-feb
            this.#dateDiv.text(val, false);
            this.#value = value[this.getField().getField()]; //milisec
        }

        this.showLabel(this.rowWrapper);

        this.#options = new DateOptions(this.#dateDiv, ".", "dd.mm.yyyy", this.getDate(value), this.dateListener, needPreset);

        this.#chooser = new MagicDateChooser(this.#options).init();

        return this.rowWrapper;
    }

    getDate(value) {
        if (_.isNull(value)) {
            return null
        }
        if (_.isNull(value[this.getField().getField()])) {
            return null
        }
        return _.getLocalDatefromMillisecond(value[this.getField().getField()].date, false)
    }

    setValue(data) {
        if (data && data[this.getField().getField()]) {
            this.#chooser.setNewDate(_.getLocalDatefromMillisecond(data[this.getField().getField()].date));
            this.#value = data[this.getField().getField()].date // milisec
        } else {
            this.#chooser.setNewDate(null)
            this.#dateDiv.text("pages.filechooser.select");
            this.#value = '';
        }
    }

    read() {

        return {
            "date": this.#value,
            "milliseconds": this.#value,
            "zone": "Etc/UTC",
            "template": null
        }
    }
}

class ObjectChooser extends FieldResolver {
    valueHolder;
    #dataPicker;
    #div;

    constructor(field) {
        super(field);
        this.field = field
    }

    create(value) {
        this.field
        this.rowWrapper = super.createFieldDiv();
        let row = h.div("row").appendTo(this.rowWrapper);

        this.showDescription(row)
        let div = h.div("chooser").appendTo(row);

        this.#div = div;
        this.valueHolder = h.input(dom.HIDDEN).id(this.getField().getField()).attr("data-type", this.getSetting("searchName")).appendTo(div);


        if (!utils.isEmpty(value)) {
            try {
                this.valueHolder.text(value[this.getField().getField()].key, false);
            } catch (error) {

            }
        }

        this.#dataPicker = new MagicRemoteChooser(
            this.#div,
            value == null ? [] : value[this.getField().getField()],
            this.valueHolder,
            "",
            this.getField(),
            this.getParent()
        );

        this.markElementsRequired({div});

        this.showLabel(row);


        //on add form we have a different key
        let errorSpan = dom.errorPlate(this.getField().getField());

        this.rowWrapper.add(errorSpan);

        return this.rowWrapper;
    }

    getDataPicker() {
        return this.#dataPicker;
    }

    read() {
        let res = +this.valueHolder.val();
        this.fireEvent(FieldResolver.EVENT_VALUE_READ, res);

        return res;
    }

    //
    setValue(value, isCustom = false) {

        if (_.isNull(value)) {
            this.valueHolder && this.valueHolder.text(null);
            this.fireEvent(FieldResolver.EVENT_VALUE_SET,  null)
            this.#dataPicker && this.#dataPicker.selectData(null)
            return
        }
    
        try {
            let fieldName = this.getField().getField()
            let id = this.getId(value, isCustom, fieldName);
            let resultValue = value[fieldName];
            if (_.isObject(resultValue)) {
                this.#dataPicker.setData(resultValue)
            } else {
                this.#dataPicker.selectData({"key": resultValue, "value": resultValue})
            }
            this.#dataPicker.init();
            if (isCustom) {
                this.#dataPicker.setData({});
                this.#dataPicker.getDataById(id, this.getField().getData().searchName)
            }
  
            this.valueHolder.text(id, false);
            this.fireEvent(FieldResolver.EVENT_VALUE_SET, resultValue == null ? null : resultValue[this.getField().getField()])
        } catch (error) {
            this.valueHolder.text(null);
            this.#dataPicker.setData({});
            this.#dataPicker.getWhere().setData('id', 0);
            this.#dataPicker.init();
            this.fireEvent(FieldResolver.EVENT_VALUE_SET, value == null ? null : value[this.getField().getField()])
        }
    }

    setPreselected(){
        this.#dataPicker
    }

    isDictionary = () => false;

    getId(value, isCustom, fieldName) {
        let id = null;
        if(_.isNull(value) || _.isObjectEmpty(value)){
            return 0
        }
        if (isCustom) {
            id = value.value;
        } else {
            id = value[fieldName].id;
            if (_.isNull(id)) {
                id = value[fieldName].key;
            }
        }
        return id;
    }
}

class ObjectChooserList extends FieldResolver {

    valueHolder;
    #dataPicker;
    #div;
    #chooserWrapper = h.div()
    #hiddenEl = h.input("hidden")
    #chooser
    #arr = []
    #divContainer = h.div('field_list')

    constructor(field) {
        super(field);
        this.field = field
    }

    create(value) {
        this.field
        this.rowWrapper = super.createFieldDiv();
        let row = h.div("row").appendTo(this.rowWrapper);

        this.showDescription(row)

        this.markElementsRequired({row});

        this.showLabel(row);
        this.initChooser()
        h.div().text("global.fields.add").andText(this.field.getField(), false).click(() => {
            this.#chooser.showForm()
        }).appendTo(this.rowWrapper)
        this.#divContainer.appendTo(this.rowWrapper)
        return this.rowWrapper;
    }

    setValue(value) {
        this.#arr = []
        this.rebuildContainer()
        let fieldName = this.getField().getField()

        // let v = value[fieldName]
        console.log(value);

    }

    initChooser() {
        this.#chooser = new MagicRemoteChooser(this.#chooserWrapper, null, this.#hiddenEl, "", MagicRemoteChooser.createMagicChooserHeader(this.field.getData().searchName), this);
    }

    getFormFieldListener() {
        return (event) => {
            if (_e(event.type, MagicRemoteChooser.EVENT_ITEM_SELECTED)) {
                let inArr = this.#arr.find((e) => {
                    if (_e(e.id, event.additional.id)) {
                        return true
                    }
                })
                if (!inArr) {
                    this.#arr.push(event.additional)
                    this.fireEvent(FieldResolver.EVENT_ADD_CHOOSER_TO_LIST, event.additional)
                    this.rebuildContainer()
                }
            }
        }
    }

    rebuildContainer() {
        this.#divContainer.text(null)
        _.each(this.#arr, (item, i) => {
            h.div("item").addIf(item.icon, h.img(item.icon, 16).wh(16))
                .add(h.div("item_name").text(item.name, false))
                .add(h.div("remove_item").text("remove", false).click(() => {
                    this.#arr.splice(i, 1)
                    this.fireEvent(FieldResolver.EVENT_REMOVE_CHOOSER_TO_LIST, item)
                    this.rebuildContainer()
                }))
                .appendTo(this.#divContainer)
        })
    }

    read() {
        return this.#arr.map((el) => el.id)
    }


}

class InputField extends FieldResolver {
    #holder;

    constructor(field) {
        super(field);
    }

    read() {
        if (this.getIsId() && parseInt(this.#holder.val()) == this.#holder.val()) {
            return parseInt(this.#holder.val());
        }
        return this.#holder.val();
    }

    setValue(value) {

        if (this.getField().getData().customized && value) {
            this.#holder.text(value.value, false);
            return;
        }

        let text = ''
        if (value) {
            text = value[this.getField().getField()];
        }
        this.#holder.text(text, false);
    }

    setThisValue(el, value) {
        if (el == null) {
            el = this.html().text(null);
        }
        if (value != null) {
            el.value = value;
        }
    }

    html() {
        return this.#holder;
    }

    createWebObject() {
        return h.input(this.getField().getType()).id(this.getField().getField());
    }

    create(value) {
        this.rowWrapper = super.createFieldDiv();
        let div = this.createContainer().appendTo(this.rowWrapper);
        let descr = h.div("description")
        if (_.e('checkbox', this.getType())) {
            descr.appendTo(this.rowWrapper);
        } else {
            descr.appendTo(div);
        }
        let el = this.createWebObject();
        this.#holder = el;

        if (this.toShowPlaceHolder()) {
            el.placeHolder(this.getPlaceHolder(), false);
        }

        this.markElementsRequired(el, div);

        this.setThisValue(el, value);

        if (value != null) {
            el.text(value[this.getField().getField()], false);
        }

        this.showLabel(div);
        this.showDescription(descr);
        this.addElementToInput(div, el);

        dom.errorPlate(this.getField().getField()).appendTo(this.rowWrapper);

        div.clIf(utils.e(this.getField().getType(), dom.HIDDEN), dom.HIDDEN);

        return this.rowWrapper;
    }

    addElementToInput(div, el) {
        h.from(div).add(el);
    }
}

class PasswordField extends InputField {
    constructor(field) {
        super(field);
    }

    createWebObject() {
        return h.input("password").noCache().id(this.getField().getField());
    }
}

class NumberField extends InputField {
    #hidden;
    #holder;

    constructor(field, hidden) {
        super(field);
        this.#hidden = hidden;
        this.#holder = h.input(this.getField().getType()).id(this.getField().getField());
    }

    read() {
        return this.#holder.val();
    }

    createWebObject() {
        return this.#holder;
    }

    getHolder() {
        return this.#holder
    }
}

class DoubleField extends InputField {

    constructor(field) {
        super(field);
    }

    read() {
        let inputFieldValue = super.read()
        return parseFloat(inputFieldValue)
    }

}

class TextArea extends InputField {
    #el;
    #editor;
    #data;

    constructor(field) {
        super(field);

        this.field = field;
    }

    createWebObject() {

        var field = this.field.getField();

        return this.#el = h.tag("textarea").noCache().id(new Date().getTime() + "_" + field);
    }

    read() {
        if (this.#editor) {

            return this.#editor.getContents();
        } else {

            return this.#el.val();
        }
    }

    setValue(value) {

        this.#data = value;

        if (value) {

            let val;

            if (utils.notNull(value[this.getField().getField()])) {
                val = value[this.getField().getField()];

                if (this.#editor) {
                    this.#editor.setContents(val);

                    return;
                } else {
                    this.#el.text(val, false);

                    return;
                }

            } else {
                val = value;

                if (this.#editor) {
                    this.#editor.setContents(val);
                    return;

                } else {
                    this.#el.text(value, false);

                    return;
                }
            }

        } else {
            if (this.#editor) {
                this.#editor.setContents("");

                return;
            } else {
                this.#el.text("", false)
            }
        }
    }

    afterAdd() {
        if (utils.isNull(this.#editor)) {
            this.#editor = utils.initMCE(this.#el);

            this.setValue(this.#data)
        }

        this.fireEvent(FieldResolver.EVENT_AFTER_ADD, this);

        return;
    }
}

class CheckBox extends InputField {
    constructor(field) {
        super(field);
        this.field = field
    }

    create(value) {
        let label = this.getField().getData().translation
        this.rowWrapper = super.createFieldDiv()

        let options = new MagicTrueFalseSelectorOptions(label, this.field.getData().checked, true, false, true)
        this.selector = new MagicTrueFalseSelector(options).draw(this.rowWrapper);

        return this.rowWrapper;
    }

    setValue(val) {
        if (_.isNull(val)) {
            this.selector.setValue(this.field.getData().checked)
            return;
        }
        let value = val[this.getField().getField()];
        this.selector.setValue(value)
    }

    read() {
        return this.selector.getValue()
    }
}

class CollectionField extends InputField {

    create() {
        this.rowWrapper = super.createFieldDiv();
        let row = this.createContainer().appendTo(this.rowWrapper);
        this.showLabel(row);
        this.showDescription(row)
        this.input = h.input('text').appendTo(row);
        return this.rowWrapper;
    }

    read() {
        return this.input.val().split(" ")
    }

    setValue() {

    }

}

class DictionaryChooser extends ObjectChooser {
    constructor(field) {
        super(field);
    }

    setValue(data) {
        if (data && data.value) {
            DictionaryChooser.getDictionaryById(data.value, (data) => {
                super.setValue({[this.getField().getField()]: data.content[0]});
            })
        } else super.setValue(null);
    }

    static getDictionaryById(id, callback) {
        let req = new MagicRequest();
        req.query.push(
            {
                "field": "id",
                "value": +id,
                "searchName": "",
                "dataType": "dictionary",
                "type": "="
            }
        )
        Executor.runPostWithPayload(Environment.dataApi + "/api/v2/shared/dictionary/root/fetch/data", callback, req)
    }

    isDictionary = () => true;
}

class CategoryAttributesField extends InputField {

    #attributesContainer;
    #attrArr = [];

    constructor(field) {
        super(field);
        this.field = field
        this.#attributesContainer = h.div("attributes_container");
    }

    create(value) {
        this.rowWrapper = super.createFieldDiv();
        let row = this.createContainer().appendTo(this.rowWrapper);
        h.div("build_collection").text("global.field.add_attributes").click(() => {
            this.addAttributes();
        }).appendTo(row)
        this.showLabel(row);
        this.showDescription(row)
        this.#attributesContainer.appendTo(row)
        return this.rowWrapper;
    }

    addAttributes(val) {
        let attr = new AttributeItem(this)
        this.#attrArr.push(attr)
        if (_.notNull(val)) {
            attr.setValue(val)
        }
    }

    getAttributeContainer() {
        return this.#attributesContainer
    }

    removeItem(e) {
        this.#attrArr = this.#attrArr.filter((elem) => elem != e)
        _.each(this.#attrArr, (l) => {
            console.log(l.getValue());
        })
    }

    setValue(value) {
        this.#attributesContainer.text(null)
        this.#attrArr = []
        if (value) {
            let arr = value[this.getField().getField()];
            _.each(arr, (item) => {
                this.addAttributes(item)
            })
        }

    }

    read() {
        return this.#attrArr.map(e => e.getValue())
    }

}

class AttributeItem {
    #name
    #selector
    #parent
    #options = [
        {"label": "text", "value": "text"},
        {"label": "number", "value": "number"},
        {"label": "double", "value": "double"},
        {"label": "collections", "value": "collections"},
        {"label": "dictionary", "value": "dictionary"},
        {"label": "checkbox", "value": "checkbox"},
        {"label": "text_area", "value": "text_area"},
        {"label": "file", "value": "file"},
        {"label": "files", "value": "files"},
        {"label": "object_chooser", "value": "object_chooser"}
    ]

    constructor(parent) {
        this.#parent = parent
        this.#name = h.input("text")
        this.#selector = _Select.create().addOptions(this.#options)
        this.build()
    }

    build() {
        let div = h.div("attribute_item")
            .add(h.div("attribute_name").text("global.field.name").add(this.#name))
            .add(h.div("attribute_type").text("global.field.type").add(this.#selector))
            .add(h.div('remove').text("global.fields.remove").click((e) => {
                div.remove();
                this.#parent.removeItem(e)
            })).appendTo(this.#parent.getAttributeContainer())
    }

    getValue() {
        let att = {
            "name": this.#name.val(),
            "type": this.#selector.getSelectedValue()
        }
        return att
    }

    setValue(val) {
        this.#name.text(val.name, false)
        this.#selector.selectValue(val.type)
    }

}

class CategoryAttributesValuesField extends InputField {

    #attributesValuesContainer
    #arrayValues = []

    //arrayValues contains field name and fieldResolver like [{name: name, amount: class Number}, {name: name, amount: class Number}]

    constructor(field) {
        super(field);
        this.field = field
        this.#attributesValuesContainer = h.div("attributes_values_container");
    }

    create(value) {
        this.rowWrapper = super.createFieldDiv();
        let row = this.createContainer().appendTo(this.rowWrapper);
        this.showLabel(row);
        this.showDescription(row)
        this.#attributesValuesContainer.appendTo(row)

        this.fireEvent(FieldResolver.EVENT_VALUES_CONTAINER_CREATED, {
            div: this.#attributesValuesContainer,
            field: this
        })
        return this.rowWrapper;
    }

    setValue(value) {
        this.#arrayValues = [];
        this.#attributesValuesContainer.text(null);

    }

    read() {
        return this.getValues().map((e) => {
            let newobj = {
                name: e.name,
                value: e.value.read(),
                formValue: ""
            }
            return newobj
        })
    }

    getValues() {
        return this.#arrayValues
    }

    setValues(values) {
        this.#arrayValues = values
    }

}


