class h {
    static #_____items = [];
    #el;

    isH = true;

    isInput = false;

    #style;

    static span(cl, text = null, translate = false) {
        let he = h.tag("span");
        he.clIf(cl, cl);
        if (utils.notNull(text)) {
            he.text(text, translate);
        }
        return he;
    }
    prependToIf(condition, to) {
        if (to == null) {
            log.debug("trying prepend to null object, ignoring");
            return this;
        }
        if(condition){
            to.isH ? to.get().prepend(this.get()) : to.prepend(this.get());
        }

        return this;
    }

    alt(text, toTranslate = false){
        if (toTranslate) {
            text = _.$(text);
        }
        this.get().title = text;
        this.get().alt = text;
        return this;
    }

    static textElement(cl = null, placeholder = null, toTranslate) {
        return h.tag('input').placeHolderIf(_.notNull(placeholder), placeholder, toTranslate).clIf(_.notNull(cl), cl)
    }

    static numberElement(cl = null, placeholder = null, toTranslate) {
        return h.tag('number').placeHolderIf(_.notNull(placeholder), placeholder, toTranslate).clIf(_.notNull(cl), cl)
    }

    handleEvent(eventName, func) {
      if(eventName && func) {
        this.get().addEventListener(eventName, (e) => {
          func(e, this);
      });
      }

        return this;
    }
    handleEventIf(cond , eventName, func) {
        if (cond){
            this.handleEvent(eventName, func)
        }

        return this;
    }



    childNodesAsArray(toWrap = true){
        let res = [];
        let children = this.get().children;
        if (_.isEmpty(children)){
            return res;
        }
        for (let i =0; i< children.length; i ++ ){
            let el = children.item(i);
            res.push(toWrap ? h.from(el) : el);
        }
        return  res;
    }
    firstChildNodeOfClass(cl , toWrap = true){
        let res = this.childNodesAsArray()
        return _.find(res, (item)=> {return (h.from(item)).ccl(cl)});
    }

    cloneNodeEl(){
       let clone = this.get().cloneNode(true)
       return h.from(clone)
    }

    remove() {
        this.get().remove();
        return this;
    }

    getChildsLength(){
      return this.get().children.length;
    }

    static fromId(id, where = null) {
        if(_.isNull(where)){
            return h.from(document.getElementById(id));
        }
        if(where instanceof h){
            return h.from(where.get().querySelector("#"+id));
        }
        return h.from(where.querySelector("#"+id));
    }


    static fromFirst(selector) {
        return h.from(document.querySelector(selector));
    }

    firstChildByClass(className) {
        return h.from(this.get().querySelector(className))
    }

    static from(el) {
        if (utils.isNull(el)) {
            log.trace("trying to add null element, ignoring it");
            return null;
        }
        if (el.isH) {
            return el;
        }
        let he = h.h();
        he.#el = el;
        return he;
    }

    static input(type) {
        let he = h.h();
        h.isInput = true;
        he.#el = document.createElement("input");
        he.attr("type", type);
        return he;
    }

    inputFunc(f) {
        let _this = this;
        this.get().oninput = function (e) {
            f(_this.get(), e);
        };
        return this;
    }


    noCache() {
        this.get().autocomplete = "off";
        return this;
    }

    clIf(toAdd, cl) {
        if (toAdd) {
            this.cl(cl);
        }

        return this;
    }

    addIf(toAdd, ...elements) {
        if (toAdd) {
            _.each(elements, el => {
                this.add(el instanceof Function ? el() : el);
            })
        }
        return this;
    }

    has(attr) {
        return utils.notNull(this.getA(attr));
    }

    attrIf(b, attr, value) {
        if (b) {
            this.attr(attr, value);
        } else {
            this.attr(attr, null);
        }
        return this;
    }

    get() {
        return this.#el;
    }

    parent(toWrap = true) {

        if (toWrap) {
            let item = this.get().parentElement;
            if (item == null){
                return null; //avoid to have null pointer in
            }
            return h.from(this.get().parentElement);
        }

        return this.get().parentElement;
    }

    next(toWrap = true) {
        if (toWrap) return h.from(this.get().nextElementSibling);

        return this.get().nextElementSibling;
    }

    addBefore(elToAdd, beforeElement) {
        this.get().insertBefore(
            elToAdd.isH ? elToAdd.get() : elToAdd,
            beforeElement.isH ? beforeElement.get() : beforeElement
        );
        return this;
    }

    containsTag(tag) {
        let items = utils.isEmpty(this.get().querySelector(tag));
        return utils.notNull(items) && items.length > 0;
    }

    val() {
        return utils.isNull(this.get().value)
            ? this.get().innerHTML
            : this.get().value;
    }

    disable(){
        this.setA('disabled', true)
        return this
    }

    enable(){
        this.get().removeAttribute('disabled')
        return this
    }

    setVal(val) {
        utils.isNull(this.get().value)
            ? this.get().innerHTML = val
            : this.get().value = val;

        return this;
    }

    remove() {
        this.get().remove();
        return this;
    }

    static queryById(doc, id) {
        return h.from(doc.get().querySelector('.'+id));
    }

    static fromFirst(selector) {
        return h.from(document.querySelector(selector));
    }

    static from(el) {
        if (utils.isNull(el)) {
            log.trace("trying to add null element, ignoring it");
            return null;
        }
        if (el.isH) {
            return el;
        }
        let he = h.h();
        he.#el = el;
        return he;
    }

    static input(type) {
        let he = h.h();
        h.isInput = true;
        he.#el = document.createElement("input");
        he.attr("type", type);
        return he;
    }


    inputFunc(f) {
        let _this = this;
        this.get().oninput = function (e) {
            f(_this.get(), e);
        };
        return this;
    }

    noCache() {
        this.get().autocomplete = "off";
        return this;
    }

    hideIf(toHide) {
        if (!toHide) return this;

        return this.hide();
    }

    clIf(toAdd, cl) {
        if (toAdd) {
            this.cl(cl);
        }

        return this;
    }


    has(attr) {
        return utils.notNull(this.getA(attr));
    }

    attrIf(b, attr, value) {
        if (b) {
            this.attr(attr, value);
        } else {
            this.attr(attr, null);
        }
        return this;
    }

    get() {
        return this.#el;
    }

    parent(toWrap = true) {
        if (toWrap) return h.from(this.get().parentElement);

        return this.get().parentElement;
    }

    next(toWrap = true) {
        if (toWrap) return h.from(this.get().nextElementSibling);

        return this.get().nextElementSibling;
    }

    addBefore(elToAdd, beforeElement) {
        this.get().insertBefore(
            elToAdd.isH ? elToAdd.get() : elToAdd,
            beforeElement.isH ? beforeElement.get() : beforeElement
        );
        return this;
    }

    containsTag(tag) {
        let items = utils.isEmpty(this.get().querySelector(tag));
        return utils.notNull(items) && items.length > 0;
    }

    val() {
        return utils.isNull(this.get().value)
            ? this.get().innerHTML
            : this.get().value;
    }

    setVal(val) {
        utils.isNull(this.get().value)
            ? this.get().innerHTML = val
            : this.get().value = val;

        return this;
    }

    onKey(func) {
        let _this = this;
        this.get().onkeyup = function (e) {
            func(_this.get(), e);
        };
        return this;
    }

    byId(id) {
        let res = h.#_____items[id].#el;
        if (res != null) {
            return res.#el;
        }
        return res;
    }

    static tag(tag) {
        let he = h.h();
        he.#el = document.createElement(tag);
        return he;
    }

    href(href) {
        this.get().href = href;
    }

    add(...elements) {
        _.each(elements, he => {
            if (he == null) {
                log.debug("trying to add null object, ignoring");
                return;
            }
            he.isH ? this.get().append(he.get()) : this.get().append(he);
        })
        return this;
    }

    prependTo(to) {
        to.isH ? to.get().prepend(this.get()) : to.prepend(this.get());
        return this;
    }

    static message(text) {
        dom.toast(_.$(text));
    }

    toHtml() {
        return this.get().outerHTML;
    }

    getContent() {
        return this.get().innerHTML;
    }

    setContent(cont){
        this.get().innerHTML = cont;
        return this
    }

    style() {

        return this.getStyle();
    }

    zIndex(val) {

        this.getStyle().zIndex(val);

        return this;
    }


    static img(path, size = 1) {
        if (Environment.defaultTemplate === undefined) {
            Environment.defaultTemplate = "";
        }

        if (_.isNull(path)) {
            return h.img("null.png");
        }

        let img = h.tag("img");

        if (utils.startsWith(path, Environment.fileStorageApi)) {
            if (!_e(size, 1)) {
              let file = FileFactory.load(path, new FileDisplayOptions(size, size, "loaded", size, false));

              if(file.getIsDeleted()) img.src(file.getSrc());

              else {
                img.src(file.getSrc() + "/" + size);
                img.attr("have-full-path", "true");
              }

                return img;
            }
        }

        let realPath =
            (path.trim().indexOf("http") == 0
                ? path
                : Environment.self + Environment.defaultTemplate + "images/" + path) +
            "?v=" +
            Environment.version;
        img.src(realPath);

        return img;
    }


    first(toWrap = true) {
        if (toWrap) return h.from(this.get().children[0]);
        return this.get().children[0];
    }

    second(toWrap = true) {
        if (toWrap) return h.from(this.get().children[1]);
        return this.get().children[0];
    }

    child(num, toWrap = true) {
        if (toWrap) return h.from(this.get().children[num]);
        return this.get().children[num];
    }

    performClick() {
        this.get().click();
        return this;
    }

    performClickIf(cond) {
        if (cond) this.performClick();
        return this;
    }

    bgColor(color) {
        this.get().style.backgroundColor = color;

        return this;
    }

    color(color) {
        this.get().style.color = color;

        return this;
    }

    draggable(onStart = null, onDrop = null) {
        this.attr("draggable", true);
        this.handleEvent("dragstart", onStart);
        this.handleEvent("dragend", onDrop);

        return this;
    }

    draggableIf(cond, onStart = null, onDrop = 0) {
        if (cond) {
            this.draggable(onStart, onDrop);
        }
        return this;
    }

    dragOver(func) {
        return this.handleEvent("dragover", func);
    }

    dragDrop(func) {
        return this.handleEvent("drop", func);
    }

    dragLeave(func) {
        return this.handleEvent("dragleave", func);
    }

    dragEnd(func) {
        return this.handleEvent("dragend", func);
    }

    last(toWrap = true) {
        if (toWrap)
            return h.from(this.get().children[this.get().children.length - 1]);
        return this.get().children[this.get().children.length - 1];
    }

    firstByClass(cl, into = true, wrap = false) {
        if (into) {
            let nodes = this.get().children;
            return _.each(nodes, (noda)=>{
                if (noda.classList != null && noda.classList.contains(cl)) {
                    return wrap ? h.from(noda) : noda;
                }
            })
        } else {
            let el = this.get();
            while (!el.classList.contains(cl)) {
                if (el.parentElement == null) {
                    el = null;
                    break;
                }

                el = el.parentElement;
            }
            return wrap ? h.from(el) : el;
        }

        return null;
    }

    static a(href, text, toTranslate = false, targetBlank = false) {
        let a = h.tag("a");
        a.href(href);
        a.text(text, toTranslate);

        if (targetBlank) {
            a.attr("target", "_blank")
        }

        return a;
    }

    static magicView(objectId, remoteType, rootData = false, fav = null, listener = null, listRequest = null, dataRequest = null) {
        return new MagicView(objectId, null, remoteType, rootData, fav, listener, listRequest, dataRequest);
    }

    static textArea(text, toTranslate = false) {
        let a = h.tag("textarea");
        a.text(text, toTranslate);
        return a;
    }

    blur(f) {
        let _this = this;
        this.get().onblur = function (e) {
            f(_this.get(), e);
        };
        return this;
    }

    onFocus(f){
        let _this = this;
        this.get().onfocus = function (e) {
            f(_this.get(), e);
        };
        return this;
    }

    onInput(f){
        let _this = this;
        this.get().oninput = function (e) {
            f(_this.get(), e);
        };
        return this;
    }

    click(f, preventParentClick = true) {
        let _this = this;
        this.get().onclick = function (e) {
            f(_this.get(), e);
            if (preventParentClick)
                e.stopPropagation();
        };
        return this;
    }

    clickIf(condition, f, preventive = true) {
        if (condition) {
            this.click(f, preventive);
        }
        return this;
    }

    clickIfPresent(f, preventParentClick = true) {
        if (_.notNull(f)) {
            this.click(f, preventParentClick);
        }
        return this;
    }

    doubleClick(f, preventParentClick = true) {
        let _this = this;
        this.get().ondblclick = function (e) {
            f(_this.get(), e);
            if (preventParentClick)
                e.stopPropagation();
        };
        return this;
    }

    doubleClickIf(condition, f, preventive = true) {
        if (condition) {
            this.doubleClick(f, preventive);
        }
        return this;
    }

    clickPreventive(f) {
        let _this = this;
        this.get().onclick = function (e) {
            e.preventDefault();
            f(_this.get(), e);
            return false;
        };
        return this;
    }

    checked() {
        return this.get().checked;
    }

    change(f) {
        let _this = this;
        this.get().onchange = function (e) {
            f(_this, e);
        };
        return this;
    }

    changeIf(condition, f) {
        if (condition) {
            this.change(f);
        }
        return this;
    }


    setA(attr, value) {
        if (value == null) {
            this.get().removeAttribute(attr);
        } else {

            this.get().setAttribute(attr, value);
        }
        return this;
    }

    setAIf(cond, attr, value) {
        if (cond) {
            this.setA(attr, value);
        }

        return this;
    }

    getA(attr) {
        return this.get().getAttribute(attr);
    }

    getData(attr) {
        return this.getA("data-" + attr);
    }

    findData(attr){
        let res = this.getData(attr);
        if (res == null){
            let children = this.childNodes(null, true);
            if (children.length > 0){
                return _.each(children, (child) => {
                    var res = child.findData(attr);
                    if (_.notNull(res)) {
                        return res;
                    }
                });
            }
        }
        return null;
    }
    findFirstContainingClass(cl){
       if (this.ccl(cl)){
           return this;
       }

       let parent = this.parent(true);
       if (_.notNull(parent)){
           return parent.findFirstContainingClass(cl)
       }

    }

    getDataId(attr) {
        return this.getA("data-id");
    }

    setData(attr, value) {
        this.get().setAttribute("data-" + attr, value);
        return this;
    }

    static anyByClassExists(cl) {
        return document.getElementsByClassName(cl).length > 0;
    }

    width(width) {
        this.getStyle().width(width);

        return this;
    }

    srcHover(img) {
        let _this = this;
        let _src = this.get().src;
        this.hover(() => {
            this.src(img);
        });
        this.nover(() => {
            this.src(_src);
        });
    }

    getOffsetLeft() {
        return this.get().offsetLeft;
    }

    getOffsetTop() {
        return this.get().offsetTop;
    }

    getRect() {
        return this.get().getBoundingClientRect();
    }

    hover(f) {
        this.get().onmouseover = function (e) {
            f(this, e);
        };
    }

    nover(f) {
        this.get().onmouseleave = function (e) {
            f(this, e);
        };
    }

    scrollIntoView(block = "center", inline = "start") {
        this.get().scrollIntoView({
            block: block,
            inline: inline
        })

        return this;
    }

    eachOfClass(cl, func, travers = false, toWrap = false) {
        let children = this.get().querySelectorAll("." + cl);
        let ret = null;
        if (utils.isNull(children) || children.length < 1) {
            return;
        }
        for (let i = 0; i < children.length; i++) {
            ret = func(toWrap ? h.from(children[i]) : children[i]);
            if (utils.notNull(ret) && !utils.e(ret, utils.CONTINUE)) {
                return ret;
            }

            if (travers && dom.hasChildren(children[i])) {
                h.from(children[i]).eachOfClass(cl, func);
            }
        }

        return this;
    }

    eachOf(func, tag = null, toH = false) {
        let children = this.childNodes(tag);
        let ret = null;
        if (utils.isNull(children) || children.length < 1) {
            return;
        }

        _.each(children, (child) => {
          if(toH) {
            ret = func(h.from(child));
          } else {
            ret = func(child);
          }

            if (utils.notNull(ret) && !utils.e(ret, utils.CONTINUE)) {
                return ret;
            }
        })

        return this;
    }

    hide() {
        return this.cl(dom.HIDDEN);
    }

    hideModal() {
        MagicPage.hideWindow(this)
        return this
    }

    showModal() {
        MagicPage.showWindow(this)
        return this
    }

    show() {
        return this.rcl(dom.HIDDEN);
    }

    showIf(cond){
      if(!cond) return this;

      return this.show();
    }

    height(height) {
        this.getStyle().height(height);

        return this;
    }

    wh(wh) {
        this.width(wh);
        this.height(wh);
        return this;
    }

    data_id(id) {
        this.attr("data-id", id);
        return this;
    }

    toggle(cl) {
        return this.get().classList.toggle(cl);
    }

    ccl(cl) {
        if (utils.isNull(this.get())) {
            return false;
        }
        return this.get().classList.contains(cl);
    }

    data_text(text) {
        this.attr("data-text", text);
        return this;
    }

    withAllChilds(tag = null, func, toWrap = true) {
      if(_.isNull(tag)) tag = "**";
      let ret = null;
      let children = Array.from(this.get().getElementsByTagName(tag.toUpperCase()));

      _.each(children, (child) => {
        if(toWrap) {
          ret = func(h.from(child));
        } else {
          ret = func(child);
        }

        if (utils.notNull(ret) && !utils.e(ret, utils.CONTINUE)) {
            return ret;
          }
      })

      return this;
    }

    id(id) {
        this.get().id = id;
        return this;
    }

    getId() {
        return this.get().id;
    }

    src(src) {
        if (utils.ne(src.indexOf("http"), 0)) {
            if (utils.ne(src.indexOf(Environment.self), 0)) {
                if (!src.startsWith("/images")) {
                    src =
                        Environment.self + Environment.defaultTemplate + "/images/" + src + "?v=" + Environment.version;
                } else {
                    src = Environment.self + Environment.defaultTemplate + src + "?v=" + Environment.version;
                }
            }
        }

        if (this.ccl("__DIV_IMAGE__")){

            this.get().style.backgroundImage = "url('" + src + "')";
        } else {

            this.get().src = src;
        }
        return this;
    }

    cl(obj) {
        if (obj.trim().indexOf(" ") > -1) {
            let cls = obj.split(" ");
            utils.each(cls, (cl) => {
                this.get().classList.add(cl);
            });
        } else {
            this.get().classList.add(obj.trim());
        }
        return this;
    }

    rcl(cl) {
        this.get().classList.remove(cl);
        return this;
    }

    textIf(condition, val, translate = true){
        if(condition){
            return this.text(val, translate)
        }
        return this
    }

    _textIf(condition, val) {
        if (condition) {
            return this._text(val, false);
        }
        return  this;
    }

    setDimension(dimensionClass){
        if(!(dimensionClass instanceof Dimension)){
            return this
        }
        if(_.notNull(dimensionClass.getWidth())){
            this.getStyle().width(dimensionClass.getWidth(), dimensionClass.getUnit());
        }

        if(_.notNull(dimensionClass.getHeight())){
            this.getStyle().height(dimensionClass.getHeight(), dimensionClass.getUnit());
        }
        return this;
    }


    text(val, translate = true) {

        if (val == null) {
            translate = false;
            val = "";
        }

        if(_.isTrue(translate) && _.containsText(val, false, " ", "!", ",", ";", ":", "]", "[") ){
            throw Error("key should be in the fotmat 'xx.xx.xx_xx*' " + val)
        }


        if (translate) val = _.$(val);

        this.get() instanceof HTMLInputElement ||
        this.get() instanceof HTMLTextAreaElement
            ? (this.get().value = val)
            : (this.get().innerHTML = val);
        return this;
    }

    _text(val){
        return this.text(val, false);
     }


    andText(text, toTranslate) {
        if (this.get() instanceof HTMLInputElement || this.get() instanceof HTMLTextAreaElement) {
            this.get().value =
                this.get().value + (toTranslate ? _.$(text) : text);
        } else {
            this.get().innerHTML =
                this.get().innerHTML + (toTranslate ? _.$(text) : text);
        }

        return this;
    }

    andTextIf(condition, text, toTranslate = false){
        if (condition){
            this.andText(text, toTranslate);
        }
        return this
    }

    readOnly(boolean = true) {
        if (typeof boolean == "boolean") {
            this.get().readOnly = boolean;
        }
        return this;
    }

    appendTo(obj) {

        if (obj?.isH) {
            obj.add(this.get());
        } else {
            obj.appendChild(this.get());
        }
        return this;
    }

    appendToIf(condition, obj) {
        if(condition){
            if (obj.isH) {
                obj.add(this.get());
            } else {
                obj.appendChild(this.get());
            }
        }
        return this;
    }

    after(obj) {
        if (obj.isH) {
            obj.get().after(this.get());
        } else {
            obj.after(this.get());
        }
        return this;

    }

    appendToBody() {
        this.appendTo(document.body);
        return this;
    }

    childNodes(tag = null, toWrap = false) {

        if (tag == null) {
            if(toWrap){
                let arr = [];
                _.each(this.get().childNodes, (child)=>{
                    arr.push(child);
                }, true);
                return arr
            }else{
                return this.get().childNodes;
            }
        } else {
            let children = this.get().childNodes;
            let res = [];
            utils.each(children, (child) => {
                if (utils.e(child.tagName, tag)) {
                    res.push(toWrap ? h.from(child) : child);
                }
            });

            return res;
        }
    }

    /**
     * For some reason old method can be still used, so we just duplicate it now
     * @param obj
     * @returns {h.h}
     */
    appendChild(obj) {
        this.add(obj);
        return this;
    }

    placeHolderIf(condition, text, toTranslate = true) {
        if (condition) {
            this.placeHolder(text, toTranslate)
        }

        return this;
    }

    placeHolder(text, toTranslate = true) {
        if (text == null) {
            this.get().placeHolder = "";
        }
        this.get().placeholder = toTranslate
            ? _.$(text, toTranslate)
            : text;

        return this;
    }


    focus() {
        this.get().focus()
        return this
    }

    // onFocus(f) {
    //     this.handleEvent("focus", (f));

    //     return this;
    // }

    onKeydown(f) {

        this.handleEvent("keydown", f);

        return this;
    }

    onKeypress(f) {

        this.handleEvent("keypress", f);

        return this;
    }

    tagName(tag = null) {

        if (tag == null){
            return this.get().tagName;
        }

        return _.e(this.get().tagName, tag);
    }

    isInPage() {
        return (this.get() == document.body) ? false : document.body.contains(this.get());
    }

    static inputBtn(text) {
        let he = h.h();
        h.isInput = true;
        he.#el = document.createElement("input");
        he.attr("type", "button");
        he.text(text)
        return he;
    }

    setEl(el) {
        this.#el = el;
        return this
    }

    attr(attr, value) {
        if (attr === "checked") {
            this.get().cheked = value;
        }

        if (utils.isNull(value)) {
            this.get().removeAttribute(attr);
        }
        this.get().setAttribute(attr, value);
        return this;
    }

    attrChecked(value) {
        if (value) {
            this.get().setAttribute("checked", "checked");
        }
        return this;
    }

    static div(cl = null) {
        let he = h.tag("div");
        he.clIf(cl, cl);
        return he;
    }

    static label(f, text, totranslate = true) {
        let he = h.tag("label");
        he.text(text, totranslate);
        he.attr("for", f.isH ? f.get().id : f);
        return he;
    }

    static h() {
        return new h();
    }

    static divImg(src, size = 1) {

        let img = h.img(src).get().src;
        let div = h.div("div_image").cl("__DIV_IMAGE__");
        div.get().style.display = "block"
        div.get().style.backgroundImage = "url('" + img + "')";
        div.get().style.backgroundSize = "contain";

        if (utils.startsWith(src, Environment.fileStorageApi)) {
            if (!_e(size, 1)) {
              let file = FileFactory.load(src, new FileDisplayOptions(size, size, "loaded", size, false));
              if(file.getIsDeleted()) {
                div.get().style.backgroundImage = "url('" + file.getSrc() + "')";
              } else {
                div.get().style.backgroundImage = "url('" + file.getSrc() + '/' + size + "')";
              }
            }
        }

        return div;
    }


    getSrc() {
        return this.get().src;
    }

    pointer() {
        this.getStyle().pointer();

        return this;
    }

    getStyle() {
        if (_.isNull(this.#style)) {
            this.#style = new Style(this);
        }

        return this.#style;
    }

    fixed() {
        this.getStyle().fixed();

        return this;
    }
}

class Style {
    #style;
    #h;

    constructor(h) {
        this.#h = h;
        this.#style = h.get().style;
    }

    fixed() {
        this.#style.position = "fixed";
    }

    background(background) {
        this.#style.background = background;
        return this;
    }

    transform(str) {
      this.#style.transform = str;
      return this;
    }

    color(color) {
        this.#style.background = color;
        return this;
    }

    size(fontSize) {
        this.#style.fontSize = fontSize;
        return this;
    }

    padding(padding) {
        this.#style.padding = padding + "px";
    }

    paddingLeft(padding) {
        this.#style.paddingLeft = padding + "px";
        return this;
    }

    paddingRight(padding) {
        this.#style.paddingRight = padding + "px";
    }

    roundBorder() {
        this.radius("50%");
        return this;
    }

    pointer() {
        this.#style.cursor = "pointer";
        return this;
    }

    radius(radius) {
        this.#style.borderRadius = radius;
    }

    border(border) {
        this.#style.border = border;
        return this;
    }

    grid() {
        this.#style.display = "grid";
        return this;
    }

    gridRepeatColumn(count, size) {
        this.#style.gridTemplateColumns = `repeat(${count}, ${size})`;
        return this;
    }

    gridRepeatRow(count, size) {
        this.#style.gridTemplateRows = `repeat(${count}, ${size})`;
        return this;
    }

    gridArea(rowStart, columnStart, rowEnd, columnEnd) {
        this.#style.gridArea =
            rowStart + "/" + columnStart + "/" + rowEnd + "/" + columnEnd;

        return this;
    }

    top(val) {
        this.#style.top = val + "px";

        return this;
    }

    bottom(val) {
        this.#style.bottom = val + "px";

        return this;
    }

    left(val) {
        this.#style.left = val + "px";

        return this;
    }

    absolute() {
        this.#style.position = "absolute";

        return this;
    }

    gridRowStart(val) {
        this.#style.gridRowStart = val;

        return this;
    }

    back() {
        return this.#h;
    }

    rotateDeg(deg) {
        this.#style.transform = `rotate(${deg}deg)`;

        return this
    }

    horizontMirror() {
        this.#style.transform = "scale(-1, 1)";

        return this;
    }

    verticalMirror() {
        this.#style.transform = "scale(1, -1)";

        return this;
    }

    mirror() {
        this.#style.transform = "scale(-1, -1)";

        return this;
    }

    zIndex(val) {
        this.#style.zIndex = val;

        return this;
    }

    display(what){
        this.#style.display = what;
        return this;
    }

    width(width, unit = "px") {
        this.#style.width = width + unit;

        return this;
    }

    height(height) {
        this.#style.height = height + "px";

        return this;
    }

    right(val) {
        this.#style.right = val + "px";

        return this;
    }

    removeItem(name) {
        this.#style.removeProperty(name);

        return this;
    }

    persentWidth(val) {
        this.#style.width = val + "%"
    }
}

class _Select extends h {

    static create(){
        let sel = h.tag('select');
        return new _Select().setEl(sel.get())
    }

    static from(el){
       return new _Select().setEl(el.isH ? el.get() : el)
    }

    addOptions(option, label = 'label', value = 'value', toTranslate = false){
        _.each(option, (item)=>{
            _Select.tag('option')
            .text(item[label], toTranslate)
            .setA("value", item[value])
            .setData("option", item[value])
            .appendTo(this)
        });
        return this;
    }

    addOption(label = 'label', value = 'value', toTranslate = false){
        _Select.tag('option')
        .text(label, toTranslate)
        .setData("option", value)
        .appendTo(this)
        return this;
    }

    selectValue(value){
        _.each(this.get().options, (option, i)=> {
            if (_e(option.value, value)) {
                this.get().selectedIndex = i;
            };
        });
        return this;
    }

    allowNoSelection(){
        this.addOption('no_selected', -1 );
        return this;
    }

    selectedOption(toWrap = false){
        let res =  this.get().options[this.get().selectedIndex];
        return toWrap ? h.from(res): res;
    }

    getSelectedValue(value = "value"){
        return this.selectedOption(true).getData("option")
    }

    getSelectedIndex(){
        return this.selectedOption().index;
    }



}

class MultiSelector{
    #options;
    #isTranslate;
    #where;
    #label;
    #value;
    colorCounter = 0

    static COLOR = ["#FFFACD", "#84ff91", "#E6E6FA", "#1E90FF", "#FF00FF", "#87CEEB"];
    static VALUE = "value";
    static SELECTED = "selected";

    constructor(options, where, isTranslate = true, label = "label", value = "value"){
        this.#options = options;
        this.#isTranslate = isTranslate;
        this.#where = where;
        this.#label = label;
        this.#value = value;
        this.init();
    }

    init(){
        this.buildWindow();
        this.buildOption();
    }

    buildWindow(){
        this.window = h.div("multi_select_window").appendTo(this.#where);
        this.selectedWindow = h.div("selectes_window").appendTo(this.window);
        this.optionWindow = h.div("option_window").appendTo(this.window);
    }

    buildOption(){
       _.each(this.#options, (opt)=>{
            h.div("option").text(opt[this.#label], this.#isTranslate).setData(MultiSelector.VALUE, opt[this.#value]).click((el)=>{
                if(!h.from(el).ccl(MultiSelector.SELECTED)){
                   this.createSelectedDiv(h.from(el))
                }else{
                   this.unselectDiv(h.from(el))
                }
            }).appendTo(this.optionWindow)
        })
    }

    createSelectedDiv(el){
        let value = el.getData(MultiSelector.VALUE);
        el.cl(MultiSelector.SELECTED)
        let selectedDiv = h.div("selected_option").text(el.val(), false).setData(MultiSelector.VALUE, value)
            .add(h.div("div_image")
                .cl("close_option")
                .click(()=>{
                selectedDiv.remove();
                _.each(this.optionWindow.childNodes(), (opt)=>{
                    if(_e(opt.getData(MultiSelector.VALUE), value)){
                        opt.rcl(MultiSelector.SELECTED);
                    }
                }, true)

                }))
            .appendTo(this.selectedWindow)

        selectedDiv.style().background(MultiSelector.COLOR[this.colorCounter])
       !_e(this.colorCounter, MultiSelector.COLOR.length-1) ? this.colorCounter++ : this.colorCounter = 0;
    }

    unselectDiv(el){
        let value = el.getData(MultiSelector.VALUE)
        el.rcl(MultiSelector.SELECTED);
        _.each(this.selectedWindow.childNodes(), (div)=>{
            if(_e(div.getData(MultiSelector.VALUE), value)){
                div.remove();
            }
        }, true)
    }

    getValue(){
        let val = [];
        _.each(this.selectedWindow.childNodes(), (div)=>{
            val.push(div.getData(MultiSelector.VALUE));
        }, true)
        return val
    }

    selectAllValues(){
        _.each(this.optionWindow.childNodes(), (el)=>{
            if(!h.from(el).ccl(MultiSelector.SELECTED)){
                this.createSelectedDiv(h.from(el));
            }
        })
    }

    deselectAllValues(){
        _.each(this.optionWindow.childNodes(), (el)=>{
            if(h.from(el).ccl(MultiSelector.SELECTED)){
                this.unselectDiv(h.from(el));
            }
        })
    }

    setValue(val){
        _.each(this.optionWindow.childNodes(), (el)=>{
            if(!h.from(el).ccl(MultiSelector.SELECTED) && _e(h.from(el).getData(MultiSelector.VALUE),val)){
                this.createSelectedDiv(h.from(el));
            }
        })
    }

    setValues(valArr){
        _.each(valArr, (val)=>{
            this.setValue(val);
        })
    }
}
