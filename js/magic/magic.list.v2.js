class MagicListOptions {
  static SELECT_MODE_NONE = 0;
  static SELECT_MODE_SINGLE = 1;
  static SELECT_MULTI = 2;
  #attribute;

  #searchEnabled;
  #settingsEnabled;

  #dataPath;

  #specificationRoot;

  #parent;

  #apiRoot;
  #dataType;

  #selectable = false;

  #dataPathModifier;

  #listener;

  static DEFAULT(type) {
    return new MagicListOptions(type);
  }

  #queryModifier;

  #columnRenderer;
  setQueryModifier(modifier) {
    this.#queryModifier = modifier;
  }

  setColumnRenderer(renderer) {
    this.#columnRenderer = renderer;
  }

  getColumnRenderer() {
    return this.#columnRenderer;
  }

  getParent() {
    return this.#parent;
  }

  getQueryModifier() {
    return this.#queryModifier;
  }

  setDataPathModifier(dataPathModifier) {
    this.#dataPathModifier = dataPathModifier;
  }

  getDataPathModifier() {
    return this.#dataPathModifier;
  }

  getAttribute(){
    return this.#attribute
  }

  setAttribute(attribute){
    this.#attribute= attribute;
    return this;
  }
  constructor(
    dataType,
    specificationRoot = null,
    apiRoot = null,
    dataPath = null,
    settingsEnable = true,
    searchEnabled = true,
    selectMode = MagicListOptions.SELECT_MODE_NONE,
    parent = null
  ) {
    if (specificationRoot == null) {
      specificationRoot = "/root/fetch/list";
    }
    if (apiRoot == null) {
      apiRoot = "/api/v2/";
    }

    if (dataPath == null) {
      dataPath = "/root/fetch/data";
    }

    this.#dataType = dataType;
    this.#specificationRoot = specificationRoot;
    this.#apiRoot = apiRoot;
    this.#dataPath = dataPath;
    this.#searchEnabled = searchEnabled;
    this.#settingsEnabled = true;
    this.selectable = false;
    this.#parent = parent;
  }

  getDataPath(path = "") {
    let res =
      Environment.dataApi +
      this.#apiRoot +
      this.#dataType +
      this.#dataPath +
      path;
    if (this.getDataPathModifier() != null) {
      let mod = this.getDataPathModifier();

      res = mod(res, this);
    }

    return res;
  }

  getSpecificationPath() {
    return (
      Environment.dataApi +
      this.#apiRoot +
      this.#dataType +
      this.#specificationRoot
    );
  }

  getRoot(afterPath = "") {
    return Environment.dataApi + this.#apiRoot + this.#dataType + afterPath;
  }

  getDataType() {
    return this.#dataType;
  }

  getSpecificationRoot() {
    return this.#specificationRoot;
  }

  getApiRoot() {
    return this.#apiRoot;
  }

  getaPath() {
    return this.#dataPath;
  }

  getSettingsEnabled() {
    return this.#settingsEnabled;
  }

  getSearchEnabled() {
    return this.#searchEnabled;
  }
}

class MagicList2 {
  #options;
  #listListener;

  #container;
  #magicTable;

  #headers;

  #dataType;

  #grid = h.div("pg_table_container");

  #form = h.div("modal").cl("hidden").id(new Date().getTime());

  #sortAndSearchForm = h.div("table_config");
  #sort;

  #searchForm;

  #ordering;

  #favoriteType;
  #lastData;

  #pagination;

  #searchEnabled;

  #id;

  getGrid() {
    return this.#grid;
  }

  getFavoriteType() {
    return this.#favoriteType;
  }

  getPagination() {
    return this.#pagination;
  }
  getRootPath() {
    return this.getOptions().getRoot();
  }

  getId() {
    return this.#id;
  }

  getLastData() {
    return this.#lastData;
  }

  getOrdering() {
    return this.#ordering;
  }

  getTableObject() {
    return this.#magicTable;
  }

  getContainer() {
    return this.#container;
  }

  getSearchEnabled() {
    return this.#searchEnabled;
  }

  setListListener(listener) {
    this.#listListener = listener;
  }

  constructor(dataType, options = null) {
    if (options == null) {
      options = MagicListOptions.DEFAULT(dataType);
    }
    this.#options = options;
    this.#dataType = dataType;
    /**
     * We assume that here can only 1 parameter to be changed.
     * For this, we accept nulls as value by default
     */
  }

  getForm() {
    return this.#form;
  }

  getSort() {
    return this.#sort;
  }

  getOptions() {
    return this.#options;
  }

  getListListener() {
    return this.#listListener;
  }

  getSearchAndSettingsBar() {
    return this.#sortAndSearchForm;
  }

  load(where) {
    let _this = this;

    Executor.runGet(this.getOptions().getSpecificationPath(), function (data) {
      _this.build(data, where);
    });
  }

  build(data, where) {
    this.#lastData = data;
    this.#id = where;
    this.#container = h.from(where.isH ? where : get(where));

    this.drawListTitle(where);
    //form is used to generate edit form or add form
    this.#form.appendTo(this.#container);

    //grid is a container where <table> is generated
    this.#grid.appendTo(this.#container);
    this.drawPagination();

    //when we are ready we can draw values from the server

    this.drawValues();

    return this;
  }

  drawListTitle() {
    let data = this.#lastData;
    this.#favoriteType = data.objectType;
    if (data.showTitle) {
      h.div("title").text(data.tableTitle).appendTo(this.#container);
    }
    this.#headers = data; //todo check if we still need to hold it here
    this.#sortAndSearchForm.appendTo(this.#container);
    this.drawSettingsForm();
    if (this.#options.getSearchEnabled()) {
      this.drawSearch(data);
    }
    this.drawTableActions(data);
    this.createDataHeaders(data["fields"]);
  }

  createDataHeaders() {
    this.#magicTable = new MagicTable(this).drawHeader(this.#grid);
  }

  drawSettingsForm() {
    this.#sort = new MagicSort(this);
    this.#sort.build();

    this.#ordering = new MagicOrdering(this);
    this.#ordering.build();
  }

  drawTableActions(data) {
    //List actions are coming from server, if they are existing we need to draw them
    //Default action are: settings : open list settings
    //                    download : download the content of the table
    //                    new      : creates a new element
    // if action differs then it is expected that
    //                   drawCustomAction is defined by user
    // this method should be loaded on the page itself
    if (utils.isEmpty(data.listActions)) {
      return;
    }

    let div = h.div("actions");

    for (let i = 0; i < data.listActions.length; i++) {
      try {
        div.add(this.drawTableAction(data.listActions[i], div, data));
      } catch (ex) {
        log.error(ex);
      }
    }

    this.getSearchAndSettingsBar().add(div);
  }

  drawTableAction(action, div, data) {
    let _this = this;
    if (action == "settings") {
      return h
        .tag("span")
        .cl("settings")
        .add(
          h
            .img("sort_icon.png.png")
            .cl(action)
            .wh(16)
            .click(function () {
              _this.getSort().show();
              _this.getOrdering().close();
            })
        )
        .add(
          h
            .img("settings.png")
            .cl(action)
            .wh(16)
            .click(function () {
              _this.getOrdering().show();
              _this.getSort().close();
            })
        )
        .get();
    }

    if (action == "download") {
      let _this = this;

      return h
        .img("download.png")
        .cl(action)
        .wh(16)
        .click(function () {
          Executor.runPostWithPayload(_this.getRootPath() + "/root/download/data", (data) => {
            h.a( _this.getRootPath() + "/root/download/data/" + data.pathToDownload, "", false ).get().click();
            },_this.createPayload(), (e) => {})
        })
        .get();
    }

    if (action == "new") {
      let _ = this;

      this.#grid.add(div);
      return h
        .input("button")
        .cl(action)
        .cl("insert_new_btn")
        .text("pages.forms.insert_new")
        .click(function () {
          new fetcher(
            _.#form.get().id,
            "/api/v2/" + _.#options.getDataType(),
            "form",
            null,
            _
          ).new();
        })
        .get();
    }

    if (action == "import") {
      return h
        .img("import.png")
        .wh("16px")
        .cl(action)
        .click(function (e) {
          if (e.getAttribute("disabled") != undefined) {
            h.message("global.messages.import.in_process");
          }

          let progress = new Loading(_this.getGrid().get());
          progress.show(e);

          Executor.runPost(
            _this.getRootPath() + "/import/",
            function () {
              progress.hide();
              dom.toast(_.$("global.messages.success"));
              _this.drawValues();
            },
            function (e, x, z) {
              progress.hide();
              Executor.errorFunction(e, x, z);
            }
          );
        })
        .get();
    }

    try {
      return drawCustomAction(action, div, null, this, data);
    } catch (ex) {
      log.error(ex);
      return h.span(
        "info.error.list.actions",
        _.$("global.list.actions.not.implemented." + action)
      );
    }
  }

  drawValues() {
    let path = this.getOptions().getDataPath();
    let _this = this;
    Executor.runPostWithPayload(
      path,
      function (data) {
        _this.buildValues(data);
      },
      this.createPayload()
    );
  }

  buildValues(data) {
    this.#magicTable.rebuildHeaders();
    this.#magicTable.drawValues(data, this);
    this.getPagination().rebuildPages(data);
  }

  drawPagination() {
    this.#pagination = new MagicPagination(this.#magicTable);
    this.#pagination.build(this.getId());
  }

  drawSearch(data) {
    if (this.getOptions().getSearchEnabled()) {
      this.#searchForm = new SearchForm(data, this.getId(), this);
      this.#searchForm.build();
    }
  }

  createPayload() {
    if (this.#pagination == null) {
      return new MagicRequest();
    }

    let req = new MagicRequest();
    let page = this.#pagination.getPageNumber();
    req.limit = this.#pagination.getPerPage();
    req.page = page;

    if (this.getOptions().getSearchEnabled()) {
      req.query = this.#searchForm.getSearchCriteria();
    }

    req.sort = this.getSort().getSort();

    if (this.getOptions().getQueryModifier() != undefined) {
      let func = this.getOptions().getQueryModifier();
      func(req, this.getOptions());
    }

    return req;
  }
}
