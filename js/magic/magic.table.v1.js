class MagicTable extends h {
  #magicTable;

  constructor(magicTable) {
    super();
    this.#magicTable = magicTable;
  }

  getId() {
    return this.#magicTable.getId();
  }

  #currentRow;

  #dataConverter;
  #root = h.tag("table").cl("table magic");
  #eventListener;

  #sortable = [];
  #searchable = [];
  #editable = [];

  getParent() {
    return this.#magicTable;
  }

  getRoot() {
    return this.#root;
  }

  getDataConverter() {
    return this.#dataConverter;
  }

  createRow(header = false) {
    this.#currentRow = new MagicTableRow(this, header);
    return this.#currentRow;
  }

  /**
   *
   * @param event Always TableEvent
   */
  callEventListener(event) {
    if (this.#eventListener != undefined) {
      this.#eventListener(event);
    }
  }

  get() {
    return this.#root.get();
  }

  rebuildHeaders() {
    this.#root.text(null);
    this.drawHeader(
      this.#where,
      this.#cellType,
      this.#dataConverter,
      this.#eventListener
    );
  }

  #where;
  #cellType;
  #rowType;

  drawHeader(
    where,
    cellType,
    rowType,
    dataConverter = null,
    eventListener = null
  ) {
    this.#where = where;
    this.#cellType = cellType;
    this.#rowType = rowType;

    let data = this.#magicTable.getLastData().fields;
    this.#dataConverter = dataConverter;
    this.#eventListener = eventListener;
    this.#root.text(null);
    this.#root.appendTo(where);

    this.#root.get().remove();

    let headers = this.#magicTable.getOrdering()?.getHeaders();

    if (headers != null) {
      data = headers;
    }

    this.callEventListener(
      new TableEvent("table.header.creation.started", this, data)
    );
    this.createRow(true);
    let _this = this;
    for (let i = 0; i < data.length; i++) {
      let item = data[i];

      let cell;
      if (item.visible) {
        this.callEventListener(
          new TableEvent("table.header.cell.creation.started", this, data)
        );
        cell = this.#currentRow.createCell(
          "cell_header",
          _.$(item.translation),
          true
        );
        this.callEventListener(
          new TableEvent("table.header.creation.finished", item, cell)
        );
      }
      // create sortImage in HeaderList, call a function from MagicSort. Function give us the same img as the sortForm
      if (item.sortable && !item.actions?.length && cell) {
        let hCell = cell.get();
        let sortImage = this.#magicTable.getSort().buildImgForTable(item.field);
        let sortImageAction = sortImage
          .click(function (e) {
            _this.setFieldOrder(e);
          })
          .get();
        hCell.lastElementChild.appendChild(sortImageAction);
      }
    }
    this.callEventListener(
      new TableEvent("table.header.creation.finished", this, data)
    );

    this.#root.appendTo(where);

    return this;
  }

  setFieldOrder(e) {
    this.#magicTable.getSort().setFieldOrder(e, true);
  }

  drawValues(data, list, dataConverter = null, eventListener = null) {
    if (dataConverter == null) {
      dataConverter = MagicTable.defaultConverter;
    }
    let headers = this.#magicTable.getOrdering()?.getHeaders();

    let visibleFields;
    if (headers != null) {
      visibleFields = headers;
    } else {
      visibleFields = this.#magicTable.getLastData().fields;
    }

    if (utils.e(data.content.length, 0)) {
      h.message("global.no_data");
      return;
    }
    let favoriteEnabled = Favorite.enabled();
    utils.each(data.content, (item) => {
      this.createRow();
      utils.each(visibleFields, (field) => {
        if (utils.isFalse(field.visible)) {
          return utils.CONTINUE;
        }
        let cell;
        if (!utils.isEmpty(field.actions)) {
          let divActions = h.div("actions");
          let content = h.tag("span").cl("actions_cell");
          let imgContent = h
            .img("dots.png")
            .cl("actions_cell_dots")
            .wh(16)
            .click(function () {
              content.get().classList.toggle("hidden");
              dom.linkTo(content.get(), imgContent.get());
            })
            .appendTo(divActions);

          let arr = [];

          utils.each(field.actions, (action) => {
            let act = h.from(
              this.drawAction(action, content, list, item["id"], this, item)
            );
            arr.push(act);
            content.add(act);
          });

          let hasCustomImpl = utils.each(arr, (x) => {
            if (x.ccl("custom_action")) return true;
          });

          if (!hasCustomImpl) {
            content.cl("hidden").appendTo(divActions);
          }

          cell = hasCustomImpl
            ? this.#currentRow.createCell("cell_header", content)
            : this.#currentRow.createCell("cell_header", divActions);
          this.callEventListener(
            new TableEvent("table.header.cell.created", cell, data)
          );
        } else {
          let value = dataConverter(
            field,
            item[field.field],
            item,
            this,
            null,
            null,
            favoriteEnabled
          );

          cell = this.#currentRow.createCell("cell_header", value);
          this.callEventListener(
            new TableEvent("table.header.cell.created", cell, data)
          );
          this.callEventListener(
            new TableEvent("table.header.cell.created_action", cell, data)
          );
        }
      });
    });
  }

  /**
   * Here only generating actions for the row
   * @param action - name of the action
   * @param cell - a place where the action will be displayed
   * @param rowAction - true if row (//todo to remove it)
   * @param id - id of the line
   * @param data - the whole row of data
   * @returns {HTMLSpanElement|HTMLImageElement|*|h.h}
   */

  drawAction(action, cell, list, id, magicTable, data) {
    let res = null;
    try {
      res = drawCustomAction(action, id, list, data);
    } catch (ex) {
      log.error(ex);
    }

    if (res != null) {
      return h.from(res).cl("custom_action");
    }

    if (action == "edit") {
      return h
        .img("edit.png")
        .wh(16)
        .cl(action)
        .click(function () {
          new fetcher(
            list.getForm().get().id,
            list.getInitData()["apiRoot"] + list.getInitData()["dataType"],
            "form",
            null,
            list
          ).loadId(id);
        });
    }
    if (action == "delete") {
      let _this = this;
      return h
        .img("rm.png")
        .wh(16)
        .click(function (e) {
          let deleteRule = "____user_delete_objects_rule";
          let confirmation = "confirm_delete";
          let magicPrompt = new MagicPrompt(
            confirmation,
            function () {
              _this.deleteRow(e, list, id);
            },
            deleteRule
          );
          magicPrompt.deleteOrPromptDelete();
        });
    }
    if (action == "details") {
      return h
        .img("details.png")
        .cl(action)
        .wh(16)
        .click(function () {
          showDetails(id);
        })
        .get();
    }

    if (utils.e(action, "import")) {
      return h
        .img("import.png")
        .cl(action)
        .wh(16)
        .click(function (e) {
          if (e.getAttribute("disabled") != undefined) {
            h.message("global.messages.import.in_process");
          }

          let progress = new Loading(list.getGrid());
          progress.show(img);

          Executor.runPost(
            list.getRootPath() + "/import/",
            function () {
              progress.hide();
              dom.toast(_.$("global.messages.success"));
              e.removeAttribute("disabled");
              list.drawValues();
            },
            function (e) {
              Executor.errorFunction(e);
              progress.hide();
              e.removeAttribute("disabled");
            }
          );
        })
        .get();
    }

    if (res == null) {
      return h.span(
        "info.error.list.actions",
        "global.list.actions.not.implemented." + action
      );
    }
  }

  deleteRow(e, list, id) {
    let path = Environment.dataApi + list.getInitData()["apiRoot"];

    Executor.run(
      path + list.getInitData()["dataType"] + "/delete/" + id,
      "DELETE",
      function (res) {
        while (
          !e.classList.contains("data_row") &&
          !e.classList.contains("table")
        ) {
          e = e.parentElement;
        }

        if (e.classList.contains("data_row")) {
          e.remove();
        } else if (e.classList.contains("table")) {
          log.trace("No data_row in table");
        }
        h.message("global.messages.delete.success");
      }
    );
  }

  static defaultConverter(
    header,
    what,
    data,
    table,
    dataType = null,
    onclick = null,
    favoriteEnabled
  ) {
    let res = null;
    try {
      res = drawCustomField(header, what, data, table, this, favoriteEnabled);
    } catch (ex) {
      log.error(ex);
    }

    if (res != null) {
      return res;
    }

    if (utils.e(header.field, "id")) {
      if (favoriteEnabled) {
        let heart = h
          .span(Favorite.FAVORITE_CLASS + "_holder", "", false)
          .click(function (e) {
            Favorite.markElementAsFavorite(
              e,
              data,
              table.getParent().getFavoriteType()
            );
          });
        Favorite.markIfElementFavorite(
          heart,
          data,
          table.getParent().getFavoriteType()
        );
        return h
          .div("id_with_favorite")
          .add(heart)
          .add(h.span("id_content", what, false));
      }
    }

    let type =
      header == null
        ? dataType
        : header.type == undefined
        ? header.getType()
        : header.type;

    //here is going something interesting. Something like a hell

    if (utils.e(type, "checkbox")) {
      if (!utils.isFalse(what)) {
        return h.img("true.png").wh(16);
      } else if (what === false || what === "false" || what === "False") {
        return h.img("false.png").wh(16);
      }
    } else if (utils.e(type, "object_chooser")) {
      let val = h.tag("span").cl("value_span");
      let res = "";

      if (what != null && !utils.isEmpty(what.value)) {
        h.a("#")
          .cl("object_link")
          .text(what.value, false)
          .click(
            onclick == null
              ? function () {
                  h.magicView(
                    what.key,
                    header.remoteType,
                    false,
                    header.searchName
                  );
                  return false;
                }
              : onclick
          )
          .appendTo(val);
      } else {
        val.text(what == null ? what : what.value);
      }

      //we add icon here if object contains an Icon (or download file link)
      if (
        what != null &&
        what.additional != null &&
        what.additional.length > 0
      ) {
        res = h
          .img(what.additional)
          .cl("image_in_details")
          .click(function (e) {
            utils.showFullImage(e.src);
          })
          .wh(16);
        val.get().prepend(res.get());
      }
      if (what == null) {
        val.text(null);
      }
      return val;
    }

    if (utils.e(type, "file")) {
      let img = "";
      if (utils.isEmpty(what)) {
        return "&nbsp;";
      }

      if (utils.isImage(what)) {
        img = h
          .img(what)
          .wh(24)
          .click(function (e, ex) {
            utils.showFullImage(e.src);
          });
      } else {
        img = document.createElement("a");
        img.href = what;
        img.target = "__blank";
        img.innerHTML = _.$("global.pages.download");
      }

      return h.from(img);
    }

    if (what == null || what.value == undefined) {
      return what;
    }
    return what.value;
  }
}

class MagicTableRow extends h {
  #parent;
  #root;
  #isHeader;

  constructor(table, isHeader) {
    super();
    this.#parent = table;
    this.#root = MagicTableRow.tag("tr")
      .cl("tr")
      .cl("data_row")
      .appendTo(table);
    this.#isHeader = isHeader;
  }

  parent() {
    return this.#parent;
  }

  get() {
    return this.#root;
  }

  getDataConverter() {
    return this.#parent.getDataConverter();
  }

  createCell(type, value) {
    new MagicTableCell(this, this.#isHeader).draw(type, value);
    return this.get();
  }
}

class MagicTableCell extends h {
  #parent;

  #root;

  #isHeader;

  constructor(row, isHeader = false) {
    super();
    this.#parent = row;
    this.#isHeader = isHeader;
    let tag = isHeader ? "th" : "td";
    this.#root = h.tag(tag);

    if (isHeader) {
      this.#root.cl("th").cl("data_header");
    } else {
      this.#root.cl("td").cl("data_cell");
    }
  }

  get() {
    return this.#root;
  }

  draw(type, value) {
    let dataConverter = this.#parent.getDataConverter();
    if (dataConverter) {
      value = dataConverter(value, value);
    }
    if (value != null && value.isH) {
      this.#root.add(value).appendTo(this.#parent.get());
    } else {
      this.#root.text(value, false).cl(type).appendTo(this.#parent.get());
    }
  }

  parent() {
    return this.#parent;
  }
}

class TableEvent {
  event;
  target;
  data;
  el;

  constructor(event, target, data, el = null) {
    this.event = event;
    this.target = target;
    this.data = data;
    this.el = el;
  }
}
