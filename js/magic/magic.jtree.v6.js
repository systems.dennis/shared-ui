
class MagicTreeOptions extends Lombok {
    paddingDef =10 ;
    onSelect;
    id = null;
    subFetcher;
    treeListener;
    itemRenderer;
    toTranslate = false
    isShowCount = true;
    nodeRenderer;
    imgDiv;

    constructor(onSelect, subFetcher) {
        super().$()
        this.setOnSelect(onSelect);
        this.setSubFetcher(subFetcher);
    }

}

class MagicTree {
    static ITEM_CLASS = "new_item";
    static BOX_CLASS = "tree_box";

    #options;

    #storage = new Map();

    #userDataDiv = h.div("tree_data_content");

    constructor(options = new MagicTreeOptions()) {
        this.#options = options;
    }

    getOptions() {
        return this.#options;
    }

    getListener() {
        return this.#options.getTreeListener();
    }

    getContent() {
        return this.#userDataDiv;
    }

    fireEvent(eventName, element, elementId, tree) {
        if (_.notNull(this.getListener())) {
            this.getListener()(new MagicTreeEvent(eventName, element, elementId, tree));
        }
    }

    addItem(idRoot, data) {
        let level = this.getLevel(idRoot);

        this.box = idRoot.firstByClass(MagicTree.BOX_CLASS);

        if (this.box == null) {
            this.box = h.div(MagicTree.BOX_CLASS).appendTo(idRoot);
        }

        let mainText;
        if (this.#options.getNodeRenderer()){
            mainText = this.#options.getNodeRenderer()(data);
        } else {
            mainText = this.defaultRenderItem(data);
        }

        this.fireEvent(MagicTreeEvent.PREBUILD_TEXT, mainText, data, this);

        const id = idRoot.get().id + "_" + level;

        let res = h.div(MagicTree.ITEM_CLASS)
            .setData("parent", idRoot.get().id)
            .setData("level", level)
            .appendTo(this.box);


        this.mainBranchContainer = h.div("branch_container").id(data.id).appendTo(res);
        if (_e(data.cnt, 0)) {
            this.mainBranchContainer.click(function (e, ev) {
                // dom.redirect('?id=' + data.id)
                try {
                    ev.preventDefault();

                    let el = document.querySelectorAll(".clicked");
                    for (let i = 0; i < el.length; i++) {
                        el[i].classList.remove("clicked");
                    }
                    e.classList.add("clicked");

                    _this.getOptions().getOnSelect()(data, _this.getOptions().getId());
                } catch (e) {
                    console.error(e);
                }
            })
        } else {
            this.mainBranchContainer.click(function (e) {
                _this.collapseOrFetch(e);
            });
        }

        if (data.selectable) {
            mainText.appendTo(this.mainBranchContainer);
        } else {
            this.mainBranchContainer
                .addIf(this.#options.getImgDiv(), h.div("img_div"))
                .add(h.div("first_level_title").add(mainText))
                .cl(data.id);

        }

        let _this = this;
        if (data.cnt > 0) {
            h.img("collapsed.png.png")
                .cl("collapsed")
                .wh(16)
                .prependTo(this.mainBranchContainer)
        }

        res.get().style.paddingLeft = this.getOptions().getPaddingDef() * level;

        this.fireEvent(MagicTreeEvent.ADD_ITEM, res, data.id, this);

        this.#storage.set(data, new TreeItem(idRoot, id, this, res, this.mainBranchContainer));

        return res;
    }

    collapseOrFetch(e) {
        if (e.classList.contains("expanded_tree")) {
            this.collapse(e);
        } else {
            this.expand(e);
        }
    }

    defaultRenderItem(data) {
        let _this = this;
        return h.a('#', data.name, this.#options.getToTranslate()).andTextIf(this.#options.getIsShowCount(), data.cnt > 0 ? " [" + data.cnt + "]" : "", false)
    }

    collapse(e) {
        let el = h.from(e).rcl("expanded_tree");
        el.firstByClass("collapsed", true, true).src("collapsed.png.png").rcl("expanded_tree_item");
        let divel = el.parent(true).get().querySelectorAll(".tree_box");
        _.each(divel, (el) => el.style.display = "none")
        this.fireEvent(MagicTreeEvent.COLLAPSE, h.from(e), h.from(e).getId(), this);
    }

    getNode(data) {
        return this.#storage.get(data);
    }

    removeNodeByData(data) {
        let item = this.getNode(data);
        item.removeSelf();
        this.#storage.delete(data);
    }

    expand(e) {
        e = h.from(e).cl("expanded_tree")
        e.firstByClass("collapsed", true, true).src("expanded.png.png").cl("expanded_tree_item")

        if (!e.ccl("data_fetched")) {
            e.cl("data_fetched");

            let data = this.getOptions().getSubFetcher()(e);
            _.each(data, item => {
                this.addItem(e.parent(true), item);
            });
        }

        e.parent(true).eachOfClass(MagicTree.BOX_CLASS, divEl => {
            divEl.style.display = "block";
        });

        this.fireEvent(MagicTreeEvent.EXPAND, h.from(e), h.from(e).getId(), this);
    }

    remove(idRoot) {
        idRoot.get().remove();
        this.fireEvent(MagicTreeEvent.EXPAND, idRoot, idRoot, this);
    }

    getLevel(root) {
        let level = root.getData("level");
        if (_e(level, undefined)) {
            level = 0;
        }

        return parseInt(level) + 1;
    }
}

class TreeItem extends Lombok{
    parentNode;
    id;
    self;
    isHide = false;
    parent;
    nodeContainer

    constructor(parentNode, id, magicTree, self, nodeContainer) {
        super().$()
        this.setParentNode(parentNode)
        this.setId(id);
        this.setParent(magicTree);
        this.setSelf(self);
        this.setNodeContainer(nodeContainer);
    }

    toggleChilren() {
        let box = this.getChildBox();
        if (!box) {
            return;
        }

        if (this.getIsHide()) {
            box.show();
        }

        if (!this.getIsHide()) {
            box.hide();
        }

        this.setIsHide(!this.getIsHide())
    }

    hideChildren() {
        let box = this.getChildBox();
        if (box) {
            box.hide();
            this.setIsHide(true);
        }
    }

    showChildren() {
        let box = this.getChildBox();
        if (box) {
            box.show();
            this.setIsHide(false);
        }
    }

    getChildBox() {
        return h.fromId(this.getId()).firstChildByClass('.' + MagicTree.BOX_CLASS)
    }

    removeSelf() {
        this.getSelf().remove();
    }
}