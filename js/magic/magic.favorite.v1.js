class Favorite{

    static FAVORITE_CLASS = "____fav";
    static FAVORITE_API = Environment.dataApi + "/api/v2/shared/favorite/";

    static enabled(){
        let res = false;
        try {
            if(cache.get(Authorization.TOKEN_FIELD)){
               Executor.runGet(Favorite.FAVORITE_API + "enabled", function (data){
            res = data.enabled;
        }, false, Executor.HEADERS, function (e){log.info(e.responseText)})  
            }
         
        } catch (error) {

        }
        
        return  res;
    }

    static markIfElementFavorite(where, data, type){
        //is_favorite

        Executor.runPostWithPayload(Favorite.FAVORITE_API + "is_favorite", function (data){
            if (data.enabled) {
                h.from(where).cl(Favorite.FAVORITE_CLASS);
            } else {
                h.from(where).rcl(Favorite.FAVORITE_CLASS);
            }
        } , Favorite.createPayload(data, type) , function (e){log.info(e.responseText)})
    }


    static markElementAsFavorite(where, data, type, callback = null){
        if (h.from(where).get().classList.contains(Favorite.FAVORITE_CLASS)){
            Executor.runPostWithPayload(Favorite.FAVORITE_API + "delete", function (){
                h.from(where).rcl(Favorite.FAVORITE_CLASS);
                if (callback && _.isFunction(callback)){
                    callback(false);
                }
            },this.createPayload(data, type), function (e){log.info(e.responseText),  Executor.HEADERS})
        } else {
            Executor.runPostWithPayload(Favorite.FAVORITE_API + "add", function (data) {
                h.from(where).cl(Favorite.FAVORITE_CLASS);
                if (callback && _.isFunction(callback)){
                    callback(false);
                }
            }, Favorite.createPayload(data, type), function (e){log.info(e.responseText)})
        }
    }

    static createPayload(data, type){
        return {
            "type" : type,
            "modelId" : data.id
        }
    }
}
