class Favorite {
    static FAVORITE_CLASS = "____fav";

    static enabled(serverRequest) {
        if (!serverRequest) {
            log.promisedDebug(() => {
                return {"type": "favorite", "text": "NO SERVER REQUEST"}
            })
            return false;
        }
        let res = false;
        const request = serverRequest.copy().cacheResponse(null, true).shared('favorite').request('enabled').method(Executor.GET_METHOD);

        try {
            if (cache.get(Authorization.TOKEN_FIELD)) {
                request.run((data) => {
                    res = data.enabled;
                }, (error) => {
                    log.promisedDebug(() => {
                        return {"type": "favorite", "text": "ENABLED SERVER ERROR", "error": error?.responseText}
                    })
                })
            }
        } catch (error) {
            log.promisedDebug(() => {
                return {"type": "favorite", "text": "ENABLED SERVER ERROR", "error": error?.responseText}
            })
        }

        return res;
    }

    static markIfElementFavorite(where, data, type, serverRequest) {
        const request = serverRequest.copy().shared('favorite').cacheResponse(600000, true).request('is_favorite').method(Executor.POST_METHOD);
        request.payload(Favorite.createPayload(data, type));

        request.run((data) => {

            Favorite.toggleFavoriteClass(where, data.enabled);
        }, (error) => {
            log.promisedDebug(() => {
                return {"type": "favorite", "text": "CHECK FAVORITE SERVER ERROR", "error": error}
            })
        });
    }

    static toggleFavoriteClass(where, toAdd) {
        if (toAdd) {
            h.from(where).cl(Favorite.FAVORITE_CLASS);
        } else {
            h.from(where).rcl(Favorite.FAVORITE_CLASS);
        }
    }

    static markElementAsFavorite(where, data, type, callback = null, serverRequest) {
        const request = serverRequest.copy().forceNoCache().shared('favorite').method(Executor.POST_METHOD)
            .payload(Favorite.createPayload(data, type));
        if (h.from(where).get().classList.contains(Favorite.FAVORITE_CLASS)) {
            request.request('delete').run(() => {
                h.from(where).rcl(Favorite.FAVORITE_CLASS);
                if (callback && _.isFunction(callback)) {
                    callback(false);
                }
                StorageCacheProvider.getInstance().deleteFromCache(request.copy().shared("favorite").request("is_favorite")
                    .payload(Favorite.createPayload(data, type)));
            }, (error) => {
                log.promisedDebug(() => {
                    return {"type": "favorite", "text": "REMOVE FAVORITE SERVER ERROR", "error": error?.responseText}
                })
            })
        } else {
            request.request('add').run(() => {
                h.from(where).cl(Favorite.FAVORITE_CLASS);
                if (callback && _.isFunction(callback)) {
                    callback(true);
                }
                StorageCacheProvider.getInstance()
                    .deleteFromCache(request.copy().shared("favorite").request("is_favorite")
                        .payload(Favorite.createPayload(data, type)));
            }, (error) => {
                log.promisedDebug(() => {
                    return {"type": "favorite", "text": "ADD FAVORITE SERVER ERROR", "error": error?.responseText}
                })
            })
        }
    }

    static createPayload(data, type) {
        return {
            "type": type, "modelId": data.id
        }
    }
}
