class h {
  static #_____items = [];
  #el;

  isH = true;

  isInput = false;

  static span(cl, text = null, translate = false) {
    let he = h.tag("span");
    he.cl(cl);
    if (utils.notNull(text)) {
      he.text(text, translate);
    }
    return he;
  }

  handleEvent(eventName, func) {
    this.get().addEventListener(eventName, (e) => {
      func(e, this);
    });

    return this;
  }

  remove() {
    this.get().remove();
    return this;
  }

  static fromId(id) {
    return h.from(document.getElementById(id));
  }

  static fromFirst(selector) {
    return h.from(document.querySelector(selector)[0]);
  }

  static from(el) {
    if (utils.isNull(el)) {
      log.trace("trying to add null element, ignoring it");
      return null;
    }
    if (el.isH) {
      return el;
    }
    let he = h.h();
    he.#el = el;
    return he;
  }

  static input(type) {
    let he = h.h();
    h.isInput = true;
    he.#el = document.createElement("input");
    he.attr("type", type);
    return he;
  }

  inputFunc(f) {
    let _this = this;
    this.get().oninput = function (e) {
      f(_this.get(), e);
    };
    return this;
  }

  noCache() {
    this.get().autocomplete = "off";
    return this;
  }

  hideIf(toHide) {
    if (!toHide) return;

    if (this.isInput) {
      this.attr("type", dom.HIDDEN);
    } else {
      this.get().style.display = "none";
    }
    return this;
  }

  clIf(toAdd, cl) {
    if (toAdd) {
      this.cl(cl);
    }

    return this;
  }

  addIf(toAdd, elToAdd) {
    if (toAdd) {
      this.add(elToAdd instanceof Function ? elToAdd() : elToAdd);
    }

    return this;
  }

  has(attr) {
    return utils.notNull(this.getA(attr));
  }

  attrIf(b, attr, value) {
    if (b) {
      this.attr(attr, value);
    } else {
      this.attr(attr, null);
    }
    return this;
  }

  get() {
    return this.#el;
  }

  parent(toWrap = true) {
    if (toWrap) return h.from(this.get().parentElement);

    return this.get().parentElement;
  }

  next(toWrap = true) {
    if (toWrap) return h.from(this.get().nextElementSibling);

    return this.get().nextElementSibling;
  }

  addBefore(elToAdd, beforeElement) {
    this.get().insertBefore(
      elToAdd.isH ? elToAdd.get() : elToAdd,
      beforeElement.isH ? beforeElement.get() : beforeElement
    );
    return this;
  }

  containsTag(tag) {
    let items = utils.isEmpty(this.get().querySelector(tag));
    return utils.notNull(items) && items.length > 0;
  }

  val() {
    return utils.isNull(this.get().value)
      ? this.get().innerHTML
      : this.get().value;
  }

  onKey(func) {
    let _this = this;
    this.get().onkeyup = function (e) {
      func(_this.get(), e);
    };
    return this;
  }

  byId(id) {
    let res = h.#_____items[id].#el;
    if (res != null) {
      return res.#el;
    }
    return res;
  }

  static tag(tag) {
    let he = h.h();
    he.#el = document.createElement(tag);
    return he;
  }

  href(href) {
    this.get().href = href;
  }

  pointer() {
    this.get().style.cursor = "pointer";
    return this;
  }

  add(he) {
    if (he == null) {
      log.debug("trying to add null object, ignoring");
      return this;
    }
    he.isH ? this.get().append(he.get()) : this.get().append(he);
    return this;
  }

  prependTo(to) {
    to.isH ? to.get().prepend(this.get()) : to.prepend(this.get());
    return this;
  }

  static message(text) {
    dom.toast(_.$(text));
  }

  toHtml() {
    return this.get().outerHTML;
  }

  getContent() {
    return this.get().innerHTML;
  }

  style() {
    return new Style(this);
  }

  static img(path, size = 1) {
    if (Environment.defaultTemplate === undefined) {
      Environment.defaultTemplate = "";
    }

    if (_.isNull(path)) {
      return h.img("null");
    }

    let img = h.tag("img");

    if (utils.startsWith(path, Environment.fileStorageApi)) {
      if (!_e(size, 1)) {
        img.src(path + "/" + size);
        return img;
      }
    }

    let realPath =
      (path.trim().indexOf("http") == 0
        ? path
        : Environment.self + Environment.defaultTemplate + "images/" + path) +
      "?v=" +
      Environment.version;
    img.src(realPath);

    return img;
  }

  first(toWrap = true) {
    if (toWrap) return h.from(this.get().children[0]);
    return this.get().children[0];
  }

  child(num, toWrap = true) {
    if (toWrap) return h.from(this.get().children[num]);
    return this.get().children[num];
  }

  performClick() {
    this.get().click();
    return this;
  }

  performClickIf(cond) {
    if (cond) this.performClick();
    return this;
  }

  bgColor(color) {
    this.get().style.backgroundColor = color;

    return this;
  }

  color(color) {
    this.get().style.color = color;

    return this;
  }

  draggable(onStart = null, onDrop = null) {
    this.attr("draggable", true);

    if (_.notNull(onStart)) {
      this.handleEvent("dragstart", onStart);
    }

    if (_.notNull(onDrop)) {
      this.handleEvent("dragend", onDrop);
    }
  }

  draggableIf(cond, onStart = null, onDrop = 0) {
    if (cond) {
      this.draggable(onStart, onDrop);
    }
    return this;
  }

  dragOver(func) {
    return this.handleEvent("dragover", func);
  }

  dragDrop(func) {
    return this.handleEvent("drop", func);
  }

  dragLeave(func) {
    return this.handleEvent("dragleave", func);
  }

  last(toWrap = true) {
    if (toWrap)
      return h.from(this.get().children[this.get().children.length - 1]);
    return this.get().children[this.get().children.length - 1];
  }

  firstByClass(cl, into = true) {
    if (into) {
      let nodes = this.get().children;
      for (let i = 0; i < nodes.length; i++) {
        if (nodes[i].classList != null && nodes[i].classList.contains(cl)) {
          return nodes[i];
        }
      }
    } else {
      let el = this.get();
      while (!el.classList.contains(cl)) {
        if (el.parentElement == null) {
          el = null;
          break;
        }

        el = el.parentElement;
      }
      return el;
    }

    return null;
  }

  static a(href, text, toTranslate = false) {
    let a = h.tag("a");
    a.href(href);
    a.text(text, toTranslate);
    return a;
  }

  static magicView(
    objectId,
    remoteType,
    rootData = false,
    fav = null,
    listener = null
  ) {
    return new MagicView(objectId, null, remoteType, rootData, fav, listener);
  }

  static textArea(text, toTranslate = false) {
    let a = h.tag("textarea");
    a.text(text, toTranslate);
    return a;
  }

  click(f) {
    let _this = this;
    this.get().onclick = function (e) {
      f(_this.get(), e);
    };
    return this;
  }

  clickIf(condition, f) {
    if (condition) {
      this.click(f);
    }
    return this;
  }

  clickIfPresent(f) {
    if (_.notNull(f)) {
      this.click(f);
    }
    return this;
  }

  clickPreventive(f) {
    let _this = this;
    this.get().onclick = function (e) {
      e.preventDefault();
      f(_this.get(), e);
      return false;
    };
    return this;
  }

  change(f) {
    let _this = this;
    this.get().onchange = function () {
      f(_this.get());
    };
    return this;
  }

  setA(attr, value) {
    this.get().setAttribute(attr, value);
    return this;
  }
  setAIf(cond, attr, value) {
    if (cond) {
      this.setA(attr, value);
    }

    return this;
  }
  getA(attr) {
    return this.get().getAttribute(attr);
  }

  getData(attr) {
    return this.getA("data-" + attr);
  }

  getDataId(attr) {
    return this.getA("data-id");
  }

  setData(attr, value) {
    this.get().setAttribute("data-" + attr, value);
    return this;
  }

  static anyByClassExists(cl) {
    return document.getElementsByClassName(cl).length > 0;
  }

  width(width) {
    this.get().style.width = width + "px";
    return this;
  }

  srcHover(img) {
    let _this = this;
    let _src = this.get().src;
    this.hover(() => {
      this.src(img);
    });
    this.nover(() => {
      this.src(_src);
    });
  }

  getOffsetLeft() {
    return this.get().offsetLeft;
  }

  getOffsetTop() {
    return this.get().offsetTop;
  }

  getRect() {
    return this.get().getBoundingClientRect();
  }

  hover(f) {
    this.get().onmouseover = function (e) {
      f(this, e);
    };
  }

  nover() {
    this.get().onmouseleave = function (e) {
      f(this, e);
    };
  }

  eachOfClass(cl, func, travers = false, toWrap = false) {
    let children = this.get().querySelectorAll("." + cl);
    let ret = null;
    if (utils.isNull(children) || children.length < 1) {
      return;
    }
    for (let i = 0; i < children.length; i++) {
      ret = func(toWrap ? h.from(children[i]) : children[0]);
      if (utils.notNull(ret) && !utils.e(ret, utils.CONTINUE)) {
        return ret;
      }

      if (travers && dom.hasChildren(children[i])) {
        h.from(children[i]).eachOfClass(cl, func);
      }
    }

    return this;
  }

  eachOf(func, tag = null) {
    let children = this.childNodes(tag);
    let ret = null;
    if (utils.isNull(children) || children.length < 1) {
      return;
    }
    for (let i = 0; i < children.length; i++) {
      ret = func(children[i]);

      if (utils.notNull(ret) && !utils.e(ret, utils.CONTINUE)) {
        return ret;
      }
    }
    return this;
  }

  hide() {
    return this.cl(dom.HIDDEN);
  }

  show() {
    return this.rcl(dom.HIDDEN);
  }

  height(height) {
    this.get().style.height = height + "px";
    return this;
  }

  wh(wh) {
    this.width(wh);
    this.height(wh);
    return this;
  }

  data_id(id) {
    this.attr("data-id", id);
    return this;
  }

  toggle(cl) {
    return this.get().classList.toggle(cl);
  }

  ccl(cl) {
    if (utils.isNull(this.get())) {
      return false;
    }
    return this.get().classList.contains(cl);
  }

  data_text(text) {
    this.attr("data-text", text);
    return this;
  }

  id(id) {
    this.get().id = id;
    let _this = this;
    h.#_____items.push({ id, _this });
    return this;
  }

  getId() {
    return this.get().id;
  }

  src(src) {
    if (utils.ne(src.indexOf("http"), 0)) {
      if (utils.ne(src.indexOf(Environment.self), 0)) {
        if (!src.startsWith("/images")) {
          src =
            Environment.self + "/images/" + src + "?v=" + Environment.version;
        } else {
          src = Environment.self + src + "?v=" + Environment.version;
        }
      }
    }
    this.get().src = src;
    return this;
  }

  cl(obj) {
    if (obj.trim().indexOf(" ") > -1) {
      let cls = obj.split(" ");
      utils.each(cls, (cl) => {
        this.get().classList.add(cl);
      });
    } else {
      this.get().classList.add(obj.trim());
    }
    return this;
  }

  rcl(cl) {
    this.get().classList.remove(cl);
    return this;
  }

  text(val, translate = true) {
    if (val == null) {
      translate = false;
      val = "";
    }
    if (translate) val = _.$(val);

    this.get() instanceof HTMLInputElement ||
    this.get() instanceof HTMLTextAreaElement
      ? (this.get().value = val)
      : (this.get().innerHTML = val);
    return this;
  }

  andText(text, toTranslate) {
    this.get().innerHTML =
      this.get().innerHTML + (toTranslate ? _.$(text) : text);
    return this;
  }

  readOnly(boolean = true) {
    if (typeof boolean == "boolean") {
      this.get().readOnly = boolean;
    }
    return this;
  }

  appendTo(obj) {
    if (obj.isH) {
      obj.add(this.get());
    } else {
      obj.appendChild(this.get());
    }
    return this;
  }

  appendToBody() {
    this.appendTo(document.body);
    return this;
  }

  childNodes(tag = null, toWrap = false) {
    if (tag == null) {
      return toWrap ? h.from(this.get().childNodes) : this.get().childNodes;
    } else {
      let children = this.get().childNodes();
      let res = [];
      utils.each(children, (child) => {
        if (utils.e(child.tagName, tag)) {
          res.push(toWrap ? h.from(child) : child);
        }
      });

      return res;
    }
  }

  /**
   * For some reason old method can be still used, so we just duplicate it now
   * @param obj
   * @returns {h.h}
   */
  appendChild(obj) {
    this.add(obj);
    return this;
  }

  placeHolder(text, toTranslate = true) {
    if (text == null) {
      this.get().placeHolder = "";
    }
    this.get().placeholder = toTranslate
      ? _.$(text, toTranslate)
      : text;

    return this;
  }

  focus() {
    this.get().focus();
    return this;
  }

  static inputBtn(text) {
    let he = h.h();
    h.isInput = true;
    he.#el = document.createElement("input");
    he.attr("type", "button");
    he.text(text);
    return he;
  }

  setEl(el) {
    this.#el = el;
  }

  attr(attr, value) {
    if (attr === "checked") {
      this.get().cheked = value;
    }

    if (utils.isNull(value)) {
      this.get().removeAttribute(attr);
    }
    this.get().setAttribute(attr, value);
    return this;
  }

  attrChecked(value) {
    if (value) {
      this.get().setAttribute("checked", "checked");
    }
    return this;
  }

  static div(cl) {
    let he = h.tag("div");
    he.cl(cl);
    return he;
  }

  static label(f, text, totranslate = true) {
    let he = h.tag("label");
    he.text(text, totranslate);
    he.attr("for", f.isH ? f.get().id : f);
    return he;
  }

  jq() {
    return $(get());
  }

  static h() {
    return new h();
  }

  static divImg(src) {
    let img = h.img(src).get().src;
    let div = h.div("div_image");
    div.get().style.backgroundImage = "url('" + img + "')";
    div.get().style.backgroundSize = "contain";
    return div;
  }

  getSrc() {
    return this.get().src;
  }
}

class Style {
  #style;
  #h;
  constructor(h) {
    this.#h = h;
    this.#style = h.get().style;
  }

  background(background) {
    this.#style.background = background;
    return this;
  }
  color(color) {
    this.#style.background = color;
    return this;
  }
  size(fontSize) {
    this.#style.fontSize = fontSize;
    return this;
  }

  roundBorder() {
    this.radius("50%");
    return this;
  }
  radius(radius) {
    this.#style.borderRadius = radius;
  }

  border(border) {
    this.#style.border = border;
    return this;
  }

  grid() {
    this.#style.display = "grid";
    return this;
  }

  gridRepeatColumn(count, size) {
    this.#style.gridTemplateColumns = `repeat(${count}, ${size})`;
    return this;
  }

  gridRepeatRow(count, size) {
    this.#style.gridTemplateRows = `repeat(${count}, ${size})`;
    return this;
  }

  gridArea(rowStart, columnStart, rowEnd, columnEnd) {
    this.#style.gridArea =
      rowStart + "/" + columnStart + "/" + rowEnd + "/" + columnEnd;

    return this;
  }

  top(val) {
    this.#style.top = val + "px";

    return this;
  }

  bottom(val) {
    this.#style.bottom = val + "px";

    return this;
  }

  left(val) {
    this.#style.left = val + "px";

    return this;
  }

  absolute() {
    this.#style.position = "absolute";

    return this;
  }

  gridRowStart(val) {
    this.#style.gridRowStart = val;

    return this;
  }

  back() {
    return this.#h;
  }

  rotateDeg(deg) {
    this.#style.transform = `rotate(${deg}deg)`;

    return this;
  }

  horizontMirror() {
    this.#style.transform = "scale(-1, 1)";

    return this;
  }

  verticalMirror() {
    this.#style.transform = "scale(1, -1)";

    return this;
  }

  mirror() {
    this.#style.transform = "scale(-1, -1)";

    return this;
  }
}
