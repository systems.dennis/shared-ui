class MagicRemoteChooserOptions {

    /**
     * MODE_WINDOW will put the container to the MODAL window
     * MODEL_INLINE will put the container the current window
     */
    static  MODE_INLINE;
    static MODE_WINDOW;

    #allowFavorite = true;
    #allowAdd = true;
    #allowEdit = true;
    #request;
    #fetchById = false;
    #mode = MagicRemoteChooserOptions.MODE_INLINE;

    #itemRenderer;

    #eventListener;

    #parent;


    #listOptions = null;

    constructor() {
        
    }

    getRequest(){
        return this.#request;
    }

    setRequest(request){
        this.#request = request;
        return this;
    }

    getMode(){
        return this.#mode;
    }

    disableFavorite() {
        this.#allowFavorite = false;
        return this;
    }
    setListOption(listOptions){
        this.#listOptions = listOptions;
        return this;
    }
    disableAdd() {
        this.#allowAdd = false;
        return this;
    }

    disableEdit() {
        this.#allowEdit = false;
    }

    setItemRenderer(renderer) {
        this.#itemRenderer = renderer;

        return this;
    }

    setParent(parent) {
        this.#parent = parent;
        return this;
    }

    setEventListener(listener) {
        this.#eventListener = listener;
        return this;
    }

    enableFetchById() {
        this.#fetchById = true;
        return this;
    }

    getListOptions(){
        return this.#listOptions;
    }

    getParent(){
        return this.#parent;
    }

    getItemRenderer(){
        return this.#itemRenderer;
    }

    getEventListener(){
        return this.#eventListener;
    }

    isFavoriteEnabled(){
        return this.#allowFavorite;
    }

    isEitAllowed(){
        return this.#allowEdit;
    }

    isAddAllowed(){
        return this.#allowAdd;
    }

    fireEvent(type, chooser, element) {
        if (_.isNull(this.#eventListener)) {
            return;
        }

        this.#eventListener(new MagicChooserEvent(type, chooser, element))
    }
}

class MagicRemoteChooser {
    #options;
    #closeElement;
    

    getOptions(){
        return this.#options;
    }

    #providers = [];
    #divSearch;
    #trigger;
    #triggerTextContent;
    #icon;

    constructor(options = new MagicRemoteChooserOptions()) {
        this.#options = options;
        this.#providers.push(new BasicRemoteChooser(this));
    }


    drawTrigger(text = null, src = null) {
        this.#trigger = h.div('chooser').click(() => this.draw());
        this.#icon = h.img(src).appendTo(this.#trigger).hideIf(!src);
        this.#triggerTextContent = h.span('chooser-text-value').text('pages.global.search.select').appendTo(this.#trigger);

        if(text) {
            this.#triggerTextContent.text(text, false);
        }

        return this.#trigger;
    }

    getTextContent() {
        return this.#triggerTextContent;
    }

    draw() {
        this.#closeElement = h.div('close-chooser-element').click(() => this.window.hideModal());
        let tabs = new MagicTab();
        let _this = this;

        _.each (this.#providers, (provider)=> {
            tabs.addLazyTab( provider.getName(), ()=> { return provider.draw().add(this.#closeElement)}, _this )
        });

        this.#divSearch = h.div("search_panel");

        if (_e(this.getOptions().getMode(), MagicRemoteChooserOptions.MODE_WINDOW)){
            this.window = dom.createModal(dom.REMOVE_TYPE_REMOVE)
            .add(h.div("image_close_button").click(()=>{
                this.window.hideModal();
            }))
            .appendToBody()
            this.#divSearch.appendTo(this.window)
        }

        tabs.deployTo(this.#divSearch);
        return this.#divSearch;
    }

    select(data) {
        this.getOptions().fireEvent(MagicChooserEvent.CHOOSER_VALUE_CHANGED, this, data);
        this.#triggerTextContent?.text(data?.name, false);
        this.#icon?.hide();
        this.window.hideModal();
        if(data?.icon && this.#icon) {
            this.#icon.src(data.icon).show();
        }
    }

    close(){
        _.each (this.#providers, (provider)=> {provider.destroy()});
        this.#providers = null;
        this.#options = null;
    }


}

class MagicRemoteChooserProvider {
    #chooser;
    #searchElement;
    #content;

    constructor(chooser) {
        this.#chooser = chooser;

        this.#content = h.div("remote_chooser_content");
        this.createSearchComponent();

    }

    draw() {
        return this.#content;
    }


    createSearchComponent(){
        let options = _.isNull(this.#chooser.getOptions().getListOptions()) 
        ? this.createSearchOptions() 
        : this.#chooser.getOptions().getListOptions();
        
        options.setDisplayType(MagicListOptions.DISPLAY_AS_GREED);

        this.#searchElement = new MagicList2(options);
        this.#searchElement.load(this.#content)

    }

    getName(){}

    destroy() {
        this.#chooser = null;
        this.#searchElement = null;
        this.#content = null;
    }

    select(data) {
        this.#chooser.select(data)
    }

    createSearchOptions() {
        let request = this.#chooser.getOptions().getRequest();
        let options  = new MagicListOptions(request);


        let gridOptions = new MagicGridOptions();
        gridOptions.setDefaultClickFunction((data)=> {
            this.select(data)
        
        })

        if (_.notNull(this.#chooser.getOptions().getItemRenderer()) ){
            gridOptions.setCustomLayoutFunction(this.#chooser.getOptions().getItemRenderer());
        }

        
        options.setGridOptions(gridOptions);
        return options
    }
}


class MagicFavoriteProvider extends MagicRemoteChooserProvider {
    constructor(chooser) {
        super(chooser);
    }
    
    getName() {
        return "global.app.favorite";
    }
}


class BasicRemoteChooser extends MagicRemoteChooserProvider {
    constructor(chooser) {
        super(chooser);
    }

    getName() {
        return "global.app.remote_chooser";
    }
}
