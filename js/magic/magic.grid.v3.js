class GridViewController {
    #containerClassName;
    #displayedFunction;
    #isDefault;
    #viewControllerId;

    constructor(viewControllerId, displayedFunction, isDefault = false, containerClassName) {
        this.#containerClassName = containerClassName;
        this.#displayedFunction = displayedFunction;
        this.#viewControllerId = viewControllerId;
        this.#isDefault = isDefault;
    }

    get isDefault() {
        return this.#isDefault;
    }

    get viewControllerId() {
        return this.#viewControllerId;
    }

    get containerClassName() {
        return this.#containerClassName;
    }

    drawItem(data, dataConverter, grid) {
        return this.#displayedFunction(data, dataConverter, grid);
    }
}

class GridMultipleViews {
    #viewControllers = [];
    #selectedController;
    #grid;
    #container;

    get gridViewIds() {
        return this.#selectedController.map((element) => element.viewControllerId);
    }

    setContainer(container) {
        this.#container = container;

        return this;
    }

    removeContainerClassNames() {
        this.#viewControllers.forEach((viewController) => {
            const className = viewController.containerClassName;
            if(className) {
                this.#container?.rcl(className);
            }
        })
        const selectedClassName = this.#selectedController.containerClassName;
        if(selectedClassName) {
            this.#container?.cl(selectedClassName);
        }
    }

    changeViewController(controllerId) {
        this.#selectedController = this.#viewControllers.find((element) => _e(element.viewControllerId, controllerId)) || null;
        if(!this.#selectedController) {
            return;
        }
        this.removeContainerClassNames();
        this.redraw();
    }

    setGrid(grid) {
        this.#grid = grid;

        return this;
    }

    redraw() {
        this.#grid?.redraw();
    }

    addViewController(viewController) {
        this.#viewControllers.push(viewController);

        return this;
    }

    drawItem(data, dataConverter, grid, itemContainer) {
        if(!this.#selectedController) {
            this.#selectedController = this.getDefaultViewController();
        }
        if(!this.#selectedController) {
            return;
        }
        this.removeContainerClassNames();
        const drawView = this.#selectedController.drawItem(data, dataConverter, grid);

        itemContainer.add(drawView);
    }

    getDefaultViewController() {
        return this.#viewControllers.filter((viewController) => viewController.isDefault)[0] || null;
    }
}

class MagicGridOptions {
    #showEmptyFields = true;
    #customLayoutFunction;
    #multipleViews;
    #defaultClickFunction;
    #container = h.div("item_container");

    getContainer() {
        return this.#container;
    }

    setMultipleViews(multipleView) {
        this.#multipleViews = multipleView;
        return this;
    };

    setDefaultClickFunction(func) {
        this.#defaultClickFunction = func;
        return this;
    }

    getDefaultClickFunction() {
        return this.#defaultClickFunction;
    }

    getMultipleViews() {
        return this.#multipleViews;
    }

    setShowEmptyFields(toShow) {
        this.#showEmptyFields = toShow;
        return this;
    }

    setCustomLayoutFunction(func) {
        this.#customLayoutFunction = func;
        return this;
    }

    getShowEmptyFields() {
        return this.#showEmptyFields;
    }

    getCustomLayoutFunction() {
        return this.#customLayoutFunction;
    }
}

class MagicGrid extends Selectable{
    #options;
    #parent;
    #where;
    #fields = [];
    favoriteEnabled;
    #container;

    constructor(parent, options = new MagicGridOptions()) {
        super(parent.getOptions());
        this.#options = options;
        this.#parent = parent;
        this.favoriteEnabled = Favorite.enabled();
        this.#container = options.getContainer();
    }

    getParent() {
        return this.#parent
    }

    getOptions() {
        return this.#options;
    }

    getWhere() {
        return this.#where;
    }

    redraw() {
        this.#parent.drawValues()
    }

    drawValues(data, list, dataConverter = null, eventListener = null) {
        this.#container.text(null);
        if (_e(data.content.length, 0)) {
            this.fireEvents({"eventName": MagicList2.EVENT_DATA_NOT_EXISTS})
            
        } else {
            this.fireEvents({"eventName": MagicList2.EVENT_DATA_EXISTS})
   
        }
        
        if (dataConverter == null) {
            dataConverter = MagicTable.defaultConverter;
        }
        let _this = this;
        this.resetCheckboxes()
        _.each(data.content, item => {
            _this.drawItem(item, dataConverter)
        })
    }

    rebuildHeaders() {

    }

    addDefaultClickFunction(container, data) {
        const clickFunction = this.#options.getDefaultClickFunction();
        if(!clickFunction) {
            return;
        }
        container.click(()=> {clickFunction(data)});
    }

    drawItem(item, dataConverter) {
        let itemContainer = h.div("item_block");
        this.addDefaultClickFunction(itemContainer, item);
        let headers = this.#parent.getOrdering()?.getHeaders() || this.#fields;

        const multipleViews = this.#options.getMultipleViews();
        if (multipleViews) {
            multipleViews.setGrid(this).setContainer(this.#container).drawItem(item,dataConverter, this, itemContainer);
            this.#container.add(itemContainer);
            return;
        }

        if (_.notNull(this.getOptions().getCustomLayoutFunction())) {
            itemContainer.add(this.getOptions().getCustomLayoutFunction()(item, dataConverter, this));
        } else {

            _.each(headers, (field) => {
                if (field.visible) {

                    let toShow = true;
                    if (_.isObjectEmpty(item[field.field], _.isObject(item[field.field]))) {
                        toShow = this.getOptions().getShowEmptyFields();
                    }

                    if (toShow) {
                        itemContainer
                        .add(h.div("item_value")
                        .add(h.span("element_header", field.translation, true))
                        .add(MagicGrid.transform(item[field.field], field, item, dataConverter, this)));
                    }

                }

            })
            
            if(this.getParent().getOptions().getNeedMultiselect()){

                const checkbox = this.getSelectorRenderer().bind(this)(item.id, itemContainer);
                this.addCheckbox(checkbox, item.id, item)

            }
        }

        this.#container.add(itemContainer);
    }

    getId() {
        return this.#parent.getId();
    }

    drawHeader(where) {

        h.from(where).add(this.#container);
        this.#fields = this.#parent.getLastData().fields;
        this.#where = where;
        return this;

    }

    static transform(itemElement, field, data, dataConverter, self) {

        return dataConverter(field, itemElement, data, self, field.searchType, MagicGrid.favoriteEnabled);
    }
}