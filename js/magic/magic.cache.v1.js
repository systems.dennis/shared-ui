class cache {


    static add(key, value) {
        localStorage.setItem(cache.getKey(key), value);
        return cache;
    }

    static get(key){
        return localStorage.getItem(cache.getKey(key))
    }

    static remove(key) {
        localStorage.removeItem(cache.getKey(key))
        return cache;
    }
    static removeAll(...data) {
        _.each(data, (key ) => {
            localStorage.removeItem( cache.getKey(key))
        })
        return cache;
    }

    static isPresent(key){
        return _.notNull(localStorage.getItem(cache.getKey(key)))

    }

    static copyFrom(keyTo, keyFrom){
        localStorage.setItem(cache.getKey(keyTo), localStorage.getItem(cache.getKey(keyFrom)))
        return cache;
    }

    static getLocalPrefix(){
        return Environment.self + "_" + Environment.version + "_";
    }
    static getKey(key) {
        return cache.getLocalPrefix() + key;
    }

    static toObject(field){
        return new CacheObject(field);
    }

    static x(){
        let prefix = cache.getLocalPrefix();
        for (let key in localStorage){
           if (_.startsWith(key, prefix)){
               cache.remove(key);
           }
        }

        return cache;
    }


}


class CacheObject {
     #obj;
     #objectField;

     constructor(objField) {
         this.#objectField = objField;
         this.#obj = { [objField] : {} };
     }

     set(field, fromKey){
         this.#obj[this.#objectField][[field]] = cache.get(fromKey);
         return this;
     }

     get(){
         return this.#obj;
     }
}