class MagicSliderOptions {
    #offsetStepValue = 100;
    #currentOffset = 0;
    #transition = 300;
    #isInfinite = false;
    #boxGap = 0;
    #isVertical = false;

    setIsVertical(isVertical) {
        this.#isVertical = isVertical;
        return this
    }

    getIsVertical() {
        return this.#isVertical;
    }

    setBoxGap(value) {
        this.#boxGap = value;

        return this;
    }

    getBoxGap() {
        return this.#boxGap;
    }

    setOffsetStepValue(value) {
        this.#offsetStepValue = value;

        return this;
    }

    getOffsetStepValue() {
        return this.#offsetStepValue;
    }

    setCurrentOffset(value) {
        this.#currentOffset = value;

        return this;
    }

    getCurrentOffset() {
        return this.#currentOffset;
    }

    setIsInfinite(boolean) {
        this.#isInfinite = boolean;

        return this;
    }

    getIsInfinite() {
        return this.#isInfinite;
    }

    setTransition(value) {
        this.#transition = value;

        return this;
    }

    getTransition() {
        return this.#transition;
    }
}

class MagicSlider {
    #data = [];
    #parentElement;
    #slidesWrapper;
    #maxOffset;
    #controlls;
    #options;

    constructor(
        parentElement,
        slidesWrapper,
        sliderOptions = new MagicSliderOptions()
    ) {
        this.#options = sliderOptions;
        this.#data = slidesWrapper.childNodesAsArray()
        this.#parentElement = parentElement;
        this.#slidesWrapper = slidesWrapper;
        this.#maxOffset = Math.max(this.getMaxOffset(), 0)
    }

    getMaxOffset() {
        const wrapperSize = this.#options.getIsVertical() ? this.#parentElement.get().offsetHeight : this.#parentElement.get().offsetWidth;
        const slideSize = this.#options.getIsVertical() ? this.#data[0]?.get().offsetHeight : this.#data[0]?.get().offsetWidth
        const slidesPerView = Math.round(
            wrapperSize /
            (slideSize + this.#options.getBoxGap())
        );

        const maxSlides = this.#data.length - slidesPerView;

        return maxSlides;
    }

    getData() {
        return this.#data;
    }

    addControlls(parentElement) {
        this.#controlls = {
            prev: h
                .div("slider_left_arrow div-icon")
                .click(() => this.slideToPrev())
                .prependTo(parentElement),
            next: h
                .div("slider_right_arrow div-icon")
                .click(() => this.slideToNext())
                .appendTo(parentElement),
        };

        return this;
    }

    addSlides(slidesArray, slidesWrapper) {
        slidesArray.forEach((el) => {
            el.appendTo(slidesWrapper);
        });
        return this;
    }

    isLastSlide() {
        const value = this.#options.getCurrentOffset();

        return value === this.#maxOffset;
    }

    isFirstSlide() {
        const value = this.#options.getCurrentOffset();

        return value === 0;
    }

    toggleControllsVisibility() {
        if (!this.#options.getIsInfinite()) {
            this.isFirstSlide()
                ? this.#controlls.prev.hide()
                : this.#controlls.prev.show();

            this.isLastSlide()
                ? this.#controlls.next.hide()
                : this.#controlls.next.show();
        }
    }

    slideToNext() {
        if (this.#options.getIsInfinite() && this.isLastSlide()) {
            this.removeTransition();
            this.#options.setCurrentOffset(0);
            this.moveSlider();
            this.addTransition();
        }

        this.isLastSlide()
            ? this.#options.setCurrentOffset(this.#options.getCurrentOffset())
            : this.#options.setCurrentOffset(this.#options.getCurrentOffset() + 1);

        this.moveSlider();

        return this;
    }

    slideToPrev() {
        if (this.#options.getIsInfinite() && this.isFirstSlide()) {
            this.removeTransition();
            this.#options.setCurrentOffset(this.#maxOffset);
            this.moveSlider();
            this.addTransition();
        }

        this.isFirstSlide()
            ? this.#options.setCurrentOffset(this.#options.getCurrentOffset())
            : this.#options.setCurrentOffset(this.#options.getCurrentOffset() - 1);

        this.moveSlider();

        return this;
    }

    moveSlider() {
        const slidesArray = this.getData();

        const currentSlidelideOffset =
            this.#options.getOffsetStepValue() * this.#options.getCurrentOffset();
        const currentSlideGapOffset =
            this.#options.getBoxGap() * this.#options.getCurrentOffset();

        const axis = this.#options.getIsVertical() ? 'translateY' : 'translateX'

        _.each(slidesArray, (el) => {
            el.get().style.transform = `${axis}(calc(-${currentSlidelideOffset}% - ${currentSlideGapOffset}px))`

        })

        this.toggleControllsVisibility();
    }

    build() {
        const slider = this.#slidesWrapper;
        const slidesArray = this.getData();

        this.addTransition(slidesArray);
        this.addControlls(this.#parentElement)
        this.toggleControllsVisibility();
    }

    addTransition() {
        this.getData().forEach(
            (el) => (el.get().style.transition = `${this.#options.getTransition()}ms`)
        );

        return this;
    }

    removeTransition() {
        this.getData().forEach((el) => (el.get().style.transition = "none"));

        return this;
    }
}
