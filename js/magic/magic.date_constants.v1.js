class DateConstants {
  static MONTHS = [
    "global.calendar.january",
    "global.calendar.february",
    "global.calendar.march",
    "global.calendar.april",
    "global.calendar.may",
    "global.calendar.june",
    "global.calendar.july",
    "global.calendar.august",
    "global.calendar.september",
    "global.calendar.october",
    "global.calendar.november",
    "global.calendar.december",
  ];

  static WEEKDAYS = [
    "global.calendar.days.mon",
    "global.calendar.days.tue",
    "global.calendar.days.wed",
    "global.calendar.days.thu",
    "global.calendar.days.fri",
    "global.calendar.days.sat",
    "global.calendar.days.sun"];

  static getArrayOfYears(rangeCount = 5) {
    const currentYear = new Date().getFullYear();
    const years = [currentYear + ""];

    for (let index = 0; index < rangeCount; index++) {
      const next = currentYear + index;
      const prev = currentYear - index;
      if(!_e(prev, currentYear) && !_e(next, currentYear)) {
          years.push(next + "");
          years.unshift(prev + "");
      }
    }

    return years;
  }
}
