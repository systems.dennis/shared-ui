class MagicList2 {
  #initial = {};
  #listListener;

  #container;
  #magicTable;

  #headers;

  #grid = h.div("pg_table_container");

  #form = h.div("modal").cl("hidden").id(new Date().getTime());

  #sortAndSearchForm = h.div("table_config");
  #sort;

  #searchForm;

  #ordering;

  #favoriteType;
  #lastData;

  #pagination;

  #id;

  getGrid() {
    return this.#grid;
  }

  getFavoriteType() {
    return this.#favoriteType;
  }

  getPagination() {
    return this.#pagination;
  }
  getId() {
    return this.#id;
  }

  getLastData() {
    return this.#lastData;
  }

  getOrdering() {
    return this.#ordering;
  }

  getTableObject() {
    return this.#magicTable;
  }

  getContainer() {
    return this.#container;
  }

  constructor(
    dataType,
    dataPath = "/root/fetch/data",
    specificationRoot = "/root/fetch/list",
    apiRoot = "/api/v2/",
    listListener = null
  ) {
    /**
     * We assume that here can only 1 parameter to be changed.
     * For this, we accept nulls as value by default
     */
    if (specificationRoot == null) {
      specificationRoot = "/root/fetch/list";
    }
    if (apiRoot == null) {
      apiRoot = "/api/v2/";
    }

    if (dataPath == null) {
      dataPath = "/root/fetch/data";
    }
    this.#initial = {
      dataType: dataType,
      dataPath: dataPath,
      specificationRoot: specificationRoot,
      apiRoot: apiRoot,
    };

    /**
     * List listener is a global list listener to be run
     * @type function
     */
    this.#listListener = listListener;
  }

  getForm() {
    return this.#form;
  }

  getSort() {
    return this.#sort;
  }

  getInitData() {
    return this.#initial;
  }

  getListListener() {
    return this.#listListener;
  }

  getSearchAndSettingsBar() {
    return this.#sortAndSearchForm;
  }

  getRootPath() {
    return (
      Environment.dataApi + this.#initial["apiRoot"] + this.#initial["dataType"]
    );
  }

  load(where) {
    let _this = this;

    let path =
      Environment.dataApi +
      this.#initial["apiRoot"] +
      this.#initial["dataType"] +
      this.#initial["specificationRoot"];

    Executor.runGet(path, function (data) {
      _this.build(data, where);
    });
  }

  build(data, where) {
    this.#lastData = data;
    this.#id = where;
    this.#container = h.from(get(where));

    this.drawListTitle(where);
    //form is used to generate edit form or add form
    this.#form.appendTo(this.#container);

    //grid is a container where <table> is generated
    this.#grid.appendTo(this.#container);
    this.drawPagination();

    //when we are ready we can draw values from the server

    this.drawValues();

    return this;
  }

  drawListTitle() {
    let data = this.#lastData;
    this.#favoriteType = data.objectType;
    if (data.showTitle) {
      h.div("title").text(data.tableTitle).appendTo(this.#container);
    }
    this.#headers = data; //todo check if we still need to hold it here
    this.#sortAndSearchForm.appendTo(this.#container);
    this.drawSettingsForm();
    this.drawSearch(data);
    this.drawTableActions(data);
    this.createDataHeaders(data["fields"]);
  }

  createDataHeaders() {
    this.#magicTable = new MagicTable(this).drawHeader(this.#grid);
  }

  drawSettingsForm() {
    this.#sort = new MagicSort(this);
    this.#sort.build();

    this.#ordering = new MagicOrdering(this);
    this.#ordering.build();
  }

  drawTableActions(data) {
    //List actions are coming from server, if they are existing we need to draw them
    //Default action are: settings : open list settings
    //                    download : download the content of the table
    //                    new      : creates a new element
    // if action differs then it is expected that
    //                   drawCustomAction is defined by user
    // this method should be loaded on the page itself
    if (utils.isEmpty(data.listActions)) {
      return;
    }

    let div = h.div("actions");

    for (let i = 0; i < data.listActions.length; i++) {
      try {
        div.add(this.drawTableAction(data.listActions[i], div, data));
      } catch (ex) {
        log.error(ex);
      }
    }

    this.getSearchAndSettingsBar().add(div);
  }

  drawTableAction(action, div, data) {
    let _this = this;
    if (action == "settings") {
      return h
        .tag("span")
        .cl("settings")
        .add(
          h
            .divImg("sort_icon.png.png")
            .cl(action)
            .wh(16)
            .click(function () {
              _this.getSort().show();
              _this.getOrdering().close();
            })
        )
        .add(
          h
            .divImg("settings.png")
            .cl(action)
            .wh(16)
            .click(function () {
              _this.getOrdering().show();
              _this.getSort().close();
            })
        )
        .get();
    }

    if (action == "download") {
      let _this = this;

      return h
        .divImg("download.png")
        .cl(action)
        .wh(16)
        .click(function () {
          $.ajax({
            beforeSend: Executor.prepareAuth,
            type: Executor.POST_METHOD,
            data: JSON.stringify(_this.createPayload()),
            headers: Executor.HEADERS,
            url: _this.getRootPath() + "/root/download/data",
            error: function (e) {
              log.error(e);
            },
            success: function (data) {
              h.a(
                _this.getRootPath() +
                  "/root/download/data/" +
                  data.pathToDownload,
                "",
                false
              )
                .get()
                .click();
            },
          });
        })
        .get();
    }

    if (action == "new") {
      let _ = this;

      this.#grid.add(div);
      return h
        .input("button")
        .cl(action)
        .cl("insert_new_btn")
        .text("pages.forms.insert_new")
        .click(function () {
          new fetcher(
            _.#form.get().id,
            "/api/v2/" + _.#initial["dataType"],
            "form",
            null,
            _
          ).new();
        })
        .get();
    }

    if (action == "import") {
      return h
        .divImg("import.png")
        .wh("16")
        .cl(action)
        .click(function (e) {
          if (e.getAttribute("disabled") != undefined) {
            h.message("global.messages.import.in_process");
          }

          let progress = new Loading(_this.getGrid().get());
          progress.show(e);

          Executor.runPost(
            _this.getRootPath() + "/import/",
            function () {
              progress.hide();
              dom.toast(_.$("global.messages.success"));
              _this.drawValues();
            },
            function (e, x, z) {
              progress.hide();
              Executor.errorFunction(e, x, z);
            }
          );
        })
        .get();
    }

    try {
      return drawCustomAction(action, div, null, this, data);
    } catch (ex) {
      log.error(ex);
      return h.span(
        "info.error.list.actions",
        _.$("global.list.actions.not.implemented." + action)
      );
    }
  }

  drawValues() {
    let path =
      Environment.dataApi +
      this.#initial["apiRoot"] +
      this.#initial["dataType"] +
      this.#initial["dataPath"];
    let _this = this;
    Executor.runPostWithPayload(
      path,
      function (data) {
        _this.buildValues(data);
      },
      this.createPayload()
    );
  }

  buildValues(data) {
    this.#magicTable.rebuildHeaders();
    this.#magicTable.drawValues(data, this);
    this.getPagination().rebuildPages(data);
  }

  drawPagination() {
    this.#pagination = new MagicPagination(this.#magicTable);
    this.#pagination.build(this.getId());
  }

  drawSearch(data) {
    this.#searchForm = new SearchForm(data, this.getId(), this);
    this.#searchForm.build();
  }

  createPayload() {
    if (this.#pagination == null) {
      return new MagicRequest();
    }
    let req = new MagicRequest();

    let page = this.#pagination.getPageNumber();

    let perPage = this.#pagination.getPerPage();
    req.limit = perPage;
    req.page = page;

    req.query = this.#searchForm.getSearchCriteria();

    req.sort = this.getSort().getSort();

    return req;
  }
}
