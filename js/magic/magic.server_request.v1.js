/**
 *
 *
 *         https://test_server/api/v2/api_catalog/api_sub_catalog/root/fetch/list
 *         [root             ][api  ] [entry]    [sub_entry]     [request]
 *
 */

const RESPONSE_TYPES = {
    SUCCESS: "success__response",
    ERROR: "error__response"
}

class CacheProvider {


    addToCache(request, data, type){};

    deleteFromCache(request){};

    getFromCache(request, timeout, success, fail){};

}

class LocalCacheProvider extends CacheProvider {
    static cachedItems = new Map();

    static #INSTANCE ;

    static getInstance(){
        if (_.isNull(this.#INSTANCE)){
            this.#INSTANCE = new LocalCacheProvider();
        }
        return this.#INSTANCE;
    }

    newRecord(request, timeout, success, fail, crossPage = false) {
        request.run((data) => {
                new CacheInterceptor(request, data, timeout, crossPage, RESPONSE_TYPES.SUCCESS)
                success(data)
            }
            , (error) => {
                new CacheInterceptor(request, error, timeout, crossPage, RESPONSE_TYPES.ERROR);
                if(fail && _.isFunction(fail)){
                    fail(error);
                }
            }, true);
    }
    getFromCache(request, timeout, success, fail) {
        let existingPayloads = LocalCacheProvider.cachedItems.get(request.getFullPath());
        if (_.isNull(existingPayloads)) {
            log.promisedDebug(() => {
                return {"type": "fetch", "text": "no item for url", "request": request}
            })

            this.newRecord(request.forceNoCache(), timeout, success, fail);


        } else {
            let item = _.find(existingPayloads, (e) => {
                return _.e(e.payload, request.getPayload(), true)
            })

            if (_.isNull(item)) {

                log.promisedDebug(() => {
                    return {"type": "fetch", "text": "adding for current payload", "request": request}
                })
                this.newRecord(request.forceNoCache(), timeout, success, fail);

            } else {
                log.promisedDebug(() => {
                    return {"type": "fetch", "text": "USING CACHE DATA", "request": request}
                });
                if (_e(item.type, RESPONSE_TYPES.SUCCESS)) {
                    success(item.value);
                } else {
                    if (fail && _.isFunction(fail)) {
                        fail(item.value);
                    }
                }
            }
        }
    }


    addToCache(request, data, type) {


        let existingPayloads =  LocalCacheProvider.cachedItems.get(request.getFullPath());
        if (_.isNull(existingPayloads)) {
            LocalCacheProvider.cachedItems.set(request.getFullPath(), [{"payload": request.getPayload(), "value": data, "type": type}])
        } else {
            let item = _.find(existingPayloads, (e) => {
                return _.e(e.payload, request.getPayload(), true)
            })

            if (_.isNull(item)) {
                existingPayloads.push({"payload": request.getPayload(), "value": data, "type": type});
                log.promisedDebug(() => {
                    return {"text": "Cached item added with new Payload", "request": request}
                })
            } else {
                log.promisedDebug(() => {
                    {
                        return {"text": "Cached item skipped as existing", "request": request}
                    }
                });
            }
        }


    }

    deleteFromCache(item) {
        //todo remove only with payload that equals current
        LocalCacheProvider.cachedItems.delete(item.getFullPath());
        log.promisedDebug(() => {
            {
                return {"text": "Deleted ", "request": item.getFullPath()}
            }
        });

    }

}

class StorageCacheDataElement {
    cacheTimeMc;
    response;
    constructor(response) {
        this.response = response;
        this.cacheTimeMc = new Date().getTime();
    }

    toString() {
        return JSON.stringify(this);
    }
}

class StorageDataTimeController {
    #response;
    #timeout;
    #cacheTimeMc;
    #defaultTimeOut = 600000;
    // сутки
    #maxTimeout = 86400000;
    #parseData;

    constructor(data, timeout) {
        this.#timeout = timeout;
        this.parseData(data);
    }

    parseData(data) {
        if (!data) {
            return;
        }
        this.#parseData = JSON.parse(data);
        this.#response = this.#parseData?.response;
        this.#cacheTimeMc = this.#parseData?.cacheTimeMc;
    }

    getResponse() {
        return this.#response;
    }

    isDataValid() {
        if (!this.#cacheTimeMc && !this.#parseData) {
            return false;
        }
        const timeOut = !this.#timeout || this.#timeout < 0 || this.#timeout > this.#maxTimeout ? this.#defaultTimeOut : this.#timeout;
        const nowMc = new Date().getTime();
        return nowMc - timeOut < this.#cacheTimeMc;
    }
}

class StorageCacheProvider extends CacheProvider {

    static #INSTANCE ;

    static getInstance(){
        if (_.isNull(this.#INSTANCE)){
            this.#INSTANCE = new StorageCacheProvider();
        }
        return this.#INSTANCE;
    }
    getFromCache(request, timeout, success, fail) {
        this.validateRequest(request);
        const data = cache.get(this.getKey(request));
        const dataTimeController = new StorageDataTimeController(data, timeout);
        const isDataValid  = dataTimeController.isDataValid();
        if (!isDataValid){
            request.forceNoCache().run((data)=> {
                this.addToCache(request, data);
                success(data);
            }, (error) => {
                this.addToCache(request, error);
                fail(error);
            })
        } else {
            const response = dataTimeController.getResponse();
            success(response);
        }
    }

    deleteFromCache(request) {
        cache.remove(this.getKey(request))
    }

    addToCache(request, data) {
        this.validateRequest(request);
        const dataElement = new StorageCacheDataElement(data);
        cache.add(this.getKey(request), dataElement.toString());
    }

    getKey(request){
        if (_.isNull(request.getPayload() )){
            return request.getFullPath();
        } else {
            return request.getFullPath() + "_" + JSON.stringify(request.getPayload())
        }
    }

    validateRequest(request){
        if (!_.isNull(request.getPayload())) {
            log.promisedDebug(()=> {return {"text" : "wrong usage of localstorage, this might be a problem to use localstorage with payload"}})
        }
    }
}

class CacheInterceptor {

    #payload;
    #path;
    #value;
    #provider;


    constructor(request, value, timeout = 60000, crossPage = false, type = RESPONSE_TYPES.SUCCESS) {

        if (_.isNull(request)) {
            log.promisedDebug(() => {
                return {"text": "Cached item saved as newly came request", "request": request}
            })
            return null;

        }
        let provider = !crossPage ? new LocalCacheProvider() : new StorageCacheProvider();
        this.#payload = request.getPayload();

        provider.addToCache(request, value, type);

        this.#path = request.getFullPath();

        let _this = this;
        if (timeout != null) setTimeout(() => {
            provider.deleteFromCache(request)
            _this.destroy()
        }, timeout);


    }

    getPath() {
        return this.#path;
    }

    static fetch(request, timeout, success, fail, crossPage = false) {

        if (_.isNull(request)) {
            log.promisedDebug(() => {
                return {"text": "request should not be null"}
            })
            return null;
        }

        let provider = !crossPage ? LocalCacheProvider.getInstance() : StorageCacheProvider.getInstance();
        provider.getFromCache(request, timeout, success, fail);

    }


    destroy() {
        this.#path = null;
        this.#payload = null;
        this.#value = null;
        log.promisedDebug(() => {
            {
                return {"text": "Destroyed object"}
            }
        });
    }
}


class ServerRequest {
    #root = Environment.dataApi;
    #apiPath = "api/v2";

    #entry;

    #fullPath;
    #subEntry;
    #crossPageCache = false;

    #request;
    #forceNoCache = Environment.test;
    
    #method = Executor.GET_METHOD;

    #scope = Environment.authScope;

    #payLoad;
    #async = true;
    #headers = Executor.HEADERS;
    #cacheTimeOut = 60000;

    eq(request) {
        return utils.e(this.getFullPath(), request.getFullPath()) && utils.e(this.#payLoad, request.getPayload())
    }

    getPayload() {
        return this.#payLoad
    }

    getSubEntry() {
        return this.#subEntry;
    }

    forceNoCache(force = true) {
        this.#forceNoCache = force;
        return this;
    }

    subEntry(s) {
        this.#fullPath = null;
        this.#subEntry = s;
        return this;
    }

    scope(s) {
        this.#scope = s;
        return this;
    }

    getScope(){
        return this.#scope;
    }

    headers(h) {
        this.#headers = h;
        return this;
    }

    async(a) {
        this.#async = a;
        return this;
    }

    method(m) {
        this.#method = m;
        return this;
    }

    payload(p) {
        this.#payLoad = p;
        return this;
    }

    cacheResponse(cacheTimeOut = null, crossPage = false) {
        this.#cacheTimeOut = cacheTimeOut;
        this.#crossPageCache = crossPage;
        return this;
    }

    request(r) {
        this.#fullPath = null;
        this.#request = r;
        return this;
    }

    entry(e) {
        this.#fullPath = null;
        this.#entry = e;
        return this;
    }

    api(a) {
        this.#fullPath = null;
        this.#apiPath = a;
        return this;
    }

    root(r) {
        this.#fullPath = null;
        this.#root = r;
        return this;
    }

    getRequest() {
        return this.#request;
    }


    copy(destroy, newURI) {
        let request = new ServerRequest()
            .root(this.#root).async(this.#async).scope(this.#scope).payload(null)
            .subEntry(this.#subEntry).entry(this.#entry).request(newURI).method(this.#method)
            .api(this.#apiPath).headers(this.#headers).cacheResponse(this.#cacheTimeOut);

        if (this.#forceNoCache) {
            request.forceNoCache();
        }

        if (destroy) {
            this.request(null).api(null).method(null).headers(null)
                .request(null).subEntry(null).payload(null).scope(null);
        }
        return request;
    }

    run(success, fail = null) {

        if (!this.#forceNoCache) {
            CacheInterceptor.fetch(this, this.#cacheTimeOut, success, fail, this.#crossPageCache);
            return;
        }


        if (this.#payLoad) {

            if (_.e(this.#method, Executor.POST_METHOD)) {
                Executor.runPostWithPayload(this.getFullPath(), this.successFunction(success), this.#payLoad, fail, this.#headers)
            } else {
                Executor.runPutWithPayload(this.getFullPath(), this.successFunction(success), this.#payLoad, fail, this.#headers)
            }
        } else {
            Executor.run(this.getFullPath(), this.#method, success, this.#async, fail, this.#headers);
        }
    }

    successFunction = (success) => {
        if (!this.#cacheTimeOut || !this.#payLoad) {
            return success;
        }

        return (data) => {
            success(data);
        }
    }

    shared(subEntry) {
        return this.api("api/v2").entry("shared").subEntry(subEntry);
    }

    getFullPath() {
        if (this.#fullPath != null) {
            return this.#fullPath;
        }
        this.#fullPath = this.#root + "/" + this.#apiPath + "/" + this.#entry + "/" + this.#subEntry + "/" + this.#request;
        return this.#fullPath;
    }

    getUniqueId() {
        return this.#apiPath + this + "entry" + this.#subEntry;
    }

    search(magicRequest, success, fail) {
        this.payload(magicRequest).method(Executor.HEADERS).method(Executor.POST_METHOD).request("root/fetch/data").run(success, fail)
    }

    searchWithoutRun(magicRequest){
        return this.payload(magicRequest).method(Executor.HEADERS).method(Executor.POST_METHOD).request("root/fetch/data")
    }

    formFetch(id, success, fail) {
        this.request("root/fetch/form" + (_.isNull(id) ? "" : "?id=" + id)).method(Executor.GET_METHOD).run(success, fail);
    }

    formFetchWithoutRun(id, success, fail) {
        return this.request("root/fetch/form" + (_.isNull(id) ? "" : "?id=" + id)).method(Executor.GET_METHOD);
    }

    fetchData(){
        return this.request("root/fetch/data").method(Executor.GET_METHOD)
    }

    listFetch(success, fail) {
        this.request("root/fetch/list").method(Executor.GET_METHOD).cacheResponse(600000, true).run(success, fail);
    }

    listFetchWithoutRun(){
       return this.request("root/fetch/list").method(Executor.GET_METHOD)
    }

    byId(id, success, fail) {
        this.request("id/" + id).run(success, fail);
    }

    download(payLoad, success, fail) {
        this.payload(payLoad).forceNoCache().request("/root/download/data").method(Executor.POST_METHOD).run(success, fail);
    }

    import(success, fail) {
        this.request("/import").forceNoCache().method(Executor.POST_METHOD).run(success, fail);
    }

    fetchDetails(id, success, fail) {
        this.request("/root/fetch/details/" + id).method(Executor.GET_METHOD).run(success, fail)
    }

    list(success, fail) {
        this.request("list").method(Executor.GET_METHOD).run(success, fail);
    }

    add(payload, success, fail) {
        this.request("add").forceNoCache().method(Executor.POST_METHOD).payload(payload).run(success, fail);
    }

    edit(payload, success, fail) {
        this.request("edit").forceNoCache().method(Executor.PUT_METHOD).payload(payload).run(success, fail);
    }

    delete(id, success, fail){
        this.request("delete/" + id).method(Executor.DELETE_METHOD).run(success, fail);
    }

    searchType(type, success, fail){
        this.api("api/v1").entry("/search").subEntry("/type/").request(type + "?s=").method(Executor.GET_METHOD).run(success, fail);
    }

    getById(id, success, fail){
        this.request("get/"+id).method(Executor.GET_METHOD).run(success, fail);
    }

}