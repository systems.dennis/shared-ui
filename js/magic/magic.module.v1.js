

class MagicPromptButton extends Button{
    static OK_LABEL = "global.ok"
    static CANCEL_LABEL = "global.cancel"
    constructor(name,  click = null, icon = null, isAction = false ) {
        super(name,  click = null, icon = null, isAction = false )
    }
}

class Component {
    static createButton(text, click, cl) {
        return h.input('button').text(text).click(click).clIf(_.notNull(cl), cl);
    }

    static createEditButton(request, id, text = "global.app.edit", cl = "edit_button", formOptions = new MagicFormOptions()) {
        return Component.createButton(text, () => {
            formOptions.setServerRequest(request)
                .setFormMultiServerRequest(searchSeverByField)

            MagicForm.Instance(request.getUniqueId(), formOptions).load(id);
        }, cl)
    }

    static createDeleteButton(id, request, list = null) {
        return h.input('button').cl("delete_button").text('global.app.delete').click(() => {
            Component.simpleDeletePrompt(() => {
                request.delete(id, () => {
                    dom.message();
                    list?.drawValues();
                })
            })
        })
    }

    static simpleDeletePrompt(success, fail = () => {
    }, title = "global.prompt.confirm_delete", text ="global.prompt.delete_item", buttons = null ) {
        let opts = new MagicPromptOptions(title, text, buttons);
        opts.applyClick(MagicPromptButton.OK_LABEL, () => {
            success()
            opts.closeSelf()
        }).setIsAction(MagicPromptButton.OK_LABEL, true)
            .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                opts.closeSelf()
                fail();
            }).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
            .allowNoRepeat("global.prompt_DELETE").newPrompt();
    }


}

class AbstractTableFilter {

    #parent;

    setParent(parent) {
        this.#parent = parent;
        return this;
    }

    getParent() {
        return this.#parent;
    }

    reload() {
        this.#parent?.reload();
    };

    createComponent(where) {

    }

    toQuery(query) {

    }

    /**
     * binded to productfilter itself
     */
    countedItemQueryRender(item, index) {
        let counter = h.span("item_count");

        this.getParent().getRequest().copy().setLimitQuery(this.countQuery(item.value))
            .search(MagicRequest.ofSingle(), (data) => {
                counter._text(this.inBrakes(data.totalElements))
            })
        return h.div("item_value")
            .addIf(item.value, h.span("item_value").cl('dropdown_value_item_'+ item.value)._text(item.key))
            .add(counter)
    }

    countQuery(value) {
        if (_.e(value, "all")) {
            return null;
        }

        return this.prepareCountQuery(value);
    }

    getField() {

    }

    prepareCountQuery(value) {
        return Query.eq(this.getField()).value(value);
    }

    inBrakes(text) {
        return " (" + text + ")";
    }


}

class AbstractTrueFalseFilter extends AbstractTableFilter {

    #value;
    #module;
    #field;


    constructor(module, field, defaultValue) {
        super()
        this.#module = module;
        this.#field = field;
        this.#value = defaultValue;

    }


    createComponent(where) {
        let trueFalse = new MagicTrueFalseSelectorOptions("global.pages.index.selectors." + this.getField(), this.isSelected(), true, false, true)
        trueFalse.setComponentListener(AbstractTrueFalseFilter.selectValue.bind(this))
        trueFalse.setParent(this.#module)
        return new MagicTrueFalseSelector(this.modifyOptions(trueFalse)).draw(where);
    }

    modifyOptions(options) {
        return options;
    }

    //@binded to  filter itself
    static selectValue(val) {
        if (!val.changed) {
            return;
        }
        this.#value = val.currentValue;
        try {
            this.#module.reload()
        } catch (e) {
            log.promisedDebug(() => {
                return {"message": "Module reload method caused an exception", "module": this.#module, "error": e}
            })
        }
    }

    isSelected() {
        return this.#value;
    }

    toQuery(query) {
        if (this.isSelected()) {
            query.and(Query.eq(this.getField()).value(this.getValue()))
        }
    }

    setValue(val) {
        this.#value = val;
        return this;
    }

    getField() {
        return this.#field;
    }

    getValue() {
        return this.#value;
    }

    getModule(){
        return this.#module;
    }

}

class ChooserTrueFalseFilter extends AbstractTrueFalseFilter {
    #searchName;


    getSearchName() {
        return this.#searchName;
    }

    constructor(module, name, defaultValue, searchName = null) {
        super(module, name, defaultValue);
        this.#searchName = _.ofNullable(searchName, () => name)
    }

    toQuery(query) {

        if (this.isSelected()) {
            query.and(Query.eq(this.getField()).value(this.getValue()).chooser().searchName(this.getSearchName()))
        }
    }
}

class ChooserInFilter extends AbstractTableFilter {
    #dropDown;
    #request;
    #field;
    #keyField;
    #searchName;
    #renderer
    #enableMultiselect = true
    #searchEnabled = true

    /**
     *
     * @param request request for the dropdown
     * @param field - a field to search in a list [Object.id, Object.performer, etc]
     * @param searchName - to use for the chooser, if searchName is null then searchName is taken from field
     * @param keyField - a field to search at
     *
     */
    constructor(request, field, searchName = null, keyField = 'name') {

        super();
        this.#request = request;
        this.#field = field;

        this.#keyField = keyField;
        this.#searchName = _.ofNullable(searchName, () => field)
    }

    getDropDown() {
        return this.#dropDown;
    }

    setDropdown(dropdown) {
        this.#dropDown = dropdown
    }

    setRenderer(renderer) {
        this.#renderer = renderer;
        return this;
    }

    getRequest() {
        return this.#request
    }

    getRenderer() {
        return this.#renderer
    }

    getField() {
        return this.#field;
    }

    prepareCountQuery(value) {
        return Query.eq(this.getField()).value(value).chooser().searchName(this.getSearchName())
    }

    getSearchName() {
        return this.#searchName;
    }

    getKey() {
        return this.#keyField;
    }

    turnOffMultiselect() {
        this.#enableMultiselect = false
        return this
    }

    turnOffSearchEnabled(){
        this.#searchEnabled = false
        return this
    }

    isSearchEnabled() {
        return this.#searchEnabled
    }

    isMultiselectEnabled () {
        return this.#enableMultiselect
    }

    createComponent(where) {
        const statusDataProvider = this.modifyProvider(new RemoteDataProvider(this.#request, this.#keyField))

        const options = new MagicDropDownOptions(statusDataProvider)
            .setIsSearhEnabled(this.#searchEnabled)
            .setItemRenderer(this.#renderer)
            .setTagRenderer(this.#renderer)
            .setIsDeleteTagButtonEnabled(false)
            .setPreSelectedValue(MagicDropDownOptions.SELECT_NONE)
            .addListener((event) => {
                if (_e(event.eventName, DropDownEvent.DD_EVENT_ITEM_SELECTED) || _e(event.eventName, DropDownEvent.DD_EVENT_ITEM_DESELECTED)) {
                    this.reload()
                }
            }, false)

        if(this.#enableMultiselect){
            options.enableMultiSelect()
        }
        this.#dropDown = new MagicDropDown(this.modifyOptions(options))

        where.add(this.#dropDown.build().values);
    }

    modifyOptions(options) {
        return options;
    }


    modifyProvider(provider) {
        return provider;
    }


    toQuery(query) {
        let values = Array.from(this.#dropDown.getSelectedItems())
        if (!_.isEmpty(values)) {
            query.and(Query.in(this.#field).value(values).chooser().searchName(this.#searchName))
        }
    }
}

class Scoped {
    #scope;

    getScope() {
        return this.#scope;
    }

    setScope(scope) {
        this.#scope = scope;
        return this;
    }
}

class Converter extends Scoped {
    static ID_FIELD = "id";
    static NO_IMAGE = "null.png";
    #data;

    get(what) {
        if (_.isNull(this.#data)) {
            log.promisedDebug(() => {

                return {"field": what, "error": "product_not_loaded"}
            })
            return null;
        }
        return this.#data[what];
    }

    getRequest() {

    }

    getId() {
        return this.get(Converter.ID_FIELD)
    }

    getData() {
        return this.#data
    }

    load(data) {
        if (!_.isObject(data)) {
            try {

                this.getRequest().copy().async(false).byId(data, (res) => {
                    data = res
                }, () => {
                    data = {
                        "id": data
                    }
                    log.promisedDebug(() => {
                        return {"product": data, "error": "not_found"}
                    })
                });
            } catch (Exception) {
                this.invalid = true;
            }
        }

        this.#data = data;
        return this;
    }
}

class Module {
    #data;
    #scope = Environment.authScope;

    getData() {
        return this.#data;
    }

    setScope(scope) {
        this.#scope = scope;
        return this;
    }

    getScope() {
        return this.#scope;
    }

    constructor(data = undefined) {
        this.#data = data;
    }

    /**
     * Generally use to initialize values before class instance is processed
     */
    init() {
    };

    /**
     * renders ui for module
     *
     * always returns h component to be injected later
     * NEVER NULL
     */
    build() {

    }


}


h.prototype.load = function (list) {
    list.load(this);
    return this
}


h.prototype.authorizedClick = function (f, scope, preventParentClick = true) {

    this.setData("scope", scope);

    this.click(() => {
        if (!this.getData("scope")) {
            f(this.get(), preventParentClick)
        } else {
            let scope = this.getData("scope");
            let auth = AuthUtils.getAuthorizationByScope(scope);
            if (!auth.hasToken()) {
                log.promisedDebug(() => {
                    return {
                        "searching_for": scope,
                        "message": "no token is present. what a pity now creating authorization window"
                    }
                })
                //login window

                AuthUtils.requestAuthorization(scope);
            } else {
                log.promisedDebug(() => {
                    return {"searching_for": scope, "message": "token found, we do required actions"}
                })
                f(this.get(), preventParentClick)
            }
        }

    }, preventParentClick);


}

class ListableModule extends Module {
    static DELETE = "delete"
    #list;
    #noDataComponent = null
    #filters = [];
    #sorts = [];
    #initialized = false;

    constructor(data) {
        super(data);
    }

    addFilter(...filter) {
        if(!_.isEmpty(filter)) {
            _.each(filter, (item) => {
                if (!(item instanceof AbstractTableFilter)) {
                    throw new Error("Filter should be of type AbstractTableFilter")
                }
                this.#filters.push(item.setParent(this))
            })
        }

        return this;
    }

    addSort(...sort){
        if(!_.isEmpty(sort)) {
            _.each(sort, (item) => {
                if (!(item instanceof Sort)) {
                    throw new Error("Sort should be of type Sort")
                }
                this.#sorts.push(item)
            })
        }

        return this;
    }


    setNoDataComponent(noDataComponent) {
        this.#noDataComponent = noDataComponent

        return this
    }

    reload(resetPage = false) {
        if (!this.#initialized) {
            return;
        }
        this.#list.drawValues();

        if (resetPage) {
            this.#list.getOptions().getCustomPagination().changePage(0)
        }
    }

    getList() {
        return _.ofNullable(this.#list, () => {
            return this.createList()
        });
    }

    createList(layout = null, searchEnabled = true) {
        let options = this.createOptions()
        this.#list = new MagicList2(this.modifyOptionsWithGreed(options, layout, searchEnabled))
        return this.#list;
    }

    getRequest() {
        return null;
    }

    createOptions() {
        return new MagicListOptions().setServerRequest(_.ofNullable(this.getRequest(), () => new Error("empty request!")));
    }

    modifyOptions(options, searchEnabled) {
        let formOptions = options.getFormOptions();
        if (!_.isNull(formOptions)) { //todo
            formOptions.setServerRequest(options.getRequest());
            formOptions.setIsDragFields(false);
            formOptions.setScope(this.getScope());
        }
        options.getSearchFormOptions().setNeedSavedItems(false);
        options.setQueryRetriever(this.modifyQuery.bind(this));
        options.setSearchEnabled(searchEnabled)

        options.setNoRepeatPropertyPrompt(null)

        if (this.#noDataComponent) {
            options.setListListeners((data) => this.drawEmptyBlock(data, options.getGridOptions().getContainer()));
        }

        return options;
    }

    drawEmptyBlock(eventData, parentContainer) {
        if (_e(eventData.eventName, MagicList2.EVENT_DATA_NOT_EXISTS)) {

            parentContainer.add(this.#noDataComponent);
        }

        const emptyBlock = h.fromId(this.#noDataComponent.getId());
        if (_e(eventData.eventName, MagicList2.EVENT_DATA_EXISTS) && emptyBlock) {
            emptyBlock.remove();
        }
    }

    modifyOptionsWithGreed(options, layout, searchEnabled) {
        this.modifyOptions(options, searchEnabled)
        if (_.notNull(layout)) {
            ListableModule.setAsGreed(layout, options);
        }
        return options
    }

    static setAsGreed(layout, options) {
        options
            .setGridOptions(new MagicGridOptions().setCustomLayoutFunction(layout))
            .setDisplayType(MagicListOptions.DISPLAY_AS_GREED);
        return options;
    }

    // Expect null or Query Object
    modifyQuery() {

        let query = new Query();

        _.each(this.#filters, (filter) => {
            filter.toQuery(query);
        })

        _.each(this.#sorts, (sort) => {
            query.getSort().merge(sort);
        })
        return query;
    }

    /**
     * Create a div with a filters that had been added
     * filter div = a custom filter div to build filter. if null, a new is created, otherwise provided is used
     * @returns {*}
     */
    build(filterDiv = null) {
        let container = this.createTableContainer();
         filterDiv = _.ofNullable(filterDiv, ()=>  h.div("filter"));

        _.each(this.#filters, (filter) => {
            filter.createComponent(filterDiv)
        })

        this.#initialized = true;
        return container.add(filterDiv).add(h.div("content").load(this.getList()))
    }

    createTableContainer() {
        return h.div("content")
    }

    getListOptions(){
        return this.#list.getOptions()
    }


}


class ModalModuleOptions {
    #useCurtain = true;
    #position;
    #removeType = dom.REMOVE_TYPE_REMOVE;
    #listener;
    #request;
    #displayTitle = {"title": "", "display": true}

    constructor(request) {
        this.#request = request;
    }

    setDisplayTitle(toDisplay, text = null, toTranslate = true) {
        this.#displayTitle = {"title": toTranslate ? _.$(text) : text, "display": toDisplay}
        return this
    }

    setListener(listener) {
        this.#listener = listener
    }

    getListener() {
        return this.#listener
    }

    getDisplayTitle() {
        return this.#displayTitle;
    }

    disableCurtain() {
        this.#useCurtain = false;
        return this;
    }

    getRemoveType() {
        return this.#removeType;
    }

    setRemoveType(type) {
        this.#removeType = type;
        return this;
    }

}

class ModalModule extends Module {
    #options;
    #data
    #modal;

    static MODAL_WINDOW_CREATED = "modalWindowCreated"
    static MODAL_WINDOW_CLOSED = "modalWindowClosed"


    constructor(data, options = new ModalModuleOptions()) {
        super(data);
        this.#data = data
        this.#options = options;
    }

    createModal(removeType, className) {
        return dom.createModal(removeType, className)
    }

    build(className= null) {
        this.#modal = this.createModal(this.#options.getRemoveType(), className)
            .add(h.div('main_contant_div')
                .clIf(this.addClassToModal(), this.addClassToModal())
                .add(h.div("image_close_button").click(() => this.closeModal()))
                .add(this.createTitle())
                .add(_.ofNullable(this.createContent(), () => console.log("No content for modal window")))
                .add(this.createFooter()))
            .appendToBody()
        this.fire(ModalModule.MODAL_WINDOW_CREATED, this.#modal)
        return this.#modal
    }

    fire(eventType, data, additional) {
        if (this.#options.getListener()) {
            this.#options.getListener()({"eventType": eventType, "data": data, "additional": additional})
        }
    }

    addClassToModal() {
        return null
    }

    closeModal() {
        this.#modal.hideModal()
        this.fire(ModalModule.MODAL_WINDOW_CLOSED, this.#modal)
    }

    isHiddenWindow() {
        return this.#modal.ccl("hidden");
    }

    showModal() {
        this.#modal.showModal()
    }

    createTitle() {
        if (this.#options.getDisplayTitle().display) {
            return h.div('modal_title')._text(this.#options.getDisplayTitle().title);
        } else {
            return h.span("empty_modal_title");
        }

    }

    getOptions() {
        return _.ofNullable(this.#options, () => {
            new Error("options are not set! for modal module")
        })
    }

    createContent() {
        return null;
    }

    createFooter() {
        return h.span("footer_modal_window");
    }

    getData() {
        return this.#data;
    }

}


