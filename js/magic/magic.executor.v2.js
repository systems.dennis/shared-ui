/**
 * @deprecated all functionality will be moved to the server request on the next release
 *
 */
class Executor {
    static GET_METHOD = "GET";
    static POST_METHOD = "POST";
    static PUT_METHOD = "PUT";
    static DELETE_METHOD = "DELETE";
    static CONTENT_TYPE = "Content-Type";
    static TEXT_TYPE = "text/plain";

    static HEADERS = {
        Accept: "application/json",
        "Content-Type": "application/json;charset=UTF-8",
    };
    static HEADERS_PLAIN = {
        Accept: "text/plain",
        "Content-Type": "text/plain; charset=UTF-8",
    };

    static prepareAuth(xhr) {
        if (typeof Environment != 'undefined') {
            var token = sessionStorage.getItem("___TOKEN___") || cache.get("___TOKEN___");

            try {
                if (!xhr.headerExist("Authorization")) {

                    xhr.setRequestHeader(
                        "Authorization", "Bearer " + token
                    );
                }
                if (!xhr.headerExist("AUTH_SCOPE")) {
                    try {
                        xhr.addHeader("AUTH_SCOPE", cache.get(Authorization.AUTH_SCOPE) || Environment.authScope)
                    } catch (e) {
                        //ignored
                        log.debug("not set auth, because prototype is not included")
                    }
                }
            } catch (e){
                log.debug("not set auth, because prototype is not included")
            }
        } else {
            console.log("NO Environment class found. It's required to have auth feature. If you don't need auth feature ignore this message")
        }
    }

    static isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    static run(path, method, success, async = true, fail = null, headers = Executor.HEADERS, payload = null) {
        var xhr = new XMLHttpRequest();
        //replace double  // in the path, if any, avoiding any protocol like https:// or http://
        if (!_.isEmpty(path, 10)) {
            path = path.substring(0, 9) + path.substring(9).replaceAll("//", "/");
        }
        xhr.open(method, path, async);

        xhr.onreadystatechange = () => {
            var res = xhr.responseText;

            if (xhr.readyState === 4) {
                let resData;
                if (Executor.isJsonString(res)) {
                    resData = JSON.parse(res)
                } else {
                    resData = res ? res : null;
                }
                if (xhr.status >= 200 && xhr.status < 400) {
                    if (_e(xhr.status, 304)) {
                        dom.toast(ToastType.CHANGED, "global.not_modified")
                    }
                    success(resData);
                } else {
                    if (_.notNull(fail)) {
                        fail(resData, xhr.status);
                    } else {
                        verifyResponse(xhr)
                    }
                }
            }
        };

        Executor.setRequestHeaders(xhr, headers);
        Executor.prepareAuth(xhr);


        if (payload) {

            xhr.send(JSON.stringify(payload));
        } else {
            xhr.send();
        }

    }

    static errorFunction = function (e) {
        verifyResponse(e);
        dom.toast(_.$(e.responseText), ToastType.ERROR);
    };

    static runGet(path, success, async = true, headerds = Executor.HEADERS, fail = null) {
        Executor.run(path, Executor.GET_METHOD, success, async, fail, headerds);
    }

    static runPostWithFailFunction(path, success, failFunction, async = true, headerds = Executor.HEADERS) {
        Executor.run(path, Executor.POST_METHOD, success, async, failFunction, headerds);
    }

    static runPost(path, success, headerds = Executor.HEADERS, async = true, fail = null) {
        Executor.run(path, Executor.POST_METHOD, success, async, fail, headerds);
    }

    static runDelete(path, success, headerds = Executor.HEADERS, async = true, fail = null) {
        Executor.run(path, Executor.DELETE_METHOD, success, async, fail, headerds);
    }

    static runPostWithPayload(path, success, payload, fail = null, headers = Executor.HEADERS, async = true) {
        Executor.run(path, Executor.POST_METHOD, success, async, fail, headers, payload);
    }

    static runPutWithPayload(path, success, payload, fail = null, headerds = Executor.HEADERS, async = true) {
        Executor.run(path, Executor.PUT_METHOD, success, async, fail, headerds, payload);
    }

    static setRequestHeaders(xhr, headers) {
        if (headers) {
            for (var key in headers) {
                try {
                    xhr.addHeader(key, headers[key]);
                } catch (e){
                    xhr.setRequestHeader(key, headers[key])
                }

            }
        }
    }

    static postFile(path, success, file, fail = null, async = true, headers = null) {
        var formData = new FormData();

        if (!_.isEmpty(path, 10)) {
            path = path.substring(0, 9) + path.substring(9).replaceAll("//", "/");
        }

        formData.set('file', file);

        var xhr = new XMLHttpRequest();
        xhr.open(Executor.POST_METHOD, path, async);

        xhr.onreadystatechange = () => {
            var res = xhr.responseText;

            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var data = JSON.parse(res)
                    success(data)

                } else {
                    if (fail) {
                    }
                }
            }
        }

        Executor.setRequestHeaders(xhr, headers);
        Executor.prepareAuth(xhr);

        xhr.send(formData);
    }
}

var current_drug_event_limits;

function get(id) {
    return document.getElementById(id);
}
