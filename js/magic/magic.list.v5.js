class Selectable{

    #listOptions;
    #listener
    //never null
    #selectedIds = [];

    static ADD_SELECTED_ID = "ADD_SELECTED_ID"
    static DELETE_SELECTED_ID = "DELETE_SELECTED_ID"

    constructor(listOptions){
        this.#listOptions = listOptions;
        this.#listener = this.#listOptions.getListListener();
    }

    fire(event){
        if(this.#listener){
            this.#listener(event)
        }

    }

    resetIds(){
        this.#selectedIds = [];
    }

    getSelectedIds(){
        return this.#selectedIds;
    }

    getListener(){
        return this.#listener;
    }

    setSelectedIds(ids){
        this.#selectedIds = _.ofNullable(ids, []);
        return this
    }

    isIdSelected(id){
        return this.#selectedIds.includes(id);
    }

    addSelectedId(id){
        !this.#selectedIds.includes(id) && this.#selectedIds.push(id);
        return this
    }

    deselectId(id){
        this.#selectedIds = this.#selectedIds.filter(itemId => !_e(itemId, id));
    }

    static drawSelectComponent(id, whereToDraw, defaultVal){

        const options = new MagicTrueFalseSelectorOptions('global.remove.one', defaultVal, false, false, true, true)
        .setDisplayType(MagicTrueFalseSelectorOptions.CHECKBOX)
        .setParent()
        .setComponentListener((event)=> Selectable.trueFalseListener.bind(this)(id, event))
        .setPreventiveClick(true)
        let checkbox = new MagicTrueFalseSelector(options);
        const checkboxWrapper = h.div('header-cart-checkbox-wrapper').appendTo(whereToDraw)
        checkbox.draw(checkboxWrapper);
        return checkbox
    }

    static trueFalseListener(id, event){
        if( _e(event.type, MagicTrueFalseSelectorEvent.VALUE_CHANGED)){
            if(event.currentValue){
                this.addSelectedId(id);
                this.fire({"eventName": Selectable.ADD_SELECTED_ID, "id": id})
            }else{
                this.deselectId(id);
                this.fire({"eventName": Selectable.DELETE_SELECTED_ID, "id": id})

            }
        }
    }

    getSelectorRenderer(){
        return _.ofNullable(this.#listOptions.getSelectorRenderer(), ()=> Selectable.drawSelectComponent )
    }

}


class TableDrug{
    onDragStart;
    allowedClass;

    onDragDrop;
}

class MagicListOptions {

    static SELECT_MODE_NONE = 0;
    static SELECT_MODE_SINGLE = 1;
    static SELECT_MULTI = 2;
    static DISPLAY_AS_TABLE = "table";
    static DISPLAY_AS_GREED = "grid";

    #drag;
    #serverRequest;
    #gridOptions;
    #searchEnabled = true;
    #settingsEnabled = true;
    #orderingEnabled = true;
    #sortEnabled = true;
    #orderedFieldsFilter = MagicListOptions.removeActionsField;
    #drawPaginationTo;
    #allowedTableActions;
    #parent;
    #customTableAction;
    #customPagination = null;
    #displayType = MagicListOptions.DISPLAY_AS_TABLE;
    #attribute;
    #searchFormOptions = new SearchFormOptions();
    #contextMenuOptions = new MagicMenuOptions();
    #needMultiSelect = false;
    #selectable = false;
    #dataPathModifier;
    #listener;
    #tableListener;
    #availableFields;
    #listGroupToPath = "";
    #listListener;
    #queryModifier;
    #groupId;
    #columnRenderer;
    #isEnabled = false;
    #formOptions;
    #defaultClickFunction;
    #selectorRenderer;
    #emptyContentDiv = null;
    
    constructor( serverRequest, parent = null){
        this.#serverRequest = serverRequest;
        this.selectable = false;
        this.#parent = parent;
        try {
            this.#formOptions = new MagicFormOptions();
        } catch {
            log.promisedDebug(()=> {return "no form class"})
        }
        
    }

    setSelectorRenderer(selector){
        this.#selectorRenderer = selector;
        return this;
    }

    setEmptyContentDiv(div){
        this.#emptyContentDiv = div;
        return this;
    }

    getEmptyContentDiv(){
        return this.#emptyContentDiv;
    }

    getSelectorRenderer(){
        return this.#selectorRenderer;
        
    }

    static removeActionsField(fields) {
        return _.filter(fields, (el) => !_e(el.field, "action")) 
    }

    setFormOptions(options){
        this.#formOptions = options;
        return this;
    }

    getFormOptions(){
        return this.#formOptions;
    }

    setSearchFormOptions(options){
        this.#searchFormOptions = options;
        return this
    }

    setDefaultClickFunction(func) {
        this.#defaultClickFunction = func;
        return this;
    }

    getDefaultClickFunction(){
        return this.#defaultClickFunction;
    }

    getSearchFormOptions(){
        return this.#searchFormOptions;
    }

    setContextMenuOptions(options){
        this.#contextMenuOptions = options;
        return this
    }

    getContextMenuOptions(){
        return this.#contextMenuOptions;
    }


    setCustomPagination(func) {
        this.#customPagination = func();
        return this;
    }

    getCustomPagination() {
        return this.#customPagination;
    }

    setDrawPaginationTo(drawTo){
        this.#drawPaginationTo  = drawTo;
    }

    getDrawPaginationTo(){
        return this.#drawPaginationTo;
    }

    getGridOptions(){
        return this.#gridOptions;
    }

    setGridOptions(opts){
        if (_.notNull(opts) && ! (opts instanceof MagicGridOptions)){
            throw  Error ("options should be extended from MagicGridOptions!")
        }
        this.#gridOptions = opts;
        return this;
    }

    setDrag(dragInfo){
        if (dragInfo instanceof TableDrug){
            this.#drag = dragInfo;
            return this;
        }
        throw Error("Drag should be instance of TableDrug");
    }

    getDrag(){
        return this.#drag;
    }


    getRequest(uri){
       return _.ofNullable(this.#serverRequest, ()=> new ServerRequest().entry()).request(uri);
    }

    setDisplayType(type){
        this.#displayType = type;
        return this;
    }

    getDisplayType(){
        return this.#displayType;
    }


    getAttribute(){
        return this.#attribute
    }

    setAttribute(attribute){
        this.#attribute= attribute;
        return this;
    }

    setServerRequest(request){
        this.#serverRequest = _.ofNullable(request, ()=> new ServerRequest().entry()).copy(false, null);
        return this
    }

    static DEFAULT(type) {
        return new MagicListOptions(type)
    };


    setAvailableFields(fields) {
        this.#availableFields = fields;
    }

    setListListener(listener){
        this.#listListener = listener;
    }

    getListListener(){
        return this.#listListener;
    }

    getOrderedFieldsFilter() {
      return this.#orderedFieldsFilter;
    }

    setOrderedFieldsFilter(func) {
      this.#orderedFieldsFilter = func;
      return this;
    }

    getAvailableFields() {
        return this.#availableFields;
    }

    setCustomTableAction(val) {
      this.#customTableAction = val; return this;
    }

    getListGroupToPath(){
        return  this.#listGroupToPath;
    }

    getCustomTableAction(){
      return this.#customTableAction;
    }

    setTableAllowedActions(actions) {
        this.#allowedTableActions = actions;
    }

    getTableAllowedActions() {
        return this.#allowedTableActions;
    }

    setQueryModifier(modifier) {
        this.#queryModifier = modifier;
        return this;
    }

    setColumnRenderer(renderer) {
        this.#columnRenderer = renderer;
    }

    setMultiSelect(val) {
        this.#needMultiSelect = val;
        return this;
    }

    getNeedMultiselect() {
        return this.#needMultiSelect;
    }

    getColumnRenderer() {
        return this.#columnRenderer;
    }

    getParent() {
        return this.#parent;
    }

    getQueryModifier() {
        return this.#queryModifier;
    }

    getTableListener() {
        return this.#tableListener;
    }

    setTableListener(listener) {
        this.#tableListener = listener;
        return this;
    }

    setDataPathModifier(dataPathModifier) {
        this.#dataPathModifier = dataPathModifier;
    }

    getDataPathModifier() {
        return this.#dataPathModifier;
    }

    setSettingsEnabled(settingsEnabled) {
        this.#settingsEnabled = settingsEnabled;
        return this;
    }

    getSettingsEnabled() {
        return this.#settingsEnabled;
    }

    setOrderingEnabled(orderingEnabled) {
        this.#orderingEnabled = orderingEnabled;
        return this;
    }

    getOrderingEnabled() {
        return this.#orderingEnabled;
    }

    setSortEnabled(sortEnabled) {
        this.#sortEnabled = sortEnabled;
        return this;
    }

    getSortEnabled() {
        return this.#sortEnabled;
    }

    getSearchEnabled() {
        return this.#searchEnabled;
    }

    setSearchEnabled(enabled){
        this.#searchEnabled = enabled;
        return this;
    }

    fire(event){
        if (_.notNull(this.#listListener)) {
            return this.#listListener(event);
        }
    }


    setIsFavoriteEnabled(value){
        this.#isEnabled = value;
        return this;
    }

    getIsFavoriteEnabled() {
        return this.#isEnabled;
    }
}

class MagicList2 {
   
    #options;
    #listListener;
    #container;
    #magicTable;
    #headers;
    #fetchData;
    #grid = h.div("pg_table_container");
    #form = h.div("modal").cl("hidden").id(new Date().getTime());
    #formClass;
    #sortAndSearchForm = h.div("table_config");
    #sort = false;
    #searchForm;
    #ordering;
    #favoriteType;
    #lastData;
    #pagination;
    #searchEnabled;
    #id;

    getSelectedIds(){
        return this.#magicTable.getSelectedIds();
    }

    setSelectedIds(ids){
        this.#magicTable.setSelectedIds(ids);
    }

    getGrid() {
        return this.#grid;
    }

    getFavoriteType() {
        return this.#favoriteType;
    }

    getPagination() {
        return this.#pagination;
    }

    getRootPath() {
        return this.getOptions().getRoot();
    }

    getId() {
        return this.#id;
    }

    getLastData() {
        return this.#lastData;
    }

    getOrdering() {
        return this.#ordering;
    }

    getTableObject() {
        return this.#magicTable;
    }

    getContainer() {
        return this.#container;
    }

    getSearchEnabled() {
        return this.#searchEnabled;
    }

    setListListener(listener) {
        this.#listListener = listener;
    }

    constructor(options = null) {

        if (options == null) {
            options = MagicListOptions.DEFAULT();
        }
        this.#options = options;


        /**
         * We assume that here can only 1 parameter to be changed.
         * For this, we accept nulls as value by default
         */

    }

    getFetchData(){
      return this.#fetchData;
    }

    getForm() {
        return this.#form;
    }

    getFormClass(){
      return this.#formClass;
    }


    getSort() {
        return this.#sort;
    }

    getOptions() {
        return this.#options;
    }

    getListListener() {
        return this.#listListener;
    }

    getSearchAndSettingsBar() {
        return this.#sortAndSearchForm;
    }

    getSearchForm() {
        return this.#searchForm;
    }


    load(where) {
        let _this = this;
        this.#options.fire(new MagicListEvent(MagicListEvent.PRELOAD_LIST, this));
        this.getOptions().getRequest().listFetch((data)=> {_this.build(data, where)})
    }

    startActions(event) {

        let action = event.eventType;
        let id = event.id;
        let list = event.parent.parent().getParent();

        if (!_.isNull(list.getOptions()) && !_.isNull(list.getOptions().getContextMenuOptions().getContextMenuListener())) {
            if (list.getOptions().getContextMenuOptions().getContextMenuListener()(event)) {
                return;
            }
        }

        if (_e(action, 'edit')) {
          let request = list.getOptions().getFormOptions().getServerRequest()
          MagicForm.Instance(request.getUniqueId(), list.getOptions().getFormOptions(), list).load(id);

            return;
        }

        if ((_e(action, "copy"))) {
            let copy = true;
            let request = list.getOptions().getFormOptions().getServerRequest()
            MagicForm.Instance(request.getUniqueId(), list.getOptions().getFormOptions(), list).load(id, copy);
            return;
        }


        if (_e(action, 'delete')) {
            if (list.getTableObject().getSelectedCount() >= 2) {
                _.each(list.getTableObject().getSelectedIds(), (id) => {
                list.deleteById(list, id)
                })
            } else {
            list.deleteById(list, id)
            }
        }

        if (_e(action, 'details')) {
            showDetails(id);
        }

        if (utils.e(action, 'import')) {

            this.getOptions().getRequest().import(()=>{ progress.hide();
                dom.toast(_.$("global.messages.success"));
                list.drawValues()
            })

        }


    }

      deleteById(list, id) {
        try{
            let opts = new MagicPromptOptions("global.prompt.allow_delete_title", "global.prompt.change_group_text.allow_delete_text");
            opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                opts.closeSelf();
                list.deleteRowByContextMenu(list, id)
                opts.markNoRepeat()
            }).setIsAction(MagicPromptButton.OK_LABEL, true)
                .applyClick(MagicPromptButton.CANCEL_LABEL, () => opts.closeSelf()).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                .allowNoRepeat("global.prompt_DELETE").newPrompt();
        }catch{
            list.deleteRowByContextMenu(list, id)
            dom.message()
        }


    }

    deleteRowByContextMenu(list, id) {
        this.#options.getRequest().delete(id, ()=> {
            LocalCacheProvider.getInstance().deleteFromCache(this.#options.getRequest().copy().fetchData())
            list.drawValues()
        });
    }


    build(data, where){
        this.#lastData = data;
        let options = this.getOptions();
        if (!_.isEmpty(options.getAvailableFields())) {
            _.each(data.fields, (item) => {
                if (!options.getAvailableFields().includes(item.field)) {
                    item.visible = false;
                }
            })
        }

        this.#id = where;
        this.#container = h.from(where.isH ? where : get(where));

        this.drawListTitle(where);
        //form is used to generate edit form or add form
        this.#form.appendTo(this.#container);

        //grid is a container where <table> is generated
        this.#grid.appendTo(this.#container);
        const customPagination = this.#options.getCustomPagination();
        if (customPagination) {
            customPagination.setParent(this).drawPagination();
        } else {
            this.drawPagination()
        }

        //when we are ready we can draw values from the server

        this.drawValues()

        return this;

    }

    drawListTitle() {
        let data = this.#lastData;
        this.#favoriteType = data.objectType;
        if (data.showTitle) {
            h.div("title").text(data.tableTitle).appendTo(this.#container);
        }
        this.#headers = data; //todo check if we still need to hold it here
        this.#sortAndSearchForm.appendTo(this.#container);
        this.drawSettingsForm();
        if (this.#options.getSearchEnabled()) {
            this.drawSearch(data);
        }
        this.drawTableActions(data)
        this.createDataHeaders(data['fields']);
    }


    createDataHeaders() {
        if (_e(this.getOptions().getDisplayType(), MagicListOptions.DISPLAY_AS_TABLE)){
            this.#magicTable = new MagicTable(this).drawHeader(this.#grid);
        } else {
            this.#magicTable = new MagicGrid(this, this.getOptions().getGridOptions()).drawHeader(this.#grid);
        }
    }

    drawSettingsForm() {
        if (this.getOptions().getSortEnabled()) {
            this.#sort = new MagicSort(this);
            this.#sort.build();
        } 

        if (this.getOptions().getOrderingEnabled()) {
            this.#ordering = new MagicOrdering(this);
            this.#ordering.build();
        }  
    }

    drawTableActions(data) {

        //List actions are coming from server, if they are existing we need to draw them
        //Default action are: settings : open list settings
        //                    download : download the content of the table
        //                    new      : creates a new element
        // if action differs then it is expected that
        //                   drawCustomAction is defined by user
        // this method should be loaded on the page itself
        if (utils.isEmpty(data.listActions)) {
            return;
        }

        let div = h.div("actions");

        for (let i = 0; i < data.listActions.length; i++) {
            try {
                div.add(this.drawTableAction(data.listActions[i], div, data))
            } catch (ex) {
                log.error(ex);
            }
        }

        this.getSearchAndSettingsBar().add(div);


    }

    drawTableAction(action, div, data) {

        if (_.notNull(this.getOptions().getTableAllowedActions())

            && !this.getOptions().getTableAllowedActions().includes(action)) {
            return;
        }

        try {
          return this.getOptions().getCustomTableAction()(action, div, data)
        } catch (error) {
        }

        let _this = this;
        if (action == 'settings') {
            return h.tag("span").cl("settings")
                .add(h.divImg("sort_icon.png.png").cl(action).wh(16).click(function () {
                    _this.getSort().show();
                    _this.getOrdering().close();
                }))
                .add(h.divImg("settings.png").cl(action).wh(16).click(function () {
                    _this.getOrdering().show();
                    _this.getSort().close();
                })).get();

        }


        if (action == 'download') {


            let _this = this;

            return h.divImg("download.png").cl(action).wh(16).click(function () {

                _this.getOptions().getRequest().download(_this.createPayload(), (data) => {
                    h.a(_this.getRootPath() + "/root/download/data/" + data.pathToDownload, "", false).get().click();
                })


            }).get();
        }


        if (action == 'new') {
            let __ = this;

            this.#grid.add(div);
            return h.input("button").cl(action).cl("insert_new_btn").text("pages.forms.insert_new").click(this.loadForm).get();
        }

        if (action == 'import') {
            return h.divImg("import.png").wh("16").cl(action).click(function (e) {
                if (e.getAttribute("disabled") != undefined) {
                    dom.message("global.messages.import.in_process", true, ToastType.NODATA)
                }

                let progress = new Loading(_this.getGrid().get());
                progress.show(e);

                _this.getOptions().getRequest().import(()=>{
                    progress.hide();
                    dom.toast(_.$("global.messages.success"), ToastType.SUCCESS)
                    _this.drawValues();
                })
            }).get();


        }

        try {
            return drawCustomAction(action, div, null, this, data)
        } catch (ex) {
            log.error(ex)
            return h.span("info.error.list.actions", _.$("global.list.actions.not.implemented." + action));
        }

    }

    loadForm = () => {
      let request = this.getOptions().getFormOptions().getServerRequest()
      this.#formClass = MagicForm.Instance(request.getUniqueId() + this.#options.getAttribute(),this.getOptions().getFormOptions(), this).load();
    }

    drawValues() {

        let _this = this;
        this.getOptions().getRequest().search(this.createPayload(), (data)=>{  _this.buildValues(data);})

    }

    buildValues(data) {
        this.#fetchData = data;
        this.#magicTable.rebuildHeaders();
        this.#magicTable.drawValues(data, this);
        const customPagination = this.#options.getCustomPagination();
        if (customPagination) {
            customPagination.rebuildPages(data);
        } else {
            this.getPagination().rebuildPages(data);
        }
        this.#options.fire(new MagicListEvent(MagicListEvent.BUILD_VALUES, this, data ));
    }

    drawPagination() {
        this.#pagination = new MagicPagination(this.#magicTable, this);
        this.#options.fire(new MagicListEvent(MagicListEvent.PREBUILD_PAGINATION, this, this.#pagination ));
        this.#pagination.build(this.getId())
    }

    drawSearch(data) {
        if (this.getOptions().getSearchEnabled()) {
            this.#searchForm = new SearchForm(data, this, this.#options.getSearchFormOptions());
            this.#searchForm.build();
        }
    }


    startActions(event, incomingList = null) {

        let action = event.eventType;
        let id = event.id;
        let list =  incomingList || event.parent.parent().getParent();
        
        if (!_.isNull(list.getOptions()) && !_.isNull(list.getOptions().getContextMenuOptions().getContextMenuListener())) {
            if (list.getOptions().getContextMenuOptions().getContextMenuListener()(event)) {
                return;
            }
        }

        if (_e(action, 'edit')) {
              let request = list.getOptions().getFormOptions().getServerRequest()
              MagicForm.Instance(request.getUniqueId(), list.getOptions().getFormOptions(), list).load(id);
            return;
        }

        if ((_e(action, "copy"))) {
            let copy = true;
            let request = list.getOptions().getFormOptions().getServerRequest()
            MagicForm.Instance(request.getUniqueId(), list.getOptions().getFormOptions(), list).load(id, copy);
            return;
        }


        if (_e(action, 'delete')) {

            if (list.getTableObject().getSelectedCount() >= 2) {
                _.each(list.getTableObject().getSelectedIds(), (id) => {
                  list.deleteById(list, id)
                })
            } else {
              list.deleteById(list, id)
            }
        } 

        if (_e(action, 'details')) {
            try {
                showDetails(id);
            } catch (e){
                log.error( e )
            }
        }

        if (utils.e(action, 'import')) {
            this.getOptions().getRequest().import(()=> {
                dom.toast(_.$("global.messages.success"));
                list.drawValues()})
        }
    }

    //todo options get server request -> remove

    createPayload(withoutCustomModifier = false) {
        if (_.isNull(this.#pagination) && _.isNull(this.#options.getCustomPagination())) {
            return new MagicRequest();
        }

        let req = new MagicRequest();
        let page = 0;
        const customPagination = this.#options.getCustomPagination();
        if (customPagination) {
            page = customPagination.getPageNumber();
            req.limit = customPagination.getPerPage();
        } else {
            page = this.#pagination.getPageNumber();
            req.limit = this.#pagination.getPerPage();
        }


        req.page = page;

        if (this.getOptions().getSearchEnabled()) {
            req.query = this.#searchForm.getSearchCriteria();
        }

        if (this.getSort()) {
            req.sort = this.getSort().getSort();
        }
        


        if (!withoutCustomModifier && this.getOptions().getQueryModifier() != undefined) {
            let func = this.getOptions().getQueryModifier();
            func(req, this);
        }

        return req;
    }

}

