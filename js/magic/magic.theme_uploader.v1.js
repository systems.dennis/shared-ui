class ThemeUploader {
    static SELECTED_THEME = "selected_theme";
    static DARK_THEME = ThemeUploader.getThemePath() +  "dark/";
    static WHITE_THEME = ThemeUploader.getThemePath() + "white/";


    static setCurrentTheme() {
        if(Environment.isNotRegisterLoginOrReset()) {
            ThemeUploader.getCurrentTheme((theme) => {
                ThemeUploader.uploadStyles(theme);
            }, () => {
                ThemeUploader.uploadStyles();
            });
        } else {
            ThemeUploader.uploadStyles();
        }
    }

    static uploadStyles(theme = null){
        if (theme && Environment.isNotRegisterLoginOrReset()) {
            Environment.defaultTemplate = theme;
        }
        Environment.styles.forEach((file, i) => {
            var isLast = false;
            if(Environment.styles.length - 1 == i) isLast = true;
            EnvironmentUploader.includeCSS(file, true, isLast);
        });
    }

    static getCurrentTheme(callback, error) {
        var xhr = new XMLHttpRequest();
        var path =
            Environment.dataApi +
            "/api/v2/shared/personal_settings/by_name?name=" +
            ThemeUploader.SELECTED_THEME;
        xhr.open("GET", path, false);

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status >= 200 && xhr.status < 400) {
                    var res = JSON.parse(xhr.responseText);
                    callback(res.value);
                } else {
                    error()
                }
            }
        };
        xhr.setRequestHeader(
            "Authorization",
            "Bearer " + sessionStorage.getItem("___TOKEN___")
        );
        xhr.setRequestHeader("AUTH_SCOPE", Environment.authScope);
        xhr.setRequestHeader("Accept", "application/json");
        xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");

        xhr.send();
    }

    static changeTheme(theme) {
        Environment.defaultTemplate = theme;
        window.location.reload()
    }

    static getThemePath(){
        return Environment.themePath ? Environment.themePath : "/themes/"
    }
}