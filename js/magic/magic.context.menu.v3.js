class MagicMenuOptions {
    #eventTargetElement;
    #id;
    #parent;
    #actions = {};
    #contextMenuToGroupTransformer = null;
    #contextMenuListener;
    #isContextMenuAvailable = true
    #event = "contextmenu"
    #translatePath = null
    #allowedClassName = null

    constructor(eventTargetElement, info, parent) {
        this.#parent = parent;
        this.#eventTargetElement = eventTargetElement;
        if (info && info.id) {
            this.#id = info.id;
        }
    }

    setAllowedClassName(className) {
        this.#allowedClassName = className
        return this
    }

    getAllowedClassName() {
        return this.#allowedClassName
    }

    setContextMenuToGroupTransformer(func) {
        this.#contextMenuToGroupTransformer = func;
        return this
    }

    getContextMenuToGroupTransformer() {
        return this.#contextMenuToGroupTransformer;
    }

    setTranslatePath(translatePAth) {
        this.#translatePath = translatePAth
        return this
    }

    getTranslatePath() {
        return this.#translatePath
    }

    getEvent() {
        return this.#event;
    }

    setEvent(event) {
        this.#event = event;
        return this
    }

    addActions(actions) {
        _.each(actions, (action) => {
            this.#actions[action.name] = action
        })

        return this;
    }

    /**
    * @deprecated since version 3, Use setContextMenuListener instead
    */
    setListner(listener) {
        this.#contextMenuListener = listener;
        return this;
    }

    /**
     * @deprecated since version 3, Use getContextMenuListener instead
     */
    getListener() {
        return this.#contextMenuListener;
    }

    setContextMenuListener(listener) {
        this.#contextMenuListener = listener;

        return this;
    }

    getContextMenuListener() {
        return this.#contextMenuListener;
    }

    getActions() {
        return this.#actions;
    }

    setActions(val) {
        this.#actions = val;

        return this;
    }

    fire(eventType, item, id, data, element) {

        if (_.notNull(this.#contextMenuListener)) this.#contextMenuListener(new MagicMenuEvent(eventType, item, id, this.#parent, data, element));
    }

    getId() {
        return this.#id;
    }

    setId(value) {
        if (value && value.id) {
            this.#id = value.id;
        }

        return this
    }

    getEventTargetElement() {
        return this.#eventTargetElement;
    }

    setEventTargetElement(eventTargetElement) {
        this.#eventTargetElement = eventTargetElement
        return this
    }

    getParent() {
        return this.#parent;
    }

    setParent(parent) {
        this.#parent = parent
        return this
    }

    setContextMenuAvailable(value) {
        this.#isContextMenuAvailable = value;

        return this
    }

    getIsContextMenuAvailable() {
        return this.#isContextMenuAvailable;
    }
}

class MagicMenu {

    static TRANSLATE_PATH = "global.menu.actions.";
    static CLASS_NAME = "context-menu__";

    #menu;
    #wrapper;
    #options;
    #data;
    #closeMenuListener

    constructor(options = new MagicMenuOptions(), data = null) {
        this.#options = options;
        this.#data = data;

        if (this.#options.getIsContextMenuAvailable()) this.build();

    }

    build() {
        const element = this.#options.getEventTargetElement();
        const allowedClass = this.#options.getAllowedClassName()
        if(allowedClass){
            if(element.ccl(allowedClass)) {
                this.addContextListener(element)
            }
        } else {
            this.addContextListener(element)
        }
    }

    addContextListener(element) {
        element.handleEvent(this.#options.getEvent(), (event) => {
            const menu = h.fromId(MagicMenu.CLASS_NAME + 'menu')
            if(menu) menu.remove()
            event.preventDefault();
            event.stopPropagation()
            this.buildMenu(event);
        })
    }

    getOptions() {
        return this.#options;
    }

    getActions() {
        return this.#options.getActions()
    }

    keyupListener() {
        document.onkeyup = (e) => {
            if (e.key === 'Escape') {
                this.removeMenu();
            }
        }
    }

    rebuildActions(actions) {
        this.#options.setActions(actions)
    }

    removeActionByName(actionName) {
        try {
            if (_e(this.getActions()[actionName].name, actionName)) {
                delete this.getActions()[actionName]
            }
        } catch (e) {

        }
    }

    addCloseMenuListeners() {
        this.#closeMenuListener = (event) => {
            this.removeMenu()
        }

        document.addEventListener('click', this.#closeMenuListener)
    }

    removeMenu() {
        this.#wrapper.remove();
        document.removeEventListener('click', this.#closeMenuListener)
    }

    buildMenu(event) {

         this.#wrapper = h.div().id(MagicMenu.CLASS_NAME + 'menu').appendToBody();
         this.#menu = h.div(MagicMenu.CLASS_NAME + 'menu').appendTo(this.#wrapper)
         this.subWrapper = h.div(MagicMenu.CLASS_NAME + 'menu').appendTo(this.#wrapper)

        _.each(Object.keys(this.getActions()), (actionName) => {
            const current = this.getActions()[actionName];

            this.drawItem(current)
        })

        this.setPosition(event)

        this.keyupListener();
        this.addCloseMenuListeners()
    }

    drawItem = (item, where = this.#menu) => {
        const itemWrapper = h.div(MagicMenu.CLASS_NAME + 'item-wrapper').setData('action', item.name).setData('allow_on_multi_rows').appendTo(where);

        itemWrapper.add(h.div(MagicMenu.CLASS_NAME + item.name + '-icon'));

        let actionName = h.div(MagicMenu.CLASS_NAME + 'item').id("menu_item_" + item.name);

        let eventData = {name: item.name};
        this.#options.fire(MagicMenuEvent.PRE_CREATED_ITEM, item, this.#data, eventData, this);


        if (this.#options.getTranslatePath()) {
            itemWrapper.add(actionName.text(this.#options.getTranslatePath() + eventData.name, false));
        } else {
            itemWrapper.add(actionName.text(MagicMenu.TRANSLATE_PATH + eventData.name));
        }


        itemWrapper.click((el, event) => {

            el = h.from(el);
            this.#options.fire(el.getData('action'), item, this.#options.getId(), this.#data, this);

            this.removeMenu();
        })

        this.#options.fire(MagicMenuEvent.CONTEXT_ITEM_CREATED, this.#data, itemWrapper, itemWrapper.getData('action'), this);

    }

    setPosition(e) {
        let posx = 0;
        let posy = 0;

        if (!e) e = window.event;

        if (e.pageX || e.pageY) {
            posx = e.pageX;
            posy = e.pageY;
        } else if (e.clientX || e.clientY) {
            posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
        }

        const menuHeight = this.#menu.get().offsetHeight;
        const menuWidth = this.#menu.get().offsetWidth;
        const viewportHeight = window.innerHeight;
        const viewportWidth = window.innerWidth;

        const spaceBelow = viewportHeight - posy;
        const spaceOnRight = viewportWidth - posx;

        posy = (spaceBelow > menuHeight) ? posy : posy - menuHeight;
        posx = (spaceOnRight > menuWidth) ? posx : posx - menuWidth

         this.#menu.style().left(posx)
         this.#menu.style().top(posy);

         this.#wrapper.show()

        return this;
    }

}

