String.prototype.addLeftSpace = function () {
    return " " + this
}

String.prototype.addRightSpace = function () {
    return this + " "
}

String.prototype.asRootUrl = function () {
    return Environment.dataApi + this;
}

String.prototype.asMediaUrl = function () {
    return Environment.self + Environment.defaultTemplate + this + "?v=" + Environment.version
}

String.prototype.toLocalUrl = function () {
    return Environment.self + this;
}


String.prototype.append = function (what) {
    return this + what
}

String.prototype.prepend = function (what) {
    return what + this
}

String.prototype.cleanUrl = function () {
    try {
        let url = this.substring(10, this.length);
        url = url.replace("//", "/");
        return this.substring(0, 10) + url;
    } catch (ex) {
        return this;
    }
}

Array.prototype.contains = function (el){
    if (_.isFunction(el._equals)){
        let res = _.find(this, (item)=> el._equals(item));
        return _.notNull(res);
    }
    return  this.indexOf(el) > -1;
}

Array.prototype.indexOfItem = function (itemRoCompare){
    _.each(this, (item, count)=> {
        if (_e(item, itemRoCompare)){
            return count
        }
    })
}

Array.prototype.removeItem = function (item){
    let index =  this.indexOfItem(item);

    if (index > -1) {
        return this.remove(index);
    } else {
        log.promisedDebug(()=> {return  {"text": "item_not_found_to_remove", "item" : item}})
        return  false;
    }
}

Array.prototype.indexOfItem = function (itemRoCompare){
    let res = _.each(this, (item, count)=> {
        if (_e(item, itemRoCompare)){
            return count
        }
    })
    return _.ofNullable(res, -1)
}

/**
 * Adds item only if it was not previously added to array
 * like Set in Java
 * @param item to be added
 * @returns {boolean} true if item was added, false otherwise
 */
Array.prototype.addIfAbsent= function (item){
    if (!this.contains(item)){
        this.push(item);
        return true;
    }

    return  false
}

Array.prototype.unshiftIfAbsent= function (item){
    if (!this.contains(item)){
        this.unshift(item);
        return true;
    }

    return  false
}

Array.prototype.remove = function (index){
    try {
        if (index > -1) {
            this.splice(index, 1);
            return true;
        } else {
            log.promisedDebug(() => {
                return {"text": "index_is_less_then_existing", "index": index}
            })
            return  false;
        }
    } catch (e){
        return  false;
    }
}

String.prototype.toDate = function (format) {
    var normalized = this.replace(/[^a-zA-Z0-9]/g, '-');
    var normalizedFormat = format.toLowerCase().replace(/[^a-zA-Z0-9]/g, '-');
    var formatItems = normalizedFormat.split('-');
    var dateItems = normalized.split('-');

    var monthIndex = formatItems.indexOf("mm");
    var dayIndex = formatItems.indexOf("dd");
    var yearIndex = formatItems.indexOf("yyyy");
    var hourIndex = formatItems.indexOf("hh");
    var minutesIndex = formatItems.indexOf("ii");
    var secondsIndex = formatItems.indexOf("ss");

    var today = new Date();

    var year = yearIndex > -1 ? dateItems[yearIndex] : today.getFullYear();
    var month = monthIndex > -1 ? dateItems[monthIndex] - 1 : today.getMonth() - 1;
    var day = dayIndex > -1 ? dateItems[dayIndex] : today.getDate();

    var hour = hourIndex > -1 ? dateItems[hourIndex] : today.getHours();
    var minute = minutesIndex > -1 ? dateItems[minutesIndex] : today.getMinutes();
    var second = secondsIndex > -1 ? dateItems[secondsIndex] : today.getSeconds();

    return new Date(year, month, day, hour, minute, second);
};


Date.prototype.daysInMonth = function (month) {
    return 33 - new Date(this.getFullYear(), month, 33).getDate();
};

Number.prototype.toDateString = function () {
    return new Date(this).toLocaleDateString();
}

Number.prototype.asDateDifference = function (el){

    let delta = Math.abs(this - el) / 1000;

    let days = Math.floor(delta / 86400);
    delta -= days * 86400;


    let hours = Math.floor(delta / 3600) % 24;
    delta -= hours * 3600;


    let minutes = Math.floor(delta / 60) % 60;
    delta -= minutes * 60;


    let seconds = delta % 60;

    return {"days" : days, "hours" : hours, "minutes" : minutes, "seconds" : seconds, "milliseconds" : delta}
}
String.prototype.firstLetter = function (){
    return Array.from(this)[0];
}
/**
 *
 * @param fractionDigits number of symbols after separator
 * @param leftBounds - number of grouped symbols for example 3 for thousands, 4 for card number
 * @param leftSeparator - a separator to use left side, normally a space, but can be also a commma for thousands
 * @param separator - a separator between right and left parts
 * @returns {string} a result of the parsing
 */
Number.prototype.asDecimal = function (fractionDigits = 2, leftBounds = 3, leftSeparator = " ", separator = ".") {
    let num = this.toFixed(fractionDigits).toString();

    let parts = num.split(".");
    let second = parts[1];
    let first = parts[0];

    let leftRes = "";
    for (let i = first.length; i >= leftBounds; i = i - leftBounds) {
        leftRes = leftSeparator + first.substring(i - leftBounds, i) + leftRes;
    }

    var moreSymbols;
    if (first.length < leftBounds) {
        moreSymbols = first.length;
    } else {
        moreSymbols = first.length % leftBounds;
    }

    leftRes = first.substring(0, moreSymbols) + leftRes;

    if (_e(fractionDigits, 0)) {
        return leftRes;
    }

    return leftRes.trim() + separator + second.trim();

}

Function.bind = function (to) {
    this.bind(to);
    return this;

}

Number.prototype.toUserSettingsDateString = function () {
    var mySimpleDateFormatter = new simpleDateFormat(Environment.dateFormat);
    var myDate = new Date(this);
    return mySimpleDateFormatter.format(myDate);
}

Number.prototype.toTimeString = function () {
    return new Date(this).toLocaleTimeString();
}

Number.prototype.compare = function (num) {

    if (this > num) {
        return 1
    }
    if (this < num) {
        return -1
    }
    if (_e(this, num)) {
        return 0
    }
}

XMLHttpRequest.prototype.addHeader = function (key, value) {
    this.setRequestHeader(key, value);
    this.getHeaders().push(key);
}

XMLHttpRequest.prototype.headerExist = function (key) {
    return this.getHeaders().includes(key)
}

XMLHttpRequest.prototype.getHeaders = function () {
    if (Array.isArray(this.headers)) {
        return this.headers;
    }
    this.headers = [];
    return this.headers;
}

Object.prototype.instanceOf = function (type, def = null) {
    if(this instanceof type) {
        return this
    }
    if(def == null){
        throw new Error("Object not type of " + type)
    }
    return def
}

Object.defineProperty(Object.prototype, 'instanceOf', {
    enumerable: false
});

