class MagicTab {


    static SELECTED_TAB_CLASS = "___selected_tab";
    static FIRST_ELEMENT = 0;
    static UNSELECTED_TAB_CONTENT = "hidden";
    static TAB_PANEL_CLASS = "tabs_panel"

    #components = [];
    #emptyToEnd;

    constructor(emptyToEnd = true) {
        this.#emptyToEnd = emptyToEnd
    }

    addLazyTab(tabName, tabBuilder, parent) {
        let tab = {}
        tab.name = tabName;
        tab.builder = tabBuilder;
        tab.component = undefined;
        tab.parent = parent;

        if (this.#components.length == 0) {
            tab.component = undefined
            tab.builder = tabBuilder;
        }

        this.#components.push(tab)

        return this;
    }

    addTab(tabName, component) {
        let tab = {}
        tab.name = tabName;
        tab.builder = undefined;
        tab.component = component;
        this.#components.push(tab);
        return this;
    }


    deployTo(where) {
        where = h.from(where);

        let tabPane = h.div(MagicTab.TAB_PANEL_CLASS);
        let _this = this;
        if (this.#components.length > 1) {
            let tabPaneHeader = h.div("tab_panel_header").appendTo(tabPane);
            let tabContentPanel = h.div(MagicTab.TAB_PANEL_CLASS + '-content').appendTo(tabPane);

            utils.each(this.#components, (cp, index) => {
                h.div("tab_pane_title").appendTo(tabPaneHeader).pointer().clIf(_.e(index , 0), MagicTab.SELECTED_TAB_CLASS).text(cp.name).click(function (e) {
                    _this.changeTab(cp, tabContentPanel);
                    tabPaneHeader.eachOf((x) => {
                        h.from(x).rcl(MagicTab.SELECTED_TAB_CLASS)
                    });
                    h.from(e).cl(MagicTab.SELECTED_TAB_CLASS);
                });
            })

            utils.each(this.#components, (x) => {
                if (utils.isNull(x.component)) {
                    return utils.CONTINUE;
                }
                tabContentPanel.add(h.from(x.component).hide())
            })
            tabPane.appendTo(where);

            this.changeTab(this.#components.find(x => x.name), tabContentPanel)
        } else {
            if (utils.e(this.#components.length, 1)) {
                if (utils.isNull(this.#components[MagicTab.FIRST_ELEMENT].component)) {
                    this.#components[MagicTab.FIRST_ELEMENT].component = this.#components[MagicTab.FIRST_ELEMENT].builder();
                }
                where.add(this.#components[MagicTab.FIRST_ELEMENT].component);

            } else {
                log.debug("tab without content, skip it!")
            }
        }

    }


    changeTab(component, tabContent) {

        //initial build of the lazy tab
        if (utils.notNull(component.builder)) {
            component.component = h.from(component.builder(component.parent));
            component.builder = undefined;
            tabContent.add(component.component);

        }

        utils.each(this.#components,
            (tabContent) => {
                if (utils.notNull(tabContent.component)) {
                    tabContent.component.cl(MagicTab.UNSELECTED_TAB_CONTENT);
                }
            })


        component.component.rcl(MagicTab.UNSELECTED_TAB_CONTENT)

    }

}
