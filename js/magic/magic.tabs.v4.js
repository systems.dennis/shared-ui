class MagicTabOptions {
    static DEFAULT_TAB_PANE_NAME = "simple_tap";

    static TAB_PANE_PERSONAL_SETTING_PREFIX = 'global.application.selected_pane_'

    static TAB_PIN_DIV_CLASS = 'pin_image'
    static TAB_PINED_DIV_CLASS = 'pinned_image'
    #tabTitleGenerator;
    #parent;
    #tabDefaultSelected;
    #tabListener;
    #counterFunction;
    #tabTranslater = false;
    #pinable = false;
    #customModalRenderer = null

    #name = MagicTabOptions.DEFAULT_TAB_PANE_NAME;

    constructor(tabTitleGenerator = MagicTabOptions.tabTitleGenerator,
                tabDefaultSelected = null, tabListener = null) {
        this.#tabListener = tabListener;
        this.#tabTitleGenerator = tabTitleGenerator;
        this.#tabDefaultSelected = tabListener;
    }

    setPinable(pinable, name) {
        this.#pinable = pinable;
        this.#name = name;
        return this;
    }

    setParent(parent){
        this.#parent = parent
    }

    getParent(){
        if(this.#parent){
            return this.#parent
        }
    }

    getChangeTabModal(successCallback){
        if(this.#customModalRenderer) {
            return this.#customModalRenderer(successCallback)
        }

        const modal = dom.createModal(dom.REMOVE_TYPE_REMOVE).hide()
        const wrapper = h.div('main_contant_div')
        modal.add(wrapper)
        wrapper
            .add(h.div('modal_title').text('global.template.change_confirm'))
            .add(h.div('modal_controls')
                .add(h.input('button').cl('modal_cancel').text('global.template.cancel').click(e => {
                    modal.hide()
                }))
                .add(h.input('button').cl('modal_confirm').text('global.template.confirm').click(e => {
                    successCallback()
                    modal.hide()
                }))
            )
        modal.appendToBody()

        return modal
    }

    setCustomModalRenderer(modalRenderer){
        this.#customModalRenderer = modalRenderer
        return this
    }

    setTabTranslater(tabTranslater){
        this.#tabTranslater = tabTranslater;
    }

    getTabTranslater(){
        return this.#tabTranslater;
    }


    hasCounter() {
        return _.notNull(this.#counterFunction);
    }

    isPinable() {
        return this.#pinable;
    }

    setCounterFunction(counterFunction) {
        this.#counterFunction = counterFunction;
        return this;
    }

    getName() {
        return this.#name;
    }

    getCounterFunction() {
        return this.#counterFunction;
    }

    //we send here selected tab to avoid multiple requests to the server getting the same setting
    static tabTitleGenerator(tab, index, tabPane, selectedTab, translater) {
        let base = h.div("tab_pane_title").id(tab.getName()).pointer().clIf(_.e(index, 0), MagicTab.SELECTED_TAB_CLASS);

        let options = tabPane.getOptions();

        if (options.hasCounter()) {
            let count = h.span("tab_pane_title_counter");
            options.getCounterFunction()(tab, count, index, options)

            if(_.isTrue(translater)){
                base.add(h.span("tab_pane_title_text", tab.getName(), translater[tab.getName()])).add(count)
            }else{
                base.add(h.span("tab_pane_title_text", tab.getName(), true)).add(count)
            }
        } else {
            if(_.isTrue(translater)){
                base.text(tab.getName(), translater[tab.getName()])
            }else{
                base.text(tab.getName())
            }
        }


        if (_.notNull(tab.image)) {
            h.div("title_tab_image").add(h.divImg(tab.image).wh(16)).prependTo(base)
        }

        if (options.isPinable()) {
            let booleanPinned = false;
            if (_.isNull(selectedTab)) {
                booleanPinned = _e(index, 0);
            } else {
                booleanPinned = _e(tab.name, selectedTab);
            }


            h.div(MagicTabOptions.TAB_PIN_DIV_CLASS).wh(16).clIf(booleanPinned, MagicTabOptions.TAB_PINED_DIV_CLASS).clickPreventive((e) => {
                tabPane.pinUnpin(tab)
            }).prependTo(base)
        }

        return base;
    }


    getListener() {
        return this.#tabListener;
    }

    setTablistener(tabListener){
        this.#tabListener = tabListener;

        return this;
    }

    getTabTitleGenerator() {
        return this.#tabTitleGenerator;
    }

}

class MagicTab {


    static SELECTED_TAB_CLASS = "___selected_tab";
    static FIRST_ELEMENT = 0;
    static UNSELECTED_TAB_CONTENT = "hidden";
    static TAB_PANEL_CLASS = "tabs_panel"

    #headers = [];
    #components = [];
    #content;
    #options;
    #selectedComponent = null
    #requireEditedCheck = false
    #changeTabModal
    #tabContentPanel
    #tabPanelHeader
    #tabToChange = null
    #elementToChange = null

    constructor(options = new MagicTabOptions()) {
        this.#options = options;
        this.#changeTabModal = this.#options.getChangeTabModal(this.onTabClickHandler.bind(this))
    }


    hideFromSelection(name){
        let headerToDisable = _.find(this.#headers, (item)=>{return  _e(item.name, name)})
        headerToDisable?.headerEl.hide()
    }

    setCustomChangeTabModal(modal){
        this.#changeTabModal = modal
        return this
    }

    getContent(){
        return this.#content;
    }

    getComponents(){
        return this.#components
    }

    getHeaders(){
        return this.#headers
    }

    getOptions() {
        return this.#options;
    }

    saveSelectedIndex(tabName) {
        try {
            PersonalSettings.setSetting(MagicTabOptions.TAB_PANE_PERSONAL_SETTING_PREFIX + this.#options.getName(), tabName)
        } catch (e) {
            dom.message('global.fail', true, ToastType.ERROR)
        }
    }

    pinUnpin(tab) {
        if (_.isFalse(this.getOptions().isPinable())) {
            return;
        }

        this.saveSelectedIndex(tab.getName());

        _.each(this.#headers, (header) => {
            let component = header.headerEl.eachOfClass(MagicTabOptions.TAB_PIN_DIV_CLASS, (el) => {
                return h.from(el);
            });
            _e(header.name, tab.getName())
             ?component.cl(MagicTabOptions.TAB_PINED_DIV_CLASS)
             :component.rcl(MagicTabOptions.TAB_PINED_DIV_CLASS);
        })
    }

    fireEvent(tab, eventName, additional, parent) {
        if (_.notNull(this.#options.getListener())) {
            let event = new MagicTabEvent();
            event.tab = tab;
            event.eventType = eventName;
            event.additional = additional;
            event.parent = parent;
            this.#options.getListener()(event)
        }
    }


    addLazyTab(tabName, tabBuilder, parent, image) {
        let tab = new MagicTabItem(tabName, tabBuilder, undefined, image, parent);

        if (_e(this.#components.length, 0)) {
            tab.setComponent(undefined)
            tab.setBuilder(tabBuilder)
        }
        this.#components.push(tab)
        this.fireEvent(tab, TableEvent.EVENT_TAB_ADDED, {'type': 'lazy', 'parent': parent}, this.#options.getParent())
        return tab;
    }

    addTab(tabName, component, image) {
        let tab = new MagicTabItem(tabName, undefined, component, image, undefined);
        this.#components.push(tab);
        this.fireEvent(tab, TableEvent.EVENT_TAB_ADDED, {'type': 'normal'}, this.#options.getParent())
        return tab;
    }

    addTabAfterDeploy(tabName, component, image){
        let tab = new MagicTabItem(tabName, undefined, component, image, undefined);
        this.#components.push(tab);
        this.generateComponent(tab, 1);
        return tab;
    }

    createElement(component, index){
        return this.#options.getTabTitleGenerator()(component, index, this, this.getSelectedName(), this.#options.getTabTranslater()).appendTo(this.#tabPanelHeader).click(e => {
            this.#tabToChange = component;
            this.#elementToChange = e;
            this.onTabClickHandler(false);
        });
    }

    generateComponent(component, index){
        let element = this.createElement(component, index)
        this.#headers.push({'name': component.getName(), 'headerEl': element})
        _.ofNullable(!component.getComponent(), ()=> this.#tabContentPanel.add(h.from(component.getComponent()).hide()))
        this.fireEvent(component, MagicTabEvent.EVENT_TAB_TITLE_GENERATED, element, this.#options.getParent())
    }


    deployTo(where) {
        this.where = h.from(where);

        let tabPane = h.div(MagicTab.TAB_PANEL_CLASS);
        this.#content = tabPane;

        if (this.#components.length > 1) {
            this.#tabPanelHeader = h.div("tab_panel_header").appendTo(tabPane);
            this.#tabContentPanel = h.div(MagicTab.TAB_PANEL_CLASS + '-content').appendTo(tabPane);
            // let selected = this.getSelectedName();
            _.each(this.#components, (component, index) => {
                this.generateComponent(component, index)
            })
            tabPane.appendTo(this.where);
            this.selectTabByName(this.getSelectedName())
        } else {
            //if there are no components or only one we should not pin it!
            this.getOptions().setPinable(false, MagicTabOptions.DEFAULT_TAB_PANE_NAME)
            if (_.e(this.#components.length, 1)) {
                let firstElement = this.#components[MagicTab.FIRST_ELEMENT]
                _.ofNullable(firstElement.getComponent(), ()=> firstElement.setComponent(this.#components[MagicTab.FIRST_ELEMENT].getBuilder()))
                this.where.add(this.#components[MagicTab.FIRST_ELEMENT].getComponent());
            } else {
                log.debug("tab without content, skip it!")
            }
        }
        this.#requireEditedCheck = true
    }

    onTabClickHandler(isTriggeredByModal = true) {
        if(this.#requireEditedCheck && this.requireConfirmToChange() && !isTriggeredByModal){
            this.#changeTabModal.show();
            return
        }
        this.changeTab(this.#tabToChange, this.#tabContentPanel);
        this.#tabPanelHeader.eachOf((x) => {
            h.from(x).rcl(MagicTab.SELECTED_TAB_CLASS)
        });
        h.from(this.#elementToChange).cl(MagicTab.SELECTED_TAB_CLASS);
    }


    changeTab(component, tabContent) {
        if (!component) return
        //initial build of the lazy tab
        if (_.notNull(component.getBuilder())) {
            component.setComponent(h.from(component.getBuilder()(component.parent)))
            component.setBuilder(undefined)
            tabContent.add(component.getComponent());
            this.fireEvent(component, TableEvent.EVENT_TAB_CONTENT_GENERATED, component.component, this.#options.getParent())
        }
        this.#selectedComponent = component;
        _.each(this.#components,
            (tabContent) => {
                _.notNull(tabContent.getComponent()) && tabContent.getComponent().cl(MagicTab.UNSELECTED_TAB_CONTENT);
            })

        component.getComponent().rcl(MagicTab.UNSELECTED_TAB_CONTENT)
        _.each(this.#headers, (header) => {
            _e(header.name, component.getName())
            ? header.headerEl.cl(MagicTab.SELECTED_TAB_CLASS)
            : header.headerEl.rcl(MagicTab.SELECTED_TAB_CLASS)
        })
        this.fireEvent(component, MagicTabEvent.EVENT_TAB_SELECTED, {tabContent}, this.#options.getParent())
    }

    selectTabByName(tabName){
        let selectedEl = _.find(this.#components, (component) => _e(component.getName(), tabName));
        this.changeTab(_.ofNullable(selectedEl, ()=> this.#components[0]), this.#tabContentPanel);
    }

    getSelectedTab(){
        return _.each(this.#headers, (header, i) => {
            if(header.headerEl.ccl(MagicTab.SELECTED_TAB_CLASS)){
                header.index = i;
                return header;
            }
        })
    }

    getSelectedName() {
        if (this.#components.length < 1) {
            return null;
        }
        let first = this.#components[0];

        if (!this.#options.isPinable()) return first.getName();
        try {
            let setting = PersonalSettings.getSetting(MagicTabOptions.TAB_PANE_PERSONAL_SETTING_PREFIX + this.#options.getName())
            return _.each(this.#components, (component) => {
                const componentName = component.getName()
                if (_e(componentName, setting)) {
                    return componentName
                }
            });
        } catch (e) {
            return first.getName();
        }

    }

    getSelectedTabComponent() {
        return this.#selectedComponent
    }

    requireConfirmToChange() {
        const activeComponent = this.getSelectedTabComponent()
        if(!activeComponent || _e(this.#tabToChange, activeComponent)) return false
        return activeComponent.getIsEdited()
    }
}

class MagicTabItem {
    #name
    #builder
    #component
    #parent
    #image
    #isEdited = false

    constructor(tabName, builder, component, image, parent) {
        this.#name = tabName;
        this.#builder = builder;
        this.#component = component;
        this.#parent = parent;
        this.#image = image;
    }

    build() {
        return this
    }

    getName(){
        return this.#name
    }

    getBuilder(){
        return this.#builder
    }

    setBuilder(builder) {
        this.#builder = builder
        return this
    }

    getComponent() {
        return this.#component
    }

    setComponent(component) {
        this.#component = component
        return this
    }

    getParent() {
        return this.#parent
    }

    getImage() {
        return this.#image
    }

    setIsEdited(value) {
        this.#isEdited = value
        return this
    }

    getIsEdited() {
        return this.#isEdited
    }
}
