class RemoteFile {
    #abstractFile;
    static NO_FILE = "no_file";
    static DLETED_FILE_ICON = "images/deleted-file.svg";

    static delete(id, callback = () => {}){
      Executor.runDelete(Environment.fileStorageApi + "delete/" + id, callback)
    }

    draw(at) {
        at.add(h.div("attachment").cl(this.#abstractFile.getOptions().getExtensionClassPrefix() + "_" + this.#abstractFile.getFileExtension())
        .addIf( this.getOptions().getShowFileName(),
            h.a(this.#abstractFile.getSrc(), this.#abstractFile.getFileName(), false, true).cl("attachment_link")
              )
        )
    }

    getFile() {
        return this.#abstractFile;
    }

    constructor(abstractFile) {
        this.#abstractFile = abstractFile;
    }

    getFileName() {
        return this.#abstractFile.getFileName();
    }

    getFileExtension() {
        return this.#abstractFile.getFileExtension();
    }

    getOptions() {
        return this.#abstractFile.getOptions();
    }

    getSrc() {
        return this.#abstractFile.getSrc();
    }

    getCustomImageFunc(){
      return this.#abstractFile.getOptions().getOnClickImage();
    }

    isImage(){
      return false;
    }

    imageClick(){
      let customFunc = this.getCustomImageFunc();
      customFunc
      ? customFunc()
      : _.showFullImage(this.getSrc())
    }

    isVideo(){
      return false;
    }

    isAudio() {
      return false;
    }

    static showFull(fileClass){
      if(fileClass.isImage()) {
        utils.showFullMedia(fileClass.getSrc(), []);
      } else if (fileClass.isVideo()) {
        utils.showFullMedia(fileClass.getSrc(), [], "video");
      } else if(fileClass.isAudio()) {
        utils.showFullMedia(fileClass.getSrc(), [], "audio")
      }
    }
}

class PdfFile extends RemoteFile {
    constructor(abstractFile) {
        super(abstractFile);
    }

}

class ExcelFile extends RemoteFile {
    constructor(abstractFile) {
        super(abstractFile);
    }
}

class DocumentFile extends RemoteFile {
    constructor(abstractFile) {
        super(abstractFile);
    }
}

class ArchiveFile extends RemoteFile {
    constructor(abstractFile) {
        super(abstractFile);
    }
}

class AudioFile extends RemoteFile {
    constructor(abstractFile) {
        super(abstractFile);
    }

    isAudio(){
      return true;
    }
}

class PresentationFile extends RemoteFile {
    constructor(abstractFile) {
        super(abstractFile);
    }
}

class ImageFile extends RemoteFile {
    constructor(abstractFile) {
        super(abstractFile);
    }

    draw(at) {
        let isPropagation = this.getFile().getOptions().getClickPropagation()
        let el = h.img(this.getSrc(), this.getOptions().getPreviewWidth()).width(this.getOptions().getWidth()).height(this.getOptions().getHeight());
        if (!_.endsWith(this.getSrc(), ImageFile.NO_FILE)){
          el.click(()=> this.imageClick(), isPropagation);
        }
        at.add(el);
    }

    isImage(){
      return true;
    }
}

class UnknownFile extends RemoteFile {

  static CLASS_NAME = "file_is_unknown";

  #abstractFile
    constructor(abstractFile) {
        super(abstractFile);

        this.#abstractFile = abstractFile;
    }

    draw(at) {
      at.add(h.div("attachment").cl(UnknownFile.CLASS_NAME)
      .addIf( this.getOptions().getShowFileName(),
          h.a(this.#abstractFile.getSrc(), this.#abstractFile.getFileName(), false, true).cl("attachment_link")
            )
      )
  }
}

class VideoFile extends RemoteFile {
  constructor(abstractFile) {
      super(abstractFile);
  }

  isVideo(){
    return true;
  }
}



class FileDisplayOptions {

    #width = 16;
    #height = 16;
    #previewWidth = Environment.defaultIconSize;
    #extensionClassPrefix = "extension_";
    #showFielName = true;
    #onClickImage = null;
    #clickPropogation = true;

    setExtensionClassPrefix(prefix){
      this.#extensionClassPrefix = prefix
      return this
    }

    getExtensionClassPrefix() {
        return this.#extensionClassPrefix;
    }

    setPreviewWidth(prwidth){
      this.#previewWidth = prwidth;
      return this
  }

    getPreviewWidth(){
        return this.#previewWidth;
    }

    setWidth(width){
      this.#width = width
      return this
    }

    getWidth(){
      return this.#width;
    }

    setHeight(height){
      this.#height = height
      return this
    }

    getHeight(){
      return this.#height;
    }

    setShowFileName(fileName){
      this.#showFielName = fileName;
      return this;
    }

    getShowFileName(){
      return this.#showFielName;
    }

    setOnClickImage(func){
      this.#onClickImage = func;
      return this;
    }

    getOnClickImage(){
      return this.#onClickImage;
    }

    setWidthandHeight(width, height){
      this.#width = width;
      if(height){
        this.#height = height;
      }else{
        this.#height = width;
      }
      return this
    }

    setClickPropagation(isPropagation){
      this.#clickPropogation = isPropagation;
      return this
    }

    getClickPropagation(){
      return this.#clickPropogation;
    }


}



class AbstractFile {
    static NO_EXTENSION = "";
    #src;
    #options;
    #file;

    setOptions(options){
        this.#options = options;
    }

    isEmptySrc(){
      return _.isNull(this.#src);
    }

    setSrc(src) {
        const https = 'https://';
        const newSrc = src.replace(https, '').replace('//', '/');
        this.#src = https + newSrc;
    }

    getSrc() {
        return this.#src;
    }

    getFile(){
        return this.#file;
    }

    setFile(file){
        this.#file = file;
        return this;
    }

    getOptions() {
        return this.#options;
    }

    getFileType() {
      return this.getFileExtension();
    }

    getFileExtension() {
        if (this.isEmptySrc()) {
            return AbstractFile.NO_EXTENSION;
        }

        if(this.getIsDeleted()) {
          return "svg";
        }

        let els = this.getFileName().split(".");
        return els[els.length - 1];
    }

    getFileName() {
        if (this.isEmptySrc()) {
            return AbstractFile.NO_EXTENSION;
        }
        let els = this.#src.split("/");
        return els[els.length - 1];
    }

    draw(at) {

    }

}

class SimpleRemoteFile extends AbstractFile {
  constructor(src, options) {
      super();
      this.setSrc(src)
      this.setOptions(options);
  }

  draw(at) {
      let ext = this.getFileExtension();
      if (_.isImage(this.getSrc())) {
        this.setFile( new ImageFile(this));
      } else if (_.in(ext, "mp3", "wav", "ogg")) {
        this.setFile( new AudioFile(this));
      } else if (_.in(ext, "mp4", "mov", "avi", "wmv")) {
        this.setFile( new VideoFile(this));
      } else if (_.in(ext, "zip", "tar", "rar", "7z")) {
        this.setFile( new ArchiveFile(this));
      } else if (_e(ext, "pdf")) {
        this.setFile( new PdfFile(this));
      } else if (_.in(ext, "doc", "docx")) {
        this.setFile( new DocumentFile(this));
      } else if (_.in(ext, "xls", "xlsx")) {
        this.setFile( new ExcelFile(this));
      } else if (_.in(ext, "ppt", "pptx")) {
        this.setFile( new PresentationFile(this));
      } else {
        this.setFile( new UnknownFile(this));
      }

      this.getFile().draw(at);
  }

  getIsDeleted(){
    return false;
  }
}

class FileStorageFile extends SimpleRemoteFile{
    #remoteData;
    #isDeleted = false;
    constructor(src, options) {
        super(src, options);
        this.setOptions(options);
        this.setSrc(src)
        this.fetchImageData();

    }

    getOriginalName() {
      return this.#remoteData.originalName;
    }

    getServerFileName() {
      return this.#remoteData.name;
    }

    getIsDeleted(){
      return this.#isDeleted;
    }

    fetchImageData(){
        Executor.runGet(
            Environment.fileStorageApi + "/info?path=" + this.getSrc(), (data) => {
                //types comes in mime types, so we need to replace image / to be able to run this method again
                this.#remoteData = data;
                if(data.hidden) {
                  this.setDeletedFile();
                }

            },
            false, Executor.HEADERS, (f) => {
              this.setDeletedFile();
            }
        );
    }

    setDeletedFile() {
      this.#isDeleted = true;
      this.setSrc(Environment.self + Environment.defaultTemplate + RemoteFile.DLETED_FILE_ICON);
    }

    getFileName() {
        return this.#remoteData?.originalName;
    }

    getId() {
      return this.#remoteData?.id;
    }

    getRemoteData(){
      return this.#remoteData
    }
}
class FileFactory{
  static load(src, options) {
    if (!utils.startsWith(src, Environment.fileStorageApi)) {
        return new SimpleRemoteFile(src, options);
    } else {
        return new FileStorageFile(src, options);
    }
  }
}
