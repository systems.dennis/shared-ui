class MagicBar{

  #maxValue;
  #currentValue;
  #were;
  #percent;
  
  constructor(maxValue, currentValue, were){
      this.#maxValue = maxValue;
      this.#currentValue = currentValue;
      this.#were = were;
      this.calculatePercent();

  }

  calculatePercent(){
  this.#percent = Math.round(this.#currentValue/this.#maxValue*100)
  if (this.#maxValue > 0 && this.#currentValue >= 0 && this.#currentValue<= this.#maxValue){
       this.drowBar();
  }
  }

  drowBar(){
     
      let barWrapper = h.div('bar_wrapper').appendTo(this.#were);
      let barMainLine = h.div('bar_main_line').appendTo(barWrapper);
      let barFillLine = h.tag('span').cl('bar_fill_line').appendTo(barMainLine);
      h.div('bar_text_persent').text(this.#percent+ ' %').appendTo(barMainLine);
      // barFillLine.get().style.width = this.#percent*98/100 + '%'
     
      let minValue = 0;

      let fillLineWidth = this.#percent*95/100;

      let timer = setInterval(function() {            
            barFillLine.get().style.width = minValue + '%';  
            minValue += 0.3;    
              if (minValue>=fillLineWidth){
                   clearInterval(timer);
              };
        }, 5);

        if(this.#were.isH){
          barWrapper.appendTo(this.#were.get());
        }else if(this.#were instanceof HTMLElement){
          barWrapper.appendTo(this.#were);
        }else if(get(this.#were)){
          barWrapper.appendTo(get(this.#were));
        }    
  
  }

}