class LateInvoke {
    renderer;
    item;
    cell;

    constructor(renderer, item, cell) {
        this.renderer = renderer;
        this.item = item;
        this.cell = cell;

    }

}

/**
 * v7.  Changed selection from click on row to click on checkbox
 *      checkbox will now be a separate column
 */
class MagicTable extends Selectable {
    static SELECTED_ROW_CLASS = 'row_selected'
    static SELECTED_ATTR = 'data-selected';
    static DELETED_ROW = "row_deleted";

    #magicTable;
    #defaultClickFunction;


    constructor(list) {
        super(list.getOptions());

        this.#magicTable = list;
        if (list.getOptions().getTableListener()) {
            this.#eventListener = list.getOptions().getTableListener();
        }
        this.#defaultClickFunction = list.getOptions().getDefaultClickFunction();
    }

    getId() {
        return this.#magicTable.getId();
    }

    getSelectedCount() {
        let res = 0;
        this.#root.eachOfClass(MagicTable.SELECTED_ROW_CLASS, () => {
            res++
        })

        return res;
    }

    findBySelectedId(id, row) {
        if (this.getSelectedCount() < 2) {
            return row;
        } else {
            return this.#root.eachOfClass(MagicTable.SELECTED_ROW_CLASS, (row) => {
                if (_e(row.getAttribute(MagicTable.SELECTED_ATTR), id)) {
                    return row;
                }
            })
        }
    }

    #currentRow;

    #dataConverter;
    #root = h.tag("table").cl("table magic");
    #eventListener;


    #sortable = [];
    #searchable = [];
    #editable = [];

    getCurrentRow() {
        return this.#currentRow
    }

    getParent() {
        return this.#magicTable;
    }

    getRoot() {
        return this.#root;
    }

    getDefaultClickFunction() {
        return this.#defaultClickFunction;
    }

    getDataConverter() {
        return this.#dataConverter;
    }

    createRow(header = false, item = null, drug) {
        this.#currentRow = new MagicTableRow(this, header, item);

        if (item?.___pseudo && item[PseudoRow.PSEUDO_ERROR_FIELD]) {
            //added info that this is only pseudo row, not saved and can be deleted

            this.#currentRow.cl("pseudo_error_field")
        }

        this.callEventListener(new TableEvent(TableEvent.TABLE_ROW_CREATED, this.#currentRow, item, header));

        if (_.notNull(drug)) {
            new Drug(item?.id, this.get(), drug.onDragDrop, drug.allowedClass);
        }
        return this.#currentRow;
    }


    /**
     *
     * @param event Always TableEvent
     */
    callEventListener(event) {
        if (_.notNull(this.#eventListener)) {
            return this.#eventListener(event);
        }
    }

    get() {
        return this.#root.get();
    }

    rebuildHeaders() {
        this.#root.text(null);
        this.drawHeader(this.#where, this.#cellType, this.#dataConverter);
    }

    getWhere() {
        return this.#where
    }

    #where;
    #cellType;
    #rowType;

    createCheckboxHeader() {
        return {
            "field": "___checkbox____",
            "translation": "global.app.checkbox.header",
            "sortable": false,
            "type": "___selection",
            "visible": true,
            "filterable": false
        }
    }

    drawHeader(where, cellType, dataConverter = null) {

        this.#where = where;
        this.#cellType = cellType;
        let fields = Array.from(this.#magicTable.getLastData().fields).sort((a, b) => {
            return a.order - b.order
        });
        this.#dataConverter = dataConverter;
        this.#root.text(null);
        this.#root.appendTo(where);
        this.#root.get().remove();


        let headers = this.#magicTable.getOrdering()?.getHeaders();

        if (headers != null) {
            fields = Array.from(headers).sort((a, b) => {
                return a.order - b.order
            });
        }

        this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CREATION_STARTED, this, fields, null, this.#magicTable))
        this.createRow(true);


        let abstractSort = new AbstractSort(this);


        /**
         * Adding a header for checkbox
         */

        fields.unshift(this.createCheckboxHeader())

        _.each(fields, (item) => {


            let cell
            if (item.visible) {
                this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CREATION_STARTED, this, fields, null, this.#magicTable))
                if (item.customized) {
                    cell = this.#currentRow.createCell('cell_header', item.translation, true);
                } else {
                    if (_e(item.field, "additionalValues")) return _.CONTINUE;
                    cell = this.#currentRow.createCell('cell_header', _.$(item.translation, false), true);
                }
                this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CREATION_FINISHED, item, cell, null, this.#magicTable))
            }
            // create sortImage in HeaderList, call a function from MagicSort. Function give us the same img as the sortForm
            if (item.sortable && !item.actions?.length && cell) {
                let hCell = cell.get();
                let tableSort = new TableSortHeader(item.field, abstractSort, cell.getThContent())
                abstractSort.addSortField(tableSort)
                tableSort.draw();
            }
        })

        let modalAbstractSort = this.#magicTable.getSort().getAbstractSort();
        modalAbstractSort.sync(abstractSort);
        modalAbstractSort.setParent(this)
        abstractSort.sync(modalAbstractSort);
        abstractSort.setParent(this)
        modalAbstractSort.buildActive();

        this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CREATION_FINISHED, this, fields, null, this.#magicTable))


        this.#root.appendTo(where);


        return this;
    }

    drawValues(data, list, dataConverter = null, eventListener = null) {

        let invokeLater = [];

        let _this = this
        if (dataConverter == null) {
            dataConverter = MagicTable.defaultConverter;
        }
        let headers = this.#magicTable.getOrdering()?.getHeaders();

        let visibleFields;
        if (headers != null) {
            visibleFields = headers;
        } else {
            visibleFields = this.#magicTable.getLastData().fields;
        }

        visibleFields = Array.from(visibleFields).sort((a, b) => {
            return a.order - b.order
        });

        /**
         * v7 change: adding force header with checkboxes
         */
        visibleFields.unshift(this.createCheckboxHeader())


        if (utils.e(data.content.length, 0)) return;

        let favoriteEnabled = list.getOptions().getIsFavoriteEnabled() && Favorite.enabled();

        this.resetCheckboxes()
        utils.each(data.content, (item) => {
            this.createRow(false, item, list.getOptions().getDrag());


            utils.each(visibleFields, (field) => {
                if (utils.isFalse(field.visible)) {
                    return utils.CONTINUE;
                }


                //v7 change: drawcheckbox in this cell
                if (_e(field.type, '___selection')) {
                    let res = h.span("selectable_cell")
                    Selectable.drawSelectComponent.bind(this)(item.id, res, this.getSelectedIds().contains(item.id))
                    this.#currentRow.createCell('cell_header', res)
                    return _.CONTINUE;
                }

                let cell;
                if (!utils.isEmpty(field.actions)) {
                    this.#currentRow.getMenuOptions().addActions(field.actions).setListner(list.startActions);

                    if (utils.notNull(this.getParent().getOptions().getColumnRenderer())) {
                        let render = this.getParent().getOptions().getColumnRenderer();
                        let res = render(field, item[field.field], item, this, null, null, favoriteEnabled);
                        if (res) {
                            this.#currentRow.createCell('cell_header', res)
                            return
                        }
                    }

                    let divActions = h.div('actions');
                    let content = h.tag("span").cl("actions_cell");
                    let imgContent = h.img("dots.png").cl("actions_cell_dots").wh(16).click(function () {
                        content.get().classList.toggle('hidden');
                        dom.linkTo(content.get(), imgContent.get());
                    }).appendTo(divActions);

                    let arr = [];

                    utils.each(field.actions, (action) => {
                        let act = h.from(this.drawAction(action.name, content, list, item['id'], this, item));
                        arr.push(act);
                        content.add(act);
                    });

                    let hasCustomImpl = utils.each(arr, (x) => {
                        if (x.ccl("custom_action")) return true;
                    });

                    if (!hasCustomImpl) {
                        content.cl('hidden').appendTo(divActions);
                    }


                    cell = hasCustomImpl ? this.#currentRow.createCell('cell_header', content) : this.#currentRow.createCell('cell_header', divActions);
                    this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CELL_CREATED, cell, item, field, this.#magicTable))


                } else {
                    let value = dataConverter(field, item[field.field], item, this, null, null, favoriteEnabled);
                    cell = this.#currentRow.createCell('cell_header', value).getParent();
                    this.callEventListener(new TableEvent(TableEvent.TABLE_HEADER_CELL_CREATED, cell.last(), item, field, this.#magicTable, {
                        field: field.field,
                        row: this.#currentRow
                    }))
                    if (field.renderer) {
                        invokeLater.push(new LateInvoke(field.renderer, item, value));
                    }
                }


            });

            utils.each(this.#magicTable.getLastData().fields, (field) => {
                if (utils.isFalse(field.visible)) {

                    if (!utils.isEmpty(field.actions)) {
                        this.#currentRow.getMenuOptions().addActions(field.actions).setListner(list.startActions);
                    }

                    return utils.CONTINUE;
                }

            })


        });
        _.each(invokeLater, (later) => {
            later.renderer(later.item, later.cell)
        })
    }

    selectRow(el, id) {
        this.callEventListener(new TableEvent(MagicTable.SELECTED_ROW_CLASS, el, id, this, this.#magicTable))
    }


    /**
     * Here only generating actions for the row
     * @param action - name of the action
     * @param cell - a place where the action will be displayed
     * @param rowAction - true if row (//todo to remove it)
     * @param id - id of the line
     * @param data - the whole row of data
     * @returns {HTMLSpanElement|HTMLImageElement|*|h.h}
     */

    drawAction(action, cell, list, id, magicTable, data) {

        let res = null;

        let _this = this;

        try {
            res = drawCustomAction(action, id, list, data)
        } catch (ex) {
            log.error(ex)

        }

        if (res != null) {
            return h.from(res).cl('custom_action');
        }

        if (_e(action, 'edit')) {

            return h.img("edit.png").wh(16).cl(action).click(() => {
                let magicFormOptions = new MagicFormOptions(list.getOptions().getServerRequest(), undefined, _this.getParent());
                magicFormOptions.setFormListener(list.getOptions().getFormListener());
                magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
                MagicForm.Instance(list.getOptions().getFavoriteType(), magicFormOptions).load(id);
            });

        }

        if (_e(action, "copy")) {

            return h.img("copy.png").wh(16).cl(action).click(function () {
                let copy = true
                let magicFormOptions = new MagicFormOptions(list.getOptions().getServerRequest(), undefined, this);
                magicFormOptions.setFormListener(list.getOptions().getFormListener());
                magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
                MagicForm.Instance(list.getOptions().getFavoriteType(), magicFormOptions).load(id, copy);
            });

        }

        if (_e(action, 'delete')) {
            let _this = this;
            return h.img("rm.png").wh(16).click(function (e) {

                let deleteRule = '____user_delete_objects_rule';
                let opts = new MagicPromptOptions("global.prompt.allow_delete_title", "global.prompt.change_group_text.allow_delete_text");
                opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                    opts.closeSelf();

                    let ids = list.getSelectedIds();

                    _.each(ids, (id) => {
                        _this.deleteRow(e, list, id);
                    })


                }).setIsAction(MagicPromptButton.OK_LABEL, true)
                    .applyClick(MagicPromptButton.CANCEL_LABEL, () => opts.closeSelf()).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                    .allowNoRepeat("global.prompt_DELETE").newPrompt();


            });
        }
        if (_e(action, 'details')) {
            return h.img("details.png").cl(action).wh(16).click(function () {
                showDetails(id);
            }).get();
        }

        if (utils.e(action, 'import')) {

            return h.img("import.png").cl(action).wh(16).click(function (e) {
                if (e.getAttribute("disabled") != undefined) {
                    h.message("global.messages.import.in_process");
                }

                let progress = new Loading(list.getGrid());
                progress.show(img);

                Executor.runPost(list.getRootPath() + "/import/", function () {
                    progress.hide();
                    dom.toast(_.$("global.messages.success"));
                    e.removeAttribute("disabled");
                    list.drawValues()

                }, function (e) {
                    Executor.errorFunction(e);
                    progress.hide();
                    e.removeAttribute("disabled");
                })
            }).get();

        }

        if (res == null) {
            return h.span("info.error.list.actions", "global.list.actions.not.implemented." + action);
        }


    }

    deleteRow(e, list, id) {
        e = h.from(e);
        let path = list.getOptions().getRoot("/delete/" + id)

        Executor.run(path, "DELETE", function () {

            while (!e.ccl('data_row') && !e.ccl('table')) {
                e = e.parent();
            }

            if (e.ccl('data_row')) {
                e.remove();
            } else if (e.ccl('table')) {
                log.trace('No data_row in table');
            }

            this.successToast(id);

            this.callEventListener(new TableEvent(MagicTable.DELETED_ROW, this, data, null, this.#magicTable))
        });
    }

    successToast(id = null) {

        var allowToast = this.callEventListener(new TableEvent(TableEvent.TABLE_TOAST_REQUESTED, this, id, null, this.#magicTable))

        if (!_e(allowToast, false)) {
            dom.toast(_.$("global.success"), ToastType.SUCCESS);
        }
    }

    static defaultConverter(header, what, data, table, dataType = null, onclick = null, favoriteEnabled) {

        let res = null;
        try {

            if (utils.notNull(table.getParent().getOptions().getColumnRenderer())) {
                let render = table.getParent().getOptions().getColumnRenderer();
                res = render(header, what, data, table, this, favoriteEnabled)
            }

            if (utils.notNull(drawCustomField) && utils.isNull(res)) {
                res = drawCustomField(header, what, data, table, this, favoriteEnabled)
            }

        } catch (ex) {
            log.error(ex)
        }

        if (!utils.isNull(res)) {
            return res;
        }

        if (utils.e(header.field, "id")) {

            let idWithFavarite = h.div("id").add(h.span("id_content", what, false))

            if (favoriteEnabled) {
                let heart = h.span(Favorite.FAVORITE_CLASS + "_holder", "", false).click(function (e) {

                    Favorite.markElementAsFavorite(e, data, table.getParent().getFavoriteType())
                })
                Favorite.markIfElementFavorite(heart, data, table.getParent().getFavoriteType())

                idWithFavarite.add(heart)
            }

            if (table.getParent()?.getOptions().getNeedMultiselect()) {

                //data, list, whereToDraw
                let val = table.isIdSelected(data.id)
                const checkbox = table.getSelectorRenderer().bind(table)(data.id, idWithFavarite, val);
                table.addCheckbox(checkbox, data.id, data)

            }

            return idWithFavarite
        }

        let type = _.ofNullable(dataType, () => {
            return (_.ofNullable(header.type, () => header.getType()))
        });


        //here is going something interesting. Something like a hell
        if (utils.e(type, "referenced_id")) {
            let miltiselector = table ? table.getParent().getOptions().getMultiServerRequest() : null;
            let refConverter = table ? table.getParent().getOptions().getReferensedConverter() : null;
            type = "object_chooser";
            if (_.notNull(miltiselector) && _.notNull(refConverter) && _.notNull(what)) {
                let scope = table.getParent().getOptions().getRequest().getScope()
                let request = miltiselector(header.field, scope);

                request.async(false).byId(what, (data) => {
                    what = refConverter(header.field, data);

                });
            }

        }
        if (utils.e(type, "checkbox")) {

            if (!utils.isFalse(what)) {
                return h.img("true.png").wh(16);
            } else if (what === false || what === "false" || what === "False") {
                return h.img("false.png").wh(16);
            }
        } else if (utils.e(type, "object_chooser")) {
            let val = h.tag("span").cl("value_span");
            let res = "";

            if (what != null && !utils.isEmpty(what.value)) {
                h.a("#").cl("object_link").text(what.value, false).click(onclick == null ? function () {

                    h.magicView(what.key, header.remoteType, false, header.searchName);

                    return false;
                } : onclick, false).appendTo(val);
            } else {
                val.text(what == null ? what : what.value);
            }

            //we add icon here if object contains an Icon (or download file link)
            if (what?.additional?.icon) {
                res = h.img(what.additional.icon, 32).cl("image_in_details").click(function (e) {

                    if (!_.isEmpty(h.from(e).getA("have-full-path"))) {
                        utils.showFullImage(_.getFullImgPath(e.src));

                        return;
                    }

                    utils.showFullImage(e.src)
                }).wh(16);
                val.get().prepend(res.get());
            }
            if (what == null) {
                val.text(null);
            }
            return val;
        }

        if (utils.e(type, "file")) {

            let img = "";
            if (utils.isEmpty(what)) {
                return "&nbsp;"
            }

            if (utils.isImage(what)) {

                img = h.img(what, Environment.defaultIconSize).wh(24).click(function (e, ex) {

                    if (!_.isEmpty(h.from(e).getA("have-full-path"))) {
                        utils.showFullMedia(_.getFullImgPath(e.src));

                        return;
                    }

                    utils.showFullMedia(e.src);
                })
            } else {
                img = document.createElement("a");
                img.href = what;
                img.target = "__blank";
                img.innerHTML = _.$("global.pages.download");
            }

            return h.from(img);
        }


        if (utils.e(type, "files")) {
            let div = h.div('icons')
            what?.forEach(e => {
                let img = "";
                if (utils.isEmpty(e)) {
                    return "&nbsp;"
                }

                if (utils.isImage(e)) {
                    img = h.img(e).wh(24).appendTo(div)
                } else {
                    img = document.createElement("a");
                    img.href = what;
                    img.target = "__blank";
                    img.innerHTML = _.$("global.pages.download");
                }
            })
            div.click(function () {
                _.showFullMedia(what[0], what)

            })

            return h.from(div);
        }
        if (_e(type, "date")) {
            if (_.isNull(what?.date)) {
                log.info("warning: date as string returned " + what + " check the back-end configuration. Since version 3.2.4 of shared component it is deprecated");
                return h.div("time_div").text(what, false);
            }
            return h.div("time_div")
                .text(_.getLocalDatefromMillisecond(what.date, true), false)
                .andTextIf(_e(header.format, "datetime"), " " + _.getLocalTimefromMillisecond(what.date), false)
        }


        return what;
    }

}

class MagicTableRow extends h {
    #parent;
    #root;
    #isHeader;
    #menuOptions;
    #menu;
    #cells = []
    #data;

    constructor(table, isHeader, info = null) {
        super();
        this.#parent = table;
        this.#root = h.tag("tr").cl("tr").cl("data_row").appendTo(table.getRoot());
        this.setClickFunction();
        this.#isHeader = isHeader;
        if (!isHeader) {
            this.#data = info;
            this.#menuOptions = new MagicMenuOptions(this.#root, info, this, "data_row");
            const custom = table?.getParent()?.getOptions()?.getContextMenuOptions()?.getContextMenuToGroupTransformer();
            if (custom) {
                this.#menuOptions.setContextMenuToGroupTransformer(custom);
            }
            ;
            this.#menu = new MagicMenu(this.#menuOptions, info);
        } else {

        }
    }

    setClickFunction() {
        const clickFunction = this.#parent.getDefaultClickFunction();
        if (!clickFunction) {
            return;
        }
        this.#root.click(clickFunction);
    }

    getMenuOptions() {
        return this.#menuOptions
    }

    getData() {
        return this.#data;
    }

    parent() {
        return this.#parent;
    }

    getCells() {
        return this.#cells;
    }

    get() {
        return this.#root.get();
    }

    getDataConverter() {
        return this.#parent.getDataConverter();
    }

    createCell(type, value) {

        let cell = new MagicTableCell(this, this.#isHeader).draw(type, value);
        this.#cells.push(cell)
        return cell;
    }


}

class MagicTableCell extends h {
    #parent;

    #root;

    #isHeader;

    #resize;

    #thContent;

    constructor(row, isHeader = false) {
        super();
        this.#parent = row;
        this.#isHeader = isHeader;
        let tag = isHeader ? "th" : "td";
        this.#root = h.tag(tag);

        if (isHeader) {
            this.#root.cl("th").cl("data_header");
        } else {
            this.#root.cl("td").cl("data_cell");
        }
    }

    get() {
        return this.#root;
    }

    getParent() {
        return this.#parent;
    }

    getThContent() {
        return this.#thContent
    }

    draw(type, value) {


        if (this.#isHeader) {

            let litType = this.#parent.parent().getParent().getFavoriteType();
            let resizeField = litType + "_" + value + '_resize'
            this.#resize = h.div('data_header__resize').setData("resize", resizeField).appendTo(this.#root);
            this.#thContent = h.div('data_header__content').appendTo(this.#root).add(h.span('data_header__span').text(value, false))
            cache.isPresent(this.#resize.getData('resize')) ? this.#thContent.get().style.width = cache.get(this.#resize.getData('resize')) : null
            this.#root.cl(type).appendTo(this.#parent.get());
            this.addResizeEvent(resizeField);
            return this;
        }

        let dataConverter = this.#parent.getDataConverter();
        if (dataConverter) {
            value = dataConverter(value, value);
        }
        if (value != null && value.isH) {
            this.#root.add(value).appendTo(this.#parent.get());
        } else {
            this.#root.cl(type).text(value, false).appendTo(this.#parent.get());
        }

        return this;
    }

    parent() {
        return this.#parent;
    }

    addResizeEvent(resizeField) {
        this.startX
        this.startWidth;
        let tEvent
        this.#resize.handleEvent('mousedown', (event) => {
            tEvent = event
            this.startX = event.clientX;
            this.startWidth = this.#thContent.get().offsetWidth;
            document.addEventListener('mousemove', this.myFunc = (ev) => {
                this.mousemoveEvent(ev, this)
            })
        })
        document.addEventListener('mouseup', (e) => {
            document.removeEventListener('mousemove', this.myFunc)
            if (_.e(resizeField, h.from(tEvent?.target)?.getData('resize'))) {
                cache.add(h.from(tEvent.target).getData("resize"), this.#thContent.get().style.width)
            }
        });
    }

    mousemoveEvent(event, this_) {
        const width = this_.startWidth + event.clientX - this_.startX;
        this_.#thContent.width(width);

    }
}

