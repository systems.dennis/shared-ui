class TableDrug{
    onDragStart;
    allowedClass;

    onDragDrop;
}
class MagicListOptions {

    static SELECT_MODE_NONE = 0;
    static SELECT_MODE_SINGLE = 1;
    static SELECT_MULTI = 2;

    static DISPLAY_AS_TABLE = "table";

    static DISPLAY_AS_GREED = "grid";

    #drag;

    #serverRequest;

    #gridOptions;

    #searchEnabled = true;
    #drawSearchFieldTo;
    #settingsEnabled;
    #orderedFieldsFilter = MagicOrdering.removeActionsField;

    #drawPaginationTo;

    #allowedTableActions;

    #searchListner
    #parent;
    #customTableAction;
    #searchDelay = 500;
    #formMultiServerRequest = null;
    #customPagination = null;
    #savedItemsInSearchForm = true;
    #quickSearchByEnter = false;
    #displayType = MagicListOptions.DISPLAY_AS_TABLE;
    #attribute;

    getQuickSearchByEnter(){
        return this.#quickSearchByEnter;
    }

    setQuickSearchByEnter(val){
        this.#quickSearchByEnter = val;
        return this;
    }

    getNeedSavedItems(){
        return this.#savedItemsInSearchForm
    }

    setNeedSavedItems(val){
        this.#savedItemsInSearchForm = val
        return this
    }

    getFormMultiServerRequest(){
        return this.#formMultiServerRequest
    }

    setFormMultiServerRequest(func){
       this.#formMultiServerRequest = func 
       return this 
    }

    setCustomPagination(func) {
        this.#customPagination = func();
        return this;
    }

    getCustomPagination() {
        return this.#customPagination;
    }

    setDrawSearchFieldTo(to){
        this.#drawSearchFieldTo = to;
        return this;
    }

    getDrawSearchFieldTo(){
        return this.#drawSearchFieldTo;
    }

    setDrawPaginationTo(drawTo){
        this.#drawPaginationTo  = drawTo;
    }

    getSearchDelay(){
        return this.#searchDelay;
    }

    setSearchDelay(delay){
        this.#searchDelay = delay;

        return this;
    }

    getDrawPaginationTo(){
        return this.#drawPaginationTo;
    }

    getGridOptions(){
        return this.#gridOptions;
    }

    setGridOptions(opts){
        if (_.notNull(opts) && ! (opts instanceof MagicGridOptions)){
            throw  Error ("options should be extended from MagicGridOptions!")
        }
        this.#gridOptions = opts;
        return this;
    }

    setDrag(dragInfo){
        if (dragInfo instanceof TableDrug){
            this.#drag = dragInfo;
            return this;
        }
        throw Error("Drag should be instance of TableDrug");
    }

    getDrag(){
        return this.#drag;
    }


    getRequest(uri){

        if (_.isNull(this.#serverRequest)){
            this.#serverRequest= new ServerRequest().entry();
        }

        return this.#serverRequest.copy(false, uri )
    }

    setDisplayType(type){
        this.#displayType = type;
        return this;
    }

    getDisplayType(){
        return this.#displayType;
    }


    getAttribute(){
        return this.#attribute
    }

    setAttribute(attribute){
        this.#attribute= attribute;
        return this;
    }

    setServerRequest(request){
        this.#serverRequest = request
        return this
    }

    #needMultiSelect = true;

    #selectable = false;

    #dataPathModifier;

    #listener;
    #tableListener;

    #availableFields;
    #listGroupToPath = "";
    #contextMenuToGroupTransformer = null;
    #listListener;

    setContextMenuToGroupTransformer(func){
      this.#contextMenuToGroupTransformer = func;

      return this;
    }

    getContextMenuToGroupTransformer() {
      return this.#contextMenuToGroupTransformer
    }

    #contextMenuListener;
    #groupId
    static DEFAULT(type) {
        return new MagicListOptions(type)
    };

    #queryModifier;


    setAvailableFields(fields) {
        this.#availableFields = fields;
    }

    setListListener(listener){
        this.#listListener = listener;
    }

    getListListener(){
        return this.#listListener;
    }

    getOrderedFieldsFilter() {
      return this.#orderedFieldsFilter;
    }

    setOrderedFieldsFilter(func) {
      this.#orderedFieldsFilter = func;

      return this;
    }

    getAvailableFields() {
        return this.#availableFields;
    }

    setCustomTableAction(val) {
      this.#customTableAction = val; return this;
    }

    getListGroupToPath(){
        return  this.#listGroupToPath
    }

    getCustomTableAction(){
      return this.#customTableAction;
    }

    setTableAllowedActions(actions) {
        this.#allowedTableActions = actions;
    }

    getTableAllowedActions() {
        return this.#allowedTableActions;
    }

    #columnRenderer

    setQueryModifier(modifier) {
        this.#queryModifier = modifier;
    }

    setColumnRenderer(renderer) {
        this.#columnRenderer = renderer;
    }

    setContextMenuListener(listener) {
        this.#contextMenuListener = listener;

        return this;
    }

    setMultiSelect(val) {
        this.#needMultiSelect = val;

        return this;
    }

    getNeedMultiselect() {
        return this.#needMultiSelect;
    }

    getContextMenuListener() {
        return this.#contextMenuListener;
    }

    getColumnRenderer() {
        return this.#columnRenderer;
    }

    getParent() {
        return this.#parent;
    }

    getQueryModifier() {
        return this.#queryModifier;
    }

    getTableListener() {
        return this.#tableListener;
    }

    getSearchListener() {
        return this.#searchListner;
    }

    #formListener = null;
    #formFieldListener = null;

    setTableListener(listener) {
        this.#tableListener = listener;
        return this;
    }

    setFormListener(listener) {
        this.#formListener = listener
    }

    setFormFieldListener(listener) {
        this.#formFieldListener = listener

        return this;
    }

    setDataPathModifier(dataPathModifier) {
        this.#dataPathModifier = dataPathModifier;
    }

    setSearchListener(listener) {
        this.#searchListner = listener;
    }

    getFormListener() {
        return this.#formListener;
    }

    getFormFieldListener() {
        return this.#formFieldListener;
    }

    getDataPathModifier() {
        return this.#dataPathModifier;
    }


    constructor( serverRequest, parent = null)
     {
         this.#serverRequest = serverRequest;

        this.#settingsEnabled = true;
        this.selectable = false;
        this.#parent = parent;
    }

    getSettingsEnabled() {
        return this.#settingsEnabled;
    }

    getSearchEnabled() {
        return this.#searchEnabled;
    }

    setSearchEnabled(enabled){
        this.#searchEnabled = enabled;
        return this;
    }

    fire(event){
        if (_.notNull(this.#listListener)) {
            return this.#listListener(event);
        }
    }

    #isEnabled = false;

    setIsFavoriteEnabled(value){
        this.#isEnabled = value;
        return this;
    }

    getIsFavoriteEnabled() {
        return this.#isEnabled;
    }
}

class MagicList2 {
    #options;
    #listListener;

    #container;
    #magicTable;

    #headers;
    #fetchData;


    #grid = h.div("pg_table_container");

    #form = h.div("modal").cl("hidden").id(new Date().getTime());

    #formClass;
    #sortAndSearchForm = h.div("table_config");
    #sort;

    #searchForm;

    #ordering;

    #favoriteType;
    #lastData;

    #pagination;

    #searchEnabled;

    #id;


    getGrid() {
        return this.#grid;
    }

    getFavoriteType() {
        return this.#favoriteType;
    }

    getPagination() {
        return this.#pagination;
    }

    getRootPath() {
        return this.getOptions().getRoot();
    }

    getId() {
        return this.#id;
    }

    getLastData() {
        return this.#lastData;
    }

    getOrdering() {
        return this.#ordering;
    }

    getTableObject() {
        return this.#magicTable;
    }

    getContainer() {
        return this.#container;
    }

    getSearchEnabled() {
        return this.#searchEnabled;
    }

    setListListener(listener) {
        this.#listListener = listener;
    }

    constructor(options = null) {

        if (options == null) {
            options = MagicListOptions.DEFAULT();
        }
        this.#options = options;


        /**
         * We assume that here can only 1 parameter to be changed.
         * For this, we accept nulls as value by default
         */

    }

    getFetchData(){
      return this.#fetchData;
    }

    getForm() {
        return this.#form;
    }

    getFormClass(){
      return this.#formClass;
    }


    getSort() {
        return this.#sort;
    }

    getOptions() {
        return this.#options;
    }

    getListListener() {
        return this.#listListener;
    }

    getSearchAndSettingsBar() {
        return this.#sortAndSearchForm;
    }

    getSearchForm() {
        return this.#searchForm;
    }


    load(where) {
        let _this = this;
        this.#options.fire(new MagicListEvent(MagicListEvent.PRELOAD_LIST, this));
        this.getOptions().getRequest().listFetch((data)=> {
            _this.build(data, where)})
    }

     startActions(event) {

          let action = event.eventType;
          let id = event.id;
          let list = event.parent.parent().getParent();


          if (!_.isNull(list.getOptions()) && !_.isNull(list.getOptions().getContextMenuListener())) {
              if (list.getOptions().getContextMenuListener()(event)) {
                  return;
              }
          }

          if (_e(action, 'edit')) {
              let magicFormOptions = new MagicFormOptions(list.getOptions().getRequest(),  list);
              magicFormOptions.setFormListener(list.getOptions().getFormListener());
              magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
              MagicForm.Instance(list.getOptions().getRequest().getUniqueId(), magicFormOptions).load(id);
              return;
          }

          if ((_e(action, "copy"))) {
              let copy = true;
              let magicFormOptions = new MagicFormOptions(list.getOptions().getRequest(),  list);
              magicFormOptions.setFormListener(list.getOptions().getFormListener());
              magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
              MagicForm.Instance(list.getOptions().getRequest().getUniqueId(), magicFormOptions).load(id, copy);
              return;
          }


          if (_e(action, 'delete')) {
              if (list.getTableObject().getSelectedCount() >= 2) {
                  _.each(list.getTableObject().getSelectedIds(), (id) => {
                    list.deleteById(list, id)
                  })
              } else {
                list.deleteById(list, id)
              }
          }

          if (_e(action, 'details')) {
              showDetails(id);
          }

          if (utils.e(action, 'import')) {

              this.getOptions().getRequest().import(()=>{ progress.hide();
                  dom.toast(_.$("global.messages.success"));
                  list.drawValues()
              })

          }


      }

      deleteById(list, id) {
        try{
            let opts = new MagicPromptOptions("global.prompt.allow_delete_title", "global.prompt.change_group_text.allow_delete_text");
            opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                opts.closeSelf();
                list.deleteRowByContextMenu(list, id)
                opts.markNoRepeat()
            }).setIsAction(MagicPromptButton.OK_LABEL, true)
                .applyClick(MagicPromptButton.CANCEL_LABEL, () => opts.closeSelf()).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                .allowNoRepeat("global.prompt_DELETE").newPrompt();
        }catch{
            list.deleteRowByContextMenu(list, id)
            dom.message()
        }


    }

    deleteRowByContextMenu(list, id) {
        this.#options.getRequest().delete(id, ()=> {
            LocalCacheProvider.getInstance().deleteFromCache(this.#options.getRequest().copy().fetchData())
            list.drawValues()
        });
    }


    build(data, where){
        this.#lastData = data;
        let options = this.getOptions();
        if (!_.isEmpty(options.getAvailableFields())) {
            _.each(data.fields, (item) => {
                if (!options.getAvailableFields().includes(item.field)) {
                    item.visible = false;
                }
            })
        }

        this.#id = where;
        this.#container = h.from(where.isH ? where : get(where));

        this.drawListTitle(where);
        //form is used to generate edit form or add form
        this.#form.appendTo(this.#container);

        //grid is a container where <table> is generated
        this.#grid.appendTo(this.#container);
        const customPagination = this.#options.getCustomPagination();
        if (customPagination) {
            customPagination.setParent(this).drawPagination();
        } else {
            this.drawPagination()
        }

        //when we are ready we can draw values from the server

        this.drawValues()

        return this;

    }

    drawListTitle() {
        let data = this.#lastData;
        this.#favoriteType = data.objectType;
        if (data.showTitle) {
            h.div("title").text(data.tableTitle).appendTo(this.#container);
        }
        this.#headers = data; //todo check if we still need to hold it here
        this.#sortAndSearchForm.appendTo(this.#container);
        this.drawSettingsForm();
        if (this.#options.getSearchEnabled()) {
            this.drawSearch(data);
        }
        this.drawTableActions(data)
        this.createDataHeaders(data['fields']);
    }


    createDataHeaders() {
        if (_e(this.getOptions().getDisplayType(), MagicListOptions.DISPLAY_AS_TABLE)){
            this.#magicTable = new MagicTable(this).drawHeader(this.#grid);
        } else {
            this.#magicTable = new MagicGrid(this, this.getOptions().getGridOptions()).drawHeader(this.#grid);
        }
    }

    drawSettingsForm() {
        this.#sort = new MagicSort(this);
        this.#sort.build();

        this.#ordering = new MagicOrdering(this);
        this.#ordering.build();
    }

    drawTableActions(data) {

        //List actions are coming from server, if they are existing we need to draw them
        //Default action are: settings : open list settings
        //                    download : download the content of the table
        //                    new      : creates a new element
        // if action differs then it is expected that
        //                   drawCustomAction is defined by user
        // this method should be loaded on the page itself
        if (utils.isEmpty(data.listActions)) {
            return;
        }

        let div = h.div("actions");

        for (let i = 0; i < data.listActions.length; i++) {
            try {
                div.add(this.drawTableAction(data.listActions[i], div, data))
            } catch (ex) {
                log.error(ex);
            }
        }

        this.getSearchAndSettingsBar().add(div);


    }

    drawTableAction(action, div, data) {

        if (_.notNull(this.getOptions().getTableAllowedActions())

            && !this.getOptions().getTableAllowedActions().includes(action)) {
            return;
        }

        try {
          return this.getOptions().getCustomTableAction()(action, div, data)
        } catch (error) {
        }

        let _this = this;
        if (action == 'settings') {
            return h.tag("span").cl("settings")
                .add(h.divImg("sort_icon.png.png").cl(action).wh(16).click(function () {
                    _this.getSort().show();
                    _this.getOrdering().close();
                }))
                .add(h.divImg("settings.png").cl(action).wh(16).click(function () {
                    _this.getOrdering().show();
                    _this.getSort().close();
                })).get();

        }


        if (action == 'download') {


            let _this = this;

            return h.divImg("download.png").cl(action).wh(16).click(function () {

                _this.getOptions().getRequest().download(_this.createPayload(), (data) => {
                    h.a(_this.getRootPath() + "/root/download/data/" + data.pathToDownload, "", false).get().click();
                })


            }).get();
        }


        if (action == 'new') {
            let __ = this;

            this.#grid.add(div);
            return h.input("button").cl(action).cl("insert_new_btn").text("pages.forms.insert_new").click(this.loadForm).get();
        }

        if (action == 'import') {
            return h.divImg("import.png").wh("16").cl(action).click(function (e) {
                if (e.getAttribute("disabled") != undefined) {
                    dom.message("global.messages.import.in_process", true, ToastType.NODATA)
                }

                let progress = new Loading(_this.getGrid().get());
                progress.show(e);

                _this.getOptions().getRequest().import(()=>{
                    progress.hide();
                    dom.toast(_.$("global.messages.success"), ToastType.SUCCESS)
                    _this.drawValues();
                })
            }).get();


        }

        try {
            return drawCustomAction(action, div, null, this, data)
        } catch (ex) {
            log.error(ex)
            return h.span("info.error.list.actions", _.$("global.list.actions.not.implemented." + action));
        }

    }

    loadForm = () => {
      let magicFormOptions = new MagicFormOptions(this.#options.getRequest(), this)
      magicFormOptions.setFormListener(this.#options.getFormListener());
      magicFormOptions.setFormFieldListener(this.#options.getFormFieldListener());
      this.#formClass = MagicForm.Instance(this.#options.getRequest().getUniqueId() + this.#options.getAttribute(), magicFormOptions).load();
    }

    drawValues() {

        let _this = this;
        this.getOptions().getRequest().search(this.createPayload(), (data)=>{  _this.buildValues(data);})

    }

    buildValues(data) {
        this.#fetchData = data;
        this.#magicTable.rebuildHeaders();
        this.#magicTable.drawValues(data, this);
        const customPagination = this.#options.getCustomPagination();
        if (customPagination) {
            customPagination.rebuildPages(data);
        } else {
            this.getPagination().rebuildPages(data);
        }
        this.#options.fire(new MagicListEvent(MagicListEvent.BUILD_VALUES, this, data ));
    }

    drawPagination() {
        this.#pagination = new MagicPagination(this.#magicTable);
        this.#options.fire(new MagicListEvent(MagicListEvent.PREBUILD_PAGINATION, this, this.#pagination ));
        this.#pagination.build(this.getId())
    }

    drawSearch(data) {
        if (this.getOptions().getSearchEnabled()) {
            this.#searchForm = new SearchForm(data, this.getId(), this, this.#options.getSearchDelay());
            this.#searchForm.setNeedSavedItems(this.#options.getNeedSavedItems()) 
            this.#searchForm.setQuickSearchByEnter(this.#options.getQuickSearchByEnter())
            this.#searchForm.build();
        }
    }


    startActions(event, incomingList = null) {

        let action = event.eventType;
        let id = event.id;
        let list =  incomingList || event.parent.parent().getParent();
        

        if (!_.isNull(list.getOptions()) && !_.isNull(list.getOptions().getContextMenuListener())) {
            if (list.getOptions().getContextMenuListener()(event)) {
                return;
            }
        }

        if (_e(action, 'edit')) {
            let magicFormOptions = new MagicFormOptions(list.getOptions().getRequest(), list);
            magicFormOptions.setFormListener(list.getOptions().getFormListener());
            magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
            MagicForm.Instance(list.getOptions().getRequest().getUniqueId(), magicFormOptions).load(id);
            return;
        }

        if ((_e(action, "copy"))) {
            let copy = true;
            let magicFormOptions = new MagicFormOptions(list.getRequest(), list);
            magicFormOptions.setFormListener(list.getOptions().getFormListener());
            magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
            MagicForm.Instance(list.getOptions().getRequest().getUniqueId(), magicFormOptions).load(id, copy);
            return;
        }


        if (_e(action, 'delete')) {

            if (list.getTableObject().getSelectedCount() >= 2) {
                _.each(list.getTableObject().getSelectedIds(), (id) => {
                  list.deleteById(list, id)
                })
            } else {
              list.deleteById(list, id)
            }
        } 

        if (_e(action, 'details')) {
            try {
                showDetails(id);
            } catch (e){
                log.error( e )
            }
        }

        if (utils.e(action, 'import')) {
            this.getOptions().getRequest().import(()=> {
                dom.toast(_.$("global.messages.success"));
                list.drawValues()})
        }
    }

    //todo options get server request -> remove

    createPayload(withoutCustomModifier = false) {
        if (_.isNull(this.#pagination) && _.isNull(this.#options.getCustomPagination())) {
            return new MagicRequest();
        }

        let req = new MagicRequest();
        let page = 0;
        const customPagination = this.#options.getCustomPagination();
        if (customPagination) {
            page = customPagination.getPageNumber();
            req.limit = customPagination.getPerPage();
        } else {
            page = this.#pagination.getPageNumber();
            req.limit = this.#pagination.getPerPage();
        }


        req.page = page;

        if (this.getOptions().getSearchEnabled()) {
            req.query = this.#searchForm.getSearchCriteria();
        }

        req.sort = this.getSort().getSort();


        if (!withoutCustomModifier && this.getOptions().getQueryModifier() != undefined) {
            let func = this.getOptions().getQueryModifier();
            func(req, this);
        }

        return req;
    }

}

