class MagicTreeOptions{
    #paddingDef;
    #onSelect;
    #id;
    #subFetcher;
    #treeListener;
    #itemRenderer;

    constructor(paddingDef = 10, onSelect, subFetcher , id = null){
        this.#paddingDef = paddingDef;
        this.#onSelect = onSelect;
        this.#id = id;
        this.#subFetcher = subFetcher;


    }

    setItemRenderer(renderer){
        this.#itemRenderer  = renderer;
    }

    setTreeListener(listener){
        this.#treeListener = listener;
    }

    getItemRenderer(){
        return this.#itemRenderer;
    }

    getTreeListener(){
        return this.#treeListener;
    }

    getPaddingDef(){
        return this.#paddingDef;
    }

    getOnSelect(){
        return this.#onSelect;
    }
    getId(){
        return this.#id;
    }

    getSubFetcher(){
        return this.#subFetcher;
    }

}

class MagicTree {

    #options

    #userDataDiv = h.div("tree_data_content");

    constructor(options) {
        this.#options = options;
    }

    getOptions(){
        return this.#options;
    }

    getListener(){
        return this.#options.getTreeListener();
    }

    getContent(){
        return this.#userDataDiv;
    }

    fireEvent(eventName, element, elementId, tree){

        if(_.notNull(this.getListener())){
            this.getListener()(new MagicTreeEvent(eventName, element, elementId, tree ));
        }
    }

    addItem(idRoot, data) {
 
        let level = this.getLevel(idRoot);

        let box = idRoot.firstByClass("tree_box");

        if (box == null) {
            box = h.div("tree_box").appendTo(idRoot);

        }

        let text = data.cnt > 0 ? (data.name + " [" + data.cnt + "]") : data.name
        if (data.selectable) {

            if (_.isNull(this.#options.getItemRenderer() )) {
                text = this.defaultRenderItem(text, data)
            } else {
                text = this.#options.getItemRenderer()(text, data);
            }
        }
        let res = h.div("new_item").id(idRoot.get().id + "_" + level)
            .setData("parent", idRoot.get().id)
            .setData("level", level)
            .appendTo(box);

        if (data.selectable) {
            text.appendTo(res);
        } else {
            res.text(text, false);
        }

        let _this = this;
        if (data.cnt > 0) {
            h.img("collapsed.png.png").data_id(data.id).cl("collapsed").wh(16).prependTo(res).click(function (e) {
                _this.collapseOrFetch(e);
            });
        } else {
            h.img("tree_item.png.png").data_id(data.id).prependTo(res).wh(16)
        }

        res.get().style.paddingLeft = this.getOptions().getPaddingDef() * level;

        this.fireEvent(MagicTreeEvent.ADD_ITEM, res, data.id, this)
        return res;
    }

    collapseOrFetch(e) {
        if (e.classList.contains("expanded_tree_item")) {
            this.collapse(e)
        } else {
            this.expand(e);
        }
    }

    defaultRenderItem(text, data){
        let _this = this;
        return h.a(null, text, false).click(function (e,ev){
            // dom.redirect('?id=' + data.id)
            try{
                ev.preventDefault();

                let el = document.querySelectorAll(".clicked");
                for (let i = 0; i< el.length; i ++ ){
                    el[i].classList.remove("clicked");
                }
                e.classList.add("clicked");

                _this.getOptions().getOnSelect()(data, _this.getOptions().getId())
            }catch (e){
                console.error(e)
            }
        });
    }

    collapse(e) {
        let el = h.from(e).src("collapsed.png.png").rcl("expanded_tree_item").firstByClass("tree_box", false);
        let divel = el.querySelectorAll('.tree_box');
        for (let i = 0; i < divel.length; i++) {
            divel[i].style.display = 'none';
        }

        this.fireEvent(MagicTreeEvent.COLLAPSE, h.from(e), h.from(e).getDataId(), this)
    }

    expand(e) {
       e = h.from(e).src("expanded.png.png").cl("expanded_tree_item");
        if (!e.ccl("data_fetched")) {
            e.cl("data_fetched");

            let data = this.getOptions().getSubFetcher()(e);
            utils.each(data, (item) => { this.addItem(e.parent(true), item) })

        }

        e.parent(true).eachOfClass("tree_box", (divEl)=> {
            divEl.style.display = 'block';
        })

        this.fireEvent(MagicTreeEvent.EXPAND, h.from(e), h.from(e).getDataId(), this) 
    }

    remove(idRoot) {
        idRoot.get().remove();
        this.fireEvent(MagicTreeEvent.EXPAND, idRoot, idRoot, this) 
    }

    getLevel(root) {
        let level = root.getData("level");
        if (utils.e(level, undefined)) {
            level = 0;
        }

        return parseInt(level) + 1;
    }


}