class MagicPromptOptions{
    static  GLOBAL_CONTEXT = "global";
    static NO_REPEAT_PREFIX = "no_repeat_property_";
    static defaultButtons(parent) {
        return [
            new MagicPromptButton(MagicPromptButton.CANCEL_LABEL,  ()=> {parent.hide()}),
            new MagicPromptButton(MagicPromptButton.OK_LABEL, null, null, true )
        ]
    }

    #buttons = [];

    #title;
    #text;

    #noRepeatProperty;

    #selfLink;



    setSelfLink(parent){
        this.#selfLink = parent;
        return this;
    }
    getButtons(){
        return this.#buttons;
    }

    getTitle(){
        return this.#title;
    }

    getText(){
        return this.#text;
    }
    getNoRepeatProperty(){
        return MagicPromptOptions.NO_REPEAT_PREFIX +  this.#noRepeatProperty;
    }

    noRepeatEnabled(){
        return _.notNull(this.#noRepeatProperty);
    }

    setIsAction(buttonText, isAction){
        _.find(this.#buttons, (button) => {return _.e(button.label, buttonText)}).isAction = isAction;
        return this;
    }

    applyClick(buttonText,   f){
        _.find(this.#buttons, (button) => {
            return _.e(button.label, buttonText)
        }).click = f;

        return this;
    }


    closeSelf(toRemove = false){
      this.#selfLink.hide(toRemove);
    }

    allowNoRepeat(noRepeatProperty = MagicPromptOptions.GLOBAL_CONTEXT){
        this.#noRepeatProperty = noRepeatProperty;
        return this;
    }

    constructor(title, text, buttons = null) {
        if(buttons == null){
            this.#buttons = MagicPromptOptions.defaultButtons(this);
        }else {
            this.#buttons = buttons
        }
        this.#title = title;
        this.#text = text;
    }

    newPrompt(){
        return new MagicPrompt(this);
    }


    markNoRepeat() {
        this.#selfLink.markNoRepeat();
    }
}
class MagicPrompt{


    #options
    #noRepeatCheckBox = null;



    #content = h.div('modal modal_prompt');

    constructor(options) {
        this.#options = options.setSelfLink(this);
        this.build();
    }

    hide(toRemove = false){
      if(toRemove) {
        this.#content.remove(); return
      }
      this.#content.hide();
    }

    toShow(){
        if (!this.#options.noRepeatEnabled()){
            return false;
        } else { 
                try {
                    const canShow =  PersonalSettings.getSetting(this.#options.getNoRepeatProperty(), false)
                    return canShow;
                } catch (e) {
                    return false;
                }
        }
    }

    build(){
        
        let div = h.div("prompt_window");
        let close = dom.createCloseImage().click(()=> this.#content.remove()).appendTo(div);
        h.div("prompt_box_title").text(this.#options.getTitle(), true).appendTo(div);
        h.div("prompt_text").text(this.#options.getText()).appendTo(div);

        if (this.#options.noRepeatEnabled()){
            
            let id = "_no_repeat_property_box";

            let checkboxDiv= h.div("check_box");

            this.checkboxOptions = new MagicTrueFalseSelectorOptions("global.prompt.show_prompt", false, true, false, true)
            this.checkbox = new MagicTrueFalseSelector(this.checkboxOptions).draw(checkboxDiv);

            div.add(checkboxDiv);

            if (!this.toShow()){
                checkboxDiv.hide()
            }
        }

        let footer = h.div("footer").appendTo(div);

        _.each(this.#options.getButtons(), (button) => {
            footer.add(button.toH())
        });

        this.#content.add(div).appendToBody();


    }

    markNoRepeat(){  
        if (!this.#options.noRepeatEnabled()){
            return false;
        } 
        try {
            PersonalSettings.getSetting(this.#options.getNoRepeatProperty(), false)
        } catch (e) {
            return false;
        }
        if (this.#options.noRepeatEnabled()){
            if (this.checkbox.getValue()){
                PersonalSettings.setSetting(this.#options.getNoRepeatProperty(), "enabled")
            }
        }
    }



}