/**
 *
 *
 *         https://test_server/api/v2/api_catalog/api_sub_catalog/root/fetch/list
 *         [root             ][api  ] [entry]    [sub_entry]     [request]
 *
 */

const RESPONSE_TYPES = {
    SUCCESS: "success__response",
    ERROR: "error__response"
}

class CacheProvider {


    addToCache(request, data, type) {
    };

    deleteFromCache(request) {
    };

    getFromCache(request, timeout, success, fail) {
    };

}

class LocalCacheProvider extends CacheProvider {
    static cachedItems = new Map();

    static #INSTANCE;

    static getInstance() {
        if (_.isNull(this.#INSTANCE)) {
            this.#INSTANCE = new LocalCacheProvider();
        }
        return this.#INSTANCE;
    }

    newRecord(request, timeout, success, fail, crossPage = false) {
        request.run((data) => {
            //Unmodified object should be stored instead of direct link
                new CacheInterceptor(request, structuredClone(data), timeout, crossPage, RESPONSE_TYPES.SUCCESS)
                success(data)
            }
            , (error) => {
                // new CacheInterceptor(request, error, timeout, crossPage, RESPONSE_TYPES.ERROR);
                if (fail && _.isFunction(fail)) {
                    fail(error);
                }
            }, true);
    }

    getFromCache(request, timeout, success, fail) {
        let existingPayloads = LocalCacheProvider.cachedItems.get(request.getFullPath());
        if (_.isNull(existingPayloads)) {
            log.promisedDebug(() => {
                return {"type": "fetch", "text": "no item for url", "request": request}
            })

            this.newRecord(request.fullCopy().forceNoCache(), timeout, success, fail);


        } else {
            let item = _.find(existingPayloads, (e) => {
                return _.e(e.payload, request.getPayload(), true)
            })

            if (_.isNull(item)) {

                log.promisedDebug(() => {
                    return {"type": "fetch", "text": "adding for current payload", "request": request}
                })

                this.newRecord(request.fullCopy().forceNoCache(), timeout, success, fail);

            } else {
                log.promisedDebug(() => {
                    return {"type": "fetch", "text": "USING CACHE DATA", "request": request}
                });
                if (_e(item.type, RESPONSE_TYPES.SUCCESS)) {
                    success(item.value);
                } else {
                    if (fail && _.isFunction(fail)) {
                        fail(item.value);
                    }
                }
            }
        }
    }


    addToCache(request, data, type) {


        let existingPayloads = LocalCacheProvider.cachedItems.get(request.getFullPath());
        if (_.isNull(existingPayloads)) {
            LocalCacheProvider.cachedItems.set(request.getFullPath(), [{
                "payload": request.getPayload(),
                "value": data,
                "type": type
            }])
        } else {

            let item = _.find(existingPayloads, (e) => {
                return _.e(e.payload, request.getPayload(), true)
            })

            if (_.isNull(item)) {
                existingPayloads.push({"payload": request.getPayload(), "value": data, "type": type});
                log.promisedDebug(() => {
                    return {"text": "Cached item added with new Payload", "request": request}
                })
            } else {
                log.promisedDebug(() => {
                    {
                        return {"text": "Cached item skipped as existing", "request": request}
                    }
                });
            }
        }


    }

    deleteFromCache(item) {
        //todo remove only with payload that equals current
        LocalCacheProvider.cachedItems.delete(item.getFullPath());
        log.promisedDebug(() => {
            {
                return {"text": "Deleted ", "request": item.getFullPath()}
            }
        });

    }

}

class StorageCacheDataElement {
    cacheTimeMc;
    response;

    constructor(response) {
        this.response = response;
        this.cacheTimeMc = new Date().getTime();
    }

    toString() {
        return JSON.stringify(this);
    }
}



class StorageDataTimeController {
    #response;
    #timeout;
    #cacheTimeMc;
    #defaultTimeOut = 600000;
    // сутки
    #maxTimeout = 86400000;
    #parseData;

    constructor(data, timeout) {
        this.#timeout = timeout;
        this.parseData(data);
    }

    parseData(data) {
        if (!data) {
            return;
        }
        this.#parseData = JSON.parse(data);
        this.#response = this.#parseData?.response;
        this.#cacheTimeMc = this.#parseData?.cacheTimeMc;
    }

    getResponse() {
        return this.#response;
    }

    isDataValid() {
        //data valid forever
        if (_.isNull(this.#timeout)) {
            return true;
        }

        if (!this.#cacheTimeMc && !this.#parseData) {
            return false;
        }

        const timeOut = !this.#timeout || this.#timeout < 0 || this.#timeout > this.#maxTimeout ? this.#defaultTimeOut : this.#timeout;
        const nowMc = new Date().getTime();
        return nowMc - timeOut < this.#cacheTimeMc;
    }
}

class StorageCacheProvider extends CacheProvider {

    static #INSTANCE;

    static getInstance() {
        if (_.isNull(this.#INSTANCE)) {
            this.#INSTANCE = new StorageCacheProvider();
        }
        return this.#INSTANCE;
    }

    getFromCache(request, timeout, success, fail) {
        this.validateRequest(request);
        const data = cache.get(this.getKey(request));
        const dataTimeController = new StorageDataTimeController(data, timeout);
        const isDataValid = dataTimeController.isDataValid() && _.notNull(dataTimeController.getResponse());
        if (!isDataValid) {
            request.fullCopy().forceNoCache().run((data) => {
                this.addToCache(request, data);
                success(data);
            }, (error) => {
                // this.addToCache(request, error);
                fail(error);
            })
        } else {
            const response = dataTimeController.getResponse();
            success(response);
        }
    }

    deleteFromCache(request) {
        if (_.notNull(request)) {
            cache.remove(this.getKey(request))
            return
        }
        log.promisedDebug(() => {
            return {
                data: "We can not delete current object from cache",
                object: this
            }
        })
    }

    addToCache(request, data) {
        this.validateRequest(request);
        const dataElement = new StorageCacheDataElement(data);
        cache.add(this.getKey(request), dataElement.toString());
    }

    getKey(request) {
        if (_.isNull(request.getPayload())) {
            return request.getFullPath();
        } else {
            return request.getFullPath() + "_" + JSON.stringify(request.getPayload())
        }
    }

    validateRequest(request) {
        if (!_.isNull(request.getPayload())) {
            log.promisedDebug(() => {
                return {"text": "wrong usage of localstorage, this might be a problem to use localstorage with payload"}
            })
        }
    }
}

class CacheInterceptor {

    #payload;
    #path;
    #value;
    #provider;


    constructor(request, value, timeout = 600000, crossPage = false, type = RESPONSE_TYPES.SUCCESS) {

        if (_.isNull(request)) {
            log.promisedDebug(() => {
                return {"text": "Cached item saved as newly came request", "request": request}
            })
            return null;

        }
        let provider = !crossPage ? new LocalCacheProvider() : new StorageCacheProvider();
        this.#payload = request.getPayload();

        provider.addToCache(request, value, type);

        this.#path = request.getFullPath();

        let _this = this;
        if (timeout != null) setTimeout(() => {
            provider.deleteFromCache(request)
            _this.destroy()
        }, timeout);


    }

    getPath() {
        return this.#path;
    }

    static fetch(request, timeout, success, fail, crossPage = false) {
        if (_.isNull(request)) {
            log.promisedDebug(() => {
                return {"text": "request should not be null"}
            })
            return null;
        }

        let provider = !crossPage ? LocalCacheProvider.getInstance() : StorageCacheProvider.getInstance();
        provider.getFromCache(request, timeout, success, fail);

    }


    destroy() {
        this.#path = null;
        this.#payload = null;
        this.#value = null;
        log.promisedDebug(() => {
            {
                return {"text": "Destroyed object"}
            }
        });
    }
}

class ServerRequestInterval {
    static REPEAT_LIMIT_NOT_SET = -1;
    #count = 0;
    #timeout;
    #limit;

    constructor(timeout, limit = ServerRequestInterval.REPEAT_LIMIT_NOT_SET) {
        this.#timeout = timeout
        this.#limit = limit
    }

    planRerun(request, success, fail){

        if (++ this.#count > this.#limit && !_e(this.#limit, ServerRequestInterval.REPEAT_LIMIT_NOT_SET)){
            return;
        }

        setTimeout( () => {
            request.run(success, fail)
        }, this.#timeout)
    }


}

class ServerRequest {

    static GET_METHOD = "GET";
    static POST_METHOD = "POST";
    static PUT_METHOD = "PUT";
    static DELETE_METHOD = "DELETE";
    static CONTENT_TYPE = "Content-Type";


    #root = Environment.dataApi;
    #apiPath = "api/v2";

    #entry;

    #fullPath;
    #subEntry;
    #crossPageCache = false;

    #request;
    #forceNoCache = Environment.test;

    #method = Executor.GET_METHOD;

    #scope = Environment.authScope;

    #payLoad;
    #async = true;

    //clone is needed here for multi scoped requests.
    #headers = new Map();
    #cacheTimeOut = 6000000;


    /**
     * Here to change payload on search.
     * Required for now to be here because we need to pass it to the RemoteChooser
     */
    #limitQuery;

    #repeat;

    setLimitQuery(query) {
        if (_.isNull(query)) {
            this.#limitQuery = null;
            return this;
        }
        if (!query instanceof Query) {
            throw new Error("Query should be instance of Query class")
        }
        this.#limitQuery = query;
        return this;
    }

    eq(request) {
        return utils.e(this.getFullPath(), request.getFullPath()) && utils.e(this.#payLoad, request.getPayload())
    }

    getPayload() {
        return this.#payLoad
    }

    getSubEntry() {
        return this.#subEntry;
    }

    forceNoCache(force = true) {
        this.#forceNoCache = force;
        return this;
    }

    subEntry(s) {
        this.#fullPath = null;
        this.#subEntry = s;
        return this;
    }

    getCacheTimeout() {
        return this.#cacheTimeOut;
    }

    scope(s) {
        this.#scope = s;
        return this;
    }

    /**
     * Adds a header, except content type. to set content type override method #getContentType or use PlainRequest
     * @param header
     * @param value
     */
    addHeader(header, value) {
        this.#headers.set(header, value)
    }

    headers(h) {
        this.#headers = h;
        return this;
    }

    async(a) {
        this.#async = a;
        return this;
    }

    method(m) {
        this.#method = m;
        return this;
    }

    payload(p) {
        this.#payLoad = p;
        return this;
    }

    cacheResponse(cacheTimeOut = null, crossPage = false) {
        this.#cacheTimeOut = cacheTimeOut;
        this.#crossPageCache = crossPage;
        return this;
    }

    isCrossPage() {
        return this.#crossPageCache;
    }

    request(r) {
        this.#fullPath = null;
        this.#request = r;
        return this;
    }

    entry(e) {
        this.#fullPath = null;
        this.#entry = e;
        return this;
    }

    api(a) {
        this.#fullPath = null;
        this.#apiPath = a;
        return this;
    }

    root(r) {
        this.#fullPath = null;
        this.#root = r;
        return this;
    }

    getRequest() {
        return this.#request;
    }

    getRepeat() {
        return this.#repeat;
    }

    enableRepeat(repeat) {
        if (! (repeat instanceof ServerRequestInterval)){
            throw Error("repeat should be instance of ServerRequestInterval")
        }
        this.#repeat = repeat
        return this.forceNoCache();
    }

    /**
     * Indicates which scope is to be used in this request
     * @returns {string} by default Environment.authScope;
     */
    getRequiredScope() {
        return this.#scope
    }

    getScope() {
        return this.#scope;
    }

    copy(destroy, newURI) {

        let request = this.getClonedObject(this)
            .root(this.#root).async(this.#async).scope(this.#scope).payload(null)
            .subEntry(this.#subEntry).entry(this.#entry).request(newURI).method(this.#method)
            .setLimitQuery(this.#limitQuery)
            .api(this.#apiPath).headers(this.#headers).cacheResponse(this.#cacheTimeOut);

        if (this.#forceNoCache) {
            request.forceNoCache();
        }

        if (destroy) {
            this.request(null).api(null).method(null).headers(null)
                .request(null).subEntry(null).payload(null).scope(null);
        }
        return request;
    }

    fullCopy() {
        return this.copy(false, this.getRequest()).payload(this.getPayload())
    }

    getForceNoCache(){
        return this.#forceNoCache
    }

    getClonedObject(obj) {
        return new obj.constructor();
    }


    getHeaders() {
        return Object.fromEntries(this.#headers);
    }

    //temp
    countFromPage(request){
        this.async(false)

        request.limit = 1;
        let count = 0

        this.payload(request).search(this.getPayload(), (data)=> {count = data.totalElements }, ()=>{} )
        return count
    }

    sendFile(file, success, fail) {

        const bindObject = {
            request: this,
            success: success,
            fail: fail
        }
        var formData = new FormData();

        formData.set('file', file);

        var xhr = new XMLHttpRequest();
        xhr.open(Executor.POST_METHOD, this.getFullPath().cleanUrl(), this.#async);

        xhr.onreadystatechange = () => {
            var res = xhr.responseText;

            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    if(res){
                        var data = JSON.parse(res)
                        this.successFunction.bind(bindObject)(success)(data);
                    } else{
                        this.successFunction.bind(bindObject)(success)({});
                    }
                } else {
                    if (fail) {
                        this.failFunction.bind(bindObject)(fail)(xhr.responseText, xhr.status);
                    }
                }
            }
        }

        Executor.setRequestHeaders(xhr, this.getHeaders());
        Executor.prepareAuth(xhr);

        xhr.send(formData);

    }

    run(success, fail = null) {

        const bindObject = {
            request: this,
            success: success,
            fail: fail
        }
        if (!this.#forceNoCache) {
            CacheInterceptor.fetch(this, this.#cacheTimeOut, success, fail, this.#crossPageCache);
            return;
        }

        /**
         * since v2
         */
        this.presetHeaders(this.#headers);

        if (this.#payLoad) {

            if (_.e(this.#method, Executor.POST_METHOD)) {
                this.preExecute();
                Executor.runPostWithPayload(this.getFullPath(), this.successFunction.bind(bindObject)(success), this.#payLoad, this.failFunction.bind(bindObject)(fail), this.getHeaders(), this.#async)
            } else {
                this.preExecute()
                Executor.runPutWithPayload(this.getFullPath(), this.successFunction.bind(bindObject)(success), this.#payLoad, this.failFunction.bind(bindObject)(fail), this.getHeaders(), this.#async)

            }
        } else {
            this.preExecute();
            Executor.run(this.getFullPath(), this.#method, success, this.#async, this.failFunction.bind(bindObject)(fail), this.getHeaders());
        }

    }

    //We have to preset headers before, because we might be using multi scopes, not central is it was before
    //in v1 we have been relaying on authScope property of Environment class.
    //since v2. we relay only on current request authorization
    presetHeaders(setContentType = true) {
        this.addHeader("AUTH_SCOPE", this.getRequiredScope());
        this.addHeader("Authorization", "Bearer " + this.getAuthorization().getSessionToken())
        if (setContentType) {
            this.addHeader("Content-type", this.getContentType())
        }
        return this
    }

    //Since v2 content type is a function to be overridden if not JSON is required
    getContentType() {
        return "application/json"
    }

    preExecute() {
        if (Environment.showRequests) {
            log.promisedDebug(() => {
                {
                    return {"text": 'Running request', "request": this}
                }
            })
        }
    }


    //Authorization is needed to select scope on the required run
    getAuthorization() {
        return new Authorization().setScope(this.getRequiredScope());
    }
 
    failFunction(fail) {

        return (error, status) => {
            if (!this.request.verifyError(error, status) && _.notNull(fail)) {
                fail(error);
            }

            if (this.request.getRepeat()){
                this.request.getRepeat().planRerun(this.request, this.success, this.fail)
            }

        }

    }

    //Since v2 handling the problems on request run id delegated to request itself
    verifyError(error, status) {
        log.promisedDebug(() => {
            return {"error": error, "status": status, 
                "path": this.getFullPath(), "request": this
            }
        })
        return this.handleNoAuthorization(error, status) & this.handleTokenProblem(error, status) & this.handleOther(error, status);
    }

    handleTokenProblem(error, status) {
        return false;
    }

    handleNoAuthorization(error, status) {
        return false;
    }

    handleOther(error, status) {
        return false
    }

    successFunction(success) {
        if (!this.request.getCacheTimeout() || !this.request.getPayload()) {
            return success;
        }

        return (data) => {
            success(data);
            if (this.request.getRepeat()){
                this.request.getRepeat().planRerun(this.request, this.success, this.fail)
            }
        }
    }

    shared(subEntry) {
        return this.api("api/v2").entry("shared").subEntry(subEntry);
    }

    getFullPath() {
        if (this.#fullPath != null) {
            return this.#fullPath;
        }
        this.#fullPath = this.#root + "/" + this.#apiPath + "/" + this.#entry
        if (_.notNull(this.#subEntry)) {
            this.#fullPath += "/" + this.#subEntry
        }
        if (_.notNull(this.#request)) {
            this.#fullPath += "/" + this.#request
        }
        return this.#fullPath;
    }

    getUniqueId() {
        return this.#apiPath + this + "entry" + this.#subEntry;
    }

    search(magicRequest, success, fail) {
        if (_.notNull(this.#limitQuery)) {
            magicRequest.query.push(this.#limitQuery.toQueryObject())
        }
        this.payload(magicRequest).headers(this.#headers).method(ServerRequest.POST_METHOD).request("root/fetch/data").run(success, fail)
    }

    searchWithoutRun(magicRequest) {
        return this.payload(magicRequest).headers(this.#headers).method(ServerRequest.POST_METHOD).request("root/fetch/data")
    }

    formFetch(id, success, fail) {
        this.request("root/fetch/form" + (_.isNull(id) ? "" : "?id=" + id)).method(ServerRequest.GET_METHOD).run(success, fail);
    }

    formFetchWithoutRun(id, success, fail) {
        return this.request("root/fetch/form" + (_.isNull(id) ? "" : "?id=" + id)).method(ServerRequest.GET_METHOD);
    }

    fetchData() {
        return this.request("root/fetch/data").method(ServerRequest.GET_METHOD)
    }

    listFetch(success, fail) {
        this.request("root/fetch/list").method(ServerRequest.GET_METHOD).cacheResponse(600000, true).run(success, fail);
    }

    listFetchWithoutRun() {
        return this.request("root/fetch/list").method(ServerRequest.GET_METHOD)
    }

    byId(id, success, fail) {
        this.request("id/" + id).run(success, fail);
    }

    byIdWithoutRun(id, success, fail) {
        return this.request("id/" + id);
    }

    download(payLoad, success, fail) {
        this.payload(payLoad).forceNoCache().request("/root/download/data").method(ServerRequest.POST_METHOD).run(success, fail);
    }

    import(success, fail) {
        this.request("/import").forceNoCache().method(ServerRequest.POST_METHOD).run(success, fail);
    }

    fetchDetails(id, success, fail) {
        this.request("/root/fetch/details/" + id).method(ServerRequest.GET_METHOD).run(success, fail)
    }

    list(success, fail) {
        this.request("list").method(ServerRequest.GET_METHOD).run(success, fail);
    }


    add(payload, success, fail) {
        this.fullCopy().request("add").forceNoCache().method(ServerRequest.POST_METHOD).payload(payload).run(ServerRequest.successWrapper.bind({
            "request": this,
            "success": success,
            "id": null
        }), fail);
    }

    edit(payload, success, fail) {
        this.fullCopy().request("edit").forceNoCache().method(ServerRequest.PUT_METHOD).payload(payload).run(ServerRequest.successWrapper.bind({
            "request": this,
            "success": success,
            "id": payload.id
        }), fail);
    }

    delete(id, success, fail) {
        this.fullCopy().request("delete/" + id).method(ServerRequest.DELETE_METHOD).run(ServerRequest.successWrapper.bind({
            "request": this,
            "success": success,
            "id": id
        }), fail);
    }

    searchType(type, success, fail) {
        this.api("api/v1").entry("/search").subEntry("/type/").request(type + "?s=").method(Executor.GET_METHOD).run(success, fail);
    }

    deleteItems(ids, success, fail) {
        this.fullCopy().request("delete/deleteItems?ids=" + ids).method(ServerRequest.DELETE_METHOD).run(ServerRequest.successWrapper.bind({
            "request": this,
            "success": success,
            "id": ids
        }), fail);
    }

    /**
     * @deprecated as duplicated method byId
     * @param id
     * @param success
     * @param fail
     */
    getById(id, success, fail) {
        this.request("get/" + id).method(ServerRequest.GET_METHOD).run(success, fail);
    }


    /**
     * After edit/add/delete, we need to clear the cache, that's why this method is here
     * @param data - a success data
     *
     * @binded request: ServerRequest, success:success, id: objectId
     */
    static successWrapper(data) {
        let provider = !this.request.isCrossPage() ? LocalCacheProvider.getInstance() : StorageCacheProvider.getInstance();
        provider.deleteFromCache(this.request.copy().listFetchWithoutRun());
        provider.deleteFromCache(this.request.copy().searchWithoutRun());
        provider.deleteFromCache(this.request.copy().byIdWithoutRun(this.id));
        this.success(data);
    }

    clear(id){
        LocalCacheProvider.getInstance().deleteFromCache(this.copy().searchWithoutRun())
        LocalCacheProvider.getInstance().deleteFromCache(this.copy().fetchData())
        if (id) {
            LocalCacheProvider.getInstance().deleteFromCache(this.copy().request("id/", id ))
        }
    }
}

class PlainRequest extends ServerRequest {
    getContentType() {
        return "text/plain"
    }
}
