class Authorization {

    static TOKEN_FIELD = "___TOKEN___";
    static ID_FIELD = "___id";
    static LOGIN_FIELD = "___login";
    static EMAIL_FIELD = "___email";
    static NAME_FIELD = "___name";
    static PHONE_FIELD = "___phone";
    static ADDRESS_FIELD = "___address";
    static IMAGE_FIELD = "___image";
    static DUE_FIELD = "___due";
    static AUTH_SCOPE = "___auth_scope"

    static LOGIN_URL = Environment.self + "/client_login";
    static API_LOGOUT_URL = Environment.authApi + "/api/v3/auth/logout";
    static API_LOGIN_URL = Environment.authApi + "/api/v3/auth/login";

    static AUTH_HEADERS = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    static login(login, password, twoFactor, storeData, error = null, success = null, scope) {

        var payload = {
            'login': login.trim(),
            'twoFactorCode': twoFactor,
            'password': password.trim()
        }

        Authorization.setErrorLogin(false);
        Executor.runPostWithPayload(this.API_LOGIN_URL, (data) => {
            Authorization.processLoginData(data.dto, storeData)
            if (success) {
                success();
                return;
            }

            let redirect = MagicPage.getPageParam("r");
            if (redirect == null ) {
                redirect = Environment.self;
            }
            dom.redirect(redirect, false, false )

        }, payload, (e) => {
            Authorization.setErrorLogin(true);
            if(error) {
              error(e); return;
            }
            dom.toast(_.$("pages.login.login_error"), ToastType.ERROR);
        }, Authorization.AUTH_HEADERS)
    }

    static processLoginData(data, storeData) {
        Authorization.storeUserData(data);
        if (storeData) {
            Authorization.saveLoginDataToLocalStorage(data);
        }
    }

    static isNotRegisterLoginOrReset() {
        let path = location.protocol + '//' + location.host + location.pathname;
        return (

            path.indexOf("/client_login") == -1 &&
            path.indexOf("/restore") == -1 &&
            path.indexOf("/restore") == -1 &&
            path.indexOf("/register") == -1
        );
    }

    static verifyPage(toReLogin) {
      
        toReLogin = toReLogin && Environment.isNotRegisterLoginOrReset();

        Authorization.reloadSessionFromCache();

        if (Authorization.processCustomVerification()) {
            return;
        }

        if (Authorization.noToken()) {
            Authorization.logout(toReLogin);
        }

        if (Authorization.tokenOverDue()) {
            Authorization.logout(toReLogin);
        }
    }

    static setErrorLogin(isError) {

        try {
            if (isError) {
                get('login').classList.add("error");
                get('password').classList.add("error");
                get('twoFactor').classList.add("error");
            } else {
                get("login")?.classList.remove("error");
                get("password")?.classList.remove("error");
                get("twoFactor")?.classList.remove("error");
            }
        } catch (e) {
            console.error(e);
        }
    }


    static storeUserData(data) {
        if (_.isNull(data.token)){
            throw Error();
        }
        sessionStorage.setItem(Authorization.TOKEN_FIELD, data.token);
        sessionStorage.setItem(Authorization.ID_FIELD, data.userData.id);
        sessionStorage.setItem(Authorization.LOGIN_FIELD, data.userData.login);
        sessionStorage.setItem(Authorization.EMAIL_FIELD, data.userData.email);
        sessionStorage.setItem(Authorization.NAME_FIELD, data.userData.name);
        sessionStorage.setItem(Authorization.PHONE_FIELD, data.userData.phone);
        sessionStorage.setItem(Authorization.ADDRESS_FIELD, data.userData.address);
        sessionStorage.setItem(Authorization.IMAGE_FIELD, data.userData.imagePath);
        sessionStorage.setItem(Authorization.DUE_FIELD, data.due);
    }

    static saveLoginDataToLocalStorage(data) {
        cache.add(Authorization.TOKEN_FIELD, data.token);
        cache.add(Authorization.ID_FIELD, data.userData.id);
        cache.add(Authorization.LOGIN_FIELD, data.userData.login);
        cache.add(Authorization.EMAIL_FIELD, data.userData.email);
        cache.add(Authorization.NAME_FIELD, data.userData.name);
        cache.add(Authorization.PHONE_FIELD, data.userData.phone);
        cache.add(Authorization.ADDRESS_FIELD, data.userData.address);
        cache.add(Authorization.IMAGE_FIELD, data.userData.imagePath);
        cache.add(Authorization.DUE_FIELD, data.due);
    }

    static reloadSessionFromCache() {
        if (_.isNull(cache.get(Authorization.TOKEN_FIELD))) {return};
        Authorization.copyField(Authorization.TOKEN_FIELD);
        Authorization.copyField(Authorization.ID_FIELD);
        Authorization.copyField(Authorization.LOGIN_FIELD);
        Authorization.copyField(Authorization.EMAIL_FIELD);
        Authorization.copyField(Authorization.PHONE_FIELD);
        Authorization.copyField(Authorization.TOKEN_FIELD);
        Authorization.copyField(Authorization.ADDRESS_FIELD);
        Authorization.copyField(Authorization.IMAGE_FIELD);
        Authorization.copyField(Authorization.DUE_FIELD);
    }

    static copyField(what) {
        sessionStorage.setItem(what, cache.get(what))
    }

    static processCustomVerification() {
        try {
            return customVerifyPage();
        } catch (ex) {
            return false;
        }
    }

    static noToken() {
        if (!Authorization.isNotRegisterLoginOrReset()) return false;
        return sessionStorage.getItem(Authorization.TOKEN_FIELD) === undefined || sessionStorage.getItem(Authorization.TOKEN_FIELD) == null;
    }

    static tokenOverDue() {
        let time = new Date().getTime()
        let due = parseInt(sessionStorage.getItem(Authorization.DUE_FIELD));
        return due < time;
    }

    static logout(remote, headers = Executor.HEADERS, success, error) {
        if (remote) {
            Executor.runPostWithFailFunction(Authorization.API_LOGOUT_URL, (e)=>{
                
            if (success) {
                success();
                return;
            }
            redirect(e);

            }, (e)=>{
                if (error) {
                    error();
                    return;
                }
                redirect(e)
            }, true, headers)
        }

        clear();
        
        function redirect(e){
          clear();
            dom.redirect(Authorization.LOGIN_URL, false, false);
          return;
        }

        function clear() {
          sessionStorage.clear();
          cache.removeAll(Authorization.TOKEN_FIELD, Authorization.DUE_FIELD,
            Authorization.IMAGE_FIELD, Authorization.ADDRESS_FIELD, Authorization.PHONE_FIELD, Authorization.NAME_FIELD,
            Authorization.EMAIL_FIELD, Authorization.PHONE_FIELD, Authorization.LOGIN_FIELD, Authorization.AUTH_SCOPE)
        }
    
    }
}