class DataProviderItem {
    key;
    value;
    origin;

    constructor(key, value, origin) {
        this.key = key;
        this.value = value;
        this.origin = origin;
    }

    _equals(el) {
        if (_.isNull(el)) {
            return false;
        }
        return _e(el.key, this.key);
    }
}

class AbstractDataProvider {
    getData(listener) {
    }

    search(text, listener) {
    }

    byValue(value) {

    }
}

/**
 * Provides data for the combobox
 */
class LocalDataProvider extends AbstractDataProvider {
    #items = [];
    #noValueItem

    static of(items) {
        let provider = new LocalDataProvider();
        _.each(items, (item) => {
            provider.validateEntry(item);
            provider.addItem(item.key, item.value)
        })
        return provider;
    }

    constructor(items = []) {
        super();
        items = _.ofNullable(items, () => []);
        _.each(items, (item) => {
            this.validateEntry(item)
            this.addItem(item.key, item.value);
        })
    }

    getNoValueItem() {
        return this.#noValueItem
    }

    setNoValueItem(item) {
        this.validateEntry(item)
        this.#noValueItem = item
        return this
    }

    validateEntry(item) {
        if (_.isNull(item.key) || _.isNull(item.value)) {
            throw new Error('Item should have key/value structure')
        }
    }

    addItem(key, value) {
        this.#items.push(new DataProviderItem(key, value));
    }

    getData(listener) {
        if (this.#noValueItem) {
            this.#items.unshift(this.#noValueItem);
        }
        return listener(this.#items);
    }

    getItems() {
        return this.#items
    }

    search(text, listener) {
        return listener(this.#items.filter((item, value) => {
            if (this.#noValueItem && _e(item.value, this.#noValueItem.value)) {
                return true
            }
            return item.key.indexOf(text) > -1
        }))
    }

    byValue(value) {
        return _.find(this.#items, (item) => {
            return _e(item.value, value)
        })
    }
}

/**
 * Remote data provider executes request t get data, instead of storing data locally
 */
class RemoteDataProvider extends AbstractDataProvider {
    #request;
    #keyField;
    #keyValue;
    #payload;
    #filterFn = null
    #noValueItem = null

    #loadedData = [];
    static #ID_FIELD = "id"
    #dataContentFetcher;

    /**
     * Request should be of type ServerRequest
     * @param request - a server request to query data
     * @param keyField - a field where to take a label
     * @param magicRequest - request to filter or limit data
     * @param keyValue - a field where to take value, normaly ID
     * @param dataContentFetcher when data is recieved sometimes the true data is located in special field, for example in case of pagination, the data is located in content directory
     */
    constructor(request, keyField, magicRequest = new MagicRequest(), keyValue = RemoteDataProvider.#ID_FIELD, dataContentFetcher = RemoteDataProvider.defaultDataContentFetcher) {
        if (!request instanceof ServerRequest) {
            log.promisedDebug(() => {
                return {"text": "request should be of type ServerRequest", "present": request}
            })
            return;
        }
        super();
        this.#request = request.copy().async(false);
        this.#keyField = keyField;
        this.#keyValue = keyValue;
        this.#payload = magicRequest;
        this.#dataContentFetcher = dataContentFetcher;

    }

    static defaultDataContentFetcher(data) {
        return data.content;
    }

    getData(listener) {
        //todo to remove only required case
        this.#payload.query = [];

        if (this.#noValueItem) {
            this.#loadedData.push(this.#noValueItem)
        }

        this.#request.search(this.#payload, (data) => {
            _.each(this.#dataContentFetcher(data), (item) => {
                if (_.notNull(this.#filterFn)) {
                    if (this.#filterFn(item)) {
                        this.#loadedData.push(new DataProviderItem(item[this.#keyField], item[this.#keyValue], item))
                    }
                } else {
                    this.#loadedData.push(new DataProviderItem(item[this.#keyField], item[this.#keyValue], item))
                }
            });
            listener(this.#loadedData);
        })
    }

    getItems() {
        return this.#loadedData
    }

    setFilterFn(filterFn) {
        this.#filterFn = filterFn
        return this
    }

    getNoValueItem() {
        return this.#noValueItem
    }

    setNoValueItem(item) {
        this.#noValueItem = item
        return this
    }

    //Search in this case is not only fetching data, but storing loaded data to easy access on "byKey" function
    search(text, listener) {
        this.#payload.query = [];
        this.#payload.query.push(Query.contains(this.#keyField).value(text).toQueryObject())

        let toReturn = [];
        this.#request.fullCopy().forceNoCache().search(this.#payload, (res) => {
            if (this.#noValueItem) {
                toReturn.push(this.#noValueItem);
            }
            _.each(this.#dataContentFetcher(res), (item) => {
                if (_.notNull(this.#filterFn)) {
                    if (this.#filterFn(item)) {
                        if (!this.#loadedData.contains(item)) {
                            this.#loadedData.push(this.toProviderItem(item))
                        }
                        toReturn.push(this.toProviderItem(item))
                    }
                } else {
                    if (!this.#loadedData.contains(item)) {
                        this.#loadedData.push(this.toProviderItem(item))
                    }
                    toReturn.push(this.toProviderItem(item))
                }

            })
            listener(toReturn);
        })
    }

    toProviderItem(item) {
        return new DataProviderItem(item[this.#keyField], item[this.#keyValue], item);
    }

    byValue(value) {
        return _.find(this.#loadedData, (item) => {
            return _e(item.value, value)
        })
    }
}


class MagicDropDownOptions {

    static SELECT_FIRST = "___first"
    static SELECT_NONE = null;
    #listeners = [];
    #itemRenderer;
    #tagRenderer;
    #isMultiSelect = false;
    #isSearchEnabled = true;
    #isDeleteTagButtonEnabled = true
    #title = "";

    #preselectedValue = MagicDropDownOptions.SELECT_FIRST;

    #provider;

    /**
     * Data provider allows to fetch data according to the params
     * Data provider should extend AbstractDataProvider
     * @param dataProvider
     */
    constructor(dataProvider = new LocalDataProvider()) {
        this.#provider = dataProvider;
    }

    /**
     * Manipulates data. add or remove data you can here
     * @returns {*}
     */

    getIsDeleteTagButtonEnabled() {
        return this.#isDeleteTagButtonEnabled
    }

    setIsDeleteTagButtonEnabled(value) {
        this.#isDeleteTagButtonEnabled = value;
        return this
    }

    getTitle() {
        if (this.#title) {
            return this.#title
        }
    }

    setTitle(title) {
        this.#title = title;
        return this;
    }


    getDataProvider() {
        return this.#provider;
    }

    addListener(listener) {
        if (_.notNull(listener)) {
            this.#listeners.push(listener);
        }
        return this
    }

    getPreSelectedValue() {
        return this.#preselectedValue
    }

    setTagRenderer(renderer) {
        this.#tagRenderer = renderer;
        return this;
    }

    getTagRenderer() {
        return this.#tagRenderer;
    }

    setPreSelectedValue(id) {
        this.#preselectedValue = id;
        return this
    }

    enableMultiSelect() {
        this.#isMultiSelect = true;
        return this
    }


    isMultiSelect() {
        return this.#isMultiSelect;
    }

    setItemRenderer(renderer) {
        this.#itemRenderer = renderer;
        return this;
    }

    getItemRenderer() {
        return this.#itemRenderer;
    }

    setSearchDisabled() {
        this.#isSearchEnabled = false;
        return this;
    }

    setIsSearhEnabled(value) {
        this.#isSearchEnabled = value
        return this
    }

    getIsSearchEnabled() {
        return this.#isSearchEnabled;
    }

    fireEvent(name, data) {
        _.each(this.#listeners, (listener) => {
            listener(new DropDownEvent(name, data))
        })
    }

    setTitleRenderer(renderer) {
        if (!(renderer instanceof DropDownTitleRenderer)) {
            throw new Error("Title renderer should be a subclass of TitleRenderer")
        }
        this.#titleRenderer = renderer;

        return this;
    }

    /**
     * Title renderer is used to generate title of dropdown.
     * it will be autobinded to the DropDown object
     * param of the method isUpdate -> to render (when  or rerender component
     * */
    #titleRenderer;

    getTitleRenderer() {
        return _.ofNullable(this.#titleRenderer, () => DefaultDropDownTitleRenderer.INSTANSE)
    }


}

/**
 * @interface cannot bee used as instance
 * To render or update rendered object
 */

class DropDownTitleRenderer {


    /**
     * Executed when title is just generated
     * @param dropDown
     */
    renderTitle(dropDown) {
    };

    /**
     * executed when selection is changed
     * @param dropDown
     */
    updateTitle(dropDown) {
    };

}

class DefaultDropDownTitleRenderer extends DropDownTitleRenderer {

    static  INSTANSE = new DefaultDropDownTitleRenderer();

    renderTitle(dropDown) {
        if (!dropDown.getOptions().getTitle()) {
            return h.span("empty_title");
        }
        h.div('title_container')._text(dropDown.getOptions().getTitle())
    }

}

/**
 * Don't use instance here because counter is a specific field!
 */
class DropDownCountedTitle extends DropDownTitleRenderer {
    #counter;

    renderTitle(dropDown) {
        if (!dropDown.getOptions().getTitle()) {
            return h.span("empty_title");
        }
        return h.div('title_container')
            .add(h.span("title_box")._text(this.formatTitle(dropDown.getOptions().getTitle(), dropDown)))
            .add(this.#counter = h.span("title_counter")._text(this.formatCounter(dropDown.getSelectedItems()?.length || 0, dropDown)))
    }


    updateTitle(dropDown) {
        //by default do nothing, we just happy it was updated
        this.#counter._text(this.formatCounter(dropDown.getSelectedItems().length, dropDown))
    }

    formatCounter(counter, dropDown) {
        if (_e(counter, 0)) {
            return "";
        }
        return " (" + counter + ") ";
    }

    formatTitle(title, dropDown) {
        return title;
    }


}

class MagicDropDown {
    static DD_VALUE_ATTR = "value";
    static DD_VALUE_NOTHING = null;
    static SELECTED_VALUES_DIV_CLASS = "selected_tags"
    static SELECTED_ITEM_CLASS = "selected"
    #options;

    #selectedItems = [];
    #dropdownValuesView;
    #ddItems;
    #isDropdownOpened = false
    #ddContainer
    #tagContainer
    #isFirstRender = true
    #searchInput



    getValues() {
        return this.#dropdownValuesView;
    }

    getItems() {
        return this.#ddItems;
    }

    constructor(options = new MagicDropDownOptions()) {
        this.#options = options;
    }

    getSearchInput() {
        return this.#searchInput
    }

    toggleDropdown(value) {
        this.#isDropdownOpened = value

        if (this.#isDropdownOpened && this.#ddContainer.ccl('hidden')) {
              this.#ddContainer.rcl('hidden')
              dom.linkTo(this.#ddItems, this.#dropdownValuesView);
              document.addEventListener('click', this.handleOutsideClick.bind(this));
          }else{
              this.#ddContainer.cl('hidden')
              document.addEventListener('click', this.handleOutsideClick.bind(this));
        }
    }

    handleOutsideClick(event) {
            const dropdownElement = this.#dropdownValuesView.get();
            const dropdownItemsElement = this.#ddContainer.get();

            if (!dropdownElement.contains(event.target) && !dropdownItemsElement.contains(event.target)) {
                this.toggleDropdown(false);
            }
    }


    getSelectedItems() {
        return this.#selectedItems;
    }

    resetSelectedItems(value) {
        if (_.notNull(value)) {
            this.#selectedItems = [value];
        } else {
            this.#selectedItems = [];
        }
    }

    getOptions() {
        return this.#options;
    }

    fromData(data, keyField, valueField) {
        _.each(data, (item, index) => {
            this.addItem(item[keyField], item[valueField]);
        })
    }

    performSearch(e) {
        this.getOptions().getDataProvider().search(e.value, (data) => {
            this.drawSelection(data)
        })
    }


    build() {
        this.#ddContainer = h.div('drop_down_container').clIf(this.getOptions().isMultiSelect(), "multi").hide()
        this.#ddItems = h.div("drop_down_values").clIf(this.getOptions().isMultiSelect(), "multi")
        this.#dropdownValuesView = h.div("drop_down").click(() => this.toggleDropdown(true))

        this.#tagContainer = h.div(MagicDropDown.SELECTED_VALUES_DIV_CLASS)

        const headerContainer = h.div('drop_down_header')
            .add(this.getOptions().getTitleRenderer().renderTitle(this))
            .add(this.#tagContainer)

        this.#dropdownValuesView
            .add(headerContainer)
            .add(this.#ddContainer)
            .get().style.position = 'relative'

        this.getOptions().fireEvent(DropDownEvent.DD_EVENT_RENDERED, {
            "selectedValuesElement": this.#dropdownValuesView,
            searchElement: this.#ddItems,
            "provider": this.getOptions().getDataProvider()
        })

        if (this.getOptions().getIsSearchEnabled()) {
            this.#searchInput = h.input("text").cl("search_text")
                .onKey(this.performSearch.bind(this)
                )
            this.#ddContainer.add(this.#searchInput)
        }
        this.#ddContainer.add(this.#ddItems);

        this.getOptions().getDataProvider().getData(this.drawSelection.bind(this))

        return {values: this.#dropdownValuesView, items: this.#ddItems};

    }

    drawSelection(data) {


        let preselectedValueSet = false;
        this.#ddItems.text(null);
        _.each(data, (item, index) => {

            let render = _.ofNullable(this.getOptions().getItemRenderer(), () => MagicDropDown.itemRenderer).bind(this)

            var rendered = this.bindSelectClick(render(item, index), item, index)

            this.#ddItems.add(rendered)

            //we set preselected value if it was before //todo Here we should do multiple preselected values if needed

            if (_e(item.value, this.getOptions().getPreSelectedValue()) && this.#isFirstRender) {
                MagicDropDown.itemSelected.bind({"dd": this, "item": item, "renderer": rendered})(item.value, item);
                preselectedValueSet = true;
            }

            if (_e(index, 0) && _e(MagicDropDownOptions.SELECT_FIRST, this.getOptions().getPreSelectedValue()) && this.#isFirstRender) {
                MagicDropDown.itemSelected.bind({"dd": this, "item": item, "renderer": rendered})(item.value, item);
                this.getOptions().setPreSelectedValue(item.value);
                preselectedValueSet = true;
            }
        })
        this.#isFirstRender = false;
    }

    //@bind to the MagicDropDown itself
    static itemRenderer(item, index) {
        return h.div("dd_item").setData(MagicDropDown.DD_VALUE_ATTR, item.value).text(item.key, false);
    }

    bindSelectClick(element, item, index) {
        let bindedClick = () => {
            MagicDropDown.itemSelected.bind({"dd": this, "item": item, "renderer": element})(item.value, item);
            if (!this.getOptions().isMultiSelect()) {
                this.toggleDropdown(false)
            }
        };
        element.click(bindedClick);

        log.promisedDebug(() => {
            return {"text": "item_rendered", "item": item, "index": index, "DD": this}
        })
        this.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_RENDERED, {
            "element": element,
            "value": item,
            "index": index,
            "DD": this
        })
        return element;
    }

    selectRenderer(isSelected, renderer) {
        let tagRendererFunction = _.ofNullable(this.getOptions().getTagRenderer(), () => MagicDropDown.createTag).bind(this);

        //clean tags area
        let tags = this.#tagContainer.text(null);

        if (_.isEmpty(this.#selectedItems ) && this.getOptions().getDataProvider().getNoValueItem()){
            //select no value item
            tags.add(tagRendererFunction(this.getOptions().getDataProvider().getNoValueItem()))
        }

        _.each(this.#selectedItems, (id) => {
            let item = this.getOptions().getDataProvider().byValue(id)
            tags.add(tagRendererFunction(item))
        })
    }

    //@bind to  the MagicDropDown
    static createTag(item) {
        if (_.isNull(item)) {
            log.promisedDebug(() => {
                return {"text": "passed null item element to create tag, we are not able to build tag"}
            })
            return h.div("not_parsable_tag")._text("not_parsable_tag");
        }
        const deleteTagButton = h.img("rm.png").cl("search_tag_remove").click(() => {
                MagicDropDown.removeSelection.bind(this)(item)
            }, false
        ).hide()

        if (this.getOptions().getIsDeleteTagButtonEnabled()) {
            deleteTagButton.show()
        }

        return h.div("dd_tag").text(item.key, false).setData(MagicDropDown.DD_VALUE_ATTR, item.value)
            .add(h.div("close_value").add(deleteTagButton))
    }

    //@bind to the MagicDropDown
    static removeSelection(item) {
        if (this.getOptions().isMultiSelect()) {

            this.getSelectedItems().removeItem(item.value)

            if (!this.getSelectedItems().length) {
                this.resetSelectedItems(this.getOptions().getPreSelectedValue())
            }


        } else {
            this.resetSelectedItems(this.getOptions().getPreSelectedValue())
        }
        this.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_SELECTED, {"value": item.value, "DD": this})
        this.selectRenderer(false,)
    }

    //@bind to the {dd: MagicDropDown, item: item, "renderer" : renderer // a div, to select/deselect}
    static itemSelected(id, item) {
        if (this.dd.#isFirstRender && this.dd.getOptions().isMultiSelect()) {
            this.dd.getSelectedItems().push(item.value);
            this.dd.selectRenderer(true, this.renderer);
            this.renderer.cl(MagicDropDown.SELECTED_ITEM_CLASS)
        }

        if (this.dd.getOptions().isMultiSelect()) {

            if (this.dd.getSelectedItems().includes(item.value)) {
                //deselect
                this.dd.getSelectedItems().removeItem(item.value);
                this.dd.selectRenderer(false, this.renderer);
                this.renderer.rcl(MagicDropDown.SELECTED_ITEM_CLASS)

            } else {
                //select
                this.dd.getSelectedItems().push(item.value);
                this.dd.selectRenderer(true, this.renderer);
                this.renderer.cl(MagicDropDown.SELECTED_ITEM_CLASS)
            }
        } else {
            // select
            this.dd.resetSelectedItems(item.value);
            this.dd.selectRenderer(true, item, this.renderer);
            this.renderer.rcl(MagicDropDown.SELECTED_ITEM_CLASS)
        }

        if (!this.dd.#isFirstRender) {
            this.dd.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_SELECTED, {"value": id, "DD": this})

            const noValueItem = this.dd.getOptions().getDataProvider().getNoValueItem()
            const isSearchEnabled = this.dd.getOptions().getIsSearchEnabled()
            if (noValueItem && isSearchEnabled && _e(noValueItem.value, id)) {
                this.dd.getOptions().getDataProvider().search('', (data) => {
                    this.dd.drawSelection(data)
                })
                this.dd.getSearchInput().setVal('')
            }

        }
        else {
            this.dd.getOptions().fireEvent(DropDownEvent.DD_EVENT_FIRST_RENDER_SELECTED, {"value": id, "DD": this})
        }
        if (!this.dd.getOptions().isMultiSelect()) {
            this.dd.toggleDropdown(false);
        }
        //when items are selected or deselected we need to update title, perhaps it depends on this change
        this.dd.getOptions().getTitleRenderer().updateTitle(this.dd)
    }
}

