class FieldResolver {
    #field;
    #fieldData;

    #parent;
    rowWrapper

    static EVENT_ELEMENT_RENDERED = "EVENT_ELEMENT_RENDERED";
    static EVENT_ERRORS_CLEANED = "EVENT_ERRORS_CLEANED";
    static EVENT_ERROR_ON_CHANGE = "EVENT_ERROR_ON_CHANGE";
    static EVENT_CONTAINER_CREATED = "EVENT_CONTAINER_CREATED";
    static EVENT_VALUES_CONTAINER_CREATED = "EVENT_VALUES_CONTAINER_CREATED";
    static EVENT_COMPONENT_CREATED = "EVENT_COMPONENT_CREATED";
    static EVENT_OBJECT_SELECTED = "EVENT_OBJECT_SELECTED";
    static EVENT_AFTER_ADD = "EVENT_AFTER_ADD";

    static EVENT_OBJECT_CHANGED;// todo remove if this is not used until 01.10.2023
    static EVENT_OBJECT_CHOOSER_CREATED = "EVENT_OBJECT_CHOOSER_CREATED";
    static EVENT_OBJECT_CLEARED = "EVENT_OBJECT_CLEARED";
    static EVENT_VALUE_SET = "EVENT_VALUE_SET";

    static EVENT_FILE_UPLOADED = "EVENT_FILE_UPLOADED";

    static EVENT_FILE_EDITED = "EVENT_FILE_EDITED";
    static EVENT_ADD_CHOOSER_TO_LIST = "EVENT_ADD_CHOOSER_TO_LIST";
    static EVENT_REMOVE_CHOOSER_TO_LIST = "EVENT_REMOVE_CHOOSER_TO_LIST";

    static EVENT_FILE_ERROR = "EVENT_FILE_ERROR";
    static EVENT_LABEL_GENERATED = "EVENT_LABEL_GENERATED";
    static EVENT_FIELD_INFO_GENERATED = "EVENT_FIELD_INFO_GENERATED";
    static EVENT_SCREENSHOT_TAKEN = "EVENT_SCREENSHOT_TAKEN";
    static EVENT_VALUE_READ = "EVENT_VALUE_READ";
    static EVENT_ICON_PREVIEW_GENERATED = "EVENT_ICON_PREVIEW_GENERATED"
    static EVENT_DROPDOWN_VALUE_SET = "DROP-DOWN-VALUE-SET";

    getParent() {
        return this.#parent;
    }

    getRowWrapper() {
        return this.rowWrapper;
    }

    setParent(parent) {
        this.#parent = parent;
        return this;
    }

    cancel() {

    }


    fireEvent(eventName, additional) {

        if (_.isNull(this.#parent) || _.isNull(this.#parent.getFormFieldListener())) {
            return
        }
        let magicFormEvent = new MagicFormEvent(eventName, this.#parent, additional, this.#field.getField());
        this.#parent.getFormFieldListener()(magicFormEvent);
    }

    constructor(field) {
        this.#field = field;

        this.#fieldData = field.getData();
    }


    static getDrawElement(field, parent) {

        let type = field.getType();

        try {
            let renderer = getCustomRenderer(field, parent);
            if (_.notNull(renderer)) return renderer;
        } catch (e) {
            console.log(e);
        }
        if (_.e(type, "checkbox")) {
            return new CheckBox(field).setParent(parent);
        }
        if (_.e(type, "number")) {
            return new NumberField(field, false).setParent(parent);
        }
        if (_.e(type, "double")) {
            return new DoubleField(field, false).setParent(parent);
        }
        if (_.e(type, "text_area")) {
            return new TextArea(field, false).setParent(parent);
        }
        if (_.e(type, "text")) {
            return new InputField(field).setParent(parent);
        }
        if (_.e(type, "hidden")) {
            return new InputField(field).setParent(parent);
        }
        if (_.e(type, "password")) {
            return new PasswordField(field).setParent(parent);
        }
        if (_.e(type, "date")) {
            return new DateElement(field).setParent(parent);
        }
        if (_.e(type, "drop-down")) {
            return new DropDown(field).setParent(parent);
        }
        if (_.e(type, "file")) {
            return new FileSelector(field).setParent(parent);
        }
        if (_.e(type, "files")) {
            return new MultiFilesSelector(field).setParent(parent);
        }
        if (_.e(type, "object_chooser")) {
            return new ObjectChooser(field).setParent(parent).setRequest(parent.getOptions().getRequest());
        }
        if (_.e(type, "referenced_ids")) {
            return new ReferencedObjectChooserList(field).setParent(parent);
        }
        if (_.e(type, "referenced_id")) {
            return new ReferencedObjectChooser(field).setParent(parent).setRequest(parent.getOptions().getRequest());
        }
        if (_e(type, "dictionary")) {
            return new DictionaryChooser(field).setParent(parent);
        }
        if (_e(type, "collections")) {
            return new CollectionField(field).setParent(parent);
        }

        return new InputField(field).setParent(parent);
    }

    clearErrors() {
        try {
            let err = get("error_" + this.#field.getField());
            if (err == null) {
                return;
            }
            err.innerHTML = "";
            err.classList.add("hidden");

            document
                .getElementById(this.#field.getField())
                .classList.remove("error_state");
            this.fireEvent(FieldResolver.EVENT_ERRORS_CLEANED, null)
        } catch (e) {
            log.error(e);
        }
    }

    read(field) {
    }

    create(field) {
    }

    setValue(value) {
    }

    isRequired() {
        return this.#fieldData.required;
    }

    toShowLabel() {
        return this.#fieldData.showLabel;
    }

    getLabelText() {
        if (this.#fieldData.customized) return this.#fieldData.translation;


        return _.$(this.#fieldData.translation);
    }

    toShowPlaceHolder() {
        return this.#fieldData.showPlaceHolder;
    }

    getDescription() {
        return this.#fieldData.description;
    }

    getPlaceHolder() {
        return this.#fieldData.placeHolder;
    }

    getSetting(s) {
        return this.getFieldData()[s];
    }

    getIsId() {
        return this.getFieldData()["id"] || this.#fieldData.field == "id";
    }

    getFieldData() {
        return this.#fieldData;
    }

    getField() {
        return this.#field;
    }

    getType() {
        return this.#field.getType();
    }

    createContainer() {
        return h.div("row").clIf(this.getFieldData(), this.getFieldData().type);
    }

    get isCustom() {
        return this.getField()?.getData()?.customized
    }

    get fieldName() {
        return this.getField().getField();
    }


    markElementsRequired(items) {
        if (!this.isRequired()) {
            return;
        }
        for (let i = 0; i < items.length; i++) {
            items.classList.add("required");
        }

    }

    showLabel(div) {
        if (this.toShowLabel()) {
            let labelDiv = h
                .div("row_label_container")
                .cl(this.getSetting("type") + "_label_container")
                .prependTo(div);

            let lb = this.createLabelComponent().cl("label").cl("text_label");

            labelDiv.add(lb);
            if (this.isRequired()) {
                lb.cl("required");
                labelDiv.add(h.span("required_cell", "*"));
            }

            this.fireEvent(FieldResolver.EVENT_LABEL_GENERATED, labelDiv)
        }

    }

    showDescription(div) {
        let DESCRIPTION_FIELD_CLASS = "prompt_descr_image";
        if (_.notNull(this.getDescription())) {
            let descr = h.div(DESCRIPTION_FIELD_CLASS).text(this.getDescription(), true).hide();
            h.div('descr_wrap')
                .add(h.divImg("menu-item-10.svg").wh(16).cl(DESCRIPTION_FIELD_CLASS).click(() => {
                    descr.toggle(dom.HIDDEN)
                }))
                .add(descr)
                .appendTo(div)
            this.fireEvent(FieldResolver.EVENT_FIELD_INFO_GENERATED, {"descr": descr, "field": this.getField()});
        }
    }

    afterAdd() {
    }

    createLabelComponent() {
        let res = h.label(this.getField().getField(), this.getLabelText(), false);

        this.fireEvent(FieldResolver.EVENT_LABEL_GENERATED, res);
        return res;
    }

    createFieldDiv() {
        return h.div("row__wrapper").setData("field", this.#field.getField())
    }

    selectorListener(event) {
        if (_e(event.type, MagicTrueFalseSelectorEvent.VALUE_CHANGED)) {
            event.parent.fillVariants()
        }
    }

    fillVariants() {
        let path = Environment.fileStorageApi + "cabinet/search/" + Environment.authScope +
            "?page=0&s=" + this.searchField.get().value + "&onPublic="
            + this.onlyPablicSelector.getValue() + "&onlyMy=true"
            + "&onlyImages=" + this.onlyImagesSelector.getValue();
        let _this = this;
        Executor.runGet(path, function (data) {
                _this.content.text(null);
                if (data["empty"] == true) {
                    _this.content.text(_.$("pages.filechooser.no_data"));
                } else {
                    for (let i = 0; i < data.content.length; i++) {
                        let src = Environment.fileStorageApi + data.content[i].downloadUrl
                        _this.makePreviewInUpdates(_this.content, src, data.content[i])
                    }
                }
            },
            true,
            Executor.HEADERS,
            function (e) {
                dom.toast(_.$("global.exception") + e, ToastType.ERROR);
            }
        );
    }

    setDirectValue(value) {

    }
}

class MultiFileHolder {

    #files = [];

    addFile(file) {
        this.#files.push(file);
    }

    removeFile(name, callback) {
        _.each(this.#files, (file, index) => {

            if (_e(this.findFile(name), file)) {
                this.#files.splice(index, 1);

                RemoteFile.delete(file.getId(), callback)

                return '';
            }
        })
    }

    findFile(name) {
        return _.find(this.#files, (file) => {
            return _.e(file.getFileName(), name)
        })
    }

    getValues() {
        return _.map(this.#files, (file) => {
            return file.getSrc();
        })
    }

    getImages() {
        let images = [];
        _.each(this.#files, (file) => {
            if (file.getFile().isImage()) {
                images.push(file.getSrc())
            }
        })

        return images;
    }

    cleanFields() {
        this.#files = [];
    }

    getLength() {
        return this.#files.length;
    }
}

class MultiFilesSelector extends FieldResolver {
    #fileChooser;
    fileNames = [];
    prevDiv;
    #loadingDiv;
    #dropDiv;
    #filesHolder = new MultiFileHolder();
    #request = null
    #options;


    constructor(field, options = new FileSelectorOptions()) {
        super(field);
        this.#options = options;
    }

    getOptions() {
        return this.#options
    }

    getFilesHolder() {
        return this.#filesHolder
    }

    getDropDiv() {
        return this.#dropDiv
    }

    clearErrors() {
    }

    cancel() {

    }

    getPrevDiv() {
        return this.prevDiv
    }

    create() {
        if (_.isNull(this.#options.getRowWrapper())) {
            this.rowWrapper = super.createFieldDiv()
            let row = this.createContainer().appendTo(this.rowWrapper);
            this.showDescription(row)
            this.showLabel(row);
            this.errorContainer = h.div("error_container").appendTo(row)
            this.wrapp = h.div('wrapp_for_attach').appendTo(row);
            this.init();
            return this.rowWrapper;
        } else {
            h.div("files_chooser_wrapper").appendTo(this.#options.getRowWrapper())
            this.rowWrapper = this.#options.getRowWrapper()
            h.div('wrapp_for_attach').appendTo(this.rowWrapper)
            this.wrapp = this.rowWrapper
            this.init();
            return this;
        }


    }

    init = () => {
        this.buildDropBlock();
        this.#options.getAllowScreenshot() && this.buildScreenshotBlock();
        this.#options.getAllowToSelectLoadedFiles() && this.buildUploadBlock();
        this.buildFileUploader();
        this.#loadingDiv = h.div("loading__files").appendTo(this.wrapp);
        this.loadingGifAnim = h.div('loading-files__gif').appendTo(this.#loadingDiv);
        this.prevDiv = h.div('wrap_preview_div').appendTo(this.#loadingDiv);
        this.uploadBox = h.div('uloaded_box').appendTo(this.wrapp);
        this.onDrop();
    }


    setRequest(request) {
        this.#options.setFormRequest(request);
        return this
    }


    getRequest() {
        return _.ofNullable(this.#options.getFormRequest(), () => this.getParent()?.getOptions().getServerRequest())
    }

    getFileRequest() {
        return _.ofNullable(this.getParent()?.getOptions().getFileRequest(), () => new FileRequest(this.getRequest().getAuthorization()))
    }

    buildFileUploader = () => {
        this.#fileChooser = new FileUploader(this.getField().getField() + "_file_chooser",
            (src) => {
                this.insertFile(src)
            }, this.rowWrapper, this.getFileRequest()).setLoadingListener(this.loadingListener);
    }

    getFileChooser() {
        return this.#fileChooser
    }

    buildDropBlock = () => {
        this.#dropDiv = h.div('drop_div')
            .add(h.div('drop_bgi'))
            .add(h.div(this.#options.getDropTitleText()).text(this.#options.getAddFilesText()))
            .add(h.a('#', this.#options.getSelectFilesText(), true).click(() => {
                if (this.#filesHolder.getLength() < this.#options.getFilesLimit()) {
                    this.#fileChooser.openSelector();
                } else {
                    dom.toast(_.$("global.attachment.max_length"))
                }

            }))
            .add(h.div('image_info').text(this.#options.getTypeOfFilesToSelectText()))
            .appendTo(this.wrapp);
    }

    buildScreenshotBlock = () => {
        this.screenshotDiv = h.div('screenshot_div')
            .add(h.a('#', 'global.attachment.take_a_screenshoot', true).click((x) => {
                _this.screenShot();
            }))
            .appendTo(this.wrapp);
    }

    buildUploadBlock = () => {
        this.uploadDiv = h.div('preview_div')
            .add(h.a('#', 'global.attachment.add_uploaded_attachments', true).click(() => {
                if (this.#options.getAllowToSelectLoadedFiles()) {
                    this.createFileSelectionForm(this.uploadBox);
                    this.#options.setAllowToSelectLoadedFiles(false)
                }
            }))
            .appendTo(this.wrapp);
    }

    onDrop = () => {
        var dropCl = "for_drop_files";
        this.#dropDiv.dragOver((event) => {
            event.preventDefault();
            this.#dropDiv.cl(dropCl)
        })

        this.#dropDiv.dragDrop((event) => {
            event.preventDefault();
            if (this.#filesHolder.getLength() < this.#options.getFilesLimit()) {
                this.#fileChooser.openSelector();
            } else {
                dom.toast(_.$("global.attachment.max_length"))
            }
        })
    }

    loadingListener = (event) => {
        var showLoadingClass = "loading_to_show";

        if (_e(event.type, LoadingEvent.LOADING_START)) {
            this.#loadingDiv.cl(showLoadingClass);
        }

        if (_e(event.type, LoadingEvent.LOADING_END)) {
            this.#loadingDiv.rcl(showLoadingClass);
        }
    }

    setValue(value) {
        this.prevDiv.text(null);
        this.#filesHolder.cleanFields();

        if (this.isCustom && value) {
            if (value.value) {
                _.each(value.value.split(","), (src) => {
                    this.insertFile(src)
                })
            }
            return;
        }


        if (value && value[this.getField().getField()] && value[this.getField().getField()].length) {
            let values = value[this.getField().getField()];
            values.forEach(e => this.insertFile(e));
        }
    }

    screenShot() {
        let _this = this
        utils.capture(function (img) {
            img.toBlob((blob) => {
                let file = new File(
                    [blob],
                    "screenshot_from_" + new Date().getTime() + ".jpg",
                    {type: "image/jpeg"});
                const dataTransfer = new DataTransfer();
                dataTransfer.items.add(file);
                _this.#fileChooser.getFileSelector().files = dataTransfer.files;
                _this.#fileChooser.upload();
                img.toDataURL("image/png");
            }, "image/jpeg");
        })
    }

    makePreviewInField(src, filename, fileType) {

        if (src.indexOf(Environment.fileStorageApi) < 0) {
            src = Environment.fileStorageApi + src;
        }
        var file = FileFactory.load(src, new FileDisplayOptions()
            .setDimension(this.#options.getPreviewImageSize())
            .setPreviewWidth(this.#options.getPreviewImageResolution())
        );

        if(!file.getFileName()){
            this.showLackFileError()
            return
        }

        let preview = h.div('attach_preview_item').clIf(src, 'image_has_uploaded');
        let previewBgi = h.div('drop_bgi').appendTo(preview);

        file.draw(previewBgi);
        this.#filesHolder.addFile(file);

        let hoverDiv = h.div('hover_preview_item').click(() => {
            this.showFull(file)
        }).appendTo(preview);

        h.div('header_hover_div')
            .add(h.div('header_div_download').click(() => {
                this.downloadImg(file.getSrc())
            }))
            .addIf(utils.isImage(file.getFileName()), h.div('header_div_edite').click(() => {
                this.startPaint(file, preview);
            }))
            .add(h.div('header_div_delete').id('header_div_delete').click(() => {
                this.deleteFilePrompt(preview, file)
            }))
            .appendTo(hoverDiv);

        this.buildFileFooter(hoverDiv, filename, fileType);
        _.isNull(this.#options.getOrderOfLoadedImages()) ? preview.prependTo(this.prevDiv) : preview.appendTo(this.prevDiv);

    }

    showLackFileError(){
        let error = h.div("error_message_field").text("global.field.file_deleted_from_server_insert_another_one")
            .add(h.div("remove_icon").click(() => error.remove()));
        error.appendTo(this.errorContainer);
        setTimeout(()=> {error.remove()}, 7000);
    }

    deleteFilePrompt = (preview, file) => {
        let opts = new MagicPromptOptions("global.prompt.the.image.will.be.removed");
        opts.applyClick(MagicPromptButton.OK_LABEL, () => {
            this.deletePreview(preview, file);
            opts.markNoRepeat();
            opts.closeSelf();
        }).setIsAction(MagicPromptButton.OK_LABEL, true)
            .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                opts.closeSelf();
                opts.markNoRepeat();
            })
            .setIsAction(MagicPromptButton.CANCEL_LABEL, false)
            .allowNoRepeat("global.prompt.delete_item").newPrompt();
    }

    buildFileFooter = (hoverDiv, filename, fileType) => {
        let footerForHoverDiv = h.div('footer_hover_div').appendTo(hoverDiv);
        let firstText = h.div('footer_div_textname').text(filename, false).appendTo(footerForHoverDiv);
        h.div('footer_div_texttype').text(fileType, false).appendTo(footerForHoverDiv);
    }

    showFull(file) {
        let fileClass = file.getFile();
        if (fileClass.isImage()) {
            utils.showFullMedia(file.getSrc(), this.#filesHolder.getImages());
        } else if (fileClass.isVideo()) {
            utils.showFullMedia(file.getSrc(), [], "video");
        } else if (fileClass.isAudio()) {
            utils.showFullMedia(file.getSrc(), [], "audio")
        }
    }

    insertFile(src, filename, fileType) {
        this.makePreviewInField(src, filename, fileType)
        this.#fileChooser.clean();
    }

    startPaint(file, preview) {
        if (file && file.getFile().isImage()) {
            let src = file.getSrc()
            let filename = file.getFileName();

            new ImgRedactor().init(src, filename).result((img, filename) => {
                this.loadingListener(new LoadingEvent(LoadingEvent.LOADING_START));
                img.toBlob(async (blob) => {
                    let fileImg = new File(
                        [blob],
                        filename,
                        {type: "image/jpeg"}
                    );
                    await this.#fileChooser.upload(fileImg);
                    this.deletePreview(preview, file);
                }, "image/jpeg");
            })
        }
    }

    downloadImg(src) {
        if (!src.includes(this.#options.getNoImage())) {
            let a = h.a(src).attr('download');
            a.get().click();
        }
    }

    deletePreview(prew, file) {
        this.#filesHolder.removeFile(file.getFileName(), () => {
            prew.remove();
        });
    }


    createFileSelectionForm(where) {
        let _this = this;
        let headerConf = h.div("row");
        let form = h.div("findFile").appendTo(where);
        let headerSearchField = h.div("row", "search").appendTo(form);
        headerConf.appendTo(form)
        let fcp = h.span("fcp_class", null).appendTo(headerConf);


        this.searchField = h
            .input("text")
            .id(this.getField().getField() + "_fcsf")
            .onKey((x) => _this.fillVariants())
            .placeHolder('global.attachment.search')
            .appendTo(headerSearchField);

        let onlyPablicSelectorOptions = new MagicTrueFalseSelectorOptions("pages.filechooser.only_public", false, true, false, true)
        onlyPablicSelectorOptions.setComponentListener(this.selectorListener)
        onlyPablicSelectorOptions.setParent(this)
        this.onlyPablicSelector = new MagicTrueFalseSelector(onlyPablicSelectorOptions).draw(fcp);


        headerConf.appendChild(fcp);

        let fcOnlyImages = h.span("fcom_class", null).appendTo(headerConf);

        let onlyImagesSelectorOptions = new MagicTrueFalseSelectorOptions("pages.filechooser.only_images", false, true, false, true)
        onlyImagesSelectorOptions.setComponentListener(this.selectorListener)
        onlyImagesSelectorOptions.setParent(this)
        this.onlyImagesSelector = new MagicTrueFalseSelector(onlyImagesSelectorOptions).draw(fcOnlyImages);


        this.content = h.div("fs_content").appendTo(form);

        h.img("close.png.png")
            .wh(16)
            .click(() => {
                _this.closeUploaded();
            })
            .appendTo(form);

        this.fillVariants();
    }


    closeUploaded() {
        this.uploadBox.first()?.remove();
        this.#options.setAllowToSelectLoadedFiles(true)
    }

    makePreviewInUpdates(div, src, data) {
        h.div('attach_preview_item')
            .add(h.div('drop_bgi')
                .add(h.img(src ? src : this.#options.getNoImage()).wh(48)))
            .add(h.div('hover_preview_item')
                .add(h.div('header_hover_div'))
                .add(h.div('footer_hover_div')
                    .add(h.div('footer_div_textname').text(data.originalName, false))
                    .add(h.div('footer_div_texttype').text(data.type, false))))
            .click(() => {
                this.insertFile(src);
                this.closeUploaded();
            }).appendTo(div);
    }

    read() {
        return this.#filesHolder.getValues()
    }
}

class FileSelectorOptions extends Lombok {
    noImage = "no_image.png.png";
    allowScreenshot = true;
    addFilesText = "global.attachment.drop_file_here_or";
    selectFilesText = "global.attachment.click_to_select";
    dropTitleText = "drop_title_text";
    typeOfFilesToSelectText = 'global.attachment.all_types_file'
    allowedFileExtensions = "*"
    allowToSelectLoadedFiles = true;
    filesLimit = Environment.max_files_upload_limit || 6;
    rowWrapper = null;
    orderOfLoadedImages = null;
    previewInfield = true
    formRequest = null
    deleteFilesPrompt = true;
    deleteFilesMessage = 'global.prompt.the.image.will.be.removed'

    previewImageSize = new Dimension().setHeight(32).setHeight(32);
    previewImageResolution = 32 //in pixels, -1 is origin size
    constructor() {
        super().$();
    }
}

class FileSelector extends FieldResolver {
    #fileChooser;
    #valueHolder = h.input(dom.HIDDEN);
    #preview;
    fileNames = [];
    prevDiv;
    #loadingDiv;
    #uploadedDiv = false;
    #canDelete = false;
    #dropDiv;
    #wrap;
    #file = null;
    #options;

    #loadFileListener;

    constructor(field, options = new FileSelectorOptions()) {
        super(field);
        this.#options = options;
    }

    getOptions() {
        return this.#options
    }

    clearErrors() {
    }

    setWrapp(el) {
        this.#wrap = el;

        return this;
    }

    setDropDiv(dropDiv) {
        this.#dropDiv = dropDiv
    }

    getFileChooser() {
        return this.#fileChooser
    }

    getWrapp() {
        return this.#wrap
    }

    getPrevDiv() {
        return this.prevDiv
    }

    create() {
        if (_.isNull(this.#options.getRowWrapper())) {
            this.rowWrapper = super.createFieldDiv();
            let row = this.createContainer().appendTo(this.rowWrapper);
            this.showDescription(row);
            this.showLabel(row);
            this.#wrap = h.div('wrapp_for_attach').appendTo(row);
            this.init();
            return this.rowWrapper
        } else {
            h.div("files_chooser_wrapper").appendTo(this.#options.getRowWrapper())
            this.rowWrapper = this.#options.getRowWrapper()
            h.div('wrapp_for_attach').appendTo(this.rowWrapper)
            this.#wrap = this.rowWrapper
            this.init();
            return this
        }
    }

    init = () => {
        this.buildDropDiv();
        this.#options.getAllowScreenshot() && this.buildScreenshotDiv();
        this.#options.getAllowToSelectLoadedFiles() && this.buildUpLoadDiv();
        this.buildPreviewContainer();
        this.buildFileWrapper();
        this.initFileChooser();
        this.onDrop();
    }

    buildPreviewContainer() {
        this.#loadingDiv = h.div("loading__files").appendTo(this.#wrap)
        this.loadingGifAnim = h.div('loading-files__gif').appendTo(this.#loadingDiv);
        this.prevDiv = h.div('wrap_preview_div').appendTo(this.#loadingDiv).hide();
        this.uploadBox = h.div('uloaded_box').appendTo(this.#wrap);
    }

    buildDropDiv = () => {
        this.#dropDiv = h.div('drop_div')
            .add(h.div('drop_bgi'))
            .add(h.div(this.#options.getDropTitleText()).text(this.#options.getAddFilesText()))
            .add(h.a('#', this.#options.getSelectFilesText(), true).click(() => {
                this.replaceImgConfirmation(() => {
                    this.#fileChooser.openSelector();
                })
            }))
            .add(h.div('image_info').text(this.#options.getTypeOfFilesToSelectText()))
            .appendTo(this.#wrap);
    }

    buildScreenshotDiv = () => {
        this.screenshotDiv = h.div('screenshot_div')
            .add(h.a('#', 'global.attachment.take_a_screenshoot', true).click((x) => {
                this.replaceImgConfirmation(() => {
                    this.makeScreenshot()
                })
            }))
            .appendTo(this.#wrap);
    }

    buildUpLoadDiv = () => {
        this.uploadDiv = h.div('preview_div')
            .add(h.a('#', 'global.attachment.add_uploaded_attachments', true).click(() => {
                if (!this.#uploadedDiv) {
                    this.createFileSelectionForm(this.uploadBox);
                    this.#uploadedDiv = true;
                }
            }))
            .appendTo(this.#wrap);
    }

    setRequest(request) {
        this.#options.setFormRequest(request);
        return this
    }

    getRequest() {
        return _.ofNullable(this.#options.getFormRequest(), () => this.getParent()?.getOptions().getServerRequest())
    }

    getFileRequest() {
        return _.ofNullable(this.getParent()?.getOptions().getFileRequest(), () => new FileRequest(this.getRequest().getAuthorization()))
    }

    initFileChooser() {
        this.#fileChooser = new FileUploader(this.getField().getField() + "_file_chooser",
            (src, fileName, fileType, data) => {
                this.#canDelete = !!data.newlyCreated;
                this.insertFile(src, fileName, fileType);

            }, this.rowWrapper, this.getFileRequest()).setLoadingListener(this.loadingListener)
    }

    onDrop = () => {
        var dropCl = "for_drop_files";
        this.#dropDiv.dragOver((event) => {
            event.preventDefault();
            this.#dropDiv.cl(dropCl)
        })

        this.#dropDiv.dragDrop((event) => {
            event.preventDefault();
            this.#dropDiv.rcl(dropCl);
            let file = event.dataTransfer.files[0];
            this.replaceImgConfirmation(() => {
                this.loadingListener(new LoadingEvent(LoadingEvent.LOADING_START));
                this.#fileChooser.upload(this.getFileRequest(), file);
            })
        })
    }

    loadingListener = (event) => {
        var showLoadingClass = "loading_to_show";

        if (_e(event.type, LoadingEvent.LOADING_START)) {
            this.#loadingDiv.cl(showLoadingClass)
        }

        if (_e(event.type, LoadingEvent.LOADING_END)) {
            this.#loadingDiv.rcl(showLoadingClass)
        }
    }

    setValue(value) {

        if (this.isCustom && value && value.value) {
            this.insertFile(value.value);
            return;
        }

        if (value && value[this.getField().getField()]) {
            this.#valueHolder.value = value[this.getField().getField()];
            this.insertFile(this.#valueHolder.value);
        } else {
            this.deletePreview();
        }
    }

    makeScreenshot() {
        let _this = this
        utils.capture(function (img) {
            img.toBlob((blob) => {
                let file = new File(
                    [blob],
                    "screenshot_from_" + new Date().getTime() + ".jpg",
                    {type: "image/jpeg"}
                );
                const dataTransfer = new DataTransfer();
                dataTransfer.items.add(_this.getFileRequest(), file);
                _this.#fileChooser.getFileSelector().files = dataTransfer.files;
                _this.#fileChooser.upload(_this.getFileRequest(), file);
            }, "image/jpeg");
        })
    }

    cancel() {
        this.removeFile();
    }

    removeFile = () => {
        if (this.#canDelete) {
            RemoteFile.delete(this.#file.getRemoteData().id)
        }
        this.#canDelete = false;
    }

    buildFileWrapper = () => {
        this.fileWrapper = h.div('attach_preview_item').cl("image_has_uploaded").prependTo(this.prevDiv);
        this.#preview = h.div('drop_bgi').appendTo(this.fileWrapper);
        this.hoverDiv = h.div('hover_preview_item').appendTo(this.fileWrapper)
    }

    makePreviewInField() {
        if (this.#options.getPreviewInfield()) {
            this.hoverDiv.text(null).add(h.div('header_hover_div')
                .add(h.div('header_div_download').click(() => this.download()))
                .addIf(this.addEditBtnToFilePreview(), h.div('header_div_edite').id('header_div_edite').click(() => this.startPaint()))
                .add(h.div('header_div_delete').id('header_div_delete').clickIf(!this.#options.getDeleteFilesPrompt(), () => this.deleteFile())
                    .add(h.div('header_div_delete').id('header_div_delete').clickIf(this.#options.getDeleteFilesPrompt()),
                        () => {
                            let opts = new MagicPromptOptions(this.#options.getDeleteFilesMessage());
                            opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                                opts.markNoRepeat();
                                opts.closeSelf();
                                this.deleteFile();
                            }).setIsAction(MagicPromptButton.OK_LABEL, true)
                                .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                                    opts.closeSelf();
                                    opts.markNoRepeat();
                                }).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                                .allowNoRepeat("global.replace_attachment").newPrompt();
                        }))).click(() => {
                this.showFull()
            })
            this.buildItemFooter();
            this.fireEvent(FieldResolver.EVENT_ICON_PREVIEW_GENERATED, this.hoverDiv)
        } else {
            this.hoverDiv.text(null)
        }


    }

    deleteFile() {
        this.removeFile();
        this.deletePreview();
    }

    buildItemFooter() {
        var footer = h.div('footer_hover_div')
        this.fileNameDiv = h.div('footer_div_textname').appendTo(footer).text(this.#file.getFileName(), false);
        this.fileTypeDiv = h.div('footer_div_texttype').appendTo(footer).text(this.#file.getFileType(), false);
    }

    addEditBtnToFilePreview = () => {
        return this.#file && this.#file.getFile().isImage()
    }

    showFull() {
        let fileClass = this.#file.getFile();
        if (fileClass.isImage()) {
            utils.showFullMedia(fileClass.getSrc(), []);
        } else if (fileClass.isVideo()) {
            utils.showFullMedia(fileClass.getSrc(), [], "video");
        } else if (fileClass.isAudio()) {
            utils.showFullMedia(fileClass.getSrc(), [], "audio")
        }
    }

    deletePreview() {
        this.#preview.text(null).add(h.img(this.#options.getNoImage(), 64).wh(64))
        this.#valueHolder.text(null)
        this.#fileChooser.clean();
        this.#file = null;
        this.prevDiv.hide();
    }

    download() {
        if (this.#file) {
            h.a(this.#file.getSrc()).attr('download').performClick();
        }
    }

    replaceImgConfirmation(myFunc) {
        if (_.isNull(this.#file)) {
            myFunc();
        } else {
            try {
                let opts = new MagicPromptOptions("global.prompt.confirm_replace");
                opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                    opts.markNoRepeat();
                    opts.closeSelf();
                    if (this.#canDelete) {
                        RemoteFile.delete(this.#file.getRemoteData().id)
                    }
                    myFunc();
                }).setIsAction(MagicPromptButton.OK_LABEL, true)
                    .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                        opts.closeSelf();
                        opts.markNoRepeat();
                    }).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                    .allowNoRepeat("global.prompt_no_repeat").newPrompt();
            } catch {
                myFunc();
            }

        }
    }

    closeUploaded() {
        this.uploadBox.first()?.remove();
        this.#uploadedDiv = false;
    }

    makePreviewInUpdates(prevDiv, src, data) {
        h.div('attach_preview_item')
            .add(h.div('drop_bgi')
                .add(h.img(src ? src : this.#options.getNoImage()).wh(48)))
            .add(h.div('hover_preview_item')
                .add(h.div('header_hover_div'))
                .add(h.div('footer_hover_div')
                    .add(h.div('footer_div_textname').text(data.originalName, false))
                    .add(h.div('footer_div_texttype').text(data.type, false))))
            .click(() => {
                this.replaceImgConfirmation(() => {
                    this.insertFile(src);
                    this.closeUploaded();
                })
            }).appendTo(prevDiv);
    }

    insertFile(src) {
        if (_.isNull(src)) {
            return false;
        }
        if (_e(src.indexOf("http"), -1)) {
            src = Environment.fileStorageApi + src;
        }
        this.#preview.text(null)
        let file = FileFactory.load(src, new FileDisplayOptions()
            .setDimension(this.#options.getPreviewImageSize())
            .setPreviewWidth(this.#options.getPreviewImageResolution()));
        file.draw(this.#preview);
        this.#valueHolder.text(src, false);
        this.#file = file;
        this.makePreviewInField();
        this.prevDiv.show();
        this.#loadFileListener && this.getLoadFileListener()(file)
    }

    setLoadFileListener(loadFileListener) {
        this.#loadFileListener = loadFileListener;
        return this;
    }

    getLoadFileListener() {
        return this.#loadFileListener
    }

    setDirectValue(value) {
        this.insertFile(value)
        return this
    }

    startPaint() {
        if (this.#file && this.#file.getFile().isImage()) {
            let src = this.#file.getSrc()
            let filename = this.#file.getServerFileName();

            new ImgRedactor().init(src, filename).result((img, filename) => {
                img.toBlob(async (blob) => {
                    let file = new File(
                        [blob],
                        filename,
                        {type: "image/jpeg"}
                    );
                    await this.#fileChooser.upload(file);
                }, "image/jpeg");
            })
        }
    }

    createFileSelectionForm(where) {
        let _this = this;
        let headerConf = h.div("row");
        let form = h.div("findFile").appendTo(where);
        let headerSearchField = h.div("row", "search").appendTo(form);
        headerConf.appendTo(form)
        let fcp = h.span("fcp_class", null).appendTo(headerConf);

        this.searchField = h
            .input("text")
            .id(this.getField().getField() + "_fcsf")
            .onKey((x) => _this.fillVariants())
            .placeHolder('global.attachment.search')
            .appendTo(headerSearchField);

        let onlyPablicSelectorOptions = new MagicTrueFalseSelectorOptions("pages.filechooser.only_public", false, true, false, true)
        onlyPablicSelectorOptions.setComponentListener(this.selectorListener)
        onlyPablicSelectorOptions.setParent(this)
        this.onlyPablicSelector = new MagicTrueFalseSelector(onlyPablicSelectorOptions).draw(fcp);


        headerConf.appendChild(fcp);

        let fcOnlyImages = h.span("fcom_class", null).appendTo(headerConf);

        let onlyImagesSelectorOptions = new MagicTrueFalseSelectorOptions("pages.filechooser.only_images", false, true, false, true)
        onlyImagesSelectorOptions.setComponentListener(this.selectorListener)
        onlyImagesSelectorOptions.setParent(this)
        this.onlyImagesSelector = new MagicTrueFalseSelector(onlyImagesSelectorOptions).draw(fcOnlyImages);


        this.content = h.div("fs_content").appendTo(form);

        h.img("close.png.png")
            .wh(16)
            .click(() => {
                _this.closeUploaded();
            })
            .appendTo(form);

        this.fillVariants();
    }


    read() {
        return this.#file?.getSrc();
    }

}

class DropDown extends FieldResolver {
    #dropDown;

    constructor(field) {
        super(field);
    }

    getDropDown() {
        return this.#dropDown;
    }

    create(value) {

        if (this.getFieldData().dataProviders) {
            value = this.getFieldData().dataProviders
        }

        this.rowWrapper = super.createFieldDiv();
        let row = this.createContainer().appendTo(this.rowWrapper);
        this.showDescription(row)

        let dropDown = dom.select(this.getField().getField());
        this.#dropDown = dropDown;
        let label = h.label(this.getField(), "").attr("for", this.getField());

        let optionsData = this.getSetting("dataProviders");
        if (optionsData == null || optionsData.length == 0) {
            log.error(
                "Data provider not set for this field " + this.getField().getField()
            );
        }

        if (Array.isArray(value)) {
            _.each(value, (val) => {
                dom.addOptionInSelect(this.#dropDown, val);
            })
        } else if (value != null) {
            dom.setCboValue(dropDown, value);
        }

        let div = h.div("dd_data");

        // $(dropDown).prettyDropdown();

        this.markElementsRequired({dropDown, div});

        this.showLabel(div);

        div.appendChild(label);
        div.appendChild(dropDown);
        div.appendChild(dom.errorPlate(this.getField().getField()));

        row.appendChild(div);

        return this.rowWrapper;
    }

    read(field) {
        return this.#dropDown.val();
    }

    setValue(val) {
        let value = val && val[this.getField().getField()]
        if (value) {
            this.#dropDown.setVal(value)
        }

        this.fireEvent(FieldResolver.EVENT_DROPDOWN_VALUE_SET, {
            dropDown: this,
            value: this.#dropDown.val(),
            inputElement: this.#dropDown
        })
    }
}

class DateElement extends FieldResolver {
    #chooser;

    #options;

    #dateDiv
    #resetChooserButton
    #value;

    constructor(field) {
        super(field);
    }

    dateListener = (event) => {
        if (_e(event.eventName, DateEvent.DATE_SELECTED)) {
            this.#value = event.data;
            this.#resetChooserButton.show();
        }
    }

    resetValue() {
        this.#value = null;
        this.#dateDiv.text("pages.filechooser.select");
        this.#resetChooserButton.hide();
    }

    create(value) {
        let needPreset = !!value;
        this.rowWrapper = super.createFieldDiv().cl("date-row");
        this.showDescription(this.rowWrapper)

        this.#dateDiv = h.div("chooser").text("pages.filechooser.select");
        this.#resetChooserButton = h.img("rm.png").click(() => {
            this.resetValue();
        }).hide()

        this.chooserDiv = h.div("chooser")
            .add(this.#dateDiv)
            .add(this.#resetChooserButton)
            .appendTo(this.rowWrapper);


        let val = "";

        if (value && value[this.getField().getField()]) {
            val = _.getLocalDatefromMillisecond(value[this.getField().getField()].date, true); // 21-feb
            this.#dateDiv.text(val, false);
            this.#value = value[this.getField().getField()]; //milisec
        }

        this.showLabel(this.rowWrapper);

        this.#options = new DateOptions(this.#dateDiv, ".", "dd.mm.yyyy", this.getDate(value), this.dateListener, needPreset);

        this.#chooser = new MagicDateChooser(this.#options).init();

        return this.rowWrapper;
    }

    getDate(value) {
        if (_.isNull(value)) {
            return null
        }
        if (_.isNull(value[this.getField().getField()])) {
            return null
        }
        return _.getLocalDatefromMillisecond(value[this.getField().getField()].date, false)
    }

    setValue(data) {
        if (data && data[this.getField().getField()]) {
            this.#chooser.setNewDate(_.getLocalDatefromMillisecond(data[this.getField().getField()].date, false));
            this.#value = data[this.getField().getField()].date // milisec
            this.#resetChooserButton.show();
        } else {
            this.#chooser.setNewDate(null)
            this.#dateDiv.text("pages.filechooser.select");
            this.#value = '';
        }
    }

    read() {

        return {
            "date": this.#value,
            "milliseconds": this.#value,
            "zone": "Etc/UTC",
            "template": null
        }
    }
}


class ObjectChooser extends FieldResolver {
    valueHolder;
    #div;
    #request;
    #oldValue;

    static SELECT_TEXT = "global.app.select_text"

    #allowDelete = true;


    getRequest() {
        return this.#request;
    }

    setRequest(request) {
        this.#request = request;
        return this;
    }

    constructor(field) {
        super(field);
        this.field = field
    }

    /**
     * Display selected value here
     * @param data
     */
    updateValue(data) {
        this.valueHolder.text(data.id, false);
    }

    static getFieldRequest(fieldName, parent) {
        let multiRequest = parent.getOptions().getFormMultiServerRequest()
        let scope = parent.getOptions().getScope();
        if (multiRequest) {
            return multiRequest(fieldName, scope)
        }
        return parent.getOptions().getRequest();
    }

    create(value) {

        // this.field
        this.rowWrapper = super.createFieldDiv();
        let row = h.div("row").appendTo(this.rowWrapper);

        this.showDescription(row)
        let div = h.div("chooser").appendTo(row);

        this.#div = div;
        this.valueHolder = h.input(dom.HIDDEN).id(this.getField().getField()).attr("data-type", this.getSetting("searchName")).appendTo(div);


        if (!utils.isEmpty(value)) {
            try {
                this.valueHolder.text(value[this.getField().getField()].key, false);
            } catch (error) {

            }
        }

        //find
        let fieldName = this.getField().getField()

        this.request = ObjectChooser.getFieldRequest(fieldName, this.getParent());
        const renderFunction = this.getParent()?.getOptions()?.getObjectChooserRenderer();
        let magicChooserOptions = new MagicRemoteChooserOptions()
            .setRequest(this.request)
            .setEventListener((data) => {
                this.chooserListener(data)
            })
            .setItemRenderer(renderFunction)

        this.magicChooser = new MagicRemoteChooser(magicChooserOptions)


        this.markElementsRequired({div});
        this.drawValue();
        this.showLabel(row);

        //on add form we have a different key
        let errorSpan = dom.errorPlate(this.getField().getField());

        this.rowWrapper.add(errorSpan);

        this.fireEvent(FieldResolver.EVENT_OBJECT_CHOOSER_CREATED, {
            "valueHolder": this.valueHolder,
            "rowWrapper": this.rowWrapper
        })

        return this.rowWrapper;
    }

    chooserListener(data) {
        if (_e(data.eventName, MagicChooserEvent.CHOOSER_VALUE_CHANGED)) {
            this.drawValue(data?.elementId);
            this.valueHolder.text(data?.elementId.id, false);
        }
        if (_e(data.eventName, "remote_chooser_value_destroyed")) {
            this.valueHolder.text(null)
        }
    }

    prepareData(data) {

        return data;
    }

    drawValue(data) {
        data = this.prepareData(data);
        this.#div.text(null);
        let _this = this;

        // ObjectConverter transform data to common object by the rule : id => key, name => value, additional => additional: {icon:'', color:'',}

        let objectConverter = new ObjectConverter()
        objectConverter.addMapping("id", "key", (val) => val)
        objectConverter.addMapping(this.getField().getData().searchField, "value", (val) => val)

        objectConverter.addMapping("icon", "additional",
            (val, res) => {
                let additional = res.additional;
                if (_.isNull(additional)) {
                    res.additional = {"icon": val}
                } else {
                    res.additional.icon = val;
                }
                return res.additional
            })

        objectConverter.addMapping("additional", "additional",
            (val, res) => {

                let additional = res.additional;

                const icon = val?.icon;

                if (icon && !_.isObject(icon)) {
                    return {...res.additional, icon};
                }

                if (!_.isObject(additional)) {
                    res.additional = {"icon": val}
                } else {
                    res.additional.icon = val
                }

                return res.additional
            })

        let convertedData = objectConverter.convertToObject(data);
        if (utils.isNull(convertedData) || utils.isEmpty(convertedData["value"])) {

            let value = h.a("#", ObjectChooser.SELECT_TEXT, true).id("remote_selector_" + this.getField()).add(h.div("select_item")).click(() => {
                this.magicChooser.draw();
            }, false);

            h.div("magic_object_chooser_content").add(value).appendTo(this.#div);
        } else {

            let drawContent;

            try {
                drawContent = drawCustomSelectChooserElementValue(this.getField(), this.getData()).click(function () {
                    _this.magicChooser.draw();
                }, false);
            } catch (e) {

            }

            if (drawContent == null) {
                drawContent = MagicTable.defaultConverter(this.getField(), convertedData, convertedData, null, "object_chooser", function () {
                }).click(function () {
                    _this.magicChooser.draw();
                });
            }

            let value = h.a("#", "").add(drawContent);


            let cleanValueImg = h.img('rm.png').click(function () {
                data = {};
                _this.drawValue(null);
                if (_this.div) {
                    _this.div.setData('id', 0);
                    _this.div.setData('id', 0);
                }
                _this.#oldValue = _this.valueHolder.val();
                _this.valueHolder.text(null);
                _this.fireEvent(MagicRemoteChooser.EVENT_ITEM_DESELECTED)
            }, false);

            //reset where and add correspondent result;
            this.#div.text(null);

            h.div("magic_object_chooser_content").add(value).addIf(this.#allowDelete, cleanValueImg).appendTo(this.#div);


            this.fireEvent(MagicRemoteChooser.EVENT_ITEMS_RENDERED, this.#div)
        }
    }

    read() {
        let res
        if (+this.valueHolder.val() == parseInt(this.valueHolder.val())) {
            res = +this.valueHolder.val()
        } else {
            res = this.valueHolder.val()
        }

        if (_.isEmpty(res)) {
            res = null
        }

        this.fireEvent(FieldResolver.EVENT_VALUE_READ, res);

        return res;
    }


    setValue(value, isCustom = false) {

        try {
            let fieldName = this.getField().getField()
            this.drawValue(value[fieldName]);
            let id = this.getId(value, isCustom, fieldName);
            this.valueHolder.text(id, false);
        } catch (e) {
            log.promisedDebug(() => e)
        }
    }

    isDictionary = () => false;

    getId(value, isCustom, fieldName) {
        let id = null;

        if (isCustom) {
            id = value.value;
        } else {
            id = value[fieldName].id;
            if (_.isNull(id)) {
                id = value[fieldName].key;
            }
        }
        return id;
    }
}

class ReferencedObjectChooserList extends FieldResolver {

    valueHolder;
    #listOfObjects = []
    #divContainer = h.div('field_list')

    constructor(field) {
        super(field);
        this.field = field
    }

    create(value) {
        this.field
        this.rowWrapper = super.createFieldDiv();
        let row = h.div("row").appendTo(this.rowWrapper);

        this.showDescription(row)
        this.markElementsRequired({row});
        this.showLabel(row);
        this.initChooser();
        h.div().text("global.fields.add").andText(this.field.getField(), false).click(() => {
            this.magicChooser.draw();
        }).appendTo(this.rowWrapper);
        this.#divContainer.appendTo(this.rowWrapper)
        return this.rowWrapper;
    }

    setValue(value) {

        this.#listOfObjects = []
        this.rebuildContainer()
        let fieldName = this.getField().getField()

        if (value) {

            let request = ObjectChooser.getFieldRequest(fieldName, this.getParent());

            let values = value[fieldName]
            _.each(values, (id) => {
                request.byId(id, (data) => {
                    this.#listOfObjects.push(data)
                    this.rebuildContainer()
                })
            })
        }
    }

    initChooser() {

        let fieldName = this.getField().getField()

        this.request = ObjectChooser.getFieldRequest(fieldName, this.getParent());

        let magicChooserOptions = new MagicRemoteChooserOptions()
            .setRequest(this.request)
            .setEventListener((data) => {
                this.chooserListener(data)
            })

        this.magicChooser = new MagicRemoteChooser(magicChooserOptions)
    }

    chooserListener(data) {

        if (_e(data.eventName, MagicChooserEvent.CHOOSER_VALUE_CHANGED)) {
            let existObjectInList = this.#listOfObjects.find((e) => {
                if (_e(e.id, data.elementId.id)) {
                    return true
                }
            })
            if (!existObjectInList) {
                this.#listOfObjects.push(data.elementId)
                this.fireEvent(FieldResolver.EVENT_ADD_CHOOSER_TO_LIST, data.elementId)
                this.rebuildContainer()
            }
        }

    }

    rebuildContainer() {
        this.#divContainer.text(null)
        _.each(this.#listOfObjects, (item, i) => {
            h.div("item").addIf(item.icon, h.img(item.icon, 16).wh(16))
                .add(h.div("item_name").text(item.name, false))
                .add(h.div("remove_item").text("remove", false).click(() => {
                    this.#listOfObjects.splice(i, 1)
                    this.fireEvent(FieldResolver.EVENT_REMOVE_CHOOSER_TO_LIST, item)
                    this.rebuildContainer()
                }))
                .appendTo(this.#divContainer)
        })
    }

    read() {
        return this.#listOfObjects.map((el) => el.id)
    }


}

class InputField extends FieldResolver {
    #holder;

    constructor(field) {
        super(field);
    }

    read() {
        if (this.getIsId() && parseInt(this.#holder.val()) == this.#holder.val()) {
            return parseInt(this.#holder.val());
        }
        return this.#holder.val();
    }

    setValue(value) {

        if (this.getField().getData().customized && value) {
            this.#holder.text(value.value, false);
            return;
        }

        let text = ''
        if (value) {
            text = value[this.getField().getField()];
        }
        this.#holder.text(text, false);
    }

    setDirectValue(value) {
        this.#holder.text(value, false);
    }

    setThisValue(el, value) {
        if (el == null) {
            el = this.html().text(null);
        }
        if (value != null) {
            el.value = value;
        }
    }

    html() {
        return this.#holder;
    }

    createWebObject() {
        return h.input(this.getField().getType()).id(this.getField().getField());
    }

    create(value) {
        this.rowWrapper = super.createFieldDiv();
        let div = this.createContainer().appendTo(this.rowWrapper);
        let descr = h.div("description")
        if (_.e('checkbox', this.getType())) {
            descr.appendTo(this.rowWrapper);
        } else {
            descr.appendTo(div);
        }
        let el = this.createWebObject();
        this.#holder = el;

        if (this.toShowPlaceHolder()) {
            el.placeHolder(this.getPlaceHolder(), false);
        }

        this.markElementsRequired(el, div);

        this.setThisValue(el, value);

        if (value != null) {
            el.text(value[this.getField().getField()], false);
        }

        this.showLabel(div);
        this.showDescription(descr);
        this.addElementToInput(div, el);

        dom.errorPlate(this.getField().getField()).appendTo(this.rowWrapper);

        div.clIf(utils.e(this.getField().getType(), dom.HIDDEN), dom.HIDDEN);

        return this.rowWrapper;
    }

    addElementToInput(div, el) {
        h.from(div).add(el);
    }
}

class PasswordField extends InputField {
    constructor(field) {
        super(field);
    }

    createWebObject() {
        return h.input("password").noCache().id(this.getField().getField());
    }
}

class NumberField extends InputField {
    #hidden;
    #holder;

    constructor(field, hidden) {
        super(field);
        this.#hidden = hidden;
        this.#holder = h.input(this.getField().getType()).id(this.getField().getField());
    }

    read() {
        return this.#holder.val();
    }

    createWebObject() {
        return this.#holder;
    }

    getHolder() {
        return this.#holder
    }
}

class DoubleField extends InputField {

    constructor(field) {
        super(field);
    }

    read() {
        let inputFieldValue = super.read()
        return parseFloat(inputFieldValue)
    }

}

class TextArea extends InputField {
    #el;
    #editor;
    #data;

    constructor(field) {
        super(field);

        this.field = field;
    }

    createWebObject() {

        var field = this.field.getField();

        return this.#el = h.tag("textarea").noCache().id(new Date().getTime() + "_" + field);
    }

    read() {
        if (this.#editor) {

            return this.#editor.getContents();
        } else {

            return this.#el.val();
        }
    }

    setDirectValue(value) {
        if (this.#editor) {
            this.#editor.setContents(value);
            return;
        } else {
            this.#el.text(value, false);
            return;
        }
    }

    setValue(value) {

        this.#data = value;

        if (value) {

            let val;


            val = _.ofNullable(value[this.getField().getField()], "");

            if (this.#editor) {
                this.#editor.setContents(val);

            } else {
                this.#el.text(val, false);

            }


        } else {
            if (this.#editor) {
                this.#editor.setContents("");

                return;
            } else {
                this.#el.text("", false)
            }
        }
    }

    afterAdd() {
        if (utils.isNull(this.#editor)) {
            this.#editor = this.initMCE(this.#el);

            this.setValue(this.#data)
        }

        this.fireEvent(FieldResolver.EVENT_AFTER_ADD, this);

        return;
    }

    initMCE(textArea) {
        return SUNEDITOR.create(textArea.get(), {
                buttonList: [
                    ['undo', 'redo'],
                    ['font', 'fontSize', 'formatBlock'],
                    ['paragraphStyle', 'blockquote'],
                    ['bold', 'underline', 'italic', 'strike', 'subscript', 'superscript'],
                    ['fontColor', 'hiliteColor', 'textStyle'],
                    ['removeFormat'],
                    ['outdent', 'indent'],
                    ['align', 'horizontalRule', 'list', 'lineHeight'],
                    ['table', 'link', 'image', 'audio'],
                    ['fullScreen', 'showBlocks', 'codeView'],
                    ['preview', 'print'],
                ],
                imageAccept: '.png, .jpg, .svg, .gif, .jpeg',
                imageUploadUrl: Environment.fileStorageApi + '/upload/true',
                imageUploadHeader: {
                    'Authorization': 'Bearer ' + cache.get('___TOKEN___'),
                    'AUTH_SCOPE': Environment.authScope
                }
            }
        );
    }
}

class CheckBox extends InputField {
    constructor(field) {
        super(field);
        this.field = field
    }

    create(value) {
        let label = this.getField().getData().translation
        this.rowWrapper = super.createFieldDiv()

        let options = new MagicTrueFalseSelectorOptions(label, this.field.getData().checked, true, false, true)
        this.selector = new MagicTrueFalseSelector(options).draw(this.rowWrapper);

        return this.rowWrapper;
    }

    setValue(val) {
        if (_.isNull(val)) {
            this.selector.setValue(this.field.getData().checked)
            return;
        }
        let value = val[this.getField().getField()];
        this.selector.setValue(value)
    }

    setDirectValue(value) {
        this.selector.setValue(value)
    }

    read() {
        return this.selector.getValue()
    }
}

class CollectionField extends InputField {

    create() {
        this.rowWrapper = super.createFieldDiv();
        let row = this.createContainer().appendTo(this.rowWrapper);
        this.showLabel(row);
        this.showDescription(row)
        this.input = h.input('text').appendTo(row);
        return this.rowWrapper;
    }

    read() {
        return this.input.val().split(" ")
    }

    setValue() {

    }

    setDirectValue(value) {
        this.input.text(value.join(' '), false);
    }

}

class DictionaryChooser extends ObjectChooser {
    constructor(field) {
        super(field);
    }

    setValue(data) {
        if (data && data.value) {
            DictionaryChooser.getDictionaryById(data.value, (data) => {
                super.setValue({[this.getField().getField()]: data.content[0]});
            })
        } else super.setValue(null);
    }

    static getDictionaryById(id, callback) {
        let req = new MagicRequest();
        req.query.push(
            {
                "field": "id",
                "value": +id,
                "searchName": "",
                "dataType": "dictionary",
                "type": "="
            }
        )
        Executor.runPostWithPayload(Environment.dataApi + "/api/v2/shared/dictionary/root/fetch/data", callback, req)
    }

    isDictionary = () => true;
}


class ReferencedObjectChooser extends ObjectChooser {

    constructor(field) {
        super(field);

    }

    prepareData(data) {

        if (_.isEmpty(data)) {
            return data;
        }

        if (_.isObject(data)) {
            return data;
        }

        let request = ObjectChooser.getFieldRequest(this.getField().getField(), this.getParent())

        let res;
        request.copy().async(false).byId(data, (resp) => {
            res = resp
        });
        return res;
    }

    getId(value, isCustom, fieldName) {
        let id = null;

        if (isCustom) {
            id = value.value;
        } else {
            id = value[fieldName];
        }
        return id;
    }
}
