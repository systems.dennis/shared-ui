/**
 * Version 2.0. Introduces SearchCriteria - and independent class that supports handling / reading data for the
 * search field.
 */


class ISearchCriteria {
    /**
     * a reference to the field the search is for
     */
    #field;

    /**
     * A type of the search
     */
    #type;

    #objectComponent;

    static NO_VALUE = '#no_value#'

    static NULL = {'label': "global.search.is_null", 'value': 'is_null'};
    static NOT_NULL = {'label': "global.search.not_null", 'value': 'not_null'};
    static EQUALS = {'label': "global.search.equals", 'value': '='};
    static NOT_EQUALS = {'label': "global.search.does.not.equal", 'value': '!='};
    static IN = {'label': "global.search.in", 'value': 'in'};
    static NOT_IN = {'label': "global.search.not_in", 'value': 'not_in'};
    static GREATER = {'label': "global.search.more", 'value': '>'};
    static LESS = {'label': "global.search.less", 'value': '<'};
    static GREATER_OR_EQUAL = {'label': "global.search.more.or.equal", 'value': '>='};
    static LESS_OR_EQUAL = {'label': "global.search.less.or.equal", 'value': '<='};
    static BEGINS = {'label': "global.search.begins.with", 'value': '...%'};
    static NOT_BEGINS = {'label': "global.search.not.begins.with", 'value': '!...%'};
    static ENDS = {'label': "global.search.ends.with", 'value': '%...'};
    static NOT_ENDS = {'label': "global.search.not.ends.with", 'value': '!%...'};
    static CONTAINS = {'label': "global.search.contains", 'value': '%...%'};
    static NOT_CONTAINS = {'label': "global.search.not.contains", 'value': '!%...%'};
    static TYPE_ARR = [
        ISearchCriteria.NULL,
        ISearchCriteria.NULL,
        ISearchCriteria.NOT_NULL,
        ISearchCriteria.EQUALS,
        ISearchCriteria.NOT_EQUALS,
        ISearchCriteria.IN,
        ISearchCriteria.NOT_IN,
        ISearchCriteria.GREATER,
        ISearchCriteria.LESS,
        ISearchCriteria.GREATER_OR_EQUAL,
        ISearchCriteria.LESS_OR_EQUAL,
        ISearchCriteria.BEGINS,
        ISearchCriteria.ENDS,
        ISearchCriteria.NOT_ENDS,
        ISearchCriteria.CONTAINS,
        ISearchCriteria.NOT_CONTAINS,
    ]

    /**
     * A type of the search comparison
     * @type defaul : equals
     */

    #searchType = ISearchCriteria.EQUALS.value;

    constructor(field, type) {
        this.#field = field;
        this.#type = type;

    }

    /**
     * Initialize search with the concrete value. Normally should be used when an existing criteria as applied
     * @param value
     * @returns {ISearchCriteria}
     */
    init(value = null) {
        this.#objectComponent = this.createComponent(value);
        return this;
    }

    /**
     * This method is only called in init method.
     * This method cannot be called in the constructor of ISearchCriteria because it cannot know anything about special
     * fields
     *
     * Should create a basic select component which later will be used by getComponent() method;
     * @param value
     * @returns {*}
     */
    createComponent(value) {
        return h.span("no_component")
    }

    getField() {
        return this.#field;
    }

    setValue(val) {

    }

    getType() {
        return this.#type;
    }

    /**
     * Holds the current comparing data - how to compare data in the backend
     * Ex.: = , != , %...% , etc.
     * @returns {string}
     */
    getSearchType() {
        return this.#searchType;
    }

    /**
     * NO_VALUE should be returned if the condition doesn't yet ready to be quired
     * Ex.: NO_VALUE will be thrown when field is selected but there is no value given (except null / not null)
     * @returns {string}
     */
    getValue() {
        return ISearchCriteria.NO_VALUE;
    }

    /**
     * This should return a component that draws a selection
     * Ex.: for remote chooser div - with selection, for input search - text field and so on/
     * @returns {*}
     */
    getComponent() {
        return this.#objectComponent;
    }

    /**
     * When property is selected, then this trigger is called. It should redraw comparator types<p>
     * Ex.: For string it possible to have contains operator, but for dates it is not possible
     * so we have to rerender that types in typeComboBox
     * @param dropDown
     */
    updateTypeDropDown(dropDown) {

    }

    /**
     * don't forget to use super.changeSearchType()
     *
     * This method changes the search type to the one selected by user in comparation type combobox
     * Ex.: Triggered when user selects "=" type instead of "!="
     * @param type
     */
    changeSearchType(type) {
        this.#searchType = type;
    }

    toTag(criteria) {
        return h.div("no_tag_impl").text("no_tag_impl", false);
    }


    buildTag(text, criteria) {

        let tagValueContainer = h.div("tag_value")
            .text("global.pages.search." + text.field)
            .andText(' ', false)
            .andText(this.translateSearchType(text.type), true)
            .andText(text.value, false);


        let _this = this;
        return h.div("tag_value_container").add(tagValueContainer).add(h.img("rm.png")
            .cl("search_tag_remove").wh(16).click(function () {
                criteria.remove()
                tagValueContainer.parent().remove();
            }));
    }

    translateSearchType(text) {
        let type = ISearchCriteria.TYPE_ARR.find((e) => {
            if (_.e(e['value'], text)) {
                return e.label
            }
        });
        return type ? type['label'] : text
    }

}

/**
 * Base for all search criteria <b>also presenting INTEGER TYPE<b>
 */
class AbstractSearchField extends ISearchCriteria {


    constructor(field, value = null) {
        super(field, value);
    }

    getValue() {
        try {
            return parseInt(this.getComponent().val());
        } catch (e) {
            return AbstractSearchField.NO_VALUE
        }
    }

    createComponent(value) {
        return h.numberElement().text(value);
    }

    setValue(val) {
        this.getComponent().text(val, false);
    }

    changeSearchType(type) {
        super.changeSearchType(type);

        if (_.isOneOf(type, AbstractSearchField.NOT_NULL.value, AbstractSearchField.NULL.value)) {
            this.getComponent().hide();
        } else {
            if (this.getComponent() != null) this.getComponent().show();
        }
    }


    toTag(criteria) {

        let value = this.getValue();

        if (_.isOneOf(this.getSearchType(), AbstractSearchField.NOT_NULL.value, AbstractSearchField.NULL.value)) {
            value = '';
        } else {
            value = " '" + value + "'";
        }

        if (_e(value, AbstractSearchField.NO_VALUE)) return null;

        let textObj = {
            field: this.getField(),
            type: this.getSearchType(),
            value: value
        }

        return h.div("tag").add(this.buildTag(textObj, criteria));
    }

    updateTypeDropDown(typeDropDown) {
        typeDropDown.text(null);
        typeDropDown.add(dom.option(AbstractSearchField.EQUALS, true))
            .add(dom.option(AbstractSearchField.NOT_EQUALS, true))
            .add(dom.option(AbstractSearchField.GREATER, true))
            .add(dom.option(AbstractSearchField.LESS, true))
            .add(dom.option(AbstractSearchField.GREATER_OR_EQUAL, true))
            .add(dom.option(AbstractSearchField.LESS_OR_EQUAL, true))
            .add(dom.option(AbstractSearchField.NULL, true))
            .add(dom.option(AbstractSearchField.NOT_NULL, true));
    }

}

class SearchNothing extends AbstractSearchField {

    createComponent(value) {
        return h.input("hidden").text(SearchNothing.NO_VALUE);
    }
}

class SearchText extends AbstractSearchField {
    constructor(field, type) {
        super(field, type);
    }

    createComponent(value) {
        return h.textElement(value);
    }

    getValue() {
        return this.getComponent().val();
    }

    updateTypeDropDown(typeDropDown) {
        typeDropDown.text(null);
        typeDropDown.add(dom.option(AbstractSearchField.EQUALS, true))
            .add(dom.option(AbstractSearchField.NOT_EQUALS, true))
            .add(dom.option(AbstractSearchField.BEGINS, true))
            .add(dom.option(AbstractSearchField.NOT_BEGINS, true))
            .add(dom.option(AbstractSearchField.CONTAINS, true))
            .add(dom.option(AbstractSearchField.NOT_CONTAINS, true))
            .add(dom.option(AbstractSearchField.ENDS, true))
            .add(dom.option(AbstractSearchField.NOT_ENDS, true))
            .add(dom.option(AbstractSearchField.NULL, true))
            .add(dom.option(AbstractSearchField.NOT_NULL, true));
    }
}

class SearchDate extends AbstractSearchField {

    constructor(field, type) {
        super(field, type);
    }

    #chooser;
    #dateHolder = h.div("row").cl("date-row");
    #value;
    #options;

    listener = (event) => {
        if (_e(event.eventName, DateEvent.DATE_SELECTED)) {

            this.#value = event.data;
        }
    }

    setValue(val) {
        this.#chooser.setNewDate(val);

        this.#value = val;
    }

    createComponent(value) {
        let val = value ? value : "";

        this.#options = new DateOptions(this.#dateHolder, ".", "dd.mm.yyyy", val, this.listener, false);

        this.#chooser = new MagicDateChooser(this.#options).init();


        if (val) {
            this.#dateHolder.text(val, false)
        } else {
            this.#dateHolder.text("pages.filechooser.select")
        }

        return this.#dateHolder;
    }

    getValue() {
        return this.#value;
    }

    updateTypeDropDown(typeDropDown) {
        typeDropDown.text(null);
        typeDropDown.add(dom.option(AbstractSearchField.EQUALS, true))
            .add(dom.option(AbstractSearchField.NOT_EQUALS, true))
            .add(dom.option(AbstractSearchField.LESS, true))
            .add(dom.option(AbstractSearchField.GREATER_OR_EQUAL, true))
            .add(dom.option(AbstractSearchField.LESS_OR_EQUAL, true))
            .add(dom.option(AbstractSearchField.NULL, true))
            .add(dom.option(AbstractSearchField.NOT_NULL, true));
    }

}

class SearchBoolean extends AbstractSearchField {
    constructor(field, type) {
        super(field, type);
    }

    setValue(value) {
        dom.setCboValue(this.getComponent(), value + '')

    }

    createComponent(value) {
        return dom.select('').cl("last_criteria_field")
            .add(dom.option({label: 'global.true', value: 'true'}, true))
            .add(dom.option({label: 'global.false', value: 'false'}, true))
            .add(dom.option({label: 'global.unset_boolean', value: 'null'}, true));
    }

    updateTypeDropDown(typeDropDown) {
        typeDropDown.text(null);
//
        typeDropDown.add(dom.option(AbstractSearchField.EQUALS, true))
            .add(dom.option(AbstractSearchField.NOT_EQUALS, true))
            .add(dom.option(AbstractSearchField.NULL, true))
            .add(dom.option(AbstractSearchField.NOT_NULL, true));
    }

    getValue() {
        var selected = dom.selectedOption(this.getComponent());

        if (_e(selected.value, 'null')) {
            return selected.value;
        }
        return _e(selected.value, 'true')
    }

}


class SearchChooser extends AbstractSearchField {
    static MODE_SINGLE = 1;
    static MODE_MULTI = 2;
    #mode = SearchChooser.MODE_SINGLE;
    #searchName;
    #id;
    #searchField;
    #valueHolder = [h.input("hidden")]
    #parent;
    #clickedchooser

    constructor(field, type, parent) {
        super(field, type);
        this.#parent = parent
    }

    init(el) {

        this.#id = el.id;
        this.#searchField = el.searchField;
        this.#searchName = el.searchName;
        super.init(el);
        return this;
    }


    setValue(value, rebuildFunc) {

        if (_.e(this.#mode, SearchChooser.MODE_SINGLE)) {
            this.#valueHolder[0].text(value, false);
            new MagicRemoteChooser(this.getComponent(), this.createHeader(), this.#valueHolder[0], null, this.createHeader())
                .getDataById(value, this.getSearchName(), rebuildFunc);
        } else {
            this.getComponent().text(null)
            let innerSelector = h.div('inner_selector').appendTo(this.getComponent());
            h.divImg("add.png.png").click(() => {
                this.createAddableChooser(innerSelector)
            }).wh(16).appendTo(innerSelector);
            _.each(value, (item) => {
                this.createAddableChooser(innerSelector, item, rebuildFunc);
            })
        }
    }

    createComponent(value) {
        let component = h.div('input_chooser');

        this.#valueHolder[0].setData("type", this.getSearchType());

        new MagicRemoteChooser(component, this.createHeader(), this.#valueHolder[0], null, this.createHeader(), this.#parent.getList().getOptions());

        return component;
    }

    toTag(criteria) {

        let value = this.getComponent().get().cloneNode(true);

        if (_.isOneOf(this.getSearchType(), AbstractSearchField.NOT_NULL.value, AbstractSearchField.NULL.value)) {
            value = '';
        }

        if (_e(value, AbstractSearchField.NO_VALUE)) return null;

        let textObj = {
            field: this.getField(),
            type: this.getSearchType(),
            value: ''
        }

        return h.div("tag").add(this.buildTag(textObj, criteria).add(value));

    }

    getSearchName() {
        return this.#searchName;
    }

    getId() {
        return this.#id;
    }

    setId(id) {
        this.#id = id;
    }

    createHeader() {
        let _this = this;
        return {
            getData() {
                return {
                    searchName: _this.#searchName,
                    searchField: _this.#searchField
                }
            },
            getField() {
                return _this.getField();
            },
            getType() {
                return 'object_chooser'
            }
        };
    }

    updateTypeDropDown(typeDropDown) {
        typeDropDown.text(null);
        typeDropDown.add(dom.option(AbstractSearchField.EQUALS, true))
            .add(dom.option(AbstractSearchField.NOT_EQUALS, true))
            .add(dom.option(AbstractSearchField.IN, true))
            .add(dom.option(AbstractSearchField.NOT_IN, true))
            .add(dom.option(AbstractSearchField.NULL, true))
            .add(dom.option(AbstractSearchField.NOT_NULL, true));
    }

    changeSearchType(val) {
        super.changeSearchType(val)

        if (_.isOneOf(val, ISearchCriteria.IN.value, ISearchCriteria.NOT_IN.value)) {
            if (_e(this.#mode, SearchChooser.MODE_SINGLE)) {
                //change here to multiple
                this.#mode = SearchChooser.MODE_MULTI
                this.rebuildPanel();
            }
        } else {
            if (_e(this.#mode, SearchChooser.MODE_MULTI)) {
                //change here to single
                this.#mode = SearchChooser.MODE_SINGLE;
                this.rebuildPanel();
            }
        }


    }

    rebuildPanel() {
        let component = this.getComponent().text(null);

        this.#valueHolder = [];
        if (_e(this.#mode, SearchChooser.MODE_SINGLE)) {
            this.#valueHolder.push(h.input('hidden'));
            let remoteOptions = new MagicListOptions();
            remoteOptions.setFormFieldListener(this.listner)
            new MagicRemoteChooser(component, this.createHeader(), this.#valueHolder[0], null, this.createHeader(), remoteOptions);
        } else {
            let innerSelector = h.div('inner_selector').appendTo(component);
            h.divImg("add.png.png").click(() => {
                this.createAddableChooser(innerSelector)
            }).wh(16).appendTo(innerSelector);
            this.createAddableChooser(innerSelector);
        }

    }

    listner(event) {

        if (_.e(event.type, MagicRemoteChooser.EVENT_ITEM_SELECTED) && _.notNull(this.#clickedchooser)) {
            h.from(this.#clickedchooser).last().show();
            this.#clickedchooser = null;
        }
    }

    createAddableChooser(inWhere, value = null, rebuildFunc) {

        let _this = this;
        let valueHolder = h.input(dom.HIDDEN);
        this.#valueHolder.push(valueHolder);
        let item = h.div("container").click((e) => this.#clickedchooser = e);
        let select = h.div("item");
        this.del = h.divImg("rm.png").wh(16).click(() => {
            item.remove();
            //hm how can he add back data?
            valueHolder.setData('removed', true);
        }).hide();
        let remoteOptions = new MagicListOptions();
        remoteOptions.setFormFieldListener((event) => {
            this.listner(event)
        })

        let chooser = new MagicRemoteChooser(select, this.createHeader(), valueHolder, null, this.createHeader(), remoteOptions);

        if (_.notNull(value)) {
            chooser.getDataById(value, this.getSearchName(), rebuildFunc)
        }
        item.add(select).add(this.del);
        inWhere.add(item);

    }

    getValue() {
        return _e(SearchChooser.MODE_SINGLE, this.#mode) ? parseInt(this.#valueHolder[0].val()) : this.getValues();

    }

    getValues() {
        let res = [];

        _.each(this.#valueHolder, (value) => {
            if (!value.getData('removed')) {
                res.push(parseInt(value.val()));
            }
        })
        return res;
    }
}

class CriteriaObject {
    static CRITERIA_LINE = 'criteria_element'
    #searchElement;
    #dropDownType = dom.select("type").cl("dropdown type-search-dropdown");
    #query;
    #parent;
    #dropDownFieldName;
    #rebuildTagsFunc;
    #searchField;

    #container = h.div(CriteriaObject.CRITERIA_LINE);

    static INTEGER_TYPE = 'integer';
    static STRING_TYPE = 'text';
    static DATE_TYPE = 'date';
    static DATETIME_TYPE = 'datetime';
    static CHECKBOX_TYPE = 'checkbox';
    static REMOTE_CHOOSER = 'object_chooser';
    static FIELD = 'field';
    static NAME = 'name';
    static TYPE = 'type';

    setValue(val) {
        return this.#searchElement.setValue(val);
    }

    getSearchType() {
        return this.#searchElement.getSearchType();
    }

    getContainer() {
        return this.#container
    }

    setSelectedField(field, compType = null) {
        dom.setCboValue(this.#dropDownFieldName, field)
        this.createComponents(dom.selectedOption(this.#dropDownFieldName, true))
        if (_.notNull(compType)) {
            dom.setCboValue(this.#dropDownType, compType)

            this.#dropDownType.get().onchange()
        }
    }

    getSearchCriteria() {
        return this.#searchElement;
    }

    constructor(dropDownFieldName, query = null, rebuildTagsFunc, searchField) {
        this.#query = query
        this.#dropDownFieldName = dropDownFieldName;
        this.#searchField = searchField;
        this.#rebuildTagsFunc = rebuildTagsFunc;
        this.#searchElement = new SearchNothing(null, null);
        this.assignListenerToField();
        this.#dropDownFieldName.get().onchange();
        this.#dropDownType.get().onchange();

    }

    assignListenerToField() {
        this.#dropDownFieldName.change((e) => {
            if (_.isNull(e.val())) {
                e.parent().remove();
                return;
            }

            let selectedOption = h.from(dom.selectedOption(e));

            this.createComponents(selectedOption);

            this.#dropDownType.get().onchange();
            this.#searchField.fireEvent(SearchEvent.DROPDOWN_NAME_FIELD_CHANGED, this)
        })
    }

    createComponents(selectedOption) {
        let dataField = selectedOption.getData(CriteriaObject.FIELD);
        let type = selectedOption.getData(CriteriaObject.TYPE);
        let dataName = selectedOption.getData(CriteriaObject.NAME);
        let field = selectedOption.val();


        let _this = this;
        this.#dropDownType.text(null);


        if (_e(type, CriteriaObject.STRING_TYPE)) {
            this.#searchElement = new SearchText(field, type).init();

        }
        if (_e(type, CriteriaObject.INTEGER_TYPE)) {
            this.#searchElement = new SearchText(field, type).init();

        }

        if (_.isOneOf(type, CriteriaObject.DATE_TYPE, CriteriaObject.DATETIME_TYPE)) {
            this.#searchElement = new SearchDate(field, type).init();
        }
        if (_e(type, CriteriaObject.REMOTE_CHOOSER)) {
            this.#searchElement = new SearchChooser(field, type, this.#searchField).init({
                'searchName': dataName,
                'searchField': dataField
            });
        }

        if (_e(type, 'collection')) {

        }
        if (_e(type, CriteriaObject.CHECKBOX_TYPE)) {
            this.#searchElement = new SearchBoolean(field, type).init()
        }

        if (_.notNull(this.#searchElement)) {
            this.#searchElement.updateTypeDropDown(this.#dropDownType)

            this.#dropDownType.change((dropDown) => {
                _this.#searchElement.changeSearchType(dropDown.val());
                _this.build()
            })

            if (this.#query) {
                dom.setCboValue(this.#dropDownType, this.#query.type);
                this.#dropDownType.get().onchange();
                this.#searchElement.setValue(this.#query.value, this.#rebuildTagsFunc);
            }
        }


    }

    build(parent = null) {
        if (_.notNull(parent)) this.#parent = parent;


        if (_.isNull(this.#searchElement)) {
            return h.div("criteria_element").text("no_criteria_builder")
        }
        let _this = this;
        this.#container.text(null);
        this.#container.add(this.#dropDownFieldName).add(this.#dropDownType)
            .add(this.#searchElement.getComponent())

            .add(h.img("/remove.png.png", 24).cl("rm_criteria").click(() => {
                _this.remove()
            }));

        return this.#container;
    }

    remove() {
        this.#container.remove();
        if (_.notNull(this.#parent)) {
            this.#parent.removeSearchCriteria(this);
        }

    }

    getContent() {
        return this.#container;
    }

}

class SearchForm {
    static SEARCHABLE = "searchable"
    #data = {};

    #searchFields = []

    form;
    #parent;
    #needSavedItems = true;
    #content;
    #delay = 500;
    #tag = h.div("tags");
    #searchBox;
    #showTemplates = [];
    #toSaveTemplate = [];
    #searchByEnter = false;

    addSearchField(field) {
        this.#searchFields.push(field)
    }

    getFields() {
        return this.#searchFields;
    }

    findByFirstBySearchType(type) {
        return _.each(this.#searchFields, (searchField) => {
            if (_e(searchField.getSearchCriteria().getField(), type)) {
                return searchField
            }
        })
    }

    getSearchFields() {
        return this.#searchFields;
    }

    setNeedSavedItems(val) {
        this.#needSavedItems = val;
        return this;
    }

    setQuickSearchByEnter(val){
        this.#searchByEnter = val;
        return this;
    }

    getQuickSearchByEnter(){
       return this.#searchByEnter;
    }

    removeSearchCriteria(criteria) {
        this.#searchFields.splice(this.#searchFields.indexOf(criteria), 1);
        this.#parent.drawValues();
        this.visibilitySaveTemplatesImgs();
    }

    clear() {
        this.#searchFields = [];
        this.form.text(null);
        this.rebuildTags();
    }

    getSearchBox() {
        return this.#searchBox;
    }

    getLast() {
        return this.#searchFields[this.#searchFields.length - 1];
    }

    fireEvent(eventName, additional) {
        if (_.isNull(this.#parent) || _.isNull(this.#parent.getOptions().getSearchListener())) {
            return;
        }
        let magicSearchEvent = new SearchEvent(eventName, this.#parent, additional);
        this.#parent.getOptions().getSearchListener()(magicSearchEvent);
    }

    show() {
        this.#content.showModal();
        this.fireEvent(SearchEvent.SEARCH_ADVANCED_OPEN, this)
    }

    hide() {
        this.#content.hideModal();
    }

    constructor(data, id, list, delay = 500) {
        this.#data = data;
        this.#parent = list;
        this.#delay = delay;

    }

    getDefaultField() {
        return this.#data['defaultField']
    }

    getList() {
        return this.#parent;
    }

    getForm() {
        return this.form;
    }


    rebuildTags() {
        this.#tag.text(null);
        _.each(this.#searchFields, (searchField) => {
            let tag = searchField.getSearchCriteria().toTag(searchField);
            if (tag != null) {
                this.#tag.add(tag);
            }
        })
    }

    build() {
        let searchBox = dom.createModal(dom.REMOVE_TYPE_HIDE).cl("search_form").hide();
        this.#searchBox = searchBox;
        let _this = this;
        this.#content = searchBox;
        let searchFieldContainer = h.div("search_box_container")

        let drawTo  = this.getList().getOptions().getDrawSearchFieldTo();


        this.search = h.input("text").placeHolder('global_search_placeholder').cl("quick_search")
            .appendTo(drawTo ?  drawTo :  searchFieldContainer);


        let advancedSearch = h.div('table-btns-panel').get();
        h.divImg("advanced_search.png.png").wh(16).cl("advanced_search").appendTo(advancedSearch).appendTo(advancedSearch);
        this.saveTamplateWindow = new SaveTemplateWindow(searchFieldContainer, this);
        if (this.#needSavedItems) {
            this.showFromTemplate = new ShowFromTemplate(searchFieldContainer, this);
            this.showFromTemplate.buildSavedItems();
        }

        searchFieldContainer.appendChild(advancedSearch);

        let reset = h.div('table-btns-panel').get();
        reset.appendChild(h.divImg('reset.png.png').wh(16).cl("reset_button_of_search").click(function () {
            _this.getList().drawValues();
            dom.successMessage();
        }).get());
        searchFieldContainer.appendChild(reset);

        advancedSearch.onclick = function () {
            _this.openAdvancedSearchWindow()

        }

        this.inputEvent();

        h.tag("h2").text("global.pages.search.params.title").appendTo(searchBox);
        let actives = h.div('box_templates').appendTo(searchBox);
        this.form = h.div("search_elements").appendTo(searchBox);
        let addCriteria = h.a("#", "global.app.list.search.add_filter", true)
            .click(() => {
                _this.addNewCriteria();
                _this.visibilitySaveTemplatesImgs()
            });

        let addCp = h.div("buttons").add(addCriteria).appendTo(searchBox);

        h.input("button").text("global.app.list.search.cancel").click(
            () => {
                _this.closeWindow(searchBox)
            }).appendTo(addCp);

        h.input("button").text("global.app.list.search.apply").click(() => _this.applySearch()).appendTo(addCp);

        h.divImg("close.png.png").wh(16).cl("search_fields_close")
            .click(() => {
                _this.closeWindow(searchBox);
                // _this.visibilityTemplatesImgs();
            }).appendTo(searchBox);


        this.#parent.getSearchAndSettingsBar().add(searchFieldContainer);
        this.#parent.getSearchAndSettingsBar().add(searchBox);

        this.inFormSaveTempleButton = h.div('div_image')
            .cl("save_template")
            .click(() => {
                this.saveTamplateWindow.show()
            })
            .appendTo(actives);

        this.inPanelSaveTempleButton = h.div('div_image')
            .cl("save_template")
            .click(() => {
                this.saveTamplateWindow.show()
            })
            .appendTo(h.from(searchFieldContainer));

        this.inFormShowTempleButton = h.div('div_image')
            .cl("template_list")
            .click(() => {
                this.showFromTemplate.show()
            })
            .appendTo(actives);

        this.inPanelShowTempleButton = h.div('div_image')
            .cl("template_list")
            .click(() => {
                this.showFromTemplate.show()
            })
            .appendTo(h.from(searchFieldContainer));


        this.visibilitySaveTemplatesImgs();

        searchFieldContainer.add(this.#tag);

        this.fireEvent(SearchEvent.SEARCH_CREATED, [this.#content, searchFieldContainer])

    }

    inputEvent() {
        if(this.#searchByEnter){
            this.search.onKey((input, keyEvent)=>{
                if(_e(keyEvent.code, "Enter")){
                    this.qiuckSearchFunc(input.value)
                    this.fireEvent(SearchEvent.SEARCH_QUICK_ENTER, {"keyEvent": keyEvent, "input": input})
                }
                this.fireEvent(SearchEvent.SEARCH_QUICK_INPUT, {
                "keyEvent": keyEvent,
                "input": input,
                "searchFunc": (q)=>{this.qiuckSearchFunc(q)}})
            })
        }else{
            this.search.inputFunc(x => {
                _debounce(() => {
                    this.qiuckSearchFunc(x.value)
                }, this.#delay)()
            });
        }
    }

    qiuckSearchFunc(x){
            let defaultField = this.getDefaultField();
            let criteria = this.findByFirstBySearchType(defaultField);
            if (x.trim().length < 1 && _.notNull(criteria)) {

                this.removeSearchCriteria(criteria);
                criteria.getContent().remove();
                this.applySearch()
                return;
            }

            if (_.isNull(criteria)) {
                criteria = this.addNewCriteria();
                criteria.setSelectedField(defaultField, ISearchCriteria.CONTAINS.value);
                criteria.build(this);
            }

            criteria.setValue(x);
            this.applySearch();
            this.visibilitySaveTemplatesImgs();
    }



    openAdvancedSearchWindow() {
        this.#content.showModal();
        dom.linkTo(this.#content, this.search);
    }


    visibilityShowTemplatesImgs() {
        if (_.e(this.showFromTemplate.getData().length, 0)) {
            this.inFormShowTempleButton.hide();
            this.inPanelShowTempleButton.hide();
        } else {
            this.inFormShowTempleButton.show();
            this.inPanelShowTempleButton.show();
        }
    }

    visibilitySaveTemplatesImgs() {
        if (_.e(this.#searchFields, 0)) {
            this.hideSaveTemplatesImg()
        } else {
            this.showSaveTemplatesImg()
        }
    }

    showSaveTemplatesImg() {
        this.inFormSaveTempleButton.show();
        this.inPanelSaveTempleButton.show();
    }

    hideSaveTemplatesImg() {
        this.inFormSaveTempleButton.hide();
        this.inPanelSaveTempleButton.hide();
    }

    refreshSaveTemlate() {
        this.showFromTemplate.buildSavedItems()
    }

    addNewCriteria(query) {
        let criteria = this.createSearchCriteria(query);
        this.addSearchField(criteria);
        this.form.add(criteria.build(this))
        return criteria;
    }

    applySearch() {
        this.rebuildTags();
        this.fireEvent(SearchEvent.SEARCH_APPLY_SEARCH_PERFORMED, this)
        this.getList().drawValues();
    }

    closeWindow(window) {
        window.cl("hidden");
    }


    getSearchCriteria() {
        let res = [];
        _.each(this.#searchFields, (field) => {


            field = field.getSearchCriteria();

            //if the search field is not yet ready or data is wrong
            if (_e(field.getValue(), AbstractSearchField.NO_VALUE)) return;

            res.push({
                'field': field.getField(),
                "value": field.getValue(),
                "searchName": field.getSearchName ? field.getSearchName() : null,
                "dataType": field.getType(),
                "type": field.getSearchType()
            })

        })

        return res;
    }


    createSearchCriteria(query = null) {
        let _this = this
        let dropDownFieldName = dom.select("criteria_element_field");
        utils.each(this.#data ['fields'], (field) => {
            if (field[SearchForm.SEARCHABLE]) {
                dom.option({label: _.$(field['translation']), value: field[CriteriaObject.FIELD]})
                    .setData(CriteriaObject.TYPE, field['searchType'])
                    .setData(CriteriaObject.FIELD, field['searchField'])
                    .setData(CriteriaObject.NAME, field['searchName'])
                    .appendTo(dropDownFieldName)

            }
        })

        if (query) {
            dom.setCboValue(dropDownFieldName, query.field)
        }

        return new CriteriaObject(dropDownFieldName, query, function () {
            _this.rebuildTags()
        }, this)
    }
}

class SaveTemplateWindow {

    #window;
    #inputText;
    #checkInp;
    #where;
    #searchForm;

    constructor(where, searchForm) {
        this.#searchForm = searchForm
        this.#where = where
        this.buildWindow()
    }

    buildWindow() {
        this.#inputText = h.input('text')
        this.#checkInp = h.input('checkbox')
        this.#window = dom.createModal(dom.REMOVE_TYPE_HIDE)
            .cl("save_template_wrapper")
            .hide()
            .add(h.div('save_template_window')
                .add(dom.createCloseImage()
                    .click(() => {
                        this.#window.hideModal();
                    }))
                .add(h.div('save_template_title')
                    .text('global.template.save_filter_as_template'))
                .add(this.#inputText
                    .placeHolder('global.template.enter_name')
                    .cl('template_save-search-input'))
                .add(h.div('template_save-flex')
                    .add(this.#checkInp
                        .id('save_search_checkbox'))
                    .add(h.label(this.#checkInp, 'search_title')
                        .text('global.template.save.menu'))
                )
                .add(h.div("buttons")
                    .add(h.input('button')
                        .text('global.template.cancel')
                        .click(() => {
                            this.closeWindow()
                        }))
                    .add(h.input('button')
                        .text('global.template.save')
                        .cl('template_save_savebtn')
                        .click(() => {
                            this.toSave()
                        }))
                )
            ).appendTo(h.from(this.#where))

    }

    closeWindow() {
        this.#inputText.get().value = '';
        this.#window.hideModal();
    }

    show() {
        this.#window.showModal();
    }

    toSave() {
        let _this = this;
        this.#searchForm.getList().getOptions().getRequest().shared("table_settings")
            .add(_this.createTableSettingsPayLoad(this.#inputText, this.#checkInp), (data)=>{
                dom.successMessage();
                _this.closeWindow();
                _this.#searchForm.refreshSaveTemlate();
            })

    }


    createTableSettingsPayLoad(textInp, checkInp) {
        return {
            "topic": this.#searchForm.getList().getRootPath(),
            "magicQuery": this.#searchForm.getList().createPayload(true),
            "addToMenu": checkInp.get().checked,
            "name": textInp.get().value,
            "fieldsOrder": this.#searchForm.getList().getOrdering().getOrder()
        }
    }


}

class ShowFromTemplate {

    #searchForm
    #where
    #dataContainer
    #data = []

    constructor(where, searchForm) {
        this.#searchForm = searchForm;
        this.#where = where;
        this.buildWindow();
    }

    getData() {
        return this.#data
    }

    buildWindow() {
        this.#dataContainer = h.tag('ul')

        this.windowWrapper = dom.createModal(dom.REMOVE_TYPE_HIDE).cl('template_list_wrapp')
            .add(h.div('template_list_window')
                .add(dom.createCloseImage()
                    .click(() => this.closeWindow())
                    .wh(16)
                    .cl('template_list_wrapp-close'))
                .add(h.div('template_list_title')
                    .text('saved_search'))
                .add(h.input('text')
                    .placeHolder('global.search')
                    .inputFunc((elem) => {
                        this.searchItem(elem)
                    }))
                .add(h.div('template_div_ul')
                    .add(this.#dataContainer)))
            .appendTo(this.#where)
            .hide();

        this.buildSavedItems();

    }

    show() {
        this.windowWrapper.showModal();
        this.buildSavedItems();
    }

    closeWindow() {
        this.windowWrapper.hideModal();
    }

    buildSavedItems() {
        let _this = this;
        if (cache.get(Authorization.TOKEN_FIELD)) {
            let request  = this.#searchForm.getList().getOptions().getRequest();
            let originClone = request.copy(false);
            request.shared("table_settings")
                .request("listAll?topic=" +  encodeURIComponent(originClone.getFullPath()));

            request.run((data) => {
                _this.#dataContainer.text(null);
                _this.#data = data;
                _this.buildInSidePanel(data)
                _.each(data, (item) => {
                    h.tag('li')
                        .add(h.a('#')
                            .text(item.name, false)
                            .click(() => {
                                _this.promptForShow(item)
                            }))
                        .add(h.divImg('delete.png')
                            .click((elem) => {
                                _this.deleteItem(item, elem)
                            })
                            .cl("search_remote_container-rm-im")
                            .wh(16))
                        .appendTo(_this.#dataContainer);
                });
                _this.#searchForm.visibilityShowTemplatesImgs();
            })
        }
    }

    buildInSidePanel(data) {
        let _this = this;
        let sidePanel = h.from(document.querySelector(".saved_templates_side_menu"));
        if (_.isTrue(sidePanel)) {
            sidePanel.text(null)
            _.each(data, (item) => {
                if (item.addToMenu) {
                    h.tag('div')
                        .cl('temolate_in_side')
                        .add(h.a('#')
                            .text(item.name, false)
                            .click(() => {
                                _this.promptForShow(item)
                            }))
                        .add(h.divImg('delete.png')
                            .click((elem) => {
                                _this.deleteItem(item, elem)
                            })
                            .cl("search_remote_container-rm-im")
                            .wh(16))
                        .appendTo(sidePanel);
                }
            });
        }
    }

    deleteItem(item, elem) {

        let opts = new MagicPromptOptions("global.prompt.confirm_remove_template");
        opts.applyClick(MagicPromptButton.OK_LABEL, () => {
            opts.markNoRepeat()
            let _this = this

            this.#searchForm.getList().getOptions().getRequest().shared("table_settings").delete(id, ()=> {
                dom.successMessage();
                h.from(elem).parent().remove();
                _this.#searchForm.refreshSaveTemlate();
            });


            opts.closeSelf();
        }).setIsAction(MagicPromptButton.OK_LABEL, true)
            .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                opts.closeSelf();
            }).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
            .allowNoRepeat("global.prompt_no_repeat_remove_template").newPrompt();
    }


    promptForShow(item) {
        if (this.#searchForm.getForm().val()) {
            let opts = new MagicPromptOptions("global.prompt.confirm_delete_old_criteria");
            opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                this.setHeaderOrder(item);
                this.showTemplate(item);
                opts.markNoRepeat()
                opts.closeSelf();
            }).setIsAction(MagicPromptButton.OK_LABEL, true)
                .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                    opts.closeSelf();
                }).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                .allowNoRepeat("global.prompt_no_repeat_delete_old_criteria").newPrompt();
        } else {
            this.showTemplate(item);
            this.setHeaderOrder(item);
        }
    }

    showTemplate(item) {
        this.#searchForm.clear();
        _.each(item.magicQuery.query, (item) => {
            this.#searchForm.addNewCriteria(item)
        });
        this.closeWindow();
        this.#searchForm.applySearch();
    }

    setHeaderOrder(item) {
        this.#searchForm.getList().getOrdering().setOrdersFromTemplate(item.fieldsOrder)
    }

    searchItem(elem) {
        _.each(this.#dataContainer.childNodes(), (li) => {
            h.from(li).first().val().toLowerCase().includes(elem.value) ? h.from(li).show() : h.from(li).hide()
        })
    }
}
