class MagicSort {
    #parent;
    #res;
    #headerFields = [];
    #activeHeaderFields = [];

    constructor(parent) {
        this.#parent = parent
    }

    #content;
    #form
    #modalAbstractSort

    #sorted = []

    close() {
        this.#content.hideModal();
    }

    show() {
        this.#content.showModal();
    }

    getAbstractSort(){
        return this.#modalAbstractSort;
    }

    getParent(){
        return this.#parent
    }

    getSort() {
        return this.#modalAbstractSort.getSort()
    }

    build() {

        let sortable = this.#parent.getLastData().fields;

        this.#content = dom.createModal(dom.REMOVE_TYPE_HIDE).cl("sortable_panel").cl("sort_form").hide().appendTo(this.#parent.getContainer());

        h.div("sort_header").text("global_sort").appendTo(this.#content);
        let _this = this;

        let container = h.div("sort_data").appendTo(this.#content);

        this.#form = container;
        this.#modalAbstractSort = new AbstractSort(this.getParent())
        utils.each(sortable, (item) => {
            //always ignore actions
            if (!utils.isEmpty(item.actions) || utils.isFalse(item.sortable)) {
                return utils.CONTINUE;
            }
            let defSorting = this.#parent.getLastData().defaultSorting;

            let span = h.div("sort_content_row").setData("field", item.field).appendTo(container);
            h.tag("span").cl("sorting_title").text(item.translation).appendTo(span);
            let modalSort = new ModalSortHeader(item.field, this.#modalAbstractSort, span);
            if (utils.e(defSorting.key, item.field)) {
                modalSort.setStatus(defSorting.value)
            }
            this.#modalAbstractSort.addSortField(modalSort);
            modalSort.draw();
        });
        this.#modalAbstractSort.addModalContainer(container)

        h.div("close_inner_form").wh(24).cl("close_inner_form_settings").click(function () {
            _this.close();
        }).appendTo(_this.#content)


    }

    changeSorting() {

    }


}


class AbstractSort{

    #sortFields = [];
    #order = 0;
    #activeFields = [];
    #synhroning;
    #parent;
    #modalContainer;
    start = null;

    static ORDER = "order"
    static FIELD = "field"


    constructor(parent){
        this.#parent = parent;
        log.promisedDebug(()=> { return {"text" : "absrtact search instantate", parent : parent}})
    }

    addSortField(sortField){
        this.#sortFields.push(sortField);
    }

    getParent(){
        return this.#parent
    }

    setActiveFields(activeFields){
        this.#activeFields = activeFields
    }

    getModalContainer(){
        return this.#modalContainer
    }

    setModalContainer(modalContainer){
        this.#modalContainer = modalContainer;
    }

    setParent(parent){
        this.#parent = parent
    }

    addModalContainer(modalContainer){
        this.#modalContainer = modalContainer;
        this.draggableSettings();
    }

    draggableSettings(){
        _.each(this.#modalContainer.childNodes(), (modalField)=>{
            modalField.draggable(
            (e)=>{
                this.start = e.currentTarget
            },
            (e)=>{
                this.start = null
            }).dragOver((e)=>{
                h.from(e.currentTarget).cl('drag_over')
                e.preventDefault();

            }).dragLeave((e)=>{
                h.from(e.currentTarget).rcl('drag_over')

            }).dragDrop((e)=>{
                this.rebuildAfterDrug(e.currentTarget)
                h.from(e.currentTarget).rcl('drag_over')

            })
        }, true);   
    }

    rebuildAfterDrug(onDroped){
        if(_.isEmpty(h.from(onDroped).getData(AbstractSort.ORDER))){
            return
        }
        let order = h.from(onDroped).getData(AbstractSort.ORDER)
        let dragfield = h.from(this.start).getData(AbstractSort.FIELD);
        _.each(this.#sortFields, (dfield)=>{
            if(_e(dfield.getField(), dragfield)){
                let notInActive = true
                _.each(this.#activeFields, (activefield, i)=>{
                    if(_e(activefield.getField(), dfield.getField())){
                        //activefield == dragedfield/ We should only replace active field
                        this.#activeFields.splice(i, 1);
                        this.#activeFields.splice(order - 1, 0, dfield);
                        this.rebuildList();
                        notInActive = false;
                        return ""
                    }
                })

                if(notInActive){
                    dfield.setStatus(AbstractSortField.DESC);
                    this.#activeFields.splice(order-1, 0, dfield);
                    this.rebuildList();
                }
            }
        })
    }

    buildActive(setOrder = true){

        _.each(this.#sortFields, (field)=>{
            let res = _.find(this.#activeFields, (obj)=>{return _e(obj.getField(), field.getField())})

            if(field.isActive() && _.isNull(res)){       
                this.#activeFields.push(field);
                return
            }else if(!field.isActive() && _.notNull(res)){
                _.each(this.#activeFields, (field, i)=>{
                    if(_e(field.getField(), res.getField())){
                        this.#activeFields.splice(i, 1)
                        field.setOrder("");
                    }
                })
            }
        })
       
        setOrder && this.setOrders()
    }

    setOrders(){
        _.each(this.#activeFields, (fields, i)=>{
            fields.setOrder(i + 1);
        })
        this.reDrawAllFields();
    }


    //Returns sort
    getSort(){

        let sort = new Sort();
        _.each(this.#activeFields, (sortField)=>{
            sort.by(sortField.getField(), _e(sortField.getStatus(), AbstractSortField.DESC));
        });

        return sort
    }

    sync(synhroning){
        this.#synhroning = synhroning;
        if(!_.isNull(synhroning.getModalContainer())){
            this.#modalContainer = synhroning.getModalContainer();
        }
    }

    reDrawAllFields(){
        _.each(this.#sortFields, (sortField)=>{
            sortField.reDraw();

            if(_.notNull(this.#synhroning)){
                _.each(this.#synhroning.getSortFields(), (syncField)=>{
                    if(_e(sortField.getField(), syncField.getField())){
                        syncField.setOrder(sortField.getOrder());
                        syncField.setStatus(sortField.getStatus());
                        syncField.reDraw();  
                    }   
                })
                this.#synhroning.buildActive(false)
            }
        })
       
        this.rebuildFieldsInModalWindow();
    }

    rebuildFieldsInModalWindow(){

        _.each(this.#sortFields, (sortfield)=>{
            _.each(this.#modalContainer.childNodes(), (child)=>{
                if(_e(sortfield.getField(), child.getData(AbstractSort.FIELD))){
                    child.setData(AbstractSort.ORDER, sortfield.getOrder())
                }
            }, true)
        })

        this.sorted = [...this.#modalContainer.childNodes()]
        this.sorted.sort((a, b)=> this.toInt(a).compare(this.toInt(b))); 
        this.#modalContainer.text(null)
        _.each(this.sorted, (child)=>{
            child.appendTo(this.#modalContainer)
        }, true)
    }

    toInt(a){
        try{
            let b = parseInt(h.from(a).getData(AbstractSort.ORDER))
            if (isNaN(b)){
                throw Error('NaN');
            }
            return b
        }catch(error){
           if(_e(error.message.indexOf("NaN"),-1 )){
            log.debug(error.message);
           }
            return this.sorted.length + 1
        }
    }

    getSortFields(){
        return this.#sortFields;
    }

    rebuildList(){
        this.findList(this.getParent())?.drawValues()
    }

    findList(where){
        if (_.isNull(where)){
            return where;
        }
        if (where instanceof MagicList2){
            return where;
        } else {
            return this.findList(where.getParent());
        }
    }

}

class AbstractSortField{


    static NOT_USED = "null";
    static DESC = 'DESC';
    static ASC = 'ASC';
    #status = AbstractSortField.NOT_USED;
    #field;
    #order;
    #abstractSort;

    constructor(field, abstractSort, order = ''){
        this.#field = field;
        this.#order = order;
        this.#abstractSort = abstractSort;
    }


    setInitStatus(status){
        this.#status = status;
        this.#abstractSort.buildActive()
    }

    getOrder(){
        return this.#order;
    }

    setOrder(order){
        this.#order = order;
    }

    getStatus(){
        return this.#status;
    }

    getField(){
        return this.#field;
    }

    isActive(){
        return !_.e(this.#status, AbstractSortField.NOT_USED);
    }

    setStatus(status){
        this.#status = status;
    }

    sync(incomming){
        if(_e(incomming.getField(), this.getField())){
            this.clone(incoming)
        }
    }

    clone(incoming){
        this.setOrder(incoming.getOrder())
        this.setStatus(incoming.getStatus())
    }

    rebuildOrder(){
        this.#abstractSort.buildActive()
    }

    nextStatus(){
        if(_e(this.getStatus(), AbstractSortField.NOT_USED)){
            this.setStatus(AbstractSortField.DESC)
            return
        }
        if(_e(this.getStatus(), AbstractSortField.DESC)){
            this.setStatus(AbstractSortField.ASC)
            return
        }
        if(_e(this.getStatus(), AbstractSortField.ASC)){
            this.setStatus(AbstractSortField.NOT_USED)
            return
        }
    }

    getImageSrc(){
        if(_e(this.getStatus(), AbstractSortField.NOT_USED)){
            return "/images/sort.png".asMediaUrl();
        }
        if(_e(this.getStatus(), AbstractSortField.DESC)){
            return '/images/sort_desc.png'.asMediaUrl();
        }
        if(_e(this.getStatus(), AbstractSortField.ASC)){
            return "/images/sort_asc.png".asMediaUrl();
        }
    }

    rebuildList(){
        this.#abstractSort.rebuildList()
    }

    reDraw(){
 
       
    }

    appendToSort(sorting){
        if (this.isActive()){
            sorting.push({
                "field": this.#field,
                "desc": _e(this.getStatus(), AbstractSortField.DESC)
            })
        }
    }

}

class TableSortHeader extends AbstractSortField{
    #where;
    #abstractSort;
    image = h.img(this.getImageSrc()).wh(16)
    order = h.div('order_div').text("", false)


    constructor(field, abstractSort, where,){
        super(field, abstractSort)
        this.#where = where;

    }

    reDraw(){
        this.image.src(this.getImageSrc());
        this.order.text(this.getOrder(), false)
    }

    draw(){
        this.sortDiv = h.div("sortDiv")
        .add(this.image)
        .add(this.order)
        .click(()=>{
            this.nextStatus()
            this.rebuildOrder()
            this.reDraw()
            this.rebuildList()
        })
        .appendTo(this.#where)
    }


}

class ModalSortHeader extends AbstractSortField{
    #where;
    #abstractSort;
    image = h.img(this.getImageSrc())
    order = h.div('order_div').text("", false)


    constructor(field, abstractSort, where,){
        super(field, abstractSort)
        this.#where = where;
        this.#abstractSort = abstractSort;

    }

    reDraw(){
        this.image.src(this.getImageSrc());
        this.order.text(this.getOrder(), false)

       
    }

    draw(){
        this.sortDiv = h.div("sortDiv")
        .add(this.image)
        .add(this.order)
        .click(()=>{
            this.nextStatus()
            this.rebuildOrder()
            this.reDraw()
            this.rebuildList()
        })
        .appendTo(this.#where)
    }
}
