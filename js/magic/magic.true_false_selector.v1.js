class MagicTrueFalseSelectorOptions {

    static SWITCH = {
        select : "switch.selected.png.png",
        deselect : "switch.deselected.png.png"
    }

    static CHECKBOX = {
        select : "checkbox.selected.png.png",
        deselect : "checkbox.deselected.png.png"
    }

    static RADIO = {
        select : "radio-checked.svg",
        deselect : "radio-unchecked.svg"
    }

    #type;
    #showLabel;
    #clickableLabel;

    #label;
    #labelFirst;
    #componentListener;
    #defaultValue;
    #imageSelected;
    #imageDeSelected;
    #context
    #parent;
    #selectorValue;
    #attributes = {};
    #preventiveClick = false;
    #customLabelRender = null

    constructor(label = "",
                defaultValue = false,
                showLabel = true,
                labelFirst = false,
                toTranslate = false,
                clickableLabel = true,
    ) {
        this.#labelFirst = labelFirst;
        if(toTranslate){label = _.$(label)}
        this.#showLabel = showLabel;
        this.#label = label;
        this.#clickableLabel = clickableLabel;
        this.#defaultValue = defaultValue;
        this.setDisplayType(MagicTrueFalseSelectorOptions.CHECKBOX);
    }

    setCustomLabelRender(customLabelRender){
        this.#customLabelRender = customLabelRender
        return this;
    }

    getCustomLabelRender(){
        return this.#customLabelRender
    }


    setPreventiveClick(val){
        this.#preventiveClick = val;
        return this
    }

    getPreventiveClick(){
        return this.#preventiveClick;
    }

    setDisplayType(type){
        this.#imageSelected = type.select;
        this.#imageDeSelected = type.deselect;
        return this;
    }

    setAttribute(name, value){
        this.#attributes[name] = value
        return this
    }

    getAttrubutes(){
        return this.#attributes
    } 

    getAttributeByName(name){
        return this.#attributes[name]
    }

    setContext(context){
        this.#context = context;
        return this
    }

    setParent(parent){
        this.#parent = parent;
        return this
    }

    getParent(){
        return this.#parent;
    }

    setSelectorValue(value){
        this.#selectorValue = value;
        return this
    }

    getSelectorValue(){
        return this.#selectorValue;
    }

    getContext(){
        return this.#context
    }

    getImageSelected() {
        return this.#imageSelected;
    }

    getImageDeselected() {
        return this.#imageDeSelected;
    }

    getType() {
        return this.#type;
    }

    getDefaultValue() {
        return this.#defaultValue;
    }

    getLabel() {
        return this.#label;
    }

    isLabelFirst() {
        return this.#labelFirst;
    }

    setComponentListener(listener) {
        this.#componentListener = listener;
        return this;
    }

    getComponentListener() {
        return this.#componentListener;
    }

    isShowLabel() {
        return this.#showLabel;
    }

    isClickableLabel() {
        return this.#clickableLabel;
    }

    getInitValue(){
        return this.#defaultValue
    }

}

class MagicTrueFalseSelectorEvent {
    static VALUE_CHANGED = "value_changed";

    static VALUE_RENDERED = "value_rendered";

    static COMPONENT_CREATED = "component_created";
    static BEFORE_COMPONENT_CREATED = "before_component_created";

    currentValue;
    initValue;
    changed;
    component;

    isSelected(){
        return this.currentValue;
    }
}

class MagicTrueFalseSelector {
    #options;
    #image;
    #currentValue;
    #label;

    constructor(options) {
        this.#options = options;
    }

    getLabel(){
      return this.#label;
    }

    parsedValue(value) {
        return _.isTrue(value);
    }

    getOptions() {
        return this.#options;
    }

    getValue() {
        return _.isNull(this.#currentValue) ? this.#options.getDefaultValue() : this.#currentValue;
    }

    createImage() {
        this.#image = h.divImg(this.getValue() ? this.checkedImage() : this.uncheckedImage())
        return this.#image;
    }

    checkedImage() {
        return this.#options.getImageSelected();
    }

    uncheckedImage() {
        return this.#options.getImageDeselected();
    }

    setValue(val, fireEvent = true) {
        this.#currentValue = val;
        this.#image.src(this.getValue() ? this.checkedImage() : this.uncheckedImage());
        if (fireEvent) {
            this.fireEvent(MagicTrueFalseSelectorEvent.VALUE_CHANGED)
            this.fireEvent(MagicTrueFalseSelectorEvent.VALUE_RENDERED);
        }
    }

    isChanged() {
        return !_e(this.initValue(), this.getValue());
    }

    initValue() {
        return this.getOptions().getInitValue();
    }

    draw(at) {
        this.fireEvent(MagicTrueFalseSelectorEvent.BEFORE_COMPONENT_CREATED)

        let _this = this;
        let options = this.getOptions();
        let div = h.div("class_selector");
        div.addIf(options.isLabelFirst() && options.isShowLabel(), this.createLabel())
            .add(this.createImage().click(() =>{ _this.toggleValue()}, this.#options.getPreventiveClick()))
            .addIf(!options.isLabelFirst() && options.isShowLabel(), this.createLabel());
        h.from(at).add(div);
        this.fireEvent(MagicTrueFalseSelectorEvent.COMPONENT_CREATED)

        this.inited = true;
        return this;

    }

    toggleValue() {
        this.setValue(!this.getValue())
    }

    createLabel() {
        let el = this;
        let labelContainer = _.ofNullable(this.#options.getCustomLabelRender(), ()=> h.span("selector_label", this.#options.getLabel(), false))
        this.#label = labelContainer.clickIf(this.#options.isClickableLabel(), () => {
                el.toggleValue();
            }, this.#options.getPreventiveClick())

        return this.#label;
    }



    fireEvent(type) {
        let listener = this.getOptions().getComponentListener();
        if (_.isNull(listener)) {
            return;
        }

        let event = new MagicTrueFalseSelectorEvent();
        event.currentValue = this.getValue();
        event.changed = this.inited;
        event.component = this;
        event.initValue = this.initValue();
        event.type = type;
        event.parent = this.getOptions().getParent()
        event.selectorValue = this.getOptions().getSelectorValue()
        listener(event);
    }
}