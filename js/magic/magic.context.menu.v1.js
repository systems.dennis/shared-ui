class MagicMenuOptions {
  #trigger;
  #id;
  #parent;
  #actions = [];
  #allowedToClickClass;
  #listener;
  #actionStorage = {};
  #contextMenuToGroupTransformer = null;
  #translatePath = null
  #translate = true;
  #event = "contextmenu"

  constructor(trigger, info, parent, allowedToClickClass, contextMenuToGroupTransformer = null) {
      this.#parent = parent;
      this.#trigger = trigger;
      this.#allowedToClickClass = allowedToClickClass;
      if (info && info.id) {
          this.#id = info.id;
      }

      if(contextMenuToGroupTransformer) {
        this.#contextMenuToGroupTransformer = contextMenuToGroupTransformer;
      }

      this.buildActionStorage();
  }

  setContextMenuToGroupTransformer(func) {
    this.#contextMenuToGroupTransformer = func;
    return this
  }

  setTranslate(isTranslate){
    this.#translate = isTranslate;
    return this
  }

  getEvent(){
    return this.#event;
  }

  setEvent(event){
    this.#event = event;
    return this
  }

  getTranslate(){
    return this.#translate;
  }

  buildActionStorage() {
    this.#actionStorage = []
    if(_.notNull(this.#actions)  && this.#actions.length) {
      var isCustom = Boolean(this.#contextMenuToGroupTransformer);

      var actionToType = _.map(this.#actions, (el) => {
        if(isCustom) {
          return this.#contextMenuToGroupTransformer(el)
        }

        return el.name;
      })

      var uniq = actionToType;

      if(isCustom) {
        uniq = Array.from(new Set(actionToType));
      }

      var actionTransform = _.map(this.#actions, (item => {
          return {
            type: isCustom ? this.#contextMenuToGroupTransformer(item) : item.name,
            full: item
          }
        }));

      _.each(uniq, (el) => { this.#actionStorage[el] = [] })

      _.each(actionTransform, (el) => { this.#actionStorage[el.type].push(el) })
    }
  }

  setTranslatePath(translatePAth){
    this.#translatePath = translatePAth
  }

  getTranslatePath(){
    return this.#translatePath
  }

  addActions(actions) {
      _.each(actions, (action) => {
          this.#actions.push(action)
      })

      this.buildActionStorage();
      return this;
  }

  setListner(listner) {
      this.#listener = listner;
      return this;
  }

  getActions() {
      return this.#actions;
  }

  getState() {
    return this.#actionStorage
  }

  setActions(val) {
      this.#actions = val;
      this.buildActionStorage();

      return this;
  }

  fire(eventType, item, id, data, element) {

      this.#listener(new MagicMenuEvent(eventType, item, id, this.#parent, data, element));
  }

  getId() {
      return this.#id;
  }

  getTrigger() {
      return this.#trigger;
  }

  getListener() {
      return this.#listener;
  }

  getAllowedToClickClass() {
      return this.#allowedToClickClass;
  }

  getParent() {
      return this.#parent;
  }
}




class MagicMenu {

static TRANSLATE_PATH = "global.menu.actions.";

  #menu;
  #wrapper;
  #options;
  #data;

  constructor(options, data = null) {
      this.#options = options;
      this.#data = data;
      this.contextListener();
  }

  static CLASS_NAME = "context-menu__";
  static OPENED_ACTIONS_HOLDER = MagicMenu.CLASS_NAME + "holder-open";

  contextListener() {
    var trigger = this.#options.getTrigger();
      trigger.handleEvent(this.#options.getEvent(), (event) => {
          let taskItemInContext = this.clickInsideElement(trigger, this.#options.getAllowedToClickClass());
          if (taskItemInContext) {
              event.preventDefault();
              this.buildMenu(event);
          }
      })
  }

  getActions(){
    return this.#options.getActions()
  }

  keyupListener() {
      window.onkeyup = (e) => {
          if (e.keyCode === 27) {
              this.removeMenu();
          }
      }
  }

  rebuildActions(actions){
    this.#options.setActions(actions)
  }

  addAction(action){
    this.#options.getActions().push(action)
    this.#options.buildActionStorage();
  }


  removeActionByName(actionName){
    _.each(this.#options.getActions(), (action, i)=>{
      if(_e(action.name, actionName)){
        this.#options.getActions().splice(i,1)
      }
    })
  }

  resizeListener() {
      window.onresize = (e) => {
          this.removeMenu();
      };
  }


  wrapperListners() {
    if(_.notNull(this.#wrapper)) {
      this.#wrapper.click((el, event) => {
        event.stopPropagation()
        if (event.target === this.#wrapper.get()) this.removeMenu();
      })

      this.#wrapper.handleEvent(this.#options.getEvent(), (event) => {
          event.preventDefault();
          this.removeMenu();
      })
    }
  }

  onListnersInMenu() {
      this.wrapperListners();
      this.keyupListener();
  }

  removeMenu() {
      this.#wrapper.text(null).remove();
  }

  buildMenu(event) {
    var actions = this.#options.getActions();
    if (_.notNull(actions) && actions.length) {
      let pos = this.getPosition(event);

      this.#wrapper = h.div(MagicMenu.CLASS_NAME + "wrapper").appendToBody();
      this.#menu = h.div(MagicMenu.CLASS_NAME + 'menu').appendTo(this.#wrapper);
      this.subWrapper = h.div(MagicMenu.CLASS_NAME + 'menu').appendTo(this.#wrapper);

      this.#menu.style().left(pos.x)
      this.#menu.style().top(pos.y);

      var selected = 0;

      this.checkSelected(selected)

      var state = this.#options.getState();

      for(let key in state) {

        var current = state[key];

        if(current.length > 1){
          this.drawMultiActions(current);
        } else {
          this.drawItem(current[0].full)
        }
      }
      this.replaceMenu(pos);
      }

      this.onListnersInMenu();
  }

  drawMultiActions = (actions) => {
    var type = actions[0].type;

    var itemWrapper = h.div(MagicMenu.CLASS_NAME + 'item-wrapper')
    .add(h.div(MagicMenu.CLASS_NAME + type + '-icon'))
    .add(h.span("menu-text").text(MagicMenu.TRANSLATE_PATH + type)).id("menu_item_" + MagicMenu.TRANSLATE_PATH + type).appendTo(this.#menu);

    itemWrapper.add(h.div(MagicMenu.CLASS_NAME + "actions-holder").width(20).height(20));

    itemWrapper.click(() => {
      this.closeActionHolders();
      itemWrapper.toggle(MagicMenu.OPENED_ACTIONS_HOLDER);

      this.subWrapper.text(null);

      this.buildSubMenu(actions, itemWrapper)
    })
  }

  closeActionHolders = () => {
    this.#wrapper.eachOfClass(MagicMenu.OPENED_ACTIONS_HOLDER,(div) => {
      div.rcl(MagicMenu.OPENED_ACTIONS_HOLDER)
    }, false, true)
  }

  buildSubMenu = (actions, trigger) => {
    _.each(actions, (action) => {

      this.drawItem(action.full, this.subWrapper);
    })

    this.subWrapper.style().left(this.getSubActionsLeft()).top(this.getSubActionsTop(trigger, this.subWrapper))
  }

  getSubActionsLeft = () => {
    var menuWidth = this.#menu.getRect().width;
    var bodyWIdth = document.body.offsetWidth;
    var menuLeft = this.#menu.getOffsetLeft()
    var left = menuWidth + menuLeft;

    if(bodyWIdth < left + menuWidth) left = menuLeft - menuWidth;

    return left;
  }

  getSubActionsTop = (trigger, subMenu) => {
    var menuHeight = subMenu.getRect().height;
    var bodyHeight = document.body.offsetHeight;

    var parentIndex = dom.getElementIndex(trigger);
    var top = trigger.getRect().height * parentIndex + this.#menu.getRect().top;

    if(top > bodyHeight - menuHeight) top = bodyHeight - menuHeight - 10;

    return top;
  }

  checkSelected = (selected) => {
    if(this.#options.getParent() &&
      this.#options.getParent().parent &&
      this.#options.getParent().parent().getSelectedCount()) {

      selected = this.#options.getParent()?.parent().getSelectedCount();
    }
  }

  drawItem = (item, where = this.#menu) => {

    var itemWrapper = h.div(MagicMenu.CLASS_NAME + 'item-wrapper').setData('action', item.name).setData('allow_on_multi_rows').appendTo(where);

    itemWrapper.add(h.div(MagicMenu.CLASS_NAME + item.name + '-icon'));

    if(this.#options.getTranslate()){
      if(this.#options.getTranslatePath()){
        itemWrapper.add(h.div(MagicMenu.CLASS_NAME + 'item').id("menu_item_" + item.name).text(this.#options.getTranslatePath() + item.name, false));
      }else{
        itemWrapper.add(h.div(MagicMenu.CLASS_NAME + 'item').id("menu_item_" + item.name).text(MagicMenu.TRANSLATE_PATH + item.name));
      }
    }else{
      itemWrapper.add(h.div(MagicMenu.CLASS_NAME + 'item').id("menu_item_" + item.name).text(item.name, false));
    }

    itemWrapper.click((el, event) => {

      el = h.from(el);
      this.#options.fire(el.getData('action'), item, this.#options.getId(), this.#data, this );

        try { onMenuClick(el, this.#data, this)} catch (exc){ }

        this.removeMenu();
    })
  }

  replaceMenu(pos) {
    var rect = this.#menu.getRect();

    var height = rect.height;
    var width = rect.width;

    var bWidth = document.body.offsetWidth;
    var bHeigth = document.body.offsetHeight;

    var left = pos.x;
    var top = pos.y;

    var posTopToBody = (bHeigth - top - height);
    var posLeftToBody = (bWidth - left-width);

    if(posTopToBody < 0) {
      this.#menu.style().removeItem("top").bottom(10)
    }

    if(posLeftToBody < 0) {
      this.#menu.style().removeItem("left").right(10)
    }
  }

  clickInsideElement(el, CLASS_NAME) {
      if (el.ccl(CLASS_NAME)) {
          return el;
      } else {
          while (el = el.parent(true)) {
              if (el.ccl(CLASS_NAME)) {
                  return el;
              }
          }
      }

      return false;
  }


  getPosition(e) {
      let posx = 0;
      let posy = 0;

      if (!e) var e = window.event;

      if (e.pageX || e.pageY) {
          posx = e.pageX;
          posy = e.pageY;
      } else if (e.clientX || e.clientY) {
          posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
          posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      }

      return {
          x: posx,
          y: posy
      }
  }

}