class Executor {

    static GET_METHOD = 'GET';
    static POST_METHOD = 'POST';
    static PUT_METHOD = 'PUT';
    static DELETE_METHOD = 'DELETE';

    static HEADERS = {
        'Accept': 'application/json',
        'Content-Type': 'application/json;charset=UTF-8'
    }
    static HEADERS_PLAIN = {
        'Accept': 'text/plain',
        'Content-Type': 'text/plain; charset=UTF-8'
    }

    static prepareAuth(xhr) {

        xhr.setRequestHeader('Authorization', 'Bearer ' + sessionStorage.getItem("___TOKEN___"));

    }


    static errorFunction = function (e) {
        verifyResponse(e);
        dom.toast(_.$(e.responseText));
    };

    static runGet(path, success, async = true, headerds = Executor.HEADERS, fail = null) {
        Executor.run(path, Executor.GET_METHOD, success, async, fail, headerds);
    }

    static runPostWithFailFunction(path, success, failFunction, async = true, headerds = Executor.HEADERS) {
        Executor.run(path, Executor.POST_METHOD, success, async, failFunction, headerds);
    }

    static runPost(path, success, headerds = Executor.HEADERS, async = true, fail = null) {
        Executor.run(path, Executor.POST_METHOD, success, async, fail, headerds);
    }

    static runDelete(path, success, headerds = Executor.HEADERS, async = true, fail = null) {
        Executor.run(path, Executor.DELETE_METHOD, success, async, fail, headerds);
    }

    static run(path, method, success, async = true, fail = null, _headerds = Executor.HEADERS) {
        $.ajax({
            headers: _headerds,
            beforeSend: Executor.prepareAuth, type: method, url: path,
            async: async,
            success: success,
            error: fail == null ? Executor.errorFunction : fail

        });
    }

    static runPostWithPayload(path, success, payload, fail = null, _headerds = Executor.HEADERS) {
        $.ajax({
            headers: _headerds,
            beforeSend: Executor.prepareAuth, type: Executor.POST_METHOD, url: path,
            data: JSON.stringify(payload),
            dataType: "json",
            success: success,
            error: fail == null ? Executor.errorFunction : fail
        });
    }

    static runPutWithPayload(path, success, payload, fail = null, _headerds = Executor.HEADERS) {
        $.ajax({
            headers: _headerds,
            beforeSend: Executor.prepareAuth, type: Executor.PUT_METHOD, url: path,
            data: JSON.stringify(payload),
            dataType: "json",
            success: success,
            error: fail == null ? Executor.errorFunction : fail
        });
    }

}


var current_drug_event_limits;


function get(id) {
    return document.getElementById(id);
}