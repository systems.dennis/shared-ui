class MagicPagination {
    #parent;

    constructor(parent) {
        this.#parent = parent
    }

    #content = h.div("magic_list_footer");

    #no_content = h.div("magic_list_footer")
        .cl("no_content")
        .text("global.page.no.results")
        .hide()

    #pages;

    #pageController;
    #perPageController;

    #totalPegeslabel;

    /**
     * Page related data that changes based on table data
     */
    #firstPageButton;
    #nextPageButton;
    #previousPageButton;
    #lastPageButton;
    #dataTotalPages;
    #currentPage;
    #totalElements;

    getPrevButton() {
        return this.#previousPageButton;
    }

    getNextButton() {
        return this.#nextPageButton;
    }

    getTotalElements(){
        return this.#totalElements;
    }

    setNoContentDiv(noContent){
        if(noContent instanceof h){
          this.#no_content = noContent;
        }
        return this
    }

    getPageController() {
        return this.#pageController;
    }

    getPerPageController() {
        return this.#perPageController;
    }

    /**
     * End of page related data
     */

    build() {

        if (!_.isEmpty(this.#parent.getParent().getOptions().getAttribute())) {
            this.#content.cl("attributted");
            this.#no_content.cl("attributted");
        }

        this.#perPageController = dom.select(null).id(this.#parent.getId() + "_" + 'page_select')
            .add(dom.option({label: 10, value: 10}))
            .add(dom.option({label: 25, value: 25}))
            .add(dom.option({label: 50, value: 50}));

        let whereToDraw = this.#parent.getParent().getOptions().getDrawPaginationTo();

        if (_.isEmpty(whereToDraw)){
            whereToDraw =    this.#parent.getParent().getContainer();
        }

        whereToDraw.add(this.#content);

        this.#parent.getParent().getContainer().add(this.#no_content);

        this.table = this.#parent.getParent().getTableObject().getWhere();

        let _this = this;
        this.#perPageController.get().onchange = function () {
            _this.checkInputValue(0)
            _this.#parent.getParent().drawValues();
        }
        let perPagelabel = h.label(this.#perPageController, "select_pages")
            .text('global.app.list.per_page').appendTo(this.#content);
        this.#content.appendChild(perPagelabel)

        this.#content.appendChild(this.#perPageController)
        MagicPage.changeSelect(this.#perPageController.get(), this.#content);

        this.#pages = h.div("footer_pages");
        this.#content.appendChild(this.#pages);

        this.buildPages();
        try {
            this.#parent?.getParent()?.getOptions().fire(new MagicListEvent(MagicListEvent.PAGINATION_FINISHED, this.#parent.getParent(), {
                "pagination": this,
                "content": this.#content,
                "drawAt" : whereToDraw
            }))
        } catch (e) {
            log.debug(e)
        }

    }

    getPageNumber() {
        let page = this.#pageController.get().value - 1;
        return page >= 0 ? page : 0;
    }

    getPerPage() {
        let page = this.#perPageController.get().value;
        if (utils.isEmpty(page)) {
            return 10;
        } else {
            return page;
        }
    }

    rebuildPages(data) {
        this.paginationVisability(data)
        this.#dataTotalPages = data.totalPages;
        this.#currentPage = data.number + 1;
        this.#totalPegeslabel.text("global.app.list.total_pages").andText(": " + data.totalPages)
        this.#pageController.text(data.pageable.pageNumber + 1, false);
        this.changeNextPrevButton(data);
        this.#totalElements = data.totalElements;
    }

    paginationVisability(data) {
        if (_.isEmpty(data.content)) {
            this.#content.hide()
            this.table.hide()
            this.#no_content.show()
        } else {
            this.#content.show()
            this.table.show()
            this.#no_content.hide()
        }
    }

    changeNextPrevButton(data) {

        try {
            this.#pageController.get().value >= data.totalPages ? this.#nextPageButton.cl('hidden') : this.#nextPageButton.rcl('hidden');
            data.number + 1 <= 1 ? this.#previousPageButton.cl('hidden') : this.#previousPageButton.rcl('hidden');

            this.#pageController.get().value >= data.totalPages ? this.#lastPageButton.cl('hidden') : this.#lastPageButton.rcl('hidden');
            data.number + 1 <= 1 ? this.#firstPageButton.cl('hidden') : this.#firstPageButton.rcl('hidden');

        } catch (exc) {
            log.error(exc);
        }


    }


    buildPages() {
        this.#pages.innerHTML = "";

        let _this = this;
        this.#pageController = h.tag("input").id(this.#parent.getId() + "_pages").attr("type", "text").appendTo(this.#pages);

        this.#pageController.change(function (e) {
            let value = e.value;
            _this.checkInputValue(value) && _this.#parent.getParent().drawValues();

        });

        this.#firstPageButton = h.img('../images/firstPage.png').wh(24).click(function () {
            _this.#pageController.get().value = _this.#pageController.get().value = 1;
            _this.#pageController.get().onchange();

        });


        this.#previousPageButton = h.img('../images/prev.png').wh(24).click(function () {
            _this.#pageController.get().value = Number(_this.#pageController.get().value) - 1;
            _this.#pageController.get().onchange();

        });

        this.#nextPageButton = h.img('../images/next.png').wh(24).click(function () {
            _this.#pageController.get().value = Number(_this.#pageController.get().value) + 1;
            _this.#pageController.get().onchange();

        });

        this.#lastPageButton = h.img('../images/lastPage.png').wh(24).click(function () {
            _this.#pageController.get().value = _this.#pageController.get().value = _this.#dataTotalPages;
            _this.#pageController.get().onchange();

        });

        this.#pages.appendChild(this.#firstPageButton);
        this.#pages.appendChild(this.#previousPageButton);
        this.#pages.appendChild(this.#pageController);
        this.#pages.appendChild(this.#nextPageButton);
        this.#pages.appendChild(this.#lastPageButton);
        this.#totalPegeslabel = h.label(this.#pageController, "pages_label").text("global.app.list.total_pages").andText(": ");
        this.#pages.appendChild(this.#totalPegeslabel);

    }

    checkInputValue(value) {

        if (value > this.#dataTotalPages && this.#currentPage == this.#dataTotalPages) {
            this.#pageController.get().value = this.#dataTotalPages;
            return false;
        }
        if (value > this.#dataTotalPages) {
            this.#pageController.get().value = this.#dataTotalPages
        }
        if (value < 1 && this.#currentPage != 1) {
            this.#pageController.get().value = 1;
        }
        if (value < 1 && this.#currentPage == 1) {
            this.#pageController.get().value = 1;
            return false;
        }
        return true

    }


}