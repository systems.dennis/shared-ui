class Button {

    constructor(name,  click = null, icon = null, isAction = false, buttonClass ) {
        this.label = name;
        this.icon = icon;
        this.click = click;
        this.isAction = isAction;
    }


    getIsAction(){
        return this.isAction;
    }


    setIsAction(isAction){
        this.isAction = isAction;
        return this;
    }

    toH(){
        return h.input("button").text(this.label, true).clickIfPresent(this.click);
    }

}
class MagicToastOptions {
    static CLASS_NAME = "toast__";
    static INTERVAL_DURATION = 10;
    static TOP_BOTTOM = "top-bottom";
    static BOTTOM_TOP = "bottom-top";

    #duration;
    #interval;
    #direction;

    constructor(duration, intervalDuration, direction = MagicToastOptions.TOP_BOTTOM) {
        this.#duration = duration;
        this.#interval = intervalDuration;
        this.#direction = direction;
        if (!duration) {
            this.#duration = 3;
        }
    }

    getDuration() {
        return this.#duration;
    }

    getDirection() {
        return this.#direction;
    }

    getInterval() {
        return this.#interval;
    }
}

class ToastType {
    static CREATED = MagicToastOptions.CLASS_NAME + "created-type";
    static CHANGED = MagicToastOptions.CLASS_NAME + "changed-type";
    static REMOVE = MagicToastOptions.CLASS_NAME + "remove-type";

    static NODATA = MagicToastOptions.CLASS_NAME + "nodata-type";
    static ERROR = MagicToastOptions.CLASS_NAME + "error_type";
    static SUCCESS = MagicToastOptions.CLASS_NAME + "success_type";
}

class Toast {
    progresPosition = 1;

    interval = 10;

    constructor(duration, interval = null, type = "no__type", direction) {

        this.duration = duration;
        this.type = type;
        this.direction = direction;

        if (interval) {
            this.interval = interval;
        }

        this.init();
    }

    init() {
        this.wrapper = h.div(MagicToastOptions.CLASS_NAME + "wrapper").fixed().cl(this.type)
        this.toastSubWrapper = h.div(MagicToastOptions.CLASS_NAME + "subwrappper").appendTo(this.wrapper);
        this.progressWrapper = h.div(MagicToastOptions.CLASS_NAME + "progress-wrapper").appendTo(this.toastSubWrapper);
        this.content = h.div(MagicToastOptions.CLASS_NAME + "content").appendTo(this.toastSubWrapper);
        this.icon = h.div(MagicToastOptions.CLASS_NAME + "icon").appendTo(this.content)
        this.textHolder = h.div(MagicToastOptions.CLASS_NAME + "text-content").appendTo(this.content);
        this.progressLine = h.div(MagicToastOptions.CLASS_NAME + "progress-line").appendTo(this.progressWrapper).width(0);
        this.close = h.div(MagicToastOptions.CLASS_NAME + "close").appendTo(this.toastSubWrapper).click(this.remove)
    }

    show(text, toTranslate = false) {
        this.init();
        this.textHolder.text(text, toTranslate);
        this.wrapper.appendToBody();
        this.setPosition();
        this.timer();
    }

    timer() {
        this.interval = setInterval(this.progress, this.interval);
    }

    setPosition = () => {
        var height = this.wrapper.getRect().height;

        this.index = MagicToastController.TOASTS_STORAGE.indexOf(this);

        var pos = this.index * height + this.index * 10 + 20;

        if (_e(this.direction, MagicToastOptions.TOP_BOTTOM)) {

            this.wrapper.style().top(pos);
        } else {
            this.wrapper.style().bottom(pos);
        }
    }

    progress = () => {
        var coefficient = this.duration / this.progresPosition;

        this.progressLine.style().persentWidth(100 / coefficient);

        this.progresPosition += MagicToastOptions.INTERVAL_DURATION;

        if (this.progresPosition > this.duration) {
            this.remove();
        }
    }

    remove = () => {
        clearInterval(this.interval);
        this.wrapper.remove();
        MagicToastController.TOASTS_STORAGE.splice(this.index, 1);
        MagicToastController.rePositionAllToasts();
    }
}

class CustomToastOptions {
    direction = MagicToastOptions.TOP_BOTTOM;

    constructor(duration, divH, toastClassName, width = "fit-content") {
        this.duration = duration;
        this.toastClassName = toastClassName;
        this.divH = divH;
        this.width = width;

        this.durationTransform();
    }

    setDirection(val) {
        this.direction = val;

        return this;
    }

    durationTransform() {
        if (this.duration) {
            this.duration = this.duration * 1000;
        } else {
            this.duration = null;
        }
    }
}

class CustomToast extends Toast {
    #options;

    constructor(options) {
        super();
        this.#options = options;

        this.duration = options.duration;
        this.interval = 10;
        this.direction = options.direction
    }

    show() {
        this.wrapper.appendToBody();
        this.classCheck();
        this.textHolder.add(this.#options.divH);
        this.setPosition();
        this.timerCheck();
        this.widthCheck();
    }


    timerCheck() {
        if (this.duration) {

            this.timer();
        } else {
            this.progressWrapper.remove();
        }
    }

    classCheck() {
        var className = this.#options.toastClassName;

        if (className) {
            this.wrapper.cl(className);
        }
    }

    widthCheck() {
        var width = this.#options.width;

        if (width) {
            this.wrapper.width(width, false);
        }
    }
}

class MagicToastController {
    static TOASTS_STORAGE = [];

    #options;

    constructor(options) {
        this.#options = options;
    }

    addToast(text, type, toTranslate = false) {
        if (text) {

            var duration = this.#options.getDuration();
            var interval = this.#options.getInterval();
            var direction = this.#options.getDirection();

            if (duration) {
                var toast = new Toast(duration * 1000, interval, type, direction);

                MagicToastController.TOASTS_STORAGE.push(toast);

                toast.show(text, toTranslate);
            }
        }
    }

    static addCustomToast(options) {
        var toast = new CustomToast(options);

        MagicToastController.TOASTS_STORAGE.push(toast);

        toast.show();
    }

    static rePositionAllToasts() {
        let arr = MagicToastController.TOASTS_STORAGE;

        if (!_.isEmpty(arr)) {
            _.each(arr, (toast) => {
                toast.setPosition();
            })
        }
    }


}


class dom {

    static CHECKED = "checked";
    static HIDDEN = "hidden";
    static CHECKBOX = "checkbox";
    static LABEL = "label";
    static INPUT = "input";
    static TEXT = "text";
    static SPAN = "span";
    static DIV = "div";
    static SELECT = "select";
    static OPTION = "option";

    static TYPE_ATTR = "type";

    static REMOVE_TYPE_REMOVE = "remove";
    static REMOVE_TYPE_HIDE = "hide";
    static MODAL = "modal";

    static toastOptions = new MagicToastOptions(Environment.defaultToastDuration);
    static toastController = new MagicToastController(dom.toastOptions);

    static toast(text, type, toTranslate = false) {
        dom.toastController.addToast(text, type, toTranslate);
    }

    static customToast(duration, divH, toastClassName = null, width = "fit-content") {

        var options = new CustomToastOptions(duration, divH, toastClassName, width);

        MagicToastController.addCustomToast(options);
    }

    static findSelectedValue(x) {
        try {
            return x.options[x.selectedIndex].innerHTML;
        } catch (e) {
            log.error(e);
            return "";
        }
    }

    static createIcon(name) {
        if (_.isNull(name)) {
            return dom.createCircle("NA");
        }
        let nameParts = name.split(" ");

        let newData = [];
        _.each(nameParts, (part) => {
            newData.push(part.firstLetter())
        })

        if (nameParts.length < 3) {
            return dom.createCircle(newData.join(""));
        } else {
            return dom.createCircle(newData.slice(0, 2).join(""));
        }

    }


    static copyToClipboard(text) {
        var copyText = document.createElement("input");

        copyText.value = text;
        // Select the text field
        copyText.select();
        copyText.setSelectionRange(0, 99999); // For mobile devices

        navigator.clipboard.writeText(copyText.value);

        dom.message("global.success")
    }


    static createCircle(name, type) {
        return h.span("named_icon")._text(name);
    }

    static setCboValue(cbo, value) {
        if (cbo.isH) {
            cbo = cbo.get();
        }
        _.each(cbo.options, (option, i) => {
            if (_e(option.value, value)) {
                cbo.selectedIndex = i;
            }
        })
    }

    static addOptionInSelect(select, value) {
        if (select.isH) {
            select = select.get();
        }

        let newOption = new Option(value.label, value.value);
        select.append(newOption)
    }

    static async waitForItem(item, whatTodo) {
        if (item() == undefined) { // define the condition as you like
            setTimeout(
                () => {
                    dom.waitForItem(item, whatTodo)
                }
                , 1000);
        } else {
            whatTodo();
        }

    }

    static chk(id = null, label, checked, where) {

        let chk = h.tag(dom.INPUT).attr(dom.TYPE_ATTR, dom.CHECKBOX).id(id);
        chk.appendTo(where);
        h.tag(dom.LABEL).text(label, false).attr('for', id).appendTo(where).get().checked = checked;
        return chk;
    }

    static navigate(to) {
        document.location.href = Environment.self + "/" + to;
    }

    static message(text = "global.success", toTranslate = true, type = ToastType.SUCCESS) {
        dom.toast(toTranslate ? _.$(text) : text, type);
    }

    static errorMessage() {
        dom.toast(_.$("global.error"), ToastType.ERROR);
    }

    static successMessage() {
        dom.toast(_.$("global.success"), ToastType.SUCCESS);
    }

    static isVariableExists(variable) {
        if (_e(typeof variable, "undefined")) {
            return false
        }
        return true

    }

    static hasChildren(obj) {
        if (obj.isH) {
            return obj.get().childNodes.length > 0;
        } else {
            return obj.childNodes.length > 0;
        }
    }

    static getCboValue(cbo, value) {
        for (let i = 0; i < cbo.options.length; i++)
            if (cbo.options[i].value == value) {
                cbo.selectedIndex = i;
                return;
            }
    }

    static selectedOption(cb, toWrap = false) {

        if (cb.isH) {

            let res = cb.get().options[cb.get().selectedIndex];
            return toWrap ? h.from(res) : res;
        } else {
            let res = cb.options[cb.selectedIndex];
            return toWrap ? h.from(res) : res;
        }
    }

    static getCursorIndex(inputH) {
        return inputH.get().selectionStart;
    }

    static setCursorIndex(inputH, i) {
        inputH.get().selectionStart = i;
    }

    static setSelectionRange(inputH, from, to) {
        inputH.setSelectionRange(from, to)
    }

    static select(id) {
        return h.tag("select").id(id).cl("dropdown");

    }

    static option(item, toTranslate = false) {
        return h.tag("option").text(item.label, toTranslate).attr("value", item.value);
    }


    static tinyTextArea() {
        return h.tag("textarea").cl("text_area_tini_mce").id("id_" + new Date().getTime() + "_txt_area");
    }

    static errorPlate(id) {
        return h.tag('span').cl("hidden").cl("error_plate").id("error_" + id);
    }

    static img(path, size) {
        return h.img(path).wh(size);
    }

    static linkTo(objectWhat, linkToObj) {
        let bounds = h.from(linkToObj).get().getBoundingClientRect();
        let hObjWhat = h.from(objectWhat).get()
        let hlinkToObj = h.from(linkToObj).get()
        hObjWhat.style.top = bounds.top + hlinkToObj.height + 'px';
        hObjWhat.style.left = bounds.left + hlinkToObj.width + 'px';
    }

    // static REMOVE_TYPE_REMOVE = "remove";
    // static REMOVE_TYPE_HIDE = "hide";
    // when create Modal, we should choose the type, how is your modal will be close

    static createModal(removeType, className = null) {
        let modalName = "modal_" + new Date().getTime() + Math.ceil(Math.random() * (1000 - 1) + 1);
        let modalDiv = h.div(_.ofNullable(className, dom.MODAL)).setData(dom.MODAL, modalName).click(() => {
            MagicPage.focusModal(modalName)
        })
        MagicPage.setModal(modalName, modalDiv, removeType);
        return modalDiv
    }

    static createCloseImage() {
        return h.div("close_inner_form").cl("div_image").wh(24)
    }

    static redirect(url, includeSelf = true, blank = true) {
        if (includeSelf) {
            url = url.toLocalUrl();
        }
        if (blank) {
            window.open(url, '_blank');
        } else {
            window.location.href = url
        }
    }

    static sortDate(arrWithDate) {
        return arrWithDate.sort((a, b) => b.date.date - a.date.date)
    }

    static editableDiv(div, isEditable) {
        if (div.isH) {
            div.attr("contenteditable", isEditable);
        } else {
            div.setAttribute("contenteditable", isEditable)
        }
        return div
    }

    static eachDomElement(func) {
        var all = Array.from(document.getElementsByTagName("*"));

        utils.each(all, func)
    }

    static getElementIndex(el) {

        if (_.notNull(el)) {
            if (el.isH && el.parent()) {
                return Array.from(el.parent().childNodes()).indexOf(el.get());
            } else if (!el.isH && el.parentElement) {
                return Array.from(el.parentElement.children).indexOf(el);
            }
        }
    }

    static setCheckboxValue(checkbox, val) {
        if (checkbox.isH) {
            checkbox.get().checked = val;
        } else {
            checkbox.checked = val;
        }
    }

    static getVideoElement(src) {
        return h.tag("video").setA("controls", true)
            .add(h.tag("source").src(src))
    }

    static getAudioElement(src) {
        return h.tag("audio").setA("controls", true)
            .add(h.tag("source").src(src))
    }

    static paginationForFetchData(data, reSerchFunc) {
        let paginDiv = h.div('pagination_div');
        let totalPages = data.totalPages;
        let currentPage = data.number + 1;
        if (totalPages > 1) {
            let prevBtn = h.img('prev.svg').cl('prev_btn_chooser').appendTo(paginDiv);
            h.div('chooser_bord').text(`${currentPage} / ${totalPages}`, false).appendTo(paginDiv);
            let nextBtn = h.img('next.svg').cl('next_btn_chooser').appendTo(paginDiv);

            if (currentPage == totalPages) {
                nextBtn.cl('disabled_btn_chooser');
                prevBtn.click(function () {
                    reSerchFunc(data.number - 1);
                    paginDiv.remove();
                })
            }
            if (currentPage == 1) {
                prevBtn.cl('disabled_btn_chooser');
                nextBtn.click(function () {
                    reSerchFunc(data.number + 1);
                    paginDiv.remove();
                })
            }
            if (currentPage !== totalPages && currentPage !== 1) {
                prevBtn.click(function () {
                    reSerchFunc(data.number - 1);
                    paginDiv.remove();
                })
                nextBtn.click(function () {
                    reSerchFunc(data.number + 1);
                    paginDiv.remove();
                })
            }
        }
        return paginDiv
    }

    static fromMinToHour(minutes) {
        let hour = minutes / 60
        if (Number.isInteger(hour)) {
            return ` (${hour} hour)`
        } else {
            return ` (${Math.ceil(hour * 10) / 10} hour)`
        }
    }

    static markLoading(component) {
        component?.cl("loading")
    }

    static markNotLoading(component) {
        component?.rcl("loading")
    }

}

class Modal {
    static WRAPPER_CLASS = "modal-window";
    static MODAL_CLASS = "modal-content";
    static CLOSE_CLASS = "close-modal-button";

    constructor() {
        this.wrapper = dom.createModal(dom.REMOVE_TYPE_REMOVE).cl(Modal.WRAPPER_CLASS)
        this.modal = h.div(Modal.MODAL_CLASS).appendTo(this.wrapper);
        this.closeBtn = h.div(Modal.CLOSE_CLASS).appendTo(this.modal)
        this.closeBtn.click(this.close)
    }

    open() {
        this.wrapper.appendToBody();

        return this;
    }

    close = () => {
        this.wrapper.remove();
    }
}
