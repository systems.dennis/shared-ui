class PseudoRow {
    #id;
    #data;

    static PSEUDO_ID_FILED = "___pseudo_id"
    static PSEUDO_ERROR_FIELD = "___pseudo_error"

    constructor(data) {
        this.#id = _.createRandomId(PseudoRow.PSEUDO_ID_FILED)
        data.id = this.#id
        this.#data = data;
        this.#data.___pseudo = true;
    }

    _equals(b) {

        if (_.isNull(b.getId)) {
            //if id was sent as an object to equal
            return _e(this.getId(), b);
        }

        return _e(this.getId(), b.getId());

    }

    unsetId() {
        let dataToReturn = structuredClone(this.#data)
        dataToReturn.id = null
        return dataToReturn
    }

    getId() {
        return this.#id;
    }

    get() {
        return this.#data;
    }

    update(data) {
        this.#data = data;
        this.#data.___pseudo = true;
        this.#data.id =this.#id
        return this
    }

    markNoError() {
        this.get()[PseudoRow.PSEUDO_ERROR_FIELD] = null
    }

    markHasError(error) {
        this.get()[PseudoRow.PSEUDO_ERROR_FIELD] = error

    }

    hasError() {

        return _.notNull(this.get()[PseudoRow.PSEUDO_ERROR_FIELD]);
    }

}

/**
 * Allow to add and save objects that are not yet in database, but can be saved later
 */
class PseudoRowHolder {
    /**
     *
     * Pseudo row holder
     * @type {[]}
     */
    #rows = []
    #options;

    /**
     * MagicListOptions
     * @param options
     */
    constructor(options) {
        this.#options = options;
    }


    addRow(row, parent) {
        row.___parent = parent;
        if (!(row instanceof PseudoRow)) {
            throw Error("row should be instance of PseudoRow")
        }
        if (!this.validateRow(row)) {
            this.#options.fireEvents(new MagicListEvent(MagicListEvent.PSEUDO_ITEM_VALIDATION_FAILED, this, row))
        } else {
            this.#options.fireEvents(new MagicListEvent(MagicListEvent.PSEUDO_ITEM_VALIDATION_SUCCESS, this, row))
        }

        let isAdded = this.#rows.addIfAbsent(row);

        if (!isAdded) {
            this.#options.fireEvents(new MagicListEvent(MagicListEvent.PSEUDO_ITEM_ALREADY_EXISTS, this, row))
        } else {
            this.#options.fireEvents(new MagicListEvent(MagicListEvent.PSEUDO_ITEM_ADDED, this, row))
        }
        parent?.drawValues()
    }

    getPseudoRows() {
        return this.#rows;
    }

    deleteRow(row) {
        this.#rows.removeItem(row)
    }

    deleteAllRows(){
        this.#rows = [];
    }

    validateRow(row) {

        if (this.#options.getPseudoItemValidator()) {
            return this.#options.getPseudoItemValidator()(row)
        }
        return true;
    }

    /**
     * Allows to save pseudo row to db and reload list if it is not null
     * Error During saving will mark a whole row as error, and the row hasError() will return true
     * Saved correctly row will be deleted from the PseudoRows, and should appear after cache is cleaned
     * @param row an id, or pseudo row itself
     * @param list list containing method drawValues or null if nothing is to be updated
     * @param request a request to process save if other than main is required
     */
    save(row, list, request = null) {
        request = _.ofNullable(request, () => {
            return list.getOptions().getRequest()
        })
        if (!_.isObject(row)) {
            row = _.find(this.#rows, (e) => {
                return _e(e.getId(), row)
            })
        }
        if (_.isNull(row)) {
            log.promisedDebug(() => {
                return {"message": "object_not_found", "code": 404, "object": row}
            })
            return
        }
        row.markNoError();


        request.add(row.unsetId(), (data) => {
            this.deleteRow(row)
            list.getOptions().fireEvents(new MagicListEvent(MagicListEvent.PSEUDO_ITEM_SAVED, this, {'pseudo_row': row, 'saved_row' : data}))
            list?.drawValues();
        }, (e) => {
            row.markHasError(e)
            list.getOptions().fireEvents(new MagicListEvent(MagicListEvent.PSEUDO_ITEM_FAILED_TO_SAVE, this,  {"pseudo_row" : row, "error" : e}))
            list?.drawValues();
        })

    }


    saveAll(list, request = null) {
        _.each(this.#rows, (row) => {
            //we pass data here not to update table all the time
            this.save(row, list, request);
        })

        list?.drawValues();
    }

}

class Selectable {

    #listOptions;
    #listener
    #listeners = []
    //never null
    #selectedIds = [];
    #checkboxes = new Map();

    static ADD_SELECTED_ID = "ADD_SELECTED_ID"
    static DELETE_SELECTED_ID = "DELETE_SELECTED_ID"
    static SELECT_ALL = 'SELECTED_ALL'
    static DELETE_ALL_SELECTED = 'DELETE_ALL_SELECTED'

    constructor(listOptions) {
        this.#listOptions = listOptions;
        this.#listener = this.#listOptions.getListListener();
        this.#listeners = this.#listOptions.getListListeners()
    }

    addCheckbox(checkbox, id, data) {
        this.#checkboxes.set(id, {"checkbox": checkbox, "data": data});
        return this
    }

    getCheckboxes() {
        return this.#checkboxes;
    }

    resetCheckboxes() {
        this.#checkboxes = new Map()
        return this
    }

    toggleAllCheckboxes(value) {
        for (let chk of this.#checkboxes.values()) {
            chk.checkbox.setValue(value);
        }
    }


    fire(event) {
        if (this.#listener) {
            this.#listener(event)
        }
    }

    fireEvents(event) {
        _.each(this.#listeners, (listener) => {
            listener(event)
        })
    }

    resetSelectedIds() {
        this.#selectedIds = [];
    }

    getSelectedIds() {
        return this.#selectedIds;
    }

    getListener() {
        return this.#listener;
    }

    setSelectedIds(ids) {
        this.#selectedIds = _.ofNullable(ids, []);
        return this
    }

    isIdSelected(id) {
        return this.#selectedIds.includes(id);
    }

    addSelectedId(id) {
        !this.#selectedIds.includes(id) && this.#selectedIds.push(id);
        return this
    }

    deselectId(id) {
        this.#selectedIds = this.#selectedIds.filter(itemId => !_e(itemId, id));
    }

    static drawSelectComponent(id, whereToDraw, defaultVal) {

        const options = new MagicTrueFalseSelectorOptions('global.remove.one', defaultVal, false, false, true, true)
            .setDisplayType(MagicTrueFalseSelectorOptions.CHECKBOX)
            .setParent()
            .setComponentListener((event) => Selectable.trueFalseListener.bind(this)(id, event, this))
            .setPreventiveClick(true)
        let checkbox = new MagicTrueFalseSelector(options);
        const checkboxWrapper = h.div('header-cart-checkbox-wrapper').appendTo(whereToDraw)
        checkbox.draw(checkboxWrapper);
        return checkbox
    }

    /**
     * @binded to Selectable itself
     * @param id
     * @param event
     * @param parent
     */
    static trueFalseListener(id, event, parent) {

        if (_e(event.type, MagicTrueFalseSelectorEvent.VALUE_CHANGED)) {
            if (event.currentValue) {
                this.addSelectedId(id);
                // this.updateSelectedCountElements()
                this.fireEvents({"eventName": Selectable.ADD_SELECTED_ID, "id": id, 'selectable': parent})
            } else {
                this.deselectId(id);
                // this.updateSelectedCountElements()
                this.fireEvents({"eventName": Selectable.DELETE_SELECTED_ID, "id": id, 'selectable': parent})

            }
        }
    }

    getSelectorRenderer() {
        return _.ofNullable(this.#listOptions.getSelectorRenderer(), () => Selectable.drawSelectComponent.bind(this))
    }

}


class TableDrug {
    onDragStart;
    allowedClass;

    onDragDrop;
}

class PseudoColumn {
    #field;
    #order = 0;
    #renderer;
    #visible = true;
    #group;
    #translation;
    #type = "text";
    setGroup(group){
        this.#group = group;
        return this;
    }


    constructor(field) {
        this.#field = field;
    }

    setType(type){
        this.#type= type;
        return this;
    }

    setOrder(order){
        this.#order = order;
        return this;
    }

    setInvisible(){
        this.#visible = false;
    }

    setRenderer(renderer){
        this.#renderer = renderer;
        return this;
    }

    setTranslation(translation){
        this.#translation = translation
        return this;
    }


    toField(){
        return  {
            "visible" : this.#visible,
            "translation" : this.#translation,
            "field": this.#field,
            "group": this.#group,
            "renderer": this.#renderer,
            "order": this.#order,
            type: this.#type

        }
    }
}

class MagicListOptions {

    static SELECT_MODE_NONE = 0;
    static SELECT_MODE_SINGLE = 1;
    static SELECT_MULTI = 2;
    static DISPLAY_AS_TABLE = "table";
    static DISPLAY_AS_GREED = "grid";

    #pseudoColumns = [];

    #drag;
    #serverRequest;
    #gridOptions;
    #searchEnabled = true;
    #settingsEnabled = true;
    #orderingEnabled = true;
    #sortEnabled = true;
    #orderedFieldsFilter = MagicListOptions.removeActionsField;
    #drawPaginationTo;
    #allowedTableActions;
    #parent;
    #customTableAction;
    #customPagination = null;
    #displayType = MagicListOptions.DISPLAY_AS_TABLE;
    #attribute;
    #searchFormOptions = new SearchFormOptions();
    #contextMenuOptions = new MagicMenuOptions();
    #needMultiSelect = false;
    #selectable = false;
    #dataPathModifier;
    #listener;
    #tableListener;
    #availableFields;
    #listGroupToPath = "";
    #listListener;
    #listListeners = [];
    #queryRetriever;
    #groupId;
    #columnRenderer;
    #isEnabled = false;
    #formOptions;
    #defaultClickFunction;
    #selectorRenderer;
    #emptyContentDiv = null;
    #multiServerRequest = null;
    #referensedConverter = null;
    #showTotalElements
    #promptNoRepeat = "global.prompt_DELETE"
    #isSelectAllEnabled = false
    #isDeleteSelectedEnabled = false;
    #isDeleteAllButtonEnabled = false
    #deleteAllButtonOptions
    #isShareButtonEnabled = false
    #isContextMenuAvailable = true

    getIsContextMenuAvailable () {
        return this.#isContextMenuAvailable
    }

    setIsContextMenuAvailable (value) {
        this.#isContextMenuAvailable = value;
        return this
    }

    addPseudoColumn(name, translation,  order = 0, renderer = null){
        this.#pseudoColumns.push(new PseudoColumn(name).setOrder(order).setRenderer(renderer).setTranslation(translation))
    }

    addPseudoColumnObject(pseudoColumn){
        if (! (pseudoColumn instanceof  PseudoColumn)){
            throw new Error('pseudo-column should be extended from PseudoColumn ')
        }
        this.#pseudoColumns .push(pseudoColumn)
    }

    getPseudoColumns (){
        return this.#pseudoColumns
    }

    /**
     * set's up a method to check if pseudo item can be added to table
     */
    #pseudoItemValidator;

    getPseudoItemValidator() {
        return this.#pseudoItemValidator;
    }

    setPseudoItemValidator(validator) {
        this.#pseudoItemValidator = validator;
        return this;
    }

    constructor(serverRequest, parent = null) {
        this.#serverRequest = serverRequest;
        this.selectable = false;
        this.#parent = parent;
        try {
            this.#formOptions = new MagicFormOptions()
                .setServerRequest(serverRequest)
                .setParent(this)

            ;
        } catch {
            log.promisedDebug(() => {
                return "no form class"
            })
        }

    }



    getIsShareButtonEnabled() {
        return this.#isShareButtonEnabled
    }

    enableShareButton() {
        this.#isShareButtonEnabled = true
        return this
    }

    getIsDeleteAllButtonEnabled() {
        return this.#isDeleteAllButtonEnabled
    }

    enableDeleteAllButtonEnabled(callback = null,
                                 title = 'global.app.clear_list',
                                 toastMessage = "global.app.no_items_toast",
                                 promptMessage = 'global.add.confirm_delete_prompt') {
        this.#deleteAllButtonOptions = {
            callback: callback,
            title: title,
            toastMessage: toastMessage,
            promptMessage: promptMessage
        }
        this.#isDeleteAllButtonEnabled = true
        return this
    }

    getDeleteAllButtonOptions() {
        return this.#deleteAllButtonOptions;
    }

    getIsDeleteSelectedEnabled() {
        return this.#isDeleteSelectedEnabled;
    }

    setIsDeleteSelectedEnabled(val) {
        this.#isDeleteSelectedEnabled = val;
        return this
    }

    getIsSelectAllEnabled() {
        return this.#isSelectAllEnabled
    }

    enableSelectAll() {
        this.#isSelectAllEnabled = true
        return this
    }

    getNoRepeatPropertyPrompt() {
        return this.#promptNoRepeat;
    }

    setNoRepeatPropertyPrompt(noRepeat) {
        this.#promptNoRepeat = noRepeat
        return this
    }

    isShowTotalElements() {
        return this.#showTotalElements
    }

    showTotalElements() {
        this.#showTotalElements = true;
        return this
    }

    hideTotalElements() {
        this.#showTotalElements = false;
        return this
    }

    setReferensedConverter(conv) {
        this.#referensedConverter = conv;
        return this;
    }

    getReferensedConverter() {
        return this.#referensedConverter;
    }

    setMultiServerRequest(req) {
        this.#multiServerRequest = req;
        return this;
    }

    getMultiServerRequest() {
        return this.#multiServerRequest;
    }

    setSelectorRenderer(selector) {
        this.#selectorRenderer = selector;
        return this;
    }

    setEmptyContentDiv(div) {
        this.#emptyContentDiv = div;
        return this;
    }

    getEmptyContentDiv() {
        return this.#emptyContentDiv;
    }

    getSelectorRenderer() {
        return this.#selectorRenderer;

    }

    static removeActionsField(fields) {
        return _.filter(fields, (el) => !_e(el.field, "action"))
    }

    setFormOptions(options) {
        this.#formOptions = options;
        return this;
    }

    getFormOptions() {
        return this.#formOptions;
    }

    setSearchFormOptions(options) {
        this.#searchFormOptions = options;
        return this
    }

    setDefaultClickFunction(func) {
        this.#defaultClickFunction = func;
        return this;
    }

    getDefaultClickFunction() {
        return this.#defaultClickFunction;
    }

    getSearchFormOptions() {
        return this.#searchFormOptions;
    }

    setContextMenuOptions(options) {
        this.#contextMenuOptions = options;
        return this
    }

    getContextMenuOptions() {
        return this.#contextMenuOptions;
    }


    setCustomPagination(func) {
        this.#customPagination = func();
        return this;
    }

    getCustomPagination() {
        return this.#customPagination;
    }

    setDrawPaginationTo(drawTo) {
        this.#drawPaginationTo = drawTo;
    }

    getDrawPaginationTo() {
        return this.#drawPaginationTo;
    }

    getGridOptions() {
        return this.#gridOptions;
    }

    setGridOptions(opts) {
        if (_.notNull(opts) && !(opts instanceof MagicGridOptions)) {
            throw Error("options should be extended from MagicGridOptions!")
        }
        this.#gridOptions = opts;
        return this;
    }

    setDrag(dragInfo) {
        if (dragInfo instanceof TableDrug) {
            this.#drag = dragInfo;
            return this;
        }
        throw Error("Drag should be instance of TableDrug");
    }

    getDrag() {
        return this.#drag;
    }


    getRequest(uri) {

        if (_.isNull(this.#serverRequest)) {
            this.#serverRequest = new ServerRequest().entry();
        }

        return this.#serverRequest.copy(false, uri)
    }

    setDisplayType(type) {
        this.#displayType = type;
        return this;
    }

    getDisplayType() {
        return this.#displayType;
    }


    getAttribute() {
        return this.#attribute
    }

    setAttribute(attribute) {
        this.#attribute = attribute;
        return this;
    }

    setServerRequest(request) {
        this.#serverRequest = request
        return this
    }

    static DEFAULT(type) {
        return new MagicListOptions(type)
    };


    setAvailableFields(fields) {
        this.#availableFields = fields;
    }

    setListListener(listener) {
        this.#listListener = listener;
    }

    setListListeners(listener) {
        this.#listListeners.push(listener);
        return this
    }

    getListListener() {
        return this.#listListener;
    }

    getListListeners() {
        return this.#listListeners;
    }

    getOrderedFieldsFilter() {
        return this.#orderedFieldsFilter;
    }

    setOrderedFieldsFilter(func) {
        this.#orderedFieldsFilter = func;
        return this;
    }

    getAvailableFields() {
        return this.#availableFields;
    }

    setCustomTableAction(val) {
        this.#customTableAction = val;
        return this;
    }

    getListGroupToPath() {
        return this.#listGroupToPath;
    }

    getCustomTableAction() {
        return this.#customTableAction;
    }

    setTableAllowedActions(actions) {
        this.#allowedTableActions = actions;
    }

    getTableAllowedActions() {
        return this.#allowedTableActions;
    }

    /**
     * @deprecated use @getQuery method instead
     * @param retriever
     * @returns {MagicListOptions}
     */
    setQueryModifier(retriever) {

        return this.setQueryRetriever(retriever);
    }

    setQueryRetriever(retriever) {
        this.#queryRetriever = retriever;
        return this;
    }

    getQueryRetriever() {
        return _.ofNullable(this.#queryRetriever, function () {
            return function () {
                return new Query()
            }
        });
    }

    setColumnRenderer(renderer) {
        this.#columnRenderer = renderer;
        return this;
    }

    setMultiSelect(val) {
        this.#needMultiSelect = val;
        return this;
    }

    getNeedMultiselect() {
        return this.#needMultiSelect;
    }

    getColumnRenderer() {
        return this.#columnRenderer;
    }

    getParent() {
        return this.#parent;
    }

    getQueryModifier() {
        return this.#queryRetriever;
    }

    getTableListener() {
        return this.#tableListener;
    }

    setTableListener(listener) {
        this.#tableListener = listener;
        return this;
    }

    setDataPathModifier(dataPathModifier) {
        this.#dataPathModifier = dataPathModifier;
    }

    getDataPathModifier() {
        return this.#dataPathModifier;
    }

    setSettingsEnabled(settingsEnabled) {
        this.#settingsEnabled = settingsEnabled;
        return this;
    }

    getSettingsEnabled() {
        return this.#settingsEnabled;
    }

    setOrderingEnabled(orderingEnabled) {
        this.#orderingEnabled = orderingEnabled;
        return this;
    }

    getOrderingEnabled() {
        return this.#orderingEnabled;
    }

    setSortEnabled(sortEnabled) {
        this.#sortEnabled = sortEnabled;
        return this;
    }

    getSortEnabled() {
        return this.#sortEnabled;
    }

    getSearchEnabled() {
        return this.#searchEnabled;
    }

    setSearchEnabled(enabled) {
        this.#searchEnabled = enabled;
        return this;
    }

    fire(event) {
        if (_.notNull(this.#listListener)) {
            return this.#listListener(event);
        }
    }

    fireEvents(event) {
        _.each(this.#listListeners, (listener) => {
            listener(event);
        })
    }

    setIsFavoriteEnabled(value) {
        this.#isEnabled = value;
        return this;
    }

    getIsFavoriteEnabled() {
        return this.#isEnabled;
    }
}

class MagicList2 {
    static EVENT_DATA_NOT_EXISTS = 'data_not_exists'
    static EVENT_DATA_EXISTS = 'data_exists'
    #options;
    #listListener;
    #listListeners;
    #container;
    #magicTable;
    #headers;
    #fetchData;
    #grid = h.div("pg_table_container");
    #form = h.div("modal").cl("hidden").id(new Date().getTime());
    #formClass;
    #sortAndSearchForm = h.div("table_config");
    #sort = false;
    #searchForm;
    #ordering;
    #favoriteType;
    #lastData;
    #pagination;
    #searchEnabled;
    #id;
    #totalElements = h.div("total_elements_content");
    #selectControlPanel

    #pseudoRowHolder;

    getPseudoRowHolder() {
        return this.#pseudoRowHolder;
    }

    getSelectedIds() {
        return this.getTableObject().getSelectedIds();
    }

    setSelectedIds(ids) {
        this.getTableObject().setSelectedIds(ids);
    }

    isSelectedIdsEmpty() {
        return !this.getTableObject().getSelectedIds().length;
    }

    getGrid() {
        return this.#grid;
    }

    getFavoriteType() {
        return this.#favoriteType;
    }

    getPagination() {
        return this.#pagination;
    }

    getRootPath() {
        return this.getOptions().getRoot();
    }

    getId() {
        return this.#id;
    }

    insertData(data) {
        this.getTableObject().insertData(data)
    }

    getLastData() {
        return this.#lastData;
    }

    getOrdering() {
        return this.#ordering;
    }

    getTableObject() {
        return this.#magicTable;
    }

    getContainer() {
        return this.#container;
    }

    getSearchEnabled() {
        return this.#searchEnabled;
    }

    setListListener(listener) {
        this.#listListener = listener;
    }

    setListListeners(listener) {
        this.#listListeners.push(listener);
        return this;
    }

    constructor(options = null) {

        if (options == null) {
            options = MagicListOptions.DEFAULT();
        }
        this.#options = options;

        this.#pseudoRowHolder = new PseudoRowHolder(options)

        /**
         * We assume that here can only 1 parameter to be changed.
         * For this, we accept nulls as value by default
         */

    }

    addPseudoRow(row) {
        this.#pseudoRowHolder.addRow(row, this)


    }

    getFetchData() {
        return this.#fetchData;
    }

    getForm() {
        return this.#form;
    }

    getFormClass() {
        return this.#formClass;
    }


    getSort() {
        return this.#sort;
    }

    getOptions() {
        return this.#options;
    }

    getListListener() {
        return this.#listListener;
    }

    getListListeners() {
        return this.#listListeners;
    }

    getSearchAndSettingsBar() {
        return this.#sortAndSearchForm;
    }

    getSearchForm() {
        return this.#searchForm;
    }


    load(where) {
        let _this = this;
        this.#options.fireEvents(new MagicListEvent(MagicListEvent.PRELOAD_LIST, this));
        this.getOptions().getRequest().listFetch((data) => {
            _this.build(data, where)
        })
    }

    deleteById(list, id) {
        try {
            let opts = new MagicPromptOptions("global.prompt.allow_delete_title", "global.prompt.change_group_text.allow_delete_text");
            opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                opts.closeSelf();
                list.deleteRowByContextMenu(list, id)
                list.drawValues();
                opts.markNoRepeat()
            }).setIsAction(MagicPromptButton.OK_LABEL, true)
                .applyClick(MagicPromptButton.CANCEL_LABEL, () => opts.closeSelf()).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                .allowNoRepeat(list.getOptions().getNoRepeatPropertyPrompt()).newPrompt();
        } catch {
            list.deleteRowByContextMenu(list, id)
            dom.message()
        }


    }

    deleteRowByContextMenu(list, id) {

        //todo delete pseudo rows

        if (this.isPseudoRow(id)) {
            list.getPseudoRowHolder().deleteRow(id);
            list.drawValues();
        } else {

            this.#options.getRequest().delete(id, () => {
                LocalCacheProvider.getInstance().deleteFromCache(this.#options.getRequest().copy().fetchData())
                list.drawValues()
            });
        }
    }

    isPseudoRow(id) {
        let stringId = id + "";
        return stringId.indexOf(PseudoRow.PSEUDO_ID_FILED) > -1;
    }


    build(data, where) {

        this.#lastData = data;

        if (!_.isEmpty(this.#options.getPseudoColumns())){
            _.each(this.getOptions().getPseudoColumns(), (column)=>{
                data.fields.push(column.toField())
            })

        }

        let options = this.getOptions();
        if (!_.isEmpty(options.getAvailableFields())) {
            _.each(data.fields, (item) => {
                if (!options.getAvailableFields().includes(item.field)) {
                    item.visible = false;
                }
            })
        }

        this.#id = where;
        this.#container = h.from(where.isH ? where : get(where));

        this.drawListTitle(where);
        //form is used to generate edit form or add form
        this.#form.appendTo(this.#container);

        //grid is a container where <table> is generated
        this.#grid.appendTo(this.#container);
        const customPagination = this.#options.getCustomPagination();
        if (customPagination) {
            customPagination.setParent(this).drawPagination();
        } else {
            this.drawPagination()
        }

        //when we are ready we can draw values from the server

        this.drawValues()

        this.#selectControlPanel = new SelectControlPanel(this, this.#container).build()

        return this;

    }

    drawListTitle() {
        let data = this.#lastData;
        this.#favoriteType = data.objectType;
        if (data.showTitle) {
            h.div("title").text(data.tableTitle).appendTo(this.#container);
        }
        this.#headers = data; //todo check if we still need to hold it here
        this.#sortAndSearchForm.appendTo(this.#container);
        this.drawSettingsForm();
        if (this.#options.getSearchEnabled()) {
            this.drawSearch(data);
        }
        if (this.#options.isShowTotalElements()) {
            this.totalElementsTitleBuild()
        }
        this.drawTableActions(data)
        this.createDataHeaders(data['fields']);
    }

    totalElementsTitleBuild() {
        h.div("total_elements")
            .add(h.div("total_elements_title").text('global.total_elements'))
            .add(this.#totalElements)
            .appendTo(this.#container);
    }

    createDataHeaders() {
        if (_e(this.getOptions().getDisplayType(), MagicListOptions.DISPLAY_AS_TABLE)) {
            this.#magicTable = new MagicTable(this).drawHeader(this.#grid);
        } else {
            this.#magicTable = new MagicGrid(this, this.getOptions().getGridOptions()).drawHeader(this.#grid);
        }

    }

    toggleListDisplayType() {
        _e(this.getOptions().getDisplayType(), MagicListOptions.DISPLAY_AS_TABLE)
        ? this.setDisplayAsGreed()
        : this.setDisplayAsTable()

        return this
    }

    setDisplayAsGreed() {
        if (_e(this.getOptions().getDisplayType(), MagicListOptions.DISPLAY_AS_GREED)) {
            log.promisedDebug(() => ({
                error: "Unable change display type",
                message: `Display type has already been set to ${MagicListOptions.DISPLAY_AS_GREED} type`
            }))
            return this
        }

        if (this.#magicTable) {
            this.#magicTable.getRoot().remove()
        }
        this.getOptions().setDisplayType(MagicListOptions.DISPLAY_AS_GREED)
        this.createDataHeaders();
        this.buildValues(this.#fetchData)

        return this
    }

    setDisplayAsTable() {
        if (_e(this.getOptions().getDisplayType(), MagicListOptions.DISPLAY_AS_TABLE)) {
            log.promisedDebug(() => ({
                error: "Unable change display type",
                message: `Display type has already been set to ${MagicListOptions.DISPLAY_AS_TABLE} type`
            }))
            return this
        }

        if (this.#magicTable){
            this.#magicTable.getOptions().getContainer().remove()
        }

        this.getOptions().setDisplayType(MagicListOptions.DISPLAY_AS_TABLE)
        this.createDataHeaders();
        this.buildValues(this.#fetchData)

        return this
    }

    drawSettingsForm() {
        if (this.getOptions().getSortEnabled()) {
            this.#sort = new MagicSort(this);
            this.#sort.build();
        }

        if (this.getOptions().getOrderingEnabled()) {
            this.#ordering = new MagicOrdering(this);
            this.#ordering.build();
        }
    }

    drawTableActions(data) {

        //List actions are coming from server, if they are existing we need to draw them
        //Default action are: settings : open list settings
        //                    download : download the content of the table
        //                    new      : creates a new element
        // if action differs then it is expected that
        //                   drawCustomAction is defined by user
        // this method should be loaded on the page itself
        if (utils.isEmpty(data.listActions)) {
            return;
        }

        let div = h.div("actions");

        for (let i = 0; i < data.listActions.length; i++) {
            try {
                div.add(this.drawTableAction(data.listActions[i], div, data))
            } catch (ex) {
                log.error(ex);
            }
        }

        this.getSearchAndSettingsBar().add(div);


    }

    drawTableAction(action, div, data) {

        if (_.notNull(this.getOptions().getTableAllowedActions())

            && !this.getOptions().getTableAllowedActions().includes(action)) {
            return;
        }

        try {
            return this.getOptions().getCustomTableAction()(action, div, data)
        } catch (error) {
        }

        let _this = this;
        if (action == 'settings') {
            return h.tag("span").cl("settings")
                .add(h.divImg("sort_icon.png.png").cl(action).wh(16).click(function () {
                    _this.getSort().show();
                    _this.getOrdering().close();
                }))
                .add(h.divImg("settings.png").cl(action).wh(16).click(function () {
                    _this.getOrdering().show();
                    _this.getSort().close();
                })).get();

        }


        if (action == 'download') {


            let _this = this;

            return h.divImg("download.png").cl(action).wh(16).click(function () {

                _this.getOptions().getRequest().download(_this.createPayload(), (data) => {
                    h.a(_this.getOptions().getRequest().copy().getFullPath() + "/root/download/data/" + data.pathToDownload, "", false).get().click();
                })


            }).get();
        }


        if (action == 'new') {
            let __ = this;

            this.#grid.add(div);
            return h.input("button").cl(action).cl("insert_new_btn").text("pages.forms.insert_new").click(this.loadForm).get();
        }

        if (action == 'import') {
            return h.divImg("import.png").wh("16").cl(action).click(function (e) {
                if (e.getAttribute("disabled") != undefined) {
                    dom.message("global.messages.import.in_process", true, ToastType.NODATA)
                }

                let progress = new Loading(_this.getGrid().get());
                progress.show(e);

                _this.getOptions().getRequest().import(() => {
                    progress.hide();
                    dom.toast(_.$("global.messages.success"), ToastType.SUCCESS)
                    _this.drawValues();
                })
            }).get();


        }

        try {
            return drawCustomAction(action, div, null, this, data)
        } catch (ex) {
            log.error(ex)
            return h.span("info.error.list.actions", _.$("global.list.actions.not.implemented." + action));
        }

    }

    loadForm = () => {
        let request = this.getOptions().getFormOptions().getServerRequest()
        this.#formClass = MagicForm.Instance(request.getUniqueId() + this.#options.getAttribute(), this.getOptions().getFormOptions(), this).load();
    }

    drawValues() {
        this.#options.fireEvents(new MagicListEvent(MagicListEvent.PREBUILD_VALUES, this));
        let _this = this;
        this.getOptions().getRequest().search(this.createPayload(), (data) => {
            const adjustedData = structuredClone(data);
            _.each(_this.getPseudoRowHolder().getPseudoRows(), (item) => {
                let dataItem = item;
                dataItem[PseudoRow.PSEUDO_ID_FILED] = item.getId();
                _.ofNullable(adjustedData.content, adjustedData).unshift(item.get())
            });

            _this.buildValues(adjustedData);
            this.#options.fireEvents(new MagicListEvent(MagicListEvent.VALUES_HAS_BEEN_REBUILDED, this));
            this.#selectControlPanel?.updateSelectedCount()
            this.#selectControlPanel?.updateMainCheckbox();
        })
    }

    buildValues(data) {
        this.#fetchData = data;
        this.#options.fireEvents(new MagicListEvent(MagicListEvent.BUILD_DATA_READY, this));
        this.#totalElements._text(data.totalElements);
        this.#magicTable.rebuildHeaders();
        this.#magicTable.drawValues(data, this);
        const customPagination = this.#options.getCustomPagination();
        if (customPagination) {
            customPagination.rebuildPages(data);
        } else {
            this.getPagination().rebuildPages(data);
        }
        if (utils.e(data.content.length, 0)) {
            this.#container.addIf(this.getOptions().getEmptyContentDiv(), this.getOptions().getEmptyContentDiv())
        } else {
            const emptyBlock = this.getOptions().getEmptyContentDiv();
            emptyBlock && emptyBlock.remove();
        }
        this.#options.fireEvents(new MagicListEvent(MagicListEvent.BUILD_VALUES, this, data));
    }

    drawPagination() {
        this.#pagination = new MagicPagination(this.#magicTable, this);
        this.#options.fireEvents(new MagicListEvent(MagicListEvent.PREBUILD_PAGINATION, this, this.#pagination));
        this.#pagination.build(this.getId())
    }

    drawSearch(data) {
        if (this.getOptions().getSearchEnabled()) {
            this.#searchForm = new SearchForm(data, this, this.#options.getSearchFormOptions());
            this.#searchForm.build();
        }
    }

    startActions(event, incomingList = null) {

        let action = event.eventType;
        let id = event.id;
        let list = incomingList || event.parent.parent().getParent();

        if (!_.isNull(list.getOptions()) && !_.isNull(list.getOptions().getContextMenuOptions().getContextMenuListener())) {
            if (list.getOptions().getContextMenuOptions().getContextMenuListener()(event)) {
                return;
            }
        }

        if (_e(action, 'edit')) {
            let request = list.getOptions().getFormOptions().getServerRequest()
            MagicForm.Instance(request.getUniqueId(), list.getOptions().getFormOptions(), list).load(id);
            return;
        }

        if ((_e(action, "copy"))) {
            let copy = true;
            let request = list.getOptions().getFormOptions().getServerRequest()
            MagicForm.Instance(request.getUniqueId(), list.getOptions().getFormOptions(), list).load(id, copy);
            return;
        }


        if (_e(action, 'delete')) {

            if (!_.isEmpty(list.getTableObject().getSelectedIds())) {
                try {
                    let opts = new MagicPromptOptions("global.prompt.allow_delete_title", "global.prompt.change_group_text.allow_delete_text");
                    opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                        opts.closeSelf();
                        list.getOptions().getRequest().deleteItems(list.getTableObject().getSelectedIds().join(","), (data) => {
                            list.getTableObject().resetSelectedIds();
                            list.drawValues();
                        })
                        opts.markNoRepeat()
                    }).setIsAction(MagicPromptButton.OK_LABEL, true)
                        .applyClick(MagicPromptButton.CANCEL_LABEL, () => opts.closeSelf()).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                        .allowNoRepeat(list.getOptions().getNoRepeatPropertyPrompt()).newPrompt();
                } catch {
                    list.getOptions().getRequest().deleteItems(list.getTableObject().getSelectedIds().join(","), (data) => {
                        list.getTableObject().resetSelectedIds();
                        list.drawValues();
                    })
                    dom.message()
                }

            } else {
                list.getTableObject().deselectId(id);
                list.deleteById(list, id);
            }
        }

        if (_e(action, 'details')) {
            try {
                showDetails(id);
            } catch (e) {
                log.error(e)
            }
        }

        if (utils.e(action, 'import')) {
            this.getOptions().getRequest().import(() => {
                dom.toast(_.$("global.messages.success"));
                list.drawValues()
            })
        }

        if (_e(action, MagicMenuEvent.PRE_CREATED_ITEM) && _e(event.item.name, "delete")) {
            if (!_.isEmpty(list.getTableObject().getSelectedIds())) {
                event.data.name = "delete_selected"
            }
        }

    }

    //todo options get server request -> remove

    createPayload() {
        if (_.isNull(this.#pagination) && _.isNull(this.#options.getCustomPagination())) {
            return new MagicRequest();
        }

        //we expect that this method returns Query object
        let query = this.getOptions().getQueryRetriever()(this);

        let limit = 10;
        let page = 0;
        const customPagination = this.#options.getCustomPagination();
        if (customPagination) {
            page = customPagination.getPageNumber();
            limit = customPagination.getPerPage();
        } else {
            page = this.#pagination.getPageNumber();
            limit = this.#pagination.getPerPage();
        }

        if (this.getOptions().getSearchEnabled()) {
            let criteria = this.getSearchForm().getSearchCriteria();
            query.merge(criteria)
        }
        if (this.getSort()) {
            query.getSort().merge(this.getSort().getSort());
        }

        return query.toPayload(page, limit);
    }

}


class SelectControlPanel {
    #list
    #options
    #container
    #mainCheckbox;
    #checkboxesContainer = h.div('checkboxes_container')
    #selectedElementsCounter = h.div("selected_elements").text('global.app.select');
    #deleteSelectedButton
    #deleteAllButton

    constructor(list, parentContainer) {
        this.#list = list
        this.#options = list.getOptions()
        this.#container = parentContainer
    }

    build() {
        let optionsCheckBox = new MagicTrueFalseSelectorOptions(
            "login.page.save_password",
            false,
            false,
            false,
            true
        );

        this.#checkboxesContainer.prependTo(this.#container);

        if (this.#options.getIsSelectAllEnabled()) {
            this.#mainCheckbox = new MagicTrueFalseSelector(optionsCheckBox)
            optionsCheckBox.setComponentListener((event) => {
                this.#list.getTableObject().toggleAllCheckboxes(event.currentValue)
            });


            let listener = (ev) => {
                if (_e(ev.eventName, Selectable.ADD_SELECTED_ID) || _e(ev.eventName, Selectable.DELETE_SELECTED_ID)) {
                    this.updateSelectedCount();
                    this.updateMainCheckbox();
                }
            }
            this.#options.setListListeners(listener);

            this.#mainCheckbox.draw(this.#checkboxesContainer);
            this.#selectedElementsCounter.appendTo(this.#checkboxesContainer)
        }

        if (this.#options.getIsDeleteSelectedEnabled()) {
            this.initDeleteSelectedElementsButton();
            this.#deleteSelectedButton.appendTo(this.#checkboxesContainer)
        }

        if (this.#options.getIsDeleteAllButtonEnabled()) {
            this.initDeleteAllButton()
            this.#deleteAllButton.appendTo(this.#checkboxesContainer)
        }

        if (this.#options.getIsShareButtonEnabled()) {
            this.initShareButton(this.#checkboxesContainer)
        }

        return this
    }

    updateSelectedCount() {
        let selectedAmount = this.#list.getTableObject().getSelectedIds().length
        if (selectedAmount > 0) {
            this.#selectedElementsCounter
                .text('global.app.select_elements')
                .andText(selectedAmount, false)
                .andText('global.app.out_of', true)
                .andText(this.#list.getFetchData()?.totalElements, false)
        } else {
            this.#selectedElementsCounter.text('global.app.select')
        }
        if (this.#options.getIsDeleteSelectedEnabled()) {
            selectedAmount > 0 ? this.#deleteSelectedButton.enable() : this.#deleteSelectedButton.disable()
        }
    }

    updateMainCheckbox() {
        let chosen = !this.#list.isSelectedIdsEmpty()

        this.#list.getTableObject().getCheckboxes().values().forEach((item) => {
            if (_.isFalse(item.checkbox.getValue())) {
                chosen = false;
            }
        })

        this.#mainCheckbox?.setValue(chosen, false)
    }

    initDeleteSelectedElementsButton() {
        this.#deleteSelectedButton = h.div('delete_chosen_div')
            .text('global.app.remove_selected_button')
            .click(() => {
                if (this.#list.isSelectedIdsEmpty()) {
                    return
                }

                this.#options.getRequest().deleteItems(this.#list.getTableObject().getSelectedIds().join(","), (data) => {
                    this.#list.drawValues();
                    this.#list.getTableObject().resetSelectedIds();
                    this.updateSelectedCount();
                })
            })
    }

    initDeleteAllButton() {
        const {callback, title, toastMessage, promptMessage} = this.#options.getDeleteAllButtonOptions()
        this.#deleteAllButton = h.div('delete_all_elements')
            .add(h.div("delete_all_elements_content").text(title))
            .click(() => {
                if (_.isFalse(this.#list.getFetchData()?.totalElements)) {
                    dom.message(toastMessage, true, ToastType.ERROR);
                    return;
                }
                let opts = new MagicPromptOptions(null, promptMessage);
                opts.applyClick(MagicPromptButton.OK_LABEL, () => {
                    if (callback) {
                        callback()
                    }
                    this.#list.getTableObject().resetSelectedIds();
                    this.#list.getTableObject().resetCheckboxes()
                    this.#options.fireEvents(new MagicListEvent(MagicListEvent.CLEAR_LIST, this));

                    opts.closeSelf()

                }).setIsAction(MagicPromptButton.OK_LABEL, true)
                    .applyClick(MagicPromptButton.CANCEL_LABEL, () => {
                        opts.closeSelf()
                    }).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
                    .allowNoRepeat("global.prompt_DELETE").newPrompt();
            })
    }

    initShareButton(parentContainer) {
        h.div('share').text('cart.all.share').appendTo(parentContainer)
    }
}
