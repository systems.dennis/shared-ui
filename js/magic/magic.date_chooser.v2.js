class DateOptions {
    static SELECTED_CLASS = "date-chooser__selected";
    static DATE_CLASS = "date-chooser__date";
    static DAY_CLASS = "date-chooser__day";
    static CLOSE_CLASS = "close-date-chooser";
    static VALUE = "value";
    static BOTTOM_CLASS = "date-chooser__bottom";
    static APPLY_CLASS = "date-chooser__apply";
    static APPLY_TRANSLATE = "global.app.list.search.apply";
    static ITEM_CLASS = "calendar-item";
    static MODAL_CLASS = "choose-calendar";
    static SELECT_CLASS = "date-select";
    static DATE_DIV_CLASS = "date-chooser__date-div";
    static TIME_DIV_CLASS = "date-chooser__date-div";
    static NO_VALUE = "no-value"
    static WRAPPER_CLASS = "choose-calendar-wrapper";
    static DD = "dd";
    static MM = "mm";
    static YYYY = "yyyy";
    static hh = "h";
    static mm = "m";
    static ss = "s";

    #listener;
    #splitter;
    #date;
    #needPresetDate;
    #format;
    #where;
    #showTime = false;
    #timeFormat;

    constructor(where, splitter = ".", format = "dd.mm.yyyy", date = "", listener = null, needPresetDate = true,) {
        this.#date = date;
        this.#listener = listener;
        this.#needPresetDate = needPresetDate;
        this.#format = format;
        this.#splitter = splitter;
        this.#where = where;

        this.replaceSplitterInValues();
    }

    replaceSplitterInValues() {

        if (this.#date) {
            this.#date = this.#date.split(".").join(this.#splitter);
        } else {
            this.#date = MagicDateChooser.getToday().split(".").join(this.#splitter);
        }

        this.#format = this.#format.split(".").join(this.#splitter);
    }

    setListener(val) {
        this.#listener = val;

        return this;
    }

    getTimeFormat() {
        return this.#timeFormat;
    }

    setNeedPresetDate(val) {
        this.#needPresetDate = val;

        return this;
    }

    setFormat(val) {
        this.#format = val;

        return this;
    }

    setSplitter(val) {
        this.#splitter = val;

        return this;
    }

    setShowTime(format) {
        this.#showTime = true;
        this.#timeFormat = format;

        return this;
    }

    isShowTime() {
        return this.#showTime;
    }

    getListener() {
        return this.#listener;
    }

    getSplitter() {
        return this.#splitter;
    }

    getDate() {
        return this.#date;
    }

    getNeedPresetDate() {
        return this.#needPresetDate;
    }

    getFormat() {
        return this.#format;
    }

    getWhere() {
        return this.#where;
    }

    createEvent(eventName, chooser, data) {
        this.#listener(new DateEvent(eventName, chooser, data));
    }
}


class DateColumn {
    #container = h.div("date-select__column");

    #columnValues;

    #selectedComponent;

    #value;

    #parent;

    constructor(columnValues, parent, value) {
        this.#columnValues = columnValues;

        this.#parent = parent;

        this.#value = value;

        this.#container.appendTo(this.#parent.getSelect())
    }

    getContainer() {
        return this.#container;
    }

    getParent() {
        return this.#parent;
    }

    getValue() {
        return this.#value;
    }

    getColumnValues() {
        return this.#columnValues;
    }

    getSelectedComponent() {
        return this.#selectedComponent;
    }

    setValue(val) {
        this.#value = val;

        return this;
    }

    setColumnValues(val) {
        this.#columnValues = val;

        return this;
    }

    preselectItemClass() {
        this.#container.eachOf((el) => {
            if (_e(parseInt(h.from(el).getData(DateOptions.VALUE)), parseInt(this.#value))) {

                this.#selectedComponent = h.from(el).cl(DateOptions.SELECTED_CLASS);

                return;
            }
        })

        return this;
    }

    addItemsToColumn() {
        this.getContainer().text(null);

        utils.each(this.#columnValues, (item) => {
            let itemH = h.div(DateOptions.ITEM_CLASS).text(item, false).appendTo(this.#container);

            itemH.setData(DateOptions.VALUE, item).click((el) => this.itemClick(el));
        })
    }

    itemClick(element, rebuildValue = true) {
        this.removeActiveClass();

        this.#selectedComponent = h.from(element).cl(DateOptions.SELECTED_CLASS).scrollIntoView()

        let val = h.from(element).getData(DateOptions.VALUE);

        this.#value = val > 9 ? val : "0" + val;
        if (!rebuildValue) {
            return;
        }
        this.#parent.rebuildValue();
    }

    removeActiveClass() {
        this.#container.eachOf((el) => {
            h.from(el).rcl(DateOptions.SELECTED_CLASS);
        })
    }
}

class Months extends DateColumn {

    type = "mm"

    constructor(columnValues, parent, value) {
        super(columnValues, parent, value);
    }

    addItemsToColumn() {

        this.getContainer().text(null);

        utils.each(this.getColumnValues(), (item, index) => {
            let itemH = h.div(DateOptions.ITEM_CLASS).text(item).appendTo(this.getContainer());

            itemH.setData(DateOptions.VALUE, index + 1).click((el) => this.itemClick(el));
        })
    }
}

class Days extends DateColumn {

    type = "dd"

    constructor(columnValues, parent, value) {
        super(columnValues, parent, value);
    }

    addItemsToColumn() {

        this.getContainer().text(null);

        utils.each(this.getColumnValues(), (item) => {

            let itemH = h.div(DateOptions.ITEM_CLASS).appendTo(this.getContainer());

            let needTranslate = item.day ? true : false;

            itemH.setData(DateOptions.VALUE, item.date).click((el) => this.itemClick(el))
                .add(h.div(DateOptions.DATE_CLASS).text(item.date, false))
                .add(h.div(DateOptions.DAY_CLASS).text(item.day, needTranslate));
        })

        return this;
    }

}

class Years extends DateColumn {

    type = "yyyy"

    constructor(columnValues, parent, value) {
        super(columnValues, parent, value);
        this.scrollListener();
    }

    scrollListener() {

        let block = this.getContainer().get();

        this.getContainer().handleEvent("scroll", () => {

            if (!this.getContainer().get().scrollTop) {

                utils.each(this.buildYeasInTop(), (yearH) => {

                    yearH.prependTo(this.getContainer());

                })

                this.getContainer().get().scrollTo(0, 30)
            }

            if (block.offsetHeight + block.scrollTop + 5 >= block.scrollHeight) {
                utils.each(this.buildYeasInBottom(), (yearH) => {

                    yearH.appendTo(this.getContainer());
                })
            }
        })
    }

    buildYeasInTop() {
        let from = this.getContainer().first().val();

        let arrOfYears = [];

        for (let i = 1; i < 4; i++) {

            let year = from - 0 + i;

            let itemH = h.div(DateOptions.ITEM_CLASS).text(year, false).setData(DateOptions.VALUE, year).click((el) => this.itemClick(el));

            arrOfYears.push(itemH)
        }

        return arrOfYears;
    }

    buildYeasInBottom() {
        let from = this.getContainer().last().val();

        let arrOfYears = [];

        for (let i = 1; i < 5; i++) {

            let year = from - i;

            let itemH = h.div(DateOptions.ITEM_CLASS).text(year, false).setData(DateOptions.VALUE, year).click((el) => this.itemClick(el));

            arrOfYears.push(itemH)
        }

        return arrOfYears;
    }
}


class Hour extends DateColumn {
    max = 24;
    type = "h";

    constructor(columnValues, parent, value) {
        super(columnValues, parent, value);
        this.setValue('00');
    }

    addItemsToColumn() {
        for (let i = 0; i < this.max; i++) {
            const text = `${i}`.length > 1 ? i : '0' + i;
            let itemH = h.div(DateOptions.ITEM_CLASS).text(text, false).appendTo(this.getContainer()).click((element) => this.itemClick(element, i));

            itemH.setData(DateOptions.VALUE, i).click((el) => this.itemClick(el));
        }
    }

    itemClick(element, i) {
        this.setValue(i);
        super.itemClick(element, true);
        this.getParent().changeTime();
    }
}

class Minute extends Hour {
    max = 60;
    type = "m"

    constructor(columnValues, parent, value) {
        super(columnValues, parent, value);
        this.setValue('00');
    }
}

class Second extends DateColumn {

    type = "s"

    constructor(columnValues, parent, value) {
        super(columnValues, parent, value);
    }

    addItemsToColumn() {

        this.getContainer().text(null);

        utils.each(this.getColumnValues(), (item, index) => {
            let itemH = h.div(DateOptions.ITEM_CLASS).text(item, false).appendTo(this.getContainer());

            itemH.setData(DateOptions.VALUE, index).click((el) => this.itemClick(el));
        })
    }
}


class MagicDateChooser {

    static addZeroToDateString(dateNumber) {
        return dateNumber > 9 ? dateNumber + "" : '0' + dateNumber;
    }


    static getToday(toShowTime = false) {
        let date = new Date().getDate();
        let month = new Date().getMonth() + 1;

        return this.addZeroToDateString(date) + "." + MagicDateChooser.addZeroToDateString(month) + "." + new Date().getFullYear();
    }

    static getNow() {
        let hour = new Date().getHours();
        let minute = new Date().getMinutes();
        let second = new Date().getSeconds();

        return this.addZeroToDateString(hour) + "." + this.addZeroToDateString(minute) + "." + this.addZeroToDateString(second);
    }

    #wrapper = h.div(DateOptions.WRAPPER_CLASS);

    #closeBtn;

    #modal = h.div(DateOptions.MODAL_CLASS).appendTo(this.#wrapper);

    #select = h.div(DateOptions.SELECT_CLASS).appendTo(this.#modal);
    #dateDiv = h.div(DateOptions.DATE_DIV_CLASS);
    #timeDiv = h.span(DateOptions.TIME_DIV_CLASS);

    #bottom = h.div(DateOptions.BOTTOM_CLASS);

    #applyBtn = h.div(DateOptions.APPLY_CLASS).text(DateOptions.APPLY_TRANSLATE);

    #columns = [];

    #timeColumns = [];

    #options;

    #splitter = ".";

    #daysColumn;

    #value;
    #time = {
        h: '00',
        m: '00'
    };

    constructor(options) {
        this.#options = options;

        this.#splitter = options.getSplitter();

        this.#value = this.#options.getDate();

    }

    setTime(time = null) {
        if (!time) {
            this.#timeDiv.text('00.00', false);
        } else {
            this.#timeDiv.text(time, false);
        }
    }

    init() {

        this.initColumns();
        this.rebuildDays();

        this.#options.getWhere().click(() => this.draw());

        return this;
    }

    setNewDate(date) {

        utils.each(this.#columns, col => {
            col.getContainer().remove();
        })

        if (date) {
            this.#options.setNeedPresetDate(true);

            this.#value = date;
        } else {
            this.#options.setNeedPresetDate(false);

            this.#value = MagicDateChooser.getToday();
        }

        this.#columns = [];

        this.initColumns();

        this.rebuildDays();

        // this.#options.getWhere().text(date, false);
    }

    getDateObject() {

        let date = {};

        utils.each(this.#columns, col => {
            date[col.type] = col.getValue();
        })

        return date;
    }

    getSelect() {
        return this.#select;
    }

    changeTime() {
        this.#timeDiv.text(null);
        this.#timeColumns.forEach((e) => {
            const value = e.getValue();
            if (value) {
                this.#time[e.type] = value;
            }
        })
        const text = this.#time.h + this.#splitter + this.#time.m;
        this.#timeDiv.text(text, false);
        this.#options.createEvent('time_changed', this, this.#time);
    }

    draw() {

        this.#closeBtn = h.div(DateOptions.CLOSE_CLASS).appendTo(this.#modal).click((el, event) => {
            event.preventDefault();
            this.close();
        });

        this.drawDateColumns();
        this.drawTimeColumns();
        this.drawBottom();
        this.setTime();
        this.#wrapper.appendToBody();
        this.writeDateToPanel();

        if (this.#options.getNeedPresetDate()) {
            this.preselect();
        }

        this.scrollToSelectedComponent();

        this.scrollToCurrentYear();

        this.#options.createEvent(DateEvent.CALENDAR_OPEN, this, this.transformDateToStr())
    }

    close() {
        this.#closeBtn.remove();
        this.#wrapper.remove();
        this.#options.createEvent(DateEvent.CALENDAR_CLOSE, this, this.transformDateToStr())
    }

    preselect() {
        if (this.#options.getNeedPresetDate()) {
            utils.each(this.#columns, (col) => {
                col.preselectItemClass()
            })
        }
    }

    initColumns() {
        let dateSplit = MagicDateChooser.getToday().split(this.#splitter);
        this.#options.getFormat().split(this.#splitter).map((dateItem, i) => {
            if (dateSplit) {
                if (_e(dateItem, DateOptions.DD)) {

                    this.#daysColumn = new Days(null, this, dateSplit[0])

                    this.#columns.push(this.#daysColumn);
                }

                if (_e(dateItem, DateOptions.MM)) {
                    this.#columns.push(new Months(DateConstants.MONTHS, this, dateSplit[1]))
                }

                if (_e(dateItem, DateOptions.YYYY)) {
                    this.#columns.push(new Years(DateConstants.getArrayOfYears(), this, dateSplit[2]))
                }
            }
        });
        if (this.#options.isShowTime()) {
            this.initTimeColumns();
        }
    }

    initTimeColumns() {
        let dateSplit = MagicDateChooser.getNow().split(this.#splitter);
        this.#options.getTimeFormat().split(this.#splitter).map((dateItem, i) => {
            if (dateSplit) {
                if (_e(dateItem, DateOptions.hh)) {
                    this.#timeColumns.push(new Hour(DateConstants.getArrayOfYears(), this, dateSplit[2]))
                }

                if (_e(dateItem, DateOptions.mm)) {
                    this.#timeColumns.push(new Minute(DateConstants.getArrayOfYears(), this, dateSplit[2]))
                }
            }
        });
    }

    rebuildDays() {
        if (utils.isNull(this.#daysColumn)) return;

        let mm = this.getDateObject().mm;
        let yyyy = this.getDateObject().yyyy;
        if (mm && yyyy) {

            let countOfDays = this.daysInMonth(mm, yyyy);

            let daysArr = []
            for (let i = 0; i < countOfDays; i++) {
                daysArr.push(i)
            }

            daysArr = daysArr.map((el) => {
                return {
                    date: el + 1,
                    day: DateConstants.WEEKDAYS[new Date(yyyy, mm - 1, el).getDay()]
                }
            })
            this.#daysColumn.setColumnValues(daysArr).addItemsToColumn();
        }

    }

    drawDateColumns() {

        utils.each(this.#columns, (col) => {
            col.addItemsToColumn();
        })
    }

    drawTimeColumns() {
        utils.each(this.#timeColumns, (col) => {
            col.addItemsToColumn();
        })
    }

    drawBottom() {
        this.#bottom.add(this.#dateDiv);

        this.writeDateToPanel();
        this.#timeDiv.appendTo(this.#bottom);
        this.#bottom.add(this.#applyBtn.click(() => this.apply()));

        this.#bottom.appendTo(this.#modal);
    }

    transformDateToStr() {
        return this.#columns.map((el) => {
            return el.getValue();
        }).join(this.#splitter)
    }

    writeDateToPanel() {
        this.#dateDiv.text(this.transformDateToStr(), false)
    }

    apply() {
        this.close();
        let value = _.rebuildDataToUserSettings(this.transformDateToStr().split(this.#splitter).reverse().join(this.#splitter));
        if (this.#timeColumns.length) {
            value += ` ${this.#time.h}.${this.#time.m}`;
        }
        this.#options.getWhere().text(value, false);

        this.#options.setNeedPresetDate(true);
        this.getTimeInMilliseconds()
        this.#options.createEvent(DateEvent.DATE_SELECTED, this, this.getTimeInMilliseconds());
    }

    getTimeInMilliseconds() {
        let obj = this.getDateObject()
        return Date.UTC(obj.yyyy, obj.mm-1, obj.dd)
    }

    daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }

    setMaxDay(date) {
        let maxDays = this.columnValues(date.month, date.year).length;

        if (date.day > maxDays) date.day = maxDays;
    }

    rebuildValue() {

        this.rebuildDays();
        this.writeDateToPanel();

        utils.each(this.#columns, (col) => {
            col.preselectItemClass()
        })

        this.scrollToSelectedComponent();

        this.#options.createEvent(DateEvent.VALUE_IS_SET, this, this.transformDateToStr())
    }

    scrollToSelectedComponent() {
        _.each(this.#columns, (column) => {
            let selected = column.getSelectedComponent();

            if (selected) {
                selected.scrollIntoView();
            }
        })
    }

    scrollToCurrentYear() {
        if (!this.#options.getNeedPresetDate()) {

            let arrWithYearColumn = this.#columns.filter((col) => _e(col.type, "yyyy"));

            if (arrWithYearColumn.length) arrWithYearColumn[0].getContainer().get().scrollTo(0, 23)
        }
    }
}

