class AuthOptions {

    #additionalHeaders = new Map();

    #allowSavePassword = true;

    #onSuccessLogin;

    #onFailLogin;

    #failLogoutFunction;

    #allow2fa = true;

    #authorization;


    disallow2fa() {
        this.#allow2fa = false;
    }


    isAllow2fa() {
        return this.#allow2fa;
    }

    #successLogoutFunction;

    static JSON = "application/json";

    setSuccessLogoutFunction(successLogoutFunction) {
        this.#successLogoutFunction = successLogoutFunction
        return this
    }

    getSuccessLogoutFunction() {
        return _.ofNullable(this.#successLogoutFunction, () => () => {
            dom.toast("global.success", ToastType.SUCCESS, true)
        })
    }

    setFailLogoutFunction(failLogoutFunction) {
        this.#failLogoutFunction = failLogoutFunction
        return this
    }

    getFailLogoutFunction() {
        return _.ofNullable(this.#failLogoutFunction, () => () => {
            dom.toast("global.fail", ToastType.ERROR, true)
        })
    }

    isAllowSavePassword() {
        return this.#allowSavePassword;
    }

    disallowSavePassword() {
        this.#allowSavePassword = false;
    }

    getAdditionalHeaders() {
        return Object.fromEntries(this.#additionalHeaders.entries());
    }

    addAdditionalHeader(key, value) {
        this.#additionalHeaders.set(key, value)
        return this;
    }

    onSuccessLogin() {
        return this.#onSuccessLogin;
    }

    onFailLogin() {
        return this.#onFailLogin;
    }

    setSuccessLogin(fun) {
        this.#onSuccessLogin = fun;
        return this;
    }

    setOnFailLogin(fun) {
        this.#onFailLogin = fun;
        return this;
    }

    constructor(authorization = new Authorization()) {
        this.#authorization = authorization;
        this.addAdditionalHeader("Content-Type", AuthOptions.JSON)
        this.addAdditionalHeader("Accept", AuthOptions.JSON)
    }

    getAuthorization() {
        return this.#authorization;
    }

    asXHRHeader() {
        return Object.fromEntries(this.#additionalHeaders);
    }


}

class LoginObject {
    #login;
    #password;
    #scope;
    #twoFa;
    #authType;


    constructor(login, password, twoFa = null, scope = null, authType = "DEFAULT") {
        this.#login = login;
        this.#password = password;
        this.#twoFa = twoFa;
        this.#scope = _.ofNullable(scope, Environment.authScope);
        this.#authType = authType
    }


    getTwoFa() {
        return this.#twoFa?.trim();
    }

    getAuthType() {
        return this.#authType;
    }

    getScope() {
        return this.#scope;
    }

    getLogin() {
        return this.#login?.trim();
    }

    getPassword() {
        return this.#password?.trim();
    }

    login(options, parent) {
        options.getAuthorization().login(this, options, parent)
    }
}

class AbstractAuthorization {

    //performs login to auth service
    //login object is instance of LoginObject, loginOptions instance of LoginOptions
    login(loginObject, loginOptions, parent) {
    }

    //performs logout from auth server and session
    logout() {
    };

    //Stores authorization data
    storeUserData(data) {
    };

    //copies data to local storage
    saveLoginDataToLocalStorage() {
    };

    //updates sessionstorage from localstorage
    reloadSessionFromCache() {
    };

    //should return a string url to login
    getLoginUrl() {
    };

    //should return a string url to logout
    getLogoutUrl() {
    };

    //performed when session is cleaned and needed some more actions
    afterSessionCleaned() {
    };

    //returns current token
    getCurrentToken() {

    }

    getSessionToken(auth) {

    }

    //clears session data of login
    clear() {

    }

    setScope(scope) {

    }

    //returns current scope
    getScope() {

    }
}

class Authorization extends AbstractAuthorization {
    static TOKEN_FIELD = "___TOKEN___";
    static SCOPE_FIELD = "___SCOPE___";
    static ID_FIELD = "___id";
    static LOGIN_FIELD = "___login";
    static EMAIL_FIELD = "___email";
    static NAME_FIELD = "___name";
    static PHONE_FIELD = "___phone";
    static ADDRESS_FIELD = "___address";
    static IMAGE_FIELD = "___image";
    static DUE_FIELD = "___due";
    static AUTH_SCOPE = "___auth_scope"
    static REFRESH_TOKEN = "___REFRESH_TOKEN___";
    static REMEMBER = "___REMEMBER___";

    #scope;
    #parent;
    #authType = "DEFAULT";

    setParent(parent) {
        if (!parent instanceof AbstractAuthorizationWindow) {
            throw Error("parent should implement window functions")
        }
        this.#parent = parent;
        return this;
    }

    getParent() {
        return this.#parent;
    }

    createPayload(loginObject) {
        return {
            'login': loginObject.getLogin(),
            'twoFactorCode': loginObject.getTwoFa(),
            'password': loginObject.getPassword()
        }
    }

    getSessionToken() {

        let token = sessionStorage.getItem(Authorization.TOKEN_FIELD) || cache.get(Authorization.TOKEN_FIELD)
        let due = sessionStorage.getItem(Authorization.DUE_FIELD) || cache.get(Authorization.DUE_FIELD);
        let refreshToken = sessionStorage.getItem(Authorization.REFRESH_TOKEN) || cache.get(Authorization.REFRESH_TOKEN)
        let rememberMarker = sessionStorage.getItem(Authorization.REMEMBER) || cache.get(Authorization.REMEMBER)

        //================= check that due is ok ========

        if (_.notNull(due) && due <= new Date().getUTCDate() && JSON.parse(rememberMarker)) {
            try {
                this.refreshTokenRequest(refreshToken)
            } catch (error) {
                return null
            }
        }
        return token;
    }

    refreshTokenRequest(refreshToken) {
        let authHeaders = new AuthOptions()
            .addAdditionalHeader("AUTH_SCOPE", this.getScope())
            .addAdditionalHeader("AUTH_TYPE", this.getAuthType())
            .getAdditionalHeaders();

        const refreshPayload = {refreshToken: refreshToken};

        Executor.runPostWithPayload(
            this.getRefreshTokenUrl(),
            (data) => {
                if (data.dto.token) {
                    this.storeUserData(data.dto);
                }
            },
            refreshPayload,
            (error) => {
                this.clear()
            },
            authHeaders, false
        );
    }

    getRefreshTokenUrl() {
        return Environment.authApi + "/api/v3/auth/refresh";
    }


    hasToken() {
        return _.notNull(this.getSessionToken())
    }

    static clearCache() {

        sessionStorage.clear();
        cache.removeAll(Authorization.TOKEN_FIELD, Authorization.DUE_FIELD, Authorization.ID_FIELD, Authorization.REFRESH_TOKEN,
            Authorization.IMAGE_FIELD, Authorization.ADDRESS_FIELD, Authorization.PHONE_FIELD, Authorization.NAME_FIELD,
            Authorization.EMAIL_FIELD, Authorization.PHONE_FIELD, Authorization.LOGIN_FIELD, Authorization.AUTH_SCOPE,
            Authorization.REMEMBER);

        sessionStorage.clear();
    }

    static me() {
        return cache.get(Authorization.ID_FIELD)
    }

    static isLoggedIn() {
        return _.notNull(this.me())
    }

    login(loginObject, loginOptions, parent) {
        this.setParent(parent);
        let authHeaders = loginOptions
            .addAdditionalHeader("AUTH_SCOPE", this.getScope())
            .addAdditionalHeader("AUTH_TYPE", loginObject.getAuthType())
            .getAdditionalHeaders();

        log.promisedDebug(() => authHeaders);

        // Authorization.setErrorLogin(false);
        Executor.runPostWithPayload(this.getLoginUrl(), (data) => {


            this.processLoginData(data.dto, loginOptions.isAllowSavePassword());

            if (_.notNull(loginOptions.onSuccessLogin())) {
                loginOptions.onSuccessLogin()(data, this.getParent());
            }


            let redirect = MagicPage.getPageParam("r");

            if (redirect) {
                new SmartRedirect(redirect)
                    .redirect()

            }

        }, this.createPayload(loginObject), (e) => {

            loginOptions.onFailLogin()(e, this.getParent());
        }, authHeaders)


    }

    processLoginData(data, storeData) {
        this.storeUserData(data);
        if (storeData) {
            this.saveLoginDataToLocalStorage(data);
        }
    }

    getLoginUrl() {
        return Environment.authApi + "/api/v3/auth/login";
    }

    getLogoutUrl() {
        return Environment.authApi + "/api/v3/auth/logout";
    }

    logout(options) {
        Executor.runPostWithFailFunction(this.getLogoutUrl(), (e) => {
            this.clear();
            options.getSuccessLogoutFunction()();

        }, (e) => {
            options.getFailLogoutFunction()(e);
        }, true, options
            .addAdditionalHeader("AUTH_SCOPE", this.getScope())
            .addAdditionalHeader('Authorization', "Bearer " + this.getCurrentToken()).asXHRHeader())

    }

    clear() {
        Authorization.clearCache();
        this.afterSessionCleaned();
    }

    afterSessionCleaned() {
    };

    reset() {

    }

    getCurrentToken() {
        return sessionStorage.getItem(Authorization.TOKEN_FIELD);
    }

    getCurrentUserId() {
        return sessionStorage.getItem(Authorization.ID_FIELD);
    }

    getScope() {
        return _.ofNullable(this.#scope, () => sessionStorage.getItem(Authorization.SCOPE_FIELD));
    }

    setScope(scope) {
        this.#scope = scope;
        return this;
    }

    setAuthType(type) {
        this.#authType = type;
        return this;
    }

    getAuthType() {
        return this.#authType;
    }

    storeUserData(data) {
        if (_.isNull(data.token)) {
            throw Error();
        }
        sessionStorage.setItem(Authorization.TOKEN_FIELD, data.token);
        sessionStorage.setItem(Authorization.REFRESH_TOKEN, data.refreshToken);
        sessionStorage.setItem(Authorization.SCOPE_FIELD, data.scope);
        sessionStorage.setItem(Authorization.ID_FIELD, data.userData.id);
        sessionStorage.setItem(Authorization.LOGIN_FIELD, data.userData.login);
        sessionStorage.setItem(Authorization.EMAIL_FIELD, data.userData.email);
        sessionStorage.setItem(Authorization.NAME_FIELD, data.userData.name);
        sessionStorage.setItem(Authorization.PHONE_FIELD, data.userData.phone);
        sessionStorage.setItem(Authorization.ADDRESS_FIELD, data.userData.address);
        sessionStorage.setItem(Authorization.IMAGE_FIELD, data.userData.imagePath);
        sessionStorage.setItem(Authorization.DUE_FIELD, data.due);
    }

    saveLoginDataToLocalStorage(data) {
        cache.add(Authorization.TOKEN_FIELD, data.token);
        cache.add(Authorization.SCOPE_FIELD, data.scope);
        cache.add(Authorization.REFRESH_TOKEN, data.refreshToken);
        cache.add(Authorization.ID_FIELD, data.userData.id);
        cache.add(Authorization.LOGIN_FIELD, data.userData.login);
        cache.add(Authorization.EMAIL_FIELD, data.userData.email);
        cache.add(Authorization.NAME_FIELD, data.userData.name);
        cache.add(Authorization.PHONE_FIELD, data.userData.phone);
        cache.add(Authorization.ADDRESS_FIELD, data.userData.address);
        cache.add(Authorization.IMAGE_FIELD, data.userData.imagePath);
        cache.add(Authorization.DUE_FIELD, data.due);
    }

    reloadSessionFromCache() {
        if (_.isNull(cache.get(Authorization.TOKEN_FIELD))) {
            return
        }
        Authorization.copyField(Authorization.TOKEN_FIELD);
        Authorization.copyField(Authorization.ID_FIELD);
        Authorization.copyField(Authorization.REFRESH_TOKEN);
        Authorization.copyField(Authorization.SCOPE_FIELD);
        Authorization.copyField(Authorization.LOGIN_FIELD);
        Authorization.copyField(Authorization.EMAIL_FIELD);
        Authorization.copyField(Authorization.PHONE_FIELD);
        Authorization.copyField(Authorization.TOKEN_FIELD);
        Authorization.copyField(Authorization.ADDRESS_FIELD);
        Authorization.copyField(Authorization.IMAGE_FIELD);
        Authorization.copyField(Authorization.DUE_FIELD);
    }
}

/**
 * Unlike a common Authorization holds a multiple authorizations in sessions
 * to invoke logout operation it should also have scope set before the operation
 */
class MultiScopeAuthorization extends Authorization {
    static AUTHORIZATION_SCOPE_PREFIX = "app.login.data"

    saveLoginDataToLocalStorage(data) {
        cache.add(this.getScopeCacheProperty(this.getScope()), JSON.stringify(data))
    }

    getScopeCacheProperty(scope) {
        return MultiScopeAuthorization.AUTHORIZATION_SCOPE_PREFIX + "_" + scope;
    }

    getByScope(scope) {
        return cache.get(this.getScopeCacheProperty(scope));
    }

    getCurrentToken() {
        return this.getSessionToken()
    }

    getCurrentUserId() {
        return this.getCurrentUserData()?.userData.id
    }

    storeUserData(data) {
        if (_.isNull(data.token)) {
            //todo move here converter to token object instead of strict error
            throw Error("no token here");
        }

        sessionStorage.setItem(this.getScopeCacheProperty(this.getScope()), JSON.stringify(data));
    }

    clear() {
        sessionStorage.removeItem(this.getScopeCacheProperty(this.getScope()))
        cache.remove(this.getScopeCacheProperty(this.getScope()));
        super.afterSessionCleaned();
    }

    getSessionToken() {

        if (_.isNull(this.getCurrentUserData())) {
            return null;
        }

        let token = this.getCurrentUserData().token;
        let refreshToken = this.getCurrentUserData().refreshToken;
        let due = this.getCurrentUserData().due;
        let rememberMarker = sessionStorage.getItem(Authorization.REMEMBER) || cache.get(Authorization.REMEMBER)

        if (due < new Date().getTime() && JSON.parse(rememberMarker)) {
            try {
                this.refreshTokenRequest(refreshToken)
            } catch (error) {
                return null
            }
        }
        return token;
    }


    login(loginObject, loginOptions, parent) {
        this.setParent(parent);
        let authHeaders = loginOptions
            .addAdditionalHeader("AUTH_SCOPE", this.getScope())
            .addAdditionalHeader("AUTH_TYPE", loginObject.getAuthType())
            .getAdditionalHeaders();

        log.promisedDebug(() => authHeaders);

        // Authorization.setErrorLogin(false);
        Executor.runPostWithPayload(this.getLoginUrl(), (data) => {


            this.processLoginData(data.dto, loginOptions.isAllowSavePassword());

            if (_.notNull(loginOptions.onSuccessLogin())) {
                loginOptions.onSuccessLogin()(data, this.getParent());
            }


            let redirect = MagicPage.getPageParam("r");

            if (redirect) {
                new SmartRedirect(redirect)
                    .redirect()

            }

        }, this.createPayload(loginObject), (e) => {

            loginOptions.onFailLogin()(e, this.getParent());
        }, authHeaders)

    }


    refreshTokenRequest(refreshToken) {
        let authHeaders = new AuthOptions()
            .addAdditionalHeader("AUTH_SCOPE", this.getScope())
            .addAdditionalHeader("AUTH_TYPE", this.getAuthType())
            .getAdditionalHeaders();

        const refreshPayload = {refreshToken: refreshToken};

        Executor.runPostWithPayload(
            this.getRefreshTokenUrl(),
            (data) => {
                if (data.dto.token) {
                    this.storeUserData(data.dto);
                }
            },
            refreshPayload,
            (error) => {
                this.clear()
            },
            authHeaders, false
        );
    }

    getRefreshTokenUrl() {
        return Environment.authApi + "/api/v3/auth/refresh";
    }

    getCurrentUserData() {
        //first look in session
        let res = sessionStorage.getItem(this.getScopeCacheProperty(this.getScope()));
        //if not found search in cache
        if (!res) res = cache.get(this.getScopeCacheProperty(this.getScope()));
        //if still not found return null (we don't have any info
        if (!res) return res;
        res = JSON.parse(res);
        if (res.due <= new Date().getUTCDate()) {
            this.clear();
            return null;
        }
        return res;
    }

}


class AbstractAuthorizationWindow {
    getLogin() {
    };

    getPassword() {
    };

    get2fa() {
    };

    setError() {
    };

    clearError() {
    };

    createLoginObject() {
    };

    createLoginButton() {
    }
}

class AuthorizationWindow extends AbstractAuthorizationWindow {
    #options;
    #login;
    #password;
    #twoFa;
    #savePasswordSwitch;
    #scope;

    getOptions() {
        return this.#options;
    }

    constructor(options, successLogin, failLogin) {
        super();
        this.#options = options;
        if (successLogin) {
            this.#options.setSuccessLogin(successLogin);
        }

        if (failLogin) {
            this.#options.setOnFailLogin(failLogin);
        }


        this.#login = h.input("text").cl("login_login").placeHolder("app.global.login.login_placeholder");
        this.#password = h.input("password").cl("login_password").placeHolder("app.global.login.login_placeholder");
        this.#twoFa = h.input("text").cl("two_fa_Code").placeHolder("app.global.login.two_fa_code_placeholder")
    }


    setScope(scope) {
        this.#scope = scope;
        return this;
    }

    build() {
        let container = h.div("login_container");
        this.createSavePasswordSwitch()
        this.createContent(container, this.#login, this.#password, this.#twoFa);
        this.createLoginButton().appendTo(container);
        return container;
    }

    createSavePasswordSwitch() {
        this.#savePasswordSwitch = new MagicTrueFalseSelector(new MagicTrueFalseSelectorOptions(_.$("global.app.login.save_password"), true));
        return this.#savePasswordSwitch;
    }

    getScope() {
        return _.ofNullable(this.#scope, () => Environment.authScope)
    }

    createLoginButton() {
        return h.input("button").cl("login_button").text("app.global.login.button").click(() => {
            this.clearError();
            this.createLoginObject().login(this.#options, this)
        })
    }


    createContent(container) {

        container.add(this.getLogin()).add(this.getPassword()).addIf(this.getOptions().isAllow2fa(), this.get2fa());
        if (this.getOptions().isAllowSavePassword()) {
            let savePassword = h.div("save_password");
            this.getSavePasswordSwitch().draw(savePassword);
            container.add(savePassword)
        }
    }

    createLoginObject() {
        sessionStorage.setItem(Authorization.REMEMBER, this.#savePasswordSwitch.getValue());
        cache.add(Authorization.REMEMBER, this.#savePasswordSwitch.getValue());
        return new LoginObject(this.getLogin().val(), this.getPassword().val(), this.get2fa()?.val(), this.getScope(),
            this.getAuthType());
    }

    getLogin() {
        return this.#login
    }

    getAuthType() {

    }

    get2fa() {
        return this.#twoFa;
    };

    getPassword() {
        return this.#password;
    }

    getSavePasswordSwitch() {
        return this.#savePasswordSwitch;
    }

    setError() {
        this.#login.cl("login_error");
        this.#password.cl("login_error")
        this.#twoFa?.cl("login_error")
    };

    clearError() {
        this.#login.rcl("login_error");
        this.#password.rcl("login_error")
        this.#twoFa?.rcl("login_error")
    }

}

