
class FormButton extends Button{
    constructor(name,  click = null, icon = null, isAction = false, buttonClass) {
        super(name,  click = null, icon = null, isAction = false)
        this.buttonClass = buttonClass
    }

    toH(){
        return h.input("button")
            .text(this.label, true)
            .cl( this.buttonClass)
            .clIf(this.isAction, "activ")
            .clickIfPresent(this.click);
     }

}


class MagicFormOptions {
    #parent;
    #formListener;
    #formFieldListener;
    #dataType;
    #fieldRenderer;
    #customButtons = [];
    #editTabs = true;
    #dragFields = true;
    #replaceFieldtoAnotherTab = true;
    #apiRoot;
    #defaultButtons = [MagicForm.CLOSE, MagicForm.CLOSE_AND_SAVE, MagicForm.SAVE];


    constructor(dataType, apiRoot = "/api/v2/", parent = null) {
        this.#dataType = dataType;
        this.#parent = parent;
        this.#apiRoot = apiRoot;
    }

    getRoot(afterPath = '') {
        return this.#apiRoot + this.#dataType + afterPath;
    }

    setIsEditTabs(isEditTabs){
        this.#editTabs = isEditTabs;
    }

    getIsEditTabs(){
        return this.#editTabs;
    }

    setIsDragFields(isDragFields){
        this.#dragFields = isDragFields;
        return this;
    }

    getIsDragFields(){
        return this.#dragFields;
    }

    setIsReplaceFieldtoAnotherTab(isReplaceFieldtoAnotherTab){
        this.#replaceFieldtoAnotherTab = isReplaceFieldtoAnotherTab;
    }

    getIsReplaceFieldtoAnotherTab(){
        return this.#replaceFieldtoAnotherTab;
    }

    setDrawField(drawField) {
        this.#fieldRenderer = drawField;
    }


    getButtons(){
        let buttons = [];
        if(dom.isVariableExists(Environment.formButtons) && !_e(Object.keys(Environment.formButtons).length,0)){
            for (let key in Environment.formButtons){
                if(_.isTrue(Environment.formButtons[key])){
                    buttons.push(key)
                }
            }
            return buttons;
        }else{
            return this.#defaultButtons
        }

    }

    getDataType(){
        return this.#dataType
    }

    geFormListener() {
        return this.#formListener;
    }

    setParent(parent){
      this.#parent = parent;
    }

    addCustomButtons(button){
       this.#customButtons.push(button)
    }

    getCustomButton(){
        return this.#customButtons
    }

    getFormFieldListener() {
        return this.#formFieldListener;
    }

    setFormListener(listener) {
        this.#formListener = listener;
        return this;
    }

    setFormFieldListener(formFieldListener) {
        this.#formFieldListener = formFieldListener;
        return this;
    }

    fire(event) {
        if (_.notNull(this.#formListener)) {
          return this.#formListener(event);
        }
    }

    fireField(event) {
        if (_.notNull(this.#formFieldListener)) {
            this.#formFieldListener(event);
        }
    }

    getFieldRenderer() {
        return this.#fieldRenderer;
    }

    getParent(){
      return this.#parent;
    }


    getFormListener(){
        return this.#formListener;
    }
    #path = Environment.dataApi ;

    setPath(path) {
        if (!this.#path.indexOf("http") == 0) {
            path = Environment.dataApi + this.#apiRoot + path;
        }
        this.#path = path;
        return path;
    }

    getServer(){
        return this.#path;
    }

    getPath(id) {

        let res = this.#path + this.#apiRoot + this.#dataType + "/root/fetch/form"
        if (id != null) {
            res += "?id=" + id;
        }

        return res;
    }


}


class MagicForm {
    static INSTANCES = [];
    static CLOSE = "close";
    static CLOSE_AND_SAVE = "close_and_save";
    static SAVE = "save";
    static ORDER = "order";
    static FIELD_DRAGGING = "field_dragging";
    static FIELD_OVER = "field_over";
    static CREATE_NEW_TAB = 'global.page.tabs.create_new_tab';
    static NEW_TAB_TITLE = 'global.page.tabs.new_tab_title';

    #executing;

    #container = dom.createModal(dom.REMOVE_TYPE_HIDE).cl("open-form").cl("form-window").appendToBody();

    #contentWrapper = h.div("form-content-wrapper").appendTo(this.#container);
    #magicMenuFields = [];
    #content =  h.div("form-content edit-person").appendTo(this.#contentWrapper);
    #options;
    #objectType;
    #id;
    #copy;
    #title = h.div("formTitle");
    #footer = h.div("footer");
    #fields = [];
    #fetchFormPath;

    #action = "";

    constructor(options) {
        this.#options = options;
        this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_INITIATED, this));
        this.#contentWrapper.add(this.#title).add(this.#content).add(this.#footer);
    }

    getFetchFormPath(){
        return this.#fetchFormPath;
    }

    setFetchFormPath(path){
        this.#fetchFormPath = path;
    }

    getWrapper(){
      return this.#contentWrapper
    }

    getId() {
        return this.#id;
    }

    setOptions(options){
        this.#options = options;
        return this;
    }
    getFields() {
        return this.#fields;
    }

    getAction() {
      return this.#action;
    }

    getOptions(){
      return this.#options;
    }

    getFormListener(){
        return this.#options.getFormListener()
    }

    getFormFieldListener(){
        return this.#options.getFormFieldListener();
    }
    static Instance(name, options) {

      let inst = _.find(this.INSTANCES, (instance) => {
        return utils.e(instance.name, name);
      })

      if (inst == null) {
          inst = {
            name: name,
            form: new MagicForm(options)
          }
          this.INSTANCES.push({name: name, form: inst.form});
      }
        return inst.form.setOptions(options)
    }

    build() {
      this.#fetchFormPath = this.#options.getPath(this.#id);
      this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_PRELOAD, this, this.#fields));
      let _this = this;
      Executor.runGet(this.#fetchFormPath, (function(res){
          _this.buildForm(res);
          _this.#objectType = res.objectType
          _this.show();
      }))
    }

    toastMessage(text, type, data = null){
      var additional = {
        type: type,
        data: data,
      }

      var allowToast = this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_TOAST_REQUESTED, this, additional));

      if(!_e(allowToast, false)) {
        dom.toast(text, type);
      }
    }

    getValue(fieldName){
        return _.find(this.#fields, (field)=> { return _.e(field.field, fieldName);   });
    }

    load(id = null, copy = false) {
        this.#id = id;
        this.build();
        this.#copy = copy;
        this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_LOADED, this));

        return this;
    }


    show() {
        this.#container.showModal();
        this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_SHOW, this));
    }

    hide() {
        this.#container.hideModal();
        this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_HIDE, this));
    }

    afterHide() {
      _.each(this.#fields, (f) => f.cancel())
    }


    buildForm(res) {

       this.#action = res.action;
        if(_.isTrue(this.#copy)){
            this.#action = this.#options.getRoot('/add')
        }
        //building title if required
        if (res.showTitle) {
            if (_.notNull(this.#id) && _.isFalse(this.#copy)) {
                res.title += "_edit";
            } else {
                res.title += "_add"
            }
            this.#title.text(res.title)
        }

        //building fields and footer if they are not yet build
        if (_.isEmpty(this.#fields)) {
            this.buildFields(res);
            this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_GENERATED_FIELDS, this, this.#fields));

            this.drawFields(res);
            this.generateFooter(res);
            this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_FOOTER_GENERATED, this, this.#footer));
        }

        if (this.#id == null) {
            _.each(this.#fields, (field) => {
                if (!field.isPreselected()) {
                    field.setValue(null)
                }
            });
        } else {
            _.each(this.#fields, (field) => {
              field.setValue(res.value);
            });
        }

        _.each(this.#fields, (field) => {
          field.afterAdd()
        })

    }

    buildFields(data) {
        _.each(data.fieldList, (field) => {
            this.#fields.push(new MagicField(field, this.#options).init(this));
        });
    }

    drawFields(data) {
        let groups = [];
        let tabTranslate = {};
        let defaultFieldNames = []
        //collect groups in array
        _.each(data.fieldList, (field) => {
            if (groups.indexOf(field.customName) === -1) {
                groups.push(field.customName)
            }
            if (defaultFieldNames.indexOf(field.group) === -1) {
                defaultFieldNames.push(field.group)
            }
        });


        _.each(data.fieldList, (field) => {
          if(field.customized) {
            tabTranslate[field.customName] = false;
          } else {
            if(_e(field.customName, field.group)){
              tabTranslate[field.customName] = true;
          } else{
              if(defaultFieldNames.includes(field.customName)){
                  tabTranslate[field.customName] = true;
              }else{
                  tabTranslate[field.customName] = false;
              }
            }
          }
        });

        let components = groups.map(e => h.div('tabs_component_div').setData('group', e));
        let fieldsArr = [];

        for (let i = 0; i < this.#fields.length; i++) {
            let compGroup = this.#fields[i].getData().customName;

            components.forEach((e) => {

                if (e.getData('group') === compGroup || _.isNull(compGroup)) {

                    let el = this.#fields[i].draw(data.value).appendTo(e)
                    fieldsArr.push(el)
                    this.#options.fire(new MagicFormEvent(MagicFormEvent.FIELD_GENERATED, this,  {
                        'field' : this.#fields[i],
                        'el' : el,
                        'val' : data.value
                    }, this.#fields[i].getField()))
                }
            })
        }

        if(_.isTrue(this.#options.getIsDragFields())){
            this.drugField(fieldsArr);
        }

        this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_FIELDS_DRAGABLE, this, fieldsArr));

        this.fieldTabOptions = new MagicTabOptions();
        this.fieldTabOptions.setTablistener(this.tabListener);
        this.fieldTabOptions.setParent(this);
        this.fieldTabOptions.setTabTranslater(tabTranslate);
        this.fieldTab = new MagicTab(this.fieldTabOptions);

        for (let k = 0; k < groups.length; k++) {
            this.fieldTab.addTab(groups[k], components[k])
            this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_TAB_ADD, this,  components[k]));
        }
        this.fieldTab.deployTo(this.#content)
        this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_TABS_GENERATED, this,  this.fieldTab));
        this.additionalTabs = groups.length

        if(_.isTrue(this.#options.getIsReplaceFieldtoAnotherTab())){
            this.contextMenuCreate();
        }
        this.setFieldOrder();
    }

    tabListener(event){
        if(_e(event.eventType, MagicTabEvent.EVENT_TAB_TITLE_GENERATED)&& _.isTrue(event.parent.#options.getIsEditTabs())){
            let text = event.additional.get().innerText;
            event.additional.text(null);
            let titleDiv = h.div("title_div").text(text,false);
            let titleInpun = h.input(text).hide();
            let image = h.div('pencil-tab').click(()=>{
                if(titleInpun.ccl("hidden")){
                    image.cl('active')
                    titleDiv.hide();
                    titleInpun.text(titleDiv.val(),false).show();
                }else{
                    if(titleInpun.val().length>=1){
                        image.rcl('active')
                        titleInpun.hide();
                        titleDiv.text(titleInpun.val(),false).show();
                        event.parent.rebuildContextMenuFields(event.tab, titleInpun.val());
                        event.tab.name = titleInpun.val();
                        event.parent.setFieldChanges();
                    }
                }
            })
            titleInpun.handleEvent('keydown', (e)=>{
                if(_e(e.key, "Enter")){
                    image.get().click();
                }
            }).attr('maxlength', 15)

            event.additional.add(titleDiv).add(titleInpun).add(image);
        };
    }

    //after change tab name, rebuild name of context fields if have same actions
    rebuildContextMenuFields(tab, newName){
        _.each(this.#magicMenuFields, (mmfield)=>{
            if(mmfield.getActions){
                let contextFields = mmfield.getActions();
                contextFields.map(elem =>{
                    if (_e(elem.action, tab.name)){
                        elem.name = newName;
                        elem.action = newName;
                        return elem
                    }
                    return elem
                })
                mmfield.rebuildActions(contextFields);
            }
        });
    }

    //create contex menu for evry field
    contextMenuCreate(){
        let headers = this.fieldTab.getHeaders();
        this.contextMenuFields = [];

        _.each(headers, (comp)=>{
          let name = comp.name.split('.').slice(-1)[0];

          this.contextMenuFields.push({ name: name, allowOnMultiSelect: false, action: comp.name});
        });
        this.contextMenuFields.push({ name: _.$(MagicForm.CREATE_NEW_TAB), allowOnMultiSelect: false, action: _.$(MagicForm.CREATE_NEW_TAB)});

        this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_CONTEXT_MENU_FIELDS_GENERATED, this, this.contextMenuFields));
        let components = this.fieldTab.getComponents();
        this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_CONTEXT_MENU_START_CREATED, this, components));
        _.each(components, (comp)=>{
           let rebuildcontextMFields = this.contextMenuFields.filter((e) => { return !_e(e.action, comp.name)})
          _.each(comp.component.childNodes(), (div, i)=>{
                let options = new MagicMenuOptions(h.from(div), {id: comp.name}, components, "row__wrapper").setActions([...rebuildcontextMFields]);
                options.setListner(this.contextMenuListener);
                options.setTranslatePath(_.$('global.forms.actions.move.to'));
                this.#magicMenuFields.push(new MagicMenu(options, div));
          })
        })
        this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_CONTEXT_MENU_FINISH_CREATED, this, components));
    }


    rebuilItemdAction(mmElement, item){
        let rebuildcontextMFields = this.contextMenuFields.filter((e) => { return !_e(e.action, item.action)});
        mmElement.rebuildActions(rebuildcontextMFields);
    }


    contextMenuListener = (ev) =>{

        if(_e(_.$(MagicForm.CREATE_NEW_TAB), ev.item.action)){
           this.addNewTab(ev);

        }else{
            _.each(ev.parent, (e)=>{
                if(_e(e.name, ev.item.action)) {
                    h.from(ev.data).appendTo(e.component);
                    this.rebuilItemdAction(ev.element, ev.item);
                }
            })
        }

        this.setFieldOrder();
        this.setFieldChanges();
    }


    // add new tab and replace field to this tabCopmonent
    addNewTab(ev){
        let newAction = {name: _.$(MagicForm.NEW_TAB_TITLE)  + this.additionalTabs, allowOnMultiSelect: false, action: _.$(MagicForm.NEW_TAB_TITLE) + this.additionalTabs}
        let createdComponent = h.div('tabs_component_div').setData('group', _.$(MagicForm.CREATE_NEW_TAB) + this.additionalTabs)
        h.from(ev.data).appendTo(createdComponent);
        // add tabTranslater befor deploy tabComponent
        let tabTranslater = this.fieldTabOptions.getTabTranslater();
        tabTranslater[_.$(MagicForm.NEW_TAB_TITLE) + this.additionalTabs] = false;
        this.fieldTabOptions.setTabTranslater(tabTranslater);
        this.fieldTab.addTabAfterDeploy(_.$(MagicForm.NEW_TAB_TITLE) + this.additionalTabs, createdComponent);

        this.contextMenuFields.push(newAction)
        this.rebuilItemdAction(ev.element, newAction);

       _.each(this.#magicMenuFields, (menu)=>{
            if(menu!=ev.element){
                //replase CREATE_NEW_TAB to the end of the list
                menu.addAction(newAction);
                menu.removeActionByName(_.$(MagicForm.CREATE_NEW_TAB))
                menu.addAction({name: _.$(MagicForm.CREATE_NEW_TAB), allowOnMultiSelect: false, action: _.$(MagicForm.CREATE_NEW_TAB)});
            }

        })
       this.additionalTabs ++

    }

    setFieldChanges(){
        let path = Environment.dataApi + '/api/v2/shared/form_setting/add';
        let payload = this.fetchFields();
        Executor.runPostWithPayload(path, ()=>{
            dom.successMessage()
        },payload);
    }

    fetchFields(){
        let fields = [];
        _.each(this.fieldTab.getComponents(), (component, i) => {
            component.component.eachOf((child)=>{
                fields.push({
                    "fieldName": child.getData('field'),
                    "position": parseInt(child.getData(MagicForm.ORDER)),
                    "tabName": component.name
                })
            }, null, true)
        });
        return {
             entityType: this.#objectType,
             positions: fields
             }
    }


    createFooterButton(buttonClass, text, isActive, action){
        let button = h.input("button")
            .text(text)
            .cl(buttonClass)
            .clIf(isActive, 'activ')
            .click(()=>{
               action()
            })
        return button
    }

    setFieldOrder(){
        let num = 0;
        _.each(this.fieldTab.getComponents(), (component, i) => {
            component.component.eachOf((child)=>{
                child.setData(MagicForm.ORDER, num);
                ++num;
            }, null, true)
        });
    }


    drugField(elements){
        let clicked;
        _.each(elements, (el)=>{
            let img = h.img('sixVertical.svg')
            if(!h.from(el).first().ccl("hidden")){h.img('sixVertical.svg').appendTo(h.from(el))}
            h.from(el).first().first().draggable((e)=>{
                h.from(e.currentTarget.parentElement.parentElement).cl(MagicForm.FIELD_DRAGGING);
                clicked = e.currentTarget.parentElement.parentElement;
                });
            h.from(el).dragOver((e)=>{
              _.each(elements, (e)=>{h.from(e).rcl(MagicForm.FIELD_OVER)});
                h.from(e.currentTarget).cl(MagicForm.FIELD_OVER);
                e.preventDefault();
            })
            h.from(el).dragDrop((e)=>{
                _.each(elements, (e)=>{h.from(e).rcl(MagicForm.FIELD_OVER)});
                _.each(elements, (e)=>{h.from(e).rcl(MagicForm.FIELD_DRAGGING)});
                if(clicked){
                    let clickedOrder = h.from(clicked).getData(MagicForm.ORDER);
                    let currOrder = h.from(e.currentTarget).getData(MagicForm.ORDER);
                    if(_e(clickedOrder, currOrder)){
                        return
                    }
                    clickedOrder>currOrder
                        ? e.currentTarget.before(clicked)
                        : e.currentTarget.after(clicked);
                    this.setFieldOrder();
                    this.setFieldChanges();
                    clicked = null;
                }

            })
        })
    }

    generateFooter(data) {
        let _this = this;
        h.divImg("close.png.png")
            .wh(24)
            .cl("close_inner_form").pointer()
            .appendTo(this.#contentWrapper)
            .click(function (e) {
                _this.hide();
                _this.afterHide();
            })
            .appendTo(this.#contentWrapper);
        _.each(this.#options.getButtons(),(button)=>{
            switch (button) {
                case MagicForm.SAVE:
                    this.#footer.add(this.createFooterButton("save_auto_form", data.commitText, null,  ()=>{_this.execute(false)}))
                    break;
                case MagicForm.CLOSE_AND_SAVE:
                    this.#footer.add(this.createFooterButton("close_and_save_auto_form", "global.pages.form.close_and_save", null, ()=>{_this.execute(true)}));
                    break;
                case MagicForm.CLOSE:
                    this.#footer.add(this.createFooterButton("close_auto_form", "global.pages.form.close", null, ()=>{_this.hide(); _this.afterHide()}));
                    break;
            }
        })
        _.each(this.#options.getCustomButton(),(button)=>{
            this.#footer.add(button.toH());
            }
        )
    }

    //when everything is done and save action is called
    execute(toHide) {
        if (this.#executing) {
          this.toastMessage(_.$("global.app.already.in_run"), ToastType.ERROR);
            return;
        }
        this.#executing = true;
        let payload = {additionalValues: []};
        _.each(this.#fields, (field) => {
            try {
                field.clearErrors();
            } catch (e) {
                log.debug("Error: cannot clean errors on the field" + field)
            }
            if(!field.getData().customized) {
              payload[field.getField()] = field.read();
            } else {
              payload.additionalValues.push({
                key: field.getField(),
                value: field.read() + ''
              })
            }
          });
        let _this = this;
        if(this.#copy){
            this.#id = null;
            payload.id = null
        }
        let method = this.#id == null ? Executor.runPostWithPayload : Executor.runPutWithPayload
        method(this.#options.getServer() + this.#action,
            (data) => {
                _this.#executing = false;
                try {
                    _this.#options.getParent().refresh();
                } catch (e) {
                    log.info(e);
                }
                this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_SAVED, this, data));
                var success = 'ID ' + data.id +', ' + _.$("global.object.saved") + ", " + _.$("global.thanks");
                this.toastMessage(success, ToastType.SUCCESS, data);

                try {
                    _this.#options.getParent().drawValues();
                } catch (e){
                    log.error(e);
                }

                if (toHide) {
                  _this.hide();
              } else {
                  _this.load(data.id)
                }
            },
            payload,
            (e) => {
                _this.#executing = false;
                verifyResponse(e);
                let data = e;
                this.#options.fire(new MagicFormEvent(MagicFormEvent.FORM_ERROR_SAVING, this, e));
                if (data != null) {
                    utils.each(data, (item) => {
                        try {
                            h.fromId("error_" + item.key).text(item.value).show();
                        }  catch (e){
                            log.error(e);
                        }
                      dom.toast(_.$(item.value), ToastType.ERROR);
                    })
                } else {

                    _.each(e, (field) => {
                      dom.toast(_.$("global.fail." + field.key), ToastType.ERROR);
                    })
                  }


            })
    }


    findField(name){
        return _.each(this.getFields(), (field)=> {
            if (_.e(field.getField(), name)){
                return field;
            }
        })
    }

}

class MagicField {
    #drawType;
    #drawClass;
    #data;

    #fieldObject;

    #field;
    #type;

    #form;
    #visible;
    #renderer;

    #options;

    #preselected;

    setPreselected(){
        this.#preselected = true;
    }

    getDrawClass(){
      return this.#drawClass;
    }

    getFieldObject(){
        return this.#fieldObject;
    }
    isPreselected(){
        return _.isTrue(this.#preselected)
    }

    constructor(_field, options) {
        this.#type = _field.type;
        this.#field = _field.field;
        this.#drawType = options.renderer;
        this.#options = options;


        this.#data = _field;
    }

    init(parent) {

        let customRenderer;
        try {
            customRenderer = this.#renderer.getRenderer(this);
        } catch (E){customRenderer = null}
        this.#drawClass = _.isNull(customRenderer) ?   FieldResolver.getDrawElement(this, parent) : customRenderer;
        return this;
    }

    cancel(){
      this.#drawClass.cancel();
      try {
          h.fromId("error_" + this.#field).hide();
      } catch (e){
          log.trace("cannot hid error_message");
      }
    }

    getData() {
        return this.#data;
    }

    getOptions(){
      return this.#options;
    }

    getType() {
        return this.#type;
    }

    setType(type){
      this.#type = type;
    }

    getField() {
        return this.#field;
    }

    read() {
        return this.#drawClass.read();
    }

    clearErrors() {
        this.#drawClass.clearErrors();
    }

    setValue(value) {
        if(this.#data.customized && value) {
          let fieldData = value.additionalValues.find(e => _e(e.key, this.#field));
          this.#drawClass.setValue(fieldData, true);
        } else {
          this.#drawClass.setValue(value);
        }
    }

    draw(value) {
        if (this.#drawClass == null) {
            dom.toast(
                "Field cannot be created: " + this.getField() + "#" + this.getType(), ToastType.ERROR
            );
        }

        if (_.notNull( this.#renderer )) {
            let res = this.#drawType(this, value);

            if (res != null) {
                return res;
            }
        }

        try {
            let res = this.#drawClass.create(value);

            if (res == null) {
                res = this.#drawType(this);
            }

            this.#fieldObject = res;
            this.#drawClass.fireEvent(FieldResolver.EVENT_CONTAINER_CREATED, this)
            if (res != null) {
                return res;
            }
        } catch (e) {
            console.error(e)
            return h.span("span").text("ERROR " + this.#field, false);
        }

        return h.span("span").text("ERROR " + this.#field, false);
    }

    afterAdd(res) {
      this.#drawClass.afterAdd(res);
    }

    setOrder(order){
        return this.#drawClass

    }
}
