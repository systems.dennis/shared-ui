class MagicRequest {
    page = 0;
    limit = 20;

    sort = [{"field": "id", "desc": true}]

    query = [];

    static ofSingle(query, sort){
        let res =  new MagicRequest();
        res.limit = 1;
        if (_.notNull(query)){
        this.query.push(query);
        }
        res.sort = sort;
        return res;
    }

    _equals(b){
        if (_.isNull(b)){
            return false;
        }

        if (! (b instanceof MagicRequest)){
            b =  MagicRequest.fromObjectArray(b)
        }
        return _e(this.asString(), b.asString())
    }

    asString(){
        return this.page + this.limit + JSON.stringify(this.sort) + JSON.stringify(this.query)
    }


    static fromObjectArray(b){
        if (_.isNull(b)){
            return new MagicRequest()
        }

        let res = new MagicRequest();

        res.limit = b["limit"];
        res.page = b["page"];
        res.sort = b["sort"];
        res.query = b["query"];
        return res
    }
}

/**
 * General class, to provide requests pyaload with builder, instead of writing payload objects manually
 *
 *
 * Initializing with construstor will create empty objects to which you can dynamically call .and method
 *
 * Builder pattern all setters return object itself
 *
 * to add sort use Sort object ex: Sort.by(field, true).by(field2, false)
 *
 */
class Query {
    #additionalQueries = []
    static QUERY_FIELD_TYPE = "text";
    #type = "=";
    #fieldType = Query.QUERY_FIELD_TYPE
    #value
    #field;
    #searchName = null;
    #sort;
    #isEmpty = false;

    copy(value) {
        return  new Query().type(this.#type)
            .fieldType(this.#fieldType).value(value).field(this.#field).searchName(this.#searchName);

    }

    /**
     * This is only used for to create empty query
     */
    constructor() {
        this.#isEmpty = true;
    }

    fieldType(type) {
        this.#fieldType = type;
        return this;
    }


    type(type) {
        this.#type = type;
        return this;
    }

    sort(sort) {
        this.#sort = sort;
        return this;
    }

    and(query) {
        this.#additionalQueries.push(query);
        return this;
    }

    andIf(fun, query) {
        if (fun) {
            this.#additionalQueries.push(query)
        }
        return this;
    }


    static eq(field = undefined) {
        return new Query().type("=").field(field)

    }

    static greater(field = undefined) {
        return new Query().type(">").field(field);
    }

    static greaterEquals(field = undefined) {
        return  new Query().type(">=").field(field)
    }

    static less(field = undefined) {
        return  new Query().type("<").field(field)

    }

    static lessEquals(field = undefined) {
        return  new Query().type("<=").field(field)
    }

    between(left, right) {
        //todo rewrite this
        this.value(left).lessEquals().and(this.copy(right).greaterEquals());
        let rightExp = this.copy(right).greaterEquals();
        this.and(rightExp)
        return this;
    }

    static notEq(field = undefined) {
        return new Query().type("!=").field(field)
    }

    static in(field = undefined) {

        return new Query().type("in").field(field)

    }

    static contains(field = undefined) {
        return new Query().type("%...%").field(field)
    }

    static starts(field = undefined) {
        return new Query().type("%...").field(field)
    }

    static ends(field = undefined) {
        return new Query().type("...%").field(field)
    }

    static notStarts(field = undefined) {
        return new Query().type("!%...").field(field)
    }

    static notEnds(field = undefined) {
        return new Query().type("!...%").field(field)
    }

    static notNull(field = undefined) {
        return new Query().field(field).type("not_null")

    }

    static isNull(field = undefined) {
        return new Query().field(field).type("is_null")

    }


    referencedId() {
        this.#fieldType = "referenced_id";
        return this;
    }

    referencedIds() {
        this.#fieldType = "referenced_ids";
        return this;
    }

    date() {
        this.#fieldType = "date";
        return this;
    }

    int() {
        this.#fieldType = "integer"
        return this;
    }

    chooser() {
        this.#fieldType = "object_chooser";
        return this;
    }

    dictionary() {
        this.#fieldType = "dictionary"
        return this;
    }

    double() {
        this.#fieldType = "double"
        return this;
    }

    number() {
        this.#fieldType = "number"
        return this;
    }

    emptyFieldType() {
        this.#fieldType = "text"
        return this;
    }


    searchName(searchName) {
        this.#searchName = searchName;
        return this;
    }

    getSort() {
        this.#isEmpty = false;
        this.#sort = _.ofNullable(this.#sort, () => new Sort());
        return this.#sort;
    }


    field(field) {
        this.#field = field;
        this.#isEmpty = false;
        return this;
    }

    value(value) {
        this.#value = value
        return this;
    }


    toQueryObject() {
        this.validate()

        if (_.isNull(this.#field)) {
            return null;
        }

        return  {
            "field": this.#field,
            "dataType": this.#fieldType,
            "value": this.#value,
            "type": this.#type,
            "searchName": this.#searchName
        }  ;


    }

    toSting() {
        return this.#field + " (val) -> " + this.#value + " (type) -> " + this.#type + " (field type)->" + this.#fieldType
    }

    validate() {
        if (_.isNull(this.#field)) {
            log.promisedDebug(() => {
                return _.ob("position", "validation.field.not_set").put("query", this.toSting()).get();

            });
        }

        if (_.isNull(this.value)) {
            log.promisedDebug(() => {
                return _.ob("position", "validation.value.not_set").put("query", this.toSting()).get();

            });
        }
    }

    merge(query) {
        this.#additionalQueries.push(...query.getAdditionalQueries());
    }

    isEmpty() {
       return  this.#isEmpty && _.isEmpty(this.#additionalQueries);
    }

    getAdditionalQueries() {
        return this.#additionalQueries;
    }

    create() {

        let res = [];

        this.pushToResultIfNonEmpty(res, this);

        _.each(this.#additionalQueries, (query) => {this.pushToResultIfNonEmpty(res, query);});

        return res;
    }

    pushToResultIfNonEmpty(res, query) {
        let queryValue = query.toQueryObject();
        queryValue && res.push(queryValue);
    }


    toPayload(page = 0, limit = 10) {

        if (this.isEmpty()) {
            let request = new MagicRequest();
            request.limit = limit;
            request.page = page;

            request.sort = _.ofNullable(this.#sort, () => new Sort()).get();
            return  request;
        }
        let payload = new MagicRequest();
        payload.query = this.create();
        payload.limit = limit;
        payload.page = page;
        payload.sort = _.ofNullable(this.#sort, () => new Sort()).get();

        return payload;
    }


}

class Sort {
    #fields = [];
    static  ID_FIELD = "id";


    by(field, desc = true) {
        this.#fields.push({"field": field, "desc": desc})
        return this;
    }

    merge(sort) {
        _.each( sort instanceof Array ? sort: sort?.get(), (sortItem) => {
            this.by(sortItem.field, sortItem.desc);
        });
    }

    byId() {
        return this.by(Sort.ID_FIELD)
    }

    get() {
        return this.#fields;
    }
}
