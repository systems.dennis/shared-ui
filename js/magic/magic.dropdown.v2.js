/**
 *
 *
 * GENERAL:
 *
 * AbstractDataItem - an element to be displayed
 * AbstractSelectedValueStrategy - a strategy to select preselected value
 * DataProvider - a provider to retrieve data, that can be used as options
 */


/**
 * When cbo values are not yet loaded (normally by click)
 * We need to setup a value that will be loaded, even in this case
 */
class AbstractSelectedValueStrategy {
    #provider

    setProvider(provider) {
        this.#provider = provider;
        return this;
    }

    getPreselectedItem(provider) {
    }

    getProvider() {
        return this.#provider;
    }

    //what should be presented if value is null

    getItemOnNull(provider) {

    }
}

class FirstItemOrNullItemStrategy extends AbstractSelectedValueStrategy {

    getPreselectedItem() {
        return _.ofNullable(this.getProvider().filterFirst(), () => {
            return this.getItemOnNull()
        });
    }

    getItemOnNull() {
        return new DataProviderItem(null, null, {}).setNoValueItem()
    }
}

class NoValuePreselectedStrategy {
    getPreselectedItem() {
        return null;
    }
}


class PreselectedValueStrategy extends AbstractSelectedValueStrategy {
    #value;

    setValue(value) {
        this.#value = value;
        return this;
    }

    getPreselectedItem() {
        return _.ofNullable(this.getProvider().createItem(this.#value), () => {
            return this.getItemOnNull()
        });
    }

    getItemOnNull() {
        return new DataProviderItem(null, null, {}).setNoValueItem()
    }
}

class PreselectedValueOrEmptyStrategy extends PreselectedValueStrategy {
    getItemOnNull() {
        return this.getProvider().getEmptyValue()?.setNoValueItem();
    }
}

class DataProviderItem {
    key;
    value;
    origin;
    noValueItem = false;

    constructor(key, value, origin) {
        this.key = key;
        this.value = value;
        this.origin = origin;
    }

    setNoValueItem() {
        this.noValueItem = true;
        return this;
    }

    isNoValueItem() {
        return this.noValueItem;
    }

    _equals(el) {
        if (_.isNull(el)) {
            return false;
        }
        return _e(el.key, this.key);
    }
}

class AbstractDataProvider {

    #emptyValue = new DataProviderItem("NONE", null, {})

    getData(listener) {
    }

    search(text, listener) {
    }

    byValue(value) {

    }

    setEmptyValue(value) {
        this.#emptyValue = value;
        return this;
    }

    getEmptyValue() {
        return this.#emptyValue
    }

    filterFirst() {

    }

    createItem(item) {
        if (item instanceof DataProviderItem) {
            return item;
        }

    }
}

/**
 * Provides data for the combobox
 */
class LocalDataProvider extends AbstractDataProvider {
    #items = [];

    static of(items) {
        let provider = new LocalDataProvider();
        _.each(items, (item) => {
            provider.validateEntry(item);
            provider.addItem(item.key, item.value, item.empty)
        })
        return provider;
    }

    constructor(items = []) {
        super();
        items = _.ofNullable(items, () => []);
        _.each(items, (item) => {
            this.validateEntry(item)
            this.addItem(item.key, item.value, item.empty);
        })
    }

    /**
     * @deprecated
     * @returns {*}
     */
    getNoValueItem() {
        return this.getEmptyValue();
    }

    /**
     * @deprecated, use setEmptyValue method instead
     * @param item
     * @returns {LocalDataProvider}
     */
    setNoValueItem(item) {
        return this.setEmptyValue(item)
    }

    validateEntry(item) {
        if (_.isNull(item.key) || _.isNull(item.value)) {
            throw new Error('Item should have key/value structure')
        }
    }

    addItem(key, value, empty = false) {
        const providerItem = new DataProviderItem(key, value)
        if(empty){
            providerItem.setNoValueItem();
        }
        this.#items.push(providerItem);
    }


    getData(listener) {
        return listener(this.#items);
    }

    getItems() {
        return this.#items
    }

    search(text, listener) {
        return listener(this.#items.filter((item, value) => {

            return item.key.indexOf(text) > -1
        }))
    }

    byValue(value) {
        return _.find(this.#items, (item) => {
            return _e(item.value, value)
        })
    }

    filterFirst() {
        return this.#items[0]
    }
}

/**
 * Remote data provider executes request t get data, instead of storing data locally
 */
class RemoteDataProvider extends AbstractDataProvider {
    #request;
    #keyField;
    #keyValue;
    #payload;
    #searchType = 'contains'
    #loadedData = [];
    static #ID_FIELD = "id"
    #dataContentFetcher;
    #maxElements = null;

    setMaxElements(maxElements) {
        this.#maxElements = maxElements;
        return this;
    }

    getMaxElements() {
        return _.ofNullable(this.#maxElements, new MagicRequest().limit);
    }

    /**
     * Request should be of type ServerRequest
     * @param request - a server request to query data
     * @param keyField - a field where to take a label
     * @param magicRequest - request to filter or limit data
     * @param keyValue - a field where to take value, normaly ID
     * @param dataContentFetcher when data is recieved sometimes the true data is located in special field, for example in case of pagination, the data is located in content directory
     */
    constructor(request, keyField, magicRequest = new MagicRequest(), keyValue = RemoteDataProvider.#ID_FIELD, dataContentFetcher = RemoteDataProvider.defaultDataContentFetcher) {
        if (!request instanceof ServerRequest) {
            log.promisedDebug(() => {
                return {"text": "request should be of type ServerRequest", "present": request}
            })
            return;
        }
        super();
        this.#request = request.copy().async(false);
        this.#keyField = keyField;
        this.#keyValue = keyValue;
        this.#payload = magicRequest;
        this.#dataContentFetcher = dataContentFetcher;

    }

    getDataContentFetcher(){
        return this.#dataContentFetcher;
    }

    getLoadedData(){
        return this.#loadedData
    }

    static defaultDataContentFetcher(data) {
        return data.content;
    }

    getData(listener) {
        //todo to remove only required case

        this.#payload.query = [];
        this.#payload.limit = this.getMaxElements();

        if (this.getEmptyValue()) {
            this.#loadedData.addIfAbsent(this.getEmptyValue())
        }

        this.#request.search(this.#payload, (data) => {
            _.each(this.#dataContentFetcher(data), (item) => {
                this.#loadedData.addIfAbsent(new DataProviderItem(item[this.#keyField], item[this.#keyValue], item))
            });
            listener(this.#loadedData);
        })
    }

    getItems() {
        return this.#loadedData
    }


    /**
     * @deprecated, use getEmptyValue method instead
     * @returns {null}
     */
    getNoValueItem() {
        return this.getEmptyValue()
    }

    /**
     * @deprecated use setEmptyValue instead
     * @param item
     * @returns {RemoteDataProvider}
     */
    setNoValueItem(item) {

        return this.setEmptyValue(item)
    }

    setSearchType(value) {
        this.#searchType = value
        return this
    }

    getSearchType(){
        return this.#searchType
    }

    createItem(item) {
        if (_.isNull(item)) {
            return item;
        }

        if (item instanceof DataProviderItem){
            this.#loadedData.addIfAbsent(item)
            return  item;
        }
        let foundItem = super.createItem(item);
        let res;
        if (!foundItem) {
            let id = item;
            //get by id (try at least)
            if (_.isObject(item)) {
                id = item.id || item.value;
            }
try {
    this.#request.fullCopy().async(false).byId(id, (data) => {
        res = new DataProviderItem(data[this.#keyField], data[this.#keyValue], data)
    }, () => {
        res={}
    })
} catch (error){
    res={}
}

        }
        this.#loadedData.addIfAbsent(res)
        return res;

    }

    createSearchPayload(searchType, text, limit) {
        const payload = {query: [], limit: null};

        const payloadBySearchType = {
            contains: () => payload.query.push(Query.contains(this.#keyField).value(text).toQueryObject()),
            starts: () => payload.query.push(Query.starts(this.#keyField).value(text).toQueryObject())
        }

        try{
            payloadBySearchType[searchType]()
        }
        catch {
            log.promisedDebug(() => ({message: 'Invalid search type', availableTypes: Object.keys(payloadBySearchType)}))
        }

        payload.limit = _.ofNullable(limit, this.getMaxElements());

        return payload
    }

    //Search in this case is not only fetching data, but storing loaded data to easy access on "byKey" function
    search(text, listener, limit = null, searchType = this.#searchType) {
        const payload = this.createSearchPayload(searchType, text, limit)

        let toReturn = [];
        this.#request.fullCopy().forceNoCache().search(payload, (res) => {
            if (_.notNull(this.getEmptyValue())) {
                toReturn.push(this.getEmptyValue());
            }
            _.each(this.#dataContentFetcher(res), (item) => {
                const transformedData = this.toProviderItem(item);
                this.#loadedData.addIfAbsent(transformedData)
                toReturn.push(transformedData)
            })
            listener(toReturn);
        })
    }

    toProviderItem(item) {
        return new DataProviderItem(item[this.#keyField], item[this.#keyValue], item);
    }

    byValue(value) {
        let res =  _.find(this.#loadedData, (item) => {
            return _e(item.value, value)
        })
        if(_.isNull(res)){
            res= this.createItem(value)
        }
        return res;
    }

    filterFirst() {
        let searchRequest = structuredClone(this.#payload);

        searchRequest.query.push(Query.contains(this.#keyField).value(text).toQueryObject())

        searchRequest.limit = 1;

        let toReturn = null;
        this.#request.fullCopy().async(false).run((data) => {
            toReturn = this.toProviderItem(this.#dataContentFetcher(data)[0]);
        })

    }
}


class MagicDropDownOptions {

    static SELECT_FIRST = "___first"
    static SELECT_NONE = null;
    #listeners = [];
    #itemRenderer;
    #tagRenderer;
    #isMultiSelect = false;
    #isSearchEnabled = true;
    #isDeleteTagButtonEnabled = true
    #title = "";
    #preselectedStrategy;
    #isRenderingAfterSearch = false
    #isDebounceEnabled = true;
    #debounceValue = 300
    #isSearchUpdateOnSelect = false
    #isCreateItemFromInputEnabled = false
    #isFooterEnabled = false;
    #footerRenderer = null

    #provider;

    /**
     * Data provider allows to fetch data according to the params
     * Data provider should extend AbstractDataProvider
     * @param dataProvider
     */
    constructor(dataProvider = new LocalDataProvider()) {
        this.#provider = dataProvider;
        this.#preselectedStrategy = new NoValuePreselectedStrategy();
    }

    /**
     * Manipulates data. add or remove data you can here
     * @returns {*}
     */

    getIsCreateItemFromInputEnabled() {
        return this.#isCreateItemFromInputEnabled
    }

    setIsCreateItemFromInputEnabled(value){
        this.#isCreateItemFromInputEnabled = value
        return this
    }

    getIsDeleteTagButtonEnabled() {
        return this.#isDeleteTagButtonEnabled
    }

    setIsDeleteTagButtonEnabled(value) {
        this.#isDeleteTagButtonEnabled = value;
        return this
    }

    setIsRenderingAfterSearch(value) {
        this.#isRenderingAfterSearch = value
        return this
    }

    getIsRenderingAfterSearch() {
        return this.#isRenderingAfterSearch
    }

    getIsSearchUpdateOnSelect() {
        return this.#isSearchUpdateOnSelect
    }

    setIsSearchUpdateOnSelect(value) {
        this.#isSearchUpdateOnSelect = value;
        return this
    }

    setIsDebounceEnabled(value) {
        this.#isDebounceEnabled = value
        return this
    }

    getIsDebounceEnabled() {
        return this.#isDebounceEnabled
    }

    getDebounceValue() {
        return this.#debounceValue
    }

    setDebounceValue(value) {
        this.#debounceValue = value;
        return this
    }

    getTitle() {
        if (this.#title) {
            return this.#title
        }
    }

    setTitle(title) {
        this.#title = title;
        return this;
    }


    getDataProvider() {
        return this.#provider;
    }

    addListener(listener) {
        if (_.notNull(listener)) {
            this.#listeners.push(listener);
        }
        return this
    }

    /**
     * @deprecated will produce errors since v2. left for a smooth integration, will be deleted in v3
     * @returns {*}
     */

    getPreSelectedValue() {
        return this.#preselectedStrategy.getPreselectedItem();
    }

    getPreselectedStrategy() {
        return this.#preselectedStrategy
    }

    setTagRenderer(renderer) {
        this.#tagRenderer = renderer;
        return this;
    }

    getTagRenderer() {
        return this.#tagRenderer;
    }


    /**
     * @deprecated since version 2, doesn't do any action
     * Use setPreselectedValueStrategy instead
     * @param id
     * @returns {MagicDropDownOptions}
     */
    setPreSelectedValue(id) {
        this.#preselectedStrategy = new PreselectedValueStrategy().setValue(id).setProvider(this.getDataProvider())
        return this
    }

    setPreselectedStrategy(value){
        if (! (value instanceof AbstractSelectedValueStrategy)){
            throw new Error('strategy should be extended from AbstractSelectedValueStrategy')
        }
        this.#preselectedStrategy = value.setProvider(this.getDataProvider());
        return this
    }


    enableMultiSelect() {
        this.#isMultiSelect = true;
        return this
    }


    isMultiSelect() {
        return this.#isMultiSelect;
    }

    setItemRenderer(renderer) {
        this.#itemRenderer = renderer;
        return this;
    }

    getItemRenderer() {
        return this.#itemRenderer;
    }

    setSearchDisabled() {
        this.#isSearchEnabled = false;
        return this;
    }

    setIsSearhEnabled(value) {
        this.#isSearchEnabled = value
        return this
    }

    getIsSearchEnabled() {
        return this.#isSearchEnabled;
    }

    fireEvent(name, data) {
        _.each(this.#listeners, (listener) => {
            listener(new DropDownEvent(name, data))
        })
    }

    setTitleRenderer(renderer) {
        if (!(renderer instanceof DropDownTitleRenderer)) {
            throw new Error("Title renderer should be a subclass of TitleRenderer")
        }
        this.#titleRenderer = renderer;

        return this;
    }

    /**
     * Title renderer is used to generate title of dropdown.
     * it will be autobinded to the DropDown object
     * param of the method isUpdate -> to render (when  or rerender component
     * */
    #titleRenderer;

    getTitleRenderer() {
        return _.ofNullable(this.#titleRenderer, () => DefaultDropDownTitleRenderer.INSTANSE)
    }

    enableFooter() {
        this.#isFooterEnabled = true
        return this
    }

    isFooterEnabled() {
        return this.#isFooterEnabled
    }

    setFooterRenderer(renderer) {
        this.#footerRenderer = renderer
    }

    getFooterRenderer(dropdownInstance) {
        if (this.#footerRenderer) {
            return this.#footerRenderer(dropdownInstance);
        }

        const resetButton = h.input('button').cl('reset_button').text('global.app.reset').hide().click(event => {
            if (dropdownInstance.getOptions().getIsSearchEnabled()) {
                dropdownInstance.getSearchInput().setVal('')
                dropdownInstance.performSearchHandler(dropdownInstance.getSearchInput().get(), event)
                dropdownInstance.getSearchInput().focus()
            }

            const selectedItems = [...dropdownInstance.getSelectedItems()]
            _.each(selectedItems, item => {
                MagicDropDown.removeSelection.call(dropdownInstance, item)
            })

            if (!dropdownInstance.getOptions().getIsRenderingAfterSearch()) {
                dropdownInstance.getOptions().getDataProvider().getData(dropdownInstance.drawSelection.bind(dropdownInstance))
            }
            dropdownInstance.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_DESELECTED, {"value": null, "DD": this})
        }, true)

        const finishButton = h.input('button').cl('finish_button').text('global.range.done').click(event => {
            dropdownInstance.toggleDropdown(false)
        }, true)

        this.addListener((event) => {
            if (_e(event.eventName, DropDownEvent.DD_EVENT_ITEM_SELECTED) ||
                _e(event.eventName, DropDownEvent.DD_EVENT_ITEM_DESELECTED) ||
                _e(event.eventName, DropDownEvent.DD_EVENT_SEARCH_PERFORMED)) {

                if ((this.getIsSearchEnabled() && dropdownInstance.getSearchInput().val()) || dropdownInstance.getSelectedItems().length) {
                    resetButton.show()
                } else {
                    resetButton.hide()
                }
            }
        })

        return h.div('footer_container').add(resetButton).add(finishButton)
    }


}

/**
 * @interface cannot bee used as instance
 * To render or update rendered object
 */

class DropDownTitleRenderer {


    /**
     * Executed when title is just generated
     * @param dropDown
     */
    renderTitle(dropDown) {
    };

    /**
     * executed when selection is changed
     * @param dropDown
     */
    updateTitle(dropDown) {
    };

}

class SingleValueOrEmptyValueTitleRenderer extends DropDownTitleRenderer {
    #component = h.div("value")

    renderTitle(dropDown) {
        let tagRendererFunction = _.ofNullable(dropDown.getOptions().getItemRenderer(), () => MagicDropDown.createTag).bind(dropDown);
        if (!_.isEmpty(dropDown.getSelectedItems())) {
            if (dropDown.getSelectedItems()[0]) {
                return this.#component.text().add(tagRendererFunction(dropDown.getOptions().getDataProvider().byValue(dropDown.getSelectedItems()[0])))
            }
        } else {
            //here empty tag
            let preselectedItem = dropDown.getOptions().getPreselectedStrategy().getItemOnNull();


            //todo maybe need in any case to select data!
            if (preselectedItem) {

                return this.#component.text().add(tagRendererFunction(preselectedItem))
            }
        }
        h.div('title_container')._text(dropDown.getOptions().getTitle())
    }

    updateTitle(dropDown) {
        this.#component.text(null)
        this.renderTitle(dropDown);
    }
}

class DefaultDropDownTitleRenderer extends DropDownTitleRenderer {

    static  INSTANSE = new DefaultDropDownTitleRenderer();

    renderTitle(dropDown) {
        if (!dropDown.getOptions().getTitle()) {
            return h.span("empty_title");
        }
        return h.div('title_container')._text(dropDown.getOptions().getTitle())
    }

}

/**
 * Don't use instance here because counter is a specific field!
 */
class DropDownCountedTitle extends DropDownTitleRenderer {

    renderTitle(dropDown) {
        if (!dropDown.getOptions().getTitle()) {
            return h.span("empty_title");
        }
        return h.div('title_container')
            .add(h.span("title_box")._text(this.formatTitle(dropDown.getOptions().getTitle(), dropDown)))
    }


    updateTitle(dropDown) {
    }


    formatTitle(title, dropDown) {
        return title;
    }

}

class MagicDropDown {
    static DD_VALUE_ATTR = "value";
    static DD_VALUE_NOTHING = null;
    static SELECTED_VALUES_DIV_CLASS = "selected_tags"
    static SELECTED_ITEM_CLASS = "selected"
    #options;

    #selectedItems = [];
    #dropdownValuesView;
    #ddItems;
    #isDropdownOpened = false
    #ddContainer
    #tagContainer
    #searchInput
    #debouncedSearchHandler
    #ddElements = []

    constructor(options = new MagicDropDownOptions()) {
        this.#options = options;

        this.#debouncedSearchHandler = _.debounce(this.performSearchHandler.bind(this), this.#options.getDebounceValue());
    }

    getElements() {
        return this.#ddElements
    }

    getValues() {
        return this.#dropdownValuesView;
    }

    getTagContainer(){
        return this.#tagContainer
    }


    getItems() {
        return this.#ddItems;
    }

    getSearchInput() {
        return this.#searchInput
    }

    setDropdownClosed() {
        this.#isDropdownOpened = false
    }

    toggleDropdown(value) {
        this.#isDropdownOpened = value
        let listener = null

        const clickedElement = event.target

        if (this.#isDropdownOpened) {
            this.#ddContainer.rcl('hidden')

            listener =this.handleOutsideClick.bind(this)
            document.addEventListener('click', listener);

            if(!this.#options.getIsRenderingAfterSearch()){
                setTimeout(() => {
                    this.getOptions().getDataProvider().getData(this.drawSelection.bind(this))
                }, 0)
            }

            new DropdownPopup(this.#ddContainer, this.#dropdownValuesView, this)

            // TODO: Change focus() method to focusitem() after magic.html.v3 migration

            dom.linkTo(this.#ddContainer, this.#dropdownValuesView);

            if(this.#options.getIsSearchEnabled()) {
                this.#searchInput.focus()
            }
        } else {
            new DropdownPopup(this.#ddContainer, this.#dropdownValuesView, this).removeContent()
            if (!clickedElement.classList.contains('element')) {
                this.#ddContainer.cl('hidden')
                if(_.notNull(listener)){
                    document.removeEventListener('click', listener)
                }

            }
        }
    }

    handleOutsideClick(event) {
        const dropdownElement = this.#dropdownValuesView.get();
        const dropdownItemsElement = this.#ddContainer.get();

        if (!dropdownElement.contains(event.target) && !dropdownItemsElement.contains(event.target)) {
            this.toggleDropdown(false);
        }
    }


    getSelectedItems() {
        return this.#selectedItems;
    }

    resetSelectedItems(value) {
        if (_.notNull(value)) {
            this.#selectedItems = [value];
        } else {
            this.#selectedItems = [];
        }
    }

    getOptions() {
        return this.#options;
    }

    fromData(data, keyField, valueField) {
        _.each(data, (item, index) => {
            this.addItem(item[keyField], item[valueField]);
        })
    }

    performSearch(input, event) {
        if (this.#options.getIsDebounceEnabled()) {
            this.#debouncedSearchHandler(input, event);
        } else {
            this.performSearchHandler(input, event)
        }
    }

    performSearchHandler(input, event) {
        this.createTagFromSearchInput(input, event)

        this.getOptions().getDataProvider().search(input.value, (data) => {
            if (this.#options.getIsRenderingAfterSearch() && !input.value) {
                this.drawSelection([]);
            } else {
                this.drawSelection(data);
            }
        });
        this.getOptions().fireEvent(DropDownEvent.DD_EVENT_SEARCH_PERFORMED, {"value": input.value, "DD": this})
    }

    createTagFromSearchInput(input, event) {
        if (_e(event.key, 'Enter') && this.#options.getIsCreateItemFromInputEnabled()) {
            this.resetSelectedItems(input.value)
            let tagRendererFunction = _.ofNullable(this.getOptions().getTagRenderer(), () => MagicDropDown.createTag).bind(this);
            this.#tagContainer
                .text(null)
                .add(tagRendererFunction({value: input.value, key: input.value}))
            this.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_SELECTED, {"value": input.value, "DD": this})
        }
    }

    build() {
        this.#ddContainer = h.div('drop_down_container').clIf(this.getOptions().isMultiSelect(), "multi").hide()
        this.#ddItems = h.div("drop_down_values").clIf(this.getOptions().isMultiSelect(), "multi")
        this.#dropdownValuesView = h.div("drop_down").click(() => this.toggleDropdown(!this.#isDropdownOpened))

        let value = this.getOptions().getPreselectedStrategy().getPreselectedItem();
        if (value){
            this.getOptions().getDataProvider().createItem(value)
            this.resetSelectedItems(this.getOptions().getPreselectedStrategy().getPreselectedItem()?.value)
        }

        this.#tagContainer = h.div(MagicDropDown.SELECTED_VALUES_DIV_CLASS)

        const headerContainer = h.div('drop_down_header')
            .add(this.getOptions().getTitleRenderer().renderTitle(this))
            .add(this.#tagContainer)

        this.#dropdownValuesView
            .add(headerContainer)
            .get().style.position = 'relative'

        this.getOptions().fireEvent(DropDownEvent.DD_EVENT_RENDERED, {
            "selectedValuesElement": this.#dropdownValuesView,
            searchElement: this.#ddItems,
            "provider": this.getOptions().getDataProvider()
        })

        if (this.getOptions().getIsSearchEnabled()) {
            this.#searchInput = h.input("text").cl("search_text element")
                .onKey(this.performSearch.bind(this)
                )
            this.#ddContainer.add(this.#searchInput)
        }
        this.#ddContainer.add(this.#ddItems);

        if(this.#options.isFooterEnabled()){
            this.#ddContainer.add(this.#options.getFooterRenderer(this))
        }

        //since v2 we always preselect a value according to strategy

        let preselectedItem = this.getOptions().getPreselectedStrategy().getPreselectedItem();

        //todo maybe need in any case to select data!
        if (preselectedItem) {
            this.getOptions().fireEvent(DropDownEvent.DD_EVENT_PRESELECTED_VALUE_RENDERED, {"value": preselectedItem.value, "DD": {"dd": this, "item": preselectedItem}})

            let tagRendererFunction = _.ofNullable(this.getOptions().getTagRenderer(), () => MagicDropDown.createTag).bind(this);
            this.#tagContainer.text().add(tagRendererFunction(preselectedItem))
        }

        return {values: this.#dropdownValuesView, items: this.#ddItems};

    }

    getRenderer() {
        return _.ofNullable(this.getOptions().getItemRenderer(), () => MagicDropDown.itemRenderer).bind(this)
    }

    drawSelection(data) {
        this.#ddItems.text(null);
        this.#ddElements = []
        const selectedItemsSet = new Set(this.#selectedItems);

        _.each(data, (item, index) => {

            let render = this.getRenderer();

            var rendered = this.bindSelectClick(render(item, index), item, index)

            if (selectedItemsSet.has(item.value)) {
                rendered.cl(MagicDropDown.SELECTED_ITEM_CLASS);
            }

            this.#ddElements.push({element: rendered, item: item})
            this.#ddItems.add(rendered)

        })

        dom.markNotLoading(this.#dropdownValuesView);

        dom.linkTo(this.#ddContainer, this.#dropdownValuesView);
        this.#ddContainer.show()
    }

    //@bind to the MagicDropDown itself
    static itemRenderer(item, index) {
        return h.div("dd_item").setData(MagicDropDown.DD_VALUE_ATTR, item.value).text(item.key, false);
    }


    bindSelectClick(element, item, index) {
        let bindedClick = () => {
            MagicDropDown.itemSelected.bind({"dd": this, "item": item, "renderer": element})(item.value, item);
            if (!this.getOptions().isMultiSelect()) {
                this.toggleDropdown(false)
            }
        };
        element.click(bindedClick);

        log.promisedDebug(() => {
            return {"text": "item_rendered", "item": item, "index": index, "DD": this}
        })
        this.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_RENDERED, {
            "element": element,
            "value": item,
            "index": index,
            "DD": this
        })
        return element;
    }


    selectRenderer(isSelected, renderer) {
        let tagRendererFunction = _.ofNullable(this.getOptions().getTagRenderer(), () => MagicDropDown.createTag).bind(this);

        //clean tags area
        let tags = this.#tagContainer.text(null);

        const preselectedItem = this.getOptions().getPreSelectedValue()
        if(!this.#selectedItems.length && preselectedItem) {
            tags.add(tagRendererFunction(preselectedItem))
            return
        }

        _.each(this.#selectedItems, (id) => {
            let item = this.getOptions().getDataProvider().byValue(id)
            tags.add(tagRendererFunction(item))
        })
    }

    //@bind to  the MagicDropDown
    static createTag(item) {
        if (_.isNull(item)) {
            log.promisedDebug(() => {
                return {"text": "passed null item element to create tag, we are not able to build tag"}
            })
            return h.div("not_parsable_tag")._text("not_parsable_tag");
        }
        const deleteTagButton = h.img("rm.png").cl("search_tag_remove").click(() => {
                MagicDropDown.removeSelection.bind(this)(item)
            }, true
        ).hide()

        if (this.getOptions().getIsDeleteTagButtonEnabled() && !item.noValueItem) {
            deleteTagButton.show()
        }

        return h.div("dd_tag").text(item.key, false).setData(MagicDropDown.DD_VALUE_ATTR, item.value)
            .add(h.div("close_value").add(deleteTagButton))
    }

    //@bind to the MagicDropDown
    static removeSelection(itemValue) {
        this.getSelectedItems().removeItem(itemValue)

        const preselectedItem = this.getOptions().getPreSelectedValue()
        if (!this.getSelectedItems().length && preselectedItem) {
            const element = this.getElements().find(item => item.item.value === preselectedItem.value)
            if (element) {
                MagicDropDown.itemSelected.bind({
                    dd: this,
                    renderer: element.element
                })(preselectedItem.value, preselectedItem)
            }
        }

        this.selectRenderer()
        this.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_DESELECTED, {"value": itemValue, "DD": this})
        this.getOptions().getTitleRenderer().updateTitle(this)
    }

    setValue(id) {
        try {
            let item = this.#options.getDataProvider().createItem(id)

            if(item) {
                MagicDropDown.itemSelected.call({"dd": this, "item": item, "renderer": null}, id, item)
            } else {
                log.promisedDebug(() => ({message: "Failed to set value"}))
            }
        }
        catch {

        }
    }

    //@bind to the {dd: MagicDropDown, item: item, "renderer" : renderer // a div, to select/deselect}
    static itemSelected(id, item) {
        if (_.isNull(id)) {
            //todo put logic to clean all filters
            // perhaps we need to mark item as preselected
            this.dd.getOptions().fireEvent(DropDownEvent.DD_EVENT_NO_VALUE_SELECTED, {"value": id, "DD": this})

            this.dd.resetSelectedItems();
            this.dd.getOptions().getTitleRenderer().updateTitle(this.dd)

            return;

        }

        if (this.dd.getOptions().isMultiSelect()) {

            if (this.dd.getSelectedItems().includes(item.value)) {
                //deselect
                this.dd.getSelectedItems().removeItem(item.value);
                this.dd.selectRenderer(false, this.renderer);
                this.renderer.rcl(MagicDropDown.SELECTED_ITEM_CLASS)
                this.dd.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_DESELECTED, {"value": id, "DD": this})


            } else {
                //select
                this.dd.getSelectedItems().push(item.value);
                this.dd.selectRenderer(true, this.renderer);
                this.renderer.cl(MagicDropDown.SELECTED_ITEM_CLASS)
                this.dd.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_SELECTED, {"value": id, "DD": this})

            }
        } else {
            // select
            this.dd.resetSelectedItems(item.value);
            this.dd.selectRenderer(true, item, this.renderer);
            this.renderer.rcl(MagicDropDown.SELECTED_ITEM_CLASS)
            this.dd.getOptions().fireEvent(DropDownEvent.DD_EVENT_ITEM_SELECTED, {"value": id, "DD": this})

        }

        if(this.dd.getOptions().getIsSearchUpdateOnSelect()) {
            this.dd.getSearchInput().setVal(item.key)
        }

        if (!this.dd.getOptions().isMultiSelect()) {
            this.dd.toggleDropdown(false);
        }
        //when items are selected or deselected we need to update title, perhaps it depends on this change
        this.dd.getOptions().getTitleRenderer().updateTitle(this.dd)
    }

}

class DropdownPopup {
    static instance = null

    #wrapper = h.div('dropdown-popup')
    #previousDropdown

    constructor(content, parentElement, dropdown) {
        if(DropdownPopup.instance) {
            DropdownPopup.instance.closePreviousDropdown(dropdown)

            return DropdownPopup.instance.removeContent().setContent(content).build(parentElement, dropdown)
        }

        DropdownPopup.instance = this.setContent(content).build(parentElement, dropdown)
    }

    removeContent() {
        this.#wrapper._text(null).remove()

        return this
    }

    setContent(content) {
        this.#wrapper.add(content)

        return this
    }

    closePreviousDropdown(dropdown) {
        if(this.#previousDropdown === dropdown) return
        this.#previousDropdown.setDropdownClosed()
    }

    build(parentElement, dropdown) {
        this.#wrapper.appendTo(parentElement)
        this.#previousDropdown = dropdown

        return this
    }
}

