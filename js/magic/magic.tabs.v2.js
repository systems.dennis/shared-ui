class MagicTabOptions {
    static DEFAULT_TAB_PANE_NAME = "simple_tap";

    static TAB_PANE_PERSONAL_SETTING_PREFIX = 'global.application.selected_pane_'

    static TAB_PIN_DIV_CLASS = 'pin_image'
    static TAB_PINED_DIV_CLASS = 'pinned_image'
    #tabTitleGenerator;
    #parent;
    #tabDefaultSelected;
    #tabListener;
    #counterFunction;
    #tabTranslater = false;
    #pinable = false;

    #name = MagicTabOptions.DEFAULT_TAB_PANE_NAME;

    constructor(tabTitleGenerator = MagicTabOptions.tabTitleGenerator,
                tabDefaultSelected = null, tabListener = null) {
        this.#tabListener = tabListener;
        this.#tabTitleGenerator = tabTitleGenerator;
        this.#tabDefaultSelected = tabListener;
    }

    setPinable(pinable, name) {
        this.#pinable = pinable;
        this.#name = name;
        return this;
    }

    setParent(parent){
        this.#parent = parent
    }

    getParent(){
        if(this.#parent){
            return this.#parent
        }
    }

    setTabTranslater(tabTranslater){
        this.#tabTranslater = tabTranslater;
    }

    getTabTranslater(){
        return this.#tabTranslater;
    }


    hasCounter() {
        return _.notNull(this.#counterFunction);
    }

    isPinable() {
        return this.#pinable;
    }

    setCounterFunction(counterFunction) {
        this.#counterFunction = counterFunction;
        return this;
    }

    getName() {
        return this.#name;
    }

    getCounterFunction() {
        return this.#counterFunction;
    }

    //we send here selected tab to avoid multiple requests to the server getting the same setting
    static tabTitleGenerator(tab, index, tabPane, selectedTab, translater) {
        let base = h.div("tab_pane_title").id(tab.name).pointer().clIf(_.e(index, 0), MagicTab.SELECTED_TAB_CLASS);

        let options = tabPane.getOptions();

        if (options.hasCounter()) {
            let count = h.span("tab_pane_title_counter");
            options.getCounterFunction()(tab, count, index, options)

            if(_.isTrue(translater)){
                base.add(h.span("tab_pane_title_text", tab.name, translater[tab.name])).add(count)
            }else{
                base.add(h.span("tab_pane_title_text", tab.name, true)).add(count)
            }
        } else {
            if(_.isTrue(translater)){
                base.text(tab.name, translater[tab.name])  
            }else{
                base.text(tab.name)
            }
        }


        if (_.notNull(tab.image)) {
            h.div("title_tab_image").add(h.divImg(tab.image).wh(16)).prependTo(base)
        }

        if (options.isPinable()) {
            let booleanPinned = false;
            if (_.isNull(selectedTab)) {
                booleanPinned = _e(index, 0);
            } else {
                booleanPinned = _e(tab.name, selectedTab);
            }


            h.div(MagicTabOptions.TAB_PIN_DIV_CLASS).wh(16).clIf(booleanPinned, MagicTabOptions.TAB_PINED_DIV_CLASS).clickPreventive((e) => {
                tabPane.pinUnpin(tab)
            }).prependTo(base)
        }

        return base;
    }


    getListener() {
        return this.#tabListener;
    }

    setTablistener(tabListener){
      this.#tabListener = tabListener;

      return this;
    }

    getTabTitleGenerator() {
        return this.#tabTitleGenerator;
    }

}

class MagicTab {


    static SELECTED_TAB_CLASS = "___selected_tab";
    static FIRST_ELEMENT = 0;
    static UNSELECTED_TAB_CONTENT = "hidden";
    static TAB_PANEL_CLASS = "tabs_panel"

    #headers = [];
    #components = [];
    #content;
    #options;

    constructor(options = new MagicTabOptions()) {
        this.#options = options;
    }


    hideFromSelection(name){
        let headerToDisable = _.find(this.#headers, (item)=>{return  _e(item.name, name)})
        headerToDisable?.headerEl.hide()
    }

    getContent(){
      return this.#content;
    }

    getComponents(){
        return this.#components
    }

    getHeaders(){
        return this.#headers
    }


    getSelectedIndex() {
        if (this.#components.length < 1) {
            return null;
        }
        let first = this.#components[0];

        if (!this.#options.isPinable()) return first.name;
        try {
            let setting = PersonalSettings.getSetting(MagicTabOptions.TAB_PANE_PERSONAL_SETTING_PREFIX + this.#options.getName())
            return _.each(this.#components, (c) => {
                if (_e(c.name, setting)) {
                    return c.name
                }
            });
        } catch (e) {
            return first.name;
        }

    }

    saveSelectedIndex(tabName) {
        try {
            PersonalSettings.setSetting(MagicTabOptions.TAB_PANE_PERSONAL_SETTING_PREFIX + this.#options.getName(), tabName)
        } catch (e) {
            dom.message('global.fail', true, ToastType.ERROR)
        }
    }

    pinUnpin(tab) {
        if (_.isFalse(this.getOptions().isPinable())) {
            return;
        }

        this.saveSelectedIndex(tab.name);

        _.each(this.#headers, (header) => {
            let component = header.headerEl.eachOfClass(MagicTabOptions.TAB_PIN_DIV_CLASS, (el) => {
                return h.from(el)
            })
            if (_e(header.name, tab.name)) {
                component.cl(MagicTabOptions.TAB_PINED_DIV_CLASS)
            } else {
                component.rcl(MagicTabOptions.TAB_PINED_DIV_CLASS)
            }
        })


    }

    fireEvent(tab, eventName, additional, parent) {

        if (_.notNull(this.#options.getListener())) {
            let event = new MagicTabEvent();
            event.tab = tab;
            event.eventType = eventName;
            event.additional = additional;
            event.parent = parent;
            this.#options.getListener()(event)
        }
    }


    addLazyTab(tabName, tabBuilder, parent, image) {
        let tab = {}
        tab.name = tabName;
        tab.builder = tabBuilder;
        tab.component = undefined;
        tab.parent = parent;
        tab.image = image;

        if (this.#components.length == 0) {
            tab.component = undefined
            tab.builder = tabBuilder;
        }

        this.#components.push(tab)

        this.fireEvent(tab, TableEvent.EVENT_TAB_ADDED, {'type': 'lazy', 'parent': parent}, this.#options.getParent())
        return this;
    }

    addTab(tabName, component, image) {
        let tab = {}
        tab.name = tabName;
        tab.builder = undefined;
        tab.component = component;
        tab.image = image;
        this.#components.push(tab);

        this.fireEvent(tab, TableEvent.EVENT_TAB_ADDED, {'type': 'normal'}, this.#options.getParent())
        return this;
    }

    getOptions() {
        return this.#options;
    }


    addTabAfterDeploy(tabName, component, image){
        let tab ={}
        tab.name = tabName;
        tab.builder = undefined;
        tab.component = component;
        tab.image = image;
        this.#components.push(tab);
        
        let _this = this;
        let selected = this.getSelectedIndex();
        
        let el = this.#options.getTabTitleGenerator()(tab, 1, _this, selected, this.#options.getTabTranslater()).appendTo(this.tabPaneHeader).click(function (e) {
            _this.changeTab(tab, _this.tabContentPanel);
            _this.tabPaneHeader.eachOf((x) => {
                h.from(x).rcl(MagicTab.SELECTED_TAB_CLASS)
            });
            h.from(e).cl(MagicTab.SELECTED_TAB_CLASS);
        });
        _this.#headers.push({'name': tab.name, 'headerEl': el})
        this.fireEvent(tab, MagicTabEvent.EVENT_TAB_TITLE_GENERATED, el, this.#options.getParent())

        if (utils.isNull(tab.component)) {
            return utils.CONTINUE;
        }
        this.tabContentPanel.add(h.from(tab.component).hide())
    }


    deployTo(where) {
        this.where = h.from(where);

        let tabPane = h.div(MagicTab.TAB_PANEL_CLASS);
        this.#content = tabPane;
        let _this = this;
        if (this.#components.length > 1) {
            this.tabPaneHeader = h.div("tab_panel_header").appendTo(tabPane);
            this.tabContentPanel = h.div(MagicTab.TAB_PANEL_CLASS + '-content').appendTo(tabPane);
            let selected = this.getSelectedIndex();
            utils.each(this.#components, (cp, index) => {


                let el = this.#options.getTabTitleGenerator()(cp, index, _this, selected, this.#options.getTabTranslater()).appendTo(this.tabPaneHeader).click(function (e) {
                    _this.changeTab(cp, _this.tabContentPanel);
                    _this.tabPaneHeader.eachOf((x) => {
                        h.from(x).rcl(MagicTab.SELECTED_TAB_CLASS)
                    });
                    h.from(e).cl(MagicTab.SELECTED_TAB_CLASS);
                });
                _this.#headers.push({'name': cp.name, 'headerEl': el})
                this.fireEvent(cp, MagicTabEvent.EVENT_TAB_TITLE_GENERATED, el, this.#options.getParent())
            })

            utils.each(this.#components, (x) => {
                if (utils.isNull(x.component)) {
                    return utils.CONTINUE;
                }
                this.tabContentPanel.add(h.from(x.component).hide())
            })
            tabPane.appendTo(this.where);
//this.#components.find(x => x.name)
            let selectedEl = _.find(this.#components, (x) => _e(x.name, selected));
            if (_.isNull(selectedEl)) {
                selectedEl = this.#components[0];
            }
            this.changeTab(selectedEl, this.tabContentPanel)
        } else {
            //if there are no components or only one we should not pin it!
            this.getOptions().setPinable(false, MagicTabOptions.DEFAULT_TAB_PANE_NAME)
            if (utils.e(this.#components.length, 1)) {
                if (utils.isNull(this.#components[MagicTab.FIRST_ELEMENT].component)) {
                    this.#components[MagicTab.FIRST_ELEMENT].component = this.#components[MagicTab.FIRST_ELEMENT].builder();
                }
                this.where.add(this.#components[MagicTab.FIRST_ELEMENT].component);

            } else {
                log.debug("tab without content, skip it!")
            }
        }

    }


    changeTab(component, tabContent) {
        if (component)

            //initial build of the lazy tab
            if (utils.notNull(component.builder)) {
                component.component = h.from(component.builder(component.parent));
                component.builder = undefined;
                tabContent.add(component.component);

                this.fireEvent(component, TableEvent.EVENT_TAB_CONTENT_GENERATED, component.component, this.#options.getParent())

            }

        utils.each(this.#components,
            (tabContent) => {
                if (utils.notNull(tabContent.component)) {
                    tabContent.component.cl(MagicTab.UNSELECTED_TAB_CONTENT);
                }
            })

        component.component.rcl(MagicTab.UNSELECTED_TAB_CONTENT)

        _.each(this.#headers, (header) => {
            if (_e(header.name, component.name)) {
                header.headerEl.cl(MagicTab.SELECTED_TAB_CLASS)
            } else {
                header.headerEl.rcl(MagicTab.SELECTED_TAB_CLASS)
            }
        })

        this.fireEvent(component, MagicTabEvent.EVENT_TAB_SELECTED, {tabContent}, this.#options.getParent())

    }

    selectTabByName(tabName){
        let selectedComponent
        let selectedHeader 
        _.each(this.#components, (component)=>{
            if(_e(component.name, tabName)){
                component.component.show();
                selectedComponent = component
            }else{
                component.component.hide();
            }
        })
        _.each(this.#headers, (header)=>{
            if(_e(header.name, tabName)){
               header.headerEl.cl(MagicTab.SELECTED_TAB_CLASS)
               selectedHeader = header
            }else{
                header.headerEl.rcl(MagicTab.SELECTED_TAB_CLASS)
            }
        })
        this.fireEvent(selectedComponent, MagicTabEvent.EVENT_TAB_SELECTED, selectedHeader, this.#options.getParent())
    }

    getSelectedTab(){
      return utils.each(this.#headers, (header, i) => {
        if(header.headerEl.ccl(MagicTab.SELECTED_TAB_CLASS)){
          header.index = i;
          return header;
        }
      })
    }
}
