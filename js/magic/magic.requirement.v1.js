class $$ {
    static #___loaded = []

    static requireMagic(magicPath, version = null) {
        if (!version){
            version = $$versions[magicPath]
        }
        return $$.require(new Requirement().magic(magicPath, version))
    }
    static require(requirement) {
        if ($$.#___loaded.indexOf(requirement.getPath()) > -1) {
            return
        }
        try {
            requirement.setSync().createScript(() => {
                $$.#___loaded.push(requirement.getPath())
                console.log(requirement.getPath() + "... is loaded")
            })
        } catch (ex) {
            console.log("script cannot be loaded: " + ex)
        }

    }

}

class Requirement {
    static #count = 0;
    static MAGIC_FOLDER = "magic";
    static SCRIPT_TAG = "script";
    static HEAD_TAG = "html";
    #local = false;
    #path;
    #version = null;
    #async = true;
    #folder = null;
    #isMagic = false;
    static #finished = undefined;

    static #firstToLoad = [];
    static #asyncToLoad = []

    static isLoadingFinished() {
        return Requirement.#finished && Requirement.#count === 0;
    }


    static base() {

        return new Requirement()
            .magic("utils", MagicVersion.utils).next(true)
            .magic("html", MagicVersion.html).next(true)

    }

    finish() {



        this.createScriptFunction()


        Requirement.#finished = true;
        Requirement.showLoading()
    }

    static processed = 0;
    static getUnifiedContent(paths, name='local') {
        let xhr = new XMLHttpRequest();
        xhr.open("POST", Environment.fileStorageServer + "api/v2/js/unify", false);

        let text;
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                text = xhr.responseText;
            }
        };
        xhr.setRequestHeader(
            "Content-type", "application/json"
        );
        xhr.send( JSON.stringify({"force":true ,"name" : name, "unifyJS" : paths.map((x)=>x.getPath())}));
        return text

    }

     createScriptFunction (){


        if (Environment.developmentMode) {
            for (var e = 0; e < Requirement.#firstToLoad.length; e++) {
                if(e === Requirement.#firstToLoad.length - 1){
                    Requirement.#firstToLoad[e].createScript(function(){Requirement.#count = 100;});
                }else{
                    Requirement.#firstToLoad[e].createScript();
                }
            }
        } else {
            let script = document.createElement("script");
            let pageURL = (window.location.host + window.location.pathname).split("/").join("-").slice(0, -1)
            let content = Requirement.getUnifiedContent(Requirement.#firstToLoad, pageURL)
            script.defer = true;
            script.async = false;
            script.src = Environment.fileStorageServer +"api/v2/js/"+ content;
            script.onload = function(){Requirement.#count = 100;}

            document.children[0].prepend(script);

        }

    }

    static showLoading() {

        if (Requirement.isLoadingFinished() == true) {
            setTimeout(Requirement.showLoading, 500)
        } else {

            try {
                onPageLoad();
            } catch (e) {
                console.log(e);
            }
            ____USER.initPage();

            setTimeout(() => {
                EnvironmentUploader.endLoading()
            }, 200);

        }
    }

    magic(path, version = "1") {
        this.#folder = Requirement.MAGIC_FOLDER;
        this.#path = Requirement.MAGIC_FOLDER + "." + path;
        this.#version = version;
        this.#isMagic = true;
        return this;
    }

    toQueue() {


        if (this.#async) {
            Requirement.#asyncToLoad.push(this);
        } else {
            Requirement.#firstToLoad.push(this);
        }
//        head.append(script);
        return this;
    }

    createScript(onloadEvent) {

        const script = document.createElement(Requirement.SCRIPT_TAG);
        script.async = this.#async;
        script.src = this.getPath()

        console.log("Script loading: " + script.src) + '?v=' + Environment.version;
        script.type = "text/javascript";
        var async = this.#async
        if (async) {
            Requirement.#count++;
        }
        script.onload = function () {
            onloadEvent && onloadEvent();
            if (async) {
                Requirement.#count--
            }

        }


        document.getElementsByTagName(Requirement.HEAD_TAG)[0].prepend(script)

    }

    setFolder(folder) {
        this.#folder = folder;
    }

    getPath() {
        if (this.#local) {

            return this.getLocalFolderOrDefault() +  this.getFileName();
        } else {

            return this.getRemoteOrDefault() +  this.getFileName();
        }
    }

    getFileName() {

        return (this.#folder ? this.#folder + "/" : "")
            + this.#path
            + (this.#version ? (this.#isMagic ? (".v" + this.#version) : "." + this.#version) : "")
            + ".js";

    }


    getLocalFolderOrDefault() {
        return Environment?.localScriptFolder ? Environment?.localScriptFolder : Environment?.self + "js-local/";
    }

    getRemoteOrDefault() {
        return Environment?.scriptServer ? Environment?.scriptServer : "https://js.dennis.systems/";
    }

    setIsLocal() {
        this.#local = true;
        return this;
    }

    setSync() {
        this.#async = false;
        return this;
    }

    setPath(path) {
        this.#path = path;
        return this;
    }

    setVersion(version) {
        this.#version = version;
        return this;
    }

    next(sync = false) {
        if (sync) {
            this.setSync();
        }

        this.toQueue();
        return new Requirement();
    }

}

class MagicVersion {
    static html = "2"
    static utils = "1"
    static prompt = "2"
    static table = "4"
    static list = "3"
    static executor = "2"
    static authorization = "1"
    static bar = "1"
    static contextMenu = "1"
    static personalSettings = "1"
    static dateChooser = "1"
    static dateConstants = "1"
    static dom = "2"
    static event = "1"
    static files = "1"
    static page = "1"
    static request = "1"
    static ordering = "1"
    static grid = "1"
    static sort = "1"
    static fields = "2"
    static favorite = "1"
    static tabs = "2"
    static prototypes = "1"
    static drag = "1"
    static trueFalseSelector = "1"
    static form = "1"
    static remoteChooser = "3"
    static pagination = "1"
    static search = "2"
    static themeUploader = "1"
    static view = "1"
    static jtree = "3"
}