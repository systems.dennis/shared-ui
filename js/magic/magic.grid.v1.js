class MagicGridOptions {
    #showEmptyFields = true;
    #customLayoutFunction;

    setShowEmptyFields(toShow) {
        this.#showEmptyFields = toShow;
        return this;
    }

    setCustomLayoutFunction(func) {
        this.#customLayoutFunction = func;
        return this;
    }

    getShowEmptyFields() {
        return this.#showEmptyFields;
    }

    getCustomLayoutFunction() {
        return this.#customLayoutFunction;
    }
}

class MagicGrid {
    #options;
    #parent;
    #where;
    #fields = [];
    favoriteEnabled;
    #container = h.div("item_container");

    constructor(parent, options = new MagicGridOptions()) {
        this.#options = options;
        this.#parent = parent;
        this.favoriteEnabled = Favorite.enabled();

    }


    getParent() {
        return this.#parent
    }

    getOptions() {
        return this.#options;
    }

    getWhere() {
        return this.#where;
    }

    drawValues(data, list, dataConverter = null, eventListener = null) {

        if (dataConverter == null) {
            dataConverter = MagicTable.defaultConverter;
        }

        this.#container.text(null);
        let _this = this;
        _.each(data.content, item => {
            _this.drawItem(item, dataConverter)
        })
    }

    rebuildHeaders() {

    }

    drawItem(item, dataConverter) {
        let itemContainer = h.div("item_block");
        let headers = this.#parent.getOrdering()?.getHeaders() || this.#fields;

        if (_.notNull(this.getOptions().getCustomLayoutFunction())) {
            itemContainer.add(this.getOptions().getCustomLayoutFunction()(item, dataConverter, this));
        } else {

            _.each(headers, (field) => {
                if (field.visible) {

                    let toShow = true;
                    if (_.isObjectEmpty(item[field.field], _.isObject(item[field.field]))) {
                        toShow = this.getOptions().getShowEmptyFields();
                    }

                    if (toShow) {
                        itemContainer.add(h.div("item_value").add(h.span("element_header", field.translation, true)).add(MagicGrid.transform(item[field.field], field, item, dataConverter)));
                    }
                }

            })
        }

        this.#container.add(itemContainer);
    }

    getId() {
        return this.#parent.getId();
    }

    drawHeader(where) {

        h.from(where).add(this.#container);
        this.#fields = this.#parent.getLastData().fields;
        this.#where = where;
        return this;

    }

    static transform(itemElement, field, data, dataConverter) {

        return dataConverter(field, itemElement, data, null, field.searchType, MagicGrid.favoriteEnabled);
    }
}