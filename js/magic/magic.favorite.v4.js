class FavoriteOptions {
    #favSelectedAction;
    #favDeselectedAction;
    #favClassSelected = "____fav";
    #favClassDeselected = "____fav_deselected";
    #type;
    #id
    #serverRequest
    #autoClickAssigner = true;

    constructor(id, type, serverRequest, favSelectedAction, favDeselectedAction) {
        this.#favSelectedAction = favSelectedAction;
        this.#favDeselectedAction = favDeselectedAction;
        this.#id = id;
        this.#type = type;
        this.#serverRequest = serverRequest;
    }

    getAutoClickAssigner() {
        return this.#autoClickAssigner;
    }

    setAutoClickAssignerOff() {
        this.#autoClickAssigner = false;
        return this;
    }


    setClassSelected(cl) {
        this.#favClassSelected = cl;
        return this;
    }

    getServerRequest() {
        return this.#serverRequest;
    }

    setClassDeselected(cl) {
        this.#favClassDeselected = cl;
        return this;
    }


    getType() {
        return this.#type;
    }

    getId() {
        return this.#id;
    }

    getFavSelectedAction() {
        return this.#favSelectedAction;
    }

    getFavDeselectedAction() {
        return this.#favDeselectedAction;
    }


    getFavClassSelected() {
        return this.#favClassSelected
    }

    getFavClassDeselected() {
        return this.#favClassDeselected;
    }


}

class Favorite {
    static IS_FAVORITE_URI = "is_favorite";
    static FAVORITE_SHARED = "favorite";
    static FAVORITE_ENABLED_URI = "enabled";

    static isFavorite(options) {
        let type = options.getType();
        let id = options.getId();
    }

    static enabled(serverRequest) {
        if (!serverRequest) {
            log.promisedDebug(() => {
                return {"type": "favorite", "text": "NO SERVER REQUEST"}
            })
            return false;
        }
        let res = false;
        const request = serverRequest.copy().cacheResponse(null, true).shared(Favorite.FAVORITE_SHARED).request(Favorite.FAVORITE_ENABLED_URI).method(Executor.GET_METHOD);

        try {
            if (!serverRequest?.getAuthorization()?.hasToken()) {
                return false;
            }
            request.async(false).run((data) => {
                res = data.enabled;
            }, (error) => {
                log.promisedDebug(() => {
                    return {"type": "favorite", "text": "ENABLED SERVER ERROR", "error": error?.responseText}
                })

                return res;
            })
        } catch (error) {
            log.promisedDebug(() => {
                return {"type": "favorite", "text": "ENABLED SERVER ERROR", "error": error?.responseText}
            })
        }

        return res;
    }

    static changeTo() {
        if (Favorite.enabled(this.options.getServerRequest())) {
            h.from(this.where).cl(this.enabled ? this.options.getFavClassSelected() : this.options.getFavClassDeselected())
            h.from(this.where).rcl(!this.enabled ? this.options.getFavClassSelected() : this.options.getFavClassDeselected());
        }
    }

    static createPayload(options) {
        return {
            "type": options.getType(), "modelId": options.getId()
        }
    }

    static markElementAsFavorite() {

        const request = this.options.getServerRequest().copy().forceNoCache().shared(Favorite.FAVORITE_SHARED).method(Executor.POST_METHOD)

            .payload(Favorite.createPayload(this.options));

        if (this.enabled) {
            request.request("add")
        } else {
            request.request("delete");
        }

        request.run(Favorite.changeFavoriteListener.bind(this))


        StorageCacheProvider.getInstance()
            .deleteFromCache(request.copy().shared(Favorite.FAVORITE_SHARED).request(this.FAVORITE_ENABLED_URI)
                .payload(Favorite.createPayload(this.options, this.options.getType())));
    }


    constructor(options, where) {

        this.draw(options, where);
    }

    draw(options, where) {
        if (!Favorite.enabled(options.getServerRequest())) {
            return;
        }


        if (_.isNull(where)) {
            log.promisedDebug(() => {
                return {"item": where, "error": " isNull"}
            })
        }

        options.getServerRequest().copy().shared(Favorite.FAVORITE_SHARED).cacheResponse(600000, true)
            .request(Favorite.IS_FAVORITE_URI).payload(Favorite.createPayload(options)).method(Executor.POST_METHOD).run((data) => Favorite.changeTo.bind({
            "options": options,
            "where": where,
            "enabled": data.enabled
        })())

        if (options.getAutoClickAssigner()) {
            where.get().onclick = this.bindClickTo.bind({"options": options, "where": where});
        }
    }

    bindClickTo(e) {
        e.preventDefault();
        e.stopPropagation();

        this.enabled = !this.where.ccl(this.options.getFavClassSelected());


        Favorite.markElementAsFavorite.bind(this)();
        Favorite.changeTo.bind(this)(this.enabled);

    }

    static changeFavoriteListener(data){
        Favorite.changeTo.bind(this)(data?.enabled);
        if (data?.enabled && this.options.getFavSelectedAction()) {
            this.options.getFavSelectedAction().bind(this)()
        }
        if (!data?.enabled && this.options.getFavDeselectedAction()) {
            this.options.getFavDeselectedAction().bind(this)()
        }
    }


}

h.prototype.favorite = function (id, type, request,
                                 onSelect = null,
                                 onDeselect = null) {
    let options = new FavoriteOptions(id, type, request, onSelect, onDeselect)
    new Favorite(options, this);
    return this;
}

h.prototype.favoriteIf = function (condition, id, type, request,
                                   onSelect = null,
                                   onDeselect = null){
    if(condition){
        return this.favorite(id, type, request, onDeselect)
    }else{
        return this
    }
}
