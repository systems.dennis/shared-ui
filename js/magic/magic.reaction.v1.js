class ReactionOptions {
    #favSelectedAction;
    #favDeselectedAction;
    #favClassSelected = "____fav";
    #favClassDeselected = "____fav_deselected";
    #type;
    #id
    #serverRequest
    #autoClickAssigner = true;
    #reactionType

    constructor(id, type, serverRequest, favSelectedAction, favDeselectedAction) {
        this.#favSelectedAction = favSelectedAction;
        this.#favDeselectedAction = favDeselectedAction;
        this.#id = id;
        this.#type = type;
        this.#serverRequest = serverRequest;
    }

    setReactionType(reactionType){
        this.#reactionType = reactionType;
        return this
    }

    getReactionType(){
        return this.#reactionType;
    }

    getAutoClickAssigner() {
        return this.#autoClickAssigner;
    }

    setAutoClickAssignerOff() {
        this.#autoClickAssigner = false;
        return this;
    }


    setClassSelected(cl) {
        this.#favClassSelected = cl;
        return this;
    }

    getServerRequest() {
        return this.#serverRequest;
    }

    setClassDeselected(cl) {
        this.#favClassDeselected = cl;
        return this;
    }


    getType() {
        return this.#type;
    }

    getId() {
        return this.#id;
    }

    getReactSelectedAction() {
        return this.#favSelectedAction;
    }

    getReactDeselectedAction() {
        return this.#favDeselectedAction;
    }


    getReactClassSelected() {
        return this.#favClassSelected
    }

    getReactClassDeselected() {
        return this.#favClassDeselected;
    }


}

class Reaction {

    static IS_REACTION_URI = "is_reaction";
    static REACTION_ENABLED_URI = "enabled";
    static REACTED_CLASS = "reaction_classes";

    static isFavorite(options) {
        let type = options.getType();
        let id = options.getId();
    }

    static enabled(serverRequest) {
        if (!serverRequest) {
            log.promisedDebug(() => {
                return {"type": "reaction", "text": "NO SERVER REQUEST"}
            })
            return false;
        }
        let res = false;
        const request = serverRequest.copy().cacheResponse(null, true).shared().request(Reaction.REACTION_ENABLED_URI).method(Executor.GET_METHOD);

        try {
            if (!serverRequest?.getAuthorization()?.hasToken()) {
                return false;
            }
            request.async(false).run((data) => {
                res = data.enabled;
            }, (error) => {
                log.promisedDebug(() => {
                    return {"type": "reaction", "text": "ENABLED SERVER ERROR", "error": error?.responseText}
                })

                return res;
            })
        } catch (error) {
            log.promisedDebug(() => {
                return {"type": "favorite", "text": "ENABLED SERVER ERROR", "error": error?.responseText}
            })
        }

        return res;
    }

    static changeTo() {
        if (Reaction.enabled(this.options.getServerRequest())) {
            h.from(this.where).cl(this.enabled ? this.options.getReactClassSelected() : this.options.getReactClassDeselected())
            h.from(this.where).rcl(!this.enabled ? this.options.getReactClassSelected() : this.options.getReactClassDeselected());
        }
    }

    static createPayload(options) {
        return {
            "type": options.getType(), "modelId": options.getId(), "reactionType": options.getReactionType()
        }
    }

    static markElementAsReacted() {

        const request = this.options.getServerRequest().copy().forceNoCache().shared().method(Executor.POST_METHOD)

            .payload(Reaction.createPayload(this.options));

        if (this.enabled) {
            request.request("add")
        } else {
            request.request("delete");
        }

        request.run(Reaction.changeReactionListener.bind(this))


        StorageCacheProvider.getInstance()
            .deleteFromCache(request.copy().shared().request(this.REACTION_ENABLED_URI)
                .payload(Reaction.createPayload(this.options, this.options.getType())));
    }


    constructor(options, where) {

        this.draw(options, where);
    }

    draw(options, where) {
        if (!Reaction.enabled(options.getServerRequest())) {
            return;
        }


        if (_.isNull(where)) {
            log.promisedDebug(() => {
                return {"item": where, "error": " isNull"}
            })
        }

        options.getServerRequest().copy().shared().cacheResponse(600000, true)
            .request(Reaction.IS_REACTION_URI).payload(Reaction.createPayload(options)).method(Executor.POST_METHOD).run((data) => Reaction.changeTo.bind({
            "options": options,
            "where": where,
            "enabled": data.enabled
        })())

        if (options.getAutoClickAssigner()) {
            where.get().onclick = this.bindClickTo.bind({"options": options, "where": where});
        }
    }

    bindClickTo(e) {
        e.preventDefault();
        e.stopPropagation();

        this.enabled = !this.where.ccl(this.options.getReactClassSelected());


        Reaction.markElementAsReacted.bind(this)();
        Reaction.changeTo.bind(this)(this.enabled);

    }

    static changeReactionListener(data){
        Reaction.changeTo.bind(this)(data?.enabled);
        if (data?.enabled && this.options.getReactSelectedAction()) {
            this.options.getReactSelectedAction().bind(this)()
        }
        if (!data?.enabled && this.options.getReactDeselectedAction()) {
            this.options.getReactDeselectedAction().bind(this)()
        }
    }


}

h.prototype.favorite = function (id, type, request,
                                 onSelect = null,
                                 onDeselect = null) {
    let options = new ReactionOptions(id, type, request, onSelect, onDeselect)
        .setReactionType("favorite")
    new Reaction(options, this);
    return this;
}

h.prototype.favoriteIf = function (condition, id, type, request,
                                   onSelect = null,
                                   onDeselect = null){
    if(condition){
        return this.favorite(id, type, request, onDeselect)
    }else{
        return this
    }
}
