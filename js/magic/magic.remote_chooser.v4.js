class MagicRemoteFetcherOptions {
    #favoriteEnabled;
    #pathModifier = (path) => path;


    #serverRequest;


    constructor(favoriteEnabled = true) {
        this.#favoriteEnabled = favoriteEnabled;
    }

    setServerRequest(request){
        if (!request instanceof ServerRequest){
            throw new Error("Server request should be instance of ServerRequest")
        }
        this.#serverRequest = request;
        return this;
    }

    getServerRequest(){
        return this.#serverRequest;
    }

    isFavoriteEnabled() {
        return this.#favoriteEnabled;
    }

    setPathModifier(func){
      this.#pathModifier = func;

      return this;
    }

    getPathModifier(){
      return this.#pathModifier;
    }
}

class MagicRemoteFetcher {

    init() {
    }

    drawSelection(magicRemoteChooser) {
    }

    showSelection() {
    }

    getName() {
    }


}

class FavoriteFetcher extends MagicRemoteFetcher {

    #content = h.div('content-chooser');
    #parent;


    constructor(parent = null) {
        super();
        this.#parent = parent;
    }

    getParent() {
        return this.#parent;
    }

    showSelection() {
        //normally not needed for this selection
    }


    drawItemSelector(magicRemoteChooser) {
        magicRemoteChooser.getData(magicRemoteChooser.#parent.getHeader())
        return magicRemoteChooser.getContent();
    }

    getData(header) {
        let type = encodeURI(header.getData().searchName);
        let _this = this;
        let searchFavoritePath = this.#parent.getRequest("/byName?name=\"" + type).entry("shared").subEntry("favorite")
        searchFavoritePath.run((res)=>{
            let favType = res.result;
            let field = _this.getParent().getHeader().getField();
            let searchPathFavPath = this.#parent.getRequest("/list"+ favType + "/" + type).entry("shared").subEntry("favorite");
            searchPathFavPath.run((data)=>{_this.showVariants(data, field, favType);})
        })
    }

    showVariants(res, field, favType) {
        this.#content.text(null);
        utils.each(res.content, (item) => {
            this.#content.add(this.#parent.compile(item, field, favType))
        });
    }

    getContent() {
        return this.#content;
    }

    getName() {
        return "global.pages.chooser.remote.favorite";
    }
}

class SearchMagicFetcher extends MagicRemoteFetcher {

    #parent;
    #searchText = h.input("text").cl("search_text").placeHolder('global.search');
    #content = h.div('content-chooser');
    #selection = h.div("selector");

    constructor(parent = null) {
        super();
        this.#parent = parent;
        this.#searchText.id("search_input_" + this.#parent.getHeader().getField())
    }


    drawSelectionBox(parent) {
        if (!this.#selection.ccl("loaded")) {
            this.buildSearchForm(parent);
            this.showSelection();
        }
        return parent.#content
    }

    drawItemSelector(parent) {
        return parent.drawSelectionBox(parent);
    }

    getContent() {
        return this.#content;
    }

    getSearchText() {
        return this.#searchText;
    }

    getStatus() {
        return h.div("search_entity_title").text("global.pages.search." + this.#parent.getHeader()?.getField());
    }

    buildSearchForm(parent) {
        let _this = this;

        let searchForm = h.div("div").cl("search_entity_form").appendTo(parent.#content);
        let searchHeader = h.div("div").cl("search").cl("search_header");

        //add button

        h.img("add.png.png").cl("_s_box_add_btn").wh(16).click(function () {
            _this.#parent.showForm();
        }).appendTo(searchHeader).hide();

        searchHeader.prependTo(searchForm);
        this.#searchText.appendTo(searchHeader);
        h.div('search_backgraund_image').appendTo(searchHeader);

        this.#searchText.get().onkeyup = function () {

            _this.showSelection(0);
        };
        parent.getContent().add(searchForm);

        this.#selection.cl("loaded").appendTo(searchForm);

    }

    refresh() {
        this.showSelection(0);
    }



    showSelection(page = 0) {


        let searchPath = "/";

        let type = this.#parent.getHeader().getData().searchName.trim();

        let field = this.#parent.getHeader().getField();

        let _this = this;
        searchPath = searchPath + type + "?a=a";

        let subType = this.#parent.getHeader().getData().subType;

        if (_.notNull(subType)) {
            searchPath += "&sub_type=" + subType;
        }
        try {
            if (changeSearchPath != null) {
                searchPath = changeSearchPath(searchPath, this.#parent.getHeader().getData().searchName);
            }
        } catch (ex) { log.trace("here is a problem " + ex)}
        searchPath = this.#parent.getOptions().getPathModifier()(searchPath);

        // Here we check if chooser has multiply servers  
        let multiServerRequest
        
        try{
            multiServerRequest = this.#parent.getParent().getOptions().getFormMultiServerRequest()
        }catch{
            multiServerRequest = null
        }
         
        var favoriteRequest =  this.#parent.getRequest("/byName?name=" + type).shared("favorite");
        let favType = null;

        if (this.#parent.isFavoriteEnabled()) {
            favoriteRequest.async(false).run((res)=> {favType = res.result});
        }
        if(multiServerRequest){
            let fieldName = this.#parent.getHeader().getData().searchName
            multiServerRequest(fieldName).searchType(fieldName, (res)=>{
                let favType = res.result;
               _this.showVariants(res, fieldName, favType);
            })
        }else{
             var searchRequest = this.#parent.getRequest().api("/api/v1").entry("search").subEntry("type");
             searchRequest.request(searchPath +  "&s=" + encodeURI(_this.#searchText.get().value) + '&page=' + page )
            .run((res)=> {   
                _this.showVariants(res, field, favType);
            })

        }

    }

    showVariants(res, field, favType) {
        this.#selection.text(null);
        utils.each(res.content, (item) => {
            this.#selection.add(this.#parent.compile(item, field, favType))
        });

        this.#parent.showChooserPagination(res, this);
    }

    getName() {
        return "global.search.all";
    }


}

class MagicRemoteChooser {

    #oldValue;
    static SELECT_TEXT = "pages.global.search.select";

    static EVENT_ITEM_SELECTED = "EVENT_ITEM_SELECTED";
    static EVENT_ITEM_DESELECTED = "EVENT_ITEM_DESELECTED";

    static EVENT_ITEM_CHANGED = "EVENT_ITEM_CHANGED";
    static EVENT_ITEMS_RENDERED = "EVENT_ITEMS_RENDERED";
    static EVENT_ITEM_RENDERED = "EVENT_ITEM_RENDERED";
    static EVENT_CHOOSER_INITIATED = "EVENT_CHOOSER_INITIATED";
    static EVENT_BEFORE_INITIALIZATION = "EVENT_BEFORE_CHOOSER_INITIATED";
    static EVENT_FETCHER_ADDED = "EVENT_FETCHER_ADDED";
    static EVENT_SELECTOR_OPENED = "EVENT_SELECTOR_OPENED";
    static EVENT_SELECTOR_CLOSED = "EVENT_SELECTOR_CLOSED";


    #fetchers = [];
    #tabbed = false;
    #whereToDraw;
    #data = {};
    #hiddenValue;
    #header;

    #tab;
    #wrapper = dom.createModal(dom.REMOVE_TYPE_HIDE).cl('chooser-wrapper').hide().appendToBody();
    #content = h.div("remote-chooser").appendTo(this.#wrapper);
    #status
    #innerForm;
    #parent;
    #searchText;
    #allowDelete = true;
    #options;
    #toShowModal = true;
    #favoriteEnabled;
    #openButton;

    getOptions() {
        return this.#options
    }

    getParent(){
        return this.#parent
    }

    getOpenButton(){
      return this.#openButton;
    }

    isFavoriteEnabled() {
        return this.#favoriteEnabled;
    }

    getRequest(uri){
        var request = this.getOptions().getServerRequest();

        if (_.isNull(request)){
            request= new ServerRequest().entry();
        }

        return request.copy(true, uri )
    }

    fireEvent(eventName, additional, res = null) {

        if (_.isNull(this.#parent) || _.isNull(this.#parent.getFormFieldListener())) {
            return;
        }
        let magicFormEvent = new MagicFormEvent(eventName, this.#parent, additional, this.#header?.getField(), res);
        this.#parent.getFormFieldListener()(magicFormEvent);
    }

    constructor(whereToDraw, data, hiddenElement, header, parent, options = new MagicRemoteFetcherOptions()) {
        this.#parent = parent;
        this.#options = options;
        this.#favoriteEnabled = this.getOptions().isFavoriteEnabled() && Favorite.enabled() && this.#options.isFavoriteEnabled();
        this.#hiddenValue = h.from(hiddenElement);
        this.#header = header;
        this.#whereToDraw = h.from(whereToDraw);

        this.fireEvent(MagicRemoteChooser.EVENT_BEFORE_INITIALIZATION, this);

        if (this.#parent && this.#parent.getWrapper) {
            this.#wrapper.remove()
            this.#wrapper.appendTo(this.#parent.getWrapper())
        }

        if(data) {
          this.#data = data;
        }

        this.#tab = new MagicTab();

        try {

            this.#fetchers = getCustomSearchFetchers(this, this.getOptions().getRequest(), data);

            if (_.isEmpty(this.#fetchers)) {
                throw Error('No fetchers found')
            }
        } catch (e) {

            let fetcher;
            this.addFetcher(fetcher = new SearchMagicFetcher(this))
            this.fireEvent(MagicRemoteChooser.EVENT_FETCHER_ADDED, fetcher)
            this.addFetcher(fetcher = new FavoriteFetcher(this))
            this.fireEvent(MagicRemoteChooser.EVENT_FETCHER_ADDED, fetcher)
        }



        this.#status = new SearchMagicFetcher(this).getStatus()
        this.draw();
        this.addCloseBtn();
        this.fireEvent(MagicRemoteChooser.EVENT_CHOOSER_INITIATED, this);
    }

    setToShowModal(val){
      this.#toShowModal = val;

      return this;
    }

    getWhere() {
        return this.#whereToDraw;
    }

    getData() {
        return this.#data;
    }

    getHeader() {
        return this.#header;
    }

    setAllowDelete(val) {
        this.#allowDelete = val;
    }

    init() {
        this.drawSelection();
    }

    buildAddForm() {
        log.debug('adding form')
        if (utils.isNull(this.#innerForm)) {

            let formId = this.#whereToDraw.getId() + "_s_box_add" + new Date().getTime();

            this.#innerForm = h.div("modal").id(formId).appendTo(this.#content);
        }
        try {

            new fetcher(this.#innerForm.getId(), this.getOptions().getRequest(), "form", null, this).new();
        } catch (exc) {
            console.error(exc)
            this.#wrapper.hideModal();
        }

    }


    addCloseBtn() {
        this.#content.add(h.div('close-chooser-btn').text('global.chooser.close').click(() => {
            this.#fetchers[0].getSearchText().text('', false)
            this.hideForm();
        }))
    }

    closeImg() {
        return dom.createCloseImage().click(() => {
            this.#fetchers[0].getSearchText().text('', false)
            this.hideForm();
        })
    }

    draw() {

        utils.each(this.#fetchers, (fetcher) => {
            this.#tab.addLazyTab(fetcher.getName(), () => {
                return fetcher.getContent()
            }, fetcher)
        });

        let windowHeaders = h.div('chooser_headers').appendTo(this.#content);
        this.#status.appendTo(windowHeaders);
        this.closeImg().appendTo(windowHeaders);

        this.#tab.deployTo(this.#content);
        this.drawSelection();
        // this.showForm();
    }

    hideForm() {
        this.#wrapper.hideModal();
        this.fireEvent(MagicRemoteChooser.EVENT_SELECTOR_CLOSED)
    }

    showForm() {
        
      if(this.#toShowModal){
        this.#wrapper.showModal();
      }

        _.each(this.#fetchers, fetcher => {
                fetcher.drawItemSelector(fetcher);
                fetcher.showSelection(0)
            }
        )
        this.fireEvent(MagicRemoteChooser.EVENT_SELECTOR_OPENED, this.#wrapper);

      if(!this.#toShowModal){
        return this.#tab.getContent();
      }
    }

    getContent() {
        return this.#content;
    }


    drawSelection() {
        this.getWhere().text(null);
        let _this = this;
        if (utils.isNull(this.getData()) || utils.isEmpty(this.getData().value)) {

            let value = h.a("#", MagicRemoteChooser.SELECT_TEXT, true).id("remote_selector_" + this.getHeader().getField() ).add(h.div("select_item")).click(function () {
                _this.showForm(this)
            }, false);

            this.#openButton = h.div("magic_object_chooser_content").add(value).appendTo(this.getWhere());
        } else {

            let drawContent;

            try {
                drawContent = drawCustomSelectChooserElementValue(this.getHeader(), this.getData()).click(function () {
                    _this.showForm(this)
                }, false);
            } catch (e) {

            }

            if (drawContent == null) {
                drawContent = MagicTable.defaultConverter(this.#header, this.#data, this.#data, null, null, function () {
                }).click(function () {
                    _this.showForm();
                });
            }

            let value = h.a("#", "").add(drawContent);

            let allowDelete = true;


            let cleanValueImg = h.img('rm.png').click(function () {
                _this.#data = {};
                _this.init();
                if (_this.#whereToDraw) {
                    _this.#whereToDraw.setData('id', 0);
                    _this.#whereToDraw.setData('id', 0);
                }
                _this.#oldValue = _this.#hiddenValue.val();
                _this.#hiddenValue.text("", false);
                _this.fireEvent(MagicRemoteChooser.EVENT_ITEM_DESELECTED)
            }, false);

            //reset where and add correspondent result;
            this.getWhere().text(null);

            this.#openButton =  h.div("magic_object_chooser_content").add(value).addIf(this.#allowDelete, cleanValueImg).appendTo(this.getWhere());
            this.fireEvent(MagicRemoteChooser.EVENT_ITEMS_RENDERED, this.getWhere())
        }

    }

    setData(val) {
        this.#data = val;
    }

    compile(res, field, favType) {

        let _this = this;
        let element;

        let heart = h.div("empty");
        if (this.#favoriteEnabled) {

            heart = h.span(Favorite.FAVORITE_CLASS + "_holder", "", false).click(function (e) {

                if (_.notNull(favType)) {
                    Favorite.markElementAsFavorite(e, res, favType)
                }
                _.each(_this.#fetchers, fetcher => {
                        fetcher.drawItemSelector(fetcher);
                        fetcher.showSelection(0)
                    }
                )
            })

            if (_.notNull(favType)) {
                Favorite.markIfElementFavorite(heart, res, favType)
            }
        }
        try {
            element = drawCustomSelectChooserElement(this.#header, res, this.#data);
        } catch (e) {
            log.trace(e, true);
            element = h.div("selection").text(res[this.#header.getData().searchField], false);
        }

        if (_.isNull(element)) {

            element = h.div("selection").text(res[this.#header.getData().searchField], false);
        }
        let final = h.a("#").cl("selection").id("remote_selector_" + this.getHeader().getField() + res['id']).clickPreventive(function () {
            _this.selectData(res)
        }).add(element)

        let finaldiv = h.div('selection_div').add(final)


        if (this.#favoriteEnabled) {
            finaldiv.add(heart);
        }
      
        this.fireEvent(MagicRemoteChooser.EVENT_ITEM_RENDERED, final, res);
        return finaldiv;

    }


    selectData(data) {

        this.#oldValue = this.#hiddenValue.val();
        this.#hiddenValue.text(data.id, false);
        this.#whereToDraw.setData('id', data.id);
        if (utils.isEmpty(data.value)) {
            if(!_.isObject(this.#data)){
                this.#data = {} 
            }
            this.#data.value = data[this.#header.getData().searchField] ;
            this.#data.id = data.id;
            this.#data.key = data.id;
            this.#data.additional = data.icon;

            try {
                transformCustomData(this.#header.getField(), this.#data, data);
            } catch (e) {
                log.trace(e, true)
            }

        } else {
            this.#data = data;
        }
        this.drawValueInWhere(this.#data);
        if (!_.e(this.#oldValue, this.#hiddenValue.val())) {
            this.fireEvent(MagicRemoteChooser.EVENT_ITEM_CHANGED, data);
        }
        this.hideForm();
        this.#hiddenValue.get().dispatchEvent(new Event('change'));
        this.fireEvent(MagicRemoteChooser.EVENT_ITEM_SELECTED, data);

    }

    drawValueInWhere(data) {
        this.#data = data;
        this.drawSelection();
    }

    addFetcher(fetcher) {
        this.#fetchers.push(fetcher);
        return this;
    }

    showChooserPagination(res, fetcher) {
        let _this = this
        let totalPages = res.totalPages;
        let currentPage = res.number + 1;
        fetcher.getContent().get().querySelector('.chooser_pagination')?.remove()
        if (totalPages > 1) {
            let pagination = h.div('chooser_pagination');
            let prevBtn = h.img('prev.svg').cl('prev_btn_chooser').appendTo(pagination);
            h.div('chooser_bord').text(`${currentPage} / ${totalPages}`, false).appendTo(pagination);
            let nextBtn = h.img('next.svg').cl('next_btn_chooser').appendTo(pagination);

            if (currentPage == totalPages) {
                nextBtn.cl('disabled_btn_chooser');
                prevBtn.click(function () {
                    fetcher.showSelection(res.number - 1);
                    pagination.remove();
                })
            }
            if (currentPage == 1) {
                prevBtn.cl('disabled_btn_chooser');
                nextBtn.click(function () {
                    fetcher.showSelection(res.number + 1);
                    pagination.remove();
                })
            }
            if (currentPage !== totalPages && currentPage !== 1) {
                prevBtn.click(function () {
                    fetcher.showSelection(res.number - 1);
                    pagination.remove();
                })
                nextBtn.click(function () {
                    fetcher.showSelection(res.number + 1);
                    pagination.remove();
                })
            }

            fetcher.getContent().add(pagination.get())
        }

    }

    getDataById(id, type, addFunc = (data) => {}) {
        if (id < 1) {
            return undefined;
        }

        this.getOptions().getRequest().fetchDetails(id,(res) => {
            this.selectData(res);
            addFunc();
        });
    }

    getWrapper() {
      return this.#wrapper;
    }



    static createMagicChooserHeader(what, searchName = "name"){
        return {
            getData() {
              return {
                searchName: what,
                searchField: searchName,
              };
            },
            getField() {
              return what;
            },
            getType() {
              return "object_chooser";
            },
          };
    }


}