class MagicOrdering {
    #parent;

    constructor(parent) {
        this.#parent = parent
    }

    #content;

    #headers = [];
    #defaultHeader = []

    close() {
        this.#content.hideModal()
    }

    show() {
        this.#content.showModal()
    }

    getHeaders() {
        return this.#headers;
    }

    setHeaders(newHeader) {
        this.#headers = newHeader;
        this.#parent.drawValues();
        return this.#headers;
    }

    build() {
  
        var fields = this.#parent.getLastData().fields;

        var fieldsFilter = this.#parent.getOptions().getOrderedFieldsFilter();
        
        if(fieldsFilter) {
          fields = fieldsFilter(fields);
        }

        this.#content = dom.createModal(dom.REMOVE_TYPE_HIDE)
        .cl("sortable_panel")
        .cl("ordering_form")
        .cl("hidden")
        .appendTo(this.#parent.getContainer());

        h.div("sort_header").text("global_ordering").appendTo(this.#content);
        this.defaultButton = h.input('button').text('global.ordering.default').cl('default_order_btn').click(()=>{      
            this.setDefOrder()
        }).appendTo(this.#content);

        this.elemContainer = h.div("element_container").appendTo(this.#content);
        let _this = this;

        utils.each(fields, (item) => {
                let header = item;
                this.#headers.push(item);
                this.#defaultHeader.push(item)
                let row = h.div("header_row").appendTo(this.elemContainer);

                let input = h.input("checkbox").change(function (e) {
                    let field = e.getData("id");
                    let visible = e.checked();

                    _this.changeFieldVisibility(field, visible);

                }).id(this.getUnicNumber() + "_chk_" + header.field).data_id(header.field).attrChecked(header.visible).appendTo(row);
                h.label(input, header.translation, !item.customized).appendTo(row);
                h.span('sorting_title').appendTo(row);
  
        });
      
        new Drug(this.getUnicNumber(), this.elemContainer.get(), function (o, n) {
            _this.moveColumns(o, n);
        }, "header_row");
       
        dom.createCloseImage().click(function () {
            _this.close();
        }).appendTo(_this.#content)

    }

    getUnicNumber(){
      return  Math.ceil(Math.random() * (9999 - 1) + 1)
    }

    getOrder() {
        let arr = [];
        for (let i = 0; i < this.#headers.length; i++) {
            arr.push(this.#headers[i].field)
        }

        return arr;
    }

    setDefOrder(){
        let defArrow = this.#defaultHeader.map(e => e.field)
        this.setOrdersFromTemplate(defArrow)
    }

    setOrdersFromTemplate(newFieldsOrder){
        let modifiedHeader = [];
        let modifiedOrder = [];
        newFieldsOrder.forEach((newFieldEl) => {
            modifiedHeader.push(...this.#headers.filter(hedersEl => hedersEl.field == newFieldEl))   
            modifiedOrder.push(...[...this.elemContainer.childNodes()].filter(hedersEl => h.from(hedersEl).first().getDataId() == newFieldEl ) )
       })

       this.elemContainer.text('', false)
       modifiedOrder.forEach(e => h.from(e).appendTo(h.from(this.elemContainer)))

       this.#headers = modifiedHeader
       this.visibilityTemlateImg();
       this.#parent.drawValues();

    }

    changeFieldVisibility(field, visibility) {
        for (let i = 0; i < this.#headers.length; i++) {
            if (utils.e(this.#headers[i].field, field)) {
                this.#headers[i].visible = utils.isTrue(visibility);
                this.#parent.drawValues();
                break;
            }
        }
    }

    #array_move(arr, old_index, new_index) {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        this.visibilityTemlateImg();
        return arr; // for testing
    };

    moveColumns(a, b) {
        this.#headers = this.#array_move(this.#headers, a, b);
        this.#parent.drawValues();

    }

    visibilityTemlateImg(){
        let sameArr = true
        this.#headers.forEach((e,i)=>{
            if(!_.e(e.field, this.#defaultHeader[i].field)){
                sameArr = false
            } 
        }); 

        sameArr ? this.#parent.getSearchForm().hideSaveTemplatesImg() : this.#parent.getSearchForm().showSaveTemplatesImg();
        sameArr ? this.defaultButton.rcl('active_btn') : this.defaultButton.cl('active_btn');
    }
}