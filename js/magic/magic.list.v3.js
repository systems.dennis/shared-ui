class TableDrug{
    onDragStart;
    allowedClass;

    onDragDrop;
}
class MagicListOptions {

    static SELECT_MODE_NONE = 0;
    static SELECT_MODE_SINGLE = 1;
    static SELECT_MULTI = 2;

    static DISPLAY_AS_TABLE = "table";

    static DISPLAY_AS_GREED = "grid";

    #drag;

    #gridOptions;

    #searchEnabled;
    #settingsEnabled;
    #orderedFieldsFilter = MagicOrdering.removeActionsField;

    #drawPaginationTo;

    #allowedTableActions;
    #dataPath;

    #specificationRoot;
    #searchListner
    #parent;
    #customTableAction;
    #apiRoot;
    #dataType;

    #displayType = MagicListOptions.DISPLAY_AS_TABLE;
    #attribute;

    setDrawPaginationTo(drawTo){
        this.#drawPaginationTo  = drawTo;
    }

    getDrawPaginationTo(){
        return this.#drawPaginationTo;
    }

    getGridOptions(){
        return this.#gridOptions;
    }

    setGridOptions(opts){
        if (_.notNull(opts) && ! (opts instanceof MagicGridOptions)){
            throw  Error ("options should be extended from MagicGridOptions!")
        }
        this.#gridOptions = opts;
        return this;
    }

    setDrag(dragInfo){
        if (dragInfo instanceof TableDrug){
            this.#drag = dragInfo;
            return this;
        }
        throw Error("Drag should be instance of TableDrug");
    }

    getDrag(){
        return this.#drag;
    }



    setDisplayType(type){
        this.#displayType = type;
        return this;
    }

    getDisplayType(){
        return this.#displayType;
    }


    getAttribute(){
        return this.#attribute
    }

    setAttribute(attribute){
        this.#attribute= attribute;
        return this;
    }

    #needMultiSelect = true;

    #selectable = false;

    #dataPathModifier;

    #listener;
    #tableListener;

    #availableFields;
    #listGroupToPath = "";
    #contextMenuToGroupTransformer = null;
    #listListener;

    setContextMenuToGroupTransformer(func){
      this.#contextMenuToGroupTransformer = func;

      return this;
    }

    getContextMenuToGroupTransformer() {
      return this.#contextMenuToGroupTransformer
    }

    #contextMenuListener;
    #groupId
    static DEFAULT(type) {
        return new MagicListOptions(type)
    };

    #queryModifier;


    setAvailableFields(fields) {
        this.#availableFields = fields;
    }

    setListListener(listener){
        this.#listListener = listener;
    }

    getListListener(){
        return this.#listListener;
    }

    getOrderedFieldsFilter() {
      return this.#orderedFieldsFilter;
    }

    setOrderedFieldsFilter(func) {
      this.#orderedFieldsFilter = func;

      return this;
    }

    getAvailableFields() {
        return this.#availableFields;
    }

    setCustomTableAction(val) {
      this.#customTableAction = val; return this;
    }

    getListGroupToPath(){
        return  this.#listGroupToPath
    }

    getCustomTableAction(){
      return this.#customTableAction;
    }

    setTableAllowedActions(actions) {
        this.#allowedTableActions = actions;
    }

    getTableAllowedActions() {
        return this.#allowedTableActions;
    }

    #columnRenderer

    setQueryModifier(modifier) {
        this.#queryModifier = modifier;
    }

    setColumnRenderer(renderer) {
        this.#columnRenderer = renderer;
    }

    setContextMenuListener(listener) {
        this.#contextMenuListener = listener;

        return this;
    }

    setMultiSelect(val) {
        this.#needMultiSelect = val;

        return this;
    }

    getNeedMultiselect() {
        return this.#needMultiSelect;
    }

    getContextMenuListener() {
        return this.#contextMenuListener;
    }

    getColumnRenderer() {
        return this.#columnRenderer;
    }

    getParent() {
        return this.#parent;
    }

    getQueryModifier() {
        return this.#queryModifier;
    }

    getTableListener() {
        return this.#tableListener;
    }

    getSearchListener() {
        return this.#searchListner;
    }

    #formListener = null;
    #formFieldListener = null;

    setTableListener(listener) {
        this.#tableListener = listener;
        return this;
    }

    setFormListener(listener) {
        this.#formListener = listener
    }

    setFormFieldListener(listener) {
        this.#formFieldListener = listener

        return this;
    }

    setDataPathModifier(dataPathModifier) {
        this.#dataPathModifier = dataPathModifier;
    }

    setSearchListener(listener) {
        this.#searchListner = listener;
    }

    getFormListener() {
        return this.#formListener;
    }

    getFormFieldListener() {
        return this.#formFieldListener;
    }

    getDataPathModifier() {
        return this.#dataPathModifier;
    }


    constructor(dataType,
                specificationRoot = null,
                apiRoot = null, dataPath = null,
                settingsEnable = true,
                searchEnabled = true,
                selectMode = MagicListOptions.SELECT_MODE_NONE,
                parent = null,
    ) {

        if (specificationRoot == null) {
            specificationRoot = "/root/fetch/list";
        }
        if (apiRoot == null) {
            apiRoot = "/api/v2/"
        }

        if (dataPath == null) {
            dataPath = "/root/fetch/data";
        }

        this.#dataType = dataType;
        this.#specificationRoot = specificationRoot;
        this.#apiRoot = apiRoot;
        this.#dataPath = dataPath;
        this.#searchEnabled = searchEnabled;
        this.#settingsEnabled = true;
        this.selectable = false;
        this.#parent = parent;
    }

    getDataPath(path = '') {
        let res = Environment.dataApi + this.#apiRoot + this.#dataType + this.#dataPath + path;
        if (this.getDataPathModifier() != null) {
            let mod = this.getDataPathModifier();

            res = mod(res, this);
        }

        return res;
    }

    getSpecificationPath() {
        return   Environment.dataApi + this.#apiRoot + this.#dataType + this.#specificationRoot;
    }

    getRoot(afterPath = '') {
        return Environment.dataApi + this.#apiRoot + this.#dataType + afterPath;
    }

    getDataType() {
        return this.#dataType;
    }

    getSpecificationRoot() {
        return this.#specificationRoot;
    }

    getApiRoot() {
        return this.#apiRoot;
    }

    getaPath() {
        return this.#dataPath;
    }

    getSettingsEnabled() {
        return this.#settingsEnabled;
    }

    getSearchEnabled() {
        return this.#searchEnabled;
    }

    fire(event){
        if (_.notNull(this.#listListener)) {
            return this.#listListener(event);
        }
    }

    #isEnabled = false;

    setIsFavoriteEnabled(value){
        this.#isEnabled = value;
        return this;
    }

    getIsFavoriteEnabled() {
        return this.#isEnabled;
    }
}

class MagicList2 {
    #options;
    #listListener;

    #container;
    #magicTable;

    #headers;
    #fetchData;

    #dataType;

    #grid = h.div("pg_table_container");

    #form = h.div("modal").cl("hidden").id(new Date().getTime());

    #formClass;
    #sortAndSearchForm = h.div("table_config");
    #sort;

    #searchForm;

    #ordering;

    #favoriteType;
    #lastData;

    #pagination;

    #searchEnabled;

    #id;
    #specificationPath;

    getGrid() {
        return this.#grid;
    }

    getFavoriteType() {
        return this.#favoriteType;
    }

    getPagination() {
        return this.#pagination;
    }

    getRootPath() {
        return this.getOptions().getRoot();
    }

    getId() {
        return this.#id;
    }

    getLastData() {
        return this.#lastData;
    }

    getOrdering() {
        return this.#ordering;
    }

    getTableObject() {
        return this.#magicTable;
    }

    getContainer() {
        return this.#container;
    }

    getSearchEnabled() {
        return this.#searchEnabled;
    }

    setListListener(listener) {
        this.#listListener = listener;
    }

    constructor(dataType, options = null) {

        if (options == null) {
            options = MagicListOptions.DEFAULT(dataType);
        }
        this.#options = options;
        this.#dataType = dataType;

        /**
         * We assume that here can only 1 parameter to be changed.
         * For this, we accept nulls as value by default
         */

    }

    getFetchData(){
      return this.#fetchData;
    }

    getForm() {
        return this.#form;
    }

    getFormClass(){
      return this.#formClass;
    }

    getDataType(){
        return this.#dataType
    }

    getSort() {
        return this.#sort;
    }

    getOptions() {
        return this.#options;
    }

    getListListener() {
        return this.#listListener;
    }

    getSearchAndSettingsBar() {
        return this.#sortAndSearchForm;
    }

    getSearchForm() {
        return this.#searchForm;
    }

    setSpecificationPath(path){
        this.#specificationPath = path;
        return this
    }

    getSpecificationPath(){
        return this.#specificationPath;
    }


    load(where) {
        let _this = this;
        this.#specificationPath = this.getOptions().getSpecificationPath();

        this.#options.fire(new MagicListEvent(MagicListEvent.PRELOAD_LIST, this));

        Executor.runGet(this.#specificationPath, function (data) {
            _this.build(data, where);
        });

    }

     startActions(event) {

          let action = event.eventType;
          let id = event.id;
          let list = event.parent.parent().getParent();


          if (!_.isNull(list.getOptions()) && !_.isNull(list.getOptions().getContextMenuListener())) {
              if (list.getOptions().getContextMenuListener()(event)) {
                  return;
              }
          }

          if (_e(action, 'edit')) {
              let magicFormOptions = new MagicFormOptions(list.getOptions().getDataType(), undefined, list);
              magicFormOptions.setFormListener(list.getOptions().getFormListener());
              magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
              MagicForm.Instance(list.getOptions().getDataType(), magicFormOptions).load(id);
              return;
          }

          if ((_e(action, "copy"))) {
              let copy = true;
              let magicFormOptions = new MagicFormOptions(list.getOptions().getDataType(), undefined, list);
              magicFormOptions.setFormListener(list.getOptions().getFormListener());
              magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
              MagicForm.Instance(list.getOptions().getDataType(), magicFormOptions).load(id, copy);
              return;
          }


          if (_e(action, 'delete')) {
              if (list.getTableObject().getSelectedCount() >= 2) {
                  _.each(list.getTableObject().getSelectedIds(), (id) => {
                    list.deleteById(list, id)
                  })
              } else {
                list.deleteById(list, id)
              }
          }

          if (_e(action, 'details')) {
              showDetails(id);
          }

          if (utils.e(action, 'import')) {
              if (e.getAttribute("disabled") != undefined) {
                  h.message("global.messages.import.in_process");
              }

              let progress = new Loading(list.getGrid());
              progress.show(img);

              Executor.runPost(list.getRootPath() + "/import/", function () {
                  progress.hide();
                  dom.toast(_.$("global.messages.success"));
                  e.removeAttribute("disabled");
                  list.drawValues()

              }, function (e) {
                  Executor.errorFunction(e);
                  progress.hide();
                  e.removeAttribute("disabled");
              });
              return;

          }


      }

      deleteById(list, id) {
        let opts = new MagicPromptOptions("global.prompt.allow_delete_title", "global.prompt.change_group_text.allow_delete_text");
        opts.applyClick(MagicPromptButton.OK_LABEL, () => {
            opts.closeSelf();
            list.deleteRowByContextMenu(list, id)
            opts.markNoRepeat()
        }).setIsAction(MagicPromptButton.OK_LABEL, true)
            .applyClick(MagicPromptButton.CANCEL_LABEL, () => opts.closeSelf()).setIsAction(MagicPromptButton.CANCEL_LABEL, false)
            .allowNoRepeat("global.prompt_DELETE").newPrompt();

    }

    deleteRowByContextMenu(list, id) {
        let path = list.getOptions().getRoot("/delete/" + id)
        Executor.run(path, "DELETE", (data) => {
            list.drawValues();
            dom.message();

        });
    }


    build(data, where) {
        this.#lastData = data;
        let options = this.getOptions();
        if (!_.isEmpty(options.getAvailableFields())) {
            let newFields = [];
            _.each(data.fields, (item) => {
                if (!options.getAvailableFields().includes(item.field)) {
                    item.visible = false;
                }
            })
        }

        this.#id = where;
        this.#container = h.from(where.isH ? where : get(where));

        this.drawListTitle(where);
        //form is used to generate edit form or add form
        this.#form.appendTo(this.#container);

        //grid is a container where <table> is generated
        this.#grid.appendTo(this.#container);
        this.drawPagination()

        //when we are ready we can draw values from the server

        this.drawValues()

        return this;

    }

    drawListTitle() {
        let data = this.#lastData;
        this.#favoriteType = data.objectType;
        if (data.showTitle) {
            h.div("title").text(data.tableTitle).appendTo(this.#container);
        }
        this.#headers = data; //todo check if we still need to hold it here
        this.#sortAndSearchForm.appendTo(this.#container);
        this.drawSettingsForm();
        if (this.#options.getSearchEnabled()) {
            this.drawSearch(data);
        }
        this.drawTableActions(data)
        this.createDataHeaders(data['fields']);
    }


    createDataHeaders() {
        if (_e(this.getOptions().getDisplayType(), MagicListOptions.DISPLAY_AS_TABLE)){
            this.#magicTable = new MagicTable(this).drawHeader(this.#grid);
        } else {
            this.#magicTable = new MagicGrid(this, this.getOptions().getGridOptions()).drawHeader(this.#grid);
        }
    }

    drawSettingsForm() {
        this.#sort = new MagicSort(this);
        this.#sort.build();

        this.#ordering = new MagicOrdering(this);
        this.#ordering.build();
    }

    drawTableActions(data) {

        //List actions are coming from server, if they are existing we need to draw them
        //Default action are: settings : open list settings
        //                    download : download the content of the table
        //                    new      : creates a new element
        // if action differs then it is expected that
        //                   drawCustomAction is defined by user
        // this method should be loaded on the page itself
        if (utils.isEmpty(data.listActions)) {
            return;
        }

        let div = h.div("actions");

        for (let i = 0; i < data.listActions.length; i++) {
            try {
                div.add(this.drawTableAction(data.listActions[i], div, data))
            } catch (ex) {
                log.error(ex);
            }
        }

        this.getSearchAndSettingsBar().add(div);


    }

    drawTableAction(action, div, data) {

        if (_.notNull(this.getOptions().getTableAllowedActions())

            && !this.getOptions().getTableAllowedActions().includes(action)) {
            return;
        }

        try {
          return this.getOptions().getCustomTableAction()(action, div, data)
        } catch (error) {
        }

        let _this = this;
        if (action == 'settings') {
            return h.tag("span").cl("settings")
                .add(h.divImg("sort_icon.png.png").cl(action).wh(16).click(function () {
                    _this.getSort().show();
                    _this.getOrdering().close();
                }))
                .add(h.divImg("settings.png").cl(action).wh(16).click(function () {
                    _this.getOrdering().show();
                    _this.getSort().close();
                })).get();

        }


        if (action == 'download') {


            let _this = this;

            return h.divImg("download.png").cl(action).wh(16).click(function () {
                Executor.runPostWithPayload(_this.getRootPath() + "/root/download/data", (data) => {
                  h.a(_this.getRootPath() + "/root/download/data/" + data.pathToDownload, "", false).get().click();
                }, _this.createPayload(), (e) => {
                  log.error(e)
                }) 

            }).get();
        }


        if (action == 'new') {
            let __ = this;

            this.#grid.add(div);
            return h.input("button").cl(action).cl("insert_new_btn").text("pages.forms.insert_new").click(this.loadForm).get();
        }

        if (action == 'import') {
            return h.divImg("import.png").wh("16").cl(action).click(function (e) {
                if (e.getAttribute("disabled") != undefined) {
                    dom.message("global.messages.import.in_process", true, ToastType.NODATA)
                }

                let progress = new Loading(_this.getGrid().get());
                progress.show(e);


                Executor.runPost(_this.getRootPath() + "/import/", function () {
                    progress.hide();
                    dom.toast(_.$("global.messages.success"), ToastType.SUCCESS)
                    _this.drawValues();
                }, function (e, x, z) {
                    progress.hide();
                    Executor.errorFunction(e, x, z);
                });
            }).get();


        }

        try {
            return drawCustomAction(action, div, null, this, data)
        } catch (ex) {
            log.error(ex)
            return h.span("info.error.list.actions", _.$("global.list.actions.not.implemented." + action));
        }

    }

    loadForm = () => {
      let magicFormOptions = new MagicFormOptions(this.#options.getDataType(), undefined, this)
      magicFormOptions.setFormListener(this.#options.getFormListener());
      magicFormOptions.setFormFieldListener(this.#options.getFormFieldListener());
      this.#formClass = MagicForm.Instance(this.#options.getDataType() + this.#options.getAttribute(), magicFormOptions).load();
    }

    drawValues() {
        let path = this.getOptions().getDataPath();
        let _this = this;


        Executor.runPostWithPayload(path, function (data) {
            _this.buildValues(data);

        }, this.createPayload());

    }

    buildValues(data) {
        this.#fetchData = data;
        this.#magicTable.rebuildHeaders();
        this.#magicTable.drawValues(data, this);
        this.getPagination().rebuildPages(data);
        this.#options.fire(new MagicListEvent(MagicListEvent.BUILD_VALUES, this, data ));
    }

    drawPagination() {
        this.#pagination = new MagicPagination(this.#magicTable);
        this.#options.fire(new MagicListEvent(MagicListEvent.PREBUILD_PAGINATION, this, this.#pagination ));
        this.#pagination.build(this.getId())
    }

    drawSearch(data) {
        if (this.getOptions().getSearchEnabled()) {
            this.#searchForm = new SearchForm(data, this.getId(), this);
            this.#searchForm.build();
        }
    }


    startActions(event, incomingList = null) {

        let action = event.eventType;
        let id = event.id;
        let list =  incomingList || event.parent.parent().getParent();
        

        if (!_.isNull(list.getOptions()) && !_.isNull(list.getOptions().getContextMenuListener())) {
            if (list.getOptions().getContextMenuListener()(event)) {
                return;
            }
        }

        if (_e(action, 'edit')) {
            let magicFormOptions = new MagicFormOptions(list.getOptions().getDataType(), undefined, list);
            magicFormOptions.setFormListener(list.getOptions().getFormListener());
            magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
            MagicForm.Instance(list.getOptions().getDataType(), magicFormOptions).load(id);
            return;
        }

        if ((_e(action, "copy"))) {
            let copy = true;
            let magicFormOptions = new MagicFormOptions(list.getOptions().getDataType(), undefined, list);
            magicFormOptions.setFormListener(list.getOptions().getFormListener());
            magicFormOptions.setFormFieldListener(list.getOptions().getFormFieldListener());
            MagicForm.Instance(list.getOptions().getDataType(), magicFormOptions).load(id, copy);
            return;
        }


        if (_e(action, 'delete')) {

            if (list.getTableObject().getSelectedCount() >= 2) {
                _.each(list.getTableObject().getSelectedIds(), (id) => {
                  list.deleteById(list, id)
                })
            } else {
              list.deleteById(list, id)
            }
        } 

        if (_e(action, 'details')) {
            showDetails(id);
        }

        if (utils.e(action, 'import')) {
            if (e.getAttribute("disabled") != undefined) {
                h.message("global.messages.import.in_process");
            }

            let progress = new Loading(list.getGrid());
            progress.show(img);

            Executor.runPost(list.getRootPath() + "/import/", function () {
                progress.hide();
                dom.toast(_.$("global.messages.success"));
                e.removeAttribute("disabled");
                list.drawValues()

            }, function (e) {
                Executor.errorFunction(e);
                progress.hide();
                e.removeAttribute("disabled");
            });
            return;

        }
    }


    createPayload(withoutCustomModifier = false) {
        if (this.#pagination == null) {
            return new MagicRequest();
        }

        let req = new MagicRequest();
        let page = this.#pagination.getPageNumber();
        req.limit = this.#pagination.getPerPage();
        req.page = page;

        if (this.getOptions().getSearchEnabled()) {
            req.query = this.#searchForm.getSearchCriteria();
        }

        req.sort = this.getSort().getSort();


        if (!withoutCustomModifier && this.getOptions().getQueryModifier() != undefined) {
            let func = this.getOptions().getQueryModifier();
            func(req, this);
        }

        return req;
    }

    static formListener(event) {
        log.debug(event);

    }
}

