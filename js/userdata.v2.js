class Userdata {
    #tokenField = "___TOKEN___";
    #userLanguage = '___user_lang___';
    static NO_PROFILE_IMAGE = "no_profile_image.svg";
    static USER_IMAGE_KEY = "___image";

    constructor() {
        if (Environment.self == undefined) {
            throw new Error("No auth server selected!");
        }
        if (Environment.authApi == undefined) {
            throw new Error("No auth server selected!");
        }
    }


    verifyPage(redirectTologinPage = true) {
        Authorization.verifyPage(redirectTologinPage)
    }

    isRequestEndsWith(what) {
        return document.location.pathname.endsWith(what) || document.location.pathname.endsWith(what + "/");
    }



    refresh(data) {

        if (data != undefined && data['id'] != null) {
            cache.add(Userdata.USER_IMAGE_KEY, data['imagePath'])
        }

        let img = cache.get(Userdata.USER_IMAGE_KEY);

        if (_.isNull(img)) {
            let path = Environment.dataApi + "/api/v2/flaw/hr/me"
            Executor.runGet(path, (data)=>{
                _.isEmpty(data.icon)
                ? cache.add(Userdata.USER_IMAGE_KEY, Userdata.NO_PROFILE_IMAGE)
                : cache.add(Userdata.USER_IMAGE_KEY, data.icon)
            })
           img = cache.get(Userdata.USER_IMAGE_KEY) 
        }
        
        this.changeUserImageSrc(h.img(img, Environment.defaultIconSize).getSrc());
    }


    changeLanguage(lang, toReloadPage = true) {
   
        if(cache.isPresent(this.#userLanguage) && _e(cache.get(this.#userLanguage), lang)){
            return
        }
        cache.add(this.#userLanguage, lang)
        
        let path = Environment.dataApi + "/api/v1/translation/list/" + lang;

        Executor.runGet(path, (res) => {
            _.each(res, (item) => {
                localStorage.setItem('translate_' + item.key.toLowerCase() , item.value);
            });
        }, false, Executor.HEADERS, (e) => {
            log.error("Cannot load lang: " + lang);
        })


        if (toReloadPage) {
            Executor.runPostWithFailFunction(Environment.authApi + "/api/v2/auth/profile/update_lang/" + lang, function () {
            }, function () {
            }, true)
            document.location.reload();
        }

    }

    getLanguage() {
        return cache.get(this.#userLanguage) == undefined ? Environment.defaultLang : cache.get(this.#userLanguage);
    }

    checkLanguage(){
        if(!cache.isPresent(this.#userLanguage)){
            this.changeLanguage(Environment.defaultLang)
        }
    }

    initPage() {
        // this.verifyPage();
        this.checkLanguage();
        MagicPage.include();
        MagicPage.translatePage();
        this.drawUserButton()
    }

    showEditForm(where, listener = null) {

        let magicFormOptions = new MagicFormOptions("auth/profile")
        magicFormOptions.setPath(Environment.authApi)

        if (utils.notNull(listener)) {

            magicFormOptions.setFormListener(listener);

        }

        MagicForm.Instance("auth/profile", magicFormOptions).load(sessionStorage.getItem("___id"));
    }


    formListener = (event) => {

        if(_e(event.type, MagicFormEvent.FORM_SAVED)) {
            if(_.notNull(event.additional.imagePath)){
                this.changeUserImageSrc(event.additional.imagePath)
                cache.add(Userdata.USER_IMAGE_KEY, event.additional.imagePath);
            } else {
              this.changeUserImageSrc(Userdata.NO_PROFILE_IMAGE)
              cache.remove()
            }
        }
    }

    changeUserImageSrc = (src) => {
      h.from(document).eachOfClass("___profile_picture", (item)=>{
       
        item.src(src)

      }, false, true)
    }

    static performInclude(div) {
        let container = document.getElementById(div);
        this.doSingleInclude(div);
    }


    register(login, name, password, repeatPassword, errorFunc, success = (data) => {}, customPath = null, scope, authType = 'default') {

        var path = Environment.authApi + "/api/v3/auth/register"

        let regOptions = new AuthOptions()
            .addAdditionalHeader("AUTH_SCOPE", scope)
            .addAdditionalHeader("AUTH_TYPE", authType)
            .getAdditionalHeaders()
        log.promisedDebug(()=> regOptions)

        if(customPath) {
          path = customPath;
        }
      
        var payload = {
            'email': login,
            'password': password,
            'repeatPassword': repeatPassword,
            'name': name
        }

        Executor.runPostWithPayload(path, (data) => {
            success(data);
        }, payload, (e) => {
            errorFunc(e);
            log.error(e);
            dom.toast(MagicPage.translate("global.error"), ToastType.ERROR);
        }, regOptions)
    }


    login(login, password, savePassword, twoFactor = null, error, success, scope) {
        Authorization.login(login, password, twoFactor, savePassword, error, success, scope)
    }


    changePassword(login, password, newPassword, repeatPassword, twoFactor = null, success = null) {
        var auth = sessionStorage.getItem(this.#tokenField);
        var path = Environment.authApi + "/api/v3/auth/password/reset";

        var payload = {
            'currentPassword': password,
            'login': login,
            'twoFactorCode': twoFactor,
            'password': newPassword,
            'repeatPassword': repeatPassword
        }

        Executor.runPostWithPayload(path, (data) => {
            if(success) {
              success(data);
            }
            
        }, payload, (e) => {
            verifyResponse(e); log.error(e);

            try {
              let splitMessage = e.errorMessage.split(" ")
              
              log.error(e);
              dom.toast(MagicPage.translate(splitMessage[0]) + " " + MagicPage.translate(splitMessage[1]), ToastType.ERROR);
            } catch (error) {
              dom.toast(MagicPage.translate("global.error"), ToastType.ERROR);
            }
          
            
        }, Authorization.AUTH_HEADERS)

    }

    reset(login, success, error) {
    
        var path = Environment.authApi + "/api/v3/auth/password/forgot/" + login;

        Executor.runPost(path, success, Executor.HEADERS, true, error)
    }


    logout(headers = Executor.HEADERS) {
        Authorization.logout(true, headers);
    }

    drawUserButton() {

        try {
            _.each(document.getElementsByClassName('___user'), (el) => {
                el.innerHTML = sessionStorage.getItem('___login')
            });

            let img = cache.get(Userdata.USER_IMAGE_KEY);
            if (_.isNull(img)) {
                img = Userdata.NO_PROFILE_IMAGE
            }

            this.changeUserImageSrc(h.img(img, Environment.defaultIconSize).getSrc());

        } catch (exc) {
            log.error(exc);
        }
    }


}

/**
 * @deprecated since version 2 magic server request is responsible for error validation. This method will be removed in future
 * @param resp
 * @returns {boolean} Now it returns always true
 */

function verifyResponse(resp) {

        return  true;
}

const ____USER = new Userdata();