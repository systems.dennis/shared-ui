class Root {
    static getRoots(min = false) {
        let toLoad = [];
        toLoad.push("userdata.js");
        toLoad.push("magic/magic.dom.v2.js");
        toLoad.push("magic/magic.page.v1.js")
        toLoad.push("magic/magic.event.v1.js");
        toLoad.push("magic/magic.utils.v1.js");
        toLoad.push("magic/magic.sort.v1.js");
        toLoad.push("magic/magic.html.v2.js");
        toLoad.push("magic/magic.favorite.v1.js");
        toLoad.push("magic/magic.remote_chooser.v3.js");
        toLoad.push("magic/magic.executor.v2.js");
        toLoad.push("magic/magic.tabs.v1.js");
        toLoad.push("magic/magic.cache.v1.js");
        toLoad.push("magic/magic.authorization.v1.js");
        toLoad.push("magic/magic.table.v3.js")
        toLoad.push("const.js");
        toLoad.push("loading.js");


        return toLoad;
    }

}
