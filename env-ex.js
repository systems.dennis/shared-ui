class Environment {
    static self = "/auth_ui/";
    static authApi = "http://localhost:7777";

    static dataApi = "http://localhost:7777";

    static scriptServer = "http://dennis.systems/" // Environment.self;
    static defaultLang = 'ru'


    static fileStorageApi = "http://localhost:4444/api/v2/files";


    static loadBasics() {
        let toLoad = [];


        if (this.isNotRegisterLoginOrReset()) {
            this.#includeCSS("css/multi.css")
            this.#includeCSS("css/jquery-ui.css")
            this.#includeCSS("css/menu/menu_28102022.css?v19")
        }
        this.#includeCSS("css/toast.css")

        toLoad.push("js/jquery.js")
        toLoad.push("js/tynymce.js")
        toLoad.push("js/userdata.js")
        toLoad.push("js/html.js");
        toLoad.push("js/const.js");
        toLoad.push("js/toast.js");
        toLoad.push("js/loading.js");
        toLoad.push("js/chooser/file_chooser.js");
        toLoad.push("js/magic.js");
        toLoad.push("js/magic.fields.js");
        toLoad.push("js/magic_list.js");
        toLoad.push("js/magic_view.js");
        toLoad.push("js/chooser/remote_chooser.js");
        toLoad.push("js/qrcode.js");


        this.#includeJS(toLoad, 0, function (){ ____USER.verifyPage(Environment.isNotRegisterLoginOrReset());});


        window.onload = function (){
            ____USER.initPage();

            if (onPageLoad != undefined){
                onPageLoad();
            }

        }
    }

    static isNotRegisterLoginOrReset() {
        return document.location.href.indexOf("/client_login") == -1 && document.location.href.indexOf("/restore") == -1 && document.location.href.indexOf("/register") == -1
    }

    static #includeJS(toLoad, i, onEnd) {

        if (i >= toLoad.length - 1) {
            onEnd();
            return;
        }

        try {
            const head = document.getElementsByTagName('html')[0];

            const script = document.createElement('script');
            script.async = true;
            script.defer = true;
            script.setAttribute("defer", true)
            script.src = Environment.scriptServer + toLoad[i];
            script.type = 'text/javascript';

            script.onload = function () {

                Environment.#includeJS(toLoad, ++i, onEnd);
            }

            head.prepend(script)
        } catch (Exc){
            log.error(Exc);
        }
    }

    static #includeCSS(filename) {

        var head = document.getElementsByTagName('title');
        if (head.length == 0){
            head = document.getElementsByTagName('html')
        }
        head = head[0];

        var script = document.createElement('link');
        script.href = Environment.self + filename;
        script.rel = 'stylesheet';


        head.appendChild(script)
    }

}

Environment.loadBasics();